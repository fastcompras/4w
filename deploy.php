<?php

function config($key = null, $value = null) {
	static $config = null;
	if ($config === null) {
		$config = [];
		if (file_exists(".env.example")) {
			$array = parse_ini_file(".env.example");
			if (is_array($array)) {
				$config = array_replace($config, $array);
			} else {
				die("Invalid .env.example file\n");
			}
		}
		if (file_exists(".env")) {
			$array = parse_ini_file(".env");
			if (is_array($array)) {
				$config = array_replace($config, $array);
			} else {
				die("Invalid .env file\n");
			}
		}
	}
	if ($value !== null) {
		if (!$key) {
			$config[] = $value;
		} else {
			$config[$key] = $value;
		}
		$file = fopen(".env", "w");
		if ($file) {
			foreach ($config as $key => $val) {
				fwrite($file, $key."=".((strpos($val, " ") !== false)?"\"".str_replace("\"", "\\\"", $val)."\"":$val)."\n");
			}
			fclose($file);
		}
		return $value;
	}
	if ($key) {
		if (array_key_exists($key, $config)) {
			return $config[$key];
		}
		return null;
	}
	return $config;
}

$folderName = config("SERVER_FOLDER_NAME") ? config("SERVER_FOLDER_NAME") : "4w-homolog";

function serverShouldUpdateComposer($folderName) {
	$homeDir = __DIR__."/..";
	$publicHtmlDir = $homeDir."/public_html/";
	if (!file_exists($publicHtmlDir)) {
		return "Error: public_html folder missing at '".$publicHtmlDir."'";
	}

	if (!file_exists($publicHtmlDir.$folderName."/vendor")) {
		return "vendor folder missing";
	}

	$currentComposerLockPath = $homeDir."/4w-deploy-repo/4w/composer.lock";
	$pastComposerLockPath = $publicHtmlDir.$folderName."/composer.lock";
	if (!file_exists($currentComposerLockPath) || !file_exists($pastComposerLockPath)) {
		return "one of composer locks are missing";
	}

	$currentLockData = file_get_contents($currentComposerLockPath);
	$pastLockData = file_get_contents($pastComposerLockPath);

	if (strlen($currentLockData) !== strlen($pastLockData)) {
		 return "composer lock text length changed";
	}

	if ($currentLockData !== $pastLockData) {
		return "composer lock text changed";
	}

	return false;
}

if (isset($argv) && array_key_exists(0, $argv)) {
	$expected = "production-server-script";
	if (($argv[0] === $expected) || (substr($argv[0], -4) === ".php" && array_key_exists(1, $argv) && $argv[1] === $expected)) {
		$folderName = $argv[1] === $expected ? $argv[2] : $argv[1];
		if (!$folderName || !is_string($folderName) || strlen(trim($folderName)) <= 1) {
			var_export($argv);
			die("Unexpected 'folderName' at the production server script context");
		}

		$deployDate = (new DateTime(null, new DateTimeZone('America/Sao_Paulo')))->format('Y/m/d H:i:s');
		echo "\n\tServer finished loading repository at ".$deployDate."\n\n";
		if (!file_exists(__DIR__."/4w") || !file_exists(__DIR__."/4w/public")) {
			die("Could not find expected folders '4w' and '4w/public', deploy script is probably at an incorrect state and must be fixed");
		}

		echo "\tCopying ~/public_html/".$folderName."/public/documentos to ~/4w-deploy-repo/4w/public/documentos\n";
		passthru("rm -rf ~/4w-deploy-repo/4w/public/documentos && mkdir ~/4w-deploy-repo/4w/public/documentos 2>&1");
		passthru("cp -rf ~/public_html/".$folderName."/public/documentos/* ~/4w-deploy-repo/4w/public/documentos/ 2>&1");
		passthru("chmod 777 ~/4w-deploy-repo/4w/public/documentos 2>&1");
		usleep(500000);
		if (!file_exists(__DIR__."/4w/public/documentos")) {
			die("Could not copy the directory 'documentos' from:\n~/public_html/".$folderName."/public/documentos"."\nto:\n"."~/4w-deploy-repo/4w/public/documentos/");
		}
		if (count(scandir(__DIR__."/4w/public/documentos")) < 4) {
			die("The directory 'documentos' is unexpectedly empty after copying from:\n~/public_html/".$folderName."/public/documentos"."\nto:\n"."~/4w-deploy-repo/4w/public/documentos/");
		}

		echo "\tCopying ~/public_html/".$folderName."/config to ~/4w-deploy-repo/4w/config\n";
		passthru("rm -rf ~/4w-deploy-repo/4w/config && mkdir ~/4w-deploy-repo/4w/config 2>&1");
		passthru("cp -rf ~/public_html/".$folderName."/config/* ~/4w-deploy-repo/4w/config/ 2>&1");
		usleep(500000);
		if (!file_exists(__DIR__."/4w/config")) {
			die("Could not copy the directory 'config' from:\n~/public_html/".$folderName."/config"."\nto:\n"."~/4w-deploy-repo/4w/config");
		}

		echo "\tCopying ~/public_html/".$folderName."/storage to ~/4w-deploy-repo/4w/storage\n";
		passthru("rm -rf ~/4w-deploy-repo/4w/storage && mkdir ~/4w-deploy-repo/4w/storage 2>&1");
		passthru("cp -rf ~/public_html/".$folderName."/storage/* ~/4w-deploy-repo/4w/storage/ 2>&1");
		passthru("chmod 777 ~/4w-deploy-repo/4w/public/documentos 2>&1");
		if (!file_exists(__DIR__."/4w/storage")) {
			die("Could not copy the directory 'storage' from:\n~/public_html/".$folderName."/storage"."\nto:\n"."~/4w-deploy-repo/4w/storage/");
		}

		echo "\tCreating framework folders\n";
		passthru("mkdir -p ~/4w-deploy-repo/4w/storage/framework/sessions && mkdir -p ~/4w-deploy-repo/4w/storage/framework/views && mkdir -p ~/4w-deploy-repo/4w/storage/framework/cache");

		echo "\tCopying .env file from ".$folderName."\n";
		passthru("cp -rf ~/public_html/".$folderName."/.env ./4w/.env 2>&1");

		echo "\tCreating required folders\n";
		passthru("mkdir -p ~/4w-deploy-repo/4w/bootstrap/cache && touch ~/4w-deploy-repo/4w/bootstrap/cache/services.php 2>&1");

		$reloadComposer = serverShouldUpdateComposer($folderName);

		if (is_string($reloadComposer) && substr($reloadComposer, 0, 5) === "Error") {
			die($reloadComposer."\n");
		}

		if ($reloadComposer) {
			echo "\tRunning composer install from scratch (reason: ".(is_string($reloadComposer) ? $reloadComposer : "unknown").")\n";
			passthru("cd ~/4w-deploy-repo/4w && rm -rf ~/4w-deploy-repo/4w/vendor && composer install --optimize-autoloader --prefer-dist --no-dev --profile 2>&1");
		}

		usleep(100000);
		clearstatcache();

		if (!file_exists(__DIR__."/4w/vendor") || !file_exists(__DIR__."/4w/vendor/autoload.php")) {
			echo "\tCopying composer vendor from folder".($reloadComposer ? " because composer failed" : "")."\n";
			passthru("rm -rf ./4w/vendor && cp -rf ~/public_html/".$folderName."/vendor ./4w/vendor 2>&1");
		}

		if (!file_exists(__DIR__."/4w/vendor") || !file_exists(__DIR__."/4w/vendor/autoload.php")) {
			echo "Error: The autoload file (from composer) is required, but doesn't exist!\n\n";
			exit(1);
		}

		// Remove services folder from cache because it was causing problems
		if (file_exists(__DIR__."/4w/bootstrap/cache/services.php") && filesize(__DIR__."/4w/bootstrap/cache/services.php") <= 2) {
			unlink(__DIR__."/4w/bootstrap/cache/services.php");
		}

		echo "\tSetting files and folders permissions\n";
		passthru("find ~/4w-deploy-repo/4w/ -type d -exec chmod 755 {} \;");
		passthru("find ~/4w-deploy-repo/4w/ -type f -exec chmod 644 {} \;");

		echo "\tSetting full-acess folder permissions\n";
		$fullAccessFolderList = [
			"/4w/bootstrap",
			"/4w/bootstrap/cache",
			"/4w/storage",
			"/4w/storage/cache",
			"/4w/storage/logs",
			"/4w/storage/sessions"
		];
		$fullAccessCount = 0;
		foreach ($fullAccessFolderList as $fullAccessFolder) {
			if (!file_exists(__DIR__.".".$fullAccessFolder)) {
				passthru("mkdir -p ~/4w-deploy-repo".$fullAccessFolder." 2>&1");
			}
			passthru("chmod --verbose 777 ~/4w-deploy-repo".$fullAccessFolder." 2>&1");
			//echo "Updated '~/4w-deploy-repo".$fullAccessFolder."'\n";
			$fullAccessCount += 1;
		}
		if ($fullAccessCount === 0) {
			die("Could not set folder permissions");
		}

		echo "\tOptimizing artisan and clearing caches\n";
		passthru("cd ~/4w-deploy-repo/4w && php artisan optimize && php artisan config:clear && php artisan cache:clear 2>&1");

		echo "\tCreating git repo to track future changes\n";
		file_put_contents(__DIR__."/4w/this-deploy-date.txt", $deployDate."\n");
		file_put_contents(__DIR__."/4w/.gitignore", "vendor/\nnode_modules\nbootstrap/compiled.php\napp/storage/");
		passthru("cd 4w && git init && git add . && git -c \"user.name=Deployer\" -c \"user.email=atendimento@fastcompras.com.br\" commit --quiet -m \"Deploy at ".$deployDate."\" 2>&1");

		if (file_exists(__DIR__."/error_log")) {
			echo "\n\tAn error log at was found and should be treated before trying to deploy.\n";
			echo "file: ".__DIR__."/error_log\n";
			echo "content: ".file_get_contents(__DIR__."/error_log");
			exit(1);
		}
		if (file_exists(__DIR__."/4w/error_log")) {
			echo "\n\tAn error log at was found and should be treated before trying to deploy.\n";
			echo "file: ".__DIR__."/4w/error_log\n";
			echo "content: ".file_get_contents(__DIR__."/4w/error_log");
			exit(1);
		}
		echo "\n\tFinished server script. Ready to move directories\n\n";
		passthru("mkdir -p ~/4w-deploy-repo/deploy-success-indicator");
		exit(0);
	}
}

function get_user_input($message, $default = "") {
	echo $message;
	echo $default?" [".$default."]\n":"\n";

	$handle = fopen("php://stdin","r");
	$line = fgets($handle);

	return trim($line) ? trim($line) : $default;
}

function is_windows() {
	return (strcasecmp(substr(PHP_OS, 0, 3), 'WIN') == 0);
}

if (!function_exists("ssh2_connect")) {
	echo ("Error: SSH2 extension not enabled\n");
	if(is_windows()) {
		echo "Edit the corresponding line at \"php.ini\" to enable the ssh extension dll\n";
	} else {
		echo "Use \"apt-get install libssh2-1 php-ssh2 -y\" to enable it\n";
	}
	exit(1);
}

if (!config("SERVER_HOST")) {
	config("SERVER_HOST", get_user_input("Server host url?", "netserver3.galafassi.com.br"));
}

if (config("SERVER_PORT") === null) {
	config("SERVER_PORT", get_user_input("Server port?", "8854"));
}

if (!config("SERVER_USERNAME")) {
	config("SERVER_USERNAME", get_user_input("Username for connection?", "root"));
}

if (!config("PRIVATE_KEY_PATH")) {
	config("PRIVATE_KEY_PATH", get_user_input("Path for the private key (id_rsa)?", (is_windows()?str_replace("\\", "/", getenv("USERPROFILE"))."/":"~/").".ssh/id_rsa"));
}

if (!file_exists(config("PRIVATE_KEY_PATH"))) {
	die("The private key file doest not exists at \"".config("PRIVATE_KEY_PATH")."\"");
}

if (!config("PUBLIC_KEY_PATH")) {
	config("PUBLIC_KEY_PATH", get_user_input("Path for the public key (id_rsa.pub)?", (is_windows()?str_replace("\\", "/", getenv("USERPROFILE"))."/":"~/").".ssh/id_rsa.pub"));
}

if (!file_exists(config("PUBLIC_KEY_PATH"))) {
	die("The private key file doest not exists at \"".config("PUBLIC_KEY_PATH")."\"");
}

if (!config("SHOW_ALL_SSH_COMMANDS")) {
	config("SHOW_ALL_SSH_COMMANDS", "0");
}


function my_ssh_disconnect() {
	echo "ssh disconnected unexpectedly.\n";
	die();
}

$ssh = ssh2_connect(config("SERVER_HOST"), config("SERVER_PORT")?config("SERVER_PORT"):21, ["ssh-rsa,ssh-dss"], ["disconnect" => "my_ssh_disconnect"]);

$fingerprint = ssh2_fingerprint($ssh, SSH2_FINGERPRINT_MD5 | SSH2_FINGERPRINT_HEX);

if (!config("SERVER_FINGERPRINT")) {
	config("SERVER_FINGERPRINT", $fingerprint);
} else if (config("SERVER_FINGERPRINT") !== $fingerprint) {
	echo " -- ATTENTION -- \n\tServer fingerprint is: ".$fingerprint."\n\tExpected fingerprint : ".config("SERVER_FINGERPRINT")."\n\n";
	echo "Check with the server provider if the fingerprint changed!!\n\n";
	$r = get_user_input("Trust new fingerprint? ", "y/n");
	if ($r !== "y" && $r !== "Y") {
		die("User did not consent - Deploy aborted\n");
	}
	config("SERVER_FINGERPRINT", $fingerprint);
}

$auth = @ssh2_auth_pubkey_file($ssh, config("SERVER_USERNAME"), config("PUBLIC_KEY_PATH"), config("PRIVATE_KEY_PATH"));

if (!$auth) {
	echo ("Auth Failed:\n");
	printf(
		"\tHost: %s\n\tUser: %s\n\tPublic key path: %s\n\tPrivate key path: %s\n",
		config("SERVER_HOST").(config("SERVER_PORT")?":".config("SERVER_PORT"):""),
		config("SERVER_USERNAME"),
		config("PUBLIC_KEY_PATH"),
		config("PRIVATE_KEY_PATH")
	);
	@ssh2_exec($ssh, "exit");
	unset($ssh);
	exit(1);
}

function send_to_ssh($cmd, $blocking = true, $passthru = false) {
	global $ssh;
	if (config('SHOW_ALL_SSH_COMMANDS')) {
		echo " > $cmd\n";
	}
	if (!($stream = ssh2_exec($ssh, $cmd))) {
		printf("Error: Failed to send the ssh command to server: '%s'\n", $cmd);
		if ($cmd != "exit") {
			send_to_ssh($ssh, "exit");
		}
		exit(1);
	}
	if ($blocking && $cmd !== "exit") {
		stream_set_blocking($stream, true);
		$data = "";
		while ($buf = fread($stream, $passthru ? 4 : 32)) {
			if ($passthru) {
				echo $buf;
			}
			$data .= $buf;
		}
		if ($passthru) {
			echo "\n";
		}
		fclose($stream);
		return $data;
	} else {
		return "";
	}
}

$deployDate = (new DateTime(null, new DateTimeZone('America/Sao_Paulo')))->format('Y/m/d H:i:s');
echo "Deploy script started at ".$deployDate." \n";

$startCommand = "rm -rf 4w-deploy-repo && ssh-agent bash -c 'ssh-add .ssh/deploy_key; git clone --single-branch --branch master --depth=1 git@bitbucket.org:fastcompras/4w.git 4w-deploy-repo' && cd 4w-deploy-repo && php deploy.php production-server-script ".$folderName;

send_to_ssh($startCommand, true, true);

$status = send_to_ssh("test -e ~/4w-deploy-repo/deploy-success-indicator && echo success || echo failed");

if (trim($status) !== "success") {
    echo "Deploy script halted due to missing success indicator (".trim($status).").\n";
    echo "Incomplete deploy can be accessed at \"~/4w-deploy-repo\"\n";
    echo "\n\t > ls -las ~/4w-deploy-repo/\n";
	send_to_ssh("ls -las ~/4w-deploy-repo/", true, true);
	echo "\n\t > ls -las ~/4w-deploy-repo/4w/\n";
    send_to_ssh("ls -las ~/4w-deploy-repo/4w/", true, true);
    echo "\n";
    exit(1);
}

echo "Activating new version\n";
send_to_ssh("rm -rf public_html/".$folderName."-old && mv public_html/".$folderName." public_html/".$folderName."-old && mv 4w-deploy-repo/4w public_html/".$folderName, true, true);

echo "Deploy Finished\n";
send_to_ssh("exit");

exit(0);
