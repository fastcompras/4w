# Projeto da 4W

Feito no framework Laravel e desenvolvido primeiramente por Lorenzo e passado à Guilherme Rossato em 2019.

Migrado para o repositório da Fastcompras para efetuar demandas.

## Laravel

Laravel é um framework cheio de opiniões, alterações no banco de dados devem utilizar o sistema de ORM, etc.

Procure pesquisar como resolver um problema específico no Laravel, e não no PHP. Por exemplo, para adicionar uma coluna ao banco de dados pesquise "Como adicionar uma coluna no Laravel".

## Setup para Desenvolvimento

1. Baixe o repositório

2. Crie um novo banco de dados para ele localmente (recomendo o nome `wcom4_4work`, que é igual ao que está na configuração)

```
CREATE DATABASE wcom4_4work
```

3. Edite o arquivo de configuração de conexão do banco de dados `./config/database.php`, linhas 57-61 com as credenciais do seu banco de dados

4. Abra o terminal na pasta raiz do projeto e execute o comando de iniciar o banco de dados do laravel:

```
php artisan migrate
```

Isso vai criar as tabelas do banco de dados para você

5. Inicie o servidor do Laravel e desenvolva localmente:

```
php artisan serve --port=8080
```

## Deploy

Um sistema de deploy foi desenvolvido por Guilherme Rossato, ele funciona da seguinte forma:

1. Carrega as configurações de um arquivo local na raiz do projeto chamado `.env` (parecido com o `.env-example`)
2. Utiliza a chave privada e publica para acessar o servidor especificado na porta especificada.
3. Pede para que o servidor baixe a aplicação do github (git clone) utilizando uma chave previamente salva no servidor.
4. Copia todos os arquivos de configurações do servidor em `~/public_html/4w/config` para a pasta de deploy.
5. Copia os arquivos .env com as variaveis de ambiente do servidor em `~/public_html/4w/.env` para a pasta de deploy.
6. Move a pasta em produção para `~/public_html/4w-old` e move a pasta de deploy para o lugar da pasta em produção.

Para utilizar a script, é necessário ter a extensão PHP SSH2 instalada (se não estiver, o próprio código te informará disso).

Na raiz do projeto (fora da pasta `4w`), execute:

```
php deploy.php
```

A pasta (descrita pela variavel 'SERVER_FOLDER_NAME') do servidor será substituida com a versão git atual.

Para o ambiente de [homologação](https://dev.sou4w.com.br/), utilize a variavel SERVER_FOLDER_NAME em "4w-homolog" e para [produção](https://app.sou4w.com.br/) utilize "4w".
