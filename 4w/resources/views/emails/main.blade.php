<head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="{{asset('css/lib/email/basic.css')}}" media="all" rel="stylesheet" type="text/css">
</head>
<body>
<table class="body-wrap">
    <tbody>
    <tr>
        <td></td>
        <td class="container" width="600">
            <div class="content" style="width: 800px;">
                <table class="main" width="100%" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td class="content-wrap">
                            <table cellpadding="0" cellspacing="0">
                                <tbody>
                                <tr>
                                    <td class="logo alert alert-good">
                                        <img src="{{asset('img/logo.png')}}" style="float:left; height: 34px; width: 34px;">
                                        <span style="font-size: 18px;"> Sou 4W </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        <h3>@yield('title')</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        @yield('content')
                                    </td>
                                </tr>
                                <tr>
                                    <td class="content-block">
                                        Equipe 4W.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="footer">
                    <table width="100%">
                        <tbody>
                        <tr>
                            <td class="aligncenter content-block">
                                Curta nossa página no <a href="fb.me/sou4w">Facebook</a>.
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td></td>
    </tr>
    </tbody></table>
</body>