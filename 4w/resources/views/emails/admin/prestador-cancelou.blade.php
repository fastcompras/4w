@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        O profissional {{$prestador['nome']}} cancelou o serviço que lhe estava atribuído. <br/>
        Precisamos encontrar outro profissional para atender o nosso usuário.
    </span>
@endsection