@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        O profissional {{$prestador['nome']}} rejeitou a oferta do serviço que oferecemos. <br/>
    </span>
@endsection