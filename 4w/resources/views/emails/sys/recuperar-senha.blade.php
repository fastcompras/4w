@extends('emails.main')

@section('title', 'Redefinição de Senha')

@section('content')
    <span>
        Olá, {{ $user['nome'] }}. <br/><br/>
        Se você solicitou uma redefinição de senha na 4W, clique no botão abaixo.
        <br>
        Caso não tenha realizado esta solicitação, ignore este e-mail.
        <br>
    </span>
    <br><br>
    <div class="content-block">
        <a href="{{ url('/registrar/redefinir-senha?token=' . $token) }}" class="btn btn-success">
            Redefinir Senha
        </a>
    </div>
@endsection