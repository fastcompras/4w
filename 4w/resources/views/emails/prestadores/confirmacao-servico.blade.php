@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$prestador['nome']}}. <br/><br/>
        <span>Código da Oportunidade: <strong>{{$oportunidade['id']}}</strong></span><br/>
        <span>Data e Horário: <strong>{{$oportunidade['data_hora']}}</strong></span><br/>
        <span>Bairro: <strong> {{ $oportunidade['bairro']}} </strong></span><br/>
        <span>Cidade: <strong> {{ $oportunidade['cidade']['nome'] . '/' . $oportunidade['cidade']['estado']['uf'] }} </strong></span><br/>
        <span>Cliente: <strong> {{ $oportunidade['usuario']['nome']}} </strong></span><br/>
        <span>Telefone do Cliente: <strong> {{ $oportunidade['usuario_telefone']['numero'] }} </strong></span><br/><br/>
        Acesse o portal pelo link abaixo e verifique os dados do serviço que você aceitou.
        <br/>
        <a href="{{url('/oportunidades/visualizar/'.$oportunidade['id'])}}">Clique aqui</a> para visualizar a descrição e local de serviço.
        <br/><br/>
        Caso ocorra algum imprevisto, comunique o cancelamento com até 48 horas de antecedência. A não comunicação acarretará na suspensão da sua conta por tempo indeterminado.
    </span>
@endsection