@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$prestador['nome']}}. <br/><br/>
        <span>Código da Oportunidade: <strong>{{$oportunidade['id']}}</strong></span><br/>
        <span>Data e Horário: <strong>{{$oportunidade['data_hora']}}</strong></span><br/>
        <span>Bairro: <strong> {{ $oportunidade['bairro']}} </strong></span><br/>
        <span>Cidade: <strong> {{ $oportunidade['cidade']['nome'] . '/' . $oportunidade['cidade']['estado']['uf'] }} </strong></span><br/><br/>
        Acorda que o dinheiro não dorme!  Ficaremos no aguardo de sua confirmação para que possamos dar continuidade neste processo.
        <br/>
        Esta oportunidade também foi reportada para outros profissionais da plataforma 4W e será direcionada para o primeiro que confirmar esta oportunidade clicando <a href="{{url('/oportunidades/visualizar/'.$oportunidade['id'])}}">aqui</a> ou no botão abaixo.
    </span>
    <br/><br/>
    <div class="content-block">
        <a href="{{url('/oportunidades/visualizar/'.$oportunidade['id'])}}" class="btn btn-success">Visualizar e confirmar a oferta</a>
    </div>
@endsection