@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$prestador['nome']}}. <br/><br/>
        Recebemos sua solicitação de cadastro e estamos analisando com muito carinho.<br/>
        <br/>
        Fique tranquilo que seus dados estão seguros conosco. Não compartilhamos dados cadastrais.<br/>
        <br/>
        Em breve daremos retorno.
    </span>
@endsection