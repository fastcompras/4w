@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$prestador['nome']}}. <br/><br/>
        Acesse o portal pelo link abaixo e envie os documentos que estão pendentes para conclusão do seu cadastro.<br/><br/>
        <a href="{{url('/conta/documentosPendentes')}}">Clique aqui</a> para enviar seus documentos pendentes.<br/><br/>
    </span>
@endsection