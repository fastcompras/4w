@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$prestador['nome']}}. <br/><br/>
        De acordo com a nossa análise do seu perfil, que é baseada em dados de mercado, infelizmente não podemos liberar um convite pra você nesse momento.
        <br/>
        Somos uma empresa muito nova e ainda estamos calibrando nossos modelos de análise e testando o comportamento de uso e pagamento de clientes com diversos perfis.
        <br/>
        Entendemos a sua frustração e gostaríamos de pedir desculpas.
    </span>
@endsection