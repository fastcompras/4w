@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$prestador['nome']}}. <br/><br/>
        O serviço <strong>{{ $oportunidade['id'] }}</strong> está cancelado. <br/>
        Agradecemos por ter aceitado este serviço. Estamos em busca de uma nova oportunidade para você.
        <br/><br/>
        Caso tenha alguma dúvida, sugestão ou reclamação, envie um e-mail para <strong>atendimento@4w.com.br</strong>.
    </span>
@endsection