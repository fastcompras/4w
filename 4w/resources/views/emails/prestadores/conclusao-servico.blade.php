@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$prestador['nome']}}. <br/><br/>
        O serviço <strong>{{$oportunidade['id']}}</strong> está concluído e o valor <strong>R$ {{number_format($oportunidade['valor_total'] - $oportunidade['valor_4w'], 2, ',', '.') }}</strong> será transferido para sua conta no <strong>Mercado Pago</strong>.
        <br/><br/>
        Nos ajude dando a sua avaliação deste cliente. Esta avaliação é muito importante para futuras contratações.
        <br>
        <a href="{{url('/oportunidades/visualizar/'.$oportunidade['id'])}}">Clique aqui</a> para avaliar o cliente<br/><br/>
        Caso tenha alguma dúvida, sugestão ou reclamação, envie um e-mail para <strong>atendimento@4w.com.br</strong>.
    </span>
@endsection