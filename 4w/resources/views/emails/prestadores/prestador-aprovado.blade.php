@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
       Olá, {{$prestador['nome']}}. <br/><br/>
        Vamos colocar o seu talento para trabalhar. Agora você está pronto para receber as ofertas de negócio do nosso portal.
        <br/>
        Fique atento ao seu e-mail que você receberá comunicação de oportunidades.
        <br/>
        Parabéns!
    </span>
@endsection