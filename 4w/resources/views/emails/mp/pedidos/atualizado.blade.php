@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá <strong>Administrador</strong>!<br/><br/>
        O pedido <strong>{{ $oportunidade['id'] }}</strong> foi recebido pelo Mercado Pago <br/>
        <span>Código da Oportunidade: <strong>{{$oportunidade['id']}}</strong></span><br/>
        <span>Data e Hora: <strong>{{$oportunidade['data_hora']}}</strong></span><br/>
        <span>Cliente: <strong> {{ $oportunidade['usuario']['nome']}} </strong></span><br/>
        <span>Telefone do Cliente: <strong> {{ $oportunidade['usuario_telefone']['numero'] }} </strong></span>
        <br/><br/>
        <span>Novo Status: <strong>{{$oportunidade['status_traduzido']}}</strong></span><br/>
        <span>Valor do Serviço: <strong>R$ {{number_format($oportunidade['valor_total'], 2, ',', '.') }}</strong></span><br/>
        <br/><br/>
        Você pode visualizar seu extrato no Mercado Pago clicando neste <a href="https://www.mercadopago.com.br/activities">link</a>.
    </span>
@endsection