@extends('emails.main')

@section('title', $title)

@section('content')
    <span> Olá, <strong>Administrador</strong>!</span><br><br>
    <span> Houve um erro ao tentar atualizar o pedido recebido do Mercado Pago.</span> <br><br>
    <span> O ID da oportunidade no sistema é <strong>#{{ $oportunidade['id'] }}.</strong></span> <br>
    <span> O ID do pedido no Mercado Pago é <strong>#{{ $oportunidade['id_order_mp'] }}.</strong></span> <br>
    <span> O código resultante do erro foi <strong>{{ $status }}.</strong></span>
@endsection