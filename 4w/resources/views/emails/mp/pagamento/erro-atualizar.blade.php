@extends('emails.main')

@section('title', $title)

@section('content')
    <span> Olá, <strong>Administrador</strong>!</span><br><br>
    <span> Houve um erro ao tentar atualizar o pagamento recebido do Mercado Pago.</span> <br><br>
    <span> O ID da oportunidade no sistema é <strong>#{{ $oportunidade['id'] }}.</strong></span> <br>
    <span> O ID do pagamento no Mercado Pago é <strong>#{{ $oportunidade['id_pedido_mp'] }}.</strong></span> <br>
    <span> O código resultante do erro foi <strong>{{ $status }}.</strong></span>
@endsection