@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá Administrador. <br/><br/>
        O status do pagamento referente ao serviço <strong>{{ $oportunidade['id'] }}</strong> foi atualizado. <br/>
        <span>Código da Oportunidade: <strong>{{$oportunidade['id']}}</strong></span><br/>
        <span>Data e Horário: <strong>{{$oportunidade['data_hora']}}</strong></span><br/>
        <span>Cliente: <strong> {{ $oportunidade['usuario']['nome']}} </strong></span><br/>
        <span>Telefone do Cliente: <strong> {{ $oportunidade['usuario_telefone']['numero'] }} </strong></span>
        <br/><br/>
        <span>Novo Status: <strong>{{$oportunidade['status_traduzido']}}</strong></span><br/>
        <span>Valor do Serviço: <strong>R$ {{number_format($oportunidade['valor_total'], 2, ',', '.') }}</strong></span><br/>
        <span>Forma de Pagamento: <strong>{{$oportunidade['tipo_pagamento']}}</strong></span>
        <br/><br/>
        Você pode visualizar seu extrato no Mercado Pago clicando neste <a href="https://www.mercadopago.com.br/activities">link</a>.
    </span>
@endsection