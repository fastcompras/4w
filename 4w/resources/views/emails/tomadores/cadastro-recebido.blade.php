@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$tomador['nome']}}. <br/><br/>
        Ficamos muito felizes com o seu cadastro na <strong>4W</strong>. <br/><br/>
        Temos diversos profissionais prontos para te atender. Diaristas, baby sitters, cuidadores, manutenção residencial, instrutores, entre muitos outros.
        <br/><br/>
        Acesse o portal pelo link abaixo e solicite um serviço.
    </span>
    <br/><br/>
    <div class="content-block">
        <a href="{{url('/login')}}" class="btn btn-success">Clique aqui e acesse a 4W</a>
    </div>
@endsection