@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$tomador['nome']}}. <br/><br/>
        Facilite seu dia! Acesse agora a <strong>4W</strong> e encontre o serviço que irá resolver o seu problema.
        <br/><br/>
        Temos diversos profissionais prontos para te atender. Diaristas, baby sitters, cuidadores, manutenção residencial, instrutores, entre muitos outros.
        <br/><br/>
        Acesse o portal pelo link abaixo e solicite um serviço.
    </span>
    <br/><br/>
    <div class="content-block">
        <a href="{{url('/login')}}" class="btn btn-success">Clique aqui e acesse a 4W</a>
    </div>
@endsection