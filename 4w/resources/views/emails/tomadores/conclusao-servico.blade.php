@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$tomador['nome']}}. <br/><br/>
        Acesse o portal pelo link abaixo e avalie o serviço recebido. <br>
        <a href="{{url('/oportunidades/visualizar/'.$oportunidade['id'])}}">Clique aqui</a> para avaliar o serviço<br/><br/>
        Continue utilizando a 4W. Temos diversos serviços disponíveis para você utilizar.
        <br>
        O que você precisa agora?
    </span>
@endsection