@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$tomador['nome']}}. <br/><br/>
        <span>Código da Oportunidade: <strong>{{$oportunidade['id']}}</strong></span><br/>
        <span>Data e Horário: <strong>{{$oportunidade['data_hora']}}</strong></span><br/>
        <span>Nome do Prestador: <strong> {{ $oportunidade['prestador']['nome']}} </strong></span><br/>
        <span>Telefone do Prestador: <strong> {{ $oportunidade['prestador_telefone']['numero'] }} </strong></span><br/><br/>
        Acesse o portal pelo link abaixo para visualizar o perfil do prestador que irá lhe atender.
        <br/>
    </span>
    <br/><br/>
    <div class="content-block">
        <a href="{{url('/conta/perfil/'.$oportunidade['prestador']['id'])}}" class="btn btn-success">Visualizar perfil do prestador</a>
    </div>
@endsection