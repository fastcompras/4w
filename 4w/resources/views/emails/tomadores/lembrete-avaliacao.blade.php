@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$tomador['nome']}}. <br/><br/>
        A sua avaliação é muito importante para qualificar os prestadores da 4W. É assim que garantimos a melhor indicação no seu próximo serviço. <br/><br/>
        Acesse o portal pelo link abaixo e avalie o serviço prestado.<br/>
    </span>
    <br/>
    <a href="{{url('/oportunidades/visualizar/'.$oportunidade['id'])}}">Clique aqui</a> para finalizar e avaliar o serviço.<br/><br/>
    <span>
        Continue utilizando a 4W. Temos diversos serviços disponíveis para você utilizar. O que você precisa agora?
    </span>
@endsection