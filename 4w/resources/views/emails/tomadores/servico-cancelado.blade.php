@extends('emails.main')

@section('title', $title)

@section('content')
    <span>
        Olá, {{$tomador['nome']}}. <br/><br/>
        O serviço <strong>{{ $oportunidade['id'] }}</strong> está cancelado. Foi estornado o valor de <strong>R$ {{number_format($oportunidade['valor_total'], 2, ',', '.') }}</strong> para sua conta no Mercado Pago. Você poderá utilizar este crédito para solicitar um novo serviço na 4W ou resgatar.
        <br/><br/>
        Caso tenha alguma dúvida, sugestão ou reclamação, envie um e-mail para <strong>atendimento@4w.com.br</strong>.
    </span>
@endsection