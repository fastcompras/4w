@extends('layouts.app')

@section('title', 'Sou 4W - Novo Perfil de Conta')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-user"></i>
                Perfil de Conta
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('perfil.create')}}">Novo Perfil de Conta</a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="novo-perfil-conta" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-6">
                                <input type="text" id="descricao" name="descricao" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right">
                                <a href="{{route('home')}}" class="btn btn-white">Voltar</a>
                                <button class="salvar-perfil btn btn-success"> <i class="fa fa-floppy-o"></i> Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var base_path = "{{url('/')}}";
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


            $(document).on("click", ".cancelar", function (e) {
                e.preventDefault();

            });

            $(document).on("click", ".salvar-perfil", function (e) {
                e.preventDefault();

                var action = base_path+'/cadastros/perfilConta';

                var data = {
                    descricao: $('#descricao').val(),
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', 'Perfil de conta criado com sucesso!');
                        } else {
                            mensagem('error', 'Erro ao criar perfil de conta!');
                        }

                        $('#descricao').val('');
                    }
                });
            });
        });
    </script>
@endsection