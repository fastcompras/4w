@extends('layouts.app')

@section('title', 'Sou 4W - Editar Motivo de Rejeição')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-trash"></i>
                Cadastro de Motivos de Rejeição
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('motivosrejeicao.update', ['id' => $motivo->id])}}">Atualizar Motivo de Rejeição</a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="editar-motivo" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" id="id" value="{{ $motivo->id }}" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Motivo</label>
                            <div class="col-sm-6">
                                <input type="text" id="motivo" name="motivo" value="{{$motivo->descricao}}" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right">
                                <a href="{{route('motivosrejeicao.show')}}" class="btn btn-white">Voltar</a>
                                <button class="salvar btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".salvar", function (e) {
                e.preventDefault();
                var motivo = $('#motivo').val();

                if (!motivo) {
                    mensagem('error', 'Informe o motivo de rejeição!');
                    return false;
                }

                var data = {
                    id: $('#id').val(),
                    descricao: motivo,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'PUT',
                    dataType: 'json',
                    url: base_path + '/motivos-rejeicao/atualizar',
                    cache: false,
                    success: function(data) {
                        mensagem('success', 'Motivo de rejeição atualizado com sucesso!');
                        setTimeout(function(){ window.location.href = base_path + '/motivos-rejeicao/'; }, 700);
                    },
                    error: function (response) {
                        mensagem('error', 'Erro ao atualizar o motivo! Por favor, tente novamente!');
                    }
                });
            });
        });
    </script>
@endsection