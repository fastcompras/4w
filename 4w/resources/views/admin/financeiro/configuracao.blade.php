@extends('layouts.app')

@section('title', 'Sou 4W - Configuração')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<?php
    $percentual = ($configuracao) ? $configuracao->percentual_sobre_servico : 0;
?>

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-bar-chart"></i>
                Configuração Financeira
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('financeiro.create')}}">Configuração Financeira</a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="configuracao" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nosso Percentual</label>
                            <div class="col-sm-6">
                                <input type="number" min="0" max="100" id="percentual" value="{{$percentual}}" placeholder="Percentual sobre o serviço. Ex: 12" name="percentual" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right">
                                <a href="{{route('home')}}" class="btn btn-white">Voltar</a>
                                <button class="salvar btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".salvar", function (e) {
                e.preventDefault();
                var percentual = $('#percentual').val();

                if (percentual == 0) {
                    mensagem('error', 'O valor percentual deve ser maior que 0!');
                    return false;
                }

                if (!percentual) {
                    mensagem('error', 'Você deve preencher o valor percentual sobre cada serviço!');
                    return false;
                }

                var data = {
                    percentual: percentual,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/financeiro/configuracao/salvar',
                    cache: false,
                    success: function(data) {
                        mensagem('success', 'Configuração salva com sucesso!');

                        setTimeout(function(){ window.location.href = base_path; }, 700);
                    },
                    error: function (data) {
                        mensagem('error', 'Erro ao salvar o percentual do serviço! Por favor, tente novamente!');
                    }
                });
            });
        });
    </script>
@endsection