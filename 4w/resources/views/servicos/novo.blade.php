@extends('layouts.app')

@section('title', 'Sou 4W - Novo Serviço')

@section('htmlheader')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="{{asset('css/kothing-editor/index.css')}}" media="all" rel="stylesheet" type="text/css">
<script src="{{asset('js/kothing-editor/index.js')}}"></script>
@endsection

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-gear"></i>
                <?php echo ($servico->id) ? 'Atualizar Serviço' : 'Cadastrar Serviço' ?>
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li>
                    <a href="{{route('servicos.show')}}">Lista de Serviços</a>
                </li>
                <li class="active">
                    <?php if ($servico->id) : ?>
                        <a href="{{route('servicos.update', ['id' => $servico->id])}}">Atualizar Serviço</a>
                    <?php else : ?>
                        <a href="{{route('servicos.create')}}">Novo Serviço</a>
                    <?php endif; ?>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="novo-perfil-conta" class="form-horizontal">
                        <input type="hidden" id="id" value="{{$servico->id}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-6">
                                <input type="text" id="descricao" name="descricao" value="{{ $servico->descricao }}" class="form-control">
                            </div>
                        </div>

                        @if (isset($uploadedImages) && $uploadedImages && false)
                        <div class="form-group">
                            {{ var_export($uploadedImages, true) }}
                        </div>
                        @endif
                        <div class="form-group">
                            <label class="col-sm-2 control-label" style="margin-top: 25px;">Imagem</label>
                            <div class="col-sm-6">
                            @if(isset($uploadedImages) && is_array($uploadedImages) && count($uploadedImages) > 0)
                                <style>
                                    .wordpress-image-selector {
                                        display: flex;
                                        align-items: center;
                                        overflow: hidden;
                                    }
                                    .wordpress-image-selector > div {
                                        width: 120px;
                                        min-width: 120px;
                                        flex-basis: 120px;
                                        height: 80px;
                                        box-sizing: border-box;
                                        background-position: center;
                                        background-size: cover;
                                        background-repeat: no-repeat;
                                        display: flex;
                                        justify-content: flex-end;
                                        border: 1px solid #999;
                                        cursor: pointer;
                                        z-index: 2;
                                    }
                                    .wordpress-image-selector > div + div {
                                        margin-left: 5px;
                                    }
                                    .wordpress-image-selector > div:hover {
                                        border-color: #aaa #666 #555 #aaa;
                                        background-color: #eee;
                                        z-index: 3;
                                    }
                                    .wordpress-image-selector > div > span {
                                        background-color: rgba(240, 240, 240, 0.8);
                                        border-radius: 3px;
                                        color: #444;
                                        font-family: monospace;
                                        align-self: flex-start;
                                        font-size: 11px;
                                        padding: 2px 4px;
                                    }
                                    .wordpress-image-pagination {
                                        display: flex;
                                        justify-content: center;
                                        align-items: center;
                                        margin-top: 8px;
                                    }
                                    .wordpress-image-pagination > a {
                                        margin-right: 6px;
                                    }
                                    .wordpress-image-pagination .pagination {
                                        margin: 0;
                                    }
                                    .wordpress-image-pagination .pagination li.hidden {
                                        display: none;
                                    }
                                    .wordpress-image-pagination .pagination li a {
                                        min-width: 37px;
                                        text-align: center;
                                    }
                                    .wordpress-image-selector .selected, .wordpress-image-selector .selected:hover {
                                        border-color: orange;
                                        border-width: 3px;
                                        cursor: inherit;
                                    }
                                </style>
                                <div class="actions">
                                    <a class="btn btn-white" target="_blank" href="{{ $uploadUrl }}"><i class="fa fa-upload" style="margin-right: 5px;"></i>Upload</a>
                                    <button onclick="onAddWithUrlClick()" role="button" type="button" class="btn btn-white btn-add-with-url"><i class="fa fa-link" style="margin-right: 5px;"></i><span>Adicionar com URL</span></button>
                                    <button onclick="onRemoveImageClick()" role="button" type="button" class="btn btn-white btn-add-with-url"><i class="fa fa-folder-open" style="margin-right: 5px;"></i><span>Limpar / Usar padrão</span></button>
                                </div>
                                <input type="hidden" id="top_image_url" name="top_image_url" value="{{ $servico->top_image_url }}">
                                <div class="wordpress-image-selector">
                                    @if ($servico->top_image_url)
                                        <div data-url="{{ $servico->top_image_url }}" class="selected original"></div>
                                    @endif

                                    <?php foreach ($uploadedImages as $image): ?>
                                        <div data-url="{{ $image['url'] }}"><span>{{ substr($image["period"], 4, 2) }}/{{ substr($image["period"], 0, 4) }}</span></div>
                                    <?php endforeach; ?>
                                </div>
                                <div class="wordpress-image-pagination">
                                    <ul class="pagination"></ul>
                                </div>
                                <script>
                                    function onRemoveImageClick() {
                                        const selEl = document.querySelector(".wordpress-image-selector");
                                        const input = document.querySelector("input#top_image_url");
                                        input.value = "";
                                        if (selEl.querySelector(".selected")) {
                                            selEl.querySelector(".selected").classList.remove("selected");
                                        }
                                    }
                                    function onAddWithUrlClick() {
                                        const selEl = document.querySelector(".wordpress-image-selector");
                                        const pagEl = document.querySelector(".wordpress-image-pagination");
                                        const label = document.querySelector(".btn-add-with-url span");
                                        const input = document.querySelector("input#top_image_url");

                                        if (selEl.classList.contains("hidden")) {
                                            label.innerText = "Adicionar com URL";
                                            selEl.classList.remove("hidden");
                                            pagEl.classList.remove("hidden");
                                            input.classList.remove("form-control");
                                            input.setAttribute("type", "hidden");
                                            const original = selEl.querySelector(".original");
                                            if (original) {
                                                original.setAttribute("data-url", input.value);
                                                original.style.backgroundImage = "url('"+input.value+"')";
                                                if (input.value) {
                                                    original.click();
                                                }
                                            } else if (input.value && selEl.children[0] && selEl.children[0].click) {
                                                selEl.children[0].click();
                                            }
                                        } else {
                                            label.innerText = "Adicionar do Wordpress";
                                            selEl.classList.add("hidden");
                                            pagEl.classList.add("hidden");
                                            input.setAttribute("type", "text");
                                            input.classList.add("form-control");
                                        }
                                    }
                                    function updateShownImages() {
                                        const selEl = document.querySelector(".wordpress-image-selector");
                                        const selElRect = selEl.getBoundingClientRect();
                                        let index = 0;
                                        for(let child of selEl.children) {
                                            const relativePosition = child.getBoundingClientRect().left - selElRect.left;
                                            if (relativePosition > -selElRect.width && relativePosition < selElRect.width * 2) {
                                                if (!child.classList.contains("visible")) {
                                                    child.classList.add("visible");
                                                    child.style.backgroundImage = "url('"+child.getAttribute("data-url")+"')";
                                                }
                                            } else {
                                                if (child.classList.contains("visible")) {
                                                    child.classList.remove("visible");
                                                    child.style.backgroundImage = "none";
                                                }
                                            }
                                            index++;
                                        }
                                    }
                                    function onLiClick() {
                                        const id = parseInt(this.getAttribute("data-id"), 10);
                                        if (isNaN(id) || id < 0 || id > 1000) {
                                            return console.warn("clicked id is invalid:", id);
                                        }
                                        if (!setImagePage(id)) {
                                            window.pageAnimationDesire = this;
                                            return;
                                        }

                                        const selected = document.querySelector(".wordpress-image-pagination li.active");
                                        if (selected && selected !== this) {
                                            selected.classList.remove("active");
                                        }

                                        const visibles = document.querySelectorAll(".wordpress-image-pagination li.visible");
                                        const mids = document.querySelectorAll(".wordpress-image-pagination li.mid");
                                        if (visibles.length <= 1 || mids.length <= 1) {
                                            return console.warn("Could not fint visibles (", visibles,") or mids (", mids, ")");
                                        }

                                        for (let i = 0; i < visibles.length; i++) {
                                            if (!visibles[i]) {
                                                console.warn("Visible index",i,"is strangely invalid");
                                                continue;
                                            }
                                            visibles[i].classList.remove("visible");
                                            visibles[i].classList.add("hidden");
                                        }
                                        const startIndex = Math.max(0, Math.min(id-Math.ceil(visibles.length/2), mids.length-visibles.length));
                                        const lastIndex = startIndex + visibles.length - 1;
                                        for (let i = startIndex; i <= lastIndex; i++) {
                                            if (!mids[i]) {
                                                console.log("Could not find mid at", i);
                                                continue;
                                            }
                                            mids[i].classList.add("visible");
                                            mids[i].classList.remove("hidden");
                                            if (i === id) {
                                                mids[i].classList.add("active");
                                            }
                                        }

                                        const prev = document.querySelector(".wordpress-image-pagination li:nth-child(1)");
                                        const last = document.querySelector(".wordpress-image-pagination li:last-child");
                                        if (!prev || !last) {
                                            return; console.warn("Could not find something", prev, last);
                                        }
                                        prev.setAttribute("data-id", (id - 1).toString());
                                        last.setAttribute("data-id", (id + 1).toString());
                                    }
                                    function setImagePage(id) {
                                        if (window.pageAnimationLoopIndex && window.pageAnimationLoopIndex !== 70) {
                                            return;
                                        }
                                        const selEl = document.querySelector(".wordpress-image-selector");
                                        const imagesPerPage = parseInt(selEl.getAttribute("data-images-per-page"), 10);
                                        if (isNaN(imagesPerPage)) {
                                            return console.warn()
                                        }
                                        const target = selEl.children[Math.floor(id * (imagesPerPage | 0))];
                                        if (!target) {
                                            return console.warn("There is no target at id " + Math.floor(id * (imagesPerPage | 0)));
                                        }
                                        const relativePosition = target.getBoundingClientRect().left - selEl.getBoundingClientRect().left + selEl.scrollLeft;
                                        if (Math.abs(relativePosition - selEl.scrollLeft) < 10) {
                                            return console.log("No scrolling is necessary");
                                        }
                                        window.pageAnimationLoopOrigin = selEl.scrollLeft;
                                        window.pageAnimationLoopTarget = relativePosition;
                                        window.pageAnimationLoopIndex = 0;
                                        pageAnimationLoop();
                                        return true;
                                    }
                                    function pageAnimationLoop() {
                                        if (typeof window.pageAnimationLoopIndex !== "number" || isNaN(window.pageAnimationLoopIndex) || window.pageAnimationLoopIndex < 0) {
                                            return console.warn("page animation loop executed without reason");
                                        }
                                        window.pageAnimationLoopIndex++;
                                        if (window.pageAnimationDesire && window.pageAnimationLoopIndex < 60) {
                                            window.pageAnimationLoopIndex+=4;
                                        }

                                        const t = window.pageAnimationLoopIndex / 70;
                                        const eased = (t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1);
                                        const scroll = window.pageAnimationLoopOrigin + (window.pageAnimationLoopTarget - window.pageAnimationLoopOrigin) * eased;

                                        const selEl = document.querySelector(".wordpress-image-selector");
                                        selEl.scrollTo(scroll, 0);

                                        if (window.pageAnimationLoopIndex >= 70) {
                                            window.requestAnimationFrame(updateShownImages);
                                            if (window.pageAnimationDesire) {
                                                onLiClick.call(window.pageAnimationDesire);
                                                window.pageAnimationDesire = undefined;
                                            }
                                            return;
                                        }
                                        window.requestAnimationFrame(pageAnimationLoop);
                                    }
                                    function onImageClick() {
                                        const selEl = document.querySelector(".wordpress-image-selector");
                                        const selected = selEl.querySelector(".selected");
                                        if (selected) {
                                            selected.classList.remove("selected");
                                        }
                                        this.classList.add("selected");
                                        const input = document.querySelector("input#top_image_url");
                                        input.value = this.getAttribute("data-url");
                                    }
                                    window.addEventListener("load", function() {
                                        const selEl = document.querySelector(".wordpress-image-selector");
                                        for (let image of selEl.children) {
                                            image.onclick = onImageClick;
                                        }
                                        const imageCount = selEl.children.length;
                                        const pagEl = document.querySelector(".wordpress-image-pagination");
                                        const pagNav = pagEl.querySelector(".pagination");
                                        const containerWidth = selEl.getBoundingClientRect().width;
                                        const imagesPerPage = Math.max(1, containerWidth / (selEl.children[0].getBoundingClientRect().width + 5)) | 0;
                                        const pageCount = Math.ceil(imageCount / imagesPerPage);
                                        selEl.setAttribute("data-images-per-page", imagesPerPage.toString());
                                        selEl.setAttribute("data-scroll", "0");
                                        for (let i = -1; i <= pageCount; i++) {
                                            const li = document.createElement("li");
                                            const a = document.createElement("a");
                                            if (i === -1 || i === pageCount) {
                                                a.innerText = i === -1 ? '«' : '»';
                                                li.classList.add("side-page-button");
                                            } else {
                                                li.classList.add("mid");
                                                a.innerText = (i+1).toString();
                                            }
                                            if (i === 0) {
                                                li.classList.add("active");
                                            }
                                            li.onclick = onLiClick;
                                            if (
                                                (i !== pageCount) && (
                                                    (containerWidth < 250 && i > 4) ||
                                                    (containerWidth < 400 && i > 5) ||
                                                    (containerWidth < 500 && i > 6) ||
                                                    (containerWidth < 600 && i > 8) ||
                                                    (i > 9)
                                                )
                                            ) {
                                                li.classList.add("hidden");
                                            } else if (i !== -1 && i !== pageCount) {
                                                li.classList.add("visible");
                                            }
                                            li.setAttribute("data-id", i === pageCount ? 1 : Math.max(0, Math.min((pageCount-1), i)).toString());
                                            li.appendChild(a);
                                            pagNav.appendChild(li);
                                        }
                                        updateShownImages();
                                    });
                                </script>
                            @else
                                <input type="text" id="top_image_url" name="top_image_url" value="{{ $servico->top_image_url }}" class="form-control">
                            @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Link no frontend</label>
                            <div class="col-sm-6" style='display: flex; justify-content: center; align-items: center'>
                                <span style='font-family: monospace; color: #666; font-size: 13px;'>https://www.sou4w.com.br/servicos/</span><input style='font-family: monospace; color: #666; font-size: 13px;' type="text" id="url" name="url" value="{{$servico->url}}" class="form-control untouched">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Texto na parte superior da página de contratação de serviço</label>
                            <div class="col-sm-6">
                            <textarea id="descricao_extensa" name="descricao_extensa" class="form-control" style="resize: vertical;">{{ $servico->descricao_extensa }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Texto abaixo do formulário do serviço</label>
                            <div class="col-sm-6">
                                <textarea id="editor_classic" style="display:none;"></textarea>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right">
                                <a href="{{route('servicos.show')}}" class="btn btn-white">Voltar</a>
                                <button class="salvar-servico btn btn-success"> <i class="fa fa-floppy-o"></i> Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function initEditor() {
            if (window.mainEditor) {
                return;
            }
            try {
                window.mainEditor = window.KEDITOR.create('editor_classic', {
                    display: 'block',
                    width: '100%',
                    height: '400px',
                    popupDisplay: 'full',
                    charCounter: true,
                    buttonList: [
                        ['undo', 'redo'],
                        ['font', 'fontSize', 'formatBlock'],
                        ['bold', 'underline', 'italic', 'strike'],
                        ['removeFormat'],
                        ['fontColor', 'hiliteColor'],
                        ['outdent', 'indent'],
                        ['align', 'horizontalRule', 'list', 'table'],
                        ['link', 'image'],
                        ['showBlocks', 'codeView', 'preview']
                    ]
                });
                <?php if (isset($servico->descricao_completa) && $servico->descricao_completa && strlen($servico->descricao_completa) > 0) : ?>
                    setTimeout(() => window.mainEditor.setContents(<?php echo json_encode($servico->descricao_completa); ?>), 250);
                <?php endif; ?>
            } catch (err) {
                console.error(err);
                mensagem("error", "Editor de texto não carregado corretamente (Kothing Editor)");
            }
        }

        function onTitleChange() {
            const untouchedEl = document.querySelector("input#url.untouched");
            if (untouchedEl) {
                untouchedEl.value = this.value.toLowerCase().replace(/\ /g, '-').replace(/\//g, '').replace(/\-\-+/g, '-');
            }
        }

        function onUrlChange() {
            if (this.value) {
                this.classList.remove("untouched");
            } else {
                this.classList.add("untouched");
            }
            if (this.value.indexOf(" ") !== -1 || this.value.indexOf("/") !== -1) {
                this.classList.add("error");
            } else {
                this.classList.remove("error");
            }
        }

        async function onSaveClick(event) {
            event.preventDefault();

            try {
                const data = {
                    id: document.querySelector("input#id").value,
                    url: document.querySelector("input#url").value,
                    top_image_url: document.querySelector("input#top_image_url").value,
                    descricao: document.querySelector("input#descricao").value,
                    descricao_extensa: document.querySelector("textarea#descricao_extensa").value,
                    descricao_completa: window.mainEditor.getContents(),
                    _token: document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                };

                const formData = new FormData();
                for (let key in data) {
                    if (data.hasOwnProperty(key)) {
                        if (typeof data[key] === 'string' || typeof data[key] === 'number' || typeof data[key] === 'boolean') {
                            formData.append(key, data[key]);
                        } else {
                            throw new Error("Erro: Só é possível enviar dados simples e serializaveis (primitivos) ao endpoint");
                        }
                    }
                }

                const base_path = "{{url('/')}}";
                const url = base_path+'/servicos/salvar';
                const response = await fetch(url, {
                    credentials: "include",
                    method: "post",
                    body: formData
                });

                const json = await response.json();

                if (json.success) {
                    mensagem('success', json.msg);
                    window.location.href = base_path+'/servicos';
                } else {
                    if (!json.msg) {
                        throw new Error("Servidor retornou erro sem mensagem (campo 'msg')");
                    }
                    mensagem('error', json.msg);
                }
            } catch (err) {
                mensagem('error', err.toString());
            }
        }

        window.addEventListener("load", function() {
            document.querySelector(".salvar-servico").addEventListener("click", onSaveClick);

            const descEl = document.body.querySelector("input#descricao");
            descEl.addEventListener("keyup", onTitleChange.bind(descEl));
            descEl.addEventListener("change", onTitleChange.bind(descEl));

            const urlEl = document.body.querySelector("input#url");
            urlEl.addEventListener("keyup", onUrlChange.bind(urlEl));
            urlEl.addEventListener("change", onUrlChange.bind(urlEl));

            initEditor();
        });
    </script>
@endsection
