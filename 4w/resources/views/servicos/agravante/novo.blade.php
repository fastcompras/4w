@extends('layouts.app')

@section('title', 'Sou 4W - Novo Agravante')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
    @media screen and (max-width: 1000px) and (min-width: 480px) {
        #ordem, #valor {
            width: 380px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            height: 25px !important;
            line-height: 1.428571429 !important;
        }
    }

    @media screen and (min-width: 1000px){
        #ordem, #valor {
            padding: 6px 12px !important;
            width: 480px !important;
            font-size: 14px !important;
            height: 25px !important;
            line-height: 1.428571429 !important;
        }
    }
</style>

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-fire-extinguisher "></i>
                Cadastro de Agravantes
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li>
                    <a href="{{route('agravantes.show')}}">Lista de Agravantes</a>
                </li>
                <li class="active">
                    <a href="{{route('agravantes.create')}}">Novo Agravante</a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="novo-servico-agravante" class="form-horizontal">
                        <input type="hidden" id="agravante_id" data-id="">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tipo de Serviço</label>
                            <div class="col-sm-6">
                                {{ Form::select('servicos', $servicos, null, array('id' => 'servicos', 'class' => 'form-control', 'placeholder' => 'Selecione um serviço', 'style' => 'width: 100%;')) }}
                            </div>
                        </div>
                        <div class="servicos-base form-group">
                            <label class="col-sm-2 control-label">Serviço Base</label>
                            <div class="col-sm-6">
                                {{ Form::select('servicosBase', $servicosBase, null, array('id' => 'servicosBase', 'class' => 'form-control', 'placeholder' => 'Selecione um serviço base', 'style' => 'width: 100%;')) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Regra do Preço</label>
                            <div class="col-sm-6">
                                {{ Form::select('regra', $regras, null, array('id' => 'regra', 'class' => 'form-control', 'placeholder' => 'Selecione a regra a ser aplicada', 'style' => 'width: 100%;')) }}
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Apresentação</label>
                            <div class="col-sm-6">
                                {{ Form::select('tipo_campo', $tipos, null, array('id' => 'tipo_campo', 'class' => 'form-control', 'placeholder' => 'Selecione o tipo do campo', 'style' => 'width: 100%;')) }}
                            </div>
                            <div class="col-sm-2">
                                <button title="Adicionar opções" class="add-opcoes btn btn-circle btn-sm btn-primary" style="display:none; margin-left: -10px;">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div class="opcoes-all">
                            <div class="opcao-fixa"></div>
                            <div class="opcoes"></div>
                        </div>
                        <div class="form-group valor-simples" style="display:none">
                            <label class="col-sm-2 control-label">Valor em Percentual (%)</label>
                            <div class="col-sm-6">
                                <input min="1" id="valor" name="valor" style="width:100%" class="only-number form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Rótulo do Formulário</label>
                            <div class="col-sm-6">
                                <input type="text" id="rotulo" name="rotulo" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ordem que aparecerá</label>
                            <div class="col-sm-6">
                                <input style="width: 100%" type="text" min="1" max="20" id="ordem" class="only-number form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-sm-6" style="font-size: 14px; font-weight: bold;">
                                <input type="checkbox" id="incluso"> Incluso no Serviço Base
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right">
                                <a href="{{route('agravantes.show')}}" class="btn btn-white">Voltar</a>
                                <button class="salvar-agravante btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    @include('helpers.spinner')

    <div class="hide opcoes-template">
        <div class="template-opcoes form-group">
            <label class="opcao-label col-sm-2 control-label">Opção</label>
            <div class="col-sm-3">
                <input type="text" value="" class="opcao-descricao form-control">
            </div>
            <label class="col-sm-1 control-label opcao-valor-texto">Valor</label>
            <div class="col-sm-2">
                <input type="text" value="" class="valor-reais opcao-valor form-control">
            </div>
            <div class="col-sm-2">
                <button title="Remover esta opção" class="remover-opcoes btn btn-circle btn-sm btn-danger" style="margin-left: -10px;">
                    <i class="fa fa-times"></i>
                </button>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var opcoes = $('.template-opcoes').clone();
        $('.template-opcoes').remove();

        $(document).ready(function() {
            var base_path = "{{url('/')}}";
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $('#valor').maskMoney();
            $(".only-number").spinner();

            $(document).on("click", ".salvar-agravante", function (e) {
                e.preventDefault();
                var id = $('#agravante_id').val();
                var id_servico = $('#servicos :selected').val();
                var id_servico_base = $('#servicosBase :selected').val();
                var tipo_campo = $('#tipo_campo :selected').val();
                var regra = $('#regra :selected').val();
                var valor = numeral($('#valor').val()).format('0.[00]');
                var rotulo = $('#rotulo').val();
                var ordem = $('#ordem').val();
                var incluso = 0;

                if ($('#incluso').is(':checked')) {
                    incluso = 1;
                }

                if (!id_servico_base) {
                    mensagem('error', 'Você deve selecionar um serviço que possua um serviço base!');
                    return false;
                }

                if (regra.length == 0 || id_servico.length == 0 || rotulo.length == 0 || id_servico_base.length == 0 || ordem.length == 0) {
                    mensagem('error', 'Preencha todos os campos do formulário!');
                    return false;
                }

                //Agravantes do tipo 'Radio'
                if (tipo_campo == 3) {
                    var opcoes = [];

                    $('.opcao-fixa > .template-opcoes').each(function (i, div) {
                        var obj = new Object();
                        obj.descricao = $(div).find('.opcao-descricao').val();
                        obj.valor = numeral($(div).find('.opcao-valor').val()).format('0.[00]');
                        opcoes.push(obj);
                    });

                    $('.opcoes > .template-opcoes').each(function (i, div) {
                        var obj = new Object();
                        obj.descricao = $(div).find('.opcao-descricao').val();
                        obj.valor = numeral($(div).find('.opcao-valor').val()).format('0.[00]');
                        opcoes.push(obj);
                    });
                }

                var data = {
                    id_servico: id_servico,
                    id_servico_base: id_servico_base,
                    ordem: ordem,
                    rotulo: rotulo,
                    opcoes: opcoes,
                    regra: regra,
                    incluso: incluso,
                    tipo_campo: tipo_campo,
                    valor: (tipo_campo != 3) ? valor : 0,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/servicos/preco/agravante/salvar',
                    cache: false,
                    success: function(response) {
                        mensagem('success', 'Agravante criado com sucesso!');
                        clearForm();
                    },
                    error: function (response) {

                        if (response.responseJSON.msg) {
                            mensagem('error', response.responseJSON.msg);
                        } else {
                            mensagem('error', 'Ops! Houve um erro ao salvar este agravante! Por favor, tente novamente!');
                        }

                        return false;
                    }
                });
            });

            $(document).on("change", "#servicos", function (e) {
                var servico = $('#servicos :selected').val();

                var data = {
                    id_servico: servico,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/helper/getServicosBase',
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('#servicosBase').empty();

                            if (!data.servicos.length) {
                                mensagem('error', 'Este serviço não possui um serviço base criado!');
                                return false;
                            }

                            $.each(data.servicos, function(index, obj){
                                $('#servicosBase').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });
            });

            $(document).on("change", "#tipo_campo", function (e) {
                e.preventDefault();

                var value = $(this).val();

                //RadioBox
                if (value == 3) {
                    $('.valor-simples').hide();
                    $('.opcao-fixa').show();
                    $('.add-opcoes').show();
                } else {
                    $('.opcoes').empty();
                    $('.opcao-fixa').hide();
                    $('.valor-simples').show();
                    $('.add-opcoes').hide();
                }

            });

            $(document).on("click", ".remover-opcoes", function (e) {
                e.preventDefault();

                var item = $(this).closest('.template-opcoes');

                $(item).remove();
            });

            $(document).on("click", ".add-opcoes", function (e) {
                e.preventDefault();

                addOpcao();
            });
        });

        function addOpcao() {
            var el = opcoes.clone();


            el.find('.opcao-label').text("Opção");
            el.find('.opcao-valor').maskMoney();

            $('.opcoes').append(el);
        }

        function clearForm() {
            $('#descricao').val('');
            $('#servicos').val('').change();
            $('#rotulo').val('').change();
            $('#ordem').val('').change();
            $('#tipo_valor').val('').change();
            $('#tipo_campo').val('').change();
            $('#regra').val('').change();
            $('#incluso').attr('checked', false);
            $('.servicos-base').hide();
            $('#servicosBase').val('').change();
            $('.opcoes').empty();

            $('#valor').val('');
            $('.valor-simples').hide();
        }
    </script>
@endsection