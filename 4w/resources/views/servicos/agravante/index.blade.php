@extends('layouts.app')

@section('title', 'Sou 4W - Lista de Agravantes')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="ibox-content">
        <div class="row">
            <div class="col-sm-12">
                <h2>
                    <i class="fa fa-fire-extinguisher "></i>
                    Agravantes dos Serviços Base
                </h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="{{route('home')}}">Início</a>
                    </li>
                    <li class="active">
                        <a href="{{route('agravantes.show')}}">Lista de Agravantes</a>
                    </li>
                </ol>
                <div class="hr-line-dashed"></div>
            </div>
        </div>
        <form id="filtro">
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Serviço</label>
                        {{ Form::select('servico', $servicos, Request::input('servico'), ['id' => 'servico', 'class' => 'form-control', 'placeholder' => 'Selecione um serviço', 'style' => 'width: 100%;']) }}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Serviço Base</label>
                        {{ Form::select('servicoBase', [], Request::input('servicoBase'), array('id' => 'servicoBase', 'class' => 'form-control', 'placeholder' => 'Selecione um serviço base', 'style' => 'width: 100%;')) }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label>Descrição</label>
                        {{ Form::input('text', 'descricao', Request::input('descricao'), ['id' => 'descricao', 'class' => 'form-control', 'placeholder' => 'Descrição do serviço']) }}
                    </div>
                </div>
            </div>
            <div class="hr-line-dashed" style="margin-top: 10px; margin-bottom: 10px;"></div>
            <div class="row">
                <div class="pull-right">
                    <a href="{{route('agravantes.create')}}" class="btn btn-white"><i class="fa fa-plus"></i> Novo Agravante </a>
                    <a class="btn btn-danger" href="{{ request()->url() }}"> <i class="fa fa-trash"></i> Limpar Filtro </a>
                    <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Filtrar </button>
                </div>
            </div>
        </form>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content table-responsive">
                    <?php if (count($agravantes) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição</th>
                                <th>Serviço Base</th>
                                <th>Serviço</th>
                                <th>Data de Cadastro</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($agravantes as $agravante): ?>
                                <tr>
                                    <td>{{ $agravante->id }}</td>
                                    <td>{{ $agravante->rotulo_formulario }}</td>
                                    <td>{{ $agravante->servico_base }}</td>
                                    <td>{{ $agravante->servico }}</td>
                                    <td>{{ date('d/m/Y H:i:s', strtotime($agravante->data_cadastro)) }}</td>
                                    <td class="text-right footable-visible footable-last-column">
                                        <a href={{url('/servicos/preco/agravante/atualizar/'.$agravante->id)}}>
                                            <button class="btn-warning btn-circle btn btn-xs">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a>
                                            <button data-id="{{$agravante->id}}" class="remover-agravante btn-danger btn-circle btn btn-xs">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $agravantes->appends(Request::capture()->except('page'))->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Não há agravantes cadastrados</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var base_path = "{{url('/')}}";

        $(document).ready(function() {

            // Atualiza os serviços base
            // de acordo com os parâmetros da URL
            atualizaServicoBase();

            $(document).on("click", ".remover-agravante", function (e) {
                var id = $(this).attr('data-id');
                var action = base_path+'/servicos/preco/agravante/remover/'+id;

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'DELETE',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', 'Agravante removido com sucesso!');

                            setTimeout(function(){ location.reload(); }, 600);
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });

            $(document).on("change", "#servico", function (e) {
                var servico = $('#servico :selected').val();

                var data = {
                    id_servico: servico,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/helper/getServicosBase',
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('#servicoBase').empty();

                            if (!data.servicos.length) {
                                mensagem('error', 'Este serviço não possui um serviço base criado!');
                                return false;
                            }

                            $('#servicoBase').append($("<option></option>").attr("value", 0).text('Selecione um serviço base'));

                            $.each(data.servicos, function(index, obj){
                                $('#servicoBase').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });
            });
        });


        function atualizaServicoBase() {
            var servico = $('#servico :selected').val();

            var data = {
                id_servico: servico,
                _token: CSRF_TOKEN,
            };

            $.ajax({
                data: data,
                type: 'POST',
                dataType: 'json',
                url: base_path + '/helper/getServicosBase',
                cache: false,
                success: function(data) {
                    if (data.success) {
                        $('#servicoBase').empty();

                        if (!data.servicos.length) {
                            mensagem('error', 'Este serviço não possui um serviço base criado!');
                            return false;
                        }

                        $('#servicoBase').append($("<option></option>").attr("value", 0).text('Selecione um serviço base'));

                        $.each(data.servicos, function(index, obj){
                            $('#servicoBase').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                        });
                    }
                }
            });
        }
    </script>
@endsection