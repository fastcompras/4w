@extends('layouts.app')

@section('title', 'Sou 4W - Cadastro de Serviço Base')

<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2> <i class="fa fa-blind"></i> Cadastro de Serviços Base </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li>
                    <a href="{{route('base.show')}}">Lista de Serviços Base</a>
                </li>
                <li class="active">
                    <a href="{{ route('base.create') }}"> Novo Serviço Base </a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="novo-servico-base" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tipo de Serviço (*)</label>
                            <div class="col-sm-6">
                                {{ Form::select('servicos', $servicos, '', [
                                    'id' => 'servicos',
                                    'class' => 'form-control',
                                    'placeholder' => 'Selecione um serviço',
                                    'style' => 'width: 100%;'
                                  ])
                                }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nome (*)</label>
                            <div class="col-sm-6">
                                <input type="text" id="nome" name="nome" value="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Descrição</label>
                            <div class="col-sm-6">
                                <textarea rows="6" id="descricao" name="descricao" class="form-control" style="resize: vertical;"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Valor (*)</label>
                            <div class="col-sm-6">
                                <input type="text" id="valor" name="valor" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Visibilidade no Frontend</label>
                            <div class="col-sm-6">
                                <select id="invisivel_no_frontend" name="invisivel_no_frontend" value="{{ $base->flag_escondido === 1 || $base->flag_escondido === '1' ? '1' : '0' }}" class="form-control">
                                    <option value="0" {{ $base->flag_escondido === 1 || $base->flag_escondido === '1' ? '' : 'selected' }}>Visível</option>
                                    <option value="1" {{ $base->flag_escondido === 1 || $base->flag_escondido === '1' ? 'selected' : '' }}>Invisivel</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="pull-right">
                                <a href="{{route('base.show')}}" class="btn btn-white">Voltar</a>
                                <button class="create btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $('#valor').maskMoney();

            $(document).on("click", ".create", function (e) {
                e.preventDefault();
                var nome       = $('#nome').val();
                var valor      = numeral($('#valor').val()).format('0.[00]');
                var descricao  = $('#descricao').val();
                var id_servico = $('#servicos :selected').val();
                var invisivel_no_frontend = $('#invisivel_no_frontend').val();

                var data = {
                    id_servico: id_servico,
                    valor: valor,
                    nome: nome,
                    descricao: descricao,
                    invisivel_no_frontend: invisivel_no_frontend,
                    _token: CSRF_TOKEN,
                };

                if (! validateForm(data)) {
                    mensagem('error', 'Os campos obrigatórios devem ser preenchidos!');
                    return false;
                }

                $.ajax({
                    data: data,
                    type: "POST",
                    dataType: 'json',
                    url: base_path + '/servicos/preco/base/salvar',
                    success: function(data) {
                        mensagem('success', 'Serviço base criado com sucesso!');

                        setTimeout(function() {
                            window.location.href = '/servicos/preco/base/';
                        }, 500);
                    }
                });
            });
        });

        function validateForm(data) {

            if (data.valor.length == 0) {
                return false;
            }

            if (data.nome.length == 0) {
                return false;
            }

            if (data.id_servico.length == 0) {
                return false;
            }

            return true;
        }

    </script>
@endsection