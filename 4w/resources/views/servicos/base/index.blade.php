@extends('layouts.app')

@section('title', 'Sou 4W - Lista de Serviços Base')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>
                <i class="fa fa-blind"></i>
                Serviços Base
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('base.show')}}">Lista de Serviços Base</a>
                </li>
            </ol>
            <div class="hr-line-dashed"></div>
            <div class="pull-right">
                <a href="{{route('base.create')}}" class="btn btn-white"><i class="fa fa-plus"></i> Novo Serviço Base</a>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($servicos) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição</th>
                                <th>Serviço</th>
                                <th>Data de Cadastro</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($servicos as $servico): ?>
                                <tr>
                                    <td>{{ $servico->id }}</td>
                                    <td>{{ $servico->nome }}</td>
                                    <td>{{ $servico->servico->descricao }}</td>
                                    <td>{{ $servico->data_cadastro->format('d/m/Y H:i:s') }}</td>
                                    <td class="text-right footable-visible footable-last-column">
                                        <a href={{ url('/servicos/preco/base/atualizar/'.$servico->id) }}>
                                            <button class="btn-warning btn-circle btn btn-xs">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a>
                                            <button data-id="{{$servico->id}}" class="remover-base btn-danger btn-circle btn btn-xs">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $servicos->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Não há serviços base cadastrados</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".remover-base", function (e) {
                var id = $(this).attr('data-id');
                var action = base_path+'/servicos/preco/base/remover/'+id;

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'DELETE',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function (data) {
                        mensagem('success', 'Registro removido com sucesso!');
                        location.reload();
                    }
                });
            });
        });
    </script>
@endsection