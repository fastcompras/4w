@extends('layouts.app')

@section('title', 'Sou 4W - Lista de Serviços Disponíveis')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>
                <i class="fa fa-gear"></i>
                Serviços Disponíveis
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('servicos.show')}}">Lista de Serviços</a>
                </li>
            </ol>
            <div class="hr-line-dashed"></div>
            <div class="pull-right">
                <a href="{{route('servicos.create')}}" class="btn btn-white"><i class="fa fa-plus"></i> Novo Serviço</a>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($servicos) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Descrição</th>
                                <th>Texto</th>
                                <th>Data</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($servicos as $servico): ?>
                                <tr>
                                    <td>{{$servico->id}}</td>
                                    <td>{{$servico->descricao}}</td>
                                    <td>{{strlen($servico->descricao_extensa) > 30 ? substr($servico->descricao_extensa, 0, 27).'...' : $servico->descricao_extensa}}</td>
                                    <td>{{$servico->data_cadastro->format('d/m/Y H:i:s') }}</td>
                                    <td class="text-right footable-visible footable-last-column">
                                        <a target="_blank" href="https://www.sou4w.com.br/servicos/{{$servico->url}}">
                                            <button title="Visualizar Frontend" class="btn-info btn-circle btn btn-xs">
                                                <i class="fa fa-link" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a href={{url('/servicos/atualizar/'.$servico->id)}}>
                                            <button title="Editar" class="btn-warning btn-circle btn btn-xs">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a>
                                            <button title="Remover" data-id="{{$servico->id}}" class="remover-servico btn-danger btn-circle btn btn-xs">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $servicos->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Não há serviços cadastrados</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".remover-servico", function (e) {
                var id = $(this).attr('data-id');
                var action = base_path+'/servicos/remover/'+id;

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'DELETE',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', data.msg);

                            location.reload();
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });
        });
    </script>
@endsection