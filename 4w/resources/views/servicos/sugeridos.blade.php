@extends('layouts.app')

@section('title', 'Sou 4W - Lista de Serviços Sugeridos')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>
                <i class="fa fa-lightbulb-o"></i>
                Serviços Sugeridos
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('servicos.sugestao.show')}}">Lista de Serviços Sugeridos</a>
                </li>
            </ol>
            <div class="hr-line-dashed"></div>
            <div class="pull-right">
                <a href="{{route('servicos.create')}}" class="btn btn-white"><i class="fa fa-plus"></i> Novo Serviço</a>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($servicos) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Prestador</th>
                                <th>Descrição</th>
                                <th>Data</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($servicos as $servico): ?>
                                <?php $class = $servico->getStatus() == "Pendente" ? "label label-warning" : "label label-danger"; ?>
                                <tr>
                                    <td>{{$servico->id}}</td>
                                    <td>{{$servico->usuario->nome}}</td>
                                    <td>{{$servico->descricao}}</td>
                                    <td>{{$servico->data_cadastro->format('d/m/Y H:i:s') }}</td>
                                    <td>
                                        <span class="{{$class}}">{{$servico->getStatus()}}</span>
                                    </td>
                                    <td class="text-right footable-visible footable-last-column">
                                        <a>
                                            <button title="Aprovar Serviço" class="aprovar-servico btn-primary btn-circle btn btn-xs" data-id="{{$servico->id}}">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a>
                                            <button title="Rejeitar sugestão" data-id="{{$servico->id}}" class="rejeitar-servico btn-danger btn-circle btn btn-xs">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $servicos->render(); ?>
                    <?php else: ?>
                        <h3 class="text-danger text-center">Não há sugestões de serviços</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".aprovar-servico", function (e) {
                var id = $(this).attr('data-id');
                var action = base_path+'/servicos/aprovar/'+id;

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', data.msg);

                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });

            $(document).on("click", ".rejeitar-servico", function (e) {
                var id = $(this).attr('data-id');
                var action = base_path+'/servicos/rejeitar/'+id;

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', data.msg);

                            setTimeout(function() {
                                location.reload();
                            }, 500);

                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });
        });
    </script>
@endsection