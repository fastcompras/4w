@extends('layouts.app')

@section('title', 'Sou 4W - Dashboard')

<link rel="stylesheet" href="{{url('/css/lib/dash/prestador.css')}}" />

@section('content')
    <div class="row border-bottom white-bg dashboard-header">
        <div class="col-md-5" style="margin-top: -20px;">
            <div class="social-avatar">
                <a class="pull-left" style="margin-left: -20px;">
                    <img src="https://www.sou4w.com.br/homepage/assets/logo.png">
                </a>
            </div>
            <h2>Olá, <strong>{{ Auth::user()->nome }}</strong>!</h2>
            <span>Nós da <strong>4W</strong> estamos prontos para lhe atender no que você precisar.</span> <br/>
            <span>Você pode solicitar um serviço a qualquer momento clicando no botão <strong>abaixo</strong>.</span> <br/> <br/>
            <a href="{{ url('/oportunidades/solicitar') }}" class="btn btn-success btn-sm btn-block"> <i class="fa fa-plus"></i> Solicitar Serviço </a>
            <div class="hr-line-dashed"></div>
            <div class="widget style1 blue-bg">
                <div class="row vertical-align">
                    <div class="col-xs-12">
                        <h2 class="no-margins"> {{ Auth::user()->getQuantidadeServicos(\App\Models\Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL) }} </h2> <br/>
                        <h3 class="font-bold" style="margin-bottom: -5px;">Serviços com o status <u>Selecionando Profissionais</u></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-1"></div>
        <div class="col-md-6">
            <ul class="stat-list">
                <li class="alert alert-warning">
                    <h2 class="no-margins"> {{ Auth::user()->getQuantidadeServicos(\App\Models\Oportunidade::STATUS_AGUARDANDO_PAGAMENTO) }} </h2>
                    <small class="font-bold">Serviços Aguardando Confirmação de Pagamento</small>
                </li>
                <li class="alert alert-success">
                    <h2 class="no-margins"> {{ Auth::user()->getQuantidadeServicos(\App\Models\Oportunidade::STATUS_AGENDADO) }} </h2>
                    <small class="font-bold">Serviços Agendados</small>
                </li>
                <li class="alert alert-danger">
                    <h2 class="no-margins"> {{ Auth::user()->getQuantidadeServicos(\App\Models\Oportunidade::STATUS_ENCERRADO) }}</h2>
                    <small class="font-bold">Serviços Encerrados</small>
                </li>
            </ul>
        </div>
        @if (Auth::user()->getQuantidadeServicos(\App\Models\Oportunidade::STATUS_AGUARDANDO_PAGAMENTO) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-hourglass-half"></i> Serviços Aguardando Pagamento</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosAguardandoPagamento() as $servico)
                                    <tr>
                                        <td> {{ $servico['descricao'] }} </td>
                                        <td> {{ $servico['bairro'] }} </td>
                                        <td> {{ $servico['valor'] }} </td>
                                        <td> <a href="{{ $servico['link'] }}" style="color:white;" class="btn btn-xs btn-primary"> Pagar </a> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (count(Auth::user()->servicosPendentesAvaliacao()) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-star"></i> Serviços Aguardando Avaliação </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosPendentesAvaliacao() as $servico)
                                    <tr>
                                        <td> {{ $servico['descricao'] }} </td>
                                        <td> {{ $servico['bairro'] }} </td>
                                        <td> {{ $servico['valor'] }} </td>
                                        <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Avaliar </a> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (Auth::user()->getQuantidadeServicos(\App\Models\Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-user"></i> Selecionando Profissionais </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosSelecionandoProfissional() as $servico)
                                    <tr>
                                        <td> {{ $servico['descricao'] }} </td>
                                        <td> {{ $servico['bairro'] }} </td>
                                        <td> {{ $servico['valor'] }} </td>
                                        <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Visualizar </a> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (Auth::user()->getQuantidadeServicos(\App\Models\Oportunidade::STATUS_AGENDADO) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-calendar"></i> Serviços Agendados </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosAgendados() as $servico)
                                    <tr>
                                        <td> {{ $servico['descricao'] }} </td>
                                        <td> {{ $servico['bairro'] }} </td>
                                        <td> {{ $servico['valor'] }} </td>
                                        <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Visualizar </a> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection