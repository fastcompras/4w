@extends('layouts.app')

@section('title', 'Sou 4W - Dashboard')

<link rel="stylesheet" href="{{url('/css/lib/dash/prestador.css')}}" />

@section('content')
    <div class="row border-bottom white-bg dashboard-header">
        <div class="col-md-12">
            <div class="social-avatar">
                <a class="pull-left" style="margin-left: -20px;"><img src="/img/logo.png"></a>
            </div>
            <h2 class="text-center">Bem-vindo, <strong>{{ Auth::user()->nome }}</strong>, vamos trabalhar!</h2>
            <div class="hr-line-dashed" style="margin-top: 10px; margin-bottom: 10px;"></div>
            <p class="text-center">Nós da <strong>4W</strong> estamos sempre em busca de um novo serviço para nossos prestadores.</p>
            <p class="text-center">Para encontrarmos serviços para você, é necessário que <strong>todos</strong> os documentos sejam enviados para que possamos avaliá-los.</p> <br/><br/>
            <a href="{{ url('/conta/documentosPendentes') }}" class="btn btn-danger btn-sm btn-block"> <i class="fa fa-file"></i> Complete seu Cadastro </a>
        </div>
    </div>
@endsection
