@extends('layouts.app')

@section('title', 'Sou 4W - Dashboard')

<link rel="stylesheet" href="{{url('/css/lib/dash/prestador.css')}}" />

@section('content')
    <div class="row border-bottom white-bg dashboard-header">
        <div class="col-md-6">
            <div class="social-avatar">
                <a class="pull-left" style="margin-left: -20px;">
                    <img src="https://www.sou4w.com.br/homepage/assets/logo.png">
                </a>
            </div>
            <h2>Bem-vindo, <strong>{{ Auth::user()->nome }}</strong>, vamos trabalhar!</h2>
            <span>Nós da <strong>4W</strong> estamos sempre em busca de um novo serviço para nossos prestadores.</span> <br/>
            <div class="hr-line-dashed" style="margin-top:10px; margin-bottom: 10px;"></div>
            <div class="widget style1 blue-bg">
                <div class="row vertical-align">
                    <div class="col-xs-12">
                        <h3>Total recebido por serviços realizados: {{ Auth::user()->getTotalRecebido() }} </h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <ul class="stat-list">
                <li class="alert alert-warning">
                    <h2 class="no-margins"> {{ Auth::user()->qtdOportunidadesRecebidas() }} </h2>
                    <small class="font-bold">Oportunidades Recebidas</small>
                </li>
                <li class="alert alert-success">
                    <h2 class="no-margins"> {{ Auth::user()->qtdOportunidadesAceitas() }} </h2>
                    <small class="font-bold">Oportunidades Aceitas</small>
                </li>
                <li class="alert alert-danger">
                    <h2 class="no-margins"> {{ Auth::user()->qtdOportunidadesPerdidas() }}</h2>
                    <small class="font-bold">Oportunidades Perdidas</small>
                </li>
            </ul>
        </div>
        @if (count(Auth::user()->servicosAguardandoConfirmacao()) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-question-circle"></i> Aguardando sua Confirmação </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosAguardandoConfirmacao() as $servico)
                                        <tr>
                                            <td> {{ $servico['descricao'] }} </td>
                                            <td> {{ $servico['bairro'] }} </td>
                                            <td> {{ $servico['valor_prestador'] }} </td>
                                            <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Visualizar </a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (Auth::user()->getQuantidadeServicos(\App\Models\Oportunidade::STATUS_AGENDADO) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-calendar"></i> Serviços Agendados </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosAgendados() as $servico)
                                    <tr>
                                        <td> {{ $servico['descricao'] }} </td>
                                        <td> {{ $servico['bairro'] }} </td>
                                        <td> {{ $servico['valor_prestador'] }} </td>
                                        <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Visualizar </a> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (count(Auth::user()->servicosPendentesAvaliacao()) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-star"></i> Serviços Aguardando Avaliação </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosPendentesAvaliacao() as $servico)
                                    <tr>
                                        <td> {{ $servico['descricao'] }} </td>
                                        <td> {{ $servico['bairro'] }} </td>
                                        <td> {{ $servico['valor_prestador'] }} </td>
                                        <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Avaliar </a> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (count(Auth::user()->servicosAguardandoEncerramento()) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-hourglass-half"></i> Serviços Aguardando Encerramento </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosAguardandoEncerramento() as $servico)
                                    <tr>
                                        <td> {{ $servico['descricao'] }} </td>
                                        <td> {{ $servico['bairro'] }} </td>
                                        <td> {{ $servico['valor_prestador'] }} </td>
                                        <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Encerrar </a> </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
