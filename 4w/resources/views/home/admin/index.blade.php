<?php
    use App\Models\Oportunidade;
?>

@extends('layouts.app')

@section('title', 'Sou 4W - Dashboard')

<link rel="stylesheet" href="{{url('/css/lib/dash/prestador.css')}}" />

@section('content')
    <div class="row border-bottom white-bg dashboard-header">
        <div class="col-md-12">
            <div class="social-avatar">
                <a class="pull-left" style="margin-left: -20px;">
                    <img src="https://www.sou4w.com.br/homepage/assets/logo.png">
                </a>
            </div>
            <h2>Olá, <strong>{{ Auth::user()->nome }}</strong>!</h2>
            <div class="hr-line-dashed"></div>
        </div>
        <div class="col-md-4">
            <div class="alert alert-warning">
                <h2 class="no-margins"> {{ Auth::user()->getQuantidadeServicos(Oportunidade::STATUS_AGUARDANDO_PAGAMENTO) }} </h2>
                <small class="font-bold">Serviços Aguardando Confirmação de Pagamento</small>
            </div>
        </div>
        <div class="col-md-4">
            <div class="alert alert-success">
                <h2 class="no-margins"> {{ Auth::user()->getQuantidadeServicos(Oportunidade::STATUS_AGENDADO) }} </h2>
                <small class="font-bold">Serviços Agendados</small>
            </div>
        </div>
        <div class="col-md-4">
            <div class="alert alert-danger">
                <h2 class="no-margins"> {{ Auth::user()->getQuantidadeServicos(Oportunidade::STATUS_ENCERRADO) }} </h2>
                <small class="font-bold">Serviços Encerrados</small>
            </div>
        </div>
        @if (Auth::user()->getQuantidadeServicos(Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-user"></i> Selecionando Profissionais </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                @foreach (Auth::user()->servicosSelecionandoProfissional() as $servico)
                                    <table class="table table-hover no-margins">
                                        <thead>
                                        <tr>
                                            <th>Serviço</th>
                                            <th>Bairro</th>
                                            <th>Valor</th>
                                            <th>Ação</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td> {{ $servico['descricao'] }} </td>
                                            <td> {{ $servico['bairro'] }} </td>
                                            <td> {{ $servico['valor'] }} </td>
                                            <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Visualizar </a> </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (count(Auth::user()->servicosAguardandoConfirmacao()) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-question-circle"></i> Aguardando Confirmação de algum Prestador</h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Serviço</th>
                                        <th>Bairro</th>
                                        <th>Valor</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->servicosAguardandoConfirmacao() as $servico)
                                        <tr>
                                            <td> {{ $servico['descricao'] }} </td>
                                            <td> {{ $servico['bairro'] }} </td>
                                            <td> {{ $servico['valor'] }} </td>
                                            <td> <a href="{{ url('/oportunidades/visualizar/' . $servico['id']) }}" class="btn btn-xs btn-success"> Visualizar </a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if (count(Auth::user()->usuariosPendentesAprovacao()) > 0)
            <div class="col-md-6">
                <div class="hr-line-dashed"></div>
                <div class="ibox float-e-margins" style="border-top:none;">
                    <div class="ibox-title" style="border-top: none;">
                        <h5> <i class="fa fa-file"></i> Prestadores pendentes de Aprovação </h5>
                    </div>
                    <div class="ibox-content">
                        <div>
                            <div class="feed-activity-list">
                                <table class="table table-hover no-margins">
                                    <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Data Cadastro</th>
                                        <th>Ação</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach (Auth::user()->usuariosPendentesAprovacao() as $usuario)
                                        <tr>
                                            <td> {{ $usuario['nome'] }} </td>
                                            <td> {{ $usuario['data_cadastro'] }} </td>
                                            <td> <a href="{{ url('/usuarios/prestadores/visualizarDocumentacao/' . $usuario['id']) }}" class="btn btn-xs btn-success"> Visualizar </a> </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection