<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Sou 4W - {{  $title }} </title>

    <link rel="stylesheet" href="{{url('/css/vendor.css')}}" />
    <link rel="stylesheet" href="{{url('/css/app.css')}}" />
    <link rel="stylesheet" href="{{url('/css/toastr.min.css')}}" />
    <link rel="stylesheet" href="{{url('/css/lib/conta/recuperar-senha.css')}}" />
    <script src="{{url('/js/app.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/functions.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/toastr.min.js')}} "type="text/javascript"></script>
    <script src="{{url('/js/plugins/jquery-mask/jquery.maskedinput.js')}}" type="text/javascript"></script>

</head>

<style>
    #toast-container > div.toast {
        background-image: none !important;
    }
</style>

<body class="gray-bg">
    <div class="passwordBox animated fadeInDown">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <div class="recovery">
                        <h2 class="text-center font-bold"><i class="fa fa-key"></i> Redefinição de senha</h2>
                        <hr>
                        <p> Informe sua nova senha e a repita para realizar a redefinição. </p>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="m-t" role="form">
                                    <input id="token" data-token="<?php echo $token; ?>" type="hidden">
                                    <div class="form-group">
                                        <input id="password1" type="password" class="form-control" autocomplete="new-password" placeholder="Informe a nova senha" required="">
                                    </div>
                                    <div class="form-group">
                                        <input id="password2" type="password" class="form-control" autocomplete="new-password" placeholder="Repita a nova senha" required="">
                                    </div>
                                    <button class="reset-password btn btn-success block full-width m-b">Redefinir senha</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading" style="display: none;">
        <div class="spiner-example">
            <div class="loader-image sk-spinner sk-spinner-three-bounce">
                <div class="sk-bounce1" style="width: 25px; height: 25px;"></div>
                <div class="sk-bounce2" style="width: 25px; height: 25px;"></div>
                <div class="sk-bounce3" style="width: 25px; height: 25px;"></div>
            </div>
        </div>
    </div>
</body>

@section('scripts')

<script type="text/javascript">

    $(document).ready(function(){
        var base_path = "{{url('/')}}";
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $(document).on("click", ".reset-password", function (e) {
            e.preventDefault();
            var password1 = $('#password1').val();
            var password2 = $('#password2').val();
            var action = base_path + '/register/resetPassword';

            if (password1.length == 0 || password2.length == 0) {
                mensagem('error', 'Todos os campos devem ser preenchidos!');
                return false;
            }

            if (password1 != password2) {
                mensagem('error', 'As senhas informadas não conferem!');
                return false;
            }

            var data = {
                token: $('#token').attr('data-token'),
                password: password1,
                _token: CSRF_TOKEN,
            };

            $('.loading').show();
            $.ajax({
                data: data,
                type: 'POST',
                dataType: 'json',
                url: action,
                cache: false,
                success: function () {
                    $('.loading').hide();

                    mensagem('success', 'Sua senha foi alterada com sucesso!');

                    setTimeout(function() {
                        $(location).attr('href', base_path + '/login');
                    }, 800);
                },
                error: function (response) {
                    $('.loading').hide();

                    switch (response.status) {
                        case 404:
                        case 422:
                        case 500:
                        default:
                            mensagem('error', 'Erro ao realizar a alteração de sua senha! Por favor, tente novamente!');
                            return false;
                    }
                }
            });

        });
    });
</script>

@show