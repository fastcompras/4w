<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Sou 4W - Solicitação de Serviço </title>

    <link rel="stylesheet" href="{{ url('/css/vendor.css') }}" />
    <link rel="stylesheet" href="{{ url('/css/app.css') }}" />
    <link rel="stylesheet" href="{{ url('/css/lib/registrar/page.css') }}" />
    <link rel="stylesheet" href="{{url('/css/toastr.min.css')}}" />
    <script src="{{url('/js/app.js')}}" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
    <script src="{{url('/js/toastr.min.js')}} "type="text/javascript"></script>
    <script src="{{url('/js/functions.js')}}" type="text/javascript"></script>

</head>

@include('vue.input-number')
@include('vue.input-radio')
@include('vue.input-single-checkbox')
@include('vue.input-select-perfil')
@include('vue.input-select-servico')
@include('vue.input-select-servico-base')
@include('vue.services.aggravating')
@include('vue.register.step1')
@include('vue.register.step2')
@include('vue.register.user-info')
@include('vue.input-string')
@include('vue.input-email')
@include('vue.input-cep')

<body class="gray-bg">
    <div id="app">
        <div class="wrapper wrapper-content">
            <div class="row">
                <div class="col-lg-offset-1 col-lg-10">
                    <div class="ibox product-detail">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-12 hidden-xs">
                                    <h2 class="text-center font-bold m-b-xs">
                                        @{{ stepDescription }}
                                    </h2>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-offset-1 col-md-10">

                                            <div v-show="step == 1">

                                                <step1
                                                    v-on:ammount="updateAmmount"
                                                    :service="service">
                                                </step1>

                                            </div>
                                            <div v-show="step == 2">
                                                <step2
                                                    :service="service"
                                                    :ammount="ammountOrder"
                                                    :user="user">
                                                </step2>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div v-if="step == 1" class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-offset-1 col-md-10">
                                            <div class="hr-line-dashed" style="margin-top:30px; margin-bottom:10px;"></div>
                                            <user-info
                                                :user="user"
                                                v-on:errors="updateErrors($event)">
                                            </user-info>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ibox-footer" style="height: 60px;">
                            <div class="btn-group pull-right">
                                <button v-if="step > 1" v-on:click="back" class="btn btn-4w"> <i class="fa fa-arrow-left"></i> Passo @{{ prevStep }} </button>
                                <button v-if="nextStep < 3" v-on:click="next" class="btn btn-4w pull-right"> Passo @{{ nextStep }} <i class="fa fa-arrow-right"></i> </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>


@section('scripts')

<script>

    var vm = new Vue({
        el: '#app',
        data: {
            user: {
                name: null,
                email: null,
                repeatedEmail: null,
                password: null,
                address: {
                    street: null,
                    number: null,
                    address_code: null,
                    neighborhood: null,
                    city: {
                        name: null,
                        uf: null
                    }
                }
            },
            userCopy: null,
            service: {
                id: null,
                base: null,
                aggravating: [],
            },
            step: 1,
            ammountOrder: 0,
            userErrors: {
                cep: false,
                name: false,
                email: false
            }
        },
        computed: {
            nextStep: function () {
                return this.step + 1;
            },
            prevStep: function () {
                return this.step - 1;
            },
            stepDescription: function () {
                switch (this.step) {
                    case 1:
                        return 'Conte para nós o que você precisa';
                    case 2:
                        return 'Resumo da solicitação de serviço'
                }
            },
        },
        methods: {
            updateAmmount: function (payload) {
                this.ammountOrder = payload;
            },
            resetUser: function () {
                this.user = this.userCopy;
            },
            back: function () {
                this.step = this.step - 1;
            },
            next: function () {
                let hasError = false;

                Object.keys(this.userErrors).forEach(function (key) {
                    if (vm.userErrors[key] === true) {
                        hasError = true;
                    }
                });

                if (hasError) {
                    this.user = this.userCopy;
                    mensagem('error', 'Ops! Verifique os campos do formulário!');
                    return false;
                }

                if (! this.user.name || ! this.user.address.address_code || ! this.user.email) {
                    this.user = this.userCopy;
                    mensagem('error', 'Ops! Verifique os campos do formulário!');
                    return false;
                }

                this.step = this.step + 1;
            },
            updateErrors: function (value) {
                this.userErrors = value;
            }
        },
        created: function () {
            this.userCopy = this.user;
        }
    });

</script>


@show