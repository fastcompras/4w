<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Sou 4W - {{ $title }} </title>

    <link rel="stylesheet" href="{{url('/css/vendor.css')}}" />
    <link rel="stylesheet" href="{{url('/css/app.css')}}" />
    <link rel="stylesheet" href="{{url('/css/comum.css')}}" />
    <link rel="stylesheet" href="{{url('/css/toastr.min.css')}}" />
    <link rel="stylesheet" href="{{url('/css/lib/conta/cadastro.css')}}" />
    <link rel="stylesheet" href="{{url('/css/plugins/jasny/jasny-bootstrap.min.css')}}" />
    <script src="{{url('/js/app.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/functions.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/toastr.min.js')}} "type="text/javascript"></script>
    <script src="{{url('/js/plugins/jquery-mask/jquery.maskedinput.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/jasny/jasny-bootstrap.min.js')}}" type="text/javascript"></script>

</head>

<body class="gray-bg">

    <div class="row progressbar-wrapper">
        <ul class="progressbar progressbar-wrapper">
            <li>Credenciais</li>
            <li>Contato</li>
            <li class="active">Documentação</li>
            <li>Cadastro Concluído</li>
        </ul>
    </div>
    <div class="middle-box text-center loginscreen animated fadeInDown">
            <div>
            <h2 style="color:black; margin-top: -30px;">
                <i class="fa fa-paperclip" aria-hidden="true"></i>
                Documentação
            </h2>
            <small class="text-danger">Os arquivos solicitados abaixo devem ser enviados em algum dos seguintes formatos: <strong>PDF, PNG, JPG</strong>.</small>
            <div class="hr-line-dashed"></div>
            <form class="m-t" id="passo-3" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    {{ Form::label('avatar', 'Selecione sua foto (*)', array('style'=>'color:black'))}}
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Selecionar Imagem</span>
                            <span class="fileinput-exists">Trocar</span>
                            <input type="file" id="avatar" name="avatar"/>
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('avatar', 'Selecione seus diplomas/certificados', array('style'=>'color:black'))}}
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Selecionar Documentos</span>
                            <span class="fileinput-exists">Trocar</span>
                            <input type="file" id="certificados" multiple name="certificados[]"/>
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('avatar', 'Selecione seu documento de MEI', array('style'=>'color:black'))}}
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Selecionar Documentos</span>
                            <span class="fileinput-exists">Trocar</span>
                            <input type="file" id="mei" name="mei"/>
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('rg', 'Selecione seu RG (*)', array('style'=>'color:black'))}}
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Selecionar Documento</span>
                            <span class="fileinput-exists">Trocar</span>
                            <input type="file" id="rg" name="rg"/>
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('rg', 'Selecione seu CPF', array('style'=>'color:black'))}}
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Selecionar Documento</span>
                            <span class="fileinput-exists">Trocar</span>
                            <input type="file" id="cpf" name="cpf"/>
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('rg', 'Selecione seu Comprovante de Residência (*)', array('style'=>'color:black'))}}
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Selecionar Documento</span>
                            <span class="fileinput-exists">Trocar</span>
                            <input type="file" id="residencia" name="residencia"/>
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                    </div>
                </div>
                <div class="form-group">
                    {{ Form::label('rg', 'Selecione seu Atestado de Antecedentes (*)', array('style'=>'color:black'))}}
                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                        <div class="form-control" data-trigger="fileinput">
                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                            <span class="fileinput-filename"></span>
                        </div>
                        <span class="input-group-addon btn btn-default btn-file">
                            <span class="fileinput-new">Selecionar Documento</span>
                            <span class="fileinput-exists">Trocar</span>
                            <input type="file" id="antecedentes" name="antecedentes"/>
                        </span>
                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                    </div>
                </div>
                <button class="voltar btn btn-primary block full-width m-b">Voltar ao Passo 2</button>
                <button class="finalizar btn btn-warning block full-width m-b">Pular este Passo</button>
                <button class="finalizar btn btn-success block full-width m-b">Finalizar Cadastro</button>
            </form>
            </div>
    </div>
    <div class="modal fade" id="chromeWarning" tabindex="-1" role="dialog" aria-labelledby="chromeModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="chromeModalLabel">Atenção usuário de Google Chrome</h4>
            </div>
            <div class="modal-body">
                <p>
                    Seu browser é <span class="chrome-version-label" style="font-weight: bold">Google Chrome</span> e verificamos que há um problema ao enviar arquivos através deste browser.
                </p><p>
                    Pedimos que você pule esta etapa de envio de arquivos por enquanto.<br/>
                    Sugerimos que você envie os arquivos em outro momento utilizando outro browser, como o <a href="https://www.mozilla.org/pt-BR/firefox/">Firefox</a>, <a href="https://www.opera.com/pt-br">Opera</a>, ou <a href="https://www.microsoft.com/pt-br/edge">Microsoft Edge</a>.
                </p>
                <p>O botão 'Pular este Passo' se encontra no formulário abaixo, em amarelo.</p>
                <p>Desculpe o inconveniente e obrigado pela paciência.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
            </div>
        </div>
    </div>

    @include('helpers.spinner')
</body>

@section('scripts')

<script type="text/javascript">

    window.addEventListener("load", function() {

        // Alerta e log de pessoas que acessam o site por google chrome
        const isChrome = (window.navigator && window.navigator.appVersion && window.navigator.appVersion.includes && window.navigator.appVersion.includes("Chrome") && !window.navigator.appVersion.includes("Edg") && !window.navigator.appVersion.includes("Trident"));
        if (isChrome) {
            if (window.navigator.appVersion.includes("/8")) {
                try {
                    const browser = "Google Chrome " + window.navigator.appVersion.split("Chrome/")[1].substring(0, 9);
                    document.querySelector(".chrome-version-label").innerText = browser;
                } catch (err) { }
                $("#chromeWarning").modal('show');
            }
            // Tenta enviar um log para salvar dados gerais da versão do browser do cliente para facilitar o debug do problema
            try {
                if (new Date() < new Date("2020-06-10")) {
                    function getPluginList() {
                        try {
                            let pluginDescriptors = [];
                            for (let i = 0 ; i < window.navigator.plugins.length; i++) {
                                pluginDescriptor.push(
                                    window.navigator.plugins[0].name + " - " + window.navigator.plugins[0].description
                                );
                            }
                            return pluginDescriptors.join(", ");
                        } catch (err) { return err.toString() }
                    }
                    var obj = {
                        message: "New Google Chrome access (passo-3.blade) \"" + window.location.href + "\"",
                        appVersion: window.navigator.appVersion,
                        userAgent: window.navigator.userAgent,
                        platform: window.navigator.platform,
                        plugins: getPluginList(),
                    }
                    send_err_data(obj);
                }
            } catch (err) {
                console.log("Failed sending access log");
                console.log(err)
            }
        }


        var base_path = "{{url('/')}}";
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $(document).on("click", ".voltar", function (e) {
            e.preventDefault();

            $(location).attr('href', base_path+'/registrar/passo2');
        });

        $(document).on("click", ".finalizar", function (e) {
            e.preventDefault();
            try {
                var formData = new FormData(document.querySelector('#passo-3'));

                formData.append('avatar', document.querySelectorAll('#avatar')[0].files[0]);
                formData.append('rg', document.querySelectorAll('#rg')[0].files[0]);
                formData.append('cpf', document.querySelectorAll('#cpf')[0].files[0]);
                formData.append('residencia', document.querySelectorAll('#residencia')[0].files[0]);
                formData.append('antecedentes', document.querySelectorAll('#antecedentes')[0].files[0]);
                formData.append('mei', document.querySelectorAll('#mei')[0].files[0]);

                $.each($("#certificados")[0].files, function(i, file) {
                    formData.append('certificados[]', file);
                });

                formData.append('_token', CSRF_TOKEN);

                if (typeof formData.get("avatar") !== 'undefined' && formData.get("avatar").size > 2048000*2) {
                    (send_err_data && send_err_data({mensagem: "Sua foto deve conter no máximo 4MB!", size: formData.get("avatar").size}));
                    mensagem('error', 'Sua foto deve conter no máximo 4MB! (O arquivo deve ser redimensionado ou cortado)');
                    return false;
                }

                if (typeof formData.get("rg") !== 'undefined' && formData.get("rg").size > 2048000*2) {
                    (send_err_data && send_err_data({mensagem: "Seu documento deve conter no máximo 4MB!", size: formData.get("rg").size}));
                    mensagem('error', 'Seu documento deve conter no máximo 4MB! (O arquivo deve ser redimensionado ou cortado)');
                    return false;
                }

                if (typeof formData.get("cpf") !== 'undefined' && formData.get("cpf").size > 2048000*2) {
                    (send_err_data && send_err_data({mensagem: "Seu documento deve conter no máximo 4MB!", size: formData.get("cpf").size}));
                    mensagem('error', 'Seu documento deve conter no máximo 4MB! (O arquivo deve ser redimensionado ou cortado)');
                    return false;
                }

                if (typeof formData.get("residencia") !== 'undefined' && formData.get("residencia").size > 2048000*2) {
                    (send_err_data && send_err_data({mensagem: "Seu documento deve conter no máximo 4MB!", size: formData.get("residencia").size}));
                    mensagem('error', 'Seu documento deve conter no máximo 4MB! (O arquivo deve ser redimensionado ou cortado)');
                    return false;
                }

                if (typeof formData.get("antecedentes") !== 'undefined' && formData.get("antecedentes").size > 2048000*2) {
                    (send_err_data && send_err_data({mensagem: "Seu documento deve conter no máximo 4MB!", size: formData.get("antecedentes").size}));
                    mensagem('error', 'Seu documento deve conter no máximo 4MB! (O arquivo deve ser redimensionado ou cortado)');
                    return false;
                }

                $('.loading').show();
                $.ajax({
                    url: base_path+'/registrar/finalizar', // Url do lado server que vai receber o arquivo
                    data: formData,
                    cache: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        $('.loading').hide();
                        if (data.success) {
                            mensagem('success', 'Cadastro realizado com sucesso!');

                            setTimeout(function() {
                                $(location).attr('href', base_path + data.url);
                            }, 1100);

                        } else {
                            mensagem('error', data.mensagem);
                        }
                    }
                });
            } catch (err) {
                mensagem('error', "Ocorreu um erro ao enviar os arquivos: " + err);
            }
        });
    });
</script>
<script>
function send_err_data(data) {
    console.log(data);
    fetch("https://www.sou4w.com.br/log/", {method:"post", body: JSON.stringify(data)}).then(console.log).catch(console.warn);
}
window.onerror = function(message, source, lineno, colno, error) {
    var obj = {
        message: message,
        source: source+":"+lineno+":"+colno,
        error: error
    }
    send_err_data(obj);
}
</script>

@show