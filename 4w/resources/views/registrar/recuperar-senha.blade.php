<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Sou 4W - {{  $title }} </title>

    <link rel="stylesheet" href="{{url('/css/vendor.css')}}" />
    <link rel="stylesheet" href="{{url('/css/app.css')}}" />
    <link rel="stylesheet" href="{{url('/css/toastr.min.css')}}" />
    <link rel="stylesheet" href="{{url('/css/lib/conta/recuperar-senha.css')}}" />
    <script src="{{url('/js/app.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/functions.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/toastr.min.js')}} "type="text/javascript"></script>
    <script src="{{url('/js/plugins/jquery-mask/jquery.maskedinput.js')}}" type="text/javascript"></script>

</head>

<style>
    #toast-container > div.toast {
        background-image: none !important;
    }
</style>

<body class="gray-bg">
    <div class="passwordBox animated fadeInDown">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <div class="recovery">
                        <h2 class="text-center font-bold"><i class="fa fa-key"></i> Recuperar senha</h2>
                        <hr>
                        <p> Informe seu e-mail e em breve você receberá um link para realizar a redefinição de sua senha. </p>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="m-t" role="form">
                                    <div class="form-group">
                                        <input id="email" type="email" class="form-control" placeholder="Endereço de e-mail" required="">
                                    </div>
                                    <button class="new-password btn btn-success block full-width m-b">Solicitar nova senha</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="email-sended" style="display: none;">
                        <h2 class="text-center font-bold"> <i class="fa fa-envelope"></i> E-mail enviado </h2>
                        <hr>
                        <p>
                            Para recuperar sua senha, basta clicar no link do e-mail para redefinir sua senha.
                        </p>
                        <p>
                            Caso não consiga ver o e-mail, verifique outros lugares onde ele possa estar, como sua pasta de lixo eletrônico, spam, social ou outras.
                        </p>
                        <br>
                        <a href="{{url('/login')}}">
                            <button class="btn btn-success block full-width m-b">Voltar para o Login</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="loading" style="display: none;">
        <div class="spiner-example">
            <div class="loader-image sk-spinner sk-spinner-three-bounce">
                <div class="sk-bounce1" style="width: 25px; height: 25px;"></div>
                <div class="sk-bounce2" style="width: 25px; height: 25px;"></div>
                <div class="sk-bounce3" style="width: 25px; height: 25px;"></div>
            </div>
        </div>
    </div>
</body>

@section('scripts')

<script type="text/javascript">

    $(document).ready(function(){
        var base_path = "{{url('/')}}";
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $(document).on("click", ".new-password", function (e) {
            e.preventDefault();
            var email = $('#email').val();
            var action = base_path + '/register/sendLinkToNewPassword';

            if (email.length == 0) {
                mensagem('error', 'O endereço de e-mail é obrigatório!');
                return false;
            }

            if (! isEmail(email)) {
                mensagem('error', 'O e-mail informado não é um endereço válido!');
                return false;
            }

            var data = {
                email: email,
                _token: CSRF_TOKEN,
            };

            $('.loading').show();

            $.ajax({
                data: data,
                type: 'POST',
                dataType: 'json',
                url: action,
                cache: false,
                success: function () {
                    $('.loading').hide();
                    $('.recovery').hide();
                    $('.email-sended').show();
                },
                error: function (response) {
                    $('.loading').hide();

                    switch (response.status) {
                        case 404:
                            mensagem('error', 'O e-mail informado não foi encontrado no sistema!');
                            return false;
                        case 422:
                        case 500:
                            mensagem('error', 'Erro ao enviar o e-mail! Por favor, contate nosso suporte!');
                            return false;
                    }
                }
            });

        });

        function isEmail(email) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(email);
        }
    });
</script>

@show