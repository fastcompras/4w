<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Sou 4W - {{  $title }} </title>

    <link rel="stylesheet" href="{{url('/css/vendor.css')}}" />
    <link rel="stylesheet" href="{{url('/css/app.css')}}" />
    <link rel="stylesheet" href="{{url('/css/toastr.min.css')}}" />
    <link rel="stylesheet" href="{{url('/css/lib/conta/cadastro.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <script src="{{url('/js/app.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/functions.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/toastr.min.js')}} "type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js" type="text/javascript"></script>

</head>

<style>
    .select2-container .select2-selection.select2-selection--multiple .select2-search.select2-search--inline .select2-search__field:not([placeholder='']) {
        width: 100% !important;
        margin-right: 100000px;
    }
</style>

<body class="gray-bg">
    <div class="row progressbar-wrapper">
        <ul class="progressbar">
            <li class="active">Credenciais</li>
            <li>Contato</li>
            <li>Documentação</li>
            <li>Cadastro Concluído</li>
        </ul>
    </div>
    <div class="middle-box text-center loginscreen animated fadeInDown" style="padding-top: 20px;">
        <div>
            <h2 style="color:black">
                <i class="fa fa-key" aria-hidden="true"></i>
                Perfil e Credenciais
            </h2>
            <div class="hr-line-dashed"></div>
            <form id="passo-1">
                <br/>
                <div class="form-group">
                    {{ Form::label('perfil_conta', 'Selecione o perfil em que você se encaixa', array('style'=>'color:black'))}}
                    {{ Form::select('perfil_conta', $perfis, null, array('disabled' => true, 'class' => 'form-control', 'style' => 'width: 100%;')) }}
                    <hr>
                </div>
                <div class="form-group servicos" style="display: none;">
                    {{ Form::label('servicos', 'Selecione o tipo de serviço que você presta (*)', array('style'=>'color:black'))}}
                    <select id="servicos" class="form-control" placeholder="Selecione um serviço" style="width: 100%;"></select>
                    <hr>
                </div>
                <div class="form-group">
                    {{ Form::label('nome', 'Nome Completo (*)', array('style'=>'color:black'))}}
                    <input type="text" class="form-control" id="nome" placeholder="Nome Completo" value="{{ Session::get('nome') }}" required="true">
                </div>
                <div class="form-group">
                    {{ Form::label('email', 'E-mail (*)', array('style'=>'color:black'))}}
                    <input type="email" class="form-control" id="email" value="{{ Session::get('email') }}" placeholder="E-mail" required="true">
                </div>
                <div class="form-group">
                    {{ Form::label('email_confirmacao', 'Repita seu E-mail (*)', array('style'=>'color:black'))}}
                    <input type="email" class="form-control" id="email_confirmacao" value="{{ Session::get('email_confirmacao') }}" placeholder="Confirmação de E-mail" required="true">
                </div>
                <div class="form-group">
                    {{ Form::label('credenciais', 'Senha (*)', array('style'=>'color:black'))}}
                    <input type="password" class="form-control" id="password" placeholder="Senha" required="">
                </div>
                <button class="avancar btn btn-success block full-width m-b">Avançar</button>
                <p class="text-muted text-center">
                    <small>Já possui conta?</small>
                </p>
                <a class="btn btn-sm btn-white btn-block" href="{{url('/login')}}">Login</a>
            </form>
        </div>
    </div>
</body>

<div class="modal inmodal in" id="modalPerfil" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h4 style="color:black" class="modal-title">Seja bem-vindo à 4W</h4>
            </div>
            <div class="modal-body">
                <h4 style="color:black">Selecione abaixo o tipo de perfil que você deseja se vincular em nossa plataforma</h4>
                <br/>
                <a class="perfil-tomador">
                    <div class="well">
                        <h4 style="color:black">Usuário</h4>
                        <p>
                            Como <strong>usuário</strong> você poderá facilitar sua vida criando oportunidades e encontrando profissionais adequados para suprir suas necessidades do dia-dia.<br/>
                            Ex: Contratar domésticas, cuidadores de idosos, etc.
                        </p>
                    </div>
                </a>
                <a class="perfil-prestador">
                    <div class="well">
                        <h4 style="color:black">Prestador de Serviço</h4>
                        <p>
                            Como <strong>prestador de serviços</strong> na 4W você terá a possibilidade de oferecer seus serviços e encontrar oportunidades de trabalho com facilidade e segurança.
                        </p>
                    </div>
                </a>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

@section('scripts')

<script type="text/javascript">

    $(document).ready(function(){
        var base_path = "{{url('/')}}";
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var perfil_escolhido = "{{$perfil}}";

        if (perfil_escolhido == "prestador") {
            $("#perfil_conta").val(1).change();
            $('.servicos').show();
        } else if (perfil_escolhido == "usuario") {
            $("#perfil_conta").val(2).change();
        } else {
            $('#modalPerfil').show();
        }


        $('#servicos').select2({
            multiple: true,
            placeholder: 'Selecione um ou mais serviços',
            ajax: {
                url: "/api/servicos",
                dataType: 'json',
                data: function(params) {
                    return {
                        term: params.term || '',
                        page: params.page || 1
                    }
                },
                cache: true
            },
        });

        $(document).on("click", ".avancar", function (e) {
            e.preventDefault();
            var id_servico = $('#servicos').val();
            var perfil = $('#perfil_conta').val();
            var nome = $('#nome').val();
            var senha = $('#password').val();
            var email = $('#email').val();
            var email_confirmacao = $('#email_confirmacao').val();
            var sugestao_servico = $('#sugestao_servico').val();

            if (!nome || !senha || !email) {
                mensagem('error', 'Você deve preencher todos os campos para continuar!');
                return false;
            }

            //Caso seja o prestador é necessário escolher um serviço
            if (typeof id_servico == 'undefined' && perfil == 1) {
                mensagem('error', 'Você deve selecionar um serviço!');
                return false;
            }

            if (email !== email_confirmacao) {
                mensagem('error', 'Os e-mails informados não conferem!');
                return false;
            }

            var data = {
                nome: nome,
                email: email,
                password: senha,
                id_servico: id_servico,
                sugestao: sugestao_servico,
                id_perfil_conta: perfil,
                _token: CSRF_TOKEN,
            };

            $.ajax({
                data: data,
                type: 'POST',
                dataType: 'json',
                url: base_path + '/registrar/passo1',
                cache: false,
                success: function(data) {
                    if (data.success) {
                        $(location).attr('href', base_path+'/registrar/passo2');
                    } else {
                        if (data.mensagem !== null && typeof data.mensagem === 'object') {
                            $.each(data.mensagem, function(i, item) {
                                mensagem('error', item);
                            });
                        } else {
                            mensagem('error', data.mensagem);
                        }
                    }
                }
            });
        });

        $(document).on("change", "#servicos", function (e) {
            var value = $(this).find("option:selected").text();

            if (value == "Outro") {
                $('#sugestao_servico').show();
            } else {
                $('#sugestao_servico').val('');
                $('#sugestao_servico').hide();
            }
            //Outro
        });

        $(document).on("click", ".perfil-prestador", function (e) {
            $('#modalPerfil').hide();

            $('#perfil_conta').val(1).change();
            $('.servicos').show();
        });

        $(document).on("click", ".perfil-tomador", function (e) {
            $('#modalPerfil').hide();

            $('#perfil_conta').val(2).change();
        });
    });
</script>

@show