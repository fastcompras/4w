<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Sou 4W - {{ $title }} </title>

    <link rel="stylesheet" href="{{url('/css/vendor.css')}}" />
    <link rel="stylesheet" href="{{url('/css/app.css')}}" />
    <link rel="stylesheet" href="{{url('/css/toastr.min.css')}}" />
    <link rel="stylesheet" href="{{url('/css/lib/conta/cadastro.css')}}" />
    <link rel="stylesheet" href="{{url('/css/comum.css')}}" />
    <script src="{{url('/js/app.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/functions.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/toastr.min.js')}} "type="text/javascript"></script>
    <script src="{{url('/js/plugins/jquery-mask/jquery.maskedinput.js')}}" type="text/javascript"></script>

</head>

<?php
    $estados = App\Models\Estado::orderBy('nome', 'asc')->pluck('nome', 'uf');
?>

@if(Session::get('perfil_conta') == \App\Models\PerfilConta::PERFIL_PRESTADOR)
    <?php $margin = '50px'; ?>
@else
    <?php $margin = '300px'; ?>
@endif

<body class="gray-bg">
    <div class="row progressbar-wrapper">
        <ul class="progressbar" style="display: flex;justify-content: center;">
            <li>Credenciais</li>
            <li class="active">Contato</li>
            @if(Session::get('perfil_conta') == \App\Models\PerfilConta::PERFIL_PRESTADOR)
                <li>Documentação</li>
            @endif
            <li>Cadastro Concluído</li>
        </ul>
    </div>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <h2 style="color:black">
                <i class="fa fa-phone" aria-hidden="true"></i>
                Contato e Localização
            </h2>
            <div class="hr-line-dashed"></div>
            <form class="m-t" id="passo-2">
                <div class="form-group">
                    {{ Form::label('nome', 'CEP (*)', array('style'=>'color:black'))}}
                    <input type="text" class="form-control" id="cep" value="{{ Session::get('cep') }}" placeholder="CEP" required="true">
                </div>
                <div class="form-group">
                    {{ Form::label('nome', 'Estado (*)', array('style'=>'color:black'))}}
                    {{ Form::select('estados', $estados, null, array('id'=>'estados', 'class' => 'form-control', 'placeholder' => 'Selecione seu estado', 'style' => 'width: 100%;')) }}
                </div>
                <div class="form-group cidades" style="display: none;">
                    {{ Form::label('nome', 'Cidade (*)', array('style'=>'color:black'))}}
                    <select id="cidades" class="form-control">
                        <option value=""></option>
                    </select>
                </div>
                <div class="form-group">
                    {{ Form::label('nome', 'Telefone (*)', array('style'=>'color:black'))}}
                    <input type="text" class="form-control" id="telefone" value="{{ Session::get('telefone') }}" placeholder="Telefone" required="true">
                </div>
                <div class="form-group">
                    {{ Form::label('nome', 'Logradouro (*)', array('style'=>'color:black'))}}
                    <input type="text" class="form-control" id="logradouro" value="{{ Session::get('logradouro') }}" placeholder="Endereço" required="true">
                </div>
                <div class="form-group">
                    {{ Form::label('nome', 'Número (*)', array('style'=>'color:black'))}}
                    <input type="text" class="form-control" id="numero" value="{{ Session::get('numero') }}" placeholder="Número" required="true">
                </div>
                <div class="form-group">
                    {{ Form::label('nome', 'Complemento', array('style'=>'color:black'))}}
                    <input type="text" class="form-control" id="complemento" value="{{ Session::get('complemento') }}" placeholder="Complemento">
                </div>
                <div class="form-group">
                    {{ Form::label('nome', 'Bairro (*)', array('style'=>'color:black'))}}
                    <input type="text" class="form-control" id="bairro" value="{{ Session::get('bairro') }}" placeholder="Bairro" required="true">
                </div>
                <button class="avancar btn btn-success block full-width m-b">Avançar</button>
                <button class="voltar btn btn-primary block full-width m-b">Voltar ao Passo 1</button>
            </form>
        </div>
    </div>
</body>

@include('helpers.spinner')

@section('scripts')

<script type="text/javascript">

    $(document).ready(function(){
        var base_path = "{{url('/')}}";
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        var isMobile = window.matchMedia("only screen and (max-width: 760px)");

        if (! isMobile.matches) {
            $('#cep').mask('99999-999');
            $('#telefone').mask("(99) 9999-9999?9");
        }

        $(document).on("click", ".voltar", function (e) {
            e.preventDefault();

            $(location).attr('href', base_path+'/registrar/passo1');
        });

        $(document).on("change", "#cep", function (e) {
            e.preventDefault();

            var cep = $(this).val().replace('-','');

            if (cep == "") cep = "00000000";

            //$('.sk-spinner').show();
            $('.loading').show();
            $.getJSON(base_path+'/webservice/cep/'+cep)
                    .done(function (data) {
                        var result = JSON.parse(data);
                        if (result && result.resultado != '0') {
                            var endereco = result.tipo_logradouro + ' ' + result.logradouro;
                            var bairro = result.bairro;


                            $('#estados').val(result.uf).change();
                            $('#logradouro').val(endereco);
                            $('#bairro').val(bairro);
                            $('#telefone').focus();

                            setTimeout(function (e) {
                                var id_cidade = $("#cidades option:contains('" + result.cidade + "')").val();
                                if (typeof id_cidade != 'undefined') {
                                    $('#cidades').val(id_cidade).change();
                                }
                            }, 800);
                        }
                        setTimeout(function (e) {
                            $('.loading').hide();
                        }, 1000);
                    });
        });

        $(document).on("change", "#estados", function (e) {
            var estado = $('#estados :selected').val();
            var action = base_path+'/helper/getCidades';

            var data = {
                uf: estado,
                _token: CSRF_TOKEN,
            };

            $.ajax({
                data: data,
                type: 'POST',
                dataType: 'json',
                url: action,
                cache: false,
                success: function(data) {
                    if (data.success) {
                        $('.cidades').show();
                        $('#cidades').empty();

                        $.each(data.cidades, function(index, obj){
                            $('#cidades').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                        });
                    }
                }
            });
        });

        $(document).on("click", ".avancar", function (e) {
            e.preventDefault();

            var action = base_path+'/registrar/passo2';
            var id_cidade = $('#cidades :selected').val();
            var id_estado = $('#estados :selected').val();

            if (id_cidade.length == 0) {
                mensagem('error', 'Você precisa selecionar um estado e uma cidade');
                return false;
            }

            var data = {
                cep: $('#cep').val(),
                id_cidade: id_cidade,
                id_estado: id_estado,
                logradouro: $('#logradouro').val(),
                numero: $('#numero').val(),
                complemento: $('#complemento').val(),
                bairro: $('#bairro').val(),
                telefone: $('#telefone').val(),
                _token: CSRF_TOKEN,
            };

            $('.loading').show();
            $.ajax({
                data: data,
                type: 'POST',
                dataType: 'json',
                url: action,
                cache: false,
                success: function (data) {
                    $('.loading').hide();
                    if (data.success) {
                        if (data.cadastrou) {
                            mensagem('success', 'Cadastro realizado com sucesso!');

                            setTimeout(function() {
                                $(location).attr('href', base_path + data.url);
                            }, 2000);
                        } else {
                            $(location).attr('href', base_path + data.url);
                        }

                    } else {
                        $.each(data.mensagem, function(i, item) {
                            mensagem('error', item);
                        });
                    }
                },
                error: function (data) {
                    $('.loading').hide();
                    mensagem('error', 'Erro ao cadastrar as informações! Verifique os campos do formulário!');
                    return false;
                }
            });
        });
    });
</script>

@show