@extends('layouts.app')

@section('title', 'Sou 4W - Solicitação de Serviço')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{url('/css/lib/conta/cadastro.css')}}" />
<link rel="stylesheet" href="{{url('/css/comum.css')}}" />


<?php
    $estados = App\Models\Estado::orderBy('nome', 'asc')->pluck('nome', 'uf');
?>

<style>
    .bs-wizard {margin-top: 40px;}

    /*Form Wizard*/
    .bs-wizard {border-bottom: solid 1px #e0e0e0; padding: 0 0 10px 0;}
    .bs-wizard > .bs-wizard-step {padding: 0; position: relative;}
    .bs-wizard > .bs-wizard-step + .bs-wizard-step {}
    .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {color: #31708f; font-size: 13px; font-weight: bold; margin-bottom: 5px;}
    .bs-wizard > .bs-wizard-step .bs-wizard-info {color: #999; font-size: 14px;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot {position: absolute; width: 30px; height: 30px; display: block; background: #7eadd6; top: 45px; left: 50%; margin-top: -15px; margin-left: -15px; border-radius: 50%;}
    .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {content: ' '; width: 14px; height: 14px; background: #0c3448; border-radius: 50px; position: absolute; top: 8px; left: 8px; }
    .bs-wizard > .bs-wizard-step > .progress {position: relative; border-radius: 0px; height: 8px; box-shadow: none; margin: 20px 0;}
    .bs-wizard > .bs-wizard-step > .progress > .progress-bar {width:0px; box-shadow: none; background: #4a8fb1;}
    .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {width:100%;}
    .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {width:100%;}
    .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {width:0%;}
    .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {width: 100%;}
    .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {background-color: #f5f5f5;}
    .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {opacity: 0;}
    .bs-wizard > .bs-wizard-step:last-child  > .progress {width: 0%;}
    .bs-wizard > .bs-wizard-step  > .progress {left: 50%; width: 100%;}
    .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot{ pointer-events: none; }

    #aba-1.hidden:after {
        display: none;
    }

    .botao-mercado-pago {
        margin-left: 3px;
    }

    @media screen and (max-width: 1000px) and (min-width: 480px) {
        .spinner {
            width: 380px !important;
            padding: 6px 12px !important;
            font-size: 14px !important;
            height: 25px !important;
            line-height: 1.428571429 !important;
        }
    }

    @media screen and (min-width: 1000px){
        .spinner {
            padding: 6px 12px !important;
            width: 480px !important;
            font-size: 14px !important;
            height: 25px !important;
            line-height: 1.428571429 !important;
        }
    }

    .
</style>


@section('content')
    <br>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="tabs-container">
                    <div class="ibox-title">
                        <div id="abas" class="row bs-wizard" style="border-bottom:0;">
                            <div class="col-xs-3 bs-wizard-step complete">
                                <div class="text-center bs-wizard-stepnum">Necessidade</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <a id="aba-1" data-toggle="tab" href="#tab-1" class="bs-wizard-dot"></a>
                            </div>
                            <div class="col-xs-3 bs-wizard-step">
                                <div class="text-center bs-wizard-stepnum">Localização</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <a id="aba-2" data-toggle="tab" href="#tab-2" class="bs-wizard-dot"></a>
                            </div>
                            <div class="col-xs-3 bs-wizard-step disabled">
                                <div class="text-center bs-wizard-stepnum">Pagamento</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <a id="aba-3" data-toggle="tab" href="#tab-3" class="bs-wizard-dot"></a>
                            </div>
                            <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
                                <div class="text-center bs-wizard-stepnum">Conclusão</div>
                                <div class="progress"><div class="progress-bar"></div></div>
                                <a id="aba-4" href="#" class="bs-wizard-dot"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content">
                    <div id="tab-1" class="tab-pane active">
                        <div class="product-detail">
                            <div class="ibox-content" style="border-top: none; ">
                                <h2 class="text-center font-bold m-b-xs" style="margin-top: -10px;">
                                    <i class="fa fa-pencil"></i>
                                    Descreva o serviço que você deseja
                                </h2>
                                <div id="nova-oportunidade" class="form-horizontal">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                    <input type="hidden" id="is_update" value="0" />
                                    <input type="hidden" id="id_oportunidade" value="<?= ($oportunidade) ? $oportunidade->id : '' ?>">
                                    <input type="hidden" id="id_cupom" value="<?= ($oportunidade) ? $oportunidade->id_cupom : '' ?>">
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        {{ Form::label('servicos', 'Serviço (*)', array('class' => 'col-sm-3 control-label'))}}
                                        <div class="col-sm-6">
                                            <select id="servicos" class="form-control" placeholder="Selecione um serviço" style="width: 100%;"></select>
                                        </div>
                                    </div>
                                    <div class="sugestao form-group" style="display: none;">
                                        <label class="col-sm-3 control-label">Descreva o serviço que você presta</label>
                                        <div class="col-sm-6">
                                            <input type="text" id="sugestao_servico" name="sugestao_servico" class="form-control">
                                        </div>
                                    </div>
                                    <div class="servicos-base form-group" style="display: none">
                                        <label class="col-sm-3 control-label">Serviço</label>
                                        <div class="col-sm-6">
                                            <select id="servicosBase" class="form-control" placeholder="Selecione um serviço" style="width: 100%;"></select>
                                        </div>
                                        <div class="col-sm-3">
                                            <button class="ajuda-servico-base btn btn-circle btn-success">
                                                <i class="fa fa-question" style="color:white"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <div class="adicionais">
                                        <!-- Populado via JS -->
                                    </div>
                                    <div class="hr-line-dashed"></div>
                                    <div class="outros-campos" style="display:none">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Data da execução (*)</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="data_execucao" name="data_execucao" class="form-control" required="true">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"> Hora da execução (*)</label>
                                            <div class="col-sm-6">
                                                <input type="text" id="hora_execucao" name="hora_execucao" class="form-control" required="true">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Informações Adicionais</label>
                                            <div class="col-sm-6">
                                                <textarea id="informacoes_adicionais" name="informacoes_adicionais" class="form-control" style="resize: vertical;" rows="6" required="true"></textarea>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button class="avancar-2 btn btn-success pull-right"> <i class="fa fa-arrow-right"></i> Avançar </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-2" class="tab-pane">
                        <div class="product-detail">
                            <div class="ibox-content" style="border-top: none; ">
                                <h2 class="text-center font-bold m-b-xs" style="margin-top: -10px;">
                                    <i class="fa fa-home" aria-hidden="true"></i>
                                    Informe a localização onde o serviço será realizado
                                </h2>
                                <div id="localizacao" class="form-horizontal">
                                    <div class="hr-line-dashed"></div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-9 col-sm-3">
                                            <a class="mostrar-cupom"> <i class="fa fa-gift"></i> Possui cupom de desconto? </a>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">CEP</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="cep" name="cep" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Estado</label>
                                        <div class="col-sm-8">
                                            <select id="estados" class="form-control" placeholder="Selecione um estado" style="width: 100%;"></select>
                                        </div>
                                    </div>
                                    <div class="cidades form-group" style="display: none;">
                                        <label class="col-sm-3 control-label">Cidade</label>
                                        <div class="col-sm-8">
                                            <select id="cidades" class="form-control" placeholder="Selecione uma cidade" style="width: 100%;">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Endereço (*)</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="logradouro" name="logradouro" class="form-control" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Número (*)</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="numero" name="numero" class="form-control" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Complemento</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="complemento" name="complemento" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Bairro (*)</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="bairro" name="bairro" class="form-control" required="true">
                                        </div>
                                    </div>
                                    <div class="form-group cupom-desconto" style="display:none;">
                                        <label class="col-sm-3 control-label">Cupom de Desconto</label>
                                        <div class="col-sm-8">
                                            <input type="text" id="cupom" name="cupom" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="hr-line-dashed"></div>
                                    <div class="col-md-12">
                                        <button class="avancar-3 btn btn-success pull-right" style="margin-left:3px;"> <i class="fa fa-arrow-right"></i> Avançar </button>
                                        <button class="voltar-1 btn btn-white pull-right"> <i class="fa fa-arrow-left"></i> Voltar </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-pane">
                        <div class="product-detail">
                            <div class="ibox-content" style="border-top: none; ">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h2 class="tipo-servico text-center font-bold m-b-xs" style="margin-top: -20px;"></h2>
                                        <div class="hr-line-dashed"></div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h3><i class="fa fa-pencil"></i> Resumo do Serviço</h3>
                                                <span class="resumo"></span>
                                            </div>
                                            <div class="col-md-6">
                                                <h3><i class="fa fa-plus-circle"></i> Informações Adicionais</h3>
                                                <div class="informacoes_adicionais text-primary"></div>
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <h3><i class="fa fa-calendar"></i> Data de Execução</h3>
                                        <span class="data_execucao"></span>
                                        <div class="hr-line-dashed"></div>
                                        <h3><i class="fa fa-home"></i> Localização</h3>
                                        <dl class="m-t-md">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <dt style="font-size: 12px">Endereço</dt>
                                                    <dd class="logradouro"></dd>
                                                </div>
                                                <div class="col-md-3">
                                                    <dt>Número</dt>
                                                    <dd class="numero"></dd>
                                                </div>
                                                <div class="col-md-3">
                                                    <dt>Complemento</dt>
                                                    <dd class="complemento"></dd>
                                                </div>
                                                <div class="col-md-3">
                                                    <dt>Bairro</dt>
                                                    <dd class="bairro"></dd>
                                                </div>
                                            </div>
                                        </dl>
                                        <div class="hr-line-dashed"></div>
                                        <h2 class="text-right product-main-price"></h2>
                                        <hr>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-6" style="font-size: 14px; font-weight: bold;">
                                                    <div class="ver-termos">
                                                        <input type="checkbox" id="termo-aceite"> Eu concordo com os Termos de Uso
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="hr-line-dashed"></div>
                                            <div class="col-md-12">
                                                <a class="botao-mercado-pago btn btn-success btn-facebook btn-outline pull-right"> REALIZAR O PAGAMENTO
                                                    <img src="{{url('/img/mercado-pago.png')}}" alt="Pague agora" style="height: 30px;">
                                                </a>
                                                <button class="voltar-2 btn btn-white pull-right" style="height: 44px;"> <i class="fa fa-arrow-left"></i> Voltar </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('helpers.spinner')
    <div class="modal inmodal in" id="ajuda" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4 style="color:black" class="modal-title">
                        <i class="fa fa-question-circle"></i> Descrição do Serviço
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="ibox-content">
                        <span class="conteudo-ajuda text-primary" style="font-size: 15px;"> <!-- Populado via Javascript --> </span>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button class="close-modal btn btn-success"> <i class="fa fa-check"></i> Entendi</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal in" id="termos" tabindex="-1">
        <div class="modal-dialog" style="width: 65vw;">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4 style="color:black" class="modal-title">
                        <i class="fa fa-pencil-square-o"></i> Termos de Uso do Site
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="ibox-content" style="border-top: none; overflow-y: scroll; height: 400px;">
                        <div class="section-1">
                            <h3 class="text-center text-primary"> Apresentação da Empresa</h3>
                            <p class="text-primary">Estes Termos de Uso regem seu acesso e uso, dentro do Brasil, de aplicativos, sítios de Internet, conteúdos, serviços disponibilizados pela 4W Ltda., sociedade de responsabilidade limitada, estabelecida no Brasil, com sede na Av Otávio Rocha, 134, salas 31, Centro Histórico, Porto Alegre/RS, CEP 90020-150 e inscrita no Cadastro Nacional de Pessoas Jurídicas do Ministério da Fazenda (CNPJ/MF) sob n. 28.323.465/0001-00 ou qualquer de suas afiliadas.</p>
                        </div>
                        <br>
                        <div class="section-2">
                            <h3 class="text-center text-primary"> Regras de Utilização</h3>
                            <p class="text-primary">
                                O Cadastro realizado neste Site é individual, pessoal e intransferível, não sendo permitido ao Usuário utilizar dados de terceiros ou inverídicos no seu cadastramento;
                                <br>
                                Usuário e senha são responsabilidade do usuário e não deve ser fornecido a terceiros;
                            </p>
                        </div>
                        <br>
                        <div class="section-3">
                            <h3 class="text-center text-primary"> Não é permitido</h3>
                            <ul>
                                <li>
                                    <p class="text-primary">
                                        Realizar quaisquer atos que de alguma forma possam implicar qualquer prejuízo ou dano à 4W Ltda ou a outros Usuários;
                                    </p>
                                </li>
                                <li>
                                    <p class="text-primary">
                                        Acessar áreas de programação do Site, bases de dados ou qualquer outro conjunto de informações que escape às áreas públicas ou restritas do Site;
                                    </p>
                                </li>
                                <li>
                                    <p class="text-primary">
                                        Realizar ou permitir engenharia reversa, traduzir, modificar, alterar a linguagem, compilar, descompilar, modificar, reproduzir, alugar, sublocar, divulgar, transmitir, distribuir, usar ou, de outra maneira, dispor do Site ou das ferramentas e funcionalidades nele disponibilizadas sob qualquer meio ou forma, inclusive de modo a violar direitos e Propriedade Intelectual da 4W Ltda e/ou de terceiros;
                                    </p>
                                </li>
                                <li>
                                    <p class="text-primary">
                                        Interferir na segurança ou cometer usos indevidos contra o Site ou qualquer recurso do sistema, rede ou serviço conectado ou que possa ser acessado por meio do Site, devendo acessar o Site apenas para fins lícitos e autorizados;
                                    </p>
                                </li>
                                <li>
                                    <p class="text-primary">
                                        Utilizar o domínio da 4W Ltda para criar links ou atalhos a serem disponibilizados em e-mails não solicitados (mensagens spam) ou em websites de terceiros ou do próprio Usuário ou, ainda, para realizar qualquer tipo de ação que possa vir a prejudicar a 4W Ltda ou terceiros;
                                    </p>
                                </li>
                                <li>
                                    <p class="text-primary">
                                        Utilizar aplicativos automatizados de coleta e seleção de dados para realizar operações massificadas ou para quaisquer finalidades ou, ainda, para coletar e transferir quaisquer dados que possam ser extraídos do Site para fins não permitidos ou ilícitos.
                                    </p>
                                </li>
                            </ul>
                            <p class="text-primary">
                                O Usuário concorda em indenizar, defender e isentar a 4W Ltda de qualquer reclamação, notificação, intimação ou ação judicial ou extrajudicial, ou ainda de qualquer responsabilidade, dano, custo ou despesa decorrente de qualquer violação e/ou infração cometida pelo Usuário ou qualquer pessoa agindo em seu nome, com seu consentimento ou tolerância, em relação ao Site (inclusive com relação a qualquer disposição destes Termos e Condições de Uso).
                            </p>
                            <p class="text-primary">
                                Da mesma forma, qualquer pessoa que tenha obtido os dados do Usuário relacionados a sua Conta de Acesso ou a sua navegação no Site.
                            </p>
                            <p class="text-primary">
                                A 4W Ltda poderá, a seu exclusivo critério, bloquear, restringir, desabilitar ou impedir o acesso de qualquer Usuário ao Site, total ou parcialmente, sem qualquer aviso prévio, sempre que for detectada uma conduta inadequada do Usuário, sem prejuízo das medidas administrativas, extrajudiciais e judiciais que julgar convenientes.
                            </p>
                        </div>
                        <br>
                        <div class="section-4">
                            <h3 class="text-center text-primary"> Cadastro do Usuário</h3>
                            <p class="text-primary">
                                Para obter acesso ao conteúdo completo e a todas as ferramentas e funcionalidades do Site, incluindo a possibilidade de contratar ou prestar serviços, o Usuário deverá criar uma Conta com dados pessoais, os quais serão armazenados e utilizados para identificação nas transações efetuadas no Site e que também serão usados nos termos da Política de Privacidade e Segurança, ressaltando-se que a criação de cada Conta pressupõe o consentimento expresso sobre coleta, uso, armazenamento e tratamento de dados pessoais pela 4W Ltda e/ou por terceiros por ela contratados para realizar qualquer procedimento ou processo relacionado a prestação de serviços, inclusive processamento de pagamentos.
                            </p>
                            <p class="text-primary">
                                A cada Usuário é permitida a criação de apenas uma Conta de Acesso e a 4W Ltda se reserva o direito de suspender ou cancelar quaisquer Contas de Acesso em duplicidade. Ao completar a sua Conta de Acesso, o Usuário declara que as informações  fornecidas são completas, verdadeiras, atuais e precisas, sendo de total responsabilidade do Usuário a atualização dos dados de sua Conta de Acesso sempre que houver modificação de nome, endereço ou qualquer outra informação relevante. A 4W Ltda poderá recusar, suspender ou cancelar a Conta de Acesso de um Usuário sempre que suspeitar que as informações fornecidas são falsas, incompletas, desatualizadas ou imprecisas ou ainda nos casos indicados nas leis e regulamentos aplicáveis, nestes Termos e Condições ou em qualquer Política do Site, mesmo que previamente aceito. O Usuário, no momento da criação de sua Conta de Acesso, determinará seu nome de usuário e sua senha de acesso. É de exclusiva responsabilidade do Usuário a manutenção do sigilo do nome de usuário e da senha de acesso relativos à sua Conta de Acesso, devendo o Usuário comunicar imediatamente à 4W Ltda em caso de perda, divulgação ou roubo da senha ou ainda de uso não autorizado de sua Conta. Menores (idade inferior a 18 anos) não poderão utilizar o Site.
                            </p>
                        </div>
                        <br>
                        <div class="section-5">
                            <h3 class="text-center text-primary"> Política de Privacidade e Segurança </h3>
                            <p class="text-primary">
                                A 4W Ltda. dispõe de uma política específica para regular a coleta, guarda e utilização de dados pessoais, bem como a sua segurança: Política de Privacidade e Segurança. Essa política integra inseparavelmente estes Termos e Condições de Uso, ressaltando-se que os dados de utilização do Site serão arquivados nos termos da legislação em vigor.
                            </p>
                        </div>
                        <br>
                        <div class="section-6">
                            <h3 class="text-center text-primary"> Oferta de Serviços </h3>
                            <p class="text-primary">
                                A 4W Ltda. disponibiliza a plataforma tecnológica de intermediação dos mais diversos  Serviços pessoais e domiciliares, entre quem necessita contratar e quem executa a prestação dos serviços, permitindo aos Usuários da aplicação móvel ou site web da 4W Ltda. contratar, organizar e agendar Serviços. Desta forma, o Usuário reconhece que a 4W Ltda. não é uma empresa de Prestação de Serviços e que todos os serviços são executados por Prestadores de Serviços Autônomos.
                            </p>
                            <p class="text-primary">
                                Os serviços ofertados no Site estarão restritos às localidades pré-definidas, conforme identificação no Site e periodicamente ampliada de acordo com a demanda.
                            </p>
                            <p class="text-primary">
                                Desta forma, poderá ser ofertado serviços para Usuários de localidades ainda não inclusa na plataforma. Cabendo à 4W Ltda. a seu exclusivo critério, alterar este território, incluindo ou excluindo localidades.
                            </p>
                        </div>
                        <br>
                        <div class="section-7">
                            <h3 class="text-center text-primary"> Requisitos de Conduta do Usuário </h3>
                            <p class="text-primary">
                                Ao utilizar os Serviços, o Usuário não provocará inconvenientes, embaraços, distúrbios ou danos morais, pessoais ou patrimoniais a qualquer das partes.
                            </p>
                            <p class="text-primary">
                                Em determinadas situações, para poder ter acesso ou utilizar os Serviços, poderá ser solicitado ao Usuário que apresente documento de identidade. Caso seja negado a apresentação de documento,  poderá ser negado o acesso ao uso dos Serviços.
                            </p>
                        </div>
                        <br>
                        <div class="section-8">
                            <h3 class="text-center text-primary"> Promoções </h3>
                            <p class="text-primary">
                                A 4W Ltda. pode, ao seu exclusivo critério, criar códigos promocionais passíveis de serem trocados por crédito na Conta ou outras funcionalidades, assim como benefícios relacionados com os Serviços Prestados. Esses códigos promocionais estarão sujeitos a condições adicionais que a 4W Ltda. estabelecer para cada promoção. O Usuário aceita que os Códigos Promocionais: devem ser utilizados pelo público e para a finalidade a que se destinam, e de forma legítima. Não podem, de forma alguma, ser copiados, trocados por dinheiro, vendidos, transferidos ou disponibilizados ao público em geral (seja por divulgação num espaço público ou de outra forma), a menos que expressamente permitido pela 4W Ltda. Estes códigos promocionais poderão ser cancelados em qualquer momento e por qualquer motivo sem aviso prévio, podendo perder sua validade antes de serem utilizados. A 4W Ltda. reserva o direito de reter os benefícios na eventualidade de utilização por engano ou de forma fraudulenta, ilegal ou em violação dos termos aplicáveis do Código Promocional ou dos presentes Termos.
                            </p>
                        </div>
                        <br>
                        <div class="section-9">
                            <h3 class="text-center text-primary"> Conteúdos fornecidos pelo Usuário </h3>
                            <p class="text-primary">
                                A 4W Ltda., ao seu exclusivo critério, poderá permitir ao Usuário submeter, transferir, publicar ou de outra forma disponibilizar conteúdo, informação de texto, audiovisual, incluindo comentários e feedback relacionados com os Serviços. Todo o Conteúdo fornecido pelo Usuário mantém-se propriedade do mesmo. Contudo, ao fornecer Conteúdo, o Usuário está concedendo a 4W Ltda. uma licença mundial, perpétua, irrevogável, transferível, isenta de direitos autorais (“royalties”), para utilizar, copiar, modificar, criar obras derivadas, distribuir, apresentar e executar publicamente. Podendo explorar, sob qualquer modo, tal Conteúdo do Usuário, em todos os formatos e canais de distribuição conhecidos ou futuramente concebidos, sem aviso ou consentimento prévio do Usuário, e sem a necessidade de qualquer pagamento ao mesmo ou a qualquer outra pessoa ou entidade.
                            </p>
                            <p class="text-primary">
                                O Usuário declara que nos termos previstos no presente documento, que o conteúdo fornecido não irá resultar na infração, apropriação indevida ou violação da propriedade intelectual ou dos direitos proprietários de um terceiro, ou dos direitos de publicidade ou privacidade, ou na violação de qualquer lei ou regulamento aplicável.
                            </p>
                            <p class="text-primary">
                                O Usuário compromete-se a não publicar Conteúdo de caráter difamatório, calunioso, violento, obsceno, pornográfico, ilegal ou de outra forma ofensivo ou assim determinado pela 4W Ltda ao seu exclusivo critério. A 4W Ltda.  pode, mas não é obrigada a, rever, controlar ou eliminar Conteúdo de Usuário a qualquer momento e por qualquer motivo e sem pré-aviso.
                            </p>
                        </div>
                        <br>
                        <div class="section-10">
                            <h3 class="text-center text-primary"> Pagamentos </h3>
                            <p class="text-primary">
                                O pagamento será creditado de forma antecipada a Prestação do Serviço, passando a ser devida após a conclusão do serviço. Tendo ocorrido a execução dos serviços a 4W Ltda. facilitará o pagamento em nome do Prestador de Serviços Autônomo, na qualidade de meio de pagamento.
                            </p>
                            <p class="text-primary">
                                Nenhum outro pagamento deverá ser realizado diretamente para o Prestador de Serviços, por nenhum outro meio de pagamento.
                            </p>
                            <p class="text-primary">
                                Este pagamento se refere exclusivamente aos serviços prestados, não estando incluso materiais, insumos, equipamentos necessários à execução dos serviços,  sendo de responsabilidade do Usuário a disponibilização dos mesmos.
                            </p>
                            <p class="text-primary">
                                Nos casos de eventual impossibilidade de conclusão do serviço, o Usuário poderá comunicar a divergência através do e-mail: <strong>atendimento@sou4w.com.br</strong>.
                            </p>
                            <p class="text-primary">
                                O Usuário poderá a qualquer momento solicitar o cancelamento do serviços. Caso este cancelamento ocorra em um horário inferior a 04:00 horas antes do agendamento do serviço, será cobrado o valor referente a taxa de cancelamento.
                            </p>
                        </div>
                        <br>
                        <div class="section-11">
                            <h3 class="text-center text-primary"> Preço </h3>
                            <p class="text-primary">
                                Os preços serão praticados seguindo critérios exclusivamente definidos pela 4W Ltda., considerando fatores como mercado, região, tipo de serviço, tempo de execução, entre outros fatores.
                            </p>
                            <p class="text-primary">
                                Podendo sofrer variações conforme política de preços estabelecida pela 4W Ltda, sendo que a qualquer momento, a 4W Ltda. poderá fornecer a certos Usuários ofertas e descontos promocionais que poderão resultar em valores diferentes para um mesmo serviço cobrado de outros Usuários.
                            </p>
                        </div>
                        <br>
                        <div class="section-12">
                            <h3 class="text-center text-primary"> Garantias </h3>
                            <p class="text-primary">
                                Apesar dos melhores esforços da 4W Ltda. no sentido de fornecer informações precisas, atualizadas, corretas e completas, o Site poderá conter erros técnicos, inconsistências ou erros tipográficos. O Site, seu conteúdo, suas funcionalidades e ferramentas são disponibilizados pela 4W Ltda. tal qual expostos e oferecidos na Internet, sem qualquer garantia, expressa ou implícita, quanto aos seguintes itens:
                                <ul>
                                    <li>
                            <p class="text-primary">
                                atendimento, pelo Site ou por seu conteúdo das expectativas dos Usuários;
                            </p>
                            </li>
                            <li>
                                <p class="text-primary">
                                    continuidade do acesso ao Site ou a seu conteúdo;
                                </p>
                            </li>
                            <li>
                                <p class="text-primary">
                                    adequação da qualidade do Site ou de seu conteúdo para um determinado fim;
                                </p>
                            </li>
                            <li>
                                a correção de defeitos, erros ou falhas no Site ou em seu conteúdo.
                            </li>
                            </ul>
                            </p>
                            <p class="text-primary">
                                A 4W Ltda. se reserva o direito de unilateralmente modificar, a qualquer momento e sem aviso prévio, bem como a configuração, a apresentação, o desenho, o conteúdo, as funcionalidades, as ferramentas ou qualquer outro elemento do Site, inclusive o seu cancelamento.
                            </p>
                            <p class="text-primary">
                                A 4W Ltda não faz nenhuma declaração nem dá garantia sobre a confiabilidade, pontualidade, qualidade, adequação ou disponibilidade dos serviços ou de quaisquer serviços ou bens solicitados por meio do uso do Site, nem que os serviços serão ininterruptos ou livres de erros. A 4W Ltda não garante a qualidade, adequação, segurança ou habilidade de Prestadores de Serviços Autônomos.
                            </p>
                            <p class="text-primary">
                                Você concorda que todo o risco decorrente do uso dos serviços do site e de qualquer serviço ou bem solicitado por meio desta tecnologia será sempre seu, na máxima medida permitida pela lei aplicável.
                            </p>
                        </div>
                        <br>
                        <div class="section-13">
                            <h3 class="text-center text-primary"> Limitação de Responsabilidade </h3>
                            <p class="text-primary">
                                A 4W Ltda. não será responsável por danos indiretos, incidentais, especiais, punitivos ou emergentes, inclusive lucros cessantes, perda de dados, danos morais ou patrimoniais relacionados, associados ou decorrentes de qualquer uso dos serviços. Ainda que a 4W Ltda. tenha sido alertada para a possibilidade desses danos. A 4W Ltda. não será responsável por nenhum dano, obrigação ou prejuízo decorrente do:
                                seu uso dos serviços ou sua incapacidade de acessar ou usar os serviços;
                                qualquer operação ou relacionamento entre você e qualquer Prestador de Serviços Autônomos, ainda que tenhamos sido alertados para a possibilidade desses danos.
                            </p>
                            <p class="text-primary">
                                A 4W Ltda. não será responsável por atrasos ou falhas decorrentes de causas fora do controle razoável da 4W Ltda. e, tampouco, pela qualidade  dos serviços executados por Prestadores de Serviços Autônomos.
                            </p>
                            <p class="text-primary">
                                As limitações e recusa de garantias contidas neste Termo não possuem o objetivo de limitar responsabilidades ou alterar direitos de consumidor que de acordo com a lei aplicável não podem ser limitados ou alterados.
                            </p>
                        </div>
                        <br>
                        <div class="section-14">
                            <h3 class="text-center text-primary"> Direito de Propriedade Intelectual</h3>
                            <br>
                            <h5 class="text-center">Marcas</h5>
                            <ul>
                                <li>
                                    <p class="text-primary">
                                        O Usuário não está autorizado a utilizar, sob qualquer forma ou pretexto, as Marcas, suas reproduções parciais ou integrais ou ainda suas imitações, independentemente da destinação de tal uso.
                                    </p>
                                </li>
                                <li>
                                    <p class="text-primary">
                                        O Usuário não está autorizado a utilizar, sob qualquer forma ou pretexto, as Marcas, suas reproduções parciais ou integrais ou ainda suas imitações, independentemente da destinação de tal uso.
                                    </p>
                                </li>
                                <li>
                                    <p class="text-primary">
                                        O Usuário compromete-se a se abster de fazer qualquer uso das Marcas ou de suas variações (incluindo erros de ortografia ou variações fonéticas) como nome de domínio ou parte de nome de domínio ou em qualquer nome de empresa, de qualquer tipo ou natureza, sob qualquer meio ou forma, inclusive por meio da criação de nomes de domínio ou e-mails. Todas as outras marcas, nomes de produtos, ou nomes de companhias que aparecem no site são de propriedade exclusiva de seus respectivos titulares.
                                    </p>
                                </li>
                            </ul>
                        </div>
                        <br>
                        <div class="section-15">
                            <h3 class="text-center text-primary"> Propriedade do conteúdo </h3>
                            <p class="text-primary">
                                Todo o conteúdo do Site - incluindo o nome de domínio [sou4w.com.br], programas, bases de dados, arquivos, textos, fotos, layouts, cabeçalhos e demais elementos - foi criado, desenvolvido ou cedido à 4W Ltda., é de propriedade da 4W Ltda. ou a ela licenciado e encontra-se protegido pelas leis brasileiras e tratados internacionais que versam sobre direitos de propriedade intelectual.
                            </p>
                        </div>
                        <br>
                        <div class="section-16">
                            <h3 class="text-center text-primary"> Proibição de Utilização </h3>
                            <p class="text-primary">
                                O Usuário, ao acessar o Site, atesta que respeitará a existência e a extensão dos direitos de Propriedade Intelectual da 4W Ltda., bem como de todos os direitos de terceiros que sejam usados, a qualquer título ou que venham a ser disponibilizados no Site.
                            </p>
                            <p class="text-primary">
                                O acesso ao Site e a sua regular utilização pelo Usuário não lhe confere qualquer direito ou prerrogativa sobre o qualquer Propriedade Intelectual, Marca ou outro conteúdo nele inserido. É vedada a utilização, exploração, imitação, reprodução integral ou parcial, de qualquer conteúdo sem a autorização prévia e por escrito da 4W Ltda.. É igualmente vedada a criação de quaisquer obras derivadas de qualquer Propriedade Intelectual da 4W Ltda. sem a autorização prévia e por escrito da 4W Ltda.. É expressamente proibido ao Usuário reproduzir, distribuir, modificar, exibir e criar trabalhos derivados ou qualquer outra forma de utilização de qualquer Propriedade Intelectual ou outro conteúdo deste Site e dos materiais veiculados pelo Site.
                            </p>
                            <p class="text-primary">
                                O Usuário que violar as proibições contidas na legislação sobre propriedade intelectual e nestes Termos e Condições de Uso serão responsabilizados, civil e criminalmente, pelas infrações cometidas. A 4W Ltda. não concede nenhuma autorização relacionada ao conteúdo do Site para qualquer fim. O Usuário assume toda e qualquer responsabilidade pela utilização indevida de qualquer Propriedade Intelectual ou Marcas da 4W Ltda. ou de terceiros, tanto de caráter civil quanto criminal.
                            </p>
                        </div>
                        <br>
                        <div class="section-17">
                            <h3 class="text-center text-primary"> Diversos </h3>
                            <p class="text-primary">
                                Caso a 4W Ltda. não consiga fazer valer ou cumprir qualquer cláusula ou condição contida nestes Termos e Condições de Uso ou nas Políticas, tal fato não configurará desistência, tolerância ou novação dessa cláusula ou condição destes Termos ou de qualquer Política.
                            </p>
                            <p class="text-primary">
                                Se alguma cláusula ou condição contida nestes Termos e Condições de Uso ou nas Políticas for declarada inexequível, no todo ou parcialmente, tal inexequibilidade não afetará as demais cláusulas dos Termos e Condições de Uso e das Políticas. Neste caso, a 4W Ltda. fará as adaptações necessárias para que reflitam, da forma mais próxima possível, os termos da provisão declarada inexequível.
                            </p>
                            <p class="text-primary">
                                A 4W Ltda. tem como princípio de sua atuação nos ambientes físicos e virtuais o respeito ao Usuário, agindo sempre em conformidade com as disposições do Código de Proteção e Defesa do Consumidor (Lei Federal n. 8078/90), do Marco Civil da Internet (Lei Federal n. 12965/14) e das demais normas referentes ao comércio de produtos em ambiente eletrônico.
                            </p>
                            <p class="text-primary">
                                O presente instrumento, bem como o acesso e utilização da plataforma não importam na criação de qualquer vínculo trabalhista, societário, de parceria ou associativo entre as 4W Ltda. e o Usuário.
                            </p>
                            <p class="text-primary">
                                A marcação do “check-box” ao lado do texto "Concordo com os Termos", seguida pelo clique no botão "aceitar" e/ou a utilização da Plataforma por Você são consideradas, para todos os fins de direito, como sua aceitação de todos os termos e condições do presente Termo de Uso, que terá valor de contrato, passando o mesmo a regular a relação entre as 4W Ltda. e Você.
                            </p>
                        </div>
                        <br>
                        <div class="section-18">
                            <h3 class="text-center text-primary"> Lei aplicável e Foro de Eleição</h3>
                            <p class="text-primary">
                                O Site é controlado, operado e administrado pela 4W Ltda. no Brasil, podendo ser acessado por qualquer dispositivo conectado à Internet, independentemente de sua localização geográfica. Em vista das diferenças que podem existir entre as legislações locais e nacionais, ao acessar o Site, o Usuário concorda que a legislação aplicável para fins destes Termos e Condições de Uso será aquela vigente na República Federativa do Brasil. A 4W Ltda. e o Usuário concordam que o Foro Central da Comarca de Porto Alegre, RS, Brasil, será o único competente para dirimir qualquer questão ou controvérsia oriunda ou resultante do uso do Site, renunciando expressamente a qualquer outro, por mais privilegiado que seja, ou venha a ser.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-right">
                                <button class="btn btn-success" data-dismiss="modal"> <i class="fa fa-check"></i> Eu concordo </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        var isMobile = window.matchMedia("only screen and (max-width: 760px)");
        var idCidadesPermitidas = [6953, 7638, 7424, 7066, 7494, 7051, 7670, 7035, 7218, 7669, 7271, 7774, 7267];

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";
            var id_oportunidade = $('#id_oportunidade').val();
            var id_cupom = $('#id_cupom').val();
            var now = new Date();
            var local = new Date(localStorage['hourLastOrder']);

            if (! isMobile.matches) {
                $('#cep').mask('99999-999');
            }

            $('#data_execucao').mask('99/99/9999', {autoclear: false});
            $('#hora_execucao').mask('99:99', {autoclear: false});

            $('#data_execucao').daterangepicker({
                "singleDatePicker": true,
                "autoApply": true,
                "locale": {
                    "applyLabel": 'Confirmar',
                    "cancelLabel": 'Fechar',
                    "format": "DD/MM/YYYY",
                    "separator": " - ",
                    "fromLabel": "From",
                    "toLabel": "To",
                    "customRangeLabel": "Custom",
                    "weekLabel": "S",
                    "daysOfWeek": [
                        "D",
                        "S",
                        "T",
                        "Q",
                        "Q",
                        "S",
                        "S"
                    ],
                    "monthNames": [
                        "Jan",
                        "Fev",
                        "Mar",
                        "Abr",
                        "Mai",
                        "Jun",
                        "Jul",
                        "Ago",
                        "Set",
                        "Out",
                        "Nov",
                        "Dez"
                    ],
                    "firstDay": 1
                },
                "startDate": moment().format('DD/MM/YY'),
            }, function(start, end, label) {
                $('#data_fechamento').val(start.format('YYYY-MM-DD H:mm'));
            });

            $('#hora_execucao').timepicker({
                timeFormat: 'HH:mm',
                interval: 1,
                minTime: '00',
                maxTime: '23:59pm',
                defaultTime: 'now',
                startTime: '00:00',
                dynamic: true,
                dropdown: true,
                scrollbar: true
            });

            mountSelect();

            if (id_cupom != undefined && id_cupom.length) {
                $('#cupom').prop('disabled', true);
            }

            //Verificação se veio do MercadoPago e deve carregar um serviço específico
            if (typeof local != 'undefined' || id_oportunidade.val()) {
                var difference = now - local;
                var diffMin = Math.round(((difference % 86400000) % 3600000) / 60000);

                //Back from MP -- Change to < 1
                if (diffMin < 2) {
                    var lastOrderId = localStorage['lastOrder'];

                    var order = function () {
                        var tmp = null;
                        $.ajax({
                            async: false,
                            type: 'GET',
                            dataType: 'json',
                            url: "/oportunidades/ultimaSolicitacao/" + lastOrderId,
                            success: function(data) {
                                tmp = data;
                            },
                            fail: function(data) {
                                mensagem('error', 'Erro ao buscar o serviço');
                                return false;
                            }
                        });
                        return tmp;
                    }();

                    if (typeof order != 'undefined') {
                        $('#is_update').val(1);

                        $('#servicos').append($("<option></option>").attr("value", order.id_servico).text(order.descricao_servico));
                        $('.servicos-base').show();
                        $('#servicosBase').append($("<option></option>").attr("value", order.id_servico_base).text(order.nome_servico_base));
                        $('#servicosBase').change();
                        $('#data_execucao').val(order.data_execucao);
                        $('#hora_execucao').val(order.hora_execucao);
                        $('#cep').val(order.cep);
                        $('#estados').append($("<option></option>").attr("value", order.id_estado).text(order.nome_estado));
                        $('.cidades').show();
                        $('#cidades').append($("<option></option>").attr("value", order.id_cidade).text(order.nome_cidade));

                        $('#logradouro').val(order.logradouro);
                        $('#numero').val(order.numero);
                        $('#complemento').val(order.complemento);
                        $('#bairro').val(order.bairro);
                    }
                } else {
                    var lastOrderId = id_oportunidade;

                    if (lastOrderId) {
                        var order = function () {
                            var tmp = null;
                            $.ajax({
                                async: false,
                                type: 'GET',
                                dataType: 'json',
                                url: "/oportunidades/ultimaSolicitacao/" + lastOrderId,
                                success: function(data) {
                                    tmp = data;
                                },
                                fail: function(data) {
                                    mensagem('error', 'Erro ao buscar o serviço');
                                    return false;
                                }
                            });
                            return tmp;
                        }();

                        if (typeof order != 'undefined') {
                            $('#is_update').val(1);

                            $('#servicos').append($("<option></option>").attr("value", order.id_servico).text(order.descricao_servico));
                            $('.servicos-base').show();
                            $('#servicosBase').append($("<option></option>").attr("value", order.id_servico_base).text(order.nome_servico_base));
                            $('#servicosBase').change();
                            $('#data_execucao').val(order.data_execucao);
                            $('#hora_execucao').val(order.hora_execucao);
                            $('#cep').val(order.cep);
                            $('#estados').append($("<option></option>").attr("value", order.id_estado).text(order.nome_estado));
                            $('.cidades').show();
                            $('#cidades').append($("<option></option>").attr("value", order.id_cidade).text(order.nome_cidade));

                            $('#logradouro').val(order.logradouro);
                            $('#numero').val(order.numero);
                            $('#complemento').val(order.complemento);
                            $('#bairro').val(order.bairro);
                        }
                    }
                }
            }

            fillData();

            $(document).on("click", ".mostrar-cupom", function () {
                $('.cupom-desconto').show();
            })

            $(document).on("click", ".close-modal", function (e) {
                $('#ajuda').toggle('modal');
            });

            $(document).on("click", ".ajuda-servico-base", function (e) {
                var id_base = $('#servicosBase :selected').val();
                var action = '/servicos/base/buscaDescricao';

                if (!id_base || id_base < 0) {
                    mensagem('error', 'Selecione um tipo de serviço para solicitar mais informações!');
                    return false;
                }

                var data = {
                    id_base: id_base,
                    _token: CSRF_TOKEN
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            if (data.descricao.length > 0) {
                                $('.conteudo-ajuda').html(data.descricao);
                                $('#ajuda').toggle('modal');
                            } else {
                                mensagem('error', 'Este serviço não possui descrição cadastrada!');
                                return false;
                            }
                        }
                    }
                });
            });

            $(document).on("click", "#termo-aceite", function (e) {
                var aceitou_termos = $('#termo-aceite').is(':checked');

                if (aceitou_termos) {
                    $('#termos').modal();
                }
            });

            $(document).on("click", ".botao-mercado-pago", function (e) {
                var aceitou_termos = $('#termo-aceite').is(':checked');

                if (!aceitou_termos) {
                    mensagem('error', 'Você deve aceitar os termos de uso antes de prosseguir!');
                    return false;
                }

                localStorage['hourLastOrder'] = new Date();
            });

            $(document).on("change", "#cep", function (e) {
                e.preventDefault();

                var cep = $(this).val().replace('-','');

                if (cep == "") cep = "00000000";

                swal('Por favor, aguarde!', 'Estamos buscando o endereço através do cep...', "info");
                swal.showLoading();

                $.getJSON(base_path+'/webservice/cep/'+cep)
                .done(function (data) {
                    var result = JSON.parse(data);
                    if (result.resultado != '0') {
                        var endereco = result.tipo_logradouro + ' ' + result.logradouro;
                        var bairro = result.bairro;

                        var data = {
                            uf: result.uf,
                            _token: CSRF_TOKEN,
                        };

                        $.ajax({
                            data: data,
                            type: 'POST',
                            dataType: 'json',
                            url: "/helper/getCidades",
                            cache: false,
                            success: function(data) {
                                $('#estados').empty();
                                $('#estados').append($("<option></option>").attr("value", data.id_estado).text(data.nome_estado));
                                $('#estados').val(data.id_estado).change();
                            }
                        });

                        $('#logradouro').val(endereco);
                        $('#bairro').val(bairro);
                        $('#numero').focus();

                        setTimeout(function (e) {
                            var id_cidade = $("#cidades option:contains('" + result.cidade + "')").val();
                            if (typeof id_cidade != 'undefined') {
                                $('#cidades').val(id_cidade).change();
                            }
                        }, 1500);
                    }

                    setTimeout(function (e) {
                        swal.close();
                    }, 1500);
                });
            });

            $(document).on("click", ".avancar-2", function (e) {
                var data = $('#data_execucao').val();
                var hora = $('#hora_execucao').val();

                if (data.length == 0 || hora.length == 0) {
                    mensagem('error', 'A data e hora do serviço deve ser informada!');
                    return false;
                }

                var adicionais = [];
                var id_base = $('#servicosBase :selected').val();
                var action = base_path+'/oportunidades/calculaValor';

                $('.adicionais .adicional').each(function (i, input) {
                    var obj = new Object();

                    obj.id = $(input).attr('data-id');
                    obj.type = $(input).attr('type');
                    obj.multiple = false;

                    if (obj.type == "text") {
                        obj.value = parseInt($(input).val());
                    } else if (obj.type == "checkbox") {
                        if ($(input).is(':checked')) {
                            obj.value = 1;
                        } else {
                            obj.value = 0;
                        }
                    } else if (obj.type == "radio") {
                        if ($(input).is(':checked')) {
                            obj.value = 1;
                        } else {
                            obj.value = 0;
                        }
                        obj.multiple = true;
                    }
                    adicionais.push(obj);
                });

                var data = {
                    id_base: id_base,
                    adicionais: adicionais,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            localStorage['valor'] = data.valor;
                            mudaTab(1, 2);
                        } else {
                            mensagem('error', 'Ops, erro ao calcular o valor do serviço. Por favor, tente novamente!');
                            return false;
                        }
                    }
                });
            });

            $(document).on("click", ".avancar-3", function (e) {
                e.preventDefault();
                var is_update = $('#is_update').val();
                var id_servico = $('#servicos').val();
                var sugestao = $('#sugestao_servico').val();
                var id_cidade = $('#cidades :selected').val();
                var id_estado = $('#estados :selected').val();
                var cupom = $('#cupom').val();
                var action = (is_update == 1) ? base_path+'/oportunidades/atualizar' : base_path+'/oportunidades/salvar';

                //Caso seja o prestador é necessário escolher um serviço
                if (id_servico.length == 0) {
                    mensagem('error', 'Você deve selecionar um serviço!');
                    return false;
                }

                //Não permite cidades além de porto alegre
                if (! id_cidade) {
                    mensagem('error', 'Voce deve selecionar uma cidade!');
                    return false;
                } else {

                    if (idCidadesPermitidas.indexOf(parseInt(id_cidade)) === -1) {
                        mensagem('error', 'Atualmente estamos operando apenas na região da Grande Porto Alegre!');
                        return false;
                    }

                }

                if (id_servico == 4 && sugestao.length == 0) {
                    mensagem('error', 'Você deve preencher a sugestão ou selecionar algum serviço disponível!');
                    return false;
                }

                var data = {
                    id: (is_update == 1) ? localStorage['lastOrder'] : null,
                    id_servico: id_servico,
                    id_cidade: id_cidade,
                    informacoes_adicionais: $('#informacoes_adicionais').val(),
                    sugestao_servico: sugestao,
                    data_execucao: $('#data_execucao').val(),
                    hora_execucao: $('#hora_execucao').val(),
                    cep: $('#cep').val(),
                    logradouro: $('#logradouro').val(),
                    numero: $('#numero').val(),
                    complemento: $('#complemento').val(),
                    bairro: $('#bairro').val(),
                    cupom: cupom,
                    valor: localStorage['valor'],
                    _token: CSRF_TOKEN,
                };

                swal('Por favor, aguarde!', 'Estamos calculando o valor do serviço...', "info");
                swal.showLoading();

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                }).done(function (data) {
                    localStorage['informacoes_adicionais'] = $('#informacoes_adicionais').val();
                    localStorage['logradouro'] = $('#logradouro').val();
                    localStorage['bairro'] = $('#bairro').val();
                    localStorage['numero'] = $('#numero').val();
                    localStorage['complemento'] = $('#complemento').val();
                    localStorage['data_execucao'] = $('#data_execucao').val();
                    localStorage['hora_execucao'] = $('#hora_execucao').val();
                    localStorage['lastOrder'] = data.id;
                    localStorage['valor'] = data.valor_total;


                    $('.botao-mercado-pago').attr('href', data.mercado_pago);
                    $('.tipo-servico').html("<i class='fa fa-dollar'></i> Contratação de " + data.tipo_servico);
                    $('.resumo').html(data.resumo);

                    mudaTab(2, 3);

                    swal.close();

                }).error(function (err) {
                    swal.close();

                    if (err.hasOwnProperty('responseJSON') && err.responseJSON.hasOwnProperty('erro')) {
                        mensagem('error', err.responseJSON.erro);
                    } else {
                        mensagem('error', 'Erro ao solicitar o serviço! Por favor, tente novamente!');
                    }

                    return false;
                });
            });

            $(document).on("click", ".voltar-1", function (e) {
                mudaTab(2, 1);
            });

            $(document).on("click", ".voltar-2", function (e) {
                mudaTab(3, 2);
                $('#is_update').val(1);
            });
        });

        function mudaTab(currentTab, newTab) {
            var tabFrom = $('#aba-'+currentTab);
            var tabTo = $('#aba-'+newTab);

            if (currentTab < newTab) {
                $(tabFrom).parent().addClass('disabled');
                $(tabFrom).parent().removeClass('complete');

                $(tabTo).parent().addClass('complete');
                $(tabTo).parent().removeClass('disabled');

                $('#aba-'+(newTab+1)).parent().removeClass('disabled');
            } else {
                $(tabFrom).parent().removeClass('complete');

                $(tabTo).parent().addClass('complete');
                $(tabTo).parent().removeClass('disabled');

                $('#aba-'+(currentTab+1)).parent().addClass('disabled');

            }

            if (newTab == 3 || currentTab == 3) {
                fillData();
            }

            $(tabTo).click();
        }

        function fillData() {

            if (localStorage.hasOwnProperty('informacoes_adicionais')) {
                if (localStorage['informacoes_adicionais'].length > 0) {
                    $('.informacoes_adicionais').text(localStorage['informacoes_adicionais']);
                } else {
                    $('.informacoes_adicionais').parent().remove();
                }
            }

            $('.logradouro').text(localStorage['logradouro']);
            $('.bairro').text(localStorage['bairro']);
            $('.numero').text(localStorage['numero']);


            if (localStorage.hasOwnProperty('complemento')) {
                if (localStorage['complemento'].length > 0) {
                    $('.complemento').text(localStorage['complemento']);
                } else {
                    $('.complemento').parent().remove();
                }
            }

            $('.product-main-price').text('Valor Total: R$ ' + parseFloat(localStorage['valor'], 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString());
            $('.data_execucao').text(localStorage['data_execucao'] + " " + localStorage['hora_execucao']);
        }

        function mountSelect() {
            var base_path = "{{url('/')}}";
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $('#servicos').select2({
                placeholder: 'Selecione um Serviço',
                ajax: {
                    url: "/api/servicos/",
                    dataType: 'json',
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                },
            }).on("change", function (e) {
                var id = $(this).find("option:selected").val();
                var value = $(this).find("option:selected").text();

                if (isMobile.matches) {
                    $('.ajuda-servico-base').hide();
                }

                $('.adicionais').empty();
                $('.adicionais').hide();

                if (value == "Outro") {
                    $('.sugestao').show();
                } else {
                    if (id) {
                        $('#sugestao_servico').val('');
                        $('.sugestao').hide();

                        $.ajax({
                            type: 'GET',
                            dataType: 'json',
                            url: "/api/servicos/base",
                            data: {id: id},
                            success: function(data) {
                                $('#servicosBase').empty();

                                if (data.results.length == 0) {
                                    mensagem('error', 'Este serviço não está disponível!');
                                    $('.servicos-base').hide();
                                    return false;
                                }

                                $('.servicos-base').show();
                                $('#servicosBase').append($("<option></option>").attr("value", -1).text("Selecione um serviço base"));
                                $.each(data.results, function(index, obj){
                                    $('#servicosBase').append($("<option></option>").attr("value", obj.id).text(obj.text));
                                });
                            }
                        });
                    } else {
                        $('.servicos-base').hide();
                        $('#servicosBase').empty();

                    }
                }
            });

            $('#servicosBase').select2({
                ajax: {
                    placeholder: "Selecione um serviço base",
                    url: "/api/servicos/base",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            id: $('#servicos :selected').val(),
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                },
            }).on("change", function (e) {
                var id_base = $(this).find("option:selected").val();
                var action = base_path+'/servicos/base/buscaAgravantes';
                var is_update = $('#is_update').val();

                $('.ajuda-servico-base').click();

                $('.adicionais').show();

                var data = {
                    id: (is_update == 1) ? localStorage['lastOrder'] : null,
                    id_base: id_base,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('.adicionais').empty();
                            if (data.fields.length > 0) {
                                $('.adicionais').html(data.fields);
                                $('.spinner').spinner();
                            } else {
                                mensagem('error', 'Este serviço se encontra indisponível no momento!');
                                return false;
                            }
                        }
                        $('.outros-campos').show();
                    }
                });
            });

            $('#cidades').select2({
                ajax: {
                    placeholder: "Selecione uma cidade",
                    url: "/api/cidades",
                    dataType: 'json',
                    delay: 250,
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                },
            }).on("change", function (e) {
                var cidade = $(this).find("option:selected").val();

                if (idCidadesPermitidas.indexOf(parseInt(cidade)) === -1) {
                    mensagem('error', 'Atualmente estamos operando apenas na região da Grande Porto Alegre!');
                    return false;
                }
            });

            $('#estados').select2({
                ajax: {
                    url: "/api/estados",
                    dataType: 'json',
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                },
            }).on("change", function (e) {
                var estado = $(this).find("option:selected").val();
                var action = base_path+'/helper/getCidades';

                //Estado 23 é o Rio Grande do Sul
                if (estado != 23) {
                    mensagem('error', 'Atualmente estamos operando apenas no estado do Rio Grande do Sul!');
                    return false;
                }

                var data = {
                    id: estado,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('.cidades').show();
                            $('#cidades').select2('data', null);

                            $.each(data.cidades, function(index, obj){
                                $('#cidades').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });
            });
        }
    </script>
@endsection