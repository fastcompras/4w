@extends('layouts.app')

@section('title', 'Sou 4W - Histórico de Serviços')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-history"></i>
                Histórico de Serviços
            </h2>
            <div class="hr-line-dashed"></div>
            <p class="text-primary text-danger">
                Histórico dos serviços encerrados ou finalizados que você participou.
            </p>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($oportunidades) > 0) : ?>
                        <table style="overflow-x:auto;" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Solicitante</th>
                                <th>Serviço</th>
                                <th>Valor</th>
                                <th>Status</th>
                                <th>Data</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($oportunidades as $oportunidade): ?>
                                <tr>
                                    <td> {{ $oportunidade->id }}</td>
                                    <td> {{ $oportunidade->usuario->nome }}</td>
                                    <td> {{ $oportunidade->servico->descricao }}</td>
                                    <td> R$ {{ number_format($oportunidade->valor_total, 2, ',', '.') }}</td>
                                    <td> <strong> {{ $oportunidade->getStatus() }} </strong></td>
                                    <td> {{ $oportunidade->data_execucao->format('d/m/Y') }}</td>
                                    <td class="footable-visible footable-last-column">
                                        <a href="{{ url('/oportunidades/visualizar/'.$oportunidade->id) }}">
                                            <button class="btn-success btn btn-xs"><i class="fa fa-search" aria-hidden="true"></i> Visualizar </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?= $oportunidades->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Não há serviços no histórico no momento</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        });
    </script>
@endsection