@extends('layouts.app')

@section('title', 'Sou 4W - Retorno Mercado Pago')

<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="ibox product-detail">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-offset-8 col-md-2">
                                    <button class="btn btn-danger btn-md">Status: Falha no Pagamento</button>
                                </div>
                            </div>
                            <h2 class="tipo-servico text-center font-bold m-b-xs"></h2>
                            <div class="hr-line-dashed"></div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>TRANSAÇÃO #{{$oportunidade->id}}</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Resumo do Serviço</h4>
                                    <span class="resumo"></span>
                                </div>
                                @if ($oportunidade->escopo)
                                <div class="col-md-6">
                                    <h4>Informações Adicionais</h4>
                                    <div class="informacoes_adicionais text-primary">
                                        {{$oportunidade->escopo}}
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="hr-line-dashed"></div>
                            <h4>Data de Execução</h4>
                            <span class="data_execucao">{{date("d/m/Y H:i", strtotime($oportunidade->data_execucao)) }}</span>
                            <div class="hr-line-dashed"></div>
                            <h4>Localização</h4>
                            <dl class="m-t-md">
                                <div class="row">
                                    <div class="col-md-3">
                                        <dt style="font-size: 12px">Endereço</dt>
                                        <dd class="logradouro">{{$oportunidade->logradouro}}</dd>
                                    </div>
                                    <div class="col-md-3">
                                        <dt>Número</dt>
                                        <dd class="numero">{{$oportunidade->numero}}</dd>
                                    </div>
                                    @if (!is_null($oportunidade->complemento))
                                        <div class="col-md-3">
                                            <dt>Complemento</dt>
                                            <dd class="complemento">{{$oportunidade->complemento}}</dd>
                                        </div>
                                    @endif
                                    <div class="col-md-3">
                                        <dt>Bairro</dt>
                                        <dd class="bairro">{{$oportunidade->bairro}}</dd>
                                    </div>
                                </div>
                            </dl>
                            <div class="hr-line-dashed"></div>
                            <h2 class="textproduct-main-price">
                                VALOR TOTAL: R$ {{number_format($oportunidade->valor_total, 2, ',', '.') }}</h2>
                            <hr>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var resumo = <?= json_encode($oportunidade->getResumo()); ?>;
            $('.resumo').html(<?= json_encode($oportunidade->getResumo()) ?>);
        });
    </script>
@endsection