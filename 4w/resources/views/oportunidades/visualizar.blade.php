@extends('layouts.app')

@section('title', 'Sou 4W - Detalhes do Serviço')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{url('/css/lib/servicos/visualizar.css')}}" />


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB08ofvOcnURTKpupoFr67bPypSOZmbkwY"></script>

<style>
    @media screen and (max-width: 600px) {
        .product-images  {
            visibility: hidden;
            clear: both;
            display: none;
        }
    }
</style>

@section('content')
<br>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>
            <i class="fa fa-star"></i> Detalhes da Oportunidade #<?= $oportunidade->id ?>
        </h2>
        <div class="hr-line-dashed"></div>
        <p class="text-primary text-danger">
            Informações sobre a sua oportunidade <strong>#<?= $oportunidade->id ?></strong> referente ao serviço de <strong><?= $oportunidade->servico->descricao ?></strong>.
        </p>
    </div>
</div>
<br/>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="row">
        <div class="col-lg-12">
            <input type="hidden" id="id_oportunidade" data-id="<?=$oportunidade->id?>">
            <div class="ibox">
                <div class="ibox-content" style="border-top:none;">
                    <div class="row">
                        <div class="tabs-container">
                            <ul class="nav nav-tabs" id="oportunidade-abas">
                                <li class="active"><a data-toggle="tab" id="aba-1" href="#tab-1">Detalhe do Serviço</a></li>
                                <li><a data-toggle="tab" href="#tab-2">Encontrar Prestadores</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="ibox product-detail">
                                                    <div class="ibox-content" style="border-top:none;">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="product-images slick-initialized slick-slider" role="toolbar">
                                                                    <div class="mapa-nao-encontrado image-imitation" style="display: none;">
                                                                        <h4> {{ $oportunidade->getEndereco() }} </h4>
                                                                    </div>
                                                                    <div id="map" class="image-imitation">

                                                                    </div>
                                                                    <br/>
                                                                    <a href="" target="_blank" class="text-center como-chegar">
                                                                        <h3>
                                                                            <i class="fa fa-map-marker"></i>
                                                                            Como Chegar?
                                                                        </h3>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-7">
                                                                <h2 class="font-bold m-b-xs">
                                                                    Serviço de {{ $oportunidade->servico->descricao }} (#{{ $oportunidade->id }})
                                                                </h2>
                                                                <i class="fa fa-clock-o"></i>
                                                                <small class="template-oportunidade-data-abertura"><strong>Criada em:</strong> {{ $oportunidade->data_cadastro->format('d/m/Y H:i:s') }} </small>
                                                                <div class="hr-line-dashed"></div>
                                                                <div class="m-t-md">
                                                                    <h4 class="product-main-price"><i class="fa fa-hourglass-half"></i> Status: {{ $oportunidade->getStatus() }}</h4>
                                                                    <h4 class="product-main-price"><i class="fa fa-money"></i> Valor do Serviço: R$ {{ number_format($oportunidade->valor_total, 2, ',', '.') }}</h4>
                                                                    <h4 class="product-main-price"><i class="fa fa-calendar"></i> Data de Execução: {{ $oportunidade->data_execucao->format("d/m/Y H:i:s") }} </h4>
                                                                    <h4 class="product-main-price"><i class="fa fa-user"></i> Solicitante: {{ $oportunidade->usuario->nome }} </h4>
                                                                    <h4 class="product-main-price"><i class="fa fa-comment"></i> Observações: {{ $oportunidade->informacoes_adicionais ?: 'Sem observações' }} </h4>
                                                                </div>
                                                                <div class="hr-line-dashed"></div>
                                                                <h4 class="product-main-price"> <i class="fa fa-pencil-square-o"></i> Descrição do Serviço</h4>
                                                                <div>
                                                                    <p class="template-oportunidade-descricao"></p>
                                                                </div>
                                                                <div class="hr-line-dashed"></div>
                                                                <dl class="m-t-md">
                                                                    <dt style="font-size: 12px"><i class="fa fa-map-marker"></i> Localização</dt>
                                                                    <?php $local = ($oportunidade->cidade) ? $oportunidade->cidade->nome . "/" . $oportunidade->cidade->estado->uf : "Cidade não informada"; ?>
                                                                    <dd class="template-oportunidade-cidade"> {{ $local }} </dd>
                                                                    <br>
                                                                    <dt style="font-size: 12px"><i class="fa fa-home"></i> Endereço</dt>
                                                                    <dd class="template-oportunidade-localizacao"> {{ $oportunidade->getEndereco() }} </dd>
                                                                </dl>
                                                                <div class="hr-line-dashed"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <h3 style="color:black">Filtros</h3>
                                        <br>
                                        <div class="filtros-prestadores">
                                            <form id="nova-oportunidade" class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-1 control-label">Nome</label>
                                                    <div class="col-sm-6">
                                                        <input type="text" id="nome" name="nome" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('servicos', 'Serviço', array('class' => 'col-sm-1 control-label'))}}
                                                    <div class="col-sm-6">
                                                        {{ Form::select('servicos', $servicos, null, array('class' => 'form-control', 'placeholder' => 'Selecione um serviço', 'style' => 'width: 100%;')) }}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {{ Form::label('estados', 'Estado', array('class' => 'col-sm-1 control-label'))}}
                                                    <div class="col-sm-6">
                                                        {{ Form::select('estados', $estados, null, array('id'=>'estados', 'class' => 'form-control', 'placeholder' => 'Selecione um estado', 'style' => 'width: 100%;')) }}
                                                    </div>
                                                </div>
                                                <div class="cidades form-group" style="display:none">
                                                    {{ Form::label('cidades', 'Cidade', array('class' => 'col-sm-1 control-label'))}}
                                                    <div class="col-sm-6">
                                                        <select id="cidades" name="cidade" class="form-control">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="regioes form-group" style="display:none">
                                                    {{ Form::label('regioes', 'Região', array('class' => 'col-sm-1 control-label'))}}
                                                    <div class="col-sm-6">
                                                        <select id="regioes" name="regiao" class="form-control">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="bairros form-group" style="display:none">
                                                    {{ Form::label('bairros', 'Bairro', array('class' => 'col-sm-1 control-label'))}}
                                                    <div class="col-sm-6">
                                                        <select id="bairros" name="bairro" class="form-control">
                                                            <option value=""></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <button class="limpar-filtro btn btn-danger">
                                                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                                                            Limpar Filtro
                                                        </button>
                                                        <button class="filtrar btn btn-success">
                                                            <i class="fa fa-search" aria-hidden="true"></i>
                                                            Filtrar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="wrapper">
                                            <h3 class="text-center text-primary">
                                                Prestadores de Serviço
                                            </h3>
                                            <div class="hr-line-dashed"></div>
                                            <div class="row prestadores">
                                                <?php if ($prestadores) : ?>
                                                    <?php foreach ($prestadores as $prestador) : ?>
                                                    <?php
                                                        $checked1 = ($prestador->media_avaliacoes == 1) ? "checked" : "";
                                                        $checked2 = ($prestador->media_avaliacoes == 2) ? "checked" : "";
                                                        $checked3 = ($prestador->media_avaliacoes == 3) ? "checked" : "";
                                                        $checked4 = ($prestador->media_avaliacoes == 4) ? "checked" : "";
                                                        $checked5 = ($prestador->media_avaliacoes == 5) ? "checked" : "";
                                                    ?>
                                                    <div class="template-prestador col-md-3">
                                                        <div class="contact-box center-version">
                                                            <a class="usuario-info" data-id="<?=$prestador->id_usuario?>">
                                                                <img alt="image" class="img-circle" src="{{url('/')}}{{$prestador->avatar}}">
                                                                <h3 class="m-b-xs"><strong><span style="color:black;" class="template-prestador-nome">{{$prestador->nome}}</span></strong></h3>
                                                                <small>{{$prestador->email}}</small> <br>
                                                                <h4 class="template-prestador-servico font-bold">{{$prestador->servico_prestado}}</h4>
                                                                <div class="hr-line-dashed" style="margin-top: 15px; margin-bottom: 8px"></div>
                                                                <div class="row">
                                                                    <div class="col-xs-offset-1 col-xs-8 col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                                                                        <fieldset class="media-avaliacoes">
                                                                            <input type="radio" <?= $checked5 ?> class="media5" value="5" /><label class = "full" for="media5" title="Perfeito!"></label>
                                                                            <input type="radio" <?= $checked4 ?> class="media4" value="4" /><label class = "full" for="media4" title="Muito Bom!"></label>
                                                                            <input type="radio" <?= $checked3 ?> class="media3" value="3" /><label class = "full" for="media3" title="Médio!"></label>
                                                                            <input type="radio" <?= $checked2 ?> class="media2" value="2" /><label class = "full" for="media2" title="Ruim!"></label>
                                                                            <input type="radio" <?= $checked1 ?> class="media1" value="1" /><label class = "full" for="media1" title="Muito Ruim!"></label>
                                                                        </fieldset>
                                                                    </div>
                                                                </div>
                                                                <div class="hr-line-dashed" style="margin-top: 5px;"></div>
                                                                <address class="m-t-md">
                                                                    <span class="font-bold template-prestador-logradouro">
                                                                        {{ $prestador->logradouro . ', ' . $prestador->numero }}
                                                                    </span>
                                                                    <br>
                                                                    <span class="font-bold  template-prestador-bairro">
                                                                        {{ $prestador->bairro }}
                                                                    </span>
                                                                    <br>
                                                                    <span class="font-bold template-prestador-telefone">
                                                                        {{ $prestador->numero_telefone }}
                                                                    </span>
                                                                </address>
                                                                <h5 class="text-center"><i class="fa fa-calendar"></i> <a href="{{ url('/usuario/agenda/' . $prestador->id_usuario) }}">Ver Agenda</a></h5>
                                                            </a>
                                                            <div class="contact-box-footer">
                                                                <div class="m-t-xs btn-group">
                                                                    @if ($oportunidade->possuiPrestador())
                                                                        @if ($oportunidade->ehPrestadorSelecionado($prestador->id_usuario))
                                                                            <button data-id="{{$prestador->id_usuario}}" class="btn btn-primary"> Selecionado </button>
                                                                        @else
                                                                            <button disabled data-id="{{$prestador->id_usuario}}" class="btn btn-danger"> Profissional encontrado </button>
                                                                        @endif
                                                                    @else
                                                                        @if ($oportunidade->ehCandidato($prestador->id_usuario))
                                                                            @if ($oportunidade->prestadorJaCancelou($prestador->id_usuario))
                                                                                <button data-id="{{$prestador->id_usuario}}" class="btn btn-warning"> Cancelou </button>
                                                                            @elseif ($oportunidade->prestadorRejeitou($prestador->id_usuario))
                                                                                <button title="Clique aqui para visualizar o motivo" data-id="{{$prestador->id_usuario}}" class="motivo-rejeicao btn btn-warning"> Rejeitou </button>
                                                                            @else
                                                                                <button data-id="{{$prestador->id_usuario}}" class="desvincular-prestador btn btn-danger"> Desvincular </button>
                                                                            @endif
                                                                        @else
                                                                            <button data-id="{{$prestador->id_usuario}}" class="selecionar-prestador btn btn-success"> Selecionar</button>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endforeach; ?>
                                                <?php else : ?>
                                                    <h3 class="text-danger text-center">
                                                        <i class="fa fa-thumbs-down"></i>
                                                        Não foram encontrados prestadores
                                                    </h3>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <?php if ($prestadores) : ?>
                                            <div class="row">
                                                <div class="col-md-offset-2 col-md-3"></div>
                                                <?= $prestadores->links(); ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('helpers.spinner')

<!-- Modal com informações resumidas do prestador -->
<div class="modal inmodal in" id="miniDashUsuario" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header" style="padding: 10px 15px;">
                <div class="row">
                    <div class="col-md-2">
                        <img src="" class="avatar img-thumbnail" style="height: 80px; width: 80px;">
                    </div>
                    <div class="col-md-10">
                        <button type="button" class="fechar-modal close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Fechar</span></button>
                        <h4 class="modal-title"></h4>
                        <small class="font-bold">Com este resumo você pode tomar decisões mais precisas na hora de selecionar um profissional</small>
                    </div>
                </div>
            </div>
            <div class="modal-body" style="background: #eee">
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="border-top: none">
                        <h5>
                            <i class="fa fa-tachometer" aria-hidden="true"></i>
                            Dados sobre as Ofertas de Serviço
                        </h5>
                        <div class="ibox-tools">
                            <a data-toggle="collapse" data-target="#ofertas">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div id="ofertas" class="collapse">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-xs-4">
                                    <span class="font-bold text-warning stats-label">Ofertas Pendentes</span>
                                    <h4 class="total-ofertas-pendentes"></h4>
                                </div>

                                <div class="col-xs-4">
                                    <span class="font-bold stats-label">% Todas Ofertas</span>
                                    <h4 class="text-warning pct-ofertas-pendentes"></h4>
                                </div>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-xs-4">
                                    <span class="font-bold stats-label" style="color:green">Ofertas Aceitas</span>
                                    <h4 class="total-ofertas-aceitas"></h4>
                                </div>

                                <div class="col-xs-4">
                                    <span class="font-bold stats-label">% Todas Ofertas Recebidas</span>
                                    <h4 class="pct-ofertas-aceitas" style="color:green"></h4>
                                </div>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-xs-4">
                                    <span class="font-bold text-danger stats-label">Ofertas Recusadas</span>
                                    <h4 class="total-ofertas-recusadas"></h4>
                                </div>

                                <div class="col-xs-4">
                                    <span class="font-bold stats-label">% Todas Ofertas Recebidas</span>
                                    <h4 class="text-danger pct-ofertas-recusadas"></h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="border-top: none">
                        <h5>
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            Últimos Serviços Realizados
                        </h5>
                        <div class="ibox-tools">
                            <a data-toggle="collapse" data-target="#ultimos-servicos">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div id="ultimos-servicos" class="collapse">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-2"><span class="font-bold" style="color:black">Status</span></div>
                                <div class="col-md-2"><span class="font-bold" style="color:black">Data</span></div>
                                <div class="col-md-4"><span class="font-bold" style="color:black">Serviço</span></div>
                                <div class="col-md-2"><span class="font-bold" style="color:black">Bairro</span></div>
                                <div class="col-md-2"><span class="font-bold" style="color:black">Valor</span></div>
                            </div>
                            <div class="hr-line-dashed" style="margin-top: 15px"></div>
                            <div class="tabela-servicos">
                            <!-- Populado via Javascript -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox float-e-margins">
                    <div class="ibox-title" style="border-top: none">
                        <h5>
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            Próximos Serviços
                        </h5>
                        <div class="ibox-tools">
                            <a data-toggle="collapse" data-target="#proximos-servicos">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div id="proximos-servicos" class="collapse">
                        <div class="ibox-content">
                            <div class="row">
                                <div class="col-md-2"><span class="font-bold" style="color:black">Status</span></div>
                                <div class="col-md-2"><span class="font-bold" style="color:black">Data</span></div>
                                <div class="col-md-4"><span class="font-bold" style="color:black">Serviço</span></div>
                                <div class="col-md-2"><span class="font-bold" style="color:black">Bairro</span></div>
                                <div class="col-md-2"><span class="font-bold" style="color:black">Valor</span></div>
                            </div>
                            <div class="hr-line-dashed" style="margin-top: 15px"></div>
                            <div class="tabela-proximos-servicos">
                                <!-- Populado via Javascript -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="fechar-modal btn btn-white" data-dismiss="modal">Fechar</button>
                <button data-id="" class="selecionar-prestador btn btn-success" style="display: none;">
                    Selecionar
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Template da listagem dos últimos serviços -->
<div class="hide template-ultimos-servicos">
    <div class="row servico-info" data-id="">
        <div class="col-xs-2">
            <span class="font-bold label label-danger servico-status"></span>
        </div>
        <div class="col-xs-2">
            <span class="font-bold text-primary servico-data"></span>
        </div>
        <div class="col-xs-4">
            <span class="font-bold text-primary servico-descricao"></span>
        </div>
        <div class="col-xs-2">
            <span class="font-bold text-primary servico-bairro"></span>
        </div>
        <div class="col-xs-2">
            <span class="font-bold text-primary servico-valor">R$ 0,00</span>
        </div>
        <hr>
    </div>
</div>

<!-- Template da listagem dos próximos serviços -->
<div class="hide template-proximos-servicos">
    <div class="row proximo-servico-info" data-id="">
        <div class="col-xs-2">
            <span class="font-bold label label-warning proximo-servico-status"></span>
        </div>
        <div class="col-xs-2">
            <span class="font-bold text-primary proximo-servico-data"></span>
        </div>
        <div class="col-xs-4">
            <span class="font-bold text-primary proximo-servico-descricao"></span>
        </div>
        <div class="col-xs-2">
            <span class="font-bold text-primary proximo-servico-bairro"></span>
        </div>
        <div class="col-xs-2">
            <span class="font-bold text-primary proximo-servico-valor">R$ 0,00</span>
        </div>
        <hr>
    </div>
</div>

<!-- Template da listagem dos últimos serviços -->
<div class="hide template-sem-servicos">
    <h4 class="sem-servico text-center text-danger">Este profissional não finalizou nenhum serviço recentemente.</h4>
    <h4 class="sem-proximo-servico text-center text-danger">Este profissional não possui serviços nos próximos dias.</h4>
</div>
@endsection

@section('scripts')
    <script>
        var servicos;
        var proximos_servicos;
        var sem_servicos;
        var sem_proximos_servicos;

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            servicos = $('.servico-info').clone();
            proximos_servicos = $('.proximo-servico-info').clone();
            sem_servicos = $('.sem-servico').clone();
            sem_proximos_servicos = $('.sem-proximo-servico').clone();

            $('.servico-info').remove();
            $('.proximo-servico-info').remove();
            $('.sem-servico').remove();
            $('.sem-proximo-servico').remove();

            $('.template-oportunidade-descricao').html(<?= json_encode(nl2br($oportunidade->getResumo())) ?>);

            getCoordinatesFromAddress();

            $(document).on("click", "#aba-1", function (e) {
                getCoordinatesFromAddress();
            });

            $('#oportunidade-abas a').click(function(e) {
                e.preventDefault();
                $(this).tab('show');
            });

            // store the currently selected tab in the hash value
            $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
                var id = $(e.target).attr("href").substr(1);
                window.location.hash = id;
            });

            // on load of the page: switch to the currently selected tab
            var hash = window.location.hash;
            $('#oportunidade-abas a[href="' + hash + '"]').tab('show');

            $(document).on("click", ".motivo-rejeicao", function (e) {
                var id_prestador = $(this).attr('data-id');
                var id_oportunidade = $('#id_oportunidade').attr('data-id');

                var data = {
                    id_prestador: id_prestador,
                    id_oportunidade: id_oportunidade,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/oportunidades/motivosRejeicao',
                    cache: false,
                    success: function (response) {
                        mensagem('success', 'Motivo: ' + response.motivo);
                    },
                    error: function (response) {
                        console.log(response);
                    }
                });
            });

            $(document).on("click", ".selecionar-prestador", function (e) {
                var id_prestador = $(this).attr('data-id');
                var id_oportunidade = $('#id_oportunidade').attr('data-id');

                var action = base_path+'/oportunidades/selecionarPrestador';

                var data = {
                    id_prestador_selecionado: id_prestador,
                    id_oportunidade: id_oportunidade,
                    _token: CSRF_TOKEN,
                };

                //Fecha o modal caso a seleção seja por lá
                if ($('#miniDashUsuario').is(':visible')) {
                    $('#miniDashUsuario').hide();
                }

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function (response) {
                        $('.loading').hide();

                        mensagem('success', 'Prestador selecionado com sucesso!');

                        setTimeout(function() {
                            location.reload();
                        }, 800);
                    },
                    error: function (response) {
                        $('.loading').hide();

                        switch (response.status) {
                            case 409:
                                mensagem('error', 'Não é possível vincular o criador da oportunidade como prestador!');
                                break;
                            case 404:
                            case 422:
                            case 500:
                            default:
                                mensagem('error', 'Erro ao selecionar o prestador! Por favor, tente novamente!');
                                return false;
                        }
                    }
                });
            });

            $(document).on("click", ".goback", function (e) {
                history.back(1);
            });

            $(document).on("click", ".desvincular-prestador", function (e) {
                var id_prestador = $(this).attr('data-id');
                var id_oportunidade = $('#id_oportunidade').attr('data-id');

                var action = base_path+'/oportunidades/desvincularPrestador';

                var data = {
                    id_prestador_selecionado: id_prestador,
                    id_oportunidade: id_oportunidade,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        $('.loading').hide();
                        if (data.success) {
                            mensagem('success', 'Prestador desvinculado com sucesso!');

                            setTimeout(function() {
                                location.reload();
                            }, 1500);
                        } else {
                            mensagem('error', 'Erro ao desvincular o prestador!');
                        }
                    }
                });
            });

            $(document).on("click", ".usuario-info", function (e) {
                var id_usuario = $(this).attr('data-id');
                var id_oportunidade = $('#id_oportunidade').attr('data-id');
                var action = base_path+'/usuario/buscaResumo';

                var data = {
                    id: id_usuario,
                    id_oportunidade: id_oportunidade,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        $('.loading').hide();
                        if (data.success) {
                            $('#miniDashUsuario').show();
                            montaInfo(data.data);
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });

            $(document).on("change", "#estados", function (e) {
                var estado = $('#estados :selected').val();
                var action = base_path+'/helper/getCidades';

                var data = {
                    uf: estado,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('#cidades').empty();

                            $('.cidades').show();

                            $.each(data.cidades, function(index, obj){
                                $('#cidades').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });
            });

            $(document).on("change", "#cidades", function (e) {
                var cidade = $('#cidades :selected').val();
                var action = base_path+'/helper/getRegioesPorCidade';

                var data = {
                    id_cidade: cidade,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('#regioes').empty();

                            if (data.regioes.length > 0) {
                                $('#regioes').append($("<option>Selecione uma região da cidade</option>").val(''));

                                $.each(data.regioes, function(index, obj){
                                    $('#regioes').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                                });

                                $('.regioes').show();
                            } else {
                                $('.regioes').hide();
                                $('.bairros').hide();
                                $('#bairros').empty();
                            }
                        }
                    }
                });
            });

            $(document).on("click", "#regioes", function (e) {
                var regioes = $('#regioes :selected').val();
                var actionBairros = base_path+'/helper/getBairrosPorRegiao';

                var data = {
                    id_regiao: regioes,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: actionBairros,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('#bairros').empty();

                            if (data.bairros.length > 0) {
                                $('#bairros').append($("<option>Selecione um bairro</option>").val(''));

                                $.each(data.bairros, function(index, obj){
                                    $('#bairros').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                                });

                                $('.bairros').show();
                            } else {
                                $('.bairros').hide();
                            }
                        }
                    }
                });
            });

            $(document).on("click", ".fechar-modal ", function (e) {
                var modal = $(this).closest('.modal').attr('id');

                $('#'+modal).hide();
            });
        });

        function montaInfo(data) {
            var base_path = "{{url('/')}}";
            var modal = $('#miniDashUsuario');

            if (!data.eh_candidato) {
                modal.find('.selecionar-prestador').show();
            }

            modal.find('.modal-title').text('Resumo de Atividades de ' + data.nome_usuario);
            modal.find('.avatar').attr('src', base_path+data.avatar_usuario);
            modal.find('.selecionar-prestador').attr('data-id', data.id_usuario);

            modal.find('.total-ofertas-pendentes').text(data.total_ofertas_pendentes);
            modal.find('.pct-ofertas-pendentes').text(data.pct_ofertas_pendentes + "%");

            modal.find('.total-ofertas-aceitas').text(data.total_ofertas_aceitas);
            modal.find('.pct-ofertas-aceitas').text(data.pct_ofertas_aceitas + "%");

            modal.find('.total-ofertas-recusadas').text(data.total_ofertas_recusadas);
            modal.find('.pct-ofertas-recusadas').text(data.pct_ofertas_recusadas + "%");

            addServicos(data.servicos);
            addProximosServicos(data.proximos_servicos);
        }

        function addServicos(data) {
            if (data.length == 0) {
                var el = sem_servicos.clone();

                $('.tabela-servicos').html(el);
            } else {
                $('.tabela-servicos').find('.sem-servico').remove();
                for (var item in data) {
                    addServico(data[item]);
                }
            }
        }

        function addProximosServicos(data) {
            if (data.length == 0) {
                var el = sem_proximos_servicos.clone();

                $('.tabela-proximos-servicos').html(el);
            } else {
                $('.tabela-proximos-servicosservicos').find('.sem-proximo-servico').remove();
                for (var item in data) {
                    addProximoServico(data[item]);
                }
            }
        }

        function addProximoServico(data) {
            var el = proximos_servicos.clone();

            el.attr('data-id', data.id);
            el.find('.proximo-servico-status').text(data.status);
            el.find('.proximo-servico-descricao').text(data.servico);
            el.find('.proximo-servico-bairro').text(data.bairro);
            el.find('.proximo-servico-data').text(data.data_execucao);

            $('.tabela-proximos-servicos').append(el);
        }

        function addServico(data) {
            var el = servicos.clone();

            el.attr('data-id', data.id);
            el.find('.servico-status').text(data.status);
            el.find('.servico-descricao').text(data.servico);
            el.find('.servico-bairro').text(data.bairro);
            el.find('.servico-data').text(data.data_execucao);

            $('.tabela-servicos').append(el);
        }

        function getCoordinatesFromAddress() {
            var address = <?= json_encode($oportunidade->getEnderecoParaMapa()) ?>;
            var endPoint = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=" + address;

            $('#map').empty();
            $.getJSON(endPoint, function(result){

                if (result.status == "OK") {
                    var latitude = result.results[0].geometry.location.lat;
                    var longitude = result.results[0].geometry.location.lng;

                    var map = new GMaps({
                        div: '#map',
                        lat: latitude,
                        lng: longitude
                    });

                    map.addMarker({
                        lat: latitude,
                        lng: longitude,
                        title: "Endereço do Serviço",
                        click: function(e) {
                            mensagem('success', address)
                        }
                    });

                    var $pluslink = address.replace(/\s+/g, '+');
                    $('.como-chegar').attr("href", 'https://maps.google.com/maps?q=' + $pluslink);

                } else {
                    $('#map').hide();
                    $('.mapa-nao-encontrado').show();
                    $('.como-chegar').attr("href", 'https://maps.google.com/maps?q=' + address);
                }
            });
        }
    </script>
@endsection