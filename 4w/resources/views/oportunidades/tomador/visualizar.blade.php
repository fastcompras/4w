@extends('layouts.app')

@section('title', 'Detalhe da Oportunidades')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{url('/css/lib/servicos/tomador/visualizar.css')}}" />


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB08ofvOcnURTKpupoFr67bPypSOZmbkwY"></script>

<style>
    @media screen and (max-width: 600px) {
        .product-images, .status{
            visibility: hidden;
            clear: both;
            display: none;
        }
    }
</style>

@section('content')
<br>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <input type="hidden" id="id_oportunidade" data-id="<?=$oportunidade->id?>">
        <h2>
            <i class="fa fa-star"></i> Detalhes da Oportunidade #<?= $oportunidade->id ?>
        </h2>
        <div class="hr-line-dashed"></div>
        <p class="text-primary text-danger">
            Informações sobre a sua oportunidade <strong>#<?= $oportunidade->id ?></strong> referente ao serviço de <strong><?= $oportunidade->servico->descricao ?></strong>.
        </p>
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox product-detail">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="product-images slick-initialized slick-slider" role="toolbar">
                                <div class="mapa-nao-encontrado image-imitation" style="display: none;">
                                    <h4> {{ $oportunidade->getEndereco()  }} </h4>
                                </div>
                                <div id="map" class="image-imitation">

                                </div>
                                <br/>
                                <a href="" target="_blank" class="text-center como-chegar">
                                    <h3>
                                        <i class="fa fa-map-marker"></i>
                                        Como Chegar?
                                    </h3>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row">
                                <div class="col-md-9">
                                    <h2 class="font-bold m-b-xs">Serviço de {{ $oportunidade->servico->descricao }} (#{{ $oportunidade->id }}) </h2>
                                </div>
                            </div>
                            <i class="fa fa-clock-o"></i>
                            <small class="template-oportunidade-data-abertura"><strong>Criada em:</strong> {{ $oportunidade->data_cadastro->format('d/m/Y H:i:s') }} </small>
                            <div class="hr-line-dashed"></div>
                            <div class="m-t-md">
                                <h4 class="product-main-price"><i class="fa fa-hourglass-half"></i> Status: <strong> {{ $oportunidade->getStatus() }}</strong></h4>
                                <h4 class="product-main-price"><i class="fa fa-money"></i> Valor do Serviço: R$ {{ number_format($oportunidade->valor_total, 2, ',', '.') }}</h4>
                                <h4 class="product-main-price"><i class="fa fa-calendar"></i> Data de Execução: {{ $oportunidade->data_execucao->format("d/m/Y H:i:s") }} </h4>
                                @if ($oportunidade->status == \App\Models\Oportunidade::STATUS_ENCERRADO)
                                    <h4 class="product-main-price"><i class="fa fa-calendar-times-o"></i> Data do Encerramento: {{$oportunidade->data_execucao->format("d/m/Y H:i:s") }} </h4>
                                @endif
                                @if ($oportunidade->prestador)
                                    <h4 class="product-main-price"><i class="fa fa-user"></i> Prestador: {{ $oportunidade->prestador->nome }} </h4>
                                @endif
                                <h4 class="product-main-price"><i class="fa fa-comment"></i> Observações: {{ $oportunidade->informacoes_adicionais ?: 'Sem observações' }} </h4>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <h4 class="product-main-price"> <i class="fa fa-pencil-square-o"></i> Descrição do Serviço</h4>
                            <div>
                                <p class="template-oportunidade-descricao"></p>
                            </div>
                            <div class="hr-line-dashed"></div>
                            <dl class="m-t-md">
                                <dt style="font-size: 12px"><i class="fa fa-map-marker"></i> Localização</dt>
                                <?php $local = ($oportunidade->cidade) ? $oportunidade->cidade->nome . "/" . $oportunidade->cidade->estado->uf : "Cidade não informada"; ?>
                                <dd class="template-oportunidade-cidade">{{$local}}</dd>
                                <br>
                                <dt style="font-size: 12px"><i class="fa fa-home"></i> Endereço</dt>
                                <dd class="template-oportunidade-localizacao">{{$oportunidade->getEndereco() }}</dd>
                            </dl>
                            <div class="hr-line-dashed"></div>
                            <div>
                                <div class="btn-group">
                                    @if ($usuario->solicitouServico($oportunidade->id) && !$oportunidade->foiAvaliada(\App\Models\AvaliacaoServico::DESTINATARIO_PRESTADOR) && $oportunidade->status == \App\Models\Oportunidade::STATUS_ENCERRADO)
                                        <div class="col-md-4">
                                            <button class="avaliar btn btn-sm btn-success">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                Avaliar Serviço
                                            </button>
                                        </div>
                                    @elseif ($usuario->solicitouServico($oportunidade->id) && (!in_array($oportunidade->status, [1,5,6])))
                                        <div class="col-md-6">
                                            <button data-id="{{ Auth::user()->id }}" class="cancelar-servico btn btn-sm btn-danger">
                                                <i class="fa fa-thumbs-down" aria-hidden="true"></i>
                                                Cancelar Serviço
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@include('helpers.spinner')

<div class="modal inmodal in" id="avaliarServico" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-star"></i> Avaliação do Serviço</h4>
                <small class="font-bold">Caso ache necessário você pode deixar um comentário sobre o serviço recebido</small>
            </div>
            <div class="modal-body" style="background: #eee">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Dê uma nota ao serviço recebido</label>
                            <small>(1 significa "Péssimo" e 5 significa "Sensacional")</small>
                            <br>
                            <fieldset class="rating">
                                <input type="radio" id="star5" name="rating" value="5" /><label class = "full" for="star5" title="Perfeito!"></label>
                                <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Muito Bom!"></label>
                                <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Médio!"></label>
                                <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Ruim!"></label>
                                <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Muito Ruim!"></label>
                            </fieldset>
                        </div>
                        <br><br>
                        <div class="form-group">
                            <label>Comentários</label>
                            <textarea rows="5" style="resize:none" class="comentarios-avaliacao-servico form-control"></textarea>
                        </div>
                    </div>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="button" class="fechar-modal btn btn-white" data-dismiss="modal">Fechar</button>
                <button class="avaliar-servico btn btn-success"><i class="fa fa-reply"></i> Avaliar</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $('.template-oportunidade-descricao').html(<?= json_encode(nl2br($oportunidade->getResumo())) ?>);

            getCoordinatesFromAddress();

            $(document).on("click", ".cancelar-servico", function (e) {
                var id_oportunidade = $('#id_oportunidade').attr('data-id');

                var data = {
                    id_oportunidade: id_oportunidade,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/oportunidades/cancelarSolicitacaoServico',
                    cache: false,
                    success: function(data) {
                        $('.loading').hide();
                        if (data.success) {
                            mensagem('success', data.msg);

                            setTimeout(function() {
                                $(location).attr('href', base_path+'/home');
                            }, 1500);
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });

            $(document).on("click", ".avaliar-servico", function (e) {
                var id_oportunidade = $('#id_oportunidade').attr('data-id');
                var comentarios = $('.comentarios-avaliacao-servico').val();
                var action = base_path+'/oportunidades/avaliarServico';
                var nota = 1;

                $('.rating :input').each(function(index,element) {
                    if ($(element).is(':checked')) {
                        nota = $(element).val();
                    }
                });

                var data = {
                    id_oportunidade: id_oportunidade,
                    comentarios: comentarios,
                    nota: nota,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $('#avaliarServico').toggle();

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        $('.loading').hide();
                        if (data.success) {
                            mensagem('success', data.msg);

                            setTimeout(function() {
                                location.reload();
                            }, 1500);
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });

            $(document).on("click", ".avaliar", function (e) {
                $('#avaliarServico').show();
            });

            $(document).on("click", ".fechar-modal ", function (e) {
                var modal = $(this).closest('.modal').attr('id');

                $('#'+modal).hide();
            });
        });

        function getCoordinatesFromAddress() {
            var address = <?= json_encode($oportunidade->getEnderecoParaMapa()) ?>;
            var endPoint = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=" + address;

            $.getJSON(endPoint, function(result){

                if (result.status == "OK") {
                    var latitude = result.results[0].geometry.location.lat;
                    var longitude = result.results[0].geometry.location.lng;

                    var map = new GMaps({
                        div: '#map',
                        lat: latitude,
                        lng: longitude
                    });

                    map.addMarker({
                        lat: latitude,
                        lng: longitude,
                        title: "Endereço do Serviço",
                        click: function(e) {
                            mensagem('success', address)
                        }
                    });

                    var $pluslink = address.replace(/\s+/g, '+');
                    $('.como-chegar').attr("href", 'https://maps.google.com/maps?q=' + $pluslink);

                } else {
                    $('#map').hide();
                    $('.mapa-nao-encontrado').show();
                    $('.como-chegar').attr("href", 'https://maps.google.com/maps?q=' + address);
                }
            });
        }
    </script>
@endsection