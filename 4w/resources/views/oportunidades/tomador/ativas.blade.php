@extends('layouts.app')

@section('title', 'Serviços Agendados')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-thumbs-up"></i>
                Serviços Agendados
            </h2>
            <div class="hr-line-dashed"></div>
            <p class="text-primary text-danger">
                Listagem dos serviços solicitados e que estão agendados para execução.
            </p>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($oportunidades) > 0) : ?>
                        <table style="overflow-x:auto;" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Prestador</th>
                                <th>Serviço</th>
                                <th>Valor</th>
                                <th>Data</th>
                                <th>Bairro</th>
                                <th>Status</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($oportunidades as $oportunidade): ?>
                                <tr>
                                    <td> {{ $oportunidade->id }} </td>
                                    <td> <strong>{{ $oportunidade->prestador->nome }} </strong> </td>
                                    <td> {{ $oportunidade->servico->descricao }} </td>
                                    <td> R$ {{ number_format($oportunidade->valor_total, 2, ',', '.') }} </td>
                                    <td> {{ date("d/m/Y H:i", strtotime($oportunidade->data_execucao)) }} </td>
                                    <td> {{ $oportunidade->bairro }} </td>
                                    <td> <strong> {{ $oportunidade->getStatus() }} </strong> </td>
                                    <td class="footable-visible footable-last-column">
                                        <button title="Reagendar Serviço" data-id="{{ $oportunidade->id }}" class="reagendar btn-primary btn btn-circle btn-xs"><i class="fa fa-calendar"></i></button>
                                        <a title="Detalhes do Serviço" href="{{ url('/oportunidades/visualizar/'.$oportunidade->id) }}">
                                            <button class="btn-success btn-circle btn btn-xs">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?= $oportunidades->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Você não possui serviços ativos neste momento.</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal para Reagendamento de Serviço -->
    <div class="modal inmodal in" id="reagendarServico" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-calendar"></i> Reagendamento do Serviço</h4>
                    <small class="font-bold">Após a solicitação de reagendamento será necessário encontrar algum profissional disponível.</small>
                </div>
                <div class="modal-body" style="background: #eee">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Nova data de Execução</label>
                                <input type="text" value="" class="data-reagendamento form-control">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="fechar-modal btn btn-white" data-dismiss="modal">Fechar</button>
                    <button class="solicitar-reagendamento btn btn-success">
                        <i class="fa fa-check"></i> Solicitar
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $('.data-reagendamento').mask('99/99/9999 99:99', {autoclear: false});

            $(document).on("click", ".reagendar", function (e) {
                var id = $(this).attr('data-id');

                $('.solicitar-reagendamento').attr('data-id', id);
                $('#reagendarServico').show();
            });

            $(document).on("click", ".fechar-modal ", function (e) {
                var modal = $(this).closest('.modal').attr('id');

                $('#'+modal).hide();
            });

            $(document).on("click", ".solicitar-reagendamento", function (e) {
                var id = $(this).attr('data-id');
                var data_execucao = moment($('.data-reagendamento').val(), "DD-MM-YYYY H:i:s");

                if (!data_execucao || !data_execucao.isValid()) {
                    mensagem('error', 'Você deve selecionar uma data para execução do serviço!');
                    return false;
                }

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                    data_execucao: data_execucao.format('YYYY-MM-DD HH:mm:ss')
                };


                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/oportunidades/reagendar',
                    cache: false,
                    success: function (data) {
                        $('#reagendarServico').hide();

                        mensagem('success', 'Solicitação de reagendamento concluída com sucesso!');

                        setTimeout(function () {
                            location.reload();
                        }, 600);
                    },
                    fail: function () {
                        mensagem('error', 'Erro ao remover a solicitação de serviço! Por favor, tente novamente!');
                        return false;
                    }
                });

            });

        });
    </script>
@endsection