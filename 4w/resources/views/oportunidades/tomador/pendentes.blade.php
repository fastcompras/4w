@extends('layouts.app')

@section('title', 'Serviços Sem Prestador de Serviço')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{url('/css/comum.css')}}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-exclamation-triangle"></i>
                Serviços Pendentes
            </h2>
            <div class="hr-line-dashed"></div>
            <p class="text-primary text-danger">
               Listagem dos serviços solicitados que estão pendentes de pagamento ou que ainda não foram encontrados profissionais para executá-los.
            </p>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($oportunidades) > 0) : ?>
                        <table style="overflow-x:auto;" class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Serviço</th>
                                <th>Valor</th>
                                <th>Bairro</th>
                                <th>Data</th>
                                <th>Status</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($oportunidades as $oportunidade): ?>
                                <tr>
                                    <td> {{ $oportunidade->id }}</td>
                                    <td> {{ $oportunidade->servico->descricao }}</td>
                                    <td> R$ {{ number_format($oportunidade->valor_total, 2, ',', '.') }}</td>
                                    <td> {{ $oportunidade->bairro}}</td>
                                    <td> {{ date("d/m/Y H:i", strtotime($oportunidade->data_execucao)) }}</td>
                                    <td> <strong> {{ $oportunidade->getStatus() }} </strong></td>
                                    <td class="footable-visible footable-last-column">
                                        <a title="Detalhes do Serviço" href="{{ url('/oportunidades/visualizar/'.$oportunidade->id)}}">
                                            <button class="btn-success btn btn-circle btn-xs"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </a>
                                        <a title="Editar Serviço" href="{{ url('/oportunidades/editar/'.$oportunidade->id)}}">
                                            <button class="btn-warning btn btn-circle btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                                        </a>
                                        <button title="Remover Serviço" data-id="{{ $oportunidade->id }}" class="remover btn-danger btn btn-circle btn-xs"><i class="fa fa-times" aria-hidden="true"></i></button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?= $oportunidades->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Você não possui serviços pendentes neste momento.</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    @include('helpers.spinner')
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".remover", function (e) {
                var id = $(this).attr('data-id');
                var action = base_path+'/oportunidades/remover';

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'DELETE',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function (data) {
                        mensagem('success', 'Solicitação de serviço removida com sucesso!');

                        setTimeout(function () {
                            location.reload();
                        }, 600);
                    },
                    fail: function () {
                        mensagem('error', 'Erro ao remover a solicitação de serviço! Por favor, tente novamente!');
                        return false;
                    }
                });
                $('.loading').hide();
            });
        });
    </script>
@endsection