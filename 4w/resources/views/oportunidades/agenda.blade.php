@extends('layouts.app')

@section('title', 'Sou 4W - Agenda dos Próximos Serviços')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
    .file-box {
        width: 270px !important;
    }
</style>

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-calendar"></i>
                Minha Agenda
            </h2>
            <div class="hr-line-dashed"></div>
            <p class="text-primary text-danger">
                Veja no calendários os serviços que você necessita realizar.
            </p>
        </div>
    </div>
    <br/>
    <div class="row animated fadeInDown">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div id="calendar" class="fc fc-unthemed fc-ltr">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var events = <?= json_encode($oportunidades) ?>

            $('#calendar').fullCalendar({
                events: events,
                eventRender: function(event, element) {
                    element.find('.fc-title').append("<hr>" + event.description);
                }
            });


        });

    </script>
@endsection