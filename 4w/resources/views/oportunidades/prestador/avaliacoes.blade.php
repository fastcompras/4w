@extends('layouts.app')

@section('title', 'Sou 4W - Avaliações Recebidas')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{url('/css/lib/servicos/prestador/avaliacoes.css')}}" />

@section('content')
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="wrapper wrapper-content animated fadeInRight">

                <div class="ibox-content m-b-sm border-bottom">
                    <div class="p-xs">
                        <div class="pull-left m-r-md">
                            <i class="fa fa-star mid-icon" style="color:yellow"></i>
                        </div>
                        <h2>Avaliações Recebidas</h2>
                        <span class="text-navy">As avaliações recebidas são um dos atribuitos mais importantes na denominação de um prestador de serviço a uma determinada oportunidade</span>
                    </div>
                </div>

                <div class="ibox-content forum-container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-3">
                                    <h3>Minha Média</h3>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="media-avaliacoes">
                                        <input type="radio" class="media5" value="5" /><label class = "full" for="media5" title="Perfeito!"></label>
                                        <input type="radio" class="media4" value="4" /><label class = "full" for="media4" title="Muito Bom!"></label>
                                        <input type="radio" class="media3" value="3" /><label class = "full" for="media3" title="Médio!"></label>
                                        <input type="radio" class="media2" value="2" /><label class = "full" for="media2" title="Ruim!"></label>
                                        <input type="radio" class="media1" value="1" /><label class = "full" for="media1" title="Muito Ruim!"></label>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-4">
                                    <h5>
                                        <i class="fa fa-thumbs-down" style="color:red"></i> Nota menor que 3
                                    </h5>
                                </div>
                                <div class="col-md-3">
                                    <h5>
                                        <i class="fa fa-warning" style="color:orange"></i> Nota 3
                                    </h5>
                                </div>
                                <div class="col-md-4">
                                    <h5>
                                        <i class="fa fa-thumbs-up" style="color:green"></i> Nota maior que 3
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <h3 class="text-center">Lista das Últimas Avaliações</h3>
                    <div class="hr-line-dashed"></div>
                    <div class="avaliacoes">
                        <!-- Template via JavaScript -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="hide template-avaliacoes">
        <div class="avaliacao-id forum-item" data-id="">
            <div class="row">
                <div class="col-md-6">
                    <div class="avaliacao-icone forum-icon">
                        <i class="fa fa-warning"></i>
                    </div>
                    <a class="avaliacao-servico forum-item-title"></a>
                    <div class="avaliacao-endereco forum-sub-title"></div>
                </div>
                <div class="col-md-4 forum-info">
                    <fieldset class="rating">
                        <input type="radio" class="star5" value="5" /><label class = "full" for="star5" title="Perfeito!"></label>
                        <input type="radio" class="star4" value="4" /><label class = "full" for="star4" title="Muito Bom!"></label>
                        <input type="radio" class="star3" value="3" /><label class = "full" for="star3" title="Médio!"></label>
                        <input type="radio" class="star2" value="2" /><label class = "full" for="star2" title="Ruim!"></label>
                        <input type="radio" class="star1" value="1" /><label class = "full" for="star1" title="Muito Ruim!"></label>
                    </fieldset>
                </div>
                <div class="col-md-2 forum-info">
                    <button class="abrir-comentario btn-success btn btn-md">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        Ver Comentário
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal in" id="modalVerComentarioAvaliacao" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <i class="fa fa-check modal-icon" style="color:green"></i>
                    <h4 class="modal-title">Avaliação do Serviço</h4>
                    <small class="font-bold">Veja o comentário que você recebeu sobre a prestação deste serviço</small>
                </div>
                <div class="modal-body" style="background: #eee">
                    <div class="row well">
                        <div class="col-md-2">
                            <img alt="image" class="avatar-usuario-avaliacao img-circle" src="" style="width: 45px">
                        </div>
                        <div class="col-md-10">
                            <i class="fa fa-quote-left" aria-hidden="true"></i>
                            <span class="comentario-avaliacao" style="font-size:15px">

                            </span>
                            <i class="fa fa-quote-right" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="fechar-modal btn btn-white" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        var avaliacoes;
        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";
            var nota = <?= Auth::user()->getMediaAvaliacoes(); ?>;
            var classe = "media";

            if (nota > 0) {
                classe += nota;
                $('.'+classe).attr('checked', true);
            }

            avaliacoes = $('.avaliacao-id');
            $('.avaliacao-id').remove();

            addAvaliacoes();

            $(document).on("click", ".abrir-comentario", function (e) {
                var id = $(this).attr('data-id');
                var action = base_path+'/oportunidades/buscaComentarioAvaliacao';

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('.comentario-avaliacao').text(data.comentario);
                            $('.avatar-usuario-avaliacao').attr('src', data.avatar);

                            $('#modalVerComentarioAvaliacao').show();
                        }
                    }
                });
            });

            $(document).on("click", ".fechar-modal ", function (e) {
                var modal = $(this).closest('.modal').attr('id');

                $('#'+modal).hide();
            });
        });

        function addAvaliacoes() {
            var base_path = "{{url('/')}}";
            $.getJSON(base_path+"/usuario/avaliacoes", function( data ) {
                if (data.length) {
                    for (var item in data) {
                        addAvaliacao(data[item]);
                    }
                } else {
                    $('.avaliacoes').append("<h3 class='text-danger text-center'> Você não possui avaliações disponíveis");
                }

            });
        }

        function addAvaliacao(data) {
            var element = avaliacoes.clone();
            element.attr('data-id', data.id);

            element.find('.avaliacao-servico').text(data.servico);
            element.find('.avaliacao-endereco').text(data.endereco);
            element.find('.star'+data.nota).attr('checked',true);

            if (data.nota > 3) {
                element.find('.avaliacao-icone').find('i').attr('class', 'fa fa-thumbs-up').attr('style','color:green');
            } else if (data.nota < 3) {
                element.find('.avaliacao-icone').find('i').attr('class', 'fa fa-thumbs-down').attr('style','color:red');
            } else {
                element.find('.avaliacao-icone').find('i').attr('class', 'fa fa-warning').attr('style','color:orange');
            }

            element.find('.abrir-comentario').attr('data-id', data.id);

            $('.avaliacoes').append(element);
        }

    </script>
@endsection