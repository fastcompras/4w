@extends('layouts.app')

@section('title', 'Sou 4W - Serviços Oferecidos')

<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-star"></i>
                Serviços Oferecidos
            </h2>
            <div class="hr-line-dashed"></div>
            <p class="text-primary text-danger">
                Veja abaixo a lista de serviços que selecionamos para você! Confirme antes que seja tarde.
            </p>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($oportunidades) > 0) : ?>
                    <table style="overflow-x:auto;" class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Solicitante</th>
                            <th>Serviço</th>
                            <th>Valor</th>
                            <th>Bairro</th>
                            <th>Data</th>
                            <th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($oportunidades as $oportunidade): $op = \App\Models\Oportunidade::find($oportunidade->id_oportunidade); ?>
                        <tr>
                            <td> {{ $op->id }} </td>
                            <td> {{ $op->usuario->nome }} </td>
                            <td> {{ $op->servico->descricao }} </td>
                            <td> R$ {{ number_format($op->valor_total, 2, ',', '.') }}</td>
                            <td> {{ $op->bairro }}</td>
                            <td> {{ $op->data_execucao->format('d/m/Y') }}</td>
                            <td class="footable-visible footable-last-column">
                                <a href="{{ url('/oportunidades/visualizar/'.$op->id) }}">
                                    <button class="btn-success btn btn-circle btn-xs">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <?= $oportunidades->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Você não possui nenhum serviço oferecido neste momento.</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        });
    </script>
@endsection