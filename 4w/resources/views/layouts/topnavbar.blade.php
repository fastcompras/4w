<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="show-notifications dropdown-toggle count-info" data-toggle="dropdown" href="#">
                    <i class="fa fa-bell"></i>  <span class="label label-danger">{{count(Auth::user()->notificacoes)}}</span>
                </a>
                <ul class="all-notifications dropdown-menu dropdown-messages" style="width: 350px;">
                    <!-- Filled by Javascript -->
                </ul>
            </li>
            <li>
                <a href={{url('/logout')}}>
                    <i class="fa fa-sign-out"></i> Sair
                </a>
            </li>
        </ul>
    </nav>

    <div class="template-notifications hide">
        <li class="notification" data-id="">
            <div class="dropdown-messages-box">
                <a class="pull-left" style="padding-left: 0px; padding-top: 0px;">
                    <img alt="image" class="img-circle" src="/img/bell.png">
                </a>
                <div class="media-body">
                    <span class="notification-message"></span>
                    <span class="notification-link">
                        <a style="padding:0px; font-size:12px;">Clique aqui</a> para visualizar.
                    </span>
                    <small class="notification-date text-muted"></small>
                </div>
            </div>
            <div class="hr-line-dashed" style="margin-top: 10px; margin-bottom: 10px;"></div>
        </li>

        <li class="empty-notification">
            <div class="dropdown-messages-box">
                <a class="pull-left" style="padding-left: 0px; padding-top: 0px;">
                    <img alt="image" class="img-circle" src="/img/denied.png">
                </a>
                <div class="media-body">
                    <span class="empty-notification-message">
                        Você não possui novas notificações. Para visualizar todas as notificações
                        <a href="/notificacoes/todas" style="padding:0px; font-size:12px;">Clique aqui</a>.
                    </span>
                </div>
            </div>
        </li>

        <li class="see-more-notification">
            <div class="text-center link-block">
                <a href="{{url('/notificacoes/todas')}}">
                    <i class="fa fa-envelope"></i> <strong>Ver todas as notificações</strong>
                </a>
            </div>
        </li>
    </div>

</div>
<script type="text/javascript">

    var notifications_template = $('.notification').clone();
    var empty_notification_template = $('.empty-notification').clone();
    var see_more_template = $('.see-more-notification').clone();

    $(document).ready(function(){
        var base_path = "{{url('/')}}";
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        var notification = notifications_template.clone();
        var empty_notification = empty_notification_template.clone();
        var see_more = see_more_template.clone();

        notifications_template.remove();
        empty_notification_template.remove();
        see_more_template.remove();

        $.get(base_path + '/unreadNotifications', function (response){
            addNotifications(response.notifications);
        });

        $(document).on("click", ".show-notifications", function (e) {
            e.preventDefault();
            var notifications = [];

            $('.all-notifications > .notification').each(function(i, li) {
                var id = $(li).attr('data-id');

                notifications.push(id);
            });

            var data = {
                notifications: notifications,
                _token: CSRF_TOKEN,
            };

            $.ajax({
                data: data,
                type: 'POST',
                url: base_path + '/notifications/read',
                cache: false,
                success: function (response) {
                    console.log("all notifications has been read");
                },
                error: function (response) {
                    console.log(response.msg);
                }
            });
        });

        function addNotifications(data) {
            var see_more_notifications = see_more.clone();

            if (data.length == 0) {
                var empty = empty_notification.clone();

                $('.all-notifications').html(empty);

                return false;
            }

            for (var item in data) {
                addNotification(data[item]);
            }

            $('.all-notifications').append(see_more_notifications);
        }

        function addNotification(data) {
            var info = notification.clone();

            info.attr('data-id', data.id);
            info.find('.notification-message').text(data.mensagem);
            info.find('.notification-date').text(toDatetime(data.data_cadastro));

            if (data.link) {
                info.find('.notification-link').find('a').attr('href', data.link);
            }

            $('.all-notifications').append(info);
        }
    });
</script>
