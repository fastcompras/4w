<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Sou 4W</title>
    @yield('htmlheader')

    <link rel="stylesheet" href="{{url('/css/vendor.css')}}" />
    <link rel="stylesheet" href="{{url('/css/toastr.min.css')}}" />
    <link rel="stylesheet" href="{{url('/css/app.css')}}" />
    <link rel="stylesheet" href="{{url('/css/comum.css')}}" />
    <link rel="stylesheet" href="{{url('/css/plugins/jasny/jasny-bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{url('/css/plugins/calendar/fullcalendar.min.css')}}" />
    <link rel="stylesheet" href="{{url('/js/plugins/jquery-ui/jquery-ui.css')}}" />
    <link rel="stylesheet" href="{{url('/js/plugins/fullcalendar/fullcalendar.min.css')}}" />
    <link rel="stylesheet" href="{{url('/css/plugins/crop/cropper.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.18.0/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <script src="{{url('/js/app.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/crop/src/js/cropper.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/functions.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/sweet/sweetalert.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/jquery-mask/jquery.maskedinput.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/jquery-mask-money/jquery.maskMoney.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/jasny/jasny-bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/fullcalendar/lib/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{url('/js/plugins/fullcalendar/locale/pt-br.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/locale/br.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
</head>

<body>

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('layouts.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg">

            <!-- Page wrapper -->
            @include('layouts.topnavbar')

            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            {{--@include('layouts.footer')--}}

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->


@section('scripts')

  <script type="text/javascript">
      $(document).ready(function(){
          var base_path = "{{url('/')}}";
          var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

          verificaDocumentacaoPendente(CSRF_TOKEN);

      });

      function verificaDocumentacaoPendente(token) {
          var base_path = "{{url('/')}}";
          var action = base_path+'/usuario/verificaDocumentacaoPendente';

          $.ajax({
              data: {_token: token},
              type: 'POST',
              dataType: 'json',
              url: action,
              cache: false,
              success: function (response) {

                  if (response.documentacao_pendente && response.usuario_pendente) {
                      mensagem('error', 'Você possui documentos pendentes!', 3000);
                  }

              }
          });
      }

  </script>
@show

</body>
</html>
