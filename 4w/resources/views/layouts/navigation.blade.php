<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <span>
                        <img alt="image" class="avatar img-circle" src="{{url('/')}}{{Auth::user()->getAvatar()}}" style="height: 54px; width: 54px">
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle">
                        <span class="block m-t-xs"> <strong class="font-bold"> {{ Auth::user()->nome }}</strong></span>
                        <span class="text-muted text-xs block">{{ Auth::user()->perfil->descricao }}<b class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        @if (!Auth::user()->isPrestador())
                            <li><a href="{{ url('/conta/editar/') }}">Editar Conta</a></li>
                        @endif
                        <li><a href="{{ url('/conta/perfil/'.Auth::user()->id) }}">Meu Perfil</a></li>
                        <li><a href="{{url('/logout')}}">Sair</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    4W
                </div>
            </li>
            <li class="">
                <a href="{{url('/home')}}"><i class="fa fa-tachometer"></i> <span class="nav-label">Início</span></a>
            </li>
            <li class="">
                <a href="#"><i class="fa fa-user"></i> <span class="nav-label">Minha Conta</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" style="height: 0px;">
                    @if (Auth::user()->isPrestador())
                        @if (Auth::user()->temDocumentosPendentes())
                            <li>
                                <a href="{{url('/conta/documentosPendentes')}}">
                                    Docs Pendentes
                                </a>
                            </li>
                        @endif
                    @else
                    <li>
                        <a href="{{ url('/conta/editar/') }}">
                            <span class="nav-label">Editar Conta</span>
                        </a>
                    </li>
                    @endif
                    <li><a href="{{ url('/conta/perfil/'.Auth::user()->id) }}">Meu Perfil</a></li>
                </ul>
            </li>
            <?php if (Auth::user()->isPrestador()) : ?>
            <li class="">
                <a href="#"><i class="fa fa-star"></i> <span class="nav-label">Meus Serviços</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" style="height: 0px;">
                    <li>
                        <a href="{{ url('/oportunidades/avaliacoes') }}">Minhas Avaliações</a>
                    </li>
                    <li>
                        <a href="{{ url('/oportunidades/pendentesConfirmacao') }}">Oportunidades</a>
                    </li>
                    <li>
                        <a href="{{ url('/oportunidades/historico') }}">Histórico de Serviços</a>
                    </li>
                    <li>
                        <a href="{{ url('/conta/agenda') }}">Agenda</a>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if (!Auth::user()->isAdmin()) : ?>
            <li class="">
                <a href="#"><i class="fa fa-star"></i> <span class="nav-label">Quero Contratar</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" style="height: 0px;">
                    <li>
                        <a href="{{url('/oportunidades/solicitar')}}">
                            Solicitar Serviço
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            Serviços Solicitados
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level collapse" style="height: 0px;">
                            <li>
                                <a href="{{ url('/oportunidades/agendados') }}">Serviços Agendados</a>
                            </li>
                            <li>
                                <a href="{{ url('/oportunidades/pendentes') }}">Serviços Pendentes</a>
                            </li>
                            <li>
                                <a href="{{ url('/oportunidades/historico') }}">Histórico de Serviços</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
            <?php if (Auth::user()->isAdmin() || /* Desenvolvedor */ Auth::user()->email === 'guilherme@fastcompras.com.br') : ?>
                <li class="">
                    <a href="#"><i class="fa fa-star"></i> <span class="nav-label">Serviços</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse" style="height: 0px;">
                        <li>
                            <a href="{{ url('/oportunidades/') }}">Lista de Oportunidades</a>
                        </li>
                    </ul>
                </li>
                <li class="">
                <a href="#"><i class="fa fa-lock"></i> <span class="nav-label">Administração</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse" style="height: 0px;">
                    <li>
                        <a href="#">
                            <i class="fa fa-male" aria-hidden="true"></i>
                            Perfis
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level collapse" style="height: 0px;">
                            <li>
                                <a href="{{ url('/cadastros/perfilConta') }}">Cadastrar Perfil</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            Serviços
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level collapse" style="height: 0px;">
                            <li>
                                <a href="{{ url('/servicos/novo') }}">Cadastrar Serviço</a>
                            </li>
                            <li>
                                <a href="{{ url('/servicos/') }}">Lista de Serviços</a>
                            </li>
                            <li>
                                <a href="{{ url('/servicos/sugeridos') }}">Serviços Sugeridos</a>
                            </li>
                        </ul>
                    </li>
                    <li class="">
                        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Usuários</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse" style="height: 0px;">
                            <li>
                                <a href="#">
                                    Prestadores
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="nav nav-third-level collapse">
                                    <li>
                                        <a href="{{ url('/usuarios/prestadores/') }}">Aprovados</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/usuarios/prestadores/pendentes') }}">Pendentes</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/usuarios/prestadores/reprovados') }}">Reprovados</a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ url('/usuarios/tomadores/') }}">Usuários</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-home" aria-hidden="true"></i>
                            Regiões e Bairros
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level collapse" style="height: 0px;">
                            <li>
                                <a href="{{ url('/regioes/novo') }}">Cadastrar Região</a>
                            </li>
                            <li>
                                <a href="{{ url('/bairros/novo') }}">Cadastrar Bairro</a>
                            </li>
                            <li>
                                <a href="{{ url('/regioes/') }}">Regiões</a>
                            </li>
                            <li>
                                <a href="{{ url('/bairros/') }}">Bairros</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-usd" aria-hidden="true"></i>
                            Tabela de Preços
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level collapse" style="height: 0px;">
                            <li>
                                <a href="{{ url('/servicos/preco/base/novo') }}">Cadastrar Base</a>
                            </li>
                            <li>
                                <a href="{{ url('/servicos/preco/base/') }}">Lista de Serviços Base</a>
                            </li>
                            <li>
                                <a href="{{ url('/servicos/preco/agravante/novo') }}">Cadastrar Agravantes</a>
                            </li>
                            <li>
                                <a href="{{ url('/servicos/preco/agravante/') }}">Lista de Agravantes</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-usd" aria-hidden="true"></i>
                            Financeiro
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level collapse" style="height: 0px;">
                            <li>
                                <a href="{{ url('/financeiro/configuracao') }}">Configuração</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="fa fa-trash" aria-hidden="true"></i>
                            Motivos de Rejeição
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level collapse" style="height: 0px;">
                            <li>
                                <a href="{{ url('/motivos-rejeicao/') }}">Lista de Motivos</a>
                            </li>
                            <li>
                                <a href="{{ url('/motivos-rejeicao/novo') }}">Cadastrar Motivo</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</nav>