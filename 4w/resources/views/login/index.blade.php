
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sou 4W - {{ $title }} </title>

    <link rel="stylesheet" href="css/vendor.css" />
    <link rel="stylesheet" href="css/app.css" />
    <script src="{{url('/js/app.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">

    <style>

        body {
            background: url('/img/fundo-min.jpg') no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        @media screen and (max-width: 1024px) { /* Specific to this particular image */
            img.bg {
                left: 50%;
                margin-left: -512px;   /* 50% */
            }
        }
    </style>
</head>
<body>
    <div class="middle-box text-center loginscreen animated fadeInDown">

        <div class="row">
            <div class="col-md-12">
                <img src="https://www.sou4w.com.br/homepage/assets/logo.png" style="max-width: 100px; max-height: 100px;" title="Realizar Login na 4W" alt="">
            </div>
        </div>
        <br>
        <div class="ibox-content" style="margin-top: 0px;">

            <?php if ($errors->any()) : ?>
            <div class="alert alert-danger alert-dismissable">
                O endereço de e-mail ou a senha estão incorretos. Por favor, tente novamente!
            </div>
            <?php endif; ?>

            <form class="m-t" role="form" method="post" action="{{url('/login')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="E-mail" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" autocomplete="current-password" placeholder="Senha" required="">
                </div>
                <div class="buttons" style="padding-bottom: 30px;">
                    <button type="submit" class="btn btn-success btn-sm pull-right">Fazer Login</button>
                </div>
            </form>
            <div class="hr-line-dashed"></div>
            <div class="links" style="margin-bottom: 5px;">
                <a href="{{url('/registrar/passo1?perfil=?')}}"><small>Cadastre-se | </small></a>
                <a href="/registrar/recuperar-senha"><small>Perdeu a senha?</small></a>
            </div>
            <a href="https://www.sou4w.com.br"> <small> <i class="fa fa-arrow-left"></i> Voltar para 4W </small> </a>
        </div>
    </div>

</body>
</html>

<script>
    var counter = 0;
    var backgrounds = [
            '{{ url('/img/01-min.png') }}',
            '{{ url('/img/02-min.png') }}',
            '{{ url('/img/03-min.png') }}',
            '{{ url('/img/04-min.png') }}',
            '{{ url('/img/05-min.png') }}'
    ];

    function changeBackground() {
        counter = (counter + 1) % backgrounds.length;
        $('body').css('background', 'url('+backgrounds[counter]+') no-repeat center center fixed');
        $('body').css('background-size', 'cover');
        setTimeout(changeBackground, 4000);
    }

    //changeBackground();

</script>