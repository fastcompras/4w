<script type="text/x-template" id="input-password">
    <div class="form-group field">
        <label v-if="label.length"> {{ label }} </label>
        <div>
            <input type="password"
                   class="form-control"
                   :required="required"
                   :maxlength="maxLength"
                   v-model="model">
        </div>
    </div>
</script>

<script>

    Vue.component('input-password', {
        template: '#input-password',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            label: {
                type: String,
                required: false,
            },
            value: {
                type: String,
                required: false,
            },
            maxLength: {
                type: Number,
                required: false,
                default: 255,
            }
        },
        computed: {
            model: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', newValue);
                }
            },
        }
    });

</script>