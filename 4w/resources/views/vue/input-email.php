<script type="text/x-template" id="input-email">
    <div class="form-group field">
        <label v-if="label.length"> {{ label }} </label>
        <div>
            <input type="email"
                   class="form-control" :class="error ? 'error' : ''"
                   :required="required"
                   :maxlength="maxLength"
                   v-model="model">
        </div>
    </div>
</script>

<script>

    Vue.component('input-email', {
        template: '#input-email',
        props: {
            required: {
                type: Boolean,
                required: false
            },
            label: {
                type: String,
                required: false
            },
            value: {
                type: String,
                required: false
            },
            maxLength: {
                type: Number,
                required: false,
                default: 255
            }
        },
        data: function () {
            return {
                error: false
            }
        },
        computed: {
            model: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                    this.error = ! regex.test(newValue);

                    this.$emit('input', newValue);
                    this.$emit('error', this.error);
                }
            },
        },

    });

</script>