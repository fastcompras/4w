<script type="text/x-template" id="input-number">
    <div class="form-group field">
        <label v-if="label.length"> {{ label }} </label>
        <div>
            <input type="number"
                   class="form-control"
                   :min="min"
                   :max="max"
                   :step="step"
                   :required="required"
                   v-model.number.lazy="model">
        </div>
    </div>

</script>

<script>
    Vue.component('input-number', {
        template: '#input-number',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            label: {
                type: String,
                required: false,
            },
            value: {
                type: [Number, String],
                required: false,
            },
            min: {
                type: [Number, String],
                required: false,
            },
            max: {
                type: [Number, String],
                required: false,
            },
            step: {
                type: Number,
                required: false,
                default: 1
            },
        },
        computed: {
            model: {
                get: function () {
                    return this.value;
                },
                set: function (newValue) {

                    if (newValue == null) {
                        this.$emit('input', null);
                    } else {
                        this.$emit('input', newValue);
                    }

                }
            },
        },
        watch: {
            model: function (newValue) {

                if (newValue < parseInt(this.min) || newValue.length == 0) {
                    this.model = this.min;
                }

                if (newValue > parseInt(this.max)) {
                    this.model = this.max;
                }

            }
        }
    });
</script>

