<script type="text/x-template" id="input-select-servico">
    <div class="form-group">
    <label v-if="label.length" class="pull-left"> {{ label }} </label>
    <br>
    <select v-model="selected" :multiple="multiple" class="form-control select-4w">
        <option value="null" selected disabled> {{ placeholder || 'Selecione um serviço' }} </option>
        <option v-for="option in options" :value="option.id"> {{ option.text }} </option>
    </select>
</div>
</script>

<script>
    Vue.component('input-select-servico', {
        template: '#input-select-servico',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            placeholder: {
                type: String,
                required: false,
                default: null,
            },
            multiple: {
                type: Boolean,
                required: false,
                default: false,
            },
            label: {
                type: String,
                required: false,
            },
            value: {
                type: [Number, Array],
                required: false,
            }
        },
        data: function () {
            return {
                options: null,
            }
        },
        computed: {
            selected: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', newValue);
                }
            },
        },
        created: function () {
            let vm = this;

            axios
                .get('/api/servicos')
                .then(function (response) {
                    vm.options = response.data.results;
                });

        }
    });
</script>

<style scoped>

    select option[data-default] {
        color: #888;
    }

</style>