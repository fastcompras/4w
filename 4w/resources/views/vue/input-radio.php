<script type="text/x-template" id="input-radio">
    <div class="form-group field">
        <label v-if="label.length"> {{ label }} </label>
        <div class="choice radio" v-for="item in items">
            <input type="radio" v-model="selected" key="item.id" :value="item.id"> {{ item.description }}
        </div>
    </div>
</script>

<script>

    Vue.component('input-radio', {
        template: '#input-radio',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            label: {
                type: String,
                required: false,
            },
            items: {
                type: Array,
                required: true,
            },
            value: {
                type: Number,
                required: false,
            }
        },
        computed: {
            selected: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', parseFloat(newValue));
                }
            },
        },
    });

</script>