<script type="text/x-template" id="input-select-servico-base">
    <div class="form-group">
    <label v-if="label.length" class="pull-left"> {{ label }} </label>
    <br>
    <select v-model="selected" :multiple="multiple" class="form-control select-4w">
        <option value="null" selected disabled> {{ placeholder || 'Selecione um serviço base' }} </option>
        <option v-for="option in options" :value="option"> {{ option.text }} </option>
    </select>
</div>
</script>

<script>
    Vue.component('input-select-servico-base', {
        template: '#input-select-servico-base',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            service: {
                type: Number,
                required: false,
            },
            placeholder: {
                type: String,
                required: false,
                default: null,
            },
            multiple: {
                type: Boolean,
                required: false,
                default: false,
            },
            label: {
                type: String,
                required: false,
            },
            value: {
                type: Object,
                required: false,
            }
        },
        data: function () {
            return {
                options: [],
                object: null
            }
        },
        computed: {
            selected: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', newValue);
                }
            },
        },
        watch: {
            service: function(newId) {
                let vm = this;

                vm.selected = null;

                axios
                    .get('/api/servicos/base/', {
                        params: {
                            id: newId
                        }
                    })
                    .then(function (response) {
                        vm.options = response.data.results;
                    });
            },
        }
    });
</script>

<style scoped>

    select option[data-default] {
        color: #888;
    }

</style>