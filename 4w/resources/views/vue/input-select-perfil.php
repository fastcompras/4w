<script type="text/x-template" id="input-select-perfil">
    <div class="form-group">
        <label v-if="label.length" class="pull-left"> {{ label }} </label>
        <br>
        <select v-model="selected" class="form-control select-4w">
            <option value="null" selected disabled> {{ placeholder || 'Selecione um perfil' }} </option>
            <option v-for="option in options" :value="option.id"> {{ option.text }} </option>
        </select>
    </div>
</script>

<script>
    Vue.component('input-select-perfil', {
        template: '#input-select-perfil',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            placeholder: {
                type: String,
                required: false,
                default: null,
            },
            label: {
                type: String,
                required: false,
            },
            value: {
                type: Number,
                required: false,
            }
        },
        data: function () {
            return {
                options: null,
            }
        },
        computed: {
            selected: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', newValue);
                }
            },
        },
        created: function () {
            let vm = this;

            axios
                .get('/api/perfil')
                .then(function (response) {
                    vm.options = response.data.results;
                });

        }
    });
</script>

<style scoped>

    select option[data-default] {
        color: #888;
    }

</style>