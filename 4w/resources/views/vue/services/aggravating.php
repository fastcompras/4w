<script type="text/x-template" id="aggravating">

    <div>

        <div v-for="field in list">

            <input-number
                v-if="field.field_type == 'text'"
                v-model="field.selected"
                :min="field.selected"
                :label="field.label">
            </input-number>

            <div v-if="field.field_type == 'checkbox'">
                <div class="col-md-4">
                    <input-single-checkbox
                        v-model="field.selected"
                        :label="field.label">
                    </input-single-checkbox>
                </div>
            </div>

            <input-radio
                v-if="field.field_type == 'radio'"
                v-model="field.selected"
                :label="field.label"
                :items="field.options">
            </input-radio>


        </div>

    </div>

</script>

<script>

    Vue.component('aggravating', {
        template: '#aggravating',
        props: {
            list: {
                type: Array,
                required: true,
            }
        },
    });

</script>