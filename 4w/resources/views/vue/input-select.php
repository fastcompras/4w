<script type="text/x-template" id="input-select">
    <div class="form-group">
    <label v-if="label.length"> {{ label }} </label>
    <br>
    <select v-model="selected" v-bind:multiple="multiple" class="form-control select-4w">
        <option v-for="option in options" v-bind:value="option.id">
            {{ option.text }}
        </option>
    </select>
</div>
</script>

<script>
    Vue.component('input-select', {
        template: '#input-select',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            multiple: {
                type: Boolean,
                required: false,
                default: false,
            },
            label: {
                type: String,
                required: false,
            },
            options: {
                type: Array,
                required: true,
            },
            value: {
                type: [Number, Array],
                required: false,
            }
        },
        computed: {
            selected: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', newValue);
                }
            },
        }
    });
</script>