<script type="text/x-template" id="input-cep">
    <div class="form-group field">
        <label v-if="label.length"> {{ label }} </label>
        <div>
            <input type="text"
                   class="cep form-control" :class="error ? 'error' : ''"
                   :required="required"
                   :maxlength="maxLength"
                   :minlength="minLength"
                   v-model="model.address_code">
        </div>
    </div>
</script>

<script>

    Vue.component('input-cep', {
        template: '#input-cep',
        data: function () {
            return {
                error: false
            }
        },
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            label: {
                type: String,
                required: false,
            },
            value: {
                type: [Object],
                required: false,
            },
            minLength: {
                type: Number,
                required: false,
                default: 8,
            },
            maxLength: {
                type: Number,
                required: false,
                default: 9,
            }
        },
        computed: {
            model: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', newValue);
                }
            },
        },
        watch: {
            'value.address_code': function (newValue) {
                let vm = this;
                let correctValue = newValue.replace('-', '');
                let hasError = newValue.length < this.minLength || newValue.length > this.maxLength;

                vm.error = hasError;

                if (! this.error) {
                    axios
                        .get('/api/cep/', {
                            params: {
                                cep: correctValue
                            }
                        })
                        .then(function (response) {
                            let data = response.data;

                            vm.value.street = data.logradouro;
                            vm.value.neighborhood = data.bairro;
                            vm.value.city.name = data.cidade;
                            vm.value.city.uf= data.uf;

                        })
                        .catch(function (error) {
                            vm.value.street = null;
                            vm.value.neighborhood = null;
                            vm.value.city.name = null;
                            vm.value.city.uf = null;
                        });
                }

                this.$emit('error', this.error);
            }
        }
    });
</script>