<script type="text/x-template" id="input-single-checkbox">
    <div class="form-group field">
        <div class="checkboxes">
            <label class="main-label" v-if="label.length"> {{ label }} </label>
            <div class="field-description help-block text-muted"> Algum texto aqui? </div>
            <label class="checkbox-label">
                <input type="checkbox" v-model="selected"> <span> Sim </span>
            </label>
        </div>
    </div>
</script>

<script>

    Vue.component('input-single-checkbox', {
        template: '#input-single-checkbox',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            label: {
                type: String,
                required: false,
            },
        },
        computed: {
            selected: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', newValue ? 1 : 0);
                }
            },
        }
    });

</script>