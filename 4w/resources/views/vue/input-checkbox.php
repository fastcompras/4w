<script type="text/x-template" id="input-checkbox">
    <div class="form-group field">
        <label v-if="label.length"> {{ label }} </label>
        <div v-for="item in items">
            <input type="checkbox" v-model="selected" :key="item.id" :value="item.id"> {{ item.text }}
        </div>
    </div>
</script>

<script>

    Vue.component('input-checkbox', {
        template: '#input-checkbox',
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            label: {
                type: String,
                required: false,
            },
            items: {
                type: Array,
                required: true,
            },
            value: {
                type: Array,
                required: false,
                default: []
            }
        },
        computed: {
            selected: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    this.$emit('input', newValue);
                }
            },
        }
    });

</script>