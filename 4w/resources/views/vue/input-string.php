<script type="text/x-template" id="input-string">
    <div class="form-group field">
        <label v-if="label.length"> {{ label }} </label>
        <div>
            <input type="text"
                   class="form-control" :class="error ? 'error' : ''"
                   :required="required"
                   :maxlength="maxLength"
                   v-model="model">
        </div>
    </div>
</script>

<script>
    Vue.component('input-string', {
        template: '#input-string',
        data: function () {
            return {
                error: false
            }
        },
        props: {
            required: {
                type: Boolean,
                required: false,
            },
            label: {
                type: String,
                required: false,
            },
            value: {
                type: [String],
                required: false,
            },
            minLength: {
                type: Number,
                required: false,
                default: 0,
            },
            maxLength: {
                type: Number,
                required: false,
                default: 255,
            }
        },
        computed: {
            model: {
                get: function() {
                    return this.value;
                },
                set: function(newValue) {
                    let hasError = newValue.length < this.minLength;

                    this.error = hasError;

                    this.$emit('input', newValue);
                    this.$emit('error', this.error);
                }
            },
        }
    });
</script>