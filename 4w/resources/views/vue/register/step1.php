<script type="text/x-template" id="step1">

     <div>
         <input-select-servico
             label="Selecione o tipo de serviço"
             v-model="service.id">
         </input-select-servico>

         <input-select-servico-base
             label="Especifique o serviço desejado"
             v-model="service.base"
             :service="service.id">
         </input-select-servico-base>

         <aggravating v-if="service.aggravating.length"
                      :list="service.aggravating">
         </aggravating>
     </div>

</script>

<script>

    Vue.component('step1', {
        props: [
            'service'
        ],
        template: '#step1',
        watch: {
            'service.id': function () {
                vm.service.aggravating = [];
            },
            ammount: function () {
                this.$emit('ammount', this.ammount);
            },
            selectedBaseService: function (payload) {

                if (payload) {

                    vm.service.aggravating = [];

                    axios
                        .get('/api/servicos/base/agravantes', {
                            params: {
                                base_id: payload.id
                            }
                        })
                        .then(function (response) {
                            vm.service.aggravating = response.data.results;
                        });

                }

            }
        },
        computed: {
            selectedBaseService: function () {
                return this.service.base;
            },
            listOfAggravating: function () {
                return this.service.aggravating;
            },
            initialAmmount: function () {
                return this.selectedBaseService ? this.selectedBaseService.value : 0;
            },
            ammount: function () {
                var sum = 0;

                let total = this.listOfAggravating
                    .reduce(function(sum, val) {

                        if (val.field_type == 'radio' && val.selected) {

                            for (var index in val.options) {
                                if (val.options[index].id == val.selected) {
                                    return sum += (parseFloat(val.options[index].value));
                                }
                            }

                        }

                        if (val.is_included) {
                            return sum += 0;
                        }

                        return sum += (parseFloat(val.value) * parseInt(val.selected));

                    }, 0);

                return parseFloat(this.initialAmmount) + parseFloat(total);
            }
        }
    });

</script>