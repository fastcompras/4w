<script type="text/x-template" id="user-info">

     <div>
         <div class="row">
             <div class="col-md-12 hidden-xs">
                 <h2 class="text-center font-bold m-b-xs">
                     Informe seus dados
                 </h2>
             </div>
             <div class="col-md-12">
                 <input-string
                     :label="labels.name"
                     :minLength="3"
                     v-on:error="updateErrors('name', $event)"
                     v-model="user.name">
                 </input-string>
             </div>
             <div class="col-md-12">
                 <input-email
                    :label="labels.email"
                    v-on:error="updateErrors('email', $event)"
                    v-model="user.email">
                 </input-email>
             </div>
             <div class="col-md-12">
                 <input-cep
                     :label="labels.cep"
                     v-on:error="updateErrors('cep', $event)"
                     v-model="user.address">
                 </input-cep>
             </div>
         </div>
     </div>

</script>

<script>

    Vue.component('user-info', {
        props: ['user'],
        template: '#user-info',
        data: function () {
            return {
                labels: {
                    name: "Informe seu nome completo (*)",
                    email: "Informe seu email (*)",
                    cep: "Informe o CEP do local onde será realizado o serviço (*)"
                },
                errors: {
                    name: false,
                    email: false,
                    cep: false,
                },
            }
        },
        methods: {
            updateErrors: function (key, value) {
                this.errors[key] = value;

                this.$emit('errors', this.errors);
            }
        }
    });

</script>

<style scoped>

    h2 {
        display: block;
        clear: both;
        margin-bottom: 6px;
        color: #33ACDF;
    }

</style>