<script type="text/x-template" id="step2">

     <div>
         <div class="row">
             <div class="col-md-12">
                 <h3 v-if="service.base"> {{ service.base.description }} </h3>
                 <hr>
                 <h3 v-if="user.name"> {{ user.name }} </h3>
                 <hr>
                 <h3 v-if="user.email"> {{ user.email }} </h3>
                 <hr>
                 <div v-if="user.address.address_code" class="row">
                     <div class="col-md-12">
                         <h3 v-if="user.address.address_code">CEP: {{ user.address.address_code }} </h3> <br>
                         <h3 v-if="user.address.address_code">Logradouro: {{ user.address.street }} </h3> <br>
                         <h3 v-if="user.address.address_code">Bairro: {{ user.address.neighborhood }} </h3> <br>
                         <h3 v-if="user.address.city.name">Cidade: {{ user.address.city.name }} </h3> <br>
                         <h3 v-if="user.address.city.name && user.address.city.uf"> Estado: {{ user.address.city.uf }} </h3>
                     </div>
                 </div>
                 <hr>
                 <h1 v-if="ammount">Total: R$ {{ ammount }} </h1>
             </div>
         </div>
     </div>

</script>

<script>

    Vue.component('step2', {
        props: ['user', 'service', 'ammount'],
        template: '#step2',
        computed: {}
    });

</script>


<style scoped>

    h3 {
        display: block;
        clear: both;
        margin-bottom: 6px;
        color: #000000;
    }

</style>