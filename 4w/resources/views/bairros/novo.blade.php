@extends('layouts.app')

@section('title', 'Sou 4W - Novo Bairro')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-building-o"></i>
                Cadastro de Bairros
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li>
                    <a href="{{route('bairro.show')}}">Lista de Bairros</a>
                </li>
                <li class="active">
                    <a href="{{route('bairro.create')}}">Novo Bairro</a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="nova-zona-cidade" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group">
                            {{ Form::label('estados', 'Estado', array('class' => 'col-sm-1 control-label'))}}
                            <div class="col-sm-6">
                                {{ Form::select('estados', $estados, null, array('id'=>'estados', 'class' => 'form-control', 'placeholder' => 'Selecione um estado', 'style' => 'width: 100%;')) }}
                            </div>
                        </div>
                        <div class="cidades form-group" style="display:none">
                            {{ Form::label('cidades', 'Cidade', array('class' => 'col-sm-1 control-label'))}}
                            <div class="col-sm-6">
                                <select id="cidades" name="cidade" class="form-control">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="regioes form-group" style="display:none">
                            {{ Form::label('regioes', 'Região', array('class' => 'col-sm-1 control-label'))}}
                            <div class="col-sm-6">
                                <select id="regioes" name="regiao" class="form-control">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="bairro form-group" style="display:none">
                            <label class="col-sm-1 control-label">Bairro</label>
                            <div class="col-sm-6">
                                <input type="text" id="bairro" name="bairro" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right">
                                <a href="{{route('bairro.show')}}" class="btn btn-white">Voltar</a>
                                <button class="salvar btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("change", "#estados", function (e) {
                var estado = $('#estados :selected').val();
                var action = base_path+'/helper/getCidades';

                var data = {
                    uf: estado,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('.cidades').show();
                            $('#cidades').empty();

                            $('#cidades').append($("<option></option>").attr("value", 0).text("Selecione uma cidade"));
                            $.each(data.cidades, function(index, obj){
                                $('#cidades').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });
            });

            $(document).on("change", "#cidades", function (e) {
                var cidade = $('#cidades :selected').val();
                var action = base_path+'/helper/getRegioesPorCidade';

                var data = {
                    id_cidade: cidade,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('.regioes').show();
                            $('.bairro').show();
                            $('#regioes').empty();

                            if (data.regioes.length == 0) {
                                mensagem('error', 'Esta cidade não possui regiões cadastradas! Cadastre primeiro as regiões!');
                                return false;
                            }

                            $.each(data.regioes, function(index, obj){
                                $('#regioes').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });

            });

            $(document).on("click", ".salvar", function (e) {
                e.preventDefault();
                var cidade = $('#cidades :selected').val();
                var regiao = $('#regioes :selected').val();
                var bairro = $('#bairro').val();

                if (!cidade || cidade == 0) {
                    mensagem('error', 'Selecione uma cidade!');
                    return false;
                }

                if (!regiao) {
                    mensagem('error', 'Selecione uma região de uma cidade!');
                    return false;
                }

                if (!bairro) {
                    mensagem('error', 'Selecione um bairro de uma região!');
                    return false;
                }

                var data = {
                    id_cidade: $('#cidades :selected').val(),
                    id_regiao: $('#regioes :selected').val(),
                    bairro: $('#bairro').val(),
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/bairros/salvar',
                    cache: false,
                    success: function(data) {
                        mensagem('success', 'Bairro criado com sucesso!');
                        setTimeout(function(){ window.location.href = base_path + '/bairros/'; }, 700);
                    },
                    error: function (response) {
                        mensagem('error', 'Erro ao criar o bairro! Por favor, tente novamente!');
                    }
                });
            });
        });
    </script>
@endsection