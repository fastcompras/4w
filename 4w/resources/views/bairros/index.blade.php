@extends('layouts.app')

@section('title', 'Sou 4W - Lista de Regiões')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>
                <i class="fa fa-building-o"></i>
                Bairros das Cidades
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('bairro.show')}}">Lista de Bairros</a>
                </li>
            </ol>
            <div class="hr-line-dashed"></div>
            <div class="pull-right">
                <a href="{{route('bairro.create')}}" class="btn btn-white"><i class="fa fa-plus"></i> Novo Bairro</a>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($bairros) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Bairro</th>
                                <th>Zona</th>
                                <th>Cidade</th>
                                <th>Estado</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($bairros as $bairro): ?>
                                <tr>
                                    <td>{{$bairro->id}}</td>
                                    <td>{{$bairro->nome}}</td>
                                    <td>{{$bairro->regiao->nome }}</td>
                                    <td>{{$bairro->cidade->nome }}</td>
                                    <td>{{$bairro->cidade->estado->nome }}</td>
                                    <td class="text-right footable-visible footable-last-column">
                                        <a href="{{url('/bairros/editar/'.$bairro->id)}}">
                                            <button title="Editar" class="btn-warning btn-circle btn btn-xs">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a><button title="Remover" data-id="{{$bairro->id}}" class="remover btn-danger btn-circle btn btn-xs"> <i class="fa fa-times" aria-hidden="true"></i></button></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $bairros->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Não há bairros cadastrados</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".remover", function (e) {
                var id = $(this).attr('data-id');

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'DELETE',
                    dataType: 'json',
                    url: base_path + '/bairros/remover',
                    cache: false,
                    success: function (response) {
                        $('.loading').hide();

                        mensagem('success', 'Bairro removido com sucesso!');

                        setTimeout(function() {
                            location.reload();
                        }, 800);
                    },
                    error: function (response) {
                        $('.loading').hide();

                        switch (response.status) {
                            case 404:
                            case 422:
                            case 500:
                            default:
                                mensagem('error', 'Erro ao remover o bairro! Por favor, tente novamente!');
                                return false;
                        }
                    }
                });
            });
        });
    </script>
@endsection