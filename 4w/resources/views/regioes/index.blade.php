@extends('layouts.app')

@section('title', 'Sou 4W - Lista de Regiões')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>
                <i class="fa fa-map-marker"></i>
                Regiões das Cidades
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('regiao.show')}}">Lista de Regiões</a>
                </li>
            </ol>
            <div class="hr-line-dashed"></div>
            <div class="pull-right">
                <a href="{{route('regiao.create')}}" class="btn btn-white"><i class="fa fa-plus"></i> Nova Região</a>
            </div>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($regioes) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>Cidade</th>
                                <th>Estado</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($regioes as $regiao): ?>
                                <tr>
                                    <td>{{$regiao->id}}</td>
                                    <td>{{$regiao->nome}}</td>
                                    <td>{{$regiao->cidade->nome }}</td>
                                    <td>{{$regiao->cidade->estado->nome }}</td>
                                    <td class="text-right footable-visible footable-last-column">
                                        <a href={{url('/regioes/atualizar/'.$regiao->id) }}>
                                            <button title="Editar" class="btn-warning btn-circle btn btn-xs">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                        <a>
                                            <button data-id="{{$regiao->id}}" class="remover-regiao btn-danger btn-circle btn btn-xs">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $regioes->render(); ?>
                    <?php else: ?>
                    <h3 class="text-danger text-center">Não há regiões cadastradas</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".remover-servico", function (e) {
                var id = $(this).attr('data-id');
                var action = base_path+'/servicos/remover/'+id;

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'DELETE',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', data.msg);

                            location.reload();
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });
        });
    </script>
@endsection