@extends('layouts.app')

@section('title', 'Sou 4W - Nova Zona da Cidade')

<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-map-marker"></i>
                Cadastro de Regiões
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li>
                    <a href="{{route('regiao.show')}}">Lista de Regiões</a>
                </li>
                <li class="active">
                    <?php if ($regioes->id) : ?>
                        <a href="{{route('regiao.create')}}">Atualizar Região</a>
                    <?php else : ?>
                        <a href="{{route('regiao.create')}}">Nova Região</a>
                    <?php endif; ?>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="nova-zona-cidade" class="form-horizontal">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <div class="form-group">
                            {{ Form::label('estados', 'Estado', array('class' => 'col-sm-1 control-label'))}}
                            <div class="col-sm-6">
                                {{ Form::select('estados', $estados, null, array('id'=>'estados', 'class' => 'form-control', 'placeholder' => 'Selecione um Estado', 'style' => 'width: 100%;')) }}
                            </div>
                        </div>
                        <div class="cidades form-group" style="display:none">
                            {{ Form::label('cidades', 'Cidade', array('class' => 'col-sm-1 control-label'))}}
                            <div class="col-sm-6">
                                <select id="cidades" name="cidade" class="form-control">
                                    <option value=""></option>
                                </select>
                            </div>
                        </div>
                        <div class="regiao form-group" style="display:none">
                            <label class="col-sm-1 control-label">Região</label>
                            <span class="text-danger">Ex: Região Leste</span>
                            <div class="col-sm-6">
                                <input type="text" id="regiao" name="regiao" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="pull-right">
                                <a href="{{route('regiao.show')}}" class="cancelar btn btn-white">Voltar</a>
                                <button class="salvar-regiao btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $(document).on("change", "#estados", function (e) {
                var estado = $('#estados :selected').val();
                var action = '/helper/getCidades';

                var data = {
                    uf: estado,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('.cidades').show();
                            $('.regiao').show();
                            $('#cidades').empty();

                            $.each(data.cidades, function(index, obj){
                                $('#cidades').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });

            });

            $(document).on("click", ".salvar-regiao", function (e) {
                e.preventDefault();
                var action = '/regioes/salvar';
                var method = "POST";

                var data = {
                    id_cidade: $('#cidades :selected').val(),
                    regiao: $('#regiao').val(),
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: method,
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', data.msg);
                            $('#regiao').val('');
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });
        });
    </script>
@endsection