@extends('layouts.app')

@section('title', 'Sou 4W - Editar Conta')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<?php
    $estados = App\Models\Estado::orderBy('nome', 'asc')->pluck('nome', 'id');
    $cidades = App\Models\Cidade::orderBy('nome', 'asc')->pluck('nome', 'id');

    $cep = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->cep : "";
    $logradouro = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->logradouro : "";
    $bairro = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->bairro : "";
    $numero = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->numero : "";
    $complemento = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->complemento : "";
    $telefone = (count($usuario->telefones) > 0) ? $usuario->telefones[0]->numero : "";
?>

<style>
    /* Limit image width to avoid overflow the container */
    img {
        max-width: 100%; /* This rule is very important, please do not ignore this! */
    }

    .foto-perfil {
        border: 3px solid rgba(204, 197, 197, 0.32) !important;
        border-radius: 50% !important;
        margin-left: 25px;
    }
</style>

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-user"></i>
                Minha Conta
            </h2>
            <div class="hr-line-dashed"></div>
            <p class="text-primary text-danger">
                Neste espaço você pode atualizar suas informações pessoais e sua localização.
            </p>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-user"></i> Informações Pessoais</a></li>
                            <li class=""><a data-toggle="tab" href="#tab-2"><i class="fa fa-home"></i> Localização</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="profile-image">
                                                <img src="{{$usuario->getAvatar()}}" class="foto-perfil img-circle circle-border m-b-md" alt="profile">
                                            </div>
                                            <br/>
                                            @if ($usuario->isTomador() || $usuario->isAdmin())
                                                <button type="button" class="alterar-imagem btn btn-md btn-white"> <i class="fa fa-camera"></i> Alterar Imagem</button>
                                            @endif
                                        </div>
                                        <div class="col-md-6">
                                            <div class="edicao-informacoes-pessoais">
                                                <div class="form-group">
                                                    <label for="title">Nome Completo (*)</label>
                                                    <input class="nome form-control required valid" name="nome" value="{{$usuario->nome}}" type="text" placeholder="Nome Completo">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">E-mail (*)</label>
                                                    <input type="email" class="email form-control required valid" value="{{$usuario->email}}" name="email" placeholder="E-mail">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Telefone (*)</label>
                                                    <input type="text" class="telefone form-control required valid" value="{{$telefone}}" placeholder="Telefone de Contato">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Senha</label>
                                                    <input type="password" class="senha form-control required" name="senha" placeholder="****************">
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-offset-8 col-md-3">
                                                        <button type="button" class="salvar-conta btn btn-success">
                                                            <i class="fa fa-floppy-o"></i>
                                                            Salvar Conta
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="tab-2" class="tab-pane">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="edicao-localizacao">
                                                <div class="form-group">
                                                    <label for="title">Estado</label>
                                                    {{ Form::select('estados', $estados, $id_estado, array('id'=>'estados', 'class' => 'form-control', 'placeholder' => 'Selecione seu estado', 'style' => 'width: 100%;')) }}
                                                </div>
                                                <div class="form-group">
                                                    <div class="cidade" style="">
                                                        <label for="title">Cidade</label>
                                                        {{ Form::select('cidades', $cidades, $usuario->id_cidade, array('id'=>'cidades', 'class' => 'form-control', 'placeholder' => 'Selecione sua cidade', 'style' => 'width: 100%;')) }}
                                                    </div>
                                                </div>
                                                <div class="hr-line-dashed"></div>
                                                <div class="form-group">
                                                    <label for="title">CEP</label>
                                                    <input class="cep form-control required valid" name="nome" value="{{$cep}}" type="text" placeholder="CEP">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Endereço (*)</label>
                                                    <input type="text" class="endereco form-control required valid" value="{{$logradouro}}" name="endereco" placeholder="Endereço">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Número (*)</label>
                                                    <input type="text" class="numero form-control required valid" value="{{$numero}}" placeholder="Número">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Complemento</label>
                                                    <input type="text" class="complemento form-control required" value="{{$complemento}}" name="complemento" placeholder="Complemento">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Bairro</label>
                                                    <input type="text" class="bairro form-control required" value="{{$bairro}}" name="bairro" placeholder="Bairro">
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-xs-6 col-md-offset-8 col-md-3">
                                                        <button type="button" class="salvar-localizacao btn btn-success">
                                                            <i class="fa fa-floppy-o"></i>
                                                            Salvar Localização
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('helpers.spinner')
    <div class="modal inmodal in" id="modalAlterarImagem" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header" style="background-color: #1f4988;">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <i class="fa fa-camera modal-icon" style="color:white"></i>
                    <h4 class="modal-title" style="color:white">Alterar Imagem</h4>
                    <span style="font-size:13px; color:white">(Limite de 2MB)</span>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <h4>Imagem Original</h4>
                                            <div class="image-crop">
                                                <img src="{{$usuario->getAvatar()}}" style="max-width: 100%; max-height: 100%;" class="cropper-hidden">
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <h4>Pré-Visualização</h4>
                                            <div class="img-preview img-preview-sm">
                                                <img src="" style="min-width: 0px !important; min-height: 0px !important; max-width: none !important; max-height: none !important; width: 250px; height: 167px; margin-left: -25px; margin-top: -22px;">
                                            </div>
                                            <hr>
                                            <div class="btn-group">
                                                <button class="btn btn-white" id="zoomIn" type="button">+ Zoom</button>
                                                <button class="btn btn-white" id="zoomOut" type="button">- Zoom</button>
                                                <label title="Selecione sua foto" for="inputImage" class="btn btn-success sm">
                                                    <input type="file" name="file" id="inputImage" class="hide">
                                                    <i class="fa fa-plus"></i> Selecionar Foto
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Fechar</button>
                    <button type="button" id="salvar-imagem" class="btn btn-success"><i class="fa fa-floppy-o"></i> Salvar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";
            var isMobile = window.matchMedia("only screen and (max-width: 760px)");

            if (! isMobile.matches) {
                $('.telefone').mask("(99) 9999-9999?9");
                $('.cep').mask('99999-999');
            }

            $(document).on("click", ".salvar-conta", function (e) {
                e.preventDefault();
                var action = base_path+'/conta/atualizar';
                var nome = $('.nome').val();
                var email = $('.email').val();
                var telefone = $('.telefone').val();
                var senha = $('.senha').val();

                if (nome.length == 0 || email.length == 0 || telefone.length == 0) {
                    mensagem('error', 'Por favor, preencha os dados obrigatórios!');
                    return false;
                }

                var data = {
                    nome: nome,
                    email: email,
                    telefone: telefone,
                    senha: senha,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'PUT',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        $('.loading').hide();
                        if (data.success) {
                           mensagem('success', 'Conta alterada com sucesso!');

                            $('.nome').val(nome);
                            $('.email').val(email);
                            $('.telefone').val(telefone);
                            $('.senha').val('');
                        } else {
                            mensagem('error', 'Erro ao editar sua conta! Por favor, tente novamente!');
                            /*$.each(data.mensagem, function(i, item) {
                                mensagem('error', item);
                            });*/
                        }
                    }
                });
            });

            $(document).on("click", ".salvar-localizacao", function (e) {
                e.preventDefault();
                var id_cidade = $('#cidades :selected').val();
                var cep = $('.cep').val();
                var endereco = $('.endereco').val();
                var numero = $('.numero').val();
                var complemento = $('.complemento').val();
                var bairro = $('.bairro').val();
                var action = '/conta/atualizarLocalizacao';

                if (id_cidade.length == 0) {
                    mensagem('error', 'Você precisa selecionar um estado e uma cidade');
                    return false;
                }

                if (cep.length == 0 || endereco.length == 0 || numero.length == 0 || bairro.length == 0) {
                    mensagem('error', 'Você deve preencher as informações do seu endereço!');
                    return false;
                }

                var data = {
                    id_cidade: id_cidade,
                    logradouro: endereco,
                    numero: numero,
                    complemento: complemento,
                    bairro: bairro,
                    cep: cep,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'PUT',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        $('.loading').hide();
                        if (data.success) {
                            mensagem('success', 'Localização alterada com sucesso!');

                            $('.cep').val(cep);
                            $('.endereco').val(endereco);
                            $('.numero').val(numero);
                            $('.complemento').val(complemento);
                            $('.bairro').val(bairro);

                        } else {
                            mensagem('error', 'Erro ao editar sua localização! Por favor, tente novamente!');
                            /*$.each(data.mensagem, function(i, item) {
                             mensagem('error', item);
                             });*/
                        }
                    }
                });

            });

            $(document).on("click", ".alterar-imagem", function (e) {
                $("#modalAlterarImagem").modal();
            });

            var $image = $(".image-crop > img");

            $($image).cropper({
                aspectRatio: 1,
                preview: ".img-preview",
                done: function(data) {
                    cropImgData = data;
                }
            });

            var $inputImage = $("#inputImage");
            if (window.FileReader) {
                $inputImage.change(function() {
                    var fileReader = new FileReader(), files = this.files, file;

                    if (!files.length) {
                        return;
                    }

                    file = files[0];

                    if (/^image\/\w+$/.test(file.type)) {
                        fileReader.readAsDataURL(file);
                        fileReader.onload = function () {
                            $inputImage.val("");
                            $image.cropper("reset", true).cropper("replace", this.result);
                        };
                    } else {
                        mensagem('error', "Por favor, selecione uma imagem!");
                        return false;
                    }
                });
            } else {
                $inputImage.addClass("hide");
            }

            $("#zoomIn").click(function() {
                $image.cropper("zoom", 0.1);
            });

            $("#zoomOut").click(function() {
                $image.cropper("zoom", -0.1);
            });

            $("#setDrag").click(function() {
                $image.cropper("setDragMode", "crop");
            });


            $("#salvar-imagem").click(function () {
                $('#modalAlterarImagem').modal('hide');

                $('.loading').show();
                setTimeout(function () {
                    $image.cropper("setDragMode", "crop");
                    $image.cropper('getCroppedCanvas').toBlob(function (blob) {
                        var formData = new FormData();
                        formData.append('croppedImage', blob);
                        formData.append('_token', CSRF_TOKEN);

                        $.ajax({
                            url: base_path+'/conta/atualizarImagem',
                            data: formData,
                            cache: false,
                            contentType: false,
                            enctype: 'multipart/form-data',
                            processData: false,
                            type: 'POST',
                            success: function (data) {
                                $('.loading').hide();
                                $('.close').click();

                                if (data.success) {
                                    mensagem('success', 'Foto alterada com sucesso!');

                                    setTimeout(function (e) {
                                        location.reload();
                                    }, 500);
                                }
                            }
                        });
                        $('.loading').hide();
                    });
                }, 800);
            });

            $(document).on("change", "#estados", function (e) {
                e.preventDefault();
                var estado = $('#estados :selected').val();
                var action = base_path+'/helper/getCidades';

                var data = {
                    id: estado,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('#cidades').empty();

                            $.each(data.cidades, function(index, obj){
                                $('#cidades').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });
            });

            $(document).on("change", ".cep", function (e) {
                e.preventDefault();

                var cep = $(this).val().replace('-','');

                if (cep == "") cep = "00000000";

                $('.loading').show();
                $.getJSON(base_path+'/webservice/cep/'+cep)
                        .done(function (data) {
                            var result = JSON.parse(data);
                            if (result.resultado != '0') {
                                var endereco = result.tipo_logradouro + ' ' + result.logradouro;
                                var bairro = result.bairro;

                                $('.endereco').val(endereco);
                                $('.bairro').val(bairro);
                                $('.numero').focus();
                            }
                            $('.loading').hide();
                        });
            });

            $(document).on("click", ".voltar-1", function (e) {
                mudaTab(2, 1);
            });

            $(document).on("click", ".voltar-2", function (e) {
                mudaTab(3, 2);
            });
        });

        function mudaTab(currentTab, newTab) {
            var tabFrom = $('#aba-'+currentTab);
            var tabTo = $('#aba-'+newTab);

            if (currentTab < newTab) {
                $(tabFrom).parent().addClass('disabled');
                $(tabFrom).parent().removeClass('complete');

                $(tabTo).parent().addClass('complete');
                $(tabTo).parent().removeClass('disabled');

                $('#aba-'+(newTab+1)).parent().removeClass('disabled');
            } else {
                $(tabFrom).parent().removeClass('complete');

                $(tabTo).parent().addClass('complete');
                $(tabTo).parent().removeClass('disabled');

                $('#aba-'+(currentTab+1)).parent().addClass('disabled');
            }

            $(tabTo).click();
        }
    </script>

@endsection