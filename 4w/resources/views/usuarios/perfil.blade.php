@extends('layouts.app')

@section('title', 'Sou 4W - Perfil do Usuário')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="{{url('/css/lib/servicos/prestador/avaliacoes.css')}}" />

<?php
    $estados = App\Models\Estado::orderBy('nome', 'asc')->pluck('nome', 'id');
    $cidades = App\Models\Cidade::orderBy('nome', 'asc')->pluck('nome', 'id');

    $cep = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->cep : "";
    $logradouro = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->logradouro : "";
    $bairro = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->bairro : "";
    $numero = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->numero : "";
    $complemento = (count($usuario->enderecos) > 0) ? $usuario->enderecos[0]->complemento : "";
    $telefone = (count($usuario->telefones) > 0) ? $usuario->telefones[0]->numero : "";
?>

<style>
    .select2-search__field {
        width: 100% !important;
    }

    .select2-container .select2-selection.select2-selection--multiple .select2-search.select2-search--inline .select2-search__field:not([placeholder='']) {
        width: 100% !important;
    }
</style>

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-user"></i>
                Perfil do Usuário <?= $usuario->nome ?>
            </h2>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="ibox-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs-container">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-user"></i> Informações Pessoais</a></li>
                            @if (Auth::user()->isAdmin())
                                <li><a data-toggle="tab" href="#tab-2"> <i class="fa fa-pencil"></i> Editar Cadastro </a></li>

                                @if ($usuario->isPrestador())
                                    <li><a data-toggle="tab" href="#tab-3"> <i class="fa fa-file"></i> Documentos </a></li>
                                @endif

                            @endif
                        </ul>
                        <div class="tab-content">
                            <div id="tab-1" class="tab-pane active">
                                <div class="row">
                                    <div class="col-md-6">
                                        <br>
                                        <div class="profile-image" style="width: 150px;">
                                            <img src="<?= $usuario->getAvatar() ?>" style="margin-left: 25px;" class="img-circle circle-border m-b-md" alt="profile">
                                            @if (Auth::user()->isAdmin() && $usuario->isPrestador())
                                                <h5 class="text-center"><i class="fa fa-calendar"></i> <a href="{{url('/usuario/agenda/' . $usuario->id)}}">Ver Agenda</a></h5>
                                            @endif
                                        </div>
                                        <div class="profile-info">
                                            <div class="">
                                                <div>
                                                    <h2 class="no-margins">
                                                        <?= $usuario->nome ?> <small>(<?= $usuario->perfil->descricao ?>)</small>
                                                    </h2>
                                                    <br>
                                                    <small>
                                                        <p><i class="fa fa-envelope"></i> <?= $usuario->email ?></p>
                                                        <p><i class="fa fa-map-marker"></i> <?= $usuario->getEnderecoTratado() ?></p>
                                                        <p><i class="fa fa-home"></i> <?= $usuario->getCidade() ?></p>
                                                        <p><i class="fa fa-phone"></i> <?= $usuario->getTelefoneTratado() ?></p>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (Auth::user()->isAdmin())
                                <div id="tab-2" class="tab-pane">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-edit">
                                                    <div class="form-group">
                                                        <label for="title">Nome Completo</label>
                                                        <input class="nome form-control required valid" name="nome" value="{{$usuario->nome}}" type="text" placeholder="Nome Completo">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="title">E-mail</label>
                                                        <input type="email" class="email form-control required valid" value="{{$usuario->email}}" name="email" placeholder="E-mail">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="title">Telefone</label>
                                                        <input type="text" class="telefone form-control required valid" value="{{$telefone}}" placeholder="Telefone de Contato">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Perfil</label>
                                                        {{ Form::select('perfil_conta', $perfis, $usuario->id_perfil_conta, ['id' => 'perfil_conta', 'class' => 'form-control', 'style' => 'width: 100%;']) }}
                                                    </div>
                                                    <div class="form-group servicos" style="display: none;">
                                                        <label>Tipo de Serviço</label>
                                                        <select id="servicos" multiple class="form-control" placeholder="Selecione um serviço" style="width: 100%;">
                                                            @foreach ($servicosPrestados as $servico)
                                                                <option value="{{$servico->id}}" selected> {{$servico->descricao}} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="title">Estado</label>
                                                    {{ Form::select('estados', $estados, $usuario->cidade->estado->id, array('id'=>'estados', 'class' => 'form-control', 'placeholder' => 'Selecione seu estado', 'style' => 'width: 100%;')) }}
                                                </div>
                                                <div class="form-group">
                                                    <div class="cidade" style="">
                                                        <label for="title">Cidade</label>
                                                        {{ Form::select('cidades', $cidades, $usuario->id_cidade, array('id'=>'cidades', 'class' => 'form-control', 'placeholder' => 'Selecione sua cidade', 'style' => 'width: 100%;')) }}
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">CEP</label>
                                                    <input class="cep form-control required valid" name="nome" value="{{$cep}}" type="text" placeholder="CEP">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Endereço (*)</label>
                                                    <input type="text" class="endereco form-control required valid" value="{{$logradouro}}" name="endereco" placeholder="Endereço">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Número (*)</label>
                                                    <input type="text" class="numero form-control required valid" value="{{$numero}}" placeholder="Número">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Complemento</label>
                                                    <input type="text" class="complemento form-control required" value="{{$complemento}}" name="complemento" placeholder="Complemento">
                                                </div>
                                                <div class="form-group">
                                                    <label for="title">Bairro</label>
                                                    <input type="text" class="bairro form-control required" value="{{$bairro}}" name="bairro" placeholder="Bairro">
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="pull-right">
                                                <button type="button" class="salvar btn btn-success">
                                                    <i class="fa fa-floppy-o"></i>
                                                    Salvar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if (Auth::user()->isAdmin() && $usuario->isPrestador())
                                <div id="tab-3" class="tab-pane">
                                    <div class="col-lg-12">
                                        <div class="ibox float-e-margins">
                                            <div class="ibox-content">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        @if (count($usuario->documentos) > 0)
                                                            <?php foreach ($usuario->documentos as $documento): ?>
                                                            <div class="file-box" data-id="{{$documento->id}}">
                                                                <div class="file">
                                                                    <span class="corner"></span>
                                                                    <div class="icon">
                                                                        <i class="fa fa-file"></i>
                                                                    </div>
                                                                    <div class="file-name">
                                                                        {{$documento->getTipo()}}
                                                                        <br>
                                                                        <small>{{$documento->data_cadastro->format('d/m/Y H:i:s')}}</small>
                                                                    </div>
                                                                    <div class="file-name text-center">
                                                                        <a href="{{url('/usuarios/prestadores/download/'.$usuario->id.'/'.$documento->tipo)}}" class="btn btn-xs btn-success" >
                                                                            <i class="fa fa-download"></i> Download
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php endforeach; ?>
                                                        @else
                                                            <div class="ibox float-e-margins">
                                                                <div class="ibox-content">
                                                                    <h3 class="text-danger text-center">Este usuário não enviou nenhum documento.</h3>
                                                                </div>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                </div>
            </div>
        </div>
    </div>
    @include('helpers.spinner')
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";
            var id_perfil = {{$usuario->id_perfil_conta}};

            $('.telefone').mask("(99) 9999-9999?9");

            // Prestador de Serviço
            if (id_perfil == 1) {
                $('.servicos').show();
            }

            $('#servicos').select2({
                multiple: true,
                placeholder: 'Selecione um serviço',
                ajax: {
                    url: "/api/servicos",
                    dataType: 'json',
                    data: function(params) {
                        return {
                            term: params.term || '',
                            page: params.page || 1
                        }
                    },
                    cache: true
                },
            });

            $(document).on("change", "#perfil_conta", function () {
                var perfil_conta = $(this).val();

                // Prestador de Serviço
                if (perfil_conta == 1) {
                    $('.servicos').show();
                } else {
                    $('.servicos').hide();
                }
            });


            $(document).on("click", ".salvar", function (e) {
                var id = {{$usuario->id}};
                var perfil_conta = $('#perfil_conta :selected').val();
                var servicos = $('#servicos').val();
                var nome = $('.nome').val();
                var email = $('.email').val();
                var telefone = $('.telefone').val();

                var id_cidade = $('#cidades :selected').val();
                var cep = $('.cep').val();
                var endereco = $('.endereco').val();
                var numero = $('.numero').val();
                var complemento = $('.complemento').val();
                var bairro = $('.bairro').val();

                var data = {
                    id: id,
                    perfil: perfil_conta,
                    servicos: (perfil_conta == 1) ? servicos : null,
                    nome: nome,
                    email: email,
                    telefone: telefone,
                    id_cidade: id_cidade,
                    logradouro: endereco,
                    numero: numero,
                    complemento: complemento,
                    bairro: bairro,
                    cep: cep,
                    _token: CSRF_TOKEN
                };

                // Valida o formulário
                if (!validateForm(data)) {
                    return false;
                }

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: base_path + '/conta/perfil/editar',
                    cache: false,
                    success: function (response) {
                        $('.loading').hide();

                        mensagem('success', 'Usuário atualizado com sucesso!');

                        setTimeout(function() {
                            location.reload();
                        }, 500);
                    },
                    error: function (response) {
                        $('.loading').hide();

                        switch (response.status) {
                            case 409:
                                mensagem('error', 'Erro ao atualizar o usuário! Este e-mail já existe na base de dados!');
                                break;
                            case 404:
                            case 422:
                            case 500:
                            default:
                                mensagem('error', 'Erro ao atualizar o usuário! Por favor, tente novamente!');
                                return false;
                        }
                    }
                });
            });

            $(document).on("change", "#estados", function (e) {
                e.preventDefault();
                var estado = $('#estados :selected').val();
                var action = base_path+'/helper/getCidades';

                var data = {
                    id: estado,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: 'POST',
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            $('#cidades').empty();

                            $.each(data.cidades, function(index, obj){
                                $('#cidades').append($("<option></option>").attr("value", obj.id).text(obj.nome));
                            });
                        }
                    }
                });
            });

            $(document).on("change", ".cep", function (e) {
                e.preventDefault();

                var cep = $(this).val().replace('-','');

                if (cep == "") cep = "00000000";

                $('.loading').show();
                $.getJSON(base_path+'/webservice/cep/'+cep)
                        .done(function (data) {
                            var result = JSON.parse(data);
                            if (result.resultado != '0') {
                                var endereco = result.tipo_logradouro + ' ' + result.logradouro;
                                var bairro = result.bairro;

                                $('.endereco').val(endereco);
                                $('.bairro').val(bairro);
                                $('.numero').focus();
                            }
                            $('.loading').hide();
                        });
            });

        });

        function validateForm(data) {

            if (data.nome.length == 0) {
                mensagem('error', 'O campo Nome é obrigatório!');
                return false;
            }

            if (data.email.length == 0) {
                mensagem('error', 'O campo E-mail é obrigatório!');
                return false;
            }

            if (data.telefone.length == 0) {
                mensagem('error', 'O campo Telefone é obrigatório!');
                return false;
            }

            if (data.id_cidade.length == 0) {
                mensagem('error', 'Você precisa selecionar um estado e uma cidade!');
                return false;
            }

            if (data.cep.length == 0 || data.logradouro.length == 0 || data.numero.length == 0 || data.bairro.length == 0) {
                mensagem('error', 'Você deve preencher as informações do seu endereço!');
                return false;
            }

            return true;
        }

    </script>

@endsection