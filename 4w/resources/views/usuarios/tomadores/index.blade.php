@extends('layouts.app')

@section('title', 'Sou 4W - Lista de usuários')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-user"></i>
                Usuários
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('usuarios.tomadores.show')}}">Lista de Usuários</a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($tomadores) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Usuário</th>
                                <th>E-mail</th>
                                <th>Status</th>
                                <th>Data de Cadastro</th>
                                <th>Ações</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($tomadores as $tomador): ?>
                                <tr>
                                    <td>{{ $tomador->id }}</td>
                                    <td>{{ $tomador->nome }}</td>
                                    <td>{{ $tomador->email }}</td>
                                    <td> <strong> {{ $tomador->getStatus(true) }} </strong> </td>
                                    <td>{{ $tomador->data_cadastro->format('d/m/Y H:i:s') }}</td>
                                    <td>
                                        <a href="{{url('/conta/perfil/' . $tomador->id)}}">
                                            <button class="btn-success btn-circle btn btn-xs" title="Visualizar Perfil"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                                        </a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $tomadores->render(); ?>
                    <?php else : ?>
                        <h3 class="text-danger text-center">Não existem usuários cadastrados</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        });
    </script>
@endsection