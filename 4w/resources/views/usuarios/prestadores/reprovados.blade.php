@extends('layouts.app')

@section('title', 'Sou 4W - Prestadores de serviço reprovados')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-times"></i>
                <i class="fa fa-user"></i>
                Usuários Reprovados
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('usuarios.prestadores.reprovados.show')}}">Lista de Prestadores Reprovados</a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($prestadores) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Usuário</th>
                                <th>E-mail</th>
                                <th>Status</th>
                                <th>Data de Cadastro</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($prestadores as $prestador): ?>
                                <tr>
                                    <td> {{ $prestador->id }} </td>
                                    <td> {{ $prestador->nome }} </td>
                                    <td> {{ $prestador->email }} </td>
                                    <td> <strong> {{ $prestador->getStatus(true) }} </strong></td>
                                    <td> {{ $prestador->data_cadastro->format('d/m/Y H:i:s') }} </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $prestadores->render(); ?>
                    <?php else : ?>
                        <h3 class="text-danger text-center">Não existem prestadores de serviço reprovados</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        });
    </script>
@endsection