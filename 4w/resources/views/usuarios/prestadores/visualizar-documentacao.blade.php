@extends('layouts.app')

@section('title', 'Sou 4W - Lista de Cadastros Pendentes')

<meta name="csrf-token" content="{{ csrf_token() }}" />

<style>
    .file-box {
        width: 270px !important;
    }
</style>

@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2><i class="fa fa-file"></i> Documentação do Prestador <strong><?= $usuario->nome ?></strong></h2>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" id="id" value="<?= $usuario->id ?>">
                            @if (count($documentos) > 0)
                                <?php foreach ($documentos as $documento): ?>
                                <div class="file-box" data-id="{{$documento->id}}">
                                    <div class="file">
                                        <span class="corner"></span>
                                        <div class="icon">
                                            <i class="fa fa-file"></i>
                                        </div>
                                        <div class="file-name">
                                            {{$documento->getTipo()}}
                                            <br>
                                            <small>{{$documento->data_cadastro->format('d/m/Y H:i:s')}}</small>
                                        </div>
                                        <div class="file-name text-center">
                                            <a href="{{url('/usuarios/prestadores/download/'.$usuario->id.'/'.$documento->tipo)}}">
                                                <button class="btn btn-xs btn-success"><i class="fa fa-download"></i> Download </button>
                                            </a>
                                            <button data-id="{{$documento->id}}" class="btn btn-xs btn-danger remover"><i class="fa fa-times"></i> Remover</button>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            @else
                                <div class="ibox float-e-margins">
                                    <div class="ibox-content">
                                        <h3 class="text-danger text-center">Este usuário não enviou documentos para aprovação.</h3>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="row">
                        <div class="col-md-offset-3 col-md-3">
                            <button type="button" class="rejeitar btn btn-danger btn-sm">
                                <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                                Rejeitar Documentação
                            </button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" class="aprovar btn btn-primary btn-sm">
                                <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                                Aprovar Documentação
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('helpers.spinner')
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".aprovar", function (e) {
                e.preventDefault();
                var id = $('#id').val();
                var action = base_path+'/usuarios/prestadores/aprovar';

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: "POST",
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        $('.loading').hide();
                        if (data.success) {
                            mensagem('success', data.msg);
                        } else {
                            mensagem('error', data.msg);
                        }
                        setTimeout(function() {
                            window.location.href = base_path+'/usuarios/prestadores/pendentes';
                        }, 1000);
                    }
                });
            });

             $(document).on("click", ".rejeitar", function (e) {
                e.preventDefault();
                var id = $('#id').val();
                var action = base_path+'/usuarios/prestadores/rejeitar';

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: "POST",
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        $('.loading').hide();
                        if (data.success) {
                            mensagem('success', data.msg);
                        } else {
                            mensagem('error', data.msg);
                        }
                        setTimeout(function() {
                            window.location.href = base_path+'/usuarios/prestadores/pendentes';
                        }, 800);
                    }
                });
            });

            $(document).on("click", ".remover", function (e) {
                e.preventDefault();
                var id = $(this).attr('data-id');

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $('.loading').show();
                $.ajax({
                    data: data,
                    type: 'DELETE',
                    dataType: 'json',
                    url: base_path + '/documentos/remover',
                    cache: false,
                    success: function (response) {
                        $('.loading').hide();

                        mensagem('success', 'Documento removido com sucesso!');

                        setTimeout(function() {
                            location.reload();
                        }, 800);
                    },
                    error: function (response) {
                        $('.loading').hide();

                        switch (response.status) {
                            case 404:
                            case 422:
                            case 500:
                            default:
                                mensagem('error', 'Erro ao remover o documento! Por favor, tente novamente!');
                                return false;
                        }
                    }
                });
            });
        });
    </script>
@endsection
