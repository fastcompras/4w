@extends('layouts.app')

@section('title', 'Sou 4W - Prestadores de serviço aprovados')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-check"></i>
                <i class="fa fa-user"></i>
                Usuários Aprovados
            </h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('home')}}">Início</a>
                </li>
                <li class="active">
                    <a href="{{route('usuarios.prestadores.show')}}">Lista de Prestadores</a>
                </li>
            </ol>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <?php if (count($prestadores) > 0) : ?>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Usuário</th>
                                <th>E-mail</th>
                                <th>Status</th>
                                <th>Data de Cadastro</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($prestadores as $prestador): ?>
                                <tr>
                                    <td> {{ $prestador->id }} </td>
                                    <td> {{ $prestador->nome }} </td>
                                    <td> {{ $prestador->email }} </td>
                                    <td> <strong> {{ $prestador->getStatus(true) }} </strong> </td>
                                    <td> {{ $prestador->data_cadastro->format('d/m/Y H:i:s') }}</td>
                                    <td class="text-right footable-visible footable-last-column">
                                        <a>
                                            <a href="{{url('/conta/perfil/' . $prestador->id)}} "title="Visualizar Informações" class="btn-success btn-circle btn btn-xs"> <i class="fa fa-search " aria-hidden="true"></i> </a>
                                        </a>
                                        @if ($prestador->aprovado != \App\Models\User::USUARIO_BLOQUEADO)
                                        <a>
                                            <button data-id="{{$prestador->id}}" title="Suspender Usuário" class="suspender btn-danger btn-circle btn btn-xs"> <i class="fa fa-times " aria-hidden="true"></i> </button>
                                        </a>
                                        @else
                                        <a>
                                            <button data-id="{{$prestador->id}}" title="Reativar Usuário" class="reativar btn-circle btn-primary btn btn-xs"> <i class="fa fa-refresh" aria-hidden="true"></i> </button>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo $prestadores->render(); ?>
                    <?php else : ?>
                        <h3 class="text-danger text-center">Não existem prestadores de serviço cadastrados</h3>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
@endsection

<div class="modal inmodal in" id="modalInformacoes" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content animated bounceInRight">
            <div class="modal-header">
                <h4 style="color:black" class="modal-title">Visualizar Informações</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contact-box center-version">
                            <a>
                                <img alt="image" class="avatar img-circle" src="">
                                <h3 class="m-b-xs"><strong class="nome"></strong></h3>
                                <div class="email font-bold"></div>
                                <address class="m-t-md">
                                    <strong>Localização</strong><br>
                                    <span class="endereco"></span><br>
                                    <span class="bairro"></span><br>
                                    <span class="cidade"></span><br>
                                    <span class="telefone"></span>
                                </address>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-offset-10 col-md-2">
                        <button class="fechar-modal btn btn-success">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
    <script>

        $(document).ready(function() {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".fechar-modal", function (e) {
                $('#modalInformacoes').hide();
            });

            $(document).on("click", ".informacoes", function (e) {
                var action = '/usuarios/prestadores/buscaResumo';
                var id = $(this).attr('data-id');

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: "POST",
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            addInfoUsuario(data.usuario[0]);

                            $('#modalInformacoes').show();
                        } else {
                            mensagem('error', data.msg);
                        }
                    }
                });
            });

            $(document).on("click", ".reativar", function (e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var action = base_path+'/usuarios/prestadores/reativar';

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: "POST",
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', 'Prestador reativado com sucesso!');
                        } else {
                            mensagem('error', data.msg);
                        }
                        setTimeout(function() {
                            window.location.href = base_path+'/usuarios/prestadores/';
                        }, 1000);
                    }
                });
            });

            $(document).on("click", ".suspender", function (e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var action = base_path+'/usuarios/prestadores/suspender';

                var data = {
                    id: id,
                    _token: CSRF_TOKEN,
                };

                $.ajax({
                    data: data,
                    type: "POST",
                    dataType: 'json',
                    url: action,
                    cache: false,
                    success: function(data) {
                        if (data.success) {
                            mensagem('success', 'Prestador suspenso com sucesso!');
                        } else {
                            mensagem('error', data.msg);
                        }
                        setTimeout(function() {
                            window.location.href = base_path+'/usuarios/prestadores/';
                        }, 1000);
                    }
                });
            });
        });

        function addInfoUsuario(data) {
            $('.avatar').attr('src', data.avatar);
            $('.nome').text(data.nome);
            $('.email').text(data.email);
            $('.cidade').text(data.cidade);
            $('.endereco').text(data.endereco);
            $('.bairro').text(data.bairro);
            $('.telefone').text(data.telefone);
        }
    </script>
@endsection