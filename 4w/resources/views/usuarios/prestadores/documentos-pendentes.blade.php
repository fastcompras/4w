@extends('layouts.app')

@section('title', 'Sou 4W - Novo Serviço')

<meta name="csrf-token" content="{{ csrf_token() }}" />


@section('content')
    <br/>
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>
                <i class="fa fa-file"></i>
                Documentação Pendente
            </h2>
            <div class="hr-line-dashed"></div>
            <p class="text-primary text-danger">
                Você deve enviar todos os documentos solicitados para prestar algum tipo de serviço na <strong>4W</strong>.
            </p>
        </div>
    </div>
    <br/>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form id="enviar_documentacao" class="form-horizontal" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="id" value="{{$usuario->id}}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <?php if (!$usuario->temDocumentosPendentes()) : ?>
                            <h3 class="text-danger text-center">Você não possui documentos pendentes</h3>
                        <?php else: ?>
                        <?php if (!$usuario->enviouDocumento(\App\Models\Documento::TIPO_AVATAR)) : ?>
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('avatar', 'IMAGEM DE PERFIL')}}
                                </div>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                                <span class="fileinput-new">Selecionar Documento</span>
                                                <span class="fileinput-exists">Trocar</span>
                                                <input type="file" id="avatar" name="avatar"/>
                                            </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!$usuario->enviouDocumento(\App\Models\Documento::TIPO_RG)) : ?>
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('rg', 'DOCUMENTO DE IDENTIDADE (RG)')}}
                                </div>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Selecionar Documento</span>
                                        <span class="fileinput-exists">Trocar</span>
                                        <input type="file" id="rg" name="rg"/>
                                    </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!$usuario->enviouDocumento(\App\Models\Documento::TIPO_CPF)) : ?>
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('cpf', 'CADASTRO DE PESSOA FÍSICA (CPF)')}}
                                </div>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Selecionar Documento</span>
                                        <span class="fileinput-exists">Trocar</span>
                                        <input type="file" id="cpf" name="cpf"/>
                                    </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                    </div>
                                </div>
                            </div>
                        <?php endif?>
                        <?php if (!$usuario->enviouDocumento(\App\Models\Documento::TIPO_MEI)) : ?>
                    <div class="row">
                        <div class="col-md-3">
                            {{ Form::label('cpf', 'CADASTRO DE MICROEMPREENDEDOR INDIVIDUAL (MEI)')}}
                        </div>
                        <div class="col-md-6">
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput">
                                    <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                </div>
                                <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Selecionar Documento</span>
                                        <span class="fileinput-exists">Trocar</span>
                                        <input type="file" id="mei" name="mei"/>
                                    </span>
                                <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                            </div>
                        </div>
                    </div>
                    <?php endif?>
                        <?php if (!$usuario->enviouDocumento(\App\Models\Documento::TIPO_RESIDENCIA)) : ?>
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('residencia', 'COMPROVANTE DE RESIDÊNCIA')}}
                                </div>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Selecionar Documento</span>
                                        <span class="fileinput-exists">Trocar</span>
                                        <input type="file" id="residencia" name="residencia"/>
                                    </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if (!$usuario->enviouDocumento(\App\Models\Documento::TIPO_ANTECEDENTES)) : ?>
                            <div class="row">
                                <div class="col-md-3">
                                    {{ Form::label('antecedentes', 'ANTECEDENTES CRIMINAIS')}}
                                </div>
                                <div class="col-md-6">
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput">
                                            <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Selecionar Documento</span>
                                        <span class="fileinput-exists">Trocar</span>
                                        <input type="file" id="antecedentes" name="antecedentes"/>
                                    </span>
                                        <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remover</a>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-7">
                                <button class="enviar_documentacao btn btn-success"><i class="fa fa-upload"></i> Enviar Documentação</button>
                            </div>
                        </div>
                        <?php endif;?>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="chromeWarning" tabindex="-1" role="dialog" aria-labelledby="chromeModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="chromeModalLabel">Atenção usuário de Google Chrome</h4>
            </div>
            <div class="modal-body">
                <p>
                    Seu browser é <span class="chrome-version-label" style="font-weight: bold">Google Chrome</span> e verificamos que há um problema ao enviar arquivos através deste browser.
                </p><p>
                    Sugerimos que você envie os arquivos em outro momento utilizando outro browser, como o <a href="https://www.mozilla.org/pt-BR/firefox/">Firefox</a>, <a href="https://www.opera.com/pt-br">Opera</a>, ou <a href="https://www.microsoft.com/pt-br/edge">Microsoft Edge</a>.
                </p>
                <p>Desculpe o inconveniente e obrigado pela paciência.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
            </div>
        </div>
    </div>
    @include('helpers.spinner')
@endsection

@section('scripts')
    <script>
        function send_err_data(data) {
            let extraData;
            if (new Date() < new Date("2020-06-10")) {
                function getPluginList() {
                    try {
                        let pluginDescriptors = [];
                        for (let i = 0 ; i < window.navigator.plugins.length; i++) {
                            pluginDescriptor.push(
                                window.navigator.plugins[0].name + " - " + window.navigator.plugins[0].description
                            );
                        }
                        return pluginDescriptors.join(", ");
                    } catch (err) { return err.toString() }
                }
                extraData = {
                    appVersion: window.navigator.appVersion,
                    userAgent: window.navigator.userAgent,
                    platform: window.navigator.platform,
                    plugins: getPluginList(),
                }
            }
            data.browserInfo = extraData;
            fetch("https://www.sou4w.com.br/log/", {method:"post", body: JSON.stringify(data)}).then(console.log).catch(console.warn);
        }

        $(document).ready(function() {

            // Alerta de pessoas que acessam o site por google chrome
            const isChrome = (window.navigator && window.navigator.appVersion && window.navigator.appVersion.includes && window.navigator.appVersion.includes("Chrome") && !window.navigator.appVersion.includes("Edg") && !window.navigator.appVersion.includes("Trident"));
            if (isChrome) {
                if (window.navigator.appVersion.includes("/8")) {
                    try {
                        const browser = "Google Chrome " + window.navigator.appVersion.split("Chrome/")[1].substring(0, 9);
                        document.querySelector(".chrome-version-label").innerText = browser;
                    } catch (err) { }
                    $("#chromeWarning").modal('show');
                }
                send_err_data({"message": "New Google Chrome access (documentos-pendentes.blade)"});
            }

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var base_path = "{{url('/')}}";

            $(document).on("click", ".cancelar", function (e) {
                e.preventDefault();
            });

            $(document).on("click", ".enviar_documentacao", function (e) {
                e.preventDefault();
                try {

                var action = base_path+'/conta/enviarDocumentacao';
                var formData = new FormData(document.querySelector('#enviar_documentacao'));

                if ($('#avatar').length > 0) {
                    formData.append('avatar', $('#avatar')[0].files[0]);
                }

                if ($('#rg').length > 0) {
                    formData.append('rg', $('#rg')[0].files[0]);
                }

                if ($('#cpf').length > 0) {
                    formData.append('cpf', $('#cpf')[0].files[0]);
                }

                if ($('#mei').length > 0) {
                    formData.append('mei', $('#mei')[0].files[0]);
                }

                if ($('#residencia').length > 0) {
                    formData.append('residencia', $('#residencia')[0].files[0]);
                }
                if ($('#antecedentes').length > 0) {
                    formData.append('antecedentes', $('#antecedentes')[0].files[0]);
                }

                formData.append('_token', CSRF_TOKEN);

                $('.loading').show();
                $.ajax({
                    url: action, // Url do lado server que vai receber o arquivo
                    data: formData,
                    cache: false,
                    contentType: false,
                    enctype: 'multipart/form-data',
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        $('.loading').hide();

                        mensagem('success', 'Arquivo(s) enviado(s) com sucesso!');

                        setTimeout(function() {
                            location.reload();
                        }, 1400);
                    },
                    error: function (response) {
                        if (response.status === 422) {
                            send_err_data("Got first error code 422, trying again");
                            fetch(
                                action,
                                {
                                    "credentials":"include",
                                    "headers":
                                    {
                                        "accept":"*/*",
                                        "accept-language":"pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7",
                                        "save-data":"on",
                                        "x-requested-with":"XMLHttpRequest"
                                    },
                                    "body": formData,
                                    "method":"POST",
                                    "mode":"cors"
                                }
                            ).then(function(responsei) {
                                send_err_data("Got "+responsei.status+" code on next request");
                                if (responsei.status == 422) {
                                    $('.loading').hide();
                                    return mensagem('error', response.responseJSON.mensagem);
                                }
                                responsei.json().then(function(data) {
                                    $('.loading').hide();
                                    mensagem('success', 'Arquivo(s) enviado(s) com sucesso!');
                                    setTimeout(function() {
                                        location.reload();
                                    }, 1500);
                                }).catch(function() {
                                    $('.loading').hide();
                                    return mensagem('error', 'Erro ao enviar os arquivos especificados! por favor, tente novamente!');
                                });
                            }).catch(function() {
                                $('.loading').hide();
                                mensagem('error', response.responseJSON.mensagem);
                            });
                        } else if (response.status === 500) {
                            mensagem('error', 'Erro ao enviar os arquivos! por favor, tente novamente!');
                        }
                    }
                });
                } catch (err) {
                    send_err_data({message: "Error caught while sending files: " + err, stack: err.stack});
                    mensagem('error', err.toString());
                }
            });
        });
    </script>
@endsection