<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Css Files
	|--------------------------------------------------------------------------
	|
	| Css file of your style for your emails
	| The content of these files will be added directly into the inliner
	|
	*/

	'css-files' => ['../public/css/lib/email/basic.css'],

];
