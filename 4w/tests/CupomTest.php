<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\Cupom;

class CupomTest extends TestCase
{
	use WithoutMiddleware;

	/** @test */
	public function it_should_create_the_coupon()
	{
		$this->faker = Faker\Factory::create();

		$data = [
			'codigo' => $this->faker->swiftBicNumber,
			'qtd_disponivel' => $this->faker->randomDigitNotNull,
			'desconto' => $this->faker->randomFloat(2, 0, 1000),
			'tipo_desconto' => $this->faker->numberBetween(1, 2),
			'tipo_regra' => $this->faker->numberBetween(1, 3),
			'data_expiracao' => $this->faker->date('Y-m-d H:i:s'),
		];

		$this->postJson(route('cupom.store'), $data)
				->assertResponseStatus(201);

		$content = $this->decodeResponseJson();

		$this->assertEquals($data['codigo'], $content['codigo']);
		$this->assertEquals($data['qtd_disponivel'], $content['qtd_disponivel']);
		$this->assertEquals($data['desconto'], $content['desconto']);
	}

	/** @test */
	public function it_should_update_the_coupon()
	{
		$coupon = factory(Cupom::class)->create();

		$data = [
			'qtd_disponivel' => (int) ($coupon->qtd_disponivel - 1)
		];

		$this->putJson(route('cupom.update', ['id' => $coupon->id]), $data)
				->assertResponseOk();

		$content = $this->decodeResponseJson();

		$this->assertEquals($coupon->id, $content['id']);
		$this->assertEquals($data['qtd_disponivel'], $content['qtd_disponivel']);
	}

	/** @test */
	public function it_should_show_the_coupon()
	{
		$coupon = factory(Cupom::class)->create();

		$this->get(route('cupom.find', ['id' => $coupon->id]))
			->assertResponseOk();

		$content = $this->decodeResponseJson();

		$this->assertEquals($content['id'], $coupon->id);
		$this->assertEquals($content['codigo'], $coupon->codigo);
		$this->assertEquals($content['data_expiracao'], $coupon->data_expiracao);
	}

	/** @test */
	public function it_should_delete_the_coupon()
	{
		$coupon = factory(Cupom::class)->create();

		$this->delete(route('cupom.delete', ['id' => $coupon->id]))
			->assertResponseStatus(204);
	}

	/** @test */
	public function it_should_be_used_with_quantity_available()
	{
		$coupon = factory(Cupom::class)->states('validate_by_available_quantity')->make([
			'qtd_disponivel' => 1,
		]);

		$this->assertTrue($coupon->ruleIsByAvailableQuantity());
		$this->assertTrue($coupon->canBeUsed());
	}

	/** @test */
	public function it_should_be_used_if_the_date_has_not_expired()
	{

		$date = new DateTime("2019-10-13 15:32:00");
		$dateAgo = new DateTime("2018-01-01 00:01:00");

		$coupon = factory(Cupom::class)->states('validate_by_expiration_date')->make([
			'qtd_disponivel' => 1,
			'data_expiracao' => $date->format("Y-m-d H:i:s")
		]);

		$this->assertTrue($coupon->ruleIsByExpirationDate());
		$this->assertTrue($coupon->canBeUsed());

		// Changing the expiration date to force an error
		$coupon->data_expiracao = $dateAgo->format("Y-m-d H:i:s");

		$this->assertFalse($coupon->canBeUsed());
	}

	/** @test */
	public function it_should_to_decrease_a_quantity()
	{
		$coupon = factory(Cupom::class)->states('validate_by_available_quantity')->create([
			'qtd_disponivel' => 1,
		]);

		$coupon->decreaseQuantity();

		$this->assertEquals($coupon->qtd_disponivel, 0);
	}
}
