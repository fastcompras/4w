<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\Cupom;
use App\Models\Oportunidade;

class OportunidadeTestTest extends TestCase
{
	use WithoutMiddleware;

	/** @test */
	public function it_should_validate_if_the_deal_has_a_coupon()
	{
		$coupon = factory(Cupom::class)->states('validate_by_available_quantity')->create([
			'qtd_disponivel' => 1,
		]);

		$deal = factory(Oportunidade::class)->make([
			'id_cupom' => $coupon->id
		]);

		$this->assertTrue($deal->hasDiscountCoupon());
	}

	/** @test */
	public function it_should_apply_a_discount_on_the_total()
	{
		$coupon = factory(Cupom::class)
				->states('validate_by_available_quantity', 'absolute_discount')
				->create(['qtd_disponivel' => 1]);

		$deal = factory(Oportunidade::class)->make([
			'id_cupom' => $coupon->id,
		]);

		$this->assertTrue($deal->cupom->discountIsAbsolute());
		$this->assertTrue($deal->cupom->canBeUsed());
		$this->assertEquals(
				$deal->getTotalWithDiscountCoupon(),
				round($deal->valor_total - $coupon->desconto, 2)
		);

		// Increasing the discount
		$deal->cupom->desconto = $deal->valor_total + 1000.00;

		$this->assertEquals($deal->getTotalWithDiscountCoupon(), 0.00);

		// Testing another configuration
		$deal->cupom->tipo_desconto = \App\Models\Enums\TipoDescontoCupom::PERCENTAGE;
		$deal->cupom->desconto = 10;

		$this->assertTrue($deal->cupom->discountIsPercentage());
		$this->assertEquals(
				$deal->getTotalWithDiscountCoupon(),
				round($deal->valor_total - ($deal->valor_total * ($deal->cupom->getDiscountAmount() / 100)), 2)
		);

		// Increasing the percentage
		$deal->cupom->desconto = 200;
		$this->assertEquals(
			$deal->getTotalWithDiscountCoupon(),
			0.00
		);
	}

	/** @test */
	public function it_should_to_prevent_duplicate_discount()
	{
		$coupon = factory(Cupom::class)
			->states('validate_by_available_quantity', 'percentage_discount')
			->create(['qtd_disponivel' => 1, 'desconto' => 90]);

		$deal = factory(Oportunidade::class)->create([
			'id_cupom' => $coupon->id,
		]);


		$this->assertTrue($deal->hasAvailableCupom());
		$this->assertTrue($deal->cupom->discountIsPercentage());

		$initialValue = $deal->valor_total;

		// Coupon history
		$deal->useCoupon();

		$this->assertFalse($deal->cupom->isAvailable());
		$this->assertEquals(
			$deal->getTotalWithDiscountCoupon(),
			round($deal->valor_total - ($deal->valor_total * ($deal->cupom->getDiscountAmount() / 100)), 2)
		);

		$deal->useCoupon();

		$this->assertEquals($deal->getTotalWithDiscountCoupon(), $deal->getTotalWithDiscountCoupon());
		$this->assertNotEquals($deal->getTotalWithDiscountCoupon(), $initialValue);
	}
}
