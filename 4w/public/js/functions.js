
function toDatetime(value) {
    return moment(Date.parse(value)).format('DD/MM/YYYY[ às ]HH:mm');
}

function formatValue(value) {
    return numeral(parseFloat(value)).format('0,0.00');
}

function mensagem(tipo, msg, time = 2000) {

    setTimeout(function () {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: time
        };

        if (tipo == "success")
            toastr.success(msg);
        else if (tipo == "error")
            toastr.error(msg);
    }, 300);
}

