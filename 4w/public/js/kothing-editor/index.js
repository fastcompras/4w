! function(e) {
    var t = {};

    function i(n) {
        if (t[n]) return t[n].exports;
        var o = t[n] = {
            i: n,
            l: !1,
            exports: {}
        };
        return e[n].call(o.exports, o, o.exports, i), o.l = !0, o.exports
    }
    i.m = e, i.c = t, i.d = function(e, t, n) {
        i.o(e, t) || Object.defineProperty(e, t, {
            enumerable: !0,
            get: n
        })
    }, i.r = function(e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }, i.t = function(e, t) {
        if (1 & t && (e = i(e)), 8 & t) return e;
        if (4 & t && "object" == typeof e && e && e.__esModule) return e;
        var n = Object.create(null);
        if (i.r(n), Object.defineProperty(n, "default", {
                enumerable: !0,
                value: e
            }), 2 & t && "string" != typeof e)
            for (var o in e) i.d(n, o, function(t) {
                return e[t]
            }.bind(null, o));
        return n
    }, i.n = function(e) {
        var t = e && e.__esModule ? function() {
            return e.default
        } : function() {
            return e
        };
        return i.d(t, "a", t), t
    }, i.o = function(e, t) {
        return Object.prototype.hasOwnProperty.call(e, t)
    }, i.p = "", i(i.s = "tjUo")
}({
    KFuf: function(e, t, i) {},
    KJ7s: function(e, t, i) {},
    P6u4: function(e, t, i) {
        "use strict";
        var n, o;
        n = "undefined" != typeof window ? window : this, o = function(e, t) {
            const i = {
                toolbar: {
                    default: "Default",
                    save: "Salvar",
                    font: "Fonte",
                    formats: "Formatos",
                    fontSize: "Tamanho",
                    bold: "Negrito",
                    underline: "Underline",
                    italic: "Italico",
                    strike: "Strike",
                    subscript: "Subscript",
                    superscript: "Superscript",
                    removeFormat: "Limpar formato",
                    fontColor: "Cor do Texto",
                    hiliteColor: "Cor de Fundo",
                    indent: "Adicionar Recuo",
                    outdent: "Remover Recuo",
                    align: "Alinhar",
                    alignLeft: "Alinhar esquerda",
                    alignRight: "Alinhar direita",
                    alignCenter: "Centralizar",
                    alignJustify: "Justificar",
                    list: "Lista",
                    orderList: "Lista com numeros",
                    unorderList: "Lista sem numeros",
                    horizontalRule: "Linha horizontal",
                    hr_solid: "Sólido",
                    hr_dotted: "Pontilhado",
                    hr_dashed: "Dashes",
                    table: "Tabela",
                    link: "Link",
                    image: "Imagem",
                    video: "Video",
                    fullScreen: "Tela cheia",
                    showBlocks: "Mostrar blocos",
                    codeView: "Code view",
                    undo: "Undo",
                    redo: "Redo",
                    preview: "Preview",
                    print: "print",
                    tag_p: "Parágrafo",
                    tag_div: "Normal (DIV)",
                    tag_h: "Titulo",
                    tag_blockquote: "Citação",
                    tag_pre: "Código",
                    template: "Template"
                },
                dialogBox: {
                    linkBox: {
                        title: "Inserir Link",
                        url: "URL do link",
                        text: "Texto flutuante",
                        newWindowCheck: "Abrir em nova janela"
                    },
                    imageBox: {
                        title: "Inserir Imagem",
                        file: "Selecionar do arquivo",
                        url: "Link direto da imagem",
                        altText: "Texto flutuante"
                    },
                    videoBox: {
                        title: "Inserir video",
                        url: "Media embed URL, YouTube"
                    },
                    caption: "Descrição",
                    close: "Fechar",
                    submitButton: "Enviar",
                    revertButton: "Reverter",
                    proportion: "Manter proporção",
                    width: "Largura",
                    height: "Altura",
                    basic: "Em linha",
                    left: "Esquerda",
                    right: "Direita",
                    center: "Centro"
                },
                controller: {
                    edit: "Editar",
                    remove: "Remove",
                    insertRowAbove: "Inserir linha acima",
                    insertRowBelow: "Inserir linha abaixo",
                    deleteRow: "Remover linha",
                    insertColumnBefore: "Inserir coluna à esquerda",
                    insertColumnAfter: "Inserir coluna à direita",
                    deleteColumn: "Remover coluna",
                    resize100: "Resize 100%",
                    resize75: "Resize 75%",
                    resize50: "Resize 50%",
                    resize25: "Resize 25%",
                    mirrorHorizontal: "Espelhamento, Horizontal",
                    mirrorVertical: "Espelhamento, Vertical",
                    rotateLeft: "Girar esquerda",
                    rotateRight: "Girar direita",
                    maxSize: "Max size",
                    minSize: "Min size",
                    tableHeader: "Cabeçalho",
                    mergeCells: "Combinar celulas",
                    splitCells: "Separar celulas",
                    HorizontalSplit: "Horizontal split",
                    VerticalSplit: "Vertical split"
                }
            };
            return void 0 === t && (e.KEDITOR_LANG || (e.KEDITOR_LANG = {}), e.KEDITOR_LANG.en = i), i
        }, "object" == typeof e.exports ? e.exports = n.document ? o(n, !0) : function(e) {
            if (!e.document) throw new Error("KEDITOR_LANG a window with a document");
            return o(e)
        } : o(n)
    },
    tjUo: function(e, t, i) {
        "use strict";
        i.r(t);
        i("KJ7s"), i("KFuf");
        var n = {
                name: "colorPicker",
                add: function(e) {
                    const t = e.context;
                    t.colorPicker = {
                        colorListHTML: "",
                        _colorInput: "",
                        _defaultColor: "#000",
                        _styleProperty: "color",
                        _currentColor: "",
                        _colorList: []
                    };
                    let i = this.createColorList(e.context.option, e.lang, this._makeColorList);
                    t.colorPicker.colorListHTML = i, i = null
                },
                createColorList: function(e, t, i) {
                    const n = e.colorList && 0 !== e.colorList.length ? e.colorList : ["#ff0000", "#ff5e00", "#ffe400", "#abf200", "#00d8ff", "#0055ff", "#6600ff", "#ff00dd", "#000000", "#ffd8d8", "#fae0d4", "#faf4c0", "#e4f7ba", "#d4f4fa", "#d9e5ff", "#e8d9ff", "#ffd9fa", "#f1f1f1", "#ffa7a7", "#ffc19e", "#faed7d", "#cef279", "#b2ebf4", "#b2ccff", "#d1b2ff", "#ffb2f5", "#bdbdbd", "#f15f5f", "#f29661", "#e5d85c", "#bce55c", "#5cd1e5", "#6699ff", "#a366ff", "#f261df", "#8c8c8c", "#980000", "#993800", "#998a00", "#6b9900", "#008299", "#003399", "#3d0099", "#990085", "#353535", "#670000", "#662500", "#665c00", "#476600", "#005766", "#002266", "#290066", "#660058", "#222222"];
                    let o = [],
                        l = '<div class="ke-list-inner">';
                    for (let e = 0, t = n.length; e < t; e++) "string" == typeof n[e] && (o.push(n[e]), e < t - 1) || (o.length > 0 && (l += '<div class="ke-selector-color">' + i(o) + "</div>", o = []), "object" == typeof n[e] && (l += '<div class="ke-selector-color">' + i(n[e]) + "</div>"));
                    return l += '<form class="ke-submenu-form-group">   <input type="text" maxlength="7" class="_se_color_picker_input" />   <button type="submit" class="ke-btn-primary ke-tooltip _se_color_picker_submit">       <i class="ke-icon-checked"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + t.dialogBox.submitButton + '</span></span>   </button>   <button type="button" class="ke-btn ke-tooltip _se_color_picker_remove">       <i class="ke-icon-erase"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + t.toolbar.removeFormat + "</span></span>   </button></form></div>"
                },
                _makeColorList: function(e) {
                    let t = "";
                    t += '<ul class="ke-color-pallet">';
                    for (let i, n = 0, o = e.length; n < o; n++) "string" == typeof(i = e[n]) && (t += '<li>   <button type="button" data-value="' + i + '" title="' + i + '" style="background-color:' + i + ';"></button></li>');
                    return t += "</ul>"
                },
                init: function(e, t) {
                    const i = this.plugins.colorPicker;
                    let n = t || (i.getColorInNode.call(this, e) || this.context.colorPicker._defaultColor);
                    n = i.isHexColor(n) ? n : i.rgb2hex(n) || n;
                    const o = this.context.colorPicker._colorList;
                    if (o)
                        for (let e = 0, t = o.length; e < t; e++) n.toLowerCase() === o[e].getAttribute("data-value").toLowerCase() ? this.util.addClass(o[e], "active") : this.util.removeClass(o[e], "active");
                    i.setInputText.call(this, i.colorName2hex.call(this, n))
                },
                setCurrentColor: function(e) {
                    this.context.colorPicker._currentColor = e, this.context.colorPicker._colorInput.style.borderColor = e
                },
                setInputText: function(e) {
                    e = /^#/.test(e) ? e : "#" + e, this.context.colorPicker._colorInput.value = e, this.plugins.colorPicker.setCurrentColor.call(this, e)
                },
                getColorInNode: function(e) {
                    let t = "";
                    const i = this.context.colorPicker._styleProperty;
                    for (; e && !this.util.isWysiwygDiv(e) && 0 === t.length;) 1 === e.nodeType && e.style[i] && (t = e.style[i]), e = e.parentNode;
                    return t
                },
                isHexColor: function(e) {
                    return /^#[0-9a-f]{3}(?:[0-9a-f]{3})?$/i.test(e)
                },
                rgb2hex: function(e) {
                    return (e = e.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i)) && 4 === e.length ? "#" + ("0" + parseInt(e[1], 10).toString(16)).slice(-2) + ("0" + parseInt(e[2], 10).toString(16)).slice(-2) + ("0" + parseInt(e[3], 10).toString(16)).slice(-2) : ""
                },
                colorName2hex: function(e) {
                    if (/^#/.test(e)) return e;
                    var t = this.util.createElement("div");
                    t.style.display = "none", t.style.color = e;
                    var i = this._w.getComputedStyle(this._d.body.appendChild(t)).color.match(/\d+/g).map((function(e) {
                        return parseInt(e, 10)
                    }));
                    return this.util.removeItem(t), i.length >= 3 && "#" + ((1 << 24) + (i[0] << 16) + (i[1] << 8) + i[2]).toString(16).substr(1)
                }
            },
            o = {
                name: "dialog",
                add: function(e) {
                    const t = e.context;
                    t.dialog = {};
                    let i = e.util.createElement("DIV");
                    i.className = "ke-dialog keditor-common";
                    let n = e.util.createElement("DIV");
                    n.className = "ke-dialog-back", n.style.display = "none";
                    let o = e.util.createElement("DIV");
                    o.className = "ke-dialog-inner", o.style.display = "none", i.appendChild(n), i.appendChild(o), t.dialog.modalArea = i, t.dialog.back = n, t.dialog.modal = o, t.dialog.modal.addEventListener("click", this.onClick_dialog.bind(e)), t.element.relative.appendChild(i), i = null, n = null, o = null
                },
                onClick_dialog: function(e) {
                    e.stopPropagation(), (/ke-dialog-inner/.test(e.target.className) || /close/.test(e.target.getAttribute("data-command"))) && this.plugins.dialog.close.call(this)
                },
                open: function(e, t) {
                    if (this.modalForm) return !1;
                    this.plugins.dialog._bindClose && (this._d.removeEventListener("keydown", this.plugins.dialog._bindClose), this.plugins.dialog._bindClose = null), this.plugins.dialog._bindClose = function(e) {
                        /27/.test(e.keyCode) && this.plugins.dialog.close.call(this)
                    }.bind(this), this._d.addEventListener("keydown", this.plugins.dialog._bindClose), this.context.dialog.updateModal = t, "full" === this.context.option.popupDisplay ? this.context.dialog.modalArea.style.position = "fixed" : this.context.dialog.modalArea.style.position = "absolute", this.context.dialog.kind = e, this.modalForm = this.context[e].modal;
                    const i = this.context[e].focusElement;
                    "function" == typeof this.plugins[e].on && this.plugins[e].on.call(this, t), this.context.dialog.modalArea.style.display = "block", this.context.dialog.back.style.display = "block", this.context.dialog.modal.style.display = "block", this.modalForm.style.display = "block", i && i.focus()
                },
                _bindClose: null,
                close: function() {
                    this.plugins.dialog._bindClose && (this._d.removeEventListener("keydown", this.plugins.dialog._bindClose), this.plugins.dialog._bindClose = null), this.modalForm.style.display = "none", this.context.dialog.back.style.display = "none", this.context.dialog.modalArea.style.display = "none", this.modalForm = null, this.context.dialog.updateModal = !1, this.plugins[this.context.dialog.kind].init.call(this)
                }
            },
            l = {
                name: "resizing",
                add: function(e) {
                    const t = e.context;
                    t.resizing = {
                        _resizeClientX: 0,
                        _resizeClientY: 0,
                        _resize_plugin: "",
                        _resize_w: 0,
                        _resize_h: 0,
                        _origin_w: 0,
                        _origin_h: 0,
                        _rotateVertical: !1,
                        _resize_direction: "",
                        _move_path: null,
                        _isChange: !1
                    };
                    let i = this.setController_resize.call(e);
                    t.resizing.resizeContainer = i, t.resizing.resizeDiv = i.querySelector(".ke-modal-resize"), t.resizing.resizeDot = i.querySelector(".ke-resize-dot"), t.resizing.resizeDisplay = i.querySelector(".ke-resize-display");
                    let n = this.setController_button.call(e);
                    t.resizing.resizeButton = n, n.addEventListener("mousedown", (function(e) {
                        e.stopPropagation()
                    }), !1);
                    let o = t.resizing.resizeHandles = t.resizing.resizeDot.querySelectorAll("span");
                    t.resizing.resizeButtonGroup = n.querySelector("._se_resizing_btn_group"), t.resizing.alignMenu = n.querySelector(".ke-resizing-align-list"), t.resizing.alignMenuList = t.resizing.alignMenu.querySelectorAll("button"), t.resizing.alignButton = n.querySelector("._se_resizing_align_button"), t.resizing.alignButtonIcon = t.resizing.alignButton.querySelector("i"), t.resizing.captionButton = n.querySelector("._se_resizing_caption_button"), o[0].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(e)), o[1].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(e)), o[2].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(e)), o[3].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(e)), o[4].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(e)), o[5].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(e)), o[6].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(e)), o[7].addEventListener("mousedown", this.onMouseDown_resize_handle.bind(e)), n.addEventListener("click", this.onClick_resizeButton.bind(e)), t.element.relative.appendChild(i), t.element.relative.appendChild(n), i = null, n = null, o = null
                },
                setController_resize: function() {
                    const e = this.util.createElement("DIV");
                    return e.className = "ke-resizing-container", e.style.display = "none", e.innerHTML = '<div class="ke-modal-resize">   <div class="ke-resize-display"></div></div><div class="ke-resize-dot">   <span class="tl"></span>   <span class="tr"></span>   <span class="bl"></span>   <span class="br"></span>   <span class="lw"></span>   <span class="th"></span>   <span class="rw"></span>   <span class="bh"></span></div>', e
                },
                setController_button: function() {
                    const e = this.lang,
                        t = this.util.createElement("DIV");
                    return t.className = "ke-controller ke-controller-resizing", t.innerHTML = '<div class="ke-arrow ke-arrow-up"></div><div class="ke-btn-group _se_resizing_btn_group">   <button type="button" data-command="percent" data-value="1" class="ke-tooltip">       <span>100%</span>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.resize100 + '</span></span>   </button>   <button type="button" data-command="percent" data-value="0.75" class="ke-tooltip">       <span>75%</span>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.resize75 + '</span></span>   </button>   <button type="button" data-command="percent" data-value="0.5" class="ke-tooltip">       <span>50%</span>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.resize50 + '</span></span>   </button>   <button type="button" data-command="percent" data-value="0.25" class="ke-tooltip">       <span>25%</span>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.resize25 + '</span></span>   </button>   <button type="button" data-command="rotate" data-value="-90" class="ke-tooltip">       <i class="ke-icon-rotate-left"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.rotateLeft + '</span></span>   </button>   <button type="button" data-command="rotate" data-value="90" class="ke-tooltip">       <i class="ke-icon-rotate-right"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.rotateRight + '</span></span>   </button></div><div class="ke-btn-group">   <button type="button" data-command="mirror" data-value="h" class="ke-tooltip">       <i class="ke-icon-mirror-horizontal"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.mirrorHorizontal + '</span></span>   </button>   <button type="button" data-command="mirror" data-value="v" class="ke-tooltip">       <i class="ke-icon-mirror-vertical"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.mirrorVertical + '</span></span>   </button>   <button type="button" data-command="onalign" class="ke-tooltip _se_resizing_align_button">       <i class="ke-icon-align-justify"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.toolbar.align + '</span></span>   </button>   <div class="ke-btn-group-sub keditor-common ke-list-layer ke-resizing-align-list">       <div class="ke-list-inner">           <ul class="ke-list-basic">               <li><button type="button" class="ke-btn-list ke-tooltip" data-command="align" data-value="basic">                   <i class="ke-icon-align-justify"></i>                   <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.dialogBox.basic + '</span></span>               </button></li>               <li><button type="button" class="ke-btn-list ke-tooltip" data-command="align" data-value="left">                   <i class="ke-icon-align-left"></i>                   <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.dialogBox.left + '</span></span>               </button></li>               <li><button type="button" class="ke-btn-list ke-tooltip" data-command="align" data-value="center">                   <i class="ke-icon-align-center"></i>                   <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.dialogBox.center + '</span></span>               </button></li>               <li><button type="button" class="ke-btn-list ke-tooltip" data-command="align" data-value="right">                   <i class="ke-icon-align-right"></i>                   <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.dialogBox.right + '</span></span>               </button></li>           </ul>       </div>   </div>   <button type="button" data-command="caption" class="ke-tooltip _se_resizing_caption_button">       <i class="ke-icon-caption"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.dialogBox.caption + '</span></span>   </button>   <button type="button" data-command="revert" class="ke-tooltip">       <i class="ke-icon-revert"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.dialogBox.revertButton + '</span></span>   </button>   <button type="button" data-command="update" class="ke-tooltip">       <i class="ke-icon-modify"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.edit + '</span></span>   </button>   <button type="button" data-command="delete" class="ke-tooltip">       <i class="ke-icon-delete"></i>       <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.remove + "</span></span>   </button></div>", t
                },
                call_controller_resize: function(e, t) {
                    const i = this.context.resizing;
                    i._resize_plugin = t;
                    const n = i.resizeContainer,
                        o = i.resizeDiv,
                        l = this.util.getOffset(e, this.context.element.wysiwygFrame),
                        s = i._rotateVertical = /^(90|270)$/.test(Math.abs(e.getAttribute("data-rotate")).toString()),
                        a = s ? e.offsetHeight : e.offsetWidth,
                        r = s ? e.offsetWidth : e.offsetHeight,
                        c = l.top,
                        d = l.left - this.context.element.wysiwygFrame.scrollLeft;
                    n.style.top = c + "px", n.style.left = d + "px", n.style.width = a + "px", n.style.height = r + "px", o.style.top = "0px", o.style.left = "0px", o.style.width = a + "px", o.style.height = r + "px";
                    let u = e.getAttribute("data-align") || "basic";
                    u = "none" === u ? "basic" : u, this.util.changeTxt(i.resizeDisplay, this.lang.dialogBox[u] + " (" + a + " x " + r + ")");
                    const h = this.context[t]._resizing ? "flex" : "none",
                        g = i.resizeHandles;
                    i.resizeButtonGroup.style.display = h;
                    for (let e = 0, t = g.length; e < t; e++) g[e].style.display = h;
                    const p = i.alignMenuList;
                    this.util.removeClass(i.alignButtonIcon, "ke-icon-align\\-[a-z]+"), this.util.addClass(i.alignButtonIcon, "ke-icon-align-" + ("basic" === u ? "justify" : u));
                    for (let e = 0, t = p.length; e < t; e++) p[e].getAttribute("data-value") === u ? this.util.addClass(p[e], "on") : this.util.removeClass(p[e], "on");
                    this.util.getChildElement(e.parentNode, "figcaption") ? (this.util.addClass(i.captionButton, "active"), this.context[t]._captionChecked = !0) : (this.util.removeClass(i.captionButton, "active"), this.context[t]._captionChecked = !1), this._resizingName = t, this.controllersOn(i.resizeContainer, i.resizeButton);
                    const m = this.context.element.wysiwygFrame.offsetWidth - d - i.resizeButton.offsetWidth;
                    i.resizeButton.style.top = r + c + 60 + "px", i.resizeButton.style.left = d + (m < 0 ? m : 0) + "px", i.resizeButton.firstElementChild.style.left = m < 0 ? 20 - m + "px" : "20px", i._resize_w = a, i._resize_h = r;
                    const f = (e.getAttribute("origin-size") || "").split(",");
                    return i._origin_w = f[0] || e.naturalWidth, i._origin_h = f[1] || e.naturalHeight, {
                        w: a,
                        h: r,
                        t: c,
                        l: d
                    }
                },
                _closeAlignMenu: null,
                openAlignMenu: function() {
                    this.util.addClass(this.context.resizing.alignButton, "on"), this.context.resizing.alignMenu.style.display = "inline-table", this.plugins.resizing._closeAlignMenu = function() {
                        this.util.removeClass(this.context.resizing.alignButton, "on"), this.context.resizing.alignMenu.style.display = "none", this.removeDocEvent("mousedown", this.plugins.resizing._closeAlignMenu), this.plugins.resizing._closeAlignMenu = null
                    }.bind(this), this.addDocEvent("mousedown", this.plugins.resizing._closeAlignMenu)
                },
                create_caption: function() {
                    const e = this.util.createElement("FIGCAPTION");
                    return e.setAttribute("contenteditable", !0), e.innerHTML = "<div>" + this.lang.dialogBox.caption + "</div>", e
                },
                set_cover: function(e) {
                    const t = this.util.createElement("FIGURE");
                    return t.appendChild(e), t
                },
                set_container: function(e, t) {
                    const i = this.util.createElement("DIV");
                    return i.className = "ke-component " + t, i.setAttribute("contenteditable", !1), i.appendChild(e), i
                },
                onClick_resizeButton: function(e) {
                    e.stopPropagation();
                    const t = e.target,
                        i = t.getAttribute("data-command") || t.parentNode.getAttribute("data-command");
                    if (!i) return;
                    const n = t.getAttribute("data-value") || t.parentNode.getAttribute("data-value"),
                        o = this.context.resizing._resize_plugin,
                        l = this.context[o],
                        s = l._element,
                        a = this.plugins[o];
                    if (e.preventDefault(), "function" != typeof this.plugins.resizing._closeAlignMenu || (this.plugins.resizing._closeAlignMenu(), "onalign" !== i)) {
                        switch (i) {
                            case "percent":
                                this.plugins.resizing.resetTransform.call(this, s), a.setPercentSize.call(this, 100 * n + "%", "auto"), a.onModifyMode.call(this, s, this.plugins.resizing.call_controller_resize.call(this, s, o));
                                break;
                            case "mirror":
                                const e = s.getAttribute("data-rotate") || "0";
                                let t = s.getAttribute("data-rotateX") || "",
                                    i = s.getAttribute("data-rotateY") || "";
                                "h" === n && !this.context.resizing._rotateVertical || "v" === n && this.context.resizing._rotateVertical ? i = i ? "" : "180" : t = t ? "" : "180", s.setAttribute("data-rotateX", t), s.setAttribute("data-rotateY", i), this.plugins.resizing._setTransForm(s, e, t, i);
                                break;
                            case "rotate":
                                const r = this.context.resizing,
                                    c = 1 * s.getAttribute("data-rotate") + 1 * n,
                                    d = this._w.Math.abs(c) >= 360 ? 0 : c;
                                s.setAttribute("data-rotate", d), r._rotateVertical = /^(90|270)$/.test(this._w.Math.abs(d).toString()), this.plugins.resizing.setTransformSize.call(this, s, null, null), a.onModifyMode.call(this, s, this.plugins.resizing.call_controller_resize.call(this, s, r._resize_plugin));
                                break;
                            case "onalign":
                                this.plugins.resizing.openAlignMenu.call(this);
                                break;
                            case "align":
                                const u = "basic" === n ? "none" : n;
                                l._cover.style.margin = u && "none" !== u ? "auto" : "0", this.util.removeClass(l._container, l._floatClassRegExp), this.util.addClass(l._container, "__se__float-" + u), s.setAttribute("data-align", u), a.onModifyMode.call(this, s, this.plugins.resizing.call_controller_resize.call(this, s, o));
                                break;
                            case "caption":
                                const h = !l._captionChecked;
                                if (a.openModify.call(this, !0), l._captionChecked = l.captionCheckEl.checked = h, "image" === o ? a.update_image.call(this, !1, !1) : "video" === o && (this.context.dialog.updateModal = !0, a.submitAction.call(this)), h) {
                                    const e = this.util.getChildElement(l._caption, (function(e) {
                                        return 3 === e.nodeType
                                    }));
                                    e ? this.setRange(e, 0, e, e.textContent.length) : l._caption.focus(), this.controllersOff()
                                } else a.onModifyMode.call(this, s, this.plugins.resizing.call_controller_resize.call(this, s, o)), a.openModify.call(this, !0);
                                break;
                            case "revert":
                                a.setAutoSize ? a.setAutoSize.call(this) : (a.resetAlign.call(this), this.plugins.resizing.resetTransform.call(this, s)), a.onModifyMode.call(this, s, this.plugins.resizing.call_controller_resize.call(this, s, o));
                                break;
                            case "update":
                                a.openModify.call(this), this.controllersOff();
                                break;
                            case "delete":
                                a.destroy.call(this)
                        }
                        this.history.push()
                    }
                },
                resetTransform: function(e) {
                    const t = (e.getAttribute("data-origin") || "").split(",");
                    this.context.resizing._rotateVertical = !1, e.style.transform = "", e.style.transformOrigin = "", e.setAttribute("data-rotate", ""), e.setAttribute("data-rotateX", ""), e.setAttribute("data-rotateY", ""), e.style.width = t[0] ? t[0] + "px" : "auto", e.style.height = t[1] ? t[1] + "px" : "", this.plugins.resizing.setTransformSize.call(this, e, null, null)
                },
                setTransformSize: function(e, t, i) {
                    const n = this.util.getParentElement(e, "FIGURE"),
                        o = this.context.resizing._rotateVertical,
                        l = 1 * e.getAttribute("data-rotate"),
                        s = t || e.offsetWidth,
                        a = i || e.offsetHeight,
                        r = o ? a : s,
                        c = o ? s : a;
                    this.plugins[this.context.resizing._resize_plugin].cancelPercentAttr.call(this), this.plugins[this.context.resizing._resize_plugin].setSize.call(this, s, a), n.style.width = r + "px", n.style.height = this.context[this.context.resizing._resize_plugin]._caption ? "" : c + "px";
                    let d = "";
                    if (o) {
                        let e = s / 2 + "px " + s / 2 + "px 0",
                            t = a / 2 + "px " + a / 2 + "px 0";
                        d = 90 === l || -270 === l ? t : e
                    }
                    e.style.transformOrigin = d, this.plugins.resizing._setTransForm(e, l.toString(), e.getAttribute("data-rotateX") || "", e.getAttribute("data-rotateY") || ""), this.plugins.resizing._setCaptionPosition.call(this, e, this.util.getChildElement(this.util.getParentElement(e, "FIGURE"), "FIGCAPTION"))
                },
                _setTransForm: function(e, t, i, n) {
                    let o = (e.offsetWidth - e.offsetHeight) * (/-/.test(t) ? 1 : -1),
                        l = "";
                    if (/[1-9]/.test(t) && (i || n)) switch (l = i ? "Y" : "X", t) {
                        case "90":
                            l = i && n ? "X" : n ? l : "";
                            break;
                        case "270":
                            o *= -1, l = i && n ? "Y" : i ? l : "";
                            break;
                        case "-90":
                            l = i && n ? "Y" : i ? l : "";
                            break;
                        case "-270":
                            o *= -1, l = i && n ? "X" : n ? l : "";
                            break;
                        default:
                            l = ""
                    }
                    t % 180 == 0 && (e.style.maxWidth = "100%"), e.style.transform = "rotate(" + t + "deg)" + (i ? " rotateX(" + i + "deg)" : "") + (n ? " rotateY(" + n + "deg)" : "") + (l ? " translate" + l + "(" + o + "px)" : "")
                },
                _setCaptionPosition: function(e, t) {
                    t && (t.style.marginTop = (this.context.resizing._rotateVertical ? e.offsetWidth - e.offsetHeight : 0) + "px")
                },
                onMouseDown_resize_handle: function(e) {
                    const t = this.context.resizing,
                        i = t._resize_direction = e.target.classList[0];
                    e.stopPropagation(), e.preventDefault(), t._resizeClientX = e.clientX, t._resizeClientY = e.clientY, this.context.element.resizeBackground.style.display = "block", t.resizeButton.style.display = "none", t.resizeDiv.style.float = /l/.test(i) ? "right" : /r/.test(i) ? "left" : "none";
                    const n = function() {
                            const e = t._isChange;
                            t._isChange = !1, document.removeEventListener("mousemove", o), document.removeEventListener("mouseup", n), this.plugins.resizing.cancel_controller_resize.call(this), e && this.history.push()
                        }.bind(this),
                        o = this.plugins.resizing.resizing_element.bind(this, t, i, this.context[t._resize_plugin]);
                    document.addEventListener("mousemove", o), document.addEventListener("mouseup", n)
                },
                resizing_element: function(e, t, i, n) {
                    const o = n.clientX,
                        l = n.clientY;
                    let s = i._element_w,
                        a = i._element_h;
                    const r = i._element_w + (/r/.test(t) ? o - e._resizeClientX : e._resizeClientX - o),
                        c = i._element_h + (/b/.test(t) ? l - e._resizeClientY : e._resizeClientY - l),
                        d = i._element_h / i._element_w * r;
                    /t/.test(t) && (e.resizeDiv.style.top = i._element_h - (/h/.test(t) ? c : d) + "px"), /l/.test(t) && (e.resizeDiv.style.left = i._element_w - r + "px"), /r|l/.test(t) && (e.resizeDiv.style.width = r + "px", s = r), /^(t|b)[^h]$/.test(t) ? (e.resizeDiv.style.height = d + "px", a = d) : /^(t|b)h$/.test(t) && (e.resizeDiv.style.height = c + "px", a = c), e._resize_w = s, e._resize_h = a, this.util.changeTxt(e.resizeDisplay, this._w.Math.round(s) + " x " + this._w.Math.round(a)), e._isChange = !0
                },
                cancel_controller_resize: function() {
                    const e = this.context.resizing._rotateVertical;
                    this.controllersOff(), this.context.element.resizeBackground.style.display = "none";
                    let t = this._w.Math.round(e ? this.context.resizing._resize_h : this.context.resizing._resize_w),
                        i = this._w.Math.round(e ? this.context.resizing._resize_w : this.context.resizing._resize_h);
                    if (!e && !/^\d+%$/.test(t)) {
                        const e = 16,
                            n = this.context.element.wysiwygFrame.clientWidth - 2 * e - 2;
                        t.toString().match(/\d+/)[0] > n && (t = n, i = "video" === this.context.resizing._resize_plugin ? i / t * n : "auto")
                    }
                    this.plugins[this.context.resizing._resize_plugin].setSize.call(this, t, i), this.plugins.resizing.setTransformSize.call(this, this.context[this.context.resizing._resize_plugin]._element, t, i), this.plugins[this.context.resizing._resize_plugin].init.call(this)
                }
            },
            s = {
                name: "notice",
                add: function(e) {
                    const t = e.context;
                    t.notice = {};
                    let i = e.util.createElement("DIV"),
                        n = e.util.createElement("SPAN"),
                        o = e.util.createElement("BUTTON");
                    i.className = "ke-notice", o.className = "close", o.setAttribute("aria-label", "Close"), o.setAttribute("title", e.lang.dialogBox.close), o.innerHTML = '<i aria-hidden="true" data-command="close" class="ke-icon-cancel"></i>', i.appendChild(n), i.appendChild(o), t.notice.modal = i, t.notice.message = n, o.addEventListener("click", this.onClick_cancel.bind(e)), t.element.editorArea.insertBefore(i, t.element.wysiwygFrame), i = null
                },
                onClick_cancel: function(e) {
                    e.preventDefault(), e.stopPropagation(), this.plugins.notice.close.call(this)
                },
                open: function(e) {
                    this.context.notice.message.textContent = e, this.context.notice.modal.style.display = "block"
                },
                close: function() {
                    this.context.notice.modal.style.display = "none"
                }
            },
            a = {
                align: {
                    name: "align",
                    add: function(e, t) {
                        const i = e.context;
                        i.align = {
                            _alignList: null,
                            currentAlign: ""
                        };
                        let n = this.setSubmenu.call(e),
                            o = n.querySelector("ul");
                        o.addEventListener("click", this.pickup.bind(e)), i.align._alignList = o.querySelectorAll("li button"), t.parentNode.appendChild(n), n = null, o = null
                    },
                    setSubmenu: function() {
                        const e = this.lang,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-list-layer", t.innerHTML = '<div class="ke-submenu ke-list-inner ke-list-align">   <ul class="ke-list-basic">       <li><button type="button" class="ke-btn-list ke-btn-align" data-command="justifyleft" data-value="left" title="' + e.toolbar.alignLeft + '"><span class="ke-icon-align-left"></span>' + e.toolbar.alignLeft + '</button></li>       <li><button type="button" class="ke-btn-list ke-btn-align" data-command="justifycenter" data-value="center" title="' + e.toolbar.alignCenter + '"><span class="ke-icon-align-center"></span>' + e.toolbar.alignCenter + '</button></li>       <li><button type="button" class="ke-btn-list ke-btn-align" data-command="justifyright" data-value="right" title="' + e.toolbar.alignRight + '"><span class="ke-icon-align-right"></span>' + e.toolbar.alignRight + '</button></li>       <li><button type="button" class="ke-btn-list ke-btn-align" data-command="justifyfull" data-value="justify" title="' + e.toolbar.alignJustify + '"><span class="ke-icon-align-justify"></span>' + e.toolbar.alignJustify + "</button></li>   </ul></div>", t
                    },
                    on: function() {
                        const e = this.context.align,
                            t = e._alignList,
                            i = this.commandMap.ALIGN.getAttribute("data-focus") || "left";
                        if (i !== e.currentAlign) {
                            for (let e = 0, n = t.length; e < n; e++) i === t[e].getAttribute("data-value") ? this.util.addClass(t[e], "active") : this.util.removeClass(t[e], "active");
                            e.currentAlign = i
                        }
                    },
                    pickup: function(e) {
                        e.preventDefault(), e.stopPropagation();
                        let t = e.target,
                            i = null;
                        for (; !i && !/UL/i.test(t.tagName);) i = t.getAttribute("data-command"), t = t.parentNode;
                        i && (this.execCommand(i, !1, null), this.submenuOff())
                    }
                },
                font: {
                    name: "font",
                    add: function(e, t) {
                        const i = e.context;
                        i.font = {
                            _fontList: null,
                            currentFont: ""
                        };
                        let n = this.setSubmenu.call(e);
                        n.querySelector(".ke-list-font-family").addEventListener("click", this.pickup.bind(e)), i.font._fontList = n.querySelectorAll("ul li button"), t.parentNode.appendChild(n), n = null
                    },
                    setSubmenu: function() {
                        const e = this.context.option,
                            t = this.lang,
                            i = this.util.createElement("DIV");
                        let n, o, l, s;
                        i.className = "ke-list-layer";
                        let a = e.font ? e.font : ["Arial", "Comic Sans MS", "Courier New", "Impact", "Georgia", "tahoma", "Trebuchet MS", "Verdana"],
                            r = '<div class="ke-submenu ke-list-inner ke-list-font-family">   <ul class="ke-list-basic">       <li><button type="button" class="default_value ke-btn-list" title="' + t.toolbar.default+'">(' + t.toolbar.default+")</button></li>";
                        for (l = 0, s = a.length; l < s; l++) o = (n = a[l]).split(",")[0], r += '<li><button type="button" class="ke-btn-list" data-value="' + n + '" data-txt="' + o + '" title="' + o + '" style="font-family:' + n + ';">' + o + "</button></li>";
                        return r += "   </ul>", r += "</div>", i.innerHTML = r, i
                    },
                    on: function() {
                        const e = this.context.font,
                            t = e._fontList,
                            i = this.commandMap.FONT.textContent;
                        if (i !== e.currentFont) {
                            for (let e = 0, n = t.length; e < n; e++) i === t[e].getAttribute("data-value") ? this.util.addClass(t[e], "active") : this.util.removeClass(t[e], "active");
                            e.currentFont = i
                        }
                    },
                    pickup: function(e) {
                        if (!/^BUTTON$/i.test(e.target.tagName)) return !1;
                        e.preventDefault(), e.stopPropagation();
                        const t = e.target.getAttribute("data-value");
                        if (t) {
                            const e = this.util.createElement("SPAN");
                            e.style.fontFamily = t, this.nodeChange(e, ["font-family"])
                        } else this.nodeChange(null, ["font-family"]);
                        this.submenuOff()
                    }
                },
                fontSize: {
                    name: "fontSize",
                    add: function(e, t) {
                        const i = e.context;
                        i.fontSize = {
                            _sizeList: null,
                            currentSize: ""
                        };
                        let n = this.setSubmenu.call(e),
                            o = n.querySelector("ul");
                        o.addEventListener("click", this.pickup.bind(e)), i.fontSize._sizeList = o.querySelectorAll("li button"), t.parentNode.appendChild(n), n = null, o = null
                    },
                    setSubmenu: function() {
                        const e = this.context.option,
                            t = this.lang,
                            i = this.util.createElement("DIV");
                        i.className = "ke-submenu ke-list-layer";
                        const n = e.fontSize ? e.fontSize : [8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 26, 28, 36, 48, 72];
                        let o = '<div class="ke-list-inner ke-list-font-size">   <ul class="ke-list-basic">       <li><button type="button" class="default_value ke-btn-list" title="' + t.toolbar.default+'">(' + t.toolbar.default+")</button></li>";
                        for (let e = 0, t = n.length; e < t; e++) {
                            const t = n[e];
                            o += '<li><button type="button" class="ke-btn-list" data-value="' + t + '" title="' + t + '" style="font-size:' + t + 'px;">' + t + "</button></li>"
                        }
                        return o += "   </ul></div>", i.innerHTML = o, i
                    },
                    on: function() {
                        const e = this.context.fontSize,
                            t = e._sizeList,
                            i = (this.commandMap.SIZE.textContent.match(/\d+/) || [""])[0];
                        if (i !== e.currentSize) {
                            for (let e = 0, n = t.length; e < n; e++) i === t[e].getAttribute("data-value") ? this.util.addClass(t[e], "active") : this.util.removeClass(t[e], "active");
                            e.currentSize = i
                        }
                    },
                    pickup: function(e) {
                        if (!/^BUTTON$/i.test(e.target.tagName)) return !1;
                        e.preventDefault(), e.stopPropagation();
                        const t = e.target.getAttribute("data-value");
                        if (t) {
                            const e = this.util.createElement("SPAN");
                            e.style.fontSize = t + "px", this.nodeChange(e, ["font-size"])
                        } else this.nodeChange(null, ["font-size"]);
                        this.submenuOff()
                    }
                },
                fontColor: {
                    name: "fontColor",
                    add: function(e, t) {
                        e.addModule([n]);
                        const i = e.context;
                        i.fontColor = {
                            previewEl: null,
                            colorInput: null,
                            colorList: null
                        };
                        let o = this.setSubmenu.call(e);
                        i.fontColor.colorInput = o.querySelector("._se_color_picker_input"), i.fontColor.colorInput.addEventListener("keyup", this.onChangeInput.bind(e)), o.querySelector("._se_color_picker_submit").addEventListener("click", this.submit.bind(e)), o.querySelector("._se_color_picker_remove").addEventListener("click", this.remove.bind(e)), o.addEventListener("click", this.pickup.bind(e)), i.fontColor.colorList = o.querySelectorAll("li button"), t.parentNode.appendChild(o), o = null
                    },
                    setSubmenu: function() {
                        const e = this.context.colorPicker.colorListHTML,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-submenu ke-list-layer", t.innerHTML = e, t
                    },
                    on: function() {
                        const e = this.context.colorPicker,
                            t = this.context.fontColor;
                        e._colorInput = t.colorInput, e._defaultColor = "#333333", e._styleProperty = "color", e._colorList = t.colorList, this.plugins.colorPicker.init.call(this, this.getSelectionNode(), null)
                    },
                    onChangeInput: function(e) {
                        this.plugins.colorPicker.setCurrentColor.call(this, e.target.value)
                    },
                    remove: function() {
                        this.nodeChange(null, ["color"]), this.submenuOff(), this.focus()
                    },
                    submit: function() {
                        this.plugins.fontColor.applyColor.call(this, this.context.colorPicker._currentColor)
                    },
                    pickup: function(e) {
                        e.preventDefault(), e.stopPropagation(), this.plugins.fontColor.applyColor.call(this, e.target.getAttribute("data-value"))
                    },
                    applyColor: function(e) {
                        if (!e) return;
                        const t = this.util.createElement("SPAN");
                        t.style.color = e, this.nodeChange(t, ["color"]), this.submenuOff()
                    }
                },
                hiliteColor: {
                    name: "hiliteColor",
                    add: function(e, t) {
                        e.addModule([n]);
                        const i = e.context;
                        i.hiliteColor = {
                            previewEl: null,
                            colorInput: null,
                            colorList: null
                        };
                        let o = this.setSubmenu.call(e);
                        i.hiliteColor.colorInput = o.querySelector("._se_color_picker_input"), i.hiliteColor.colorInput.addEventListener("keyup", this.onChangeInput.bind(e)), o.querySelector("._se_color_picker_submit").addEventListener("click", this.submit.bind(e)), o.querySelector("._se_color_picker_remove").addEventListener("click", this.remove.bind(e)), o.addEventListener("click", this.pickup.bind(e)), i.hiliteColor.colorList = o.querySelectorAll("li button"), t.parentNode.appendChild(o), o = null
                    },
                    setSubmenu: function() {
                        const e = this.context.colorPicker.colorListHTML,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-submenu ke-list-layer", t.innerHTML = e, t
                    },
                    on: function() {
                        const e = this.context.colorPicker,
                            t = this.context.hiliteColor;
                        e._colorInput = t.colorInput, e._defaultColor = "#FFFFFF", e._styleProperty = "backgroundColor", e._colorList = t.colorList, this.plugins.colorPicker.init.call(this, this.getSelectionNode(), null)
                    },
                    onChangeInput: function(e) {
                        this.plugins.colorPicker.setCurrentColor.call(this, e.target.value)
                    },
                    remove: function() {
                        this.nodeChange(null, ["background-color"]), this.submenuOff(), this.focus()
                    },
                    submit: function() {
                        this.plugins.hiliteColor.applyColor.call(this, this.context.colorPicker._currentColor)
                    },
                    pickup: function(e) {
                        e.preventDefault(), e.stopPropagation(), this.plugins.hiliteColor.applyColor.call(this, e.target.getAttribute("data-value"))
                    },
                    applyColor: function(e) {
                        if (!e) return;
                        const t = this.util.createElement("SPAN");
                        t.style.backgroundColor = e, this.nodeChange(t, ["background-color"]), this.submenuOff()
                    }
                },
                horizontalRule: {
                    name: "horizontalRule",
                    add: function(e, t) {
                        let i = this.setSubmenu.call(e);
                        i.querySelector("ul").addEventListener("click", this.horizontalRulePick.bind(e)), t.parentNode.appendChild(i), i = null
                    },
                    setSubmenu: function() {
                        const e = this.lang,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-submenu ke-list-layer", t.innerHTML = '<div class="ke-list-inner ke-list-line">   <ul class="ke-list-basic">       <li>           <button type="button" class="ke-btn-list btn_line ke-tooltip" data-command="horizontalRule" data-value="solid">               <hr style="border-width: 1px 0 0; border-style: solid none none; border-color: black; border-image: initial; height: 1px;" />               <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.toolbar.hr_solid + '</span></span>           </button>       </li>       <li>           <button type="button" class="ke-btn-list btn_line ke-tooltip" data-command="horizontalRule" data-value="dotted">               <hr style="border-width: 1px 0 0; border-style: dotted none none; border-color: black; border-image: initial; height: 1px;" />               <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.toolbar.hr_dotted + '</span></span>           </button>       </li>       <li>           <button type="button" class="ke-btn-list btn_line ke-tooltip" data-command="horizontalRule" data-value="dashed">               <hr style="border-width: 1px 0 0; border-style: dashed none none; border-color: black; border-image: initial; height: 1px;" />               <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.toolbar.hr_dashed + "</span></span>           </button>       </li>   </ul></div>", t
                    },
                    appendHr: function(e) {
                        const t = this.util.createElement("HR");
                        t.className = e, this.focus();
                        let i = this.insertComponent(t);
                        this.setRange(i, 0, i, 0)
                    },
                    horizontalRulePick: function(e) {
                        e.preventDefault(), e.stopPropagation();
                        let t = e.target,
                            i = null;
                        for (; !i && !/UL/i.test(t.tagName);) i = t.getAttribute("data-value"), t = t.parentNode;
                        i && (this.plugins.horizontalRule.appendHr.call(this, "__se__" + i), this.submenuOff())
                    }
                },
                list: {
                    name: "list",
                    add: function(e, t) {
                        const i = e.context;
                        i.list = {
                            _list: null,
                            currentList: ""
                        };
                        let n = this.setSubmenu.call(e),
                            o = n.querySelector("ul");
                        o.addEventListener("click", this.pickup.bind(e)), i.list._list = o.querySelectorAll("li button"), t.parentNode.appendChild(n), n = null, o = null
                    },
                    setSubmenu: function() {
                        const e = this.lang,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-submenu ke-list-layer", t.innerHTML = '<div class="ke-list-inner">   <ul class="ke-list-basic">       <li><button type="button" class="ke-btn-list ke-tooltip" data-command="OL">           <i class="ke-icon-list-number"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.toolbar.orderList + '</span></span>       </button></li>       <li><button type="button" class="ke-btn-list ke-tooltip" data-command="UL">           <i class="ke-icon-list-bullets"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.toolbar.unorderList + "</span></span>       </button></li>   </ul></div>", t
                    },
                    on: function() {
                        const e = this.context.list,
                            t = e._list,
                            i = this.commandMap.LI.getAttribute("data-focus") || "";
                        if (i !== e.currentList) {
                            for (let e = 0, n = t.length; e < n; e++) i === t[e].getAttribute("data-command") ? this.util.addClass(t[e], "active") : this.util.removeClass(t[e], "active");
                            e.currentList = i
                        }
                    },
                    pickup: function(e) {
                        e.preventDefault(), e.stopPropagation();
                        let t = e.target,
                            i = "";
                        for (; !i && !/^UL$/i.test(t.tagName);) i = t.getAttribute("data-command"), t = t.parentNode;
                        if (!i) return;
                        const n = this.getSelectedElementsAndComponents();
                        if (!n || 0 === n.length) return;
                        let o = !0,
                            l = null,
                            s = null;
                        const a = n[0],
                            r = n[n.length - 1];
                        let c = !this.util.isListCell(a) && !this.util.isComponent(a) || a.previousElementSibling ? a.previousElementSibling : a.parentNode.previousElementSibling,
                            d = !this.util.isListCell(r) && !this.util.isComponent(r) || r.nextElementSibling ? r.nextElementSibling : r.parentNode.nextElementSibling;
                        for (let e = 0, t = n.length; e < t; e++)
                            if (!this.util.isList(this.util.getRangeFormatElement(n[e], function(t) {
                                    return this.getRangeFormatElement(t) && t !== n[e]
                                }.bind(this.util)))) {
                                o = !1;
                                break
                            } if (!o || c && i === c.tagName || d && i === d.tagName) {
                            const e = c ? c.parentNode : c,
                                t = d ? d.parentNode : d;
                            c = e && !this.util.isWysiwygDiv(e) && e.nodeName === i ? e : c, d = t && !this.util.isWysiwygDiv(t) && t.nodeName === i ? t : d;
                            const o = c && c.tagName === i,
                                a = d && d.tagName === i;
                            let r = o ? c : this.util.createElement(i),
                                u = null,
                                h = null,
                                g = null,
                                p = null;
                            const m = function(e) {
                                return !this.isComponent(e) && !this.isList(e)
                            }.bind(this.util);
                            for (let e, t, l, s, a, c, d, p, f, _ = 0, b = n.length; _ < b; _++)
                                if (0 !== (t = n[_]).childNodes.length || this.util.isIgnoreNodeChange(t)) {
                                    if (s = n[_ + 1], a = t.parentNode, c = s ? s.parentNode : null, l = this.util.isListCell(t), f = this.util.isRangeFormatElement(a) ? a : null, d = l && !this.util.isWysiwygDiv(a) ? a.parentNode : a, p = l && !this.util.isWysiwygDiv(a) ? s ? a.nextSibling : a : t.nextSibling, e = this.util.createElement("LI"), this.util.isComponent(t)) {
                                        const i = /^HR$/i.test(t.nodeName);
                                        i || (e.innerHTML = "<br>"), e.innerHTML += t.outerHTML, i && (e.innerHTML += "<br>")
                                    } else e.innerHTML = t.innerHTML;
                                    r.appendChild(e), s || (h = r), s && d === c && !this.util.isRangeFormatElement(p) || (u || (u = r), o && s && d === c || s && this.util.isList(c) && c === a || r.parentNode !== d && d.insertBefore(r, p)), this.util.removeItem(t), o && null === g && (g = r.children.length - 1), s && this.util.getRangeFormatElement(c, m) !== this.util.getRangeFormatElement(a, m) && (r = this.util.createElement(i)), f && 0 === f.children.length && this.util.removeItem(f)
                                } else this.util.removeItem(t);
                            g && (u = u.children[g]), a && (p = r.children.length - 1, r.innerHTML += d.innerHTML, h = r.children[p], this.util.removeItem(d)), l = s = this.util.getEdgeChildNodes(u.firstChild, h.lastChild)
                        } else {
                            const e = this.util.getRangeFormatElement(this.getSelectionNode()),
                                t = e && e.tagName === i;
                            let o, a;
                            const r = function(e) {
                                return !this.isComponent(e)
                            }.bind(this.util);
                            t || (a = this.util.createElement(i));
                            for (let e, c, d = 0, u = n.length; d < u; d++)
                                if ((c = this.util.getRangeFormatElement(n[d], r)) && this.util.isList(c)) {
                                    if (e)
                                        if (e !== c) {
                                            const s = this.detachRangeFormatElement(o.r, o.f, a, !1, !0);
                                            l || (l = s), t || (a = this.util.createElement(i)), o = {
                                                r: e = c,
                                                f: [this.util.getParentElement(n[d], "LI")]
                                            }
                                        } else o.f.push(this.util.getParentElement(n[d], "LI"));
                                    else o = {
                                        r: e = c,
                                        f: [this.util.getParentElement(n[d], "LI")]
                                    };
                                    d === u - 1 && (s = this.detachRangeFormatElement(o.r, o.f, a, !1, !0), l || (l = s))
                                }
                        }
                        this.history.push(), n.length > 1 ? this.setRange(l.sc, 0, s.ec, s.ec.textContent.length) : this.setRange(l.ec, l.ec.textContent.length, s.ec, s.ec.textContent.length), this.submenuOff()
                    }
                },
                table: {
                    name: "table",
                    add: function(e, t) {
                        const i = e.context;
                        i.table = {
                            _element: null,
                            _tdElement: null,
                            _trElement: null,
                            _trElements: null,
                            _tableXY: [],
                            _maxWidth: !0,
                            resizeIcon: null,
                            resizeText: null,
                            headerButton: null,
                            mergeButton: null,
                            splitButton: null,
                            splitMenu: null,
                            maxText: e.lang.controller.maxSize,
                            minText: e.lang.controller.minSize,
                            _physical_cellCnt: 0,
                            _logical_cellCnt: 0,
                            _rowCnt: 0,
                            _rowIndex: 0,
                            _physical_cellIndex: 0,
                            _logical_cellIndex: 0,
                            _current_colSpan: 0,
                            _current_rowSpan: 0
                        };
                        let n = this.setSubmenu.call(e),
                            o = n.querySelector(".ke-controller-table-picker");
                        i.table.tableHighlight = n.querySelector(".ke-table-size-highlighted"), i.table.tableUnHighlight = n.querySelector(".ke-table-size-unhighlighted"), i.table.tableDisplay = n.querySelector(".ke-table-size-display");
                        let l = this.setController_table.call(e);
                        i.table.tableController = l, i.table.resizeIcon = l.querySelector("._se_table_resize > i"), i.table.resizeText = l.querySelector("._se_table_resize > span > span"), i.table.headerButton = l.querySelector("._se_table_header"), l.addEventListener("mousedown", (function(e) {
                            e.stopPropagation()
                        }), !1);
                        let s = this.setController_tableEditor.call(e);
                        i.table.resizeDiv = s, i.table.splitMenu = s.querySelector(".ke-btn-group-sub"), i.table.mergeButton = s.querySelector("._se_table_merge_button"), i.table.splitButton = s.querySelector("._se_table_split_button"), s.addEventListener("mousedown", (function(e) {
                            e.stopPropagation()
                        }), !1), o.addEventListener("mousemove", this.onMouseMove_tablePicker.bind(e)), o.addEventListener("click", this.appendTable.bind(e)), s.addEventListener("click", this.onClick_tableController.bind(e)), l.addEventListener("click", this.onClick_tableController.bind(e)), t.parentNode.appendChild(n), i.element.relative.appendChild(s), i.element.relative.appendChild(l), n = null, o = null, s = null, l = null
                    },
                    setSubmenu: function() {
                        const e = this.util.createElement("DIV");
                        return e.className = "ke-submenu ke-selector-table", e.innerHTML = '<div class="ke-table-size">   <div class="ke-table-size-picker ke-controller-table-picker"></div>   <div class="ke-table-size-highlighted"></div>   <div class="ke-table-size-unhighlighted"></div></div><div class="ke-table-size-display">1 x 1</div>', e
                    },
                    setController_table: function() {
                        const e = this.lang,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-controller ke-controller-table", t.innerHTML = '<div>   <div class="ke-btn-group">       <button type="button" data-command="resize" class="ke-tooltip _se_table_resize">           <i class="ke-icon-expansion"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.maxSize + '</span></span>       </button>       <button type="button" data-command="header" class="ke-tooltip _se_table_header">           <i class="ke-icon-table-header"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.tableHeader + '</span></span>       </button>       <button type="button" data-command="remove" class="ke-tooltip">           <i class="ke-icon-delete"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.remove + "</span></span>       </button>   </div></div>", t
                    },
                    setController_tableEditor: function() {
                        const e = this.lang,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-controller ke-controller-table-cell", t.innerHTML = '<div class="ke-arrow ke-arrow-up"></div><div>   <div class="ke-btn-group">       <button type="button" data-command="insert" data-value="row" data-option="up" class="ke-tooltip">           <i class="ke-icon-insert-row-above"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.insertRowAbove + '</span></span>       </button>       <button type="button" data-command="insert" data-value="row" data-option="down" class="ke-tooltip">           <i class="ke-icon-insert-row-below"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.insertRowBelow + '</span></span>       </button>       <button type="button" data-command="delete" data-value="row" class="ke-tooltip">           <i class="ke-icon-delete-row"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.deleteRow + '</span></span>       </button>       <button type="button" data-command="merge" class="_se_table_merge_button ke-tooltip" disabled>           <i class="ke-icon-merge-cell"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.mergeCells + '</span></span>       </button>   </div></div><div>   <div class="ke-btn-group">     <button type="button" data-command="insert" data-value="cell" data-option="left" class="ke-tooltip">       <i class="ke-icon-insert-column-left"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.insertColumnBefore + '</span></span>       </button>       <button type="button" data-command="insert" data-value="cell" data-option="right" class="ke-tooltip">           <i class="ke-icon-insert-column-right"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.insertColumnAfter + '</span></span>       </button>       <button type="button" data-command="delete" data-value="cell" class="ke-tooltip">           <i class="ke-icon-delete-column"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.deleteColumn + '</span></span>       </button>       <button type="button" data-command="onsplit" class="_se_table_split_button ke-tooltip">           <i class="ke-icon-split-cell"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.splitCells + '</span></span>       </button>       <div class="ke-btn-group-sub keditor-common ke-list-layer">           <div class="ke-list-inner">               <ul class="ke-list-basic">                   <li class="ke-btn-list" data-command="split" data-value="vertical" style="line-height:32px;" title="' + e.controller.VerticalSplit + '">                   ' + e.controller.VerticalSplit + '</li>                   <li class="ke-btn-list" data-command="split" data-value="horizontal" style="line-height:32px;" title="' + e.controller.HorizontalSplit + '">                   ' + e.controller.HorizontalSplit + "</li>               </ul>           </div>       </div>   </div></div>", t
                    },
                    appendTable: function() {
                        const e = this.util.createElement("TABLE"),
                            t = this.plugins.table.createCells,
                            i = this.context.table._tableXY[0];
                        let n = this.context.table._tableXY[1],
                            o = "<tbody>";
                        for (; n > 0;) o += "<tr>" + t.call(this, "td", i) + "</tr>", --n;
                        o += "</tbody>", e.innerHTML = o, this.insertComponent(e), this.focus(), this.plugins.table.reset_table_picker.call(this)
                    },
                    createCells: function(e, t, i) {
                        if (e = e.toLowerCase(), i) {
                            const t = this.util.createElement(e);
                            return t.innerHTML = "<br>", t
                        } {
                            let i = "";
                            for (; t > 0;) i += "<" + e + "><br></" + e + ">", t--;
                            return i
                        }
                    },
                    onMouseMove_tablePicker: function(e) {
                        e.stopPropagation();
                        let t = this._w.Math.ceil(e.offsetX / 18),
                            i = this._w.Math.ceil(e.offsetY / 18);
                        t = t < 1 ? 1 : t, i = i < 1 ? 1 : i, this.context.table.tableHighlight.style.width = t + "em", this.context.table.tableHighlight.style.height = i + "em";
                        let n = t < 5 ? 5 : t > 9 ? 10 : t + 1,
                            o = i < 5 ? 5 : i > 9 ? 10 : i + 1;
                        this.context.table.tableUnHighlight.style.width = n + "em", this.context.table.tableUnHighlight.style.height = o + "em", this.util.changeTxt(this.context.table.tableDisplay, t + " x " + i), this.context.table._tableXY = [t, i]
                    },
                    reset_table_picker: function() {
                        if (!this.context.table.tableHighlight) return;
                        const e = this.context.table.tableHighlight.style,
                            t = this.context.table.tableUnHighlight.style;
                        e.width = "1em", e.height = "1em", t.width = "5em", t.height = "5em", this.util.changeTxt(this.context.table.tableDisplay, "1 x 1"), this.submenuOff()
                    },
                    init: function() {
                        const e = this.context.table,
                            t = this.plugins.table;
                        if (t._removeEvents.call(this), t._selectedTable) {
                            const e = t._selectedTable.querySelectorAll(".ke-table-selected-cell");
                            for (let t = 0, i = e.length; t < i; t++) this.util.removeClass(e[t], "ke-table-selected-cell")
                        }
                        t._toggleEditor.call(this, !0), e._element = null, e._tdElement = null, e._trElement = null, e._trElements = null, e._tableXY = [], e._maxWidth = !0, e._physical_cellCnt = 0, e._logical_cellCnt = 0, e._rowCnt = 0, e._rowIndex = 0, e._physical_cellIndex = 0, e._logical_cellIndex = 0, e._current_colSpan = 0, e._current_rowSpan = 0, t._shift = !1, t._selectedCells = null, t._selectedTable = null, t._ref = null, t._fixedCell = null, t._selectedCell = null, t._fixedCellName = null
                    },
                    call_controller_tableEdit: function(e) {
                        const t = this.context.table,
                            i = this.plugins.table,
                            n = t.tableController;
                        i.setPositionControllerDiv.call(this, e, i._shift);
                        const o = t._element,
                            l = this.util.getOffset(o, this.context.element.wysiwygFrame);
                        t._maxWidth = !o.style.width || "100%" === o.style.width, i.resizeTable.call(this), n.style.left = l.left + "px", n.style.display = "block", n.style.top = l.top - n.offsetHeight - 2 + "px", i._shift || this.controllersOn(t.resizeDiv, n, i.init.bind(this))
                    },
                    setPositionControllerDiv: function(e, t) {
                        const i = this.context.table.resizeDiv;
                        this.plugins.table.setCellInfo.call(this, e, t), i.style.display = "block";
                        const n = this.util.getOffset(e, this.context.element.wysiwygFrame);
                        i.style.left = n.left - this.context.element.wysiwygFrame.scrollLeft + "px", i.style.top = n.top + e.offsetHeight + 12 + "px";
                        const o = this.context.element.wysiwygFrame.offsetWidth - (i.offsetLeft + i.offsetWidth);
                        o < 0 ? (i.style.left = i.offsetLeft + o + "px", i.firstElementChild.style.left = 20 - o + "px") : i.firstElementChild.style.left = "20px"
                    },
                    setCellInfo: function(e, t) {
                        const i = this.context.table,
                            n = i._element = this.plugins.table._selectedTable || this.util.getParentElement(e, "TABLE");
                        if (/THEAD/i.test(n.firstElementChild.nodeName) ? this.util.addClass(i.headerButton, "active") : this.util.removeClass(i.headerButton, "active"), t || 0 === i._physical_cellCnt) {
                            i._tdElement !== e && (i._tdElement = e, i._trElement = e.parentNode);
                            const t = i._trElements = n.rows,
                                o = e.cellIndex;
                            let l = 0;
                            for (let e = 0, i = t[0].cells, n = t[0].cells.length; e < n; e++) l += i[e].colSpan;
                            const s = i._rowIndex = i._trElement.rowIndex;
                            i._rowCnt = t.length, i._physical_cellCnt = i._trElement.cells.length, i._logical_cellCnt = l, i._physical_cellIndex = o, i._current_colSpan = i._tdElement.colSpan - 1, i._current_rowSpan, i._trElement.cells[o].rowSpan;
                            let a = [],
                                r = [];
                            for (let e, n, l = 0; l <= s; l++) {
                                e = t[l].cells, n = 0;
                                for (let t, c, d, u, h = 0, g = e.length; h < g; h++) {
                                    if (c = (t = e[h]).colSpan - 1, d = t.rowSpan - 1, u = h + n, r.length > 0)
                                        for (let e, t = 0; t < r.length; t++)(e = r[t]).row > l || (u >= e.index ? (n += e.cs, u += e.cs, e.rs -= 1, e.row = l + 1, e.rs < 1 && (r.splice(t, 1), t--)) : h === g - 1 && (e.rs -= 1, e.row = l + 1, e.rs < 1 && (r.splice(t, 1), t--)));
                                    if (l === s && h === o) {
                                        i._logical_cellIndex = u;
                                        break
                                    }
                                    d > 0 && a.push({
                                        index: u,
                                        cs: c + 1,
                                        rs: d,
                                        row: -1
                                    }), n += c
                                }
                                r = r.concat(a).sort((function(e, t) {
                                    return e.index - t.index
                                })), a = []
                            }
                            a = null, r = null
                        }
                    },
                    editTable: function(e, t) {
                        const i = this.plugins.table,
                            n = this.context.table,
                            o = n._element,
                            l = "row" === e;
                        if (l) {
                            const e = n._trElement.parentNode;
                            if (/^THEAD$/i.test(e.nodeName)) {
                                if ("up" === t) return;
                                if (!e.nextElementSibling || !/^TBODY$/i.test(e.nextElementSibling.nodeName)) return void(o.innerHTML += "<tbody><tr>" + i.createCells.call(this, "td", n._logical_cellCnt, !1) + "</tr></tbody>")
                            }
                        }
                        if (i._ref) {
                            const e = n._tdElement,
                                o = i._selectedCells;
                            if (l)
                                if (t) i.setCellInfo.call(this, "up" === t ? o[0] : o[o.length - 1], !0), i.editRow.call(this, t, e);
                                else {
                                    let e = o[0].parentNode;
                                    const n = [o[0]];
                                    for (let t, i = 1, l = o.length; i < l; i++) e !== (t = o[i]).parentNode && (n.push(t), e = t.parentNode);
                                    for (let e = 0, o = n.length; e < o; e++) i.setCellInfo.call(this, n[e], !0), i.editRow.call(this, t)
                                }
                            else {
                                const n = o[0].parentNode;
                                if (t) {
                                    let l = null;
                                    for (let e = 0, t = o.length - 1; e < t; e++)
                                        if (n !== o[e + 1].parentNode) {
                                            l = o[e];
                                            break
                                        } i.setCellInfo.call(this, "left" === t ? o[0] : l || o[0], !0), i.editCell.call(this, t, e)
                                } else {
                                    const e = [o[0]];
                                    for (let t, i = 1, l = o.length; i < l && n === (t = o[i]).parentNode; i++) e.push(t);
                                    for (let n = 0, o = e.length; n < o; n++) i.setCellInfo.call(this, e[n], !0), i.editCell.call(this, t)
                                }
                            }
                            t || i.init.call(this)
                        } else i[l ? "editRow" : "editCell"].call(this, t);
                        if (!t) {
                            const e = o.children;
                            for (let t = 0; t < e.length; t++) 0 === e[t].children.length && (this.util.removeItem(e[t]), t--);
                            0 === o.children.length && this.util.removeItem(o)
                        }
                    },
                    editRow: function(e, t) {
                        const i = this.context.table,
                            n = !e,
                            o = "up" === e,
                            l = i._rowIndex,
                            s = n || o ? l : l + i._current_rowSpan + 1,
                            a = n ? -1 : 1,
                            r = i._trElements;
                        let c = i._logical_cellCnt;
                        for (let e, t = 0, i = l + (n ? -1 : 0); t <= i; t++) {
                            if (0 === (e = r[t].cells).length) return;
                            for (let i, n, o = 0, l = e.length; o < l; o++) i = e[o].rowSpan, n = e[o].colSpan, i < 2 && n < 2 || i + t > s && s > t && (e[o].rowSpan = i + a, c -= n)
                        }
                        if (n) {
                            const e = r[l + 1];
                            if (e) {
                                const t = [];
                                let i = r[l].cells,
                                    n = 0;
                                for (let e, o, l = 0, s = i.length; l < s; l++) e = i[l], o = l + n, n += e.colSpan - 1, e.rowSpan > 1 && (e.rowSpan -= 1, t.push({
                                    cell: e.cloneNode(!1),
                                    index: o
                                }));
                                if (t.length > 0) {
                                    let o = t.shift();
                                    i = e.cells, n = 0;
                                    for (let l, s, a = 0, r = i.length; a < r && (l = i[a], s = a + n, n += l.colSpan - 1, !(s >= o.index) || (a--, n--, n += o.cell.colSpan - 1, e.insertBefore(o.cell, l), o = t.shift())); a++);
                                    if (o) {
                                        e.appendChild(o.cell);
                                        for (let i = 0, n = t.length; i < n; i++) e.appendChild(t[i].cell)
                                    }
                                }
                            }
                            i._element.deleteRow(s)
                        } else {
                            i._element.insertRow(s).innerHTML = this.plugins.table.createCells.call(this, "td", c, !1)
                        }
                        n ? this.controllersOff() : this.plugins.table.setPositionControllerDiv.call(this, t || i._tdElement, !0)
                    },
                    editCell: function(e, t) {
                        const i = this.context.table,
                            n = this.util,
                            o = !e,
                            l = "left" === e,
                            s = i._current_colSpan,
                            a = o || l ? i._logical_cellIndex : i._logical_cellIndex + s + 1,
                            r = i._trElements;
                        let c = [],
                            d = [],
                            u = 0;
                        const h = [],
                            g = [];
                        for (let e, t, l, p, m, f, _ = 0, b = i._rowCnt; _ < b; _++) {
                            e = r[_], t = a, m = !1, l = e.cells, f = 0;
                            for (let e, i, r, p, b = 0, y = l.length; b < y && (e = l[b]); b++)
                                if (i = e.rowSpan - 1, r = e.colSpan - 1, o) {
                                    if (p = b + f, d.length > 0) {
                                        const e = !l[b + 1];
                                        for (let t, i = 0; i < d.length; i++)(t = d[i]).row > _ || (p >= t.index ? (p = b + (f += t.cs), t.rs -= 1, t.row = _ + 1, t.rs < 1 && (d.splice(i, 1), i--)) : e && (t.rs -= 1, t.row = _ + 1, t.rs < 1 && (d.splice(i, 1), i--)))
                                    }
                                    i > 0 && c.push({
                                        rs: i,
                                        cs: r + 1,
                                        index: p,
                                        row: -1
                                    }), p >= t && p + r <= t + s ? h.push(e) : p <= t + s && p + r >= t ? e.colSpan -= n.getOverlapRangeAtIndex(a, a + s, p, p + r) : i > 0 && (p < t || p + r > t + s) && g.push({
                                        cell: e,
                                        i: _,
                                        rs: _ + i
                                    }), f += r
                                } else {
                                    if (b >= t) break;
                                    if (r > 0) {
                                        if (u < 1 && r + b >= t) {
                                            e.colSpan += 1, t = null, u = i + 1;
                                            break
                                        }
                                        t -= r
                                    }
                                    if (!m) {
                                        for (let e, i = 0; i < d.length; i++) t -= (e = d[i]).cs, e.rs -= 1, e.rs < 1 && (d.splice(i, 1), i--);
                                        m = !0
                                    }
                                } if (d = d.concat(c).sort((function(e, t) {
                                    return e.index - t.index
                                })), c = [], !o) {
                                if (u > 0) {
                                    u -= 1;
                                    continue
                                }
                                null !== t && l.length > 0 && (p = this.plugins.table.createCells.call(this, l[0].nodeName, 0, !0), p = e.insertBefore(p, l[t]))
                            }
                        }
                        if (o) {
                            let e, t;
                            for (let i, o = 0, l = h.length; o < l; o++) i = h[o].parentNode, n.removeItem(h[o]), 0 === i.cells.length && (e || (e = n.getArrayIndex(r, i)), t = n.getArrayIndex(r, i), n.removeItem(i));
                            for (let i, o = 0, l = g.length; o < l; o++)(i = g[o]).cell.rowSpan = n.getOverlapRangeAtIndex(e, t, i.i, i.rs);
                            this.controllersOff()
                        } else this.plugins.table.setPositionControllerDiv.call(this, t || i._tdElement, !0)
                    },
                    _closeSplitMenu: null,
                    openSplitMenu: function() {
                        this.util.addClass(this.context.table.splitButton, "on"), this.context.table.splitMenu.style.display = "inline-table", this.plugins.table._closeSplitMenu = function() {
                            this.util.removeClass(this.context.table.splitButton, "on"), this.context.table.splitMenu.style.display = "none", this.removeDocEvent("mousedown", this.plugins.table._closeSplitMenu), this.plugins.table._closeSplitMenu = null
                        }.bind(this), this.addDocEvent("mousedown", this.plugins.table._closeSplitMenu)
                    },
                    splitCells: function(e) {
                        const t = this.util,
                            i = "vertical" === e,
                            n = this.context.table,
                            o = n._tdElement,
                            l = n._trElements,
                            s = n._trElement,
                            a = n._logical_cellIndex,
                            r = n._rowIndex,
                            c = this.plugins.table.createCells.call(this, o.nodeName, 0, !0);
                        if (i) {
                            const e = o.colSpan;
                            if (c.rowSpan = o.rowSpan, e > 1) c.colSpan = this._w.Math.floor(e / 2), o.colSpan = e - c.colSpan, s.insertBefore(c, o.nextElementSibling);
                            else {
                                let t = [],
                                    i = [];
                                for (let s, r, c = 0, d = n._rowCnt; c < d; c++) {
                                    s = l[c].cells, r = 0;
                                    for (let n, l, d, u, h = 0, g = s.length; h < g; h++) {
                                        if (l = (n = s[h]).colSpan - 1, d = n.rowSpan - 1, u = h + r, i.length > 0)
                                            for (let e, t = 0; t < i.length; t++)(e = i[t]).row > c || (u >= e.index ? (r += e.cs, u += e.cs, e.rs -= 1, e.row = c + 1, e.rs < 1 && (i.splice(t, 1), t--)) : h === g - 1 && (e.rs -= 1, e.row = c + 1, e.rs < 1 && (i.splice(t, 1), t--)));
                                        if (u <= a && d > 0 && t.push({
                                                index: u,
                                                cs: l + 1,
                                                rs: d,
                                                row: -1
                                            }), n !== o && u <= a && u + l >= a + e - 1) {
                                            n.colSpan += 1;
                                            break
                                        }
                                        if (u > a) break;
                                        r += l
                                    }
                                    i = i.concat(t).sort((function(e, t) {
                                        return e.index - t.index
                                    })), t = []
                                }
                                s.insertBefore(c, o.nextElementSibling)
                            }
                        } else {
                            const e = o.rowSpan;
                            if (c.colSpan = o.colSpan, e > 1) {
                                c.rowSpan = this._w.Math.floor(e / 2);
                                const i = e - c.rowSpan,
                                    n = [],
                                    r = t.getArrayIndex(l, s) + i;
                                for (let e, t, i = 0; i < r; i++) {
                                    e = l[i].cells, t = 0;
                                    for (let o, l, s, c = 0, d = e.length; c < d && !((s = c + t) >= a); c++)(l = (o = e[c]).rowSpan - 1) > 0 && l + i >= r && s < a && n.push({
                                        index: s,
                                        cs: o.colSpan
                                    }), t += o.colSpan - 1
                                }
                                const d = l[r],
                                    u = d.cells;
                                let h = n.shift();
                                for (let e, t, i, o, l = 0, s = u.length, r = 0; l < s; l++) {
                                    if (o = (i = l + r) + (t = (e = u[l]).colSpan - 1) + 1, h && o >= h.index && (r += h.cs, o += h.cs, h = n.shift()), o >= a || l === s - 1) {
                                        d.insertBefore(c, e.nextElementSibling);
                                        break
                                    }
                                    r += t
                                }
                                o.rowSpan = i
                            } else {
                                c.rowSpan = o.rowSpan;
                                const e = t.createElement("TR");
                                e.appendChild(c);
                                for (let e, t = 0; t < r; t++) {
                                    if (0 === (e = l[t].cells).length) return;
                                    for (let i = 0, n = e.length; i < n; i++) t + e[i].rowSpan - 1 >= r && (e[i].rowSpan += 1)
                                }
                                const i = n._physical_cellIndex,
                                    a = s.cells;
                                for (let e = 0, t = a.length; e < t; e++) e !== i && (a[e].rowSpan += 1);
                                s.parentNode.insertBefore(e, s.nextElementSibling)
                            }
                        }
                        this.plugins.table.setPositionControllerDiv.call(this, o, !0)
                    },
                    mergeCells: function() {
                        const e = this.plugins.table,
                            t = this.context.table,
                            i = this.util,
                            n = e._ref,
                            o = e._selectedCells,
                            l = o[0];
                        let s = null,
                            a = null,
                            r = n.ce - n.cs + 1,
                            c = n.re - n.rs + 1,
                            d = "",
                            u = null;
                        for (let e, t, n = 1, l = o.length; n < l; n++) {
                            u !== (e = o[n]).parentNode && (u = e.parentNode);
                            for (let n = 0, o = (t = e.children).length; n < o; n++) i.isFormatElement(t[n]) && i.onlyZeroWidthSpace(t[n].textContent) && i.removeItem(t[n]);
                            d += e.innerHTML, i.removeItem(e), 0 === u.cells.length && (s ? a = u : s = u, c -= 1)
                        }
                        if (s) {
                            const e = t._trElements,
                                n = i.getArrayIndex(e, s),
                                o = i.getArrayIndex(e, a || s),
                                l = [];
                            for (let t, s = 0; s <= o; s++)
                                if (0 !== (t = e[s].cells).length)
                                    for (let e, l, a = 0, r = t.length; a < r; a++)(l = (e = t[a]).rowSpan - 1) > 0 && s + l >= n && (e.rowSpan -= i.getOverlapRangeAtIndex(n, o, s, s + l));
                                else l.push(e[s]);
                            for (let e = 0, t = l.length; e < t; e++) i.removeItem(l[e])
                        }
                        l.innerHTML += d, l.colSpan = r, l.rowSpan = c, this.controllersOff(), e.setActiveButton.call(this, !0, !1), e.call_controller_tableEdit.call(this, l), i.addClass(l, "ke-table-selected-cell")
                    },
                    toggleHeader: function() {
                        const e = this.util,
                            t = this.context.table.headerButton,
                            i = e.hasClass(t, "active"),
                            n = this.context.table._element;
                        if (i) e.removeItem(n.querySelector("thead"));
                        else {
                            const t = e.createElement("THEAD");
                            t.innerHTML = "<tr>" + this.plugins.table.createCells.call(this, "th", this.context.table._logical_cellCnt, !1) + "</tr>", n.insertBefore(t, n.firstElementChild)
                        }
                        e.toggleClass(t, "active"), /TH/i.test(this.context.table._tdElement.nodeName) ? this.controllersOff() : this.plugins.table.setPositionControllerDiv.call(this, this.context.table._tdElement, !1)
                    },
                    resizeTable: function() {
                        const e = this.context.table,
                            t = e.resizeIcon,
                            i = e.resizeText;
                        let n = "ke-icon-expansion",
                            o = "ke-icon-reduction",
                            l = e.minText,
                            s = "100%";
                        e._maxWidth || (n = "ke-icon-reduction", o = "ke-icon-expansion", l = e.maxText, s = "auto"), this.util.removeClass(t, n), this.util.addClass(t, o), this.util.changeTxt(i, l), e._element.style.width = s
                    },
                    setActiveButton: function(e, t) {
                        const i = this.context.table;
                        t && e !== t ? (i.splitButton.setAttribute("disabled", !0), i.mergeButton.removeAttribute("disabled")) : (i.splitButton.removeAttribute("disabled"), i.mergeButton.setAttribute("disabled", !0))
                    },
                    _bindOnSelect: null,
                    _bindOffSelect: null,
                    _bindOffShift: null,
                    _selectedCells: null,
                    _shift: !1,
                    _fixedCell: null,
                    _fixedCellName: null,
                    _selectedCell: null,
                    _selectedTable: null,
                    _ref: null,
                    _toggleEditor: function(e) {
                        this.context.element.wysiwyg.setAttribute("contenteditable", e), e ? this.util.removeClass(this.context.element.wysiwyg, "ke-disabled") : this.util.addClass(this.context.element.wysiwyg, "ke-disabled")
                    },
                    _offCellMultiSelect: function(e) {
                        e.stopPropagation();
                        const t = this.plugins.table;
                        t._shift ? t._initBind && (this._wd.removeEventListener("touchmove", t._initBind), t._initBind = null) : (t._removeEvents.call(this), t._toggleEditor.call(this, !0)), t._fixedCell && t._selectedTable && (t.setActiveButton.call(this, t._fixedCell, t._selectedCell), t.call_controller_tableEdit.call(this, t._selectedCell || t._fixedCell), t._selectedCells = t._selectedTable.querySelectorAll(".ke-table-selected-cell"), t._shift || (t._fixedCell = null, t._selectedCell = null, t._fixedCellName = null), this._editorRange(), this.focus())
                    },
                    _onCellMultiSelect: function(e) {
                        const t = this.plugins.table,
                            i = this.util.getParentElement(e.target, this.util.isCell);
                        if (t._shift) i === t._fixedCell ? t._toggleEditor.call(this, !0) : t._toggleEditor.call(this, !1);
                        else if (!t._ref) {
                            if (i === t._fixedCell) return;
                            t._toggleEditor.call(this, !1)
                        }
                        i && i !== t._selectedCell && t._fixedCellName === i.nodeName && t._selectedTable === this.util.getParentElement(i, "TABLE") && (t._selectedCell = i, t._setMultiCells.call(this, t._fixedCell, i))
                    },
                    _setMultiCells: function(e, t) {
                        const i = this.plugins.table,
                            n = i._selectedTable.rows,
                            o = this.util,
                            l = i._selectedTable.querySelectorAll(".ke-table-selected-cell");
                        for (let e = 0, t = l.length; e < t; e++) o.removeClass(l[e], "ke-table-selected-cell");
                        if (e === t && (o.addClass(e, "ke-table-selected-cell"), !i._shift)) return;
                        let s = !0,
                            a = [],
                            r = [];
                        const c = i._ref = {
                            _i: 0,
                            cs: null,
                            ce: null,
                            rs: null,
                            re: null
                        };
                        for (let i, l, d = 0, u = n.length; d < u; d++) {
                            i = n[d].cells, l = 0;
                            for (let n, u, h, g, p = 0, m = i.length; p < m; p++) {
                                if (h = (n = i[p]).colSpan - 1, g = n.rowSpan - 1, u = p + l, a.length > 0)
                                    for (let e, t = 0; t < a.length; t++)(e = a[t]).row > d || (u >= e.index ? (l += e.cs, u += e.cs, e.rs -= 1, e.row = d + 1, e.rs < 1 && (a.splice(t, 1), t--)) : p === m - 1 && (e.rs -= 1, e.row = d + 1, e.rs < 1 && (a.splice(t, 1), t--)));
                                if (s) {
                                    if (n !== e && n !== t || (c.cs = null !== c.cs && c.cs < u ? c.cs : u, c.ce = null !== c.ce && c.ce > u + h ? c.ce : u + h, c.rs = null !== c.rs && c.rs < d ? c.rs : d, c.re = null !== c.re && c.re > d + g ? c.re : d + g, c._i += 1), 2 === c._i) {
                                        s = !1, a = [], r = [], d = -1;
                                        break
                                    }
                                } else if (o.getOverlapRangeAtIndex(c.cs, c.ce, u, u + h) && o.getOverlapRangeAtIndex(c.rs, c.re, d, d + g)) {
                                    const e = c.cs < u ? c.cs : u,
                                        t = c.ce > u + h ? c.ce : u + h,
                                        i = c.rs < d ? c.rs : d,
                                        l = c.re > d + g ? c.re : d + g;
                                    if (c.cs !== e || c.ce !== t || c.rs !== i || c.re !== l) {
                                        c.cs = e, c.ce = t, c.rs = i, c.re = l, d = -1, a = [], r = [];
                                        break
                                    }
                                    o.addClass(n, "ke-table-selected-cell")
                                }
                                g > 0 && r.push({
                                    index: u,
                                    cs: h + 1,
                                    rs: g,
                                    row: -1
                                }), l += n.colSpan - 1
                            }
                            a = a.concat(r).sort((function(e, t) {
                                return e.index - t.index
                            })), r = []
                        }
                    },
                    _removeEvents: function() {
                        const e = this.plugins.table;
                        e._initBind && (this._wd.removeEventListener("touchmove", e._initBind), e._initBind = null), e._bindOnSelect && (this._wd.removeEventListener("mousedown", e._bindOnSelect), this._wd.removeEventListener("mousemove", e._bindOnSelect), e._bindOnSelect = null), e._bindOffSelect && (this._wd.removeEventListener("mouseup", e._bindOffSelect), e._bindOffSelect = null), e._bindOffShift && (this._wd.removeEventListener("keyup", e._bindOffShift), e._bindOffShift = null)
                    },
                    _initBind: null,
                    onTableCellMultiSelect: function(e, t) {
                        const i = this.plugins.table;
                        i._removeEvents.call(this), this.controllersOff(), i._shift = t, i._fixedCell = e, i._fixedCellName = e.nodeName, i._selectedTable = this.util.getParentElement(e, "TABLE");
                        const n = i._selectedTable.querySelectorAll(".ke-table-selected-cell");
                        for (let e = 0, t = n.length; e < t; e++) this.util.removeClass(n[e], "ke-table-selected-cell");
                        this.util.addClass(e, "ke-table-selected-cell"), i._bindOnSelect = i._onCellMultiSelect.bind(this), i._bindOffSelect = i._offCellMultiSelect.bind(this), t ? (i._bindOffShift = function() {
                            this.controllersOn(this.context.table.resizeDiv, this.context.table.tableController, this.plugins.table.init.bind(this), this.focus.bind(this)), i._ref || this.controllersOff()
                        }.bind(this), this._wd.addEventListener("keyup", i._bindOffShift, !1), this._wd.addEventListener("mousedown", i._bindOnSelect, !1)) : this._wd.addEventListener("mousemove", i._bindOnSelect, !1), this._wd.addEventListener("mouseup", i._bindOffSelect, !1), i._initBind = i.init.bind(this), this._wd.addEventListener("touchmove", i._initBind, !1)
                    },
                    onClick_tableController: function(e) {
                        e.stopPropagation();
                        const t = e.target.getAttribute("data-command") ? e.target : e.target.parentNode;
                        if (t.getAttribute("disabled")) return;
                        const i = t.getAttribute("data-command"),
                            n = t.getAttribute("data-value"),
                            o = t.getAttribute("data-option");
                        if ("function" == typeof this.plugins.table._closeSplitMenu && (this.plugins.table._closeSplitMenu(), "onsplit" === i)) return;
                        if (!i) return;
                        e.preventDefault();
                        const l = this.context.table;
                        switch (i) {
                            case "insert":
                            case "delete":
                                this.plugins.table.editTable.call(this, n, o);
                                break;
                            case "header":
                                this.plugins.table.toggleHeader.call(this);
                                break;
                            case "onsplit":
                                this.plugins.table.openSplitMenu.call(this);
                                break;
                            case "split":
                                this.plugins.table.splitCells.call(this, n);
                                break;
                            case "merge":
                                this.plugins.table.mergeCells.call(this);
                                break;
                            case "resize":
                                l.resizeDiv.style.display = "none", l._maxWidth = !l._maxWidth, this.plugins.table.resizeTable.call(this);
                                break;
                            case "remove":
                                this.util.removeItem(l._element), this.controllersOff()
                        }
                        this.focus(), this.history.push()
                    }
                },
                formatBlock: {
                    name: "formatBlock",
                    add: function(e, t) {
                        const i = e.context;
                        i.formatBlock = {
                            _formatList: null,
                            currentFormat: ""
                        };
                        let n = this.setSubmenu.call(e);
                        n.querySelector("ul").addEventListener("click", this.pickUp.bind(e)), i.formatBlock._formatList = n.querySelectorAll("li button"), t.parentNode.appendChild(n), n = null
                    },
                    setSubmenu: function() {
                        const e = this.context.option,
                            t = this.lang.toolbar,
                            i = this.util.createElement("DIV"),
                            n = e.formats && 0 !== e.formats.length ? e.formats : ["p", "div", "blockquote", "pre", "h1", "h2", "h3", "h4", "h5", "h6"];
                        i.className = "ke-submenu ke-list-layer";
                        let o = '<div class="ke-list-inner"><ul class="ke-list-basic ke-list-format">';
                        for (let e, i, l, s, a, r = 0, c = n.length; r < c; r++) "string" == typeof n[r] ? (i = "pre" === (e = n[r].toLowerCase()) || "blockquote" === e ? "range" : "replace", l = t["tag_" + ((s = /^h/.test(e) ? e.match(/\d+/)[0] : "") ? "h" : e)] + s, a = "") : (e = n[r].tag.toLowerCase(), i = "replace", l = n[r].title || e, a = (' class="' + n[r].class + '"' || !1) + 'data-format="' + i + '"'), o += '<li><button type="button" class="ke-btn-list" data-command="' + i + '" data-value="' + e + '" title="' + l + '"><' + e + a + ">" + l + "</" + e + "></button></li>";
                        return o += "</ul></div>", i.innerHTML = o, i
                    },
                    on: function() {
                        const e = this.context.formatBlock,
                            t = e._formatList,
                            i = (this.commandMap.FORMAT.getAttribute("data-focus") || "P").toLowerCase();
                        if (i !== e.currentFormat) {
                            for (let e = 0, n = t.length; e < n; e++) i === t[e].getAttribute("data-value") ? this.util.addClass(t[e], "active") : this.util.removeClass(t[e], "active");
                            e.currentFormat = i
                        }
                    },
                    pickUp: function(e) {
                        e.preventDefault(), e.stopPropagation();
                        let t = e.target,
                            i = null,
                            n = null,
                            o = null;
                        for (; !i && !/UL/i.test(t.tagName);) {
                            if (i = t.getAttribute("data-command"), n = t.getAttribute("data-value"), i) {
                                o = t.firstChild;
                                break
                            }
                            t = t.parentNode
                        }
                        if (i) {
                            if ("range" === i) {
                                const e = o.cloneNode(!1);
                                this.applyRangeFormatElement(e)
                            } else {
                                const e = this.getRange(),
                                    t = e.startOffset,
                                    i = e.endOffset;
                                let l = this.getSelectedElementsAndComponents();
                                if (0 === l.length) return;
                                let s = l[0],
                                    a = l[l.length - 1];
                                const r = this.util.getNodePath(e.startContainer, s),
                                    c = this.util.getNodePath(e.endContainer, a);
                                let d = {},
                                    u = !1,
                                    h = !1;
                                const g = function(e) {
                                    return !this.isComponent(e)
                                }.bind(this.util);
                                for (let e, t, i, n, o = 0, r = l.length; o < r; o++) {
                                    if (i = o === r - 1, t = this.util.getRangeFormatElement(l[o], g), n = this.util.isList(t), !e && n) d = {
                                        r: e = t,
                                        f: [this.util.getParentElement(l[o], "LI")]
                                    }, 0 === o && (u = !0);
                                    else if (e && n)
                                        if (e !== t) {
                                            const r = this.detachRangeFormatElement(d.r, d.f, null, !1, !0);
                                            u && (s = r.sc, u = !1), i && (a = r.ec), n ? (d = {
                                                r: e = t,
                                                f: [this.util.getParentElement(l[o], "LI")]
                                            }, i && (h = !0)) : e = null
                                        } else d.f.push(this.util.getParentElement(l[o], "LI")), i && (h = !0);
                                    if (i && this.util.isList(e)) {
                                        const e = this.detachRangeFormatElement(d.r, d.f, null, !1, !0);
                                        (h || 1 === r) && (a = e.ec, u && (s = e.sc || a))
                                    }
                                }
                                this.setRange(this.util.getNodeFromPath(r, s), t, this.util.getNodeFromPath(c, a), i);
                                for (let e, t, i = 0, r = (l = this.getSelectedElementsAndComponents()).length; i < r; i++)(e = l[i]).nodeName.toLowerCase() === n.toLowerCase() || this.util.isComponent(e) || ((t = o.cloneNode(!1)).innerHTML = e.innerHTML, e.parentNode.insertBefore(t, e), this.util.removeItem(e)), 0 === i && (s = t || e), i === r - 1 && (a = t || e), t = null;
                                this.setRange(this.util.getNodeFromPath(r, s), t, this.util.getNodeFromPath(c, a), i), this.history.push()
                            }
                            this.submenuOff()
                        }
                    }
                },
                template: {
                    name: "template",
                    add: function(e, t) {
                        e.context.template = {};
                        let i = this.setSubmenu.call(e);
                        i.querySelector("ul").addEventListener("click", this.pickup.bind(e)), t.parentNode.appendChild(i), i = null
                    },
                    setSubmenu: function() {
                        const e = this.context.option.templates,
                            t = this.util.createElement("DIV");
                        t.className = "ke-list-layer";
                        let i = '<div class="ke-submenu ke-list-inner">   <ul class="ke-list-basic">';
                        for (let t, n = 0, o = e.length; n < o; n++) i += '<li><button type="button" class="ke-btn-list" data-value="' + n + '" title="' + (t = e[n]).name + '">' + t.name + "</button></li>";
                        return i += "   </ul>", i += "</div>", t.innerHTML = i, t
                    },
                    pickup: function(e) {
                        if (!/^BUTTON$/i.test(e.target.tagName)) return !1;
                        e.preventDefault(), e.stopPropagation();
                        const t = this.context.option.templates[e.target.getAttribute("data-value")];
                        if (!t.html) throw this.submenuOff(), Error('[KEDITOR.template.fail] cause : "templates[i].html not found"');
                        this.setContents(t.html), this.submenuOff()
                    }
                },
                link: {
                    name: "link",
                    add: function(e) {
                        e.addModule([o]);
                        const t = e.context;
                        t.link = {
                            focusElement: null,
                            linkNewWindowCheck: null,
                            linkAnchorText: null,
                            _linkAnchor: null
                        };
                        let i = this.setDialog.call(e);
                        t.link.modal = i, t.link.focusElement = i.querySelector("._se_link_url"), t.link.linkAnchorText = i.querySelector("._se_link_text"), t.link.linkNewWindowCheck = i.querySelector("._se_link_check");
                        let n = this.setController_LinkButton.call(e);
                        t.link.linkBtn = n, t.link._linkAnchor = null, n.addEventListener("mousedown", (function(e) {
                            e.stopPropagation()
                        }), !1), i.querySelector(".ke-btn-primary").addEventListener("click", this.submit.bind(e)), n.addEventListener("click", this.onClick_linkBtn.bind(e)), t.dialog.modal.appendChild(i), t.element.relative.appendChild(n), i = null, n = null
                    },
                    setDialog: function() {
                        const e = this.lang,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-dialog-content", t.style.display = "none", t.innerHTML = '<form class="editor_link">   <div class="ke-dialog-header">       <button type="button" data-command="close" class="close" aria-label="Close" title="' + e.dialogBox.close + '">           <i aria-hidden="true" data-command="close" class="ke-icon-cancel"></i>       </button>       <span class="ke-modal-title">' + e.dialogBox.linkBox.title + '</span>   </div>   <div class="ke-dialog-body">       <div class="ke-dialog-form">           <label>' + e.dialogBox.linkBox.url + '</label>           <input class="ke-input-form _se_link_url" type="text" />       </div>       <div class="ke-dialog-form">           <label>' + e.dialogBox.linkBox.text + '</label><input class="ke-input-form _se_link_text" type="text" />       </div>       <div class="ke-dialog-form-footer">           <label><input type="checkbox" class="ke-dialog-btn-check _se_link_check" />&nbsp;' + e.dialogBox.linkBox.newWindowCheck + '</label>       </div>   </div>   <div class="ke-dialog-footer">       <button type="button" data-command="close" class="ke-btn-cancel" aria-label="Close" title="' + e.dialogBox.close + '"><span data-command="close">' + e.dialogBox.close + '</span></button>       <button type="submit" class="ke-btn-primary" title="' + e.dialogBox.submitButton + '"><span>' + e.dialogBox.submitButton + "</span></button>   </div></form>", t
                    },
                    setController_LinkButton: function() {
                        const e = this.lang,
                            t = this.util.createElement("DIV");
                        return t.className = "ke-controller ke-controller-link", t.innerHTML = '<div class="ke-arrow ke-arrow-up"></div><div class="link-content"><span><a target="_blank" href=""></a>&nbsp;</span>   <div class="ke-btn-group">       <button type="button" data-command="update" tabindex="-1" class="ke-tooltip">           <i class="ke-icon-edit"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.edit + '</span></span>       </button>       <button type="button" data-command="delete" tabindex="-1" class="ke-tooltip">           <i class="ke-icon-delete"></i>           <span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + e.controller.remove + "</span></span>       </button>   </div></div>", t
                    },
                    submit: function(e) {
                        this.showLoading(), e.preventDefault(), e.stopPropagation();
                        const t = function() {
                            if (0 === this.context.link.focusElement.value.trim().length) return !1;
                            const e = this.context.link,
                                t = e.focusElement.value,
                                i = e.linkAnchorText,
                                n = 0 === i.value.length ? t : i.value;
                            if (this.context.dialog.updateModal) e._linkAnchor.href = t, e._linkAnchor.textContent = n, e._linkAnchor.target = e.linkNewWindowCheck.checked ? "_blank" : "", this.history.push(), this.setRange(e._linkAnchor.childNodes[0], 0, e._linkAnchor.childNodes[0], e._linkAnchor.textContent.length);
                            else {
                                const i = this.util.createElement("A");
                                i.href = t, i.textContent = n, i.target = e.linkNewWindowCheck.checked ? "_blank" : "", this.insertNode(i), this.setRange(i.childNodes[0], 0, i.childNodes[0], i.textContent.length)
                            }
                            e.focusElement.value = "", e.linkAnchorText.value = ""
                        }.bind(this);
                        try {
                            t()
                        } finally {
                            this.plugins.dialog.close.call(this), this.closeLoading(), this.focus()
                        }
                        return !1
                    },
                    on: function(e) {
                        e || (this.context.link.linkAnchorText.value = this.getSelection().toString())
                    },
                    call_controller_linkButton: function(e) {
                        this.editLink = this.context.link._linkAnchor = e;
                        const t = this.context.link.linkBtn,
                            i = t.querySelector("a");
                        i.href = e.href, i.title = e.textContent, i.textContent = e.textContent;
                        const n = this.util.getOffset(e, this.context.element.wysiwygFrame);
                        t.style.top = n.top + e.offsetHeight + 10 + "px", t.style.left = n.left - this.context.element.wysiwygFrame.scrollLeft + "px", t.style.display = "block";
                        const o = this.context.element.wysiwygFrame.offsetWidth - (t.offsetLeft + t.offsetWidth);
                        o < 0 ? (t.style.left = t.offsetLeft + o + "px", t.firstElementChild.style.left = 20 - o + "px") : t.firstElementChild.style.left = "20px", this.controllersOn(t)
                    },
                    onClick_linkBtn: function(e) {
                        e.stopPropagation();
                        const t = e.target.getAttribute("data-command") || e.target.parentNode.getAttribute("data-command");
                        t && (e.preventDefault(), /update/.test(t) ? (this.context.link.focusElement.value = this.context.link._linkAnchor.href, this.context.link.linkAnchorText.value = this.context.link._linkAnchor.textContent, this.context.link.linkNewWindowCheck.checked = !!/_blank/i.test(this.context.link._linkAnchor.target), this.plugins.dialog.open.call(this, "link", !0)) : (this.util.removeItem(this.context.link._linkAnchor), this.context.link._linkAnchor = null, this.focus()), this.controllersOff(), this.history.push())
                    },
                    init: function() {
                        const e = this.context.link;
                        e.linkBtn.style.display = "none", e._linkAnchor = null, e.focusElement.value = "", e.linkAnchorText.value = "", e.linkNewWindowCheck.checked = !1
                    }
                },
                image: {
                    name: "image",
                    add: function(e) {
                        e.addModule([o, l, s]);
                        const t = e.context;
                        t.image = {
                            _linkElement: null,
                            _container: null,
                            _cover: null,
                            _element: null,
                            _element_w: 1,
                            _element_h: 1,
                            _element_l: 0,
                            _element_t: 0,
                            _origin_w: "auto" === t.option.imageWidth ? "" : t.option.imageWidth,
                            _origin_h: "",
                            _altText: "",
                            _caption: null,
                            captionCheckEl: null,
                            _linkValue: "",
                            _align: "none",
                            _captionChecked: !1,
                            _proportionChecked: !0,
                            _floatClassRegExp: "__se__float\\-[a-z]+",
                            _xmlHttp: null,
                            _resizing: t.option.imageResizing,
                            _defaultAuto: "auto" === t.option.imageWidth,
                            _uploadFileLength: 0
                        };
                        let i = this.setDialog.call(e);
                        t.image.modal = i, t.image.imgUrlFile = i.querySelector("._se_image_url"), t.image.imgInputFile = t.image.focusElement = i.querySelector("._se_image_file"), t.image.altText = i.querySelector("._se_image_alt"), t.image.imgLink = i.querySelector("._se_image_link"), t.image.imgLinkNewWindowCheck = i.querySelector("._se_image_link_check"), t.image.captionCheckEl = i.querySelector("._se_image_check_caption"), t.image.modal.querySelector(".ke-dialog-tabs").addEventListener("click", this.openTab.bind(e)), t.image.modal.querySelector(".ke-btn-primary").addEventListener("click", this.submit.bind(e)), t.image.imageX = {}, t.image.imageY = {}, t.option.imageResizing && (t.image.proportion = i.querySelector("._se_image_check_proportion"), t.image.imageX = i.querySelector("._se_image_size_x"), t.image.imageY = i.querySelector("._se_image_size_y"), t.image.imageX.value = t.option.imageWidth, t.image.imageX.addEventListener("change", this.setInputSize.bind(e, "x")), t.image.imageY.addEventListener("change", this.setInputSize.bind(e, "y")), i.querySelector(".ke-dialog-btn-revert").addEventListener("click", this.sizeRevert.bind(e))), t.dialog.modal.appendChild(i), i = null
                    },
                    setDialog: function() {
                        const e = this.context.option,
                            t = this.lang,
                            i = this.util.createElement("DIV");
                        i.className = "ke-dialog-content", i.style.display = "none";
                        let n = '<div class="ke-dialog-header">   <button type="button" data-command="close" class="close" aria-label="Close" title="' + t.dialogBox.close + '">       <i aria-hidden="true" data-command="close" class="ke-icon-cancel"></i>   </button>   <span class="ke-modal-title">' + t.dialogBox.imageBox.title + '</span></div><div class="ke-dialog-tabs">   <button type="button" class="_se_tab_link active" data-tab-link="image">' + t.toolbar.image + '</button>   <button type="button" class="_se_tab_link" data-tab-link="url">' + t.toolbar.link + '</button></div><form class="editor_image" method="post" enctype="multipart/form-data">   <div class="_se_tab_content _se_tab_content_image">       <div class="ke-dialog-body">';
                        return e.imageFileInput && (n += '   <div class="ke-dialog-form"></div>'), e.imageUrlInput && (n += '   <div class="ke-dialog-form">       <label>' + t.dialogBox.imageBox.url + '</label>       <input class="ke-input-form _se_image_url" type="text" />   </div>'), n += '           <div class="ke-dialog-form">               <label>' + t.dialogBox.imageBox.altText + '</label><input class="ke-input-form _se_image_alt" type="text" />           </div>', e.imageResizing && (n += '       <div class="ke-dialog-form">           <div class="ke-dialog-size-text"><label class="size-w">' + t.dialogBox.width + '</label><label class="ke-dialog-size-x">&nbsp;</label><label class="size-h">' + t.dialogBox.height + '</label></div>           <input class="ke-input-control _se_image_size_x" type="number" min="1" ' + ("auto" === e.imageWidth ? "disabled" : "") + ' /><label class="ke-dialog-size-x">x</label><input class="ke-input-control _se_image_size_y" type="number" min="1" disabled />           <label><input type="checkbox" class="ke-dialog-btn-check _se_image_check_proportion" checked disabled/>&nbsp;' + t.dialogBox.proportion + '</label>           <button type="button" title="' + t.dialogBox.revertButton + '" class="ke-btn ke-dialog-btn-revert" style="float: right;"><i class="ke-icon-revert"></i></button>       </div>'), n += '           <div class="ke-dialog-form-footer">               <label><input type="checkbox" class="ke-dialog-btn-check _se_image_check_caption" />&nbsp;' + t.dialogBox.caption + '</label>           </div>       </div>   </div>   <div class="_se_tab_content _se_tab_content_url" style="display: none">       <div class="ke-dialog-body">           <div class="ke-dialog-form">               <label>' + t.dialogBox.linkBox.url + '</label><input class="ke-input-form _se_image_link" type="text" />           </div>           <label><input type="checkbox" class="_se_image_link_check"/>&nbsp;' + t.dialogBox.linkBox.newWindowCheck + '</label>       </div>   </div>   <div class="ke-dialog-footer">       <div class="footer-div">           <label><input type="radio" name="keditor_image_radio" class="ke-dialog-btn-radio" value="none" checked>' + t.dialogBox.basic + '</label>           <label><input type="radio" name="keditor_image_radio" class="ke-dialog-btn-radio" value="left">' + t.dialogBox.left + '</label>           <label><input type="radio" name="keditor_image_radio" class="ke-dialog-btn-radio" value="center">' + t.dialogBox.center + '</label>           <label><input type="radio" name="keditor_image_radio" class="ke-dialog-btn-radio" value="right">' + t.dialogBox.right + '</label>       </div>       <button type="button" data-command="close" class="ke-btn-cancel" aria-label="Close" title="' + t.dialogBox.close + '"><span data-command="close">' + t.dialogBox.close + '</span></button>       <button type="submit" class="ke-btn-primary" title="' + t.dialogBox.submitButton + '"><span>' + t.dialogBox.submitButton + "</span></button>   </div></form>", i.innerHTML = n, i
                    },
                    openTab: function(e) {
                        const t = this.context.image.modal,
                            i = "init" === e ? t.querySelector("._se_tab_link") : e.target;
                        if (!/^BUTTON$/i.test(i.tagName)) return !1;
                        const n = i.getAttribute("data-tab-link");
                        let o, l, s;
                        for (l = t.getElementsByClassName("_se_tab_content"), o = 0; o < l.length; o++) l[o].style.display = "none";
                        for (s = t.getElementsByClassName("_se_tab_link"), o = 0; o < s.length; o++) this.util.removeClass(s[o], "active");
                        return t.querySelector("._se_tab_content_" + n).style.display = "block", this.util.addClass(i, "active"), "image" === n ? this.context.image.imgInputFile.focus() : "url" === n && this.context.image.imgLink.focus(), !1
                    },
                    submitAction: function(e) {
                        if (e.length > 0) {
                            let t = 0;
                            const i = [];
                            for (let n = 0, o = e.length; n < o; n++) /image/i.test(e[n].type) && (i.push(e[n]), t += e[n].size);
                            const n = this.context.option.imageUploadSizeLimit;
                            if (n > 0) {
                                let e = 0;
                                const i = this._variable._imagesInfo;
                                for (let t = 0, n = i.length; t < n; t++) e += 1 * i[t].size;
                                if (t + e > n) {
                                    const i = "[KEDITOR.imageUpload.fail] Size of uploadable total images: " + n / 1e3 + "KB";
                                    return this._imageUploadError(i, {
                                        limitSize: n,
                                        currentSize: e,
                                        uploadSize: t
                                    }) && s.open.call(this, i), void this.closeLoading()
                                }
                            }
                            this.context.image._uploadFileLength = i.length;
                            const o = this.context.option.imageUploadUrl,
                                l = this.context.option.imageUploadHeader,
                                a = this.context.dialog.updateModal ? 1 : i.length;
                            if ("string" == typeof o && o.length > 0) {
                                const e = new FormData;
                                for (let t = 0; t < a; t++) e.append("file-" + t, i[t]);
                                if (this.context.image._xmlHttp = this.util.getXMLHttpRequest(), this.context.image._xmlHttp.onreadystatechange = this.plugins.image.callBack_imgUpload.bind(this, this.context.image._linkValue, this.context.image.imgLinkNewWindowCheck.checked, this.context.image.imageX.value, this.context.image._align, this.context.dialog.updateModal, this.context.image._element), this.context.image._xmlHttp.open("post", o, !0), "object" == typeof l && Object.keys(l).length > 0)
                                    for (let e in l) this.context.image._xmlHttp.setRequestHeader(e, l[e]);
                                this.context.image._xmlHttp.send(e)
                            } else
                                for (let e = 0; e < a; e++) this.plugins.image.setup_reader.call(this, i[e], this.context.image._linkValue, this.context.image.imgLinkNewWindowCheck.checked, this.context.image.imageX.value, this.context.image._align, e, a - 1)
                        }
                    },
                    onRender_imgInput: function() {
                        try {
                            this.plugins.image.submitAction.call(this, this.context.image.imgInputFile.files)
                        } catch (e) {
                            throw this.closeLoading(), Error('[KEDITOR.imageUpload.fail] cause : "' + e.message + '"')
                        }
                    },
                    setup_reader: function(e, t, i, n, o, l, s) {
                        const a = new FileReader;
                        this.context.dialog.updateModal && (this.context.image._element.setAttribute("data-file-name", e.name), this.context.image._element.setAttribute("data-file-size", e.size)), a.onload = function(e, r, c) {
                            try {
                                e ? this.plugins.image.update_src.call(this, a.result, r, c) : this.plugins.image.create_image.call(this, a.result, t, i, n, o, c), l === s && this.closeLoading()
                            } catch (e) {
                                throw this.closeLoading(), Error('[KEDITOR.imageFileRendering.fail] cause : "' + e.message + '"')
                            }
                        }.bind(this, this.context.dialog.updateModal, this.context.image._element, e), a.readAsDataURL(e)
                    },
                    callBack_imgUpload: function(e, t, i, n, o, l) {
                        if (4 === this.context.image._xmlHttp.readyState) {
                            if (200 !== this.context.image._xmlHttp.status) throw this.closeLoading(), Error("[KEDITOR.imageUpload.fail] status: " + this.context.image._xmlHttp.status + ", responseURL: " + this.context.image._xmlHttp.responseURL); {
                                const a = JSON.parse(this.context.image._xmlHttp.responseText);
                                if (a.errorMessage) this.closeLoading(), this._imageUploadError(a.errorMessage, a.result) && s.open.call(this, a.errorMessage);
                                else {
                                    const s = a.result;
                                    for (let a, r = 0, c = s.length; r < c; r++) a = {
                                        name: s[r].name,
                                        size: s[r].size
                                    }, o ? this.plugins.image.update_src.call(this, s[r].url, l, a) : this.plugins.image.create_image.call(this, s[r].url, e, t, i, n, a)
                                }
                                this.closeLoading()
                            }
                        }
                    },
                    onRender_imgUrl: function() {
                        if (0 === this.context.image.imgUrlFile.value.trim().length) return !1;
                        try {
                            const e = {
                                name: this.context.image.imgUrlFile.value.split("/").pop(),
                                size: 0
                            };
                            this.context.dialog.updateModal ? this.plugins.image.update_src.call(this, this.context.image.imgUrlFile.value, this.context.image._element, e) : this.plugins.image.create_image.call(this, this.context.image.imgUrlFile.value, this.context.image._linkValue, this.context.image.imgLinkNewWindowCheck.checked, this.context.image.imageX.value + "px", this.context.image._align, e)
                        } catch (e) {
                            throw Error('[KEDITOR.imageURLRendering.fail] cause : "' + e.message + '"')
                        } finally {
                            this.closeLoading()
                        }
                    },
                    onRender_link: function(e, t, i) {
                        if (t.trim().length > 0) {
                            const n = this.util.createElement("A");
                            return n.href = /^https?:\/\//.test(t) ? t : "http://" + t, n.target = i ? "_blank" : "", n.setAttribute("data-image-link", "image"), e.setAttribute("data-image-link", t), n.appendChild(e), n
                        }
                        return e
                    },
                    setInputSize: function(e) {
                        if (!this.context.dialog.updateModal) return;
                        const t = this.context.image;
                        t.proportion.checked && ("x" === e ? t.imageY.value = Math.round(t._element_h / t._element_w * t.imageX.value) : t.imageX.value = Math.round(t._element_w / t._element_h * t.imageY.value))
                    },
                    submit: function(e) {
                        const t = this.context.image,
                            i = this.plugins.image;
                        this.showLoading(), e.preventDefault(), e.stopPropagation(), t._linkValue = t.imgLink.value, t._altText = t.altText.value, t._align = t.modal.querySelector('input[name="keditor_image_radio"]:checked').value, t._captionChecked = t.captionCheckEl.checked, t._resizing && (t._proportionChecked = t.proportion.checked);
                        try {
                            this.context.dialog.updateModal && i.update_image.call(this, !1, !1), t.imgInputFile && t.imgInputFile.files.length > 0 ? i.onRender_imgInput.call(this) : t.imgUrlFile && t.imgUrlFile.value.trim().length > 0 ? i.onRender_imgUrl.call(this) : this.closeLoading()
                        } catch (e) {
                            throw this.closeLoading(), Error('[KEDITOR.image.submit.fail] cause : "' + e.message + '"')
                        } finally {
                            this.plugins.dialog.close.call(this)
                        }
                        return !1
                    },
                    setImagesInfo: function(e, t) {
                        const i = this._variable._imagesInfo;
                        let n = e.getAttribute("data-index"),
                            o = null,
                            l = "";
                        if (n) {
                            l = "update", n *= 1;
                            for (let e = 0, t = i.length; e < t; e++)
                                if (n === i[e].index) {
                                    o = i[e];
                                    break
                                } o.src = e.src, o.name = e.getAttribute("data-file-name"), o.size = 1 * e.getAttribute("data-file-size")
                        } else l = "create", n = this._variable._imageIndex, this._variable._imageIndex++, e.setAttribute("data-index", n), e.setAttribute("data-file-name", t.name), e.setAttribute("data-file-size", t.size), o = {
                            src: e.src,
                            index: 1 * n,
                            name: t.name,
                            size: t.size
                        }, i.push(o);
                        o.delete = this.plugins.image.destroy.bind(this, e), o.select = function() {
                            e.scrollIntoView(!0), this._w.setTimeout(function() {
                                this.plugins.image.onModifyMode.call(this, e, this.plugins.resizing.call_controller_resize.call(this, e, "image"))
                            }.bind(this))
                        }.bind(this), e.setAttribute("origin-size", e.naturalWidth + "," + e.naturalHeight), e.setAttribute("data-origin", e.offsetWidth + "," + e.offsetHeight), this._imageUpload(e, n, l, o, --this.context.image._uploadFileLength < 0 ? 0 : this.context.image._uploadFileLength)
                    },
                    checkImagesInfo: function() {
                        const e = this.context.element.wysiwyg.getElementsByTagName("IMG"),
                            t = this._variable._imagesInfo;
                        if (e.length === t.length) return;
                        const i = this.plugins.image,
                            n = [],
                            o = [];
                        for (let e = 0, i = t.length; e < i; e++) o[e] = t[e].index;
                        for (let t, l = 0, s = e.length; l < s; l++) t = e[l], this.util.getParentElement(t, ".ke-image-container") ? !t.getAttribute("data-index") || o.indexOf(1 * t.getAttribute("data-index")) < 0 ? (n.push(this._variable._imageIndex), t.removeAttribute("data-index"), i.setImagesInfo.call(this, t, {
                            name: t.getAttribute("data-file-name") || t.src.split("/").pop(),
                            size: t.getAttribute("data-file-size") || 0
                        })) : n.push(1 * t.getAttribute("data-index")) : (n.push(this._variable._imageIndex), i.onModifyMode.call(this, t, null), i.openModify.call(this, !0), i.update_image.call(this, !0, !1));
                        for (let e, i = 0; i < t.length; i++) e = t[i].index, n.indexOf(e) > -1 || (t.splice(i, 1), this._imageUpload(null, e, "delete", null, 0), i--)
                    },
                    _onload_image: function(e, t) {
                        t && this.plugins.image.setImagesInfo.call(this, e, t)
                    },
                    create_image: function(e, t, i, n, o, l) {
                        const s = this.context.image;
                        let a = this.util.createElement("IMG");
                        a.addEventListener("load", this.plugins.image._onload_image.bind(this, a, l)), a.src = e, a.setAttribute("data-align", o), a.alt = s._altText, (a = this.plugins.image.onRender_link.call(this, a, t, i)).setAttribute("data-rotate", "0"), s._resizing && (/\d+/.test(n) && ((n = 1 * n.match(/\d+/)[0]) > 0 ? a.style.width = n + "px" : n = ""), a.setAttribute("data-proportion", s._proportionChecked));
                        const r = this.plugins.resizing.set_cover.call(this, a),
                            c = this.plugins.resizing.set_container.call(this, r, "ke-image-container");
                        s._captionChecked && (s._caption = this.plugins.resizing.create_caption.call(this), s._caption.setAttribute("contenteditable", !1), r.appendChild(s._caption)), r.style.margin = "none" !== o ? "auto" : "0", this.util.removeClass(c, s._floatClassRegExp), this.util.addClass(c, "__se__float-" + o), s._resizing && /\d+/.test(n) || (this.context.resizing._resize_plugin = "image", s._element = a, s._cover = r, s._container = c, this.plugins.image.setAutoSize.call(this)), this.insertComponent(c)
                    },
                    update_image: function(e, t) {
                        const i = this.context.image,
                            n = i._linkValue;
                        let o = i._element,
                            l = i._cover,
                            s = i._container,
                            a = !1;
                        const r = 1 * i.imageX.value !== o.offsetWidth || 1 * i.imageY.value !== o.offsetHeight;
                        if (null === l && (a = !0, o = i._element.cloneNode(!0), l = this.plugins.resizing.set_cover.call(this, o)), null === s && (l = l.cloneNode(!0), a = !0, s = this.plugins.resizing.set_container.call(this, l, "ke-image-container")), a && (s.innerHTML = "", s.appendChild(l)), o.alt = i._altText, i._resizing && (o.setAttribute("data-proportion", i._proportionChecked), r && this.plugins.image.setSize.call(this, i.imageX.value, i.imageY.value)), i._captionChecked ? i._caption || (i._caption = this.plugins.resizing.create_caption.call(this), l.appendChild(i._caption)) : i._caption && (this.util.removeItem(i._caption), i._caption = null), i._align && "none" !== i._align ? l.style.margin = "auto" : l.style.margin = "0", this.util.removeClass(s, this.context.image._floatClassRegExp), this.util.addClass(s, "__se__float-" + i._align), o.setAttribute("data-align", i._align), n.trim().length > 0)
                            if (null !== i._linkElement) i._linkElement.href = n, i._linkElement.target = i.imgLinkNewWindowCheck.checked ? "_blank" : "", o.setAttribute("data-image-link", n);
                            else {
                                let e = this.plugins.image.onRender_link.call(this, o, n, this.context.image.imgLinkNewWindowCheck.checked);
                                l.insertBefore(e, i._caption)
                            }
                        else if (null !== i._linkElement) {
                            const e = o;
                            e.setAttribute("data-image-link", "");
                            let t = e.cloneNode(!0);
                            l.removeChild(i._linkElement), l.insertBefore(t, i._caption), o = t
                        }
                        if (a) {
                            const e = this.util.isRangeFormatElement(i._element.parentNode) || this.util.isWysiwygDiv(i._element.parentNode) ? i._element : /^A$/i.test(i._element.parentNode.nodeName) ? i._element.parentNode : this.util.getFormatElement(i._element) || i._element;
                            e.parentNode.insertBefore(s, e), this.util.removeItem(e), o = s.querySelector("img")
                        }
                        if (!e && (/\d+/.test(o.style.height) || i._resizing && r || this.context.resizing._rotateVertical && i._captionChecked) && this.plugins.resizing.setTransformSize.call(this, o, null, null), e && this.plugins.image.setImagesInfo.call(this, o, {
                                name: o.getAttribute("data-file-name") || o.src.split("/").pop(),
                                size: o.getAttribute("data-file-size") || 0
                            }), t) {
                            this.plugins.image.init.call(this);
                            const e = this.plugins.resizing.call_controller_resize.call(this, o, "image");
                            this.plugins.image.onModifyMode.call(this, o, e)
                        }
                        this.history.push()
                    },
                    update_src: function(e, t, i) {
                        t.src = e, this._w.setTimeout(this.plugins.image.setImagesInfo.bind(this, t, i))
                    },
                    sizeRevert: function() {
                        const e = this.context.image;
                        e._origin_w && (e.imageX.value = e._element_w = e._origin_w, e.imageY.value = e._element_h = e._origin_h)
                    },
                    onModifyMode: function(e, t) {
                        const i = this.context.image;
                        i._linkElement = /^A$/i.test(e.parentNode.nodeName) ? e.parentNode : null, i._element = e, i._cover = this.util.getParentElement(e, "FIGURE"), i._container = this.util.getParentElement(e, ".ke-image-container"), i._caption = this.util.getChildElement(i._cover, "FIGCAPTION"), i._align = e.getAttribute("data-align") || "none", t && (i._element_w = t.w, i._element_h = t.h, i._element_t = t.t, i._element_l = t.l);
                        let n = i._element.getAttribute("data-origin");
                        n ? (n = n.split(","), i._origin_w = 1 * n[0], i._origin_h = 1 * n[1]) : t && (i._origin_w = t.w, i._origin_h = t.h, i._element.setAttribute("data-origin", t.w + "," + t.h))
                    },
                    openModify: function(e) {
                        const t = this.context.image;
                        t.imgUrlFile.value = t._element.src, t._altText = t.altText.value = t._element.alt, t._linkValue = t.imgLink.value = null === t._linkElement ? "" : t._linkElement.href, t.imgLinkNewWindowCheck.checked = t._linkElement && "_blank" === t._linkElement.target, t.modal.querySelector('input[name="keditor_image_radio"][value="' + t._align + '"]').checked = !0, t._align = t.modal.querySelector('input[name="keditor_image_radio"]:checked').value, t._captionChecked = t.captionCheckEl.checked = !!t._caption, t._resizing && (t.proportion.checked = t._proportionChecked = "false" !== t._element.getAttribute("data-proportion"), t.imageX.value = t._element.offsetWidth, t.imageY.value = t._element.offsetHeight, t.imageX.disabled = !1, t.imageY.disabled = !1, t.proportion.disabled = !1), e || this.plugins.dialog.open.call(this, "image", !0)
                    },
                    on: function(e) {
                        if (!e) {
                            const e = this.context.image;
                            e.imageX.value = e._origin_w = e._defaultAuto ? "" : this.context.option.imageWidth, e.imageY.value = e._origin_h = ""
                        }
                    },
                    setSize: function(e, t) {
                        const i = this.context.image;
                        i._element.style.width = /^\d+$/.test(e) ? e + "px" : e, i._element.style.height = /^\d+$/.test(t) ? t + "px" : t
                    },
                    setAutoSize: function() {
                        const e = this.context.image;
                        this.plugins.resizing.resetTransform.call(this, e._element), this.plugins.image.cancelPercentAttr.call(this);
                        const t = (e._element.getAttribute("data-origin") || "").split(",");
                        e._element.style.maxWidth = "100%", e._element.style.width = t[0] ? t[0] + "px" : "100%", e._element.style.height = "", e._cover.style.width = "", e._cover.style.height = ""
                    },
                    setPercentSize: function(e) {
                        const t = this.context.image;
                        t._element.style.maxWidth = "100%", t._container.style.width = e, t._container.style.height = "", t._cover.style.width = "100%", t._cover.style.height = "", t._element.style.width = "100%", t._element.style.height = "", /100/.test(e) && (this.util.removeClass(t._container, this.context.image._floatClassRegExp), this.util.addClass(t._container, "__se__float-center"))
                    },
                    cancelPercentAttr: function() {
                        const e = this.context.image;
                        e._element.style.maxWidth = "none", e._cover.style.width = "", e._cover.style.height = "", e._container.style.width = "", e._container.style.height = "", this.util.removeClass(e._container, this.context.image._floatClassRegExp), this.util.addClass(e._container, "__se__float-" + e._align)
                    },
                    resetAlign: function() {
                        const e = this.context.image;
                        e._element.setAttribute("data-align", ""), e._align = "none", e._cover.style.margin = "0", this.util.removeClass(e._container, e._floatClassRegExp)
                    },
                    destroy: function(e) {
                        const t = e || this.context.image._element,
                            i = this.util.getParentElement(t, ".ke-image-container") || t,
                            n = 1 * t.getAttribute("data-index");
                        if (this.util.removeItem(i), this.plugins.image.init.call(this), this.controllersOff(), this.history.push(), n >= 0) {
                            const e = this._variable._imagesInfo;
                            for (let t = 0, i = e.length; t < i; t++)
                                if (n === e[t].index) return e.splice(t, 1), void this._imageUpload(null, n, "delete", null, 0)
                        }
                    },
                    init: function() {
                        const e = this.context.image;
                        e.imgInputFile && (e.imgInputFile.value = ""), e.imgUrlFile && (e.imgUrlFile.value = ""), e.altText.value = "", e.imgLink.value = "", e.imgLinkNewWindowCheck.checked = !1, e.modal.querySelector('input[name="keditor_image_radio"][value="none"]').checked = !0, e.captionCheckEl.checked = !1, e._element = null, this.plugins.image.openTab.call(this, "init"), e._resizing && (e.proportion.checked = !1, e.imageX.value = e._defaultAuto ? "" : this.context.option.imageWidth, e.imageY.value = "", e.imageX.disabled = e._defaultAuto, e.imageY.disabled = !0, e.proportion.disabled = !0)
                    }
                },
                video: {
                    name: "video",
                    add: function(e) {
                        e.addModule([o, l]);
                        const t = e.context;
                        t.video = {
                            _container: null,
                            _cover: null,
                            _element: null,
                            _element_w: t.option.videoWidth,
                            _element_h: t.option.videoHeight,
                            _element_l: 0,
                            _element_t: 0,
                            _origin_w: t.option.videoWidth,
                            _origin_h: t.option.videoHeight,
                            _caption: null,
                            captionCheckEl: null,
                            _captionChecked: !1,
                            _proportionChecked: !0,
                            _align: "none",
                            _floatClassRegExp: "__se__float\\-[a-z]+",
                            _resizing: t.option.videoResizing,
                            _youtubeQuery: t.option.youtubeQuery
                        };
                        let i = this.setDialog.call(e);
                        t.video.modal = i, t.video.focusElement = i.querySelector("._se_video_url"), t.video.captionCheckEl = i.querySelector("._se_video_check_caption"), i.querySelector(".ke-btn-primary").addEventListener("click", this.submit.bind(e)), t.video.videoWidth = {}, t.video.videoHeight = {}, t.option.videoResizing && (t.video.videoWidth = i.querySelector("._se_video_size_x"), t.video.videoHeight = i.querySelector("._se_video_size_y"), t.video.proportion = i.querySelector("._se_video_check_proportion"), t.video.videoWidth.value = t.option.videoWidth, t.video.videoHeight.value = t.option.videoHeight, t.video.videoWidth.addEventListener("change", this.setInputSize.bind(e, "x")), t.video.videoHeight.addEventListener("change", this.setInputSize.bind(e, "y")), i.querySelector(".ke-dialog-btn-revert").addEventListener("click", this.sizeRevert.bind(e))), t.dialog.modal.appendChild(i), i = null
                    },
                    setDialog: function() {
                        const e = this.context.option,
                            t = this.lang,
                            i = this.util.createElement("DIV");
                        i.className = "ke-dialog-content", i.style.display = "none";
                        let n = '<form class="editor_video">   <div class="ke-dialog-header">       <button type="button" data-command="close" class="close" aria-label="Close" title="' + t.dialogBox.close + '">           <i aria-hidden="true" data-command="close" class="ke-icon-cancel"></i>       </button>       <span class="ke-modal-title">' + t.dialogBox.videoBox.title + '</span>   </div>   <div class="ke-dialog-body">       <div class="ke-dialog-form">           <label>' + t.dialogBox.videoBox.url + '</label>           <input class="ke-input-form _se_video_url" type="text" />       </div>';
                        return e.videoResizing && (n += '   <div class="ke-dialog-form">       <div class="ke-dialog-size-text"><label class="size-w">' + t.dialogBox.width + '</label><label class="ke-dialog-size-x">&nbsp;</label><label class="size-h">' + t.dialogBox.height + '</label></div>       <input type="number" class="ke-input-control _se_video_size_x" /><label class="ke-dialog-size-x">x</label><input type="number" class="ke-input-control _se_video_size_y" />       <label><input type="checkbox" class="ke-dialog-btn-check _se_video_check_proportion" checked/>&nbsp;' + t.dialogBox.proportion + '</label>       <button type="button" title="' + t.dialogBox.revertButton + '" class="ke-btn ke-dialog-btn-revert" style="float: right;"><i class="ke-icon-revert"></i></button>   </div>'), n += '       <div class="ke-dialog-form-footer">           <label><input type="checkbox" class="ke-dialog-btn-check _se_video_check_caption" />&nbsp;' + t.dialogBox.caption + '</label>       </div>   </div>   <div class="ke-dialog-footer">       <div class="footer-div">           <label><input type="radio" name="keditor_video_radio" class="ke-dialog-btn-radio" value="none" checked>' + t.dialogBox.basic + '</label>           <label><input type="radio" name="keditor_video_radio" class="ke-dialog-btn-radio" value="left">' + t.dialogBox.left + '</label>           <label><input type="radio" name="keditor_video_radio" class="ke-dialog-btn-radio" value="center">' + t.dialogBox.center + '</label>           <label><input type="radio" name="keditor_video_radio" class="ke-dialog-btn-radio" value="right">' + t.dialogBox.right + '</label>       </div>       <button type="button" data-command="close" class="ke-btn-cancel" aria-label="Close" title="' + t.dialogBox.close + '"><span data-command="close">' + t.dialogBox.close + '</span></button>       <button type="submit" class="ke-btn-primary" title="' + t.dialogBox.submitButton + '"><span>' + t.dialogBox.submitButton + "</span></button>   </div></form>", i.innerHTML = n, i
                    },
                    setInputSize: function(e) {
                        this.context.video.proportion.checked && ("x" === e ? this.context.video.videoHeight.value = Math.round(this.context.video._element_h / this.context.video._element_w * this.context.video.videoWidth.value) : this.context.video.videoWidth.value = Math.round(this.context.video._element_w / this.context.video._element_h * this.context.video.videoHeight.value))
                    },
                    submitAction: function() {
                        if (0 === this.context.video.focusElement.value.trim().length) return !1;
                        const e = this.context.video,
                            t = /^\d+$/.test(e.videoWidth.value) ? e.videoWidth.value : this.context.option.videoWidth,
                            i = /^\d+$/.test(e.videoHeight.value) ? e.videoHeight.value : this.context.option.videoHeight;
                        let n = null,
                            o = null,
                            l = null,
                            s = e.focusElement.value.trim();
                        if (e._align = e.modal.querySelector('input[name="keditor_video_radio"]:checked').value, /^<iframe.*\/iframe>$/.test(s)) n = (new this._w.DOMParser).parseFromString(s, "text/html").querySelector("iframe");
                        else {
                            if (n = this.util.createElement("IFRAME"), /youtu\.?be/.test(s) && (s = s.replace("watch?v=", ""), /^\/\/.+\/embed\//.test(s) || (s = s.replace(s.match(/\/\/.+\//)[0], "//www.youtube.com/embed/")), e._youtubeQuery.length > 0))
                                if (/\?/.test(s)) {
                                    const t = s.split("?");
                                    s = t[0] + "?" + e._youtubeQuery + "&" + t[1]
                                } else s += "?" + e._youtubeQuery;
                            n.src = s
                        }
                        this.context.dialog.updateModal ? (e._element.src = n.src, l = e._container, o = this.util.getParentElement(e._element, "FIGURE"), n = e._element) : (n.frameBorder = "0", n.allowFullscreen = !0, n.onload = function() {
                            this.setAttribute("origin-size", this.offsetWidth + "," + this.offsetHeight), this.setAttribute("data-origin", this.offsetWidth + "," + this.offsetHeight), this.style.height = this.offsetHeight + "px"
                        }.bind(n), e._element = n, o = this.plugins.resizing.set_cover.call(this, n), l = this.plugins.resizing.set_container.call(this, o, "ke-video-container"), this._variable._videosCnt++), e._cover = o, e._container = l;
                        const a = 1 * t !== n.offsetWidth || 1 * i !== n.offsetHeight;
                        e._resizing && (this.context.video._proportionChecked = e.proportion.checked, n.setAttribute("data-proportion", e._proportionChecked)), e._captionChecked ? e._caption || (e._caption = this.plugins.resizing.create_caption.call(this), o.appendChild(e._caption)) : e._caption && (this.util.removeItem(e._caption), e._caption = null), a && this.plugins.video.setSize.call(this, t, i), e._align && "none" !== e._align ? o.style.margin = "auto" : o.style.margin = "0", this.util.removeClass(l, this.context.video._floatClassRegExp), this.util.addClass(l, "__se__float-" + e._align), n.setAttribute("data-align", e._align), this.context.dialog.updateModal ? (/\d+/.test(o.style.height) || e._resizing && a || this.context.resizing._rotateVertical && e._captionChecked) && this.plugins.resizing.setTransformSize.call(this, n, null, null) : this.insertComponent(l), this.history.push()
                    },
                    submit: function(e) {
                        this.showLoading(), e.preventDefault(), e.stopPropagation(), this.context.video._captionChecked = this.context.video.captionCheckEl.checked;
                        try {
                            this.plugins.video.submitAction.call(this)
                        } finally {
                            this.plugins.dialog.close.call(this), this.closeLoading()
                        }
                        return this.focus(), !1
                    },
                    _update_videoCover: function(e) {
                        const t = this.context.video;
                        e.frameBorder = "0", e.allowFullscreen = !0, e.onload = function() {
                            this.setAttribute("origin-size", this.offsetWidth + "," + this.offsetHeight), this.setAttribute("data-origin", this.offsetWidth + "," + this.offsetHeight), this.style.height = this.offsetHeight + "px"
                        }.bind(e);
                        const i = this.util.getParentElement(e, this.util.isComponent) || this.util.getParentElement(e, function(e) {
                            return this.isWysiwygDiv(e.parentNode)
                        }.bind(this.util));
                        t._element = e = e.cloneNode(!1);
                        const n = this.plugins.resizing.set_cover.call(this, e),
                            o = this.plugins.resizing.set_container.call(this, n, "ke-video-container"),
                            l = i.getElementsByTagName("FIGCAPTION")[0];
                        if (l) {
                            const e = this.plugins.resizing.create_caption.call(this);
                            e.innerHTML = l.innerHTML, n.appendChild(e)
                        }
                        const s = (e.getAttribute("origin-size") || "").split(","),
                            a = s[0] || this.context.option.videoWidth,
                            r = s[1] || this.context.option.videoHeight;
                        this.plugins.video.setSize.call(this, a, r), i.parentNode.insertBefore(o, i), this.util.removeItem(i)
                    },
                    sizeRevert: function() {
                        const e = this.context.video;
                        e._origin_w && (e.videoWidth.value = e._element_w = e._origin_w, e.videoHeight.value = e._element_h = e._origin_h)
                    },
                    onModifyMode: function(e, t) {
                        const i = this.context.video;
                        i._element = e, i._cover = this.util.getParentElement(e, "FIGURE"), i._container = this.util.getParentElement(e, ".ke-video-container"), i._caption = this.util.getChildElement(i._cover, "FIGCAPTION"), i._align = e.getAttribute("data-align") || "none", i._element_w = t.w, i._element_h = t.h, i._element_t = t.t, i._element_l = t.l;
                        let n = i._element.getAttribute("data-origin");
                        n ? (n = n.split(","), i._origin_w = 1 * n[0], i._origin_h = 1 * n[1]) : (i._origin_w = t.w, i._origin_h = t.h, i._element.setAttribute("data-origin", t.w + "," + t.h))
                    },
                    openModify: function(e) {
                        const t = this.context.video;
                        t.focusElement.value = t._element.src, t.videoWidth.value = t._element.offsetWidth, t.videoHeight.value = t._element.offsetHeight, t._captionChecked = t.captionCheckEl.checked = !!t._caption, t.modal.querySelector('input[name="keditor_video_radio"][value="' + t._align + '"]').checked = !0, t._resizing && (t.proportion.checked = t._proportionChecked = "false" !== t._element.getAttribute("data-proportion"), t.proportion.disabled = !1), e || this.plugins.dialog.open.call(this, "video", !0)
                    },
                    checkVideosInfo: function() {
                        const e = this.context.element.wysiwyg.getElementsByTagName("IFRAME");
                        if (e.length === this._variable._videosCnt) return;
                        const t = this.plugins.video;
                        this._variable._videosCnt = e.length;
                        for (let i, n = 0, o = this._variable._videosCnt; n < o; n++) i = e[n], this.util.getParentElement(i, ".ke-video-container") || t._update_videoCover.call(this, i)
                    },
                    setSize: function(e, t) {
                        const i = this.context.video;
                        i._element.style.width = /^\d+$/.test(e) ? e + "px" : e, i._element.style.height = /^\d+$/.test(t) ? t + "px" : t
                    },
                    setAutoSize: function() {
                        const e = this.context.video;
                        this.plugins.resizing.resetTransform.call(this, e._element), this.plugins.video.cancelPercentAttr.call(this);
                        const t = (e._element.getAttribute("data-origin") || "").split(","),
                            i = (t[0] || this.context.option.videoWidth) + "px",
                            n = (t[1] || this.context.option.videoHeight) + "px";
                        e._element.style.maxWidth = "100%", e._cover.style.width = e._element.style.width = i, e._cover.style.height = e._element.style.height = n
                    },
                    setPercentSize: function(e) {
                        const t = this.context.video;
                        t._element.style.maxWidth = "100%", t._container.style.width = e, t._container.style.height = "", t._cover.style.width = "100%", t._cover.style.height = "", t._element.style.width = "100%", t._element.style.height = t._origin_h / t._origin_w * t._element.offsetWidth + "px", /100/.test(e) && (this.util.removeClass(t._container, this.context.video._floatClassRegExp), this.util.addClass(t._container, "__se__float-center"))
                    },
                    cancelPercentAttr: function() {
                        const e = this.context.video;
                        e._element.style.maxWidth = "none", e._cover.style.width = "", e._cover.style.height = "", e._container.style.width = "", e._container.style.height = "", this.util.removeClass(e._container, this.context.video._floatClassRegExp), this.util.addClass(e._container, "__se__float-" + e._align)
                    },
                    resetAlign: function() {
                        const e = this.context.video;
                        e._element.setAttribute("data-align", ""), e._align = "none", e._cover.style.margin = "0", this.util.removeClass(e._container, e._floatClassRegExp)
                    },
                    destroy: function() {
                        this._variable._videosCnt--, this.util.removeItem(this.context.video._container), this.plugins.video.init.call(this), this.controllersOff(), this.history.push()
                    },
                    init: function() {
                        const e = this.context.video;
                        e.focusElement.value = "", e.captionCheckEl.checked = !1, e._origin_w = this.context.option.videoWidth, e._origin_h = this.context.option.videoHeight, e.modal.querySelector('input[name="keditor_video_radio"][value="none"]').checked = !0, e._resizing && (e.videoWidth.value = this.context.option.videoWidth, e.videoHeight.value = this.context.option.videoHeight, e.proportion.checked = !0, e.proportion.disabled = !0)
                    }
                }
            },
            r = i("P6u4"),
            c = i.n(r);
        const d = {
            _d: document,
            _w: window,
            _onlyZeroWidthRegExp: new RegExp("^" + String.fromCharCode(8203) + "+$"),
            _tagConvertor: function(e) {
                const t = {
                    b: "strong",
                    i: "em",
                    var: "em",
                    u: "ins",
                    strike: "del",
                    s: "del"
                };
                return e.replace(/(<\/?)(b|strong|var|i|em|u|ins|s|strike|del)\b\s*(?:[^>^<]+)?\s*(?=>)/gi, (function(e, i, n) {
                    return i + ("string" == typeof t[n] ? t[n] : n)
                }))
            },
            zeroWidthSpace: "​",
            onlyZeroWidthSpace: function(e) {
                return "string" != typeof e && (e = e.textContent), "" === e || this._onlyZeroWidthRegExp.test(e)
            },
            getXMLHttpRequest: function() {
                if (!this._w.ActiveXObject) return this._w.XMLHttpRequest ? new XMLHttpRequest : null;
                try {
                    return new ActiveXObject("Msxml2.XMLHTTP")
                } catch (e) {
                    try {
                        return new ActiveXObject("Microsoft.XMLHTTP")
                    } catch (e) {
                        return null
                    }
                }
            },
            createElement: function(e) {
                return this._d.createElement(e)
            },
            createTextNode: function(e) {
                return this._d.createTextNode(e || "")
            },
            getIncludePath: function(e, t) {
                let i = "";
                const n = [],
                    o = "js" === t ? "script" : "link",
                    l = "js" === t ? "src" : "href";
                let s = "(?:";
                for (let t = 0, i = e.length; t < i; t++) s += e[t] + (t < i - 1 ? "|" : ")");
                const a = new this._w.RegExp("(^|.*[\\/])" + s + "(\\.[^\\/]+)?." + t + "(?:\\?.*|;.*)?$", "i"),
                    r = new this._w.RegExp(".+\\." + t + "(?:\\?.*|;.*)?$", "i");
                for (let e = this._d.getElementsByTagName(o), t = 0; t < e.length; t++) r.test(e[t][l]) && n.push(e[t]);
                for (let e = 0; e < n.length; e++) {
                    let t = n[e][l].match(a);
                    if (t) {
                        i = t[0];
                        break
                    }
                }
                if ("" === i && (i = n.length > 0 ? n[0][l] : ""), -1 === i.indexOf(":/") && "//" !== i.slice(0, 2) && (i = 0 === i.indexOf("/") ? location.href.match(/^.*?:\/\/[^\/]*/)[0] + i : location.href.match(/^[^\?]*\/(?:)/)[0] + i), !i) throw "[KEDITOR.util.getIncludePath.fail] The KEDITOR installation path could not be automatically detected. (name: +" + name + ", extension: " + t + ")";
                return i
            },
            getPageStyle: function() {
                let e = "";
                const t = this._d.styleSheets;
                for (let i, n = 0, o = t.length; n < o; n++) {
                    try {
                        i = t[n].cssRules
                    } catch (e) {
                        continue
                    }
                    for (let t = 0, n = i.length; t < n; t++) e += i[t].cssText
                }
                return e
            },
            convertContentsForEditor: function(e) {
                let t, i, n = "";
                e = e.trim();
                for (let o = 0, l = (t = this._d.createRange().createContextualFragment(e).childNodes).length; o < l; o++)
                    if (i = t[o].outerHTML || t[o].textContent, 3 === t[o].nodeType) {
                        const e = i.split(/\n/g);
                        let t = "";
                        for (let i = 0, o = e.length; i < o; i++)(t = e[i].trim()).length > 0 && (n += "<P>" + t + "</p>")
                    } else n += i;
                const o = {
                    "&": "&amp;",
                    " ": "&nbsp;",
                    "'": "&quot;",
                    "<": "&amp;lt;",
                    ">": "&amp;gt;"
                };
                return e = e.replace(/&|\u00A0|'|<|>/g, (function(e) {
                    return "string" == typeof o[e] ? o[e] : e
                })), 0 === n.length && (n = "<p>" + (e.length > 0 ? e : "<br>") + "</p>"), this._tagConvertor(n.replace(this._deleteExclusionTags, ""))
            },
            convertHTMLForCodeView: function(e) {
                let t = "";
                const i = this._w.RegExp;
                return function e(n) {
                    const o = n.childNodes;
                    for (let n, l = 0, s = o.length; l < s; l++)
                        if (n = o[l], /^(BLOCKQUOTE|TABLE|THEAD|TBODY|TR|OL|UL|FIGCAPTION)$/i.test(n.nodeName)) {
                            n.innerHTML = n.innerHTML.replace(/\n/g, "");
                            const o = n.nodeName.toLowerCase();
                            t += n.outerHTML.match(i("<" + o + "[^>]*>", "i"))[0] + "\n", e(n), t += "</" + o + ">\n"
                        } else t += 3 === n.nodeType ? /^\n+$/.test(n.data) ? "" : n.data : n.outerHTML + "\n"
                }(e), t
            },
            isWysiwygDiv: function(e) {
                return !(!e || 1 !== e.nodeType || !this.hasClass(e, "ke-wrapper-wysiwyg") && !/^BODY$/i.test(e.nodeName))
            },
            isFormatElement: function(e) {
                return !(!e || 1 !== e.nodeType || !/^(P|DIV|H[1-6]|LI|TH|TD)$/i.test(e.nodeName) || this.isComponent(e) || this.isWysiwygDiv(e))
            },
            isRangeFormatElement: function(e) {
                return !(!e || 1 !== e.nodeType || !/^(BLOCKQUOTE|OL|UL|PRE|FIGCAPTION|TABLE|THEAD|TBODY|TR|TH|TD)$/i.test(e.nodeName) && "range" !== e.getAttribute("data-format"))
            },
            isComponent: function(e) {
                return e && (/ke-component/.test(e.className) || /^(TABLE|HR)$/.test(e.nodeName))
            },
            getFormatElement: function(e, t) {
                if (!e) return null;
                for (t || (t = function() {
                        return !0
                    }); e;) {
                    if (this.isWysiwygDiv(e)) return null;
                    if (this.isRangeFormatElement(e) && e.firstElementChild, this.isFormatElement(e) && t(e)) return e;
                    e = e.parentNode
                }
                return null
            },
            getRangeFormatElement: function(e, t) {
                if (!e) return null;
                for (t || (t = function() {
                        return !0
                    }); e;) {
                    if (this.isWysiwygDiv(e)) return null;
                    if (this.isRangeFormatElement(e) && !/^(THEAD|TBODY|TR)$/i.test(e.nodeName) && t(e)) return e;
                    e = e.parentNode
                }
                return null
            },
            getArrayIndex: function(e, t) {
                let i = -1;
                for (let n = 0, o = e.length; n < o; n++)
                    if (e[n] === t) {
                        i = n;
                        break
                    } return i
            },
            nextIdx: function(e, t) {
                let i = this.getArrayIndex(e, t);
                return -1 === i ? -1 : i + 1
            },
            prevIdx: function(e, t) {
                let i = this.getArrayIndex(e, t);
                return -1 === i ? -1 : i - 1
            },
            getPositionIndex: function(e) {
                let t = 0;
                for (; e = e.previousSibling;) t += 1;
                return t
            },
            getNodePath: function(e, t) {
                const i = [];
                let n = !0;
                return this.getParentElement(e, function(e) {
                    return e === t && (n = !1), n && !this.isWysiwygDiv(e) && i.push(e), !1
                }.bind(this)), i.map(this.getPositionIndex).reverse()
            },
            getNodeFromPath: function(e, t) {
                let i, n = t;
                for (let t = 0, o = e.length; t < o && 0 !== (i = n.childNodes).length; t++) n = i.length <= e[t] ? i[i.length - 1] : i[e[t]];
                return n
            },
            isList: function(e) {
                return e && /^(OL|UL)$/i.test("string" == typeof e ? e : e.nodeName)
            },
            isListCell: function(e) {
                return e && /^LI$/i.test("string" == typeof e ? e : e.nodeName)
            },
            isTable: function(e) {
                return e && /^(TABLE|THEAD|TBODY|TR|TH|TD)$/i.test("string" == typeof e ? e : e.nodeName)
            },
            isCell: function(e) {
                return e && /^(TD|TH)$/i.test("string" == typeof e ? e : e.nodeName)
            },
            isBreak: function(e) {
                return e && /^BR$/i.test("string" == typeof e ? e : e.nodeName)
            },
            getListChildren: function(e, t) {
                const i = [];
                return e && e.children && 0 !== e.children.length ? (t = t || function() {
                    return !0
                }, function n(o) {
                    (e !== o && t(o) || /^BR$/i.test(e.nodeName)) && i.push(o);
                    for (let e = 0, t = o.children.length; e < t; e++) n(o.children[e])
                }(e), i) : i
            },
            getListChildNodes: function(e, t) {
                const i = [];
                return e && 0 !== e.childNodes.length ? (t = t || function() {
                    return !0
                }, function n(o) {
                    (e !== o && t(o) || /^BR$/i.test(e.nodeName)) && i.push(o);
                    for (let e = 0, t = o.childNodes.length; e < t; e++) n(o.childNodes[e])
                }(e), i) : i
            },
            getElementDepth: function(e) {
                let t = 0;
                for (e = e.parentNode; e && !this.isWysiwygDiv(e);) t += 1, e = e.parentNode;
                return t
            },
            getParentElement: function(e, t) {
                let i;
                if ("function" == typeof t) i = t;
                else {
                    let e;
                    /^\./.test(t) ? (e = "className", t = t.split(".")[1]) : /^#/.test(t) ? (e = "id", t = "^" + t.split("#")[1] + "$") : /^:/.test(t) ? (e = "name", t = "^" + t.split(":")[1] + "$") : (e = "nodeName", t = "^" + t + "$");
                    const n = new this._w.RegExp(t, "i");
                    i = function(t) {
                        return n.test(t[e])
                    }
                }
                for (; e && !i(e);) {
                    if (this.isWysiwygDiv(e)) return null;
                    e = e.parentNode
                }
                return e
            },
            getChildElement: function(e, t, i) {
                let n;
                if ("function" == typeof t) n = t;
                else {
                    let e;
                    /^\./.test(t) ? (e = "className", t = t.split(".")[1]) : /^#/.test(t) ? (e = "id", t = "^" + t.split("#")[1] + "$") : /^:/.test(t) ? (e = "name", t = "^" + t.split(":")[1] + "$") : (e = "nodeName", t = "^" + t + "$");
                    const i = new this._w.RegExp(t, "i");
                    n = function(t) {
                        return i.test(t[e])
                    }
                }
                const o = this.getListChildNodes(e, (function(e) {
                    return n(e)
                }));
                return o[i ? o.length - 1 : 0]
            },
            getEdgeChildNodes: function(e, t) {
                if (e) {
                    for (t || (t = e); e && 1 === e.nodeType && e.childNodes.length > 0 && !this.isBreak(e);) e = e.firstChild;
                    for (; t && 1 === t.nodeType && t.childNodes.length > 0 && !this.isBreak(t);) t = t.lastChild;
                    return {
                        sc: e,
                        ec: t || e
                    }
                }
            },
            getOffset: function(e, t) {
                let i = 0,
                    n = 0,
                    o = 3 === e.nodeType ? e.parentElement : e;
                const l = this.getParentElement(e, this.isWysiwygDiv.bind(this));
                for (; o && !this.hasClass(o, "ke-container") && o !== l;) i += o.offsetLeft, n += o.offsetTop, o = o.offsetParent;
                const s = t && /iframe/i.test(t.nodeName);
                return {
                    left: i + (s ? t.offsetLeft : 0),
                    top: n - l.scrollTop + (s ? t.offsetTop : 0)
                }
            },
            getOverlapRangeAtIndex: function(e, t, i, n) {
                if (e <= n ? t < i : t > i) return 0;
                const o = (e > i ? e : i) - (t < n ? t : n);
                return (o < 0 ? -1 * o : o) + 1
            },
            changeTxt: function(e, t) {
                e && t && (e.textContent = t)
            },
            hasClass: function(e, t) {
                if (e) return e.classList.contains(t.trim())
            },
            addClass: function(e, t) {
                if (!e) return;
                new this._w.RegExp("(\\s|^)" + t + "(\\s|$)").test(e.className) || (e.className += " " + t)
            },
            removeClass: function(e, t) {
                if (!e) return;
                const i = new this._w.RegExp("(\\s|^)" + t + "(\\s|$)");
                e.className = e.className.replace(i, " ").trim()
            },
            toggleClass: function(e, t) {
                if (!e) return;
                const i = new this._w.RegExp("(\\s|^)" + t + "(\\s|$)");
                i.test(e.className) ? e.className = e.className.replace(i, " ").trim() : e.className += " " + t
            },
            removeItem: function(e) {
                if (e) try {
                    e.remove()
                } catch (t) {
                    e.parentNode.removeChild(e)
                }
            },
            removeItemAllParents: function(e, t) {
                if (!e) return null;
                let i = null;
                return t || (t = function(e) {
                        const t = e.textContent.trim();
                        return 0 === t.length || /^(\n|\u200B)+$/.test(t)
                    }),
                    function e(n) {
                        if (!d.isWysiwygDiv(n)) {
                            const o = n.parentNode;
                            o && t(n) && (i = {
                                sc: n.previousElementSibling,
                                ec: n.nextElementSibling
                            }, d.removeItem(n), e(o))
                        }
                    }(e), i
            },
            removeEmptyNode: function(e) {
                const t = this;
                ! function i(n) {
                    if (n === e || !t.onlyZeroWidthSpace(n.textContent) || /^BR$/i.test(n.nodeName) || n.firstChild && /^BR$/i.test(n.firstChild.nodeName) || t.isComponent(n)) {
                        const e = n.children;
                        for (let n = 0, o = e.length, l = 0; n < o; n++) e[n + l] && !t.isComponent(e[n + l]) && (l += i(e[n + l]))
                    } else if (n.parentNode) return n.parentNode.removeChild(n), -1;
                    return 0
                }(e), 0 === e.childNodes.length && (e.innerHTML = "<br>")
            },
            isIgnoreNodeChange: function(e) {
                return 3 !== e.nodeType && !/^(span|font|b|strong|var|i|em|u|ins|s|strike|del|sub|sup|mark)$/i.test(e.nodeName)
            },
            cleanHTML: function(e) {
                const t = new this._w.RegExp("^(meta|script|link|style|[a-z]+:[a-z]+)$", "i"),
                    i = this._d.createRange().createContextualFragment(e).childNodes;
                let n = "";
                for (let e = 0, o = i.length; e < o; e++) t.test(i[e].nodeName) || (n += 1 === i[e].nodeType ? i[e].outerHTML : 3 === i[e].nodeType ? i[e].textContent : "");
                return n = n.replace(/<([a-zA-Z]+\:[a-zA-Z]+|script|style).*>(\n|.)*<\/([a-zA-Z]+\:[a-zA-Z]+|script|style)>/g, "").replace(/(<[a-zA-Z0-9]+)[^>]*(?=>)/g, (function(e, t) {
                    const i = e.match(/((?:colspan|rowspan|target|href|src|class|data-file-size|data-file-name|data-origin|origin-size|data-format)\s*=\s*"[^"]*")/gi);
                    if (i)
                        for (let e = 0, n = i.length; e < n; e++) /^class="(?!__se__)/.test(i[e]) || (t += " " + i[e]);
                    return t
                })).replace(/<\/?(span[^>^<]*)>/g, "").replace(this._deleteExclusionTags, ""), this._tagConvertor(n || e)
            },
            _deleteExclusionTags: function() {
                const e = "br|p|div|pre|blockquote|h[1-6]|ol|ul|dl|li|hr|figure|figcaption|img|iframe|audio|video|table|thead|tbody|tr|th|td|a|b|strong|var|i|em|u|ins|s|span|strike|del|sub|sup|mark".split("|");
                let t = "<\\/?(";
                for (let i = 0, n = e.length; i < n; i++) t += "(?!\\b" + e[i] + "\\b)";
                return t += "[^>^<])+>", new RegExp(t, "g")
            }()
        };
        var u = d,
            h = {
                init: function(e, t) {
                    "object" != typeof t && (t = {});
                    const i = document;
                    this._initOptions(e, t);
                    const n = i.createElement("DIV");
                    n.className = "keditor", e.id && (n.id = "keditor_" + e.id);
                    const o = i.createElement("DIV");
                    o.className = "ke-container";
                    const l = this._createToolBar(i, t.buttonList, t.plugins, t.lang),
                        s = i.createElement("DIV");
                    s.className = "ke-arrow";
                    const a = i.createElement("DIV");
                    a.className = "ke-toolbar-sticky-dummy";
                    const r = i.createElement("DIV");
                    r.className = "ke-wrapper";
                    const c = u.convertContentsForEditor(e.value),
                        d = this._initElements(t, n, l.element, s, c),
                        h = d.bottomBar,
                        g = d.wysiwygFrame;
                    let p = d.codeView;
                    const m = h.resizingBar,
                        f = h.navigation,
                        _ = h.charCounter,
                        b = (h.poweredBy, i.createElement("DIV"));
                    b.className = "ke-loading-box keditor-common", b.innerHTML = '<div class="ke-loading-effect"></div>';
                    const y = i.createElement("DIV");
                    return y.className = "ke-resizing-back", r.appendChild(g), r.appendChild(p), o.appendChild(l.element), o.appendChild(a), o.appendChild(r), o.appendChild(y), o.appendChild(b), m && o.appendChild(m), n.appendChild(o), p = this._checkCodeMirror(t, p), {
                        constructed: {
                            _top: n,
                            _relative: o,
                            _toolBar: l.element,
                            _editorArea: r,
                            _wysiwygArea: g,
                            _codeArea: p,
                            _resizingBar: m,
                            _navigation: f,
                            _charCounter: _,
                            _loading: b,
                            _resizeBack: y,
                            _stickyDummy: a,
                            _arrow: s
                        },
                        options: t,
                        plugins: l.plugins,
                        pluginCallButtons: l.pluginCallButtons
                    }
                },
                _checkCodeMirror: function(e, t) {
                    if (e.codeMirror) {
                        const i = [{
                            mode: "htmlmixed",
                            htmlMode: !0,
                            lineNumbers: !0
                        }, e.codeMirror.options || {}].reduce((function(e, t) {
                            return Object.keys(t).forEach((function(i) {
                                e[i] = t[i]
                            })), e
                        }), {});
                        "auto" === e.height && (i.viewportMargin = 1 / 0);
                        const n = e.codeMirror.src.fromTextArea(t, i);
                        n.display.wrapper.style.cssText = t.style.cssText, e.codeMirrorEditor = n, (t = n.display.wrapper).className += " ke-wrapper-code-mirror"
                    }
                    return t
                },
                _setOptions: function(e, t, i, n) {
                    this._initOptions(t.element.originElement, e);
                    const o = t.element,
                        l = o.relative,
                        s = o.editorArea,
                        a = !!e.buttonList || e.mode !== n.mode,
                        r = !!e.plugins,
                        c = this._createToolBar(document, a ? e.buttonList : n.buttonList, r ? e.plugins : i, e.lang),
                        d = document.createElement("DIV");
                    d.className = "ke-arrow", a && (l.insertBefore(c.element, o.toolbar), l.removeChild(o.toolbar), o.toolbar = c.element, o._arrow = d);
                    const u = this._initElements(e, o.topArea, a ? c.element : o.toolbar, d, o.wysiwyg.innerHTML),
                        h = u.bottomBar,
                        g = u.wysiwygFrame;
                    let p = u.codeView;
                    return o.resizingBar && l.removeChild(o.resizingBar), h.resizingBar && l.appendChild(h.resizingBar), o.resizingBar = h.resizingBar, o.navigation = h.navigation, o.charCounter = h.charCounter, o.poweredBy = h.poweredBy, s.removeChild(o.wysiwygFrame), s.removeChild(o.code), s.appendChild(g), s.appendChild(p), p = this._checkCodeMirror(e, p), o.wysiwygFrame = g, o.code = p, {
                        callButtons: a ? c.pluginCallButtons : null,
                        plugins: a || r ? c.plugins : null
                    }
                },
                _initElements: function(e, t, i, n, o) {
                    t.style.width = e.width, t.style.minWidth = e.minWidth, t.style.maxWidth = e.maxWidth, t.style.display = e.display, "string" == typeof e.position && (t.style.position = e.position), /inline/i.test(e.mode) ? (i.className += " ke-toolbar-inline", i.style.width = e.toolbarWidth) : /balloon/i.test(e.mode) && (i.className += " ke-toolbar-balloon", i.appendChild(n));
                    const l = document.createElement(e.iframe ? "IFRAME" : "DIV");
                    if (l.className = "ke-wrapper-inner ke-wrapper-wysiwyg", l.style.display = "block", e.iframe) {
                        const t = function() {
                            const t = new RegExp("(^|.*[\\/])" + (e.iframeCSSFileName || "keditor") + "(\\..+)?.css(?:\\?.*|;.*)?$", "i"),
                                i = [];
                            for (let e, n = document.getElementsByTagName("link"), o = 0, l = n.length; o < l; o++)(e = n[o].href.match(t)) && i.push(e[0]);
                            if (!i || 0 === i.length) throw '[KEDITOR.constructor.iframe.fail] The keditor CSS files installation path could not be automatically detected. Please set the option property "iframeCSSFileName" before creating editor instances.';
                            let n = "";
                            for (let e = 0, t = i.length; e < t; e++) n += '<link href="' + i[e] + '" rel="stylesheet">';
                            return n
                        }() + ("auto" === e.height ? "<style>\n/** Iframe height auto */\nbody{height: min-content; overflow: hidden;}\n</style>" : "");
                        l.allowFullscreen = !0, l.frameBorder = 0, l.addEventListener("load", (function() {
                            this.setAttribute("scrolling", "auto"), this.contentDocument.head.innerHTML = '<meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1"><title></title>' + t, this.contentDocument.body.className = "keditor-editable", this.contentDocument.body.setAttribute("contenteditable", !0), this.contentDocument.body.innerHTML = o
                        }))
                    } else l.setAttribute("contenteditable", !0), l.setAttribute("scrolling", "auto"), l.className += " keditor-editable", l.innerHTML = o;
                    l.style.height = e.height, l.style.minHeight = e.minHeight, l.style.maxHeight = e.maxHeight;
                    const s = document.createElement("TEXTAREA");
                    s.className = "ke-wrapper-inner ke-wrapper-code", s.style.display = "none", s.style.height = e.height, s.style.minHeight = e.minHeight, s.style.maxHeight = e.maxHeight, "auto" === e.height && (s.style.overflow = "hidden");
                    let a = null,
                        r = null,
                        c = null,
                        d = null;
                    if (e.resizingBar) {
                        if ((a = document.createElement("DIV")).className = "ke-resizing-bar keditor-common", (r = document.createElement("DIV")).className = "ke-navigation keditor-common", a.appendChild(r), e.charCounter) {
                            const t = document.createElement("DIV");
                            if (t.className = "ke-char-counter-wrapper", (c = document.createElement("SPAN")).className = "ke-char-counter", c.textContent = "0", t.appendChild(c), e.maxCharCount > 0) {
                                const i = document.createElement("SPAN");
                                i.textContent = " / " + e.maxCharCount, t.appendChild(i)
                            }
                            a.appendChild(t)
                        }(d = document.createElement("DIV")).className = "ke-powered-by", d.innerHTML = 'Editor: <a href="https://github.com/kothing/kothing-editor">Kothing</a>', a.appendChild(d)
                    }
                    return {
                        bottomBar: {
                            poweredBy: d,
                            resizingBar: a,
                            navigation: r,
                            charCounter: c
                        },
                        wysiwygFrame: l,
                        codeView: s
                    }
                },
                _initOptions: function(e, t) {
                    t.lang = t.lang || c.a, t.popupDisplay = t.popupDisplay || "full", t.display = t.display || ("none" !== e.style.display && e.style.display ? e.style.display : "block"), t.mode = t.mode || "classic", t.toolbarWidth = t.toolbarWidth ? /^\d+$/.test(t.toolbarWidth) ? t.toolbarWidth + "px" : t.toolbarWidth : "auto", t.stickyToolbar = /balloon/i.test(t.mode) || void 0 === t.stickyToolbar || null === t.stickyToolbar ? -1 : /^\d+/.test(t.stickyToolbar) ? 1 * t.stickyToolbar.toString().match(/\d+/)[0] : !0 === t.stickyToolbar ? 0 : -1, t.resizingBar = void 0 === t.resizingBar ? !/inline|balloon/i.test(t.mode) : t.resizingBar, t.showPathLabel = !!t.resizingBar && ("boolean" != typeof t.showPathLabel || t.showPathLabel), t.maxCharCount = /^\d+$/.test(t.maxCharCount) && t.maxCharCount > -1 ? 1 * t.maxCharCount : null, t.charCounter = t.maxCharCount > 0 || "boolean" == typeof t.charCounter && t.charCounter, t.width = t.width ? /^\d+$/.test(t.width) ? t.width + "px" : t.width : e.clientWidth ? e.clientWidth + "px" : "100%", t.minWidth = (/^\d+$/.test(t.minWidth) ? t.minWidth + "px" : t.minWidth) || "", t.maxWidth = (/^\d+$/.test(t.maxWidth) ? t.maxWidth + "px" : t.maxWidth) || "", t.height = t.height ? /^\d+$/.test(t.height) ? t.height + "px" : t.height : e.clientHeight ? e.clientHeight + "px" : "auto", t.minHeight = (/^\d+$/.test(t.minHeight) ? t.minHeight + "px" : t.minHeight) || "", t.maxHeight = (/^\d+$/.test(t.maxHeight) ? t.maxHeight + "px" : t.maxHeight) || "", t.font = t.font || null, t.fontSize = t.fontSize || null, t.formats = t.formats || null, t.colorList = t.colorList || null, t.imageResizing = void 0 === t.imageResizing || t.imageResizing, t.imageWidth = t.imageWidth && /\d+/.test(t.imageWidth) ? t.imageWidth.toString().match(/\d+/)[0] : "auto", t.imageFileInput = void 0 === t.imageFileInput || t.imageFileInput, t.imageUrlInput = void 0 === t.imageUrlInput || !t.imageFileInput || t.imageUrlInput, t.imageUploadHeader = t.imageUploadHeader || null, t.imageUploadUrl = t.imageUploadUrl || null, t.imageUploadSizeLimit = /\d+/.test(t.imageUploadSizeLimit) ? 1 * t.imageUploadSizeLimit.toString().match(/\d+/)[0] : null, t.videoResizing = void 0 === t.videoResizing || t.videoResizing, t.videoWidth = t.videoWidth && /\d+/.test(t.videoWidth) ? t.videoWidth.toString().match(/\d+/)[0] : 560, t.videoHeight = t.videoHeight && /\d+/.test(t.videoHeight) ? t.videoHeight.toString().match(/\d+/)[0] : 315, t.youtubeQuery = (t.youtubeQuery || "").replace("?", ""), t.codeMirror = t.codeMirror ? t.codeMirror.src ? t.codeMirror : {
                        src: t.codeMirror
                    } : null, t.iframe = t.fullPage || t.iframe, t.buttonList = t.buttonList || [
                        ["undo", "redo"],
                        ["bold", "underline", "italic", "strike", "subscript", "superscript"],
                        ["removeFormat"],
                        ["outdent", "indent"],
                        ["codeView", "showBlocks", "fullScreen"],
                        ["preview", "print"]
                    ]
                },
                _defaultButtons: function(e) {
                    return {
                        bold: ["_se_command_bold", e.toolbar.bold + " (CTRL+B)", "STRONG", "", '<i class="ke-icon-bold"></i>'],
                        underline: ["_se_command_underline", e.toolbar.underline + " (CTRL+U)", "INS", "", '<i class="ke-icon-underline"></i>'],
                        italic: ["_se_command_italic", e.toolbar.italic + " (CTRL+I)", "EM", "", '<i class="ke-icon-italic"></i>'],
                        strike: ["_se_command_strike", e.toolbar.strike + " (CTRL+SHIFT+S)", "DEL", "", '<i class="ke-icon-strokethrough"></i>'],
                        subscript: ["_se_command_subscript", e.toolbar.subscript, "SUB", "", '<i class="ke-icon-subscript"></i>'],
                        superscript: ["_se_command_superscript", e.toolbar.superscript, "SUP", "", '<i class="ke-icon-superscript"></i>'],
                        removeFormat: ["", e.toolbar.removeFormat, "removeFormat", "", '<i class="ke-icon-erase"></i>'],
                        indent: ["", e.toolbar.indent + " (CTRL+])", "indent", "", '<i class="ke-icon-indent-right"></i>'],
                        outdent: ["_se_command_outdent", e.toolbar.outdent + " (CTRL+[)", "outdent", "", '<i class="ke-icon-indent-left"></i>'],
                        fullScreen: ["code-view-enabled", e.toolbar.fullScreen, "fullScreen", "", '<i class="ke-icon-expansion"></i>'],
                        showBlocks: ["", e.toolbar.showBlocks, "showBlocks", "", '<i class="ke-icon-showBlocks"></i>'],
                        codeView: ["code-view-enabled", e.toolbar.codeView, "codeView", "", '<i class="ke-icon-code-view"></i>'],
                        undo: ["_se_command_undo", e.toolbar.undo + " (CTRL+Z)", "undo", "", '<i class="ke-icon-undo"></i>', !0],
                        redo: ["_se_command_redo", e.toolbar.redo + " (CTRL+Y / CTRL+SHIFT+Z)", "redo", "", '<i class="ke-icon-redo"></i>', !0],
                        preview: ["", e.toolbar.preview, "preview", "", '<i class="ke-icon-preview"></i>'],
                        print: ["", e.toolbar.print, "print", "", '<i class="ke-icon-print"></i>'],
                        save: ["_se_command_save", e.toolbar.save, "save", "", '<i class="ke-icon-save"></i>', !0],
                        font: ["ke-btn-select ke-btn-tool-font _se_command_font_family", e.toolbar.font, "font", "submenu", '<span class="txt">' + e.toolbar.font + '</span><i class="ke-icon-arrow-down"></i>'],
                        formatBlock: ["ke-btn-select ke-btn-tool-format", e.toolbar.formats, "formatBlock", "submenu", '<span class="txt _se_command_format">' + e.toolbar.formats + '</span><i class="ke-icon-arrow-down"></i>'],
                        fontSize: ["ke-btn-select ke-btn-tool-size", e.toolbar.fontSize, "fontSize", "submenu", '<span class="txt _se_command_font_size">' + e.toolbar.fontSize + '</span><i class="ke-icon-arrow-down"></i>'],
                        fontColor: ["", e.toolbar.fontColor, "fontColor", "submenu", '<i class="ke-icon-fontColor"></i>'],
                        hiliteColor: ["", e.toolbar.hiliteColor, "hiliteColor", "submenu", '<i class="ke-icon-hiliteColor"></i>'],
                        align: ["ke-btn-align", e.toolbar.align, "align", "submenu", '<i class="ke-icon-align-left _se_command_align"></i>'],
                        list: ["_se_command_list", e.toolbar.list, "list", "submenu", '<i class="ke-icon-list-number"></i>'],
                        horizontalRule: ["btn_line", e.toolbar.horizontalRule, "horizontalRule", "submenu", '<i class="ke-icon-hr"></i>'],
                        table: ["", e.toolbar.table, "table", "submenu", '<i class="ke-icon-grid"></i>'],
                        template: ["", e.toolbar.template, "template", "submenu", '<i class="ke-icon-template"></i>'],
                        link: ["", e.toolbar.link, "link", "dialog", '<i class="ke-icon-link"></i>'],
                        image: ["", e.toolbar.image, "image", "dialog", '<i class="ke-icon-image"></i>'],
                        video: ["", e.toolbar.video, "video", "dialog", '<i class="ke-icon-video"></i>']
                    }
                },
                _createModuleGroup: function(e) {
                    const t = u.createElement("DIV");
                    t.className = "ke-btn-module" + (e ? "" : " ke-btn-module-border");
                    const i = u.createElement("UL");
                    return i.className = "ke-menu-list", t.appendChild(i), {
                        div: t,
                        ul: i
                    }
                },
                _createButton: function(e, t, i, n, o, l) {
                    const s = u.createElement("LI"),
                        a = u.createElement("BUTTON");
                    return a.setAttribute("type", "button"), a.setAttribute("class", "ke-btn" + (e ? " " + e : "") + " ke-tooltip"), a.setAttribute("data-command", i), a.setAttribute("data-display", n), o += '<span class="ke-tooltip-inner"><span class="ke-tooltip-text">' + t + "</span></span>", l && a.setAttribute("disabled", !0), a.innerHTML = o, s.appendChild(a), {
                        li: s,
                        button: a
                    }
                },
                _createToolBar: function(e, t, i, n) {
                    const o = e.createElement("DIV");
                    o.className = "ke-toolbar-separator-vertical";
                    const l = e.createElement("DIV");
                    l.className = "ke-toolbar keditor-common";
                    const s = this._defaultButtons(n),
                        a = {},
                        r = {};
                    if (i) {
                        const e = i.length ? i : Object.keys(i).map((function(e) {
                            return i[e]
                        }));
                        for (let t = 0, i = e.length; t < i; t++) r[e[t].name] = e[t]
                    }
                    let c = null,
                        d = null,
                        u = null,
                        h = null,
                        g = "",
                        p = !1;
                    const m = 1 === t.length;
                    for (let i = 0; i < t.length; i++) {
                        const n = t[i];
                        if (u = this._createModuleGroup(m), "object" == typeof n) {
                            for (let e = 0; e < n.length; e++) "object" == typeof(d = n[e]) ? "function" == typeof d.add ? (c = s[g = d.name], r[g] = d) : (g = d.name, c = [d.buttonClass, d.title, d.dataCommand, d.dataDisplay, d.innerHTML]) : (c = s[d], g = d), h = this._createButton(c[0], c[1], c[2], c[3], c[4], c[5]), u.ul.appendChild(h.li), r[g] && (a[g] = h.button);
                            p && l.appendChild(o.cloneNode(!1)), l.appendChild(u.div), p = !0
                        } else if (/^\/$/.test(n)) {
                            const t = e.createElement("DIV");
                            t.className = "ke-btn-module-enter", l.appendChild(t), p = !1
                        }
                    }
                    const f = e.createElement("DIV");
                    return f.className = "ke-toolbar-cover", l.appendChild(f), {
                        element: l,
                        plugins: r,
                        pluginCallButtons: a
                    }
                }
            };
        var g = function(e, t, i) {
                return {
                    element: {
                        originElement: e,
                        topArea: t._top,
                        relative: t._relative,
                        toolbar: t._toolBar,
                        resizingBar: t._resizingBar,
                        navigation: t._navigation,
                        charCounter: t._charCounter,
                        editorArea: t._editorArea,
                        wysiwygFrame: t._wysiwygArea,
                        wysiwyg: i.iframe ? t._wysiwygArea.contentDocument.body : t._wysiwygArea,
                        code: t._codeArea,
                        loading: t._loading,
                        resizeBackground: t._resizeBack,
                        _stickyDummy: t._stickyDummy,
                        _arrow: t._arrow
                    },
                    tool: {
                        cover: t._toolBar.querySelector(".ke-toolbar-cover"),
                        bold: t._toolBar.querySelector("._se_command_bold"),
                        underline: t._toolBar.querySelector("._se_command_underline"),
                        italic: t._toolBar.querySelector("._se_command_italic"),
                        strike: t._toolBar.querySelector("._se_command_strike"),
                        subscript: t._toolBar.querySelector("._se_command_subscript"),
                        superscript: t._toolBar.querySelector("._se_command_superscript"),
                        font: t._toolBar.querySelector("._se_command_font_family .txt"),
                        fontTooltip: t._toolBar.querySelector("._se_command_font_family .ke-tooltip-text"),
                        format: t._toolBar.querySelector("._se_command_format"),
                        fontSize: t._toolBar.querySelector("._se_command_font_size"),
                        align: t._toolBar.querySelector("._se_command_align"),
                        list: t._toolBar.querySelector("._se_command_list"),
                        undo: t._toolBar.querySelector("._se_command_undo"),
                        redo: t._toolBar.querySelector("._se_command_redo"),
                        save: t._toolBar.querySelector("._se_command_save"),
                        outdent: t._toolBar.querySelector("._se_command_outdent")
                    },
                    option: i
                }
            },
            p = function(e, t) {
                const i = window,
                    n = e.context.element,
                    o = e.util,
                    l = e.context.tool.undo,
                    s = e.context.tool.redo;
                let a = null,
                    r = 0,
                    c = [{
                        contents: e.getContents(),
                        s: {
                            path: [0, 0],
                            offset: 0
                        },
                        e: {
                            path: [0, 0],
                            offset: 0
                        }
                    }];

                function d() {
                    const i = c[r];
                    n.wysiwyg.innerHTML = i.contents, e.setRange(o.getNodeFromPath(i.s.path, n.wysiwyg), i.s.offset, o.getNodeFromPath(i.e.path, n.wysiwyg), i.e.offset), e.focus(), 0 === r ? (l && l.setAttribute("disabled", !0), s && s.removeAttribute("disabled")) : r === c.length - 1 ? (l && l.removeAttribute("disabled"), s && s.setAttribute("disabled", !0)) : (l && l.removeAttribute("disabled"), s && s.removeAttribute("disabled")), e._checkComponents(), e._charCount(0, !1), e._iframeAutoHeight(), t()
                }
                return {
                    push: function() {
                        i.setTimeout(e._iframeAutoHeight), a && i.clearTimeout(a), a = i.setTimeout((function() {
                            i.clearTimeout(a), a = null,
                                function() {
                                    const i = e.getContents();
                                    if (i === c[r].contents) return;
                                    r++;
                                    const n = e.getRange();
                                    c.length > r && (c = c.slice(0, r), s && s.setAttribute("disabled", !0)), c[r] = {
                                        contents: i,
                                        s: {
                                            path: o.getNodePath(n.startContainer),
                                            offset: n.startOffset
                                        },
                                        e: {
                                            path: o.getNodePath(n.endContainer),
                                            offset: n.endOffset
                                        }
                                    }, 1 === r && l && l.removeAttribute("disabled"), e._checkComponents(), e._charCount(0, !1), t()
                                }()
                        }), 500)
                    },
                    undo: function() {
                        r > 0 && (r--, d())
                    },
                    redo: function() {
                        c.length - 1 > r && (r++, d())
                    },
                    reset: function() {
                        c = c[r = 0]
                    }
                }
            },
            m = {
                init: function(e) {
                    return {
                        create: function(t, i) {
                            return this.create(t, i, e)
                        }.bind(this)
                    }
                },
                create: function(e, t, i) {
                    "object" != typeof t && (t = {}), i && (t = [i, t].reduce((function(e, t) {
                        return Object.keys(t).forEach((function(i) {
                            e[i] = t[i]
                        })), e
                    }), {}));
                    const n = "string" == typeof e ? document.getElementById(e) : e;
                    if (!n) {
                        if ("string" == typeof e) throw Error('[KEDITOR.create.fail] The element for that id was not found (ID:"' + e + '")');
                        throw Error("[KEDITOR.create.fail] keditor requires textarea's element or id value")
                    }
                    const o = h.init(n, t);
                    if (o.constructed._top.id && document.getElementById(o.constructed._top.id)) throw Error('[KEDITOR.create.fail] The ID of the keditor you are trying to create already exists (ID:"' + o.constructed._top.id + '")');
                    return n.style.display = "none", o.constructed._top.style.display = "block", "object" == typeof n.nextElementSibling ? n.parentNode.insertBefore(o.constructed._top, n.nextElementSibling) : n.parentNode.appendChild(o.constructed._top),
                        function(e, t, i, n, o) {
                            const l = e.element.originElement.ownerDocument || document,
                                a = l.defaultView || window,
                                r = u,
                                c = {
                                    _d: l,
                                    _w: a,
                                    context: e,
                                    plugins: i || {},
                                    util: r,
                                    initPlugins: {},
                                    lang: n,
                                    submenu: null,
                                    _resizingName: "",
                                    _submenuName: "",
                                    _bindedSubmenuOff: null,
                                    submenuActiveButton: null,
                                    controllerArray: [],
                                    codeViewDisabledButtons: null,
                                    history: null,
                                    _bindControllersOff: null,
                                    _isInline: null,
                                    _isBalloon: null,
                                    _inlineToolbarAttr: {
                                        width: 0,
                                        height: 0,
                                        isShow: !1
                                    },
                                    _notHideToolbar: !1,
                                    _sticky: !1,
                                    _imageUpload: function(e, t, i, n, o) {
                                        "function" == typeof m.onImageUpload && m.onImageUpload(e, 1 * t, i, n, o)
                                    },
                                    _imageUploadError: function(e, t) {
                                        return "function" != typeof m.onImageUploadError || m.onImageUploadError(e, t)
                                    },
                                    commandMap: null,
                                    _variable: {
                                        wysiwygActive: !0,
                                        isFullScreen: !1,
                                        innerHeight_fullScreen: 0,
                                        resizeClientY: 0,
                                        tabSize: 4,
                                        minResizingSize: 65,
                                        currentNodes: [],
                                        _range: null,
                                        _selectionNode: null,
                                        _originCssText: e.element.topArea.style.cssText,
                                        _bodyOverflow: "",
                                        _editorAreaOriginCssText: "",
                                        _wysiwygOriginCssText: "",
                                        _codeOriginCssText: "",
                                        _fullScreenSticky: !1,
                                        _imagesInfo: [],
                                        _imageIndex: 0,
                                        _videosCnt: 0
                                    },
                                    callPlugin: function(e, i) {
                                        if (!this.plugins[e]) throw Error('[KEDITOR.core.callPlugin.fail] The called plugin does not exist or is in an invalid format. (pluginName:"' + e + '")');
                                        this.initPlugins[e] || (this.plugins[e].add(this, t[e]), this.initPlugins[e] = !0), "function" == typeof i && i()
                                    },
                                    addModule: function(e) {
                                        for (let t, i = 0, n = e.length; i < n; i++) t = e[i].name, this.plugins[t] || (this.plugins[t] = e[i], "function" == typeof this.plugins[t].add && this.plugins[t].add(this))
                                    },
                                    submenuOn: function(e) {
                                        this._bindedSubmenuOff && this._bindedSubmenuOff();
                                        const t = this._submenuName = e.getAttribute("data-command");
                                        this.submenu = e.nextElementSibling, this.submenu.style.display = "block", r.addClass(e, "on"), this.submenuActiveButton = e;
                                        const i = this.context.element.toolbar.offsetWidth - (e.parentElement.offsetLeft + this.submenu.offsetWidth);
                                        this.submenu.style.left = i < 0 ? i + "px" : "1px", this._bindedSubmenuOff = this.submenuOff.bind(this), this.addDocEvent("mousedown", this._bindedSubmenuOff, !1), this.plugins[t].on && this.plugins[t].on.call(this)
                                    },
                                    submenuOff: function() {
                                        this.removeDocEvent("mousedown", this._bindedSubmenuOff), this._bindedSubmenuOff = null, this.submenu && (this._submenuName = "", this.submenu.style.display = "none", this.submenu = null, r.removeClass(this.submenuActiveButton, "on"), this.submenuActiveButton = null, this._notHideToolbar = !1), this.focus()
                                    },
                                    controllersOn: function() {
                                        this._bindControllersOff && this._bindControllersOff();
                                        for (let e = 0; e < arguments.length; e++) arguments[e].style && (arguments[e].style.display = "block"), this.controllerArray[e] = arguments[e];
                                        this._bindControllersOff = this.controllersOff.bind(this), this.addDocEvent("mousedown", this._bindControllersOff, !1), this._resizingName || this.addDocEvent("keydown", this._bindControllersOff, !1)
                                    },
                                    controllersOff: function() {
                                        if (this._resizingName = "", !this._bindControllersOff) return;
                                        this.removeDocEvent("mousedown", this._bindControllersOff), this.removeDocEvent("keydown", this._bindControllersOff), this._bindControllersOff = null;
                                        const e = this.controllerArray.length;
                                        if (e > 0) {
                                            for (let t = 0; t < e; t++) "function" == typeof this.controllerArray[t] ? this.controllerArray[t]() : this.controllerArray[t].style.display = "none";
                                            this.controllerArray = []
                                        }
                                    },
                                    execCommand: function(e, t, i) {
                                        this._wd.execCommand(e, t, "formatBlock" === e ? "<" + i + ">" : i), this.history.push()
                                    },
                                    focus: function() {
                                        if ("none" === e.element.wysiwygFrame.style.display) return;
                                        const t = r.getParentElement(this.getSelectionNode(), "figcaption");
                                        t ? t.focus() : e.element.wysiwyg.focus(), this._editorRange(), d._findButtonEffectTag()
                                    },
                                    setRange: function(e, t, i, n) {
                                        if (!e || !i) return;
                                        t > e.textContent.length && (t = e.textContent.length), n > i.textContent.length && (n = i.textContent.length);
                                        const o = this._wd.createRange();
                                        o.setStart(e, t), o.setEnd(i, n);
                                        const l = this.getSelection();
                                        l.removeAllRanges && l.removeAllRanges(), l.addRange(o), this._editorRange()
                                    },
                                    getRange: function() {
                                        return this._variable._range || this._createDefaultRange()
                                    },
                                    getSelection: function() {
                                        return this._ww.getSelection()
                                    },
                                    getSelectionNode: function() {
                                        return this._variable._selectionNode && !r.isWysiwygDiv(this._variable._selectionNode) || this._editorRange(), this._variable._selectionNode || e.element.wysiwyg.firstChild
                                    },
                                    _editorRange: function() {
                                        const e = this.getSelection();
                                        let t = null,
                                            i = null;
                                        t = e.rangeCount > 0 ? e.getRangeAt(0) : this._createDefaultRange(), this._variable._range = t, i = t.collapsed ? t.commonAncestorContainer : e.extentNode || e.anchorNode, this._variable._selectionNode = i
                                    },
                                    _createDefaultRange: function() {
                                        const t = this._wd.createRange();
                                        return e.element.wysiwyg.firstChild || this.execCommand("formatBlock", !1, "P"), t.setStart(e.element.wysiwyg.firstChild, 0), t.setEnd(e.element.wysiwyg.firstChild, 0), t
                                    },
                                    getSelectedElements: function(t) {
                                        let i = this.getRange();
                                        if (r.isWysiwygDiv(i.startContainer)) {
                                            const t = e.element.wysiwyg.children;
                                            if (0 === t.length) return null;
                                            this.setRange(t[0], 0, t[t.length - 1], t[t.length - 1].textContent.trim().length), i = this.getRange()
                                        }
                                        const n = i.startContainer,
                                            o = i.endContainer,
                                            l = i.commonAncestorContainer,
                                            s = r.getListChildren(l, (function(e) {
                                                return t ? t(e) : r.isFormatElement(e)
                                            }));
                                        if (r.isWysiwygDiv(l) || r.isRangeFormatElement(l) || s.unshift(r.getFormatElement(l)), n === o || 1 === s.length) return s;
                                        let a = r.getFormatElement(n),
                                            c = r.getFormatElement(o),
                                            d = null,
                                            u = null;
                                        const h = function(e) {
                                                return !r.isTable(e) || /^TABLE$/i.test(e.nodeName)
                                            },
                                            g = r.getRangeFormatElement(a, h),
                                            p = r.getRangeFormatElement(c, h),
                                            m = g === p;
                                        for (let e, t = 0, i = s.length; t < i; t++)
                                            if (a === (e = s[t]) || !m && e === g) d = t;
                                            else if (c === e || !m && e === p) {
                                            u = t;
                                            break
                                        }
                                        return null === d && (d = 0), null === u && (u = s.length - 1), s.slice(d, u + 1)
                                    },
                                    getSelectedElementsAndComponents: function() {
                                        const e = this.getRange().commonAncestorContainer,
                                            t = r.getParentElement(e, r.isComponent);
                                        return r.isTable(e) ? this.getSelectedElements() : this.getSelectedElements(function(e) {
                                            const i = this.getParentElement(e, this.isComponent);
                                            return this.isFormatElement(e) && (!i || i === t) || this.isComponent(e) && !this.getFormatElement(e)
                                        }.bind(r))
                                    },
                                    isEdgePoint: function(e, t) {
                                        return 0 === t || t === e.nodeValue.length
                                    },
                                    showLoading: function() {
                                        e.element.loading.style.display = "block"
                                    },
                                    closeLoading: function() {
                                        e.element.loading.style.display = "none"
                                    },
                                    appendFormatTag: function(e, t) {
                                        const i = e,
                                            n = r.getFormatElement(this.getSelectionNode()),
                                            o = t || (r.isFormatElement(n) ? n.nodeName : "P"),
                                            l = r.createElement(o);
                                        return l.innerHTML = "<br>", r.isCell(i) ? i.insertBefore(l, e.nextElementSibling) : i.parentNode.insertBefore(l, i.nextElementSibling), l
                                    },
                                    insertComponent: function(e) {
                                        let t = null;
                                        const i = this.getSelectionNode(),
                                            n = r.getFormatElement(i);
                                        if (r.isListCell(n))
                                            if (/^HR$/i.test(e.nodeName)) {
                                                const t = r.createElement("LI"),
                                                    i = r.createTextNode(r.zeroWidthSpace);
                                                t.appendChild(e), t.appendChild(i), n.parentNode.insertBefore(t, n.nextElementSibling), this.setRange(i, 1, i, 1)
                                            } else this.insertNode(e, i === n ? null : i), t = r.createElement("LI"), n.parentNode.insertBefore(t, n.nextElementSibling);
                                        else this.insertNode(e, n), t = this.appendFormatTag(e);
                                        return this.history.push(), t
                                    },
                                    insertNode: function(e, t) {
                                        const i = this.getRange();
                                        let n = null;
                                        if (t) n = t.parentNode, t = t.nextElementSibling;
                                        else {
                                            const e = i.startContainer,
                                                o = i.startOffset,
                                                l = i.endContainer,
                                                s = i.endOffset,
                                                a = i.commonAncestorContainer;
                                            if (n = e, 3 === e.nodeType && (n = e.parentNode), i.collapsed) 3 === a.nodeType ? t = a.splitText(s) : (null !== n.lastChild && r.isBreak(n.lastChild) && n.removeChild(n.lastChild), t = null);
                                            else {
                                                if (e === l) {
                                                    t = this.isEdgePoint(l, s) ? l.nextSibling : l.splitText(s);
                                                    let i = e;
                                                    this.isEdgePoint(e, o) || (i = e.splitText(o)), n.removeChild(i)
                                                } else
                                                    for (this.removeNode(), n = a, t = l; t.parentNode !== a;) t = t.parentNode
                                            }
                                        }
                                        try {
                                            n.insertBefore(e, t)
                                        } catch (t) {
                                            n.appendChild(e)
                                        } finally {
                                            this.history.push()
                                        }
                                    },
                                    removeNode: function() {
                                        const e = this.getRange();
                                        if (e.deleteContents) return void e.deleteContents();
                                        const t = e.startContainer,
                                            i = e.startOffset,
                                            n = e.endContainer,
                                            o = e.endOffset,
                                            l = e.commonAncestorContainer;
                                        let s = null,
                                            a = null;
                                        const c = r.getListChildNodes(l);
                                        let d = r.getArrayIndex(c, t),
                                            u = r.getArrayIndex(c, n);
                                        for (let e = d + 1, n = t; e >= 0; e--) c[e] === n.parentNode && c[e].firstChild === n && 0 === i && (d = e, n = n.parentNode);
                                        for (let e = u - 1, t = n; e > d; e--) c[e] === t.parentNode && 1 === c[e].nodeType && (c.splice(e, 1), t = t.parentNode, --u);
                                        for (let e = d; e <= u; e++) {
                                            const l = c[e];
                                            0 === l.length || 3 === l.nodeType && void 0 === l.data ? r.removeItem(l) : l !== t ? l !== n ? (r.removeItem(l), this.history.push()) : (a = 1 === n.nodeType ? r.createTextNode(n.textContent) : r.createTextNode(n.substringData(o, n.length - o))).length > 0 ? n.data = a.data : r.removeItem(n) : (s = 1 === t.nodeType ? r.createTextNode(t.textContent) : r.createTextNode(t.substringData(0, i))).length > 0 ? t.data = s.data : r.removeItem(t)
                                        }
                                    },
                                    applyRangeFormatElement: function(e) {
                                        const t = this.getSelectedElementsAndComponents();
                                        if (!t || 0 === t.length) return;
                                        let i, n, o, l = t[t.length - 1];
                                        i = r.isRangeFormatElement(l) || r.isFormatElement(l) ? l : r.getRangeFormatElement(l) || r.getFormatElement(l), r.isCell(i) ? (n = null, o = i) : (n = i.nextSibling, o = i.parentNode);
                                        let s = r.getElementDepth(i),
                                            a = null;
                                        const c = [],
                                            d = function(e, t, i) {
                                                let n = null;
                                                return e === t || r.isTable(t) || (n = r.removeItemAllParents(t)), n ? n.ec : i
                                            };
                                        for (let i, l, u, h, g = 0, p = t.length; g < p; g++)
                                            if (i = t[g], l = i.parentNode, u = r.getElementDepth(i), r.isList(l)) {
                                                if (null === a && (a = r.createElement(l.nodeName)), a.innerHTML += i.outerHTML, c.push(i), g === p - 1 || !this.util.getParentElement(t[g + 1], (function(e) {
                                                        return e === l
                                                    }))) {
                                                    const t = this.detachRangeFormatElement(l, c, null, !0, !0);
                                                    s >= u ? (s = u, (n = d(o = t.cc, l, t.ec)) && (o = n.parentNode)) : o === t.cc && (n = t.ec), o !== t.cc && void 0 !== (h = d(o, t.cc)) && (n = h), e.appendChild(a), a = null
                                                }
                                            } else s >= u && (s = u, o = l, n = i.nextSibling), e.appendChild(i), o !== l && void 0 !== (h = d(o, l)) && (n = h);
                                        o.insertBefore(e, n), d(e, n), this.history.push();
                                        const u = this.util.getEdgeChildNodes(e.firstElementChild, e.lastElementChild);
                                        t.length > 1 ? this.setRange(u.sc, 0, u.ec, u.ec.textContent.length) : this.setRange(u.ec, u.ec.textContent.length, u.ec, u.ec.textContent.length)
                                    },
                                    detachRangeFormatElement: function(e, t, i, n, o) {
                                        const l = this.getRange(),
                                            s = l.startOffset,
                                            a = l.endOffset,
                                            c = e.childNodes,
                                            u = e.parentNode;
                                        let h = null,
                                            g = null,
                                            p = e.cloneNode(!1);
                                        const m = r.isList(i);
                                        let f = !1;

                                        function _(e, t, i) {
                                            if (r.onlyZeroWidthSpace(t) && (t.innerHTML = r.zeroWidthSpace), 3 === t.nodeType) return e.insertBefore(t, i), t;
                                            const n = t.childNodes;
                                            let o = t.cloneNode(!1),
                                                l = null,
                                                s = null;
                                            for (; n[0];) s = n[0], r.isIgnoreNodeChange(s) && !r.isListCell(o) ? (o.childNodes.length > 0 && (l || (l = o), e.insertBefore(o, i), o = t.cloneNode(!1)), e.insertBefore(s, i), l || (l = s)) : o.appendChild(s);
                                            return o.childNodes.length > 0 && (e.insertBefore(o, i), l || (l = o)), l
                                        }
                                        for (let o, l = 0, s = c.length; l < s; l++)
                                            if (o = c[l], n && 0 === l && (h = t && t.length !== s && t[0] !== o ? p : e.previousSibling), t && -1 === t.indexOf(o)) p || (p = e.cloneNode(!1)), o = o.cloneNode(!0), p.appendChild(o);
                                            else {
                                                if (p && p.children.length > 0 && (u.insertBefore(p, e), p = null), !m && r.isListCell(o)) {
                                                    const t = o.innerHTML;
                                                    (o = r.isCell(e.parentNode) ? r.createElement("DIV") : r.createElement("P")).innerHTML = t
                                                } else o = o.cloneNode(!0);
                                                n || (i ? (f || (u.insertBefore(i, e), f = !0), o = _(i, o, null)) : o = _(u, o, e), t ? (g = o, h || (h = o)) : h || (h = g = o))
                                            } const b = e.parentNode,
                                            y = e.nextSibling;
                                        p && p.children.length > 0 && b.insertBefore(p, y), r.removeItem(e);
                                        const v = n ? {
                                            cc: b,
                                            sc: h,
                                            ec: h && h.parentNode ? h.nextSibling : p && p.children.length > 0 ? p : y || null
                                        } : this.util.getEdgeChildNodes(h, g);
                                        if (o) return v;
                                        this.history.push(), !n && v && (t ? this.setRange(v.sc, s, v.ec, a) : this.setRange(v.sc, 0, v.sc, 0)), d._findButtonEffectTag()
                                    },
                                    nodeChange: function(t, i, n) {
                                        const o = this.getRange();
                                        i = !!(i && i.length > 0) && i, n = !!(n && n.length > 0) && n;
                                        const l = !t,
                                            s = l && !n && !i;
                                        let c, d, u, h = o.startContainer,
                                            g = o.startOffset,
                                            p = o.endContainer,
                                            m = o.endOffset;
                                        if (s && o.collapsed && r.isFormatElement(h.parentNode) && r.isFormatElement(p.parentNode)) return;
                                        if (l && (t = this.util.createElement("DIV")), !s && h === p) {
                                            let e = h;
                                            if (s) {
                                                if (r.getFormatElement(e) === e.parentNode) return
                                            } else if (i.length > 0) {
                                                let n = 0;
                                                for (let o = 0; o < i.length; o++)
                                                    for (; !r.isFormatElement(e) && !r.isWysiwygDiv(e);) 1 === e.nodeType && (l ? e.style[i[o]] : e.style[i[o]] === t.style[i[o]]) && n++, e = e.parentNode;
                                                if (!l && n >= i.length) return;
                                                if (l && 0 === n) return
                                            }
                                        }
                                        if (c = r.isWysiwygDiv(h) ? e.element.wysiwyg.firstChild : h, d = g, r.isBreak(c) || 1 === c.nodeType && c.childNodes.length > 0) {
                                            const e = r.isBreak(c);
                                            if (!e) {
                                                for (; c && !r.isBreak(c) && 1 === c.nodeType;) c = c.childNodes[d] || c.nextElementSibling || c.nextSibling, d = 0;
                                                let e = r.getFormatElement(c);
                                                e === r.getRangeFormatElement(e) && (e = r.createElement(r.isCell(c) ? "DIV" : "P"), c.parentNode.insertBefore(e, c), e.appendChild(c))
                                            }
                                            if (r.isBreak(c)) {
                                                const t = r.createTextNode(r.zeroWidthSpace);
                                                c.parentNode.insertBefore(t, c), c = t, e && (h === p && (p = c, m = 1), r.removeItem(h))
                                            }
                                        }
                                        if (h = c, g = d, c = r.isWysiwygDiv(p) ? e.element.wysiwyg.lastChild : p, d = m, r.isBreak(c) || 1 === c.nodeType && c.childNodes.length > 0) {
                                            const e = r.isBreak(c);
                                            if (!e) {
                                                for (; c && !r.isBreak(c) && 1 === c.nodeType;) c = (u = c.childNodes)[d > 0 ? d - 1 : d] || !/FIGURE/i.test(u[0].nodeName) ? u[0] : c.previousElementSibling || c.previousSibling || h, d = d > 0 ? c.textContent.length : d;
                                                let e = r.getFormatElement(c);
                                                e === r.getRangeFormatElement(e) && (e = r.createElement(r.isCell(e) ? "DIV" : "P"), c.parentNode.insertBefore(e, c), e.appendChild(c))
                                            }
                                            if (r.isBreak(c)) {
                                                const t = r.createTextNode(r.zeroWidthSpace);
                                                c.parentNode.insertBefore(t, c), c = t, d = 0, e && r.removeItem(p)
                                            }
                                        }
                                        p = c, m = d;
                                        const f = t.nodeName;
                                        this.setRange(h, g, p, m);
                                        let _, b, y, v = {},
                                            k = {};
                                        if (i) {
                                            b = "(?:;|^|\\s)(?:" + i[0];
                                            for (let e = 1; e < i.length; e++) b += "|" + i[e];
                                            b += ")\\s*:[^;]*\\s*(?:;|$)", b = new a.RegExp(b, "ig")
                                        }
                                        if (n) {
                                            y = "^(?:" + n[0];
                                            for (let e = 1; e < n.length; e++) y += "|" + n[e];
                                            y += ")$", y = new a.RegExp(y, "i")
                                        }
                                        const C = function(e) {
                                                if (3 === e.nodeType || r.isBreak(e)) return !0;
                                                if (s) return !1;
                                                const t = e.style.cssText;
                                                let i = "";
                                                if (b && t.length > 0 && (i = t.replace(b, "").trim()), l) {
                                                    if (b && y && !i && y.test(e.nodeName)) return !1;
                                                    if (b && !i && t) return !1;
                                                    if (y && y.test(e.nodeName)) return !1
                                                }
                                                return !(!i && e.nodeName === f) && (b && t.length > 0 && (e.style.cssText = i), !0)
                                            },
                                            x = this.getSelectedElements();
                                        r.getFormatElement(h) || (h = r.getChildElement(x[0], (function(e) {
                                            return 3 === e.nodeType
                                        })), g = 0), r.getFormatElement(p) || (m = (p = r.getChildElement(x[x.length - 1], (function(e) {
                                            return 3 === e.nodeType
                                        }))).textContent.length);
                                        const w = r.getFormatElement(h) === r.getFormatElement(p),
                                            E = x.length - (w ? 0 : 1);
                                        if (_ = t.cloneNode(!1), w) {
                                            const e = this._nodeChange_oneLine(x[0], _, C, h, g, p, m, s, l, o.collapsed);
                                            v.container = e.startContainer, v.offset = e.startOffset, k.container = e.endContainer, k.offset = e.endOffset
                                        } else v = this._nodeChange_startLine(x[0], _, C, h, g, s, l);
                                        for (let e = 1; e < E; e++) _ = t.cloneNode(!1), this._nodeChange_middleLine(x[e], _, C, s, l);
                                        E > 0 && !w ? (_ = t.cloneNode(!1), k = this._nodeChange_endLine(x[E], _, C, p, m, s, l)) : w || (k = v), this.setRange(v.container, v.offset, k.container, k.offset), this.history.push()
                                    },
                                    _stripRemoveNode: function(e, t) {
                                        if (!t || 3 === t.nodeType) return;
                                        const i = t.childNodes;
                                        for (; i[0];) e.insertBefore(i[0], t);
                                        e.removeChild(t)
                                    },
                                    _nodeChange_oneLine: function(e, t, i, n, o, l, s, c, d, u) {
                                        const h = e,
                                            g = t,
                                            p = [t],
                                            m = e.cloneNode(!1),
                                            f = n === l;
                                        let _, b, y, v, k = n,
                                            C = o,
                                            x = l,
                                            w = s,
                                            E = !1,
                                            T = !1;

                                        function S(e) {
                                            const t = new a.RegExp("(?:;|^|\\s)(?:" + v + "null)\\s*:[^;]*\\s*(?:;|$)", "ig");
                                            let i = "";
                                            return t && e.style.cssText.length > 0 && (i = t.test(e.style.cssText)), !i
                                        }
                                        if (function e(n, o) {
                                                const l = n.childNodes;
                                                for (let n = 0, s = l.length; n < s; n++) {
                                                    let s = l[n];
                                                    if (!s) continue;
                                                    let a, d = o;
                                                    if (!E && s === k) {
                                                        const e = r.createTextNode(1 === k.nodeType ? "" : k.substringData(0, C)),
                                                            n = r.createTextNode(1 === k.nodeType ? "" : k.substringData(C, f && w >= C ? w - C : k.data.length - C));
                                                        for (e.data.length > 0 && o.appendChild(e), b = s, _ = [], v = ""; b !== m && b !== h && null !== b;) i(b) && 1 === b.nodeType && S(b) && (_.push(b.cloneNode(!1)), v += b.style.cssText.substr(0, b.style.cssText.indexOf(":")) + "|"), b = b.parentNode;
                                                        const l = _.pop() || n;
                                                        for (y = b = l; _.length > 0;) b = _.pop(), y.appendChild(b), y = b;
                                                        if (t.appendChild(l), m.appendChild(t), k = n, C = 0, E = !0, b !== n && b.appendChild(k), !f) continue
                                                    }
                                                    if (T || s !== x) {
                                                        if (E) {
                                                            if (1 === s.nodeType && !r.isBreak(s)) {
                                                                r.isIgnoreNodeChange(s) ? (t = t.cloneNode(!1), m.appendChild(s), m.appendChild(t), p.push(t), n--) : e(s, s);
                                                                continue
                                                            }
                                                            for (b = s, _ = [], v = ""; null !== b.parentNode && b !== h && b !== t;) 1 === b.nodeType && !r.isBreak(s) && (T || i(b)) && S(b) && (_.push(b.cloneNode(!1)), v += b.style.cssText.substr(0, b.style.cssText.indexOf(":")) + "|"), b = b.parentNode;
                                                            const l = _.pop() || s;
                                                            for (y = b = l; _.length > 0;) b = _.pop(), y.appendChild(b), y = b;
                                                            l === s ? o = T ? m : t : T ? (m.appendChild(l), o = b) : (t.appendChild(l), o = b)
                                                        }
                                                        a = s.cloneNode(!1), o.appendChild(a), 1 !== s.nodeType || r.isBreak(s) || (d = a), e(s, d)
                                                    } else {
                                                        const e = r.createTextNode(1 === x.nodeType ? "" : x.substringData(w, x.length - w)),
                                                            n = r.createTextNode(f || 1 === x.nodeType ? "" : x.substringData(0, w));
                                                        if (e.data.length > 0) {
                                                            for (b = s, v = "", _ = []; b !== m && b !== h && null !== b;) 1 === b.nodeType && S(b) && (_.push(b.cloneNode(!1)), v += b.style.cssText.substr(0, b.style.cssText.indexOf(":")) + "|"), b = b.parentNode;
                                                            for (a = y = b = _.pop() || e; _.length > 0;) b = _.pop(), y.appendChild(b), y = b;
                                                            m.appendChild(a), b.textContent = e.data
                                                        }
                                                        for (b = s, _ = [], v = ""; b !== m && b !== h && null !== b;) i(b) && 1 === b.nodeType && S(b) && (_.push(b.cloneNode(!1)), v += b.style.cssText.substr(0, b.style.cssText.indexOf(":")) + "|"), b = b.parentNode;
                                                        const o = _.pop() || n;
                                                        for (y = b = o; _.length > 0;) b = _.pop(), y.appendChild(b), y = b;
                                                        t.appendChild(o), x = n, w = n.data.length, T = !0, !c && u && (t = n, n.textContent = r.zeroWidthSpace), b !== n && b.appendChild(x)
                                                    }
                                                }
                                            }(e, m), c = c && d)
                                            for (let e = 0; e < p.length; e++) {
                                                let t = p[e],
                                                    i = r.createTextNode(u ? r.zeroWidthSpace : t.textContent);
                                                m.insertBefore(i, t), m.removeChild(t), 0 === e && (k = x = i)
                                            } else {
                                                if (d)
                                                    for (let e = 0; e < p.length; e++) {
                                                        let t = p[e];
                                                        if (u)
                                                            for (; t !== g;) t = t.parentNode;
                                                        this._stripRemoveNode(m, t)
                                                    }
                                                u && (k = x = t)
                                            }
                                        const N = r.onlyZeroWidthSpace(t.textContent);
                                        N && (t.textContent = " "), r.removeEmptyNode(m), N && (t.textContent = r.zeroWidthSpace);
                                        const z = c || !x.textContent;
                                        return e.parentNode.insertBefore(m, e), r.removeItem(e), u && (C = k.textContent.length, w = x.textContent.length), 0 === x.textContent.length && (r.removeItem(x), x = k), {
                                            startContainer: k,
                                            startOffset: C,
                                            endContainer: x,
                                            endOffset: z ? x.textContent.length : w
                                        }
                                    },
                                    _nodeChange_middleLine: function(e, t, i, n, o) {
                                        const l = e.cloneNode(!1),
                                            s = [t];
                                        let a = !0;
                                        if (function e(n, o) {
                                                const c = n.childNodes;
                                                for (let n = 0, d = c.length; n < d; n++) {
                                                    let d = c[n];
                                                    if (!d) continue;
                                                    let u = o;
                                                    if (r.isIgnoreNodeChange(d)) l.appendChild(t), t = t.cloneNode(!1), l.appendChild(d), l.appendChild(t), s.push(t), o = t, n--;
                                                    else {
                                                        if (i(d)) {
                                                            a = !1;
                                                            let e = d.cloneNode(!1);
                                                            o.appendChild(e), 1 !== d.nodeType || r.isBreak(d) || (u = e)
                                                        }
                                                        e(d, u)
                                                    }
                                                }
                                            }(e.cloneNode(!0), t), !a) {
                                            if (l.appendChild(t), n && o)
                                                for (let e = 0; e < s.length; e++) {
                                                    let t = s[e],
                                                        i = r.createTextNode(t.textContent);
                                                    l.insertBefore(i, t), l.removeChild(t)
                                                } else if (o)
                                                    for (let e = 0; e < s.length; e++) this._stripRemoveNode(l, s[e]);
                                            r.removeEmptyNode(l), e.parentNode.insertBefore(l, e), r.removeItem(e)
                                        }
                                    },
                                    _nodeChange_startLine: function(e, t, i, n, o, l, s) {
                                        const a = e,
                                            c = [t],
                                            d = e.cloneNode(!1);
                                        let u, h, g, p = n,
                                            m = o,
                                            f = !1;
                                        if (function e(n, o) {
                                                const l = n.childNodes;
                                                for (let n = 0, s = l.length; n < s; n++) {
                                                    const s = l[n];
                                                    if (!s) continue;
                                                    let _ = o;
                                                    if (f && !r.isBreak(s)) {
                                                        if (1 === s.nodeType) {
                                                            r.isIgnoreNodeChange(s) ? (t = t.cloneNode(!1), d.appendChild(s), d.appendChild(t), c.push(t), n--) : e(s, s);
                                                            continue
                                                        }
                                                        for (h = s, u = []; null !== h.parentNode && h !== a && h !== t;) 1 === h.nodeType && i(h) && u.push(h.cloneNode(!1)), h = h.parentNode;
                                                        if (u.length > 0) {
                                                            const e = u.pop();
                                                            for (g = h = e; u.length > 0;) h = u.pop(), g.appendChild(h), g = h;
                                                            t.appendChild(e), o = h
                                                        } else o = t
                                                    }
                                                    if (f || s !== p) {
                                                        if (!f || i(s)) {
                                                            const e = s.cloneNode(!1);
                                                            o.appendChild(e), 1 !== s.nodeType || r.isBreak(s) || (_ = e)
                                                        }
                                                        e(s, _)
                                                    } else {
                                                        const e = r.createTextNode(1 === p.nodeType ? "" : p.substringData(0, m)),
                                                            n = r.createTextNode(1 === p.nodeType ? "" : p.substringData(m, p.length - m));
                                                        for (e.data.length > 0 && o.appendChild(e), h = o, u = []; h !== d && null !== h;) 1 === h.nodeType && i(h) && u.push(h.cloneNode(!1)), h = h.parentNode;
                                                        const l = u.pop() || o;
                                                        for (g = h = l; u.length > 0;) h = u.pop(), g.appendChild(h), g = h;
                                                        l !== o ? (t.appendChild(l), o = h) : o = t, r.isBreak(s) && t.appendChild(s.cloneNode(!1)), d.appendChild(t), p = n, m = 0, f = !0, o.appendChild(p)
                                                    }
                                                }
                                            }(e, d), l = l && s)
                                            for (let e = 0; e < c.length; e++) {
                                                let t = c[e],
                                                    i = r.createTextNode(t.textContent);
                                                d.insertBefore(i, t), d.removeChild(t), 0 === e && (p = i)
                                            } else if (s)
                                                for (let e = 0; e < c.length; e++) this._stripRemoveNode(d, c[e]);
                                        return l || 0 !== d.children.length ? (r.removeEmptyNode(d), r.onlyZeroWidthSpace(d.textContent) && (p = d.firstChild, m = 0), e.parentNode.insertBefore(d, e), r.removeItem(e)) : e.childNodes ? p = e.childNodes[0] : (p = r.createTextNode(r.zeroWidthSpace), e.appendChild(p)), {
                                            container: p,
                                            offset: m
                                        }
                                    },
                                    _nodeChange_endLine: function(e, t, i, n, o, l, s) {
                                        const a = e,
                                            c = [t],
                                            d = e.cloneNode(!1);
                                        let u, h, g, p = n,
                                            m = o,
                                            f = !1;
                                        if (function e(n, o) {
                                                const l = n.childNodes;
                                                for (let n = l.length - 1; 0 <= n; n--) {
                                                    const s = l[n];
                                                    if (!s) continue;
                                                    let _ = o;
                                                    if (f && !r.isBreak(s)) {
                                                        if (1 === s.nodeType) {
                                                            r.isIgnoreNodeChange(s) ? (t = t.cloneNode(!1), d.appendChild(s), d.appendChild(t), c.push(t), n--) : e(s, s);
                                                            continue
                                                        }
                                                        for (h = s, u = []; null !== h.parentNode && h !== a && h !== t;) i(h) && 1 === h.nodeType && u.push(h.cloneNode(!1)), h = h.parentNode;
                                                        if (u.length > 0) {
                                                            const e = u.pop();
                                                            for (g = h = e; u.length > 0;) h = u.pop(), g.appendChild(h), g = h;
                                                            t.insertBefore(e, t.firstChild), o = h
                                                        } else o = t
                                                    }
                                                    if (f || s !== p) {
                                                        if (!f || i(s)) {
                                                            const e = s.cloneNode(!1);
                                                            o.insertBefore(e, o.firstChild), 1 !== s.nodeType || r.isBreak(s) || (_ = e)
                                                        }
                                                        e(s, _)
                                                    } else {
                                                        const e = r.createTextNode(1 === p.nodeType ? "" : p.substringData(m, p.length - m)),
                                                            n = r.createTextNode(1 === p.nodeType ? "" : p.substringData(0, m));
                                                        for (e.data.length > 0 && o.insertBefore(e, o.firstChild), h = o, u = []; h !== d && null !== h;) i(h) && 1 === h.nodeType && u.push(h.cloneNode(!1)), h = h.parentNode;
                                                        const l = u.pop() || o;
                                                        for (g = h = l; u.length > 0;) h = u.pop(), g.appendChild(h), g = h;
                                                        l !== o ? (t.insertBefore(l, t.firstChild), o = h) : o = t, r.isBreak(s) && t.appendChild(s.cloneNode(!1)), d.insertBefore(t, d.firstChild), p = n, m = n.data.length, f = !0, o.insertBefore(p, o.firstChild)
                                                    }
                                                }
                                            }(e, d), l = l && s)
                                            for (let e = 0; e < c.length; e++) {
                                                let t = c[e],
                                                    i = r.createTextNode(t.textContent);
                                                d.insertBefore(i, t), d.removeChild(t), e === c.length - 1 && (p = i, m = i.textContent.length)
                                            } else if (s)
                                                for (let e = 0; e < c.length; e++) this._stripRemoveNode(d, c[e]);
                                        return l || 0 !== d.childNodes.length ? (r.removeEmptyNode(d), r.onlyZeroWidthSpace(d.textContent) ? (p = d.firstChild, m = p.textContent.length) : r.onlyZeroWidthSpace(p) && (p = d, m = 0), e.parentNode.insertBefore(d, e), r.removeItem(e)) : e.childNodes ? p = e.childNodes[0] : (p = r.createTextNode(r.zeroWidthSpace), e.appendChild(p)), {
                                            container: p,
                                            offset: m
                                        }
                                    },
                                    commandHandler: function(t, i) {
                                        switch (i) {
                                            case "selectAll":
                                                const n = e.element.wysiwyg,
                                                    o = r.getChildElement(n.firstChild, (function(e) {
                                                        return 0 === e.childNodes.length || 3 === e.nodeType
                                                    })) || n.firstChild,
                                                    l = r.getChildElement(n.lastChild, (function(e) {
                                                        return 0 === e.childNodes.length || 3 === e.nodeType
                                                    }), !0) || n.lastChild;
                                                this.setRange(o, 0, l, l.textContent.length);
                                                break;
                                            case "codeView":
                                                this.toggleCodeView(), r.toggleClass(t, "active");
                                                break;
                                            case "fullScreen":
                                                this.toggleFullScreen(t), r.toggleClass(t, "active");
                                                break;
                                            case "indent":
                                            case "outdent":
                                                this.indent(i);
                                                break;
                                            case "undo":
                                                this.history.undo();
                                                break;
                                            case "redo":
                                                this.history.redo();
                                                break;
                                            case "removeFormat":
                                                this.removeFormat(), this.focus();
                                                break;
                                            case "print":
                                                this.print();
                                                break;
                                            case "preview":
                                                this.preview();
                                                break;
                                            case "showBlocks":
                                                this.toggleDisplayBlocks(), r.toggleClass(t, "active");
                                                break;
                                            case "save":
                                                if ("function" == typeof e.option.callBackSave) e.option.callBackSave(this.getContents());
                                                else {
                                                    if ("function" != typeof m.save) throw Error("[KEDITOR.core.commandHandler.fail] Please register call back function in creation option. (callBackSave : Function)");
                                                    m.save()
                                                }
                                                e.tool.save && e.tool.save.setAttribute("disabled", !0);
                                                break;
                                            default:
                                                const s = r.hasClass(this.commandMap[i], "active");
                                                "SUB" === i && r.hasClass(this.commandMap.SUP, "active") ? this.nodeChange(null, null, ["SUP"]) : "SUP" === i && r.hasClass(this.commandMap.SUB, "active") && this.nodeChange(null, null, ["SUB"]), this.nodeChange(s ? null : this.util.createElement(i), null, [i]), this.focus()
                                        }
                                    },
                                    removeFormat: function() {
                                        this.nodeChange()
                                    },
                                    indent: function(e) {
                                        const t = this.getSelectedElements();
                                        let i, n;
                                        for (let o = 0, l = t.length; o < l; o++) i = t[o], n = /\d+/.test(i.style.marginLeft) ? 1 * i.style.marginLeft.match(/\d+/)[0] : 0, "indent" === e ? n += 25 : n -= 25, i.style.marginLeft = (n < 0 ? 0 : n) + "px";
                                        d._findButtonEffectTag(), this.history.push()
                                    },
                                    toggleDisplayBlocks: function() {
                                        r.toggleClass(e.element.wysiwyg, "ke-show-block")
                                    },
                                    toggleCodeView: function() {
                                        const t = this._variable.wysiwygActive,
                                            i = this.codeViewDisabledButtons;
                                        for (let e = 0, n = i.length; e < n; e++) i[e].disabled = t;
                                        if (this.controllersOff(), t) {
                                            let t = "";
                                            if (e.option.fullPage) {
                                                const i = this._wd.head.children;
                                                let n = "";
                                                for (let e = 0, t = i.length; e < t; e++) n += i[e].outerHTML + "\n";
                                                t = "<!DOCTYPE html>\n<html>\n" + this._wd.head.outerHTML.match(/^<head[^>]*>\B/)[0] + "\n" + n + "</head>\n" + this._wd.body.outerHTML.match(/^<body[^>]*>\B/)[0] + "\n" + r.convertHTMLForCodeView(e.element.wysiwyg) + "</body>\n</html>"
                                            } else t = r.convertHTMLForCodeView(e.element.wysiwyg);
                                            e.element.code.style.display = "block", e.element.wysiwygFrame.style.display = "none", this._setCodeView(t), this._variable._codeOriginCssText = this._variable._codeOriginCssText.replace(/(\s?display(\s+)?:(\s+)?)[a-zA-Z]+(?=;)/, "display: block"), this._variable._wysiwygOriginCssText = this._variable._wysiwygOriginCssText.replace(/(\s?display(\s+)?:(\s+)?)[a-zA-Z]+(?=;)/, "display: none"), "auto" !== e.option.height || e.option.codeMirrorEditor || (e.element.code.style.height = e.element.code.scrollHeight > 0 ? e.element.code.scrollHeight + "px" : "auto"), e.option.codeMirrorEditor && e.option.codeMirrorEditor.refresh(), this._variable.wysiwygActive = !1, e.element.code.focus()
                                        } else {
                                            const t = this._getCodeView();
                                            if (e.option.fullPage) {
                                                const e = (new this._w.DOMParser).parseFromString(t, "text/html"),
                                                    i = e.head.children;
                                                for (let t = 0, n = i.length; t < n; t++) /script/i.test(i[t].tagName) && e.head.removeChild(i[t]);
                                                this._wd.head.innerHTML = e.head.innerHTML, this._wd.body.innerHTML = r.convertContentsForEditor(e.body.innerHTML);
                                                const n = e.body.attributes;
                                                for (let e = 0, t = n.length; e < t; e++) "contenteditable" !== n[e].name && this._wd.body.setAttribute(n[e].name, n[e].value)
                                            } else e.element.wysiwyg.innerHTML = t.length > 0 ? r.convertContentsForEditor(t) : "<p><br></p>";
                                            e.element.wysiwygFrame.scrollTop = 0, e.element.code.style.display = "none", e.element.wysiwygFrame.style.display = "block", this._variable._codeOriginCssText = this._variable._codeOriginCssText.replace(/(\s?display(\s+)?:(\s+)?)[a-zA-Z]+(?=;)/, "display: none"), this._variable._wysiwygOriginCssText = this._variable._wysiwygOriginCssText.replace(/(\s?display(\s+)?:(\s+)?)[a-zA-Z]+(?=;)/, "display: block"), "auto" !== e.option.height || e.option.codeMirrorEditor || (e.element.code.style.height = "0px"), this._variable.wysiwygActive = !0, this.focus()
                                        }
                                    },
                                    toggleFullScreen: function(t) {
                                        const i = e.element.topArea,
                                            n = e.element.toolbar,
                                            o = e.element.editorArea,
                                            s = e.element.wysiwyg,
                                            c = e.element.code;
                                        this._variable.isFullScreen ? (this._variable.isFullScreen = !1, s.style.cssText = this._variable._wysiwygOriginCssText, c.style.cssText = this._variable._codeOriginCssText, n.style.cssText = "", o.style.cssText = this._variable._editorAreaOriginCssText, i.style.cssText = this._variable._originCssText, l.body.style.overflow = this._variable._bodyOverflow, e.option.stickyToolbar > -1 && (r.removeClass(n, "ke-toolbar-sticky"), d.onScroll_window()), this._variable._fullScreenSticky && (this._variable._fullScreenSticky = !1, e.element._stickyDummy.style.display = "block", r.addClass(n, "ke-toolbar-sticky")), r.removeClass(t.firstElementChild, "ke-icon-reduction"), r.addClass(t.firstElementChild, "ke-icon-expansion")) : (this._variable.isFullScreen = !0, i.style.position = "fixed", i.style.top = "0", i.style.left = "0", i.style.width = "100%", i.style.height = "100%", i.style.zIndex = "2147483647", "none" !== e.element._stickyDummy.style.display && (this._variable._fullScreenSticky = !0, e.element._stickyDummy.style.display = "none", r.removeClass(n, "ke-toolbar-sticky")), this._variable._bodyOverflow = l.body.style.overflow, l.body.style.overflow = "hidden", this._variable._editorAreaOriginCssText = o.style.cssText, this._variable._wysiwygOriginCssText = s.style.cssText, this._variable._codeOriginCssText = c.style.cssText, o.style.cssText = n.style.cssText = "", s.style.cssText = s.style.cssText.match(/\s?display(\s+)?:(\s+)?[a-zA-Z]+;/)[0], c.style.cssText = c.style.cssText.match(/\s?display(\s+)?:(\s+)?[a-zA-Z]+;/)[0], n.style.width = s.style.height = c.style.height = "100%", n.style.position = "relative", this._variable.innerHeight_fullScreen = a.innerHeight - n.offsetHeight, o.style.height = this._variable.innerHeight_fullScreen + "px", r.removeClass(t.firstElementChild, "ke-icon-expansion"), r.addClass(t.firstElementChild, "ke-icon-reduction"))
                                    },
                                    print: function() {
                                        const e = r.createElement("IFRAME");
                                        e.style.display = "none";
                                        const t = r.createElement("DIV"),
                                            i = r.createElement("STYLE");
                                        i.innerHTML = r.getPageStyle(), t.className = "keditor-editable", t.innerHTML = this.getContents(), l.body.appendChild(e);
                                        let n = e.contentWindow || e.contentDocument;
                                        n.document && (n = n.document), n.head.appendChild(i), n.body.appendChild(t);
                                        try {
                                            if (e.focus(), -1 !== a.navigator.userAgent.indexOf("MSIE") || l.documentMode || a.StyleMedia) try {
                                                e.contentWindow.document.execCommand("print", !1, null)
                                            } catch (t) {
                                                e.contentWindow.print()
                                            } else e.contentWindow.print()
                                        } catch (e) {
                                            throw Error("[KEDITOR.core.print.fail] error: " + e)
                                        } finally {
                                            r.removeItem(e)
                                        }
                                    },
                                    preview: function() {
                                        const t = r.getPageStyle(),
                                            i = this.getContents(),
                                            o = a.open("", "_blank");
                                        o.mimeType = "text/html", e.option.fullPage ? o.document.write("<!DOCTYPE html><html>" + this._wd.head.outerHTML + this._wd.body.outerHTML + "</html>") : o.document.write('<!DOCTYPE html><html><head><meta charset="utf-8" /><meta name="viewport" content="width=device-width, initial-scale=1"><title>' + n.toolbar.preview + "</title><style>" + t + '</style></head><body class="keditor-editable">' + i + "</body></html>")
                                    },
                                    setContents: function(t) {
                                        if (c._variable.wysiwygActive) {
                                            const i = r.convertContentsForEditor(t);
                                            i !== e.element.wysiwyg.innerHTML && (e.element.wysiwyg.innerHTML = i, c.history.push())
                                        } else {
                                            const e = r.convertHTMLForCodeView(t);
                                            e !== c._getCodeView() && c._setCodeView(e)
                                        }
                                    },
                                    getContents: function() {
                                        const t = e.element.wysiwyg.innerHTML,
                                            i = r.createElement("DIV");
                                        i.innerHTML = t;
                                        const n = r.getListChildren(i, (function(e) {
                                            return /FIGCAPTION/i.test(e.nodeName)
                                        }));
                                        for (let e = 0, t = n.length; e < t; e++) n[e].removeAttribute("contenteditable");
                                        return e.option.fullPage ? "<!DOCTYPE html><html>" + this._wd.head.outerHTML + this._wd.body.outerHTML + "</html>" : i.innerHTML
                                    },
                                    addDocEvent: function(t, i, n) {
                                        l.addEventListener(t, i, n), e.option.iframe && this._wd.addEventListener(t, i)
                                    },
                                    removeDocEvent: function(t, i) {
                                        l.removeEventListener(t, i), e.option.iframe && this._wd.removeEventListener(t, i)
                                    },
                                    _charCount: function(t, i) {
                                        const n = e.element.charCounter;
                                        if (!n) return !0;
                                        (!t || t < 0) && (t = 0);
                                        const o = e.option.maxCharCount;
                                        if (a.setTimeout((function() {
                                                n.textContent = e.element.wysiwyg.textContent.length
                                            })), o > 0) {
                                            let l = !1;
                                            const s = e.element.wysiwyg.textContent.length;
                                            if (s > o) {
                                                c._editorRange();
                                                const e = c.getRange(),
                                                    t = e.endOffset - 1,
                                                    i = c.getSelectionNode().textContent;
                                                c.getSelectionNode().textContent = i.slice(0, e.endOffset - 1) + i.slice(e.endOffset, i.length), c.setRange(e.endContainer, t, e.endContainer, t), l = !0
                                            } else s + t > o && (l = !0);
                                            if (l) return i && !r.hasClass(n, "ke-blink") && (r.addClass(n, "ke-blink"), a.setTimeout((function() {
                                                r.removeClass(n, "ke-blink")
                                            }), 600)), !1
                                        }
                                        return !0
                                    },
                                    _checkComponents: function() {
                                        this.plugins.image && (this.initPlugins.image ? this.plugins.image.checkImagesInfo.call(this) : this.callPlugin("image", this.plugins.image.checkImagesInfo.bind(this))), this.plugins.video && (this.initPlugins.video ? this.plugins.video.checkVideosInfo.call(this) : this.callPlugin("video", this.plugins.video.checkVideosInfo.bind(this)))
                                    },
                                    _setCodeView: function(t) {
                                        e.option.codeMirrorEditor ? e.option.codeMirrorEditor.getDoc().setValue(t) : e.element.code.value = t
                                    },
                                    _getCodeView: function() {
                                        return e.option.codeMirrorEditor ? e.option.codeMirrorEditor.getDoc().getValue() : e.element.code.value
                                    },
                                    _init: function() {
                                        c._ww = e.option.iframe ? e.element.wysiwygFrame.contentWindow : a, c._wd = l, a.setTimeout((function() {
                                            o.iframe && (c._wd = e.element.wysiwygFrame.contentDocument, e.element.wysiwyg = c._wd.body, "auto" === o.height && (c._iframeAuto = c._wd.body, c._iframeAutoHeight())), c._checkComponents(), c.history = p(c, d._onChange_historyStack)
                                        })), c.codeViewDisabledButtons = e.element.toolbar.querySelectorAll('.ke-toolbar button:not([class~="code-view-enabled"])'), c._isInline = /inline/i.test(e.option.mode), c._isBalloon = /balloon/i.test(e.option.mode), c.commandMap = {
                                            FORMAT: e.tool.format,
                                            FONT: e.tool.font,
                                            FONT_TOOLTIP: e.tool.fontTooltip,
                                            SIZE: e.tool.fontSize,
                                            ALIGN: e.tool.align,
                                            LI: e.tool.list,
                                            LI_ICON: e.tool.list && e.tool.list.querySelector("i"),
                                            STRONG: e.tool.bold,
                                            INS: e.tool.underline,
                                            EM: e.tool.italic,
                                            DEL: e.tool.strike,
                                            SUB: e.tool.subscript,
                                            SUP: e.tool.superscript,
                                            OUTDENT: e.tool.outdent
                                        }, c._variable._originCssText = e.element.topArea.style.cssText
                                    },
                                    _iframeAutoHeight: function() {
                                        c._iframeAuto && (e.element.wysiwygFrame.style.height = c._iframeAuto.offsetHeight + "px")
                                    }
                                },
                                d = {
                                    _directionKeyKeyCode: new a.RegExp("^(8|13|32|46|33|34|35|36|37|38|39|40|46|98|100|102|104)$"),
                                    _historyIgnoreKeycode: new a.RegExp("^(9|1[6-8]|20|3[3-9]|40|45|11[2-9]|12[0-3]|144|145)$"),
                                    _onButtonsCheck: new a.RegExp("^(STRONG|INS|EM|DEL|SUB|SUP|LI)$"),
                                    _keyCodeShortcut: {
                                        65: "A",
                                        66: "B",
                                        83: "S",
                                        85: "U",
                                        73: "I",
                                        89: "Y",
                                        90: "Z",
                                        219: "[",
                                        221: "]"
                                    },
                                    _shortcutCommand: function(e, t) {
                                        let i = null;
                                        switch (d._keyCodeShortcut[e]) {
                                            case "A":
                                                i = "selectAll";
                                                break;
                                            case "B":
                                                i = "STRONG";
                                                break;
                                            case "S":
                                                t && (i = "DEL");
                                                break;
                                            case "U":
                                                i = "INS";
                                                break;
                                            case "I":
                                                i = "EM";
                                                break;
                                            case "Z":
                                                i = t ? "redo" : "undo";
                                                break;
                                            case "Y":
                                                i = "redo";
                                                break;
                                            case "[":
                                                i = "outdent";
                                                break;
                                            case "]":
                                                i = "indent"
                                        }
                                        return !!i && (c.commandHandler(c.commandMap[i], i), !0)
                                    },
                                    _findButtonEffectTag: function() {
                                        const t = c.commandMap,
                                            i = this._onButtonsCheck,
                                            o = [],
                                            l = [];
                                        let s = !0,
                                            a = !0,
                                            d = !0,
                                            u = !0,
                                            h = !0,
                                            g = !0,
                                            p = !0,
                                            m = "";
                                        for (let f = c.getSelectionNode(); !r.isWysiwygDiv(f) && f; f = f.parentNode)
                                            if (1 === f.nodeType && !r.isBreak(f))
                                                if (m = f.nodeName.toUpperCase(), l.push(m), r.isFormatElement(f)) {
                                                    s && t.FORMAT && (o.push("FORMAT"), r.changeTxt(t.FORMAT, m), t.FORMAT.setAttribute("data-focus", m), s = !1);
                                                    const e = f.style.textAlign;
                                                    a && e && t.ALIGN && (o.push("ALIGN"), t.ALIGN.className = "ke-icon-align-" + e, t.ALIGN.setAttribute("data-focus", e), a = !1), g && f.style.marginLeft && 1 * (f.style.marginLeft.match(/\d+/) || [0])[0] > 0 && t.OUTDENT && (o.push("OUTDENT"), t.OUTDENT.removeAttribute("disabled"), g = !1)
                                                } else {
                                                    if (d && r.isList(m) && t.LI && (o.push("LI"), t.LI.setAttribute("data-focus", m), /UL/i.test(m) ? (r.removeClass(t.LI_ICON, "ke-icon-list-number"), r.addClass(t.LI_ICON, "ke-icon-list-bullets")) : (r.removeClass(t.LI_ICON, "ke-icon-list-bullets"), r.addClass(t.LI_ICON, "ke-icon-list-number")), d = !1), u && f.style.fontFamily.length > 0 && t.FONT) {
                                                        o.push("FONT");
                                                        const e = (f.style.fontFamily || f.face || n.toolbar.font).replace(/["']/g, "");
                                                        r.changeTxt(t.FONT, e), r.changeTxt(t.FONT_TOOLTIP, e), u = !1
                                                    }
                                                    h && f.style.fontSize.length > 0 && t.SIZE && (o.push("SIZE"), r.changeTxt(t.SIZE, f.style.fontSize), h = !1), p && /^A$/.test(m) && null === f.getAttribute("data-image-link") && c.plugins.link ? (e.link && c.controllerArray[0] === e.link.linkBtn || c.callPlugin("link", (function() {
                                                        c.plugins.link.call_controller_linkButton.call(c, f)
                                                    })), p = !1) : p && e.link && c.controllerArray[0] === e.link.linkBtn && c.controllersOff(), i.test(m) && o.push(m)
                                                } for (let e = 0; e < o.length; e++) m = o[e], i.test(m) && r.addClass(t[m], "active");
                                        for (let e in t) o.indexOf(e) > -1 || (t.FONT && /^FONT$/i.test(e) ? (r.changeTxt(t.FONT, n.toolbar.font), r.changeTxt(t.FONT_TOOLTIP, n.toolbar.font)) : t.SIZE && /^SIZE$/i.test(e) ? r.changeTxt(t.SIZE, n.toolbar.fontSize) : t.ALIGN && /^ALIGN$/i.test(e) ? (t.ALIGN.className = "ke-icon-align-left", t.ALIGN.removeAttribute("data-focus")) : t.OUTDENT && /^OUTDENT$/i.test(e) ? t.OUTDENT.setAttribute("disabled", !0) : t.LI && r.isListCell(e) ? (t.LI.removeAttribute("data-focus"), r.removeClass(t.LI_ICON, "ke-icon-list-bullets"), r.addClass(t.LI_ICON, "ke-icon-list-number"), r.removeClass(t.LI, "active")) : r.removeClass(t[e], "active"));
                                        c._variable.currentNodes = l.reverse(), e.option.showPathLabel && (e.element.navigation.textContent = c._variable.currentNodes.join(" > "))
                                    },
                                    _cancelCaptionEdit: function() {
                                        this.setAttribute("contenteditable", !1), this.removeEventListener("blur", d._cancelCaptionEdit)
                                    },
                                    onMouseDown_toolbar: function(e) {
                                        let t = e.target;
                                        if (r.getParentElement(t, ".ke-submenu")) e.stopPropagation(), c._notHideToolbar = !0;
                                        else {
                                            e.preventDefault();
                                            let i = t.getAttribute("data-command"),
                                                n = t.className;
                                            for (; !i && !/ke-menu-list/.test(n) && !/ke-toolbar/.test(n);) i = (t = t.parentNode).getAttribute("data-command"), n = t.className;
                                            i === c._submenuName && e.stopPropagation()
                                        }
                                    },
                                    onClick_toolbar: function(e) {
                                        e.preventDefault(), e.stopPropagation();
                                        let t = e.target,
                                            i = t.getAttribute("data-display"),
                                            n = t.getAttribute("data-command"),
                                            o = t.className;
                                        for (; !n && !/ke-menu-list/.test(o) && !/ke-toolbar/.test(o);) t = t.parentNode, n = t.getAttribute("data-command"), i = t.getAttribute("data-display"), o = t.className;
                                        if ((n || i) && !t.disabled) {
                                            if (c.focus(), i) return !/submenu/.test(i) || null !== t.nextElementSibling && t === c.submenuActiveButton ? /dialog/.test(i) ? void c.callPlugin(n, (function() {
                                                c.plugins.dialog.open.call(c, n, !1)
                                            })) : void c.submenuOff() : void c.callPlugin(n, (function() {
                                                c.submenuOn(t)
                                            }));
                                            n && c.commandHandler(t, n)
                                        }
                                    },
                                    onMouseDown_wysiwyg: function(e) {
                                        c._isBalloon && d._hideToolbar();
                                        const t = r.getParentElement(e.target, r.isCell);
                                        if (!t) return;
                                        const i = c.plugins.table;
                                        t === i._fixedCell || i._shift || c.callPlugin("table", (function() {
                                            i.onTableCellMultiSelect.call(c, t, !1)
                                        }))
                                    },
                                    onClick_wysiwyg: function(t) {
                                        const i = t.target;
                                        if ("false" === e.element.wysiwyg.getAttribute("contenteditable")) return;
                                        if (t.stopPropagation(), /^FIGURE$/i.test(i.nodeName)) {
                                            const e = i.querySelector("IMG"),
                                                n = i.querySelector("IFRAME");
                                            if (e) {
                                                if (t.preventDefault(), !c.plugins.image) return;
                                                return void c.callPlugin("image", (function() {
                                                    const t = c.plugins.resizing.call_controller_resize.call(c, e, "image");
                                                    c.plugins.image.onModifyMode.call(c, e, t), r.getParentElement(e, ".ke-image-container") || (c.plugins.image.openModify.call(c, !0), c.plugins.image.update_image.call(c, !0, !0))
                                                }))
                                            }
                                            if (n) {
                                                if (t.preventDefault(), !c.plugins.video) return;
                                                return void c.callPlugin("video", (function() {
                                                    const e = c.plugins.resizing.call_controller_resize.call(c, n, "video");
                                                    c.plugins.video.onModifyMode.call(c, n, e)
                                                }))
                                            }
                                        }
                                        const n = r.getParentElement(i, "FIGCAPTION");
                                        if (n && "ture" !== n.getAttribute("contenteditable") && (t.preventDefault(), n.setAttribute("contenteditable", !0), n.focus(), c._isInline && !c._inlineToolbarAttr.isShow)) {
                                            d._showToolbarInline();
                                            const e = function() {
                                                d._hideToolbar(), n.removeEventListener("blur", e)
                                            };
                                            n.addEventListener("blur", e)
                                        }
                                        const o = r.getFormatElement(c.getSelectionNode()),
                                            l = r.getRangeFormatElement(c.getSelectionNode());
                                        if (!c.getRange().collapsed || o && o !== l || "false" === i.getAttribute("contenteditable") || (c.execCommand("formatBlock", !1, r.isRangeFormatElement(l) ? "DIV" : "P"), c.focus()), c._editorRange(), d._findButtonEffectTag(), c._isBalloon) {
                                            const e = c.getRange();
                                            e.collapsed ? d._hideToolbar() : d._showToolbarBalloon(e)
                                        }
                                        m.onClick && m.onClick(t)
                                    },
                                    _showToolbarBalloon: function(t) {
                                        const i = t || c.getRange(),
                                            n = e.element.toolbar,
                                            o = c.getSelection();
                                        let s;
                                        if (o.focusNode === o.anchorNode) s = o.focusOffset < o.anchorOffset;
                                        else {
                                            const e = r.getListChildNodes(i.commonAncestorContainer);
                                            s = r.getArrayIndex(e, o.focusNode) < r.getArrayIndex(e, o.anchorNode)
                                        }
                                        let u = i.getClientRects();
                                        u = u[s ? 0 : u.length - 1];
                                        const h = a.scrollX || l.documentElement.scrollLeft,
                                            g = a.scrollY || l.documentElement.scrollTop,
                                            p = e.element.topArea.offsetWidth,
                                            m = d._getStickyOffsetTop();
                                        let f = 0,
                                            _ = e.element.topArea;
                                        for (; _ && !/^(BODY|HTML)$/i.test(_.nodeName);) f += _.offsetLeft, _ = _.offsetParent;
                                        n.style.visibility = "hidden", n.style.display = "block";
                                        const b = a.Math.round(e.element._arrow.offsetWidth / 2),
                                            y = n.offsetWidth,
                                            v = n.offsetHeight,
                                            k = /iframe/i.test(e.element.wysiwygFrame.nodeName) ? e.element.wysiwygFrame.getClientRects()[0] : null;
                                        k && (u = {
                                            left: u.left + k.left,
                                            top: u.top + k.top,
                                            right: u.right + k.right - k.width,
                                            bottom: u.bottom + k.bottom - k.height
                                        }), d._setToolbarOffset(s, u, n, f, p, h, g, m, b), y === n.offsetWidth && v === n.offsetHeight || d._setToolbarOffset(s, u, n, f, p, h, g, m, b), n.style.visibility = ""
                                    },
                                    _setToolbarOffset: function(t, i, n, o, l, s, c, d, u) {
                                        const h = n.offsetWidth,
                                            g = n.offsetHeight,
                                            p = (t ? i.left : i.right) - o - h / 2 + s,
                                            m = p + h - l,
                                            f = (t ? i.top - g - u : i.bottom + u) - d + c;
                                        let _ = p < 0 ? 1 : m < 0 ? p : p - m - 1 - 1;
                                        n.style.left = a.Math.floor(_) + "px", n.style.top = a.Math.floor(f) + "px", t ? (r.removeClass(e.element._arrow, "ke-arrow-up"), r.addClass(e.element._arrow, "ke-arrow-down"), e.element._arrow.style.top = g + "px") : (r.removeClass(e.element._arrow, "ke-arrow-down"), r.addClass(e.element._arrow, "ke-arrow-up"), e.element._arrow.style.top = -u + "px");
                                        const b = a.Math.floor(h / 2 + (p - _));
                                        e.element._arrow.style.left = (b + u > n.offsetWidth ? n.offsetWidth - u : b < u ? u : b) + "px"
                                    },
                                    _showToolbarInline: function() {
                                        const t = e.element.toolbar;
                                        t.style.visibility = "hidden", t.style.display = "block", c._inlineToolbarAttr.width = t.style.width = e.option.toolbarWidth, c._inlineToolbarAttr.top = t.style.top = -1 - t.offsetHeight + "px", "function" == typeof m.showInline && m.showInline(t, e), d.onScroll_window(), c._inlineToolbarAttr.isShow = !0, t.style.visibility = ""
                                    },
                                    _hideToolbar: function() {
                                        c._notHideToolbar || (e.element.toolbar.style.display = "none", c._inlineToolbarAttr.isShow = !1), c._notHideToolbar = !1
                                    },
                                    onKeyDown_wysiwyg: function(t) {
                                        const i = t.keyCode,
                                            n = t.shiftKey,
                                            o = t.ctrlKey || t.metaKey,
                                            l = t.altKey;
                                        if (a.setTimeout(c._iframeAutoHeight), c._isBalloon && d._hideToolbar(), o && d._shortcutCommand(i, n)) return t.preventDefault(), t.stopPropagation(), !1;
                                        const s = c.getSelectionNode(),
                                            u = c.getRange(),
                                            h = u.startContainer !== u.endContainer,
                                            g = c._resizingName;
                                        let p = r.getFormatElement(s) || s,
                                            f = r.getRangeFormatElement(s);
                                        switch (i) {
                                            case 8:
                                                if (h) break;
                                                if (g) {
                                                    t.preventDefault(), t.stopPropagation(), c.plugins[g].destroy.call(c);
                                                    break
                                                }
                                                if (r.isWysiwygDiv(s.parentNode) && !s.previousSibling && r.isFormatElement(s) && !r.isListCell(s)) return t.preventDefault(), t.stopPropagation(), s.innerHTML = "<br>", !1;
                                                const i = u.commonAncestorContainer;
                                                if (0 === u.startOffset && 0 === u.endOffset) {
                                                    if (f && p && !r.isCell(f) && !/^FIGCAPTION$/i.test(f.nodeName)) {
                                                        let e = !0,
                                                            n = i;
                                                        for (; n && n !== f && !r.isWysiwygDiv(n);) {
                                                            if (n.previousSibling) {
                                                                e = !1;
                                                                break
                                                            }
                                                            n = n.parentNode
                                                        }
                                                        if (e && f.parentNode) {
                                                            t.preventDefault(), c.detachRangeFormatElement(f, r.isListCell(p) ? [p] : null, null, !1, !1);
                                                            break
                                                        }
                                                    }
                                                    if (r.isComponent(i.previousSibling)) {
                                                        const e = i.previousSibling;
                                                        r.removeItem(e)
                                                    }
                                                }
                                                break;
                                            case 46:
                                                if (g) {
                                                    t.preventDefault(), t.stopPropagation(), c.plugins[g].destroy.call(c);
                                                    break
                                                }
                                                if ((r.isFormatElement(s) || null === s.nextSibling) && u.startOffset === s.textContent.length) {
                                                    let e = p.nextElementSibling;
                                                    if (r.isComponent(e)) {
                                                        t.preventDefault(), r.onlyZeroWidthSpace(p) && r.removeItem(p), (r.hasClass(e, "ke-component") || /^IMG$/i.test(e.nodeName)) && (t.stopPropagation(), r.hasClass(e, "ke-image-container") || /^IMG$/i.test(e.nodeName) ? (e = /^IMG$/i.test(e.nodeName) ? e : e.querySelector("img"), c.callPlugin("image", (function() {
                                                            const t = c.plugins.resizing.call_controller_resize.call(c, e, "image");
                                                            c.plugins.image.onModifyMode.call(c, e, t), r.getParentElement(e, ".ke-component") || (c.plugins.image.openModify.call(c, !0), c.plugins.image.update_image.call(c, !0, !0))
                                                        }))) : r.hasClass(e, "ke-video-container") && (t.stopPropagation(), c.callPlugin("video", (function() {
                                                            const t = e.querySelector("iframe"),
                                                                i = c.plugins.resizing.call_controller_resize.call(c, t, "video");
                                                            c.plugins.video.onModifyMode.call(c, t, i)
                                                        }))), c.history.push());
                                                        break
                                                    }
                                                }
                                                break;
                                            case 9:
                                                if (t.preventDefault(), o || l || r.isWysiwygDiv(s)) break;
                                                c.controllersOff();
                                                let a = s;
                                                for (; !r.isCell(a) && !r.isWysiwygDiv(a);) a = a.parentNode;
                                                if (a && r.isCell(a)) {
                                                    const e = r.getParentElement(a, "table"),
                                                        t = r.getListChildren(e, r.isCell);
                                                    let i = n ? r.prevIdx(t, a) : r.nextIdx(t, a);
                                                    i !== t.length || n || (i = 0), -1 === i && n && (i = t.length - 1);
                                                    const o = t[i];
                                                    if (!o) return !1;
                                                    c.setRange(o, 0, o, 0);
                                                    break
                                                }
                                                const d = c.getSelectedElements();
                                                if (n)
                                                    for (let e, t = 0, i = d.length; t < i; t++) e = d[t].firstChild, /^\s{1,4}$/.test(e.textContent) ? r.removeItem(e) : /^\s{1,4}/.test(e.textContent) && (e.textContent = e.textContent.replace(/^\s{1,4}/, ""));
                                                else {
                                                    const e = r.createTextNode(new Array(c._variable.tabSize + 1).join(" "));
                                                    if (1 === d.length) c.insertNode(e), c.setRange(e, c._variable.tabSize, e, c._variable.tabSize);
                                                    else
                                                        for (let t = 0, i = d.length; t < i; t++) d[t].insertBefore(e.cloneNode(!1), d[t].firstChild)
                                                }
                                                c.history.push();
                                                break;
                                            case 13:
                                                if (h) break;
                                                p = r.getFormatElement(s), f = r.getRangeFormatElement(s);
                                                const m = r.getParentElement(f, "FIGCAPTION");
                                                if (f && p && !r.isCell(f) && !/^FIGCAPTION$/i.test(f.nodeName)) {
                                                    if (!c.getRange().commonAncestorContainer.nextElementSibling && r.onlyZeroWidthSpace(p.innerText.trim())) {
                                                        t.preventDefault();
                                                        const e = c.appendFormatTag(f, r.isCell(f.parentNode) ? "DIV" : r.isListCell(p) ? "P" : null);
                                                        r.removeItemAllParents(p), c.setRange(e, 1, e, 1);
                                                        break
                                                    }
                                                }
                                                if (f && m && r.getParentElement(f, r.isList) && (t.preventDefault(), p = c.appendFormatTag(p), c.setRange(p, 0, p, 0)), g) {
                                                    t.preventDefault(), t.stopPropagation();
                                                    const i = e[g],
                                                        n = i._container,
                                                        o = n.previousElementSibling || n.nextElementSibling;
                                                    let l = null;
                                                    r.isListCell(n.parentNode) ? l = r.createElement("BR") : (l = r.createElement(r.isFormatElement(o) ? o.nodeName : "P")).innerHTML = "<br>", n.parentNode.insertBefore(l, n), c.callPlugin(g, (function() {
                                                        c.controllersOff();
                                                        const e = c.plugins.resizing.call_controller_resize.call(c, i._element, g);
                                                        c.plugins[g].onModifyMode.call(c, i._element, e)
                                                    })), c.history.push()
                                                }
                                        }
                                        if (n && /16/.test(i)) {
                                            t.preventDefault(), t.stopPropagation();
                                            const e = c.plugins.table;
                                            if (e && !e._shift && !e._ref) {
                                                const t = r.getParentElement(p, r.isCell);
                                                if (t) return void e.onTableCellMultiSelect.call(c, t, !0)
                                            }
                                        }
                                        const _ = !o && !l && 1 === t.key.length;
                                        if (!c._charCount(1, _) && _) return t.preventDefault(), t.stopPropagation(), !1;
                                        m.onKeyDown && m.onKeyDown(t)
                                    },
                                    onKeyUp_wysiwyg: function(e) {
                                        c._editorRange();
                                        const t = e.keyCode;
                                        let i = c.getSelectionNode();
                                        if (c._isBalloon && !c.getRange().collapsed) return void d._showToolbarBalloon();
                                        if (8 === t && r.isWysiwygDiv(i) && "" === i.textContent) {
                                            e.preventDefault(), e.stopPropagation(), i.innerHTML = "";
                                            const t = r.createElement(r.isFormatElement(c._variable.currentNodes[0]) ? c._variable.currentNodes[0] : "P");
                                            return t.innerHTML = "<br>", i.appendChild(t), c.setRange(t, 0, t, 0), d._findButtonEffectTag(), void c._checkComponents()
                                        }
                                        const n = r.getFormatElement(i),
                                            o = r.getRangeFormatElement(i);
                                        if (n && n !== o || (c.execCommand("formatBlock", !1, r.isRangeFormatElement(o) ? "DIV" : "P"), c.focus(), i = c.getSelectionNode()), d._directionKeyKeyCode.test(t) && d._findButtonEffectTag(), c._checkComponents(), !c._charCount(1, 1 === e.key.length) && 1 === e.key.length) return e.preventDefault(), e.stopPropagation(), !1;
                                        d._historyIgnoreKeycode.test(t) || c.history.push(), m.onKeyUp && m.onKeyUp(e)
                                    },
                                    onScroll_wysiwyg: function(e) {
                                        c.controllersOff(), c._isBalloon && d._hideToolbar(), m.onScroll && m.onScroll(e)
                                    },
                                    onMouseDown_resizingBar: function(t) {
                                        t.stopPropagation(), c._variable.resizeClientY = t.clientY, e.element.resizeBackground.style.display = "block", l.addEventListener("mousemove", d._resize_editor), l.addEventListener("mouseup", (function t() {
                                            e.element.resizeBackground.style.display = "none", l.removeEventListener("mousemove", d._resize_editor), l.removeEventListener("mouseup", t)
                                        }))
                                    },
                                    _resize_editor: function(t) {
                                        const i = e.element.editorArea.offsetHeight + (t.clientY - c._variable.resizeClientY);
                                        e.element.wysiwygFrame.style.height = e.element.code.style.height = (i < c._variable.minResizingSize ? c._variable.minResizingSize : i) + "px", c._variable.resizeClientY = t.clientY
                                    },
                                    onResize_window: function() {
                                        c.controllersOff(), 0 !== e.element.toolbar.offsetWidth && (c._variable.isFullScreen ? (c._variable.innerHeight_fullScreen += a.innerHeight - e.element.toolbar.offsetHeight - c._variable.innerHeight_fullScreen, e.element.editorArea.style.height = c._variable.innerHeight_fullScreen + "px") : c._sticky && (e.element.toolbar.style.width = e.element.topArea.offsetWidth - 2 + "px", d.onScroll_window()))
                                    },
                                    onScroll_window: function() {
                                        if (c._variable.isFullScreen || 0 === e.element.toolbar.offsetWidth) return;
                                        const t = e.element,
                                            i = t.editorArea.offsetHeight,
                                            n = (this.scrollY || l.documentElement.scrollTop) + e.option.stickyToolbar,
                                            o = d._getStickyOffsetTop() - (c._isInline ? t.toolbar.offsetHeight : 0);
                                        n < o ? d._offStickyToolbar() : n + c._variable.minResizingSize >= i + o ? (c._sticky || d._onStickyToolbar(), t.toolbar.style.top = i + o + e.option.stickyToolbar - n - c._variable.minResizingSize + "px") : n >= o && d._onStickyToolbar()
                                    },
                                    _getStickyOffsetTop: function() {
                                        let t = e.element.topArea,
                                            i = 0;
                                        for (; t;) i += t.offsetTop, t = t.offsetParent;
                                        return i
                                    },
                                    _onStickyToolbar: function() {
                                        const t = e.element;
                                        c._isInline || (t._stickyDummy.style.height = t.toolbar.offsetHeight + "px", t._stickyDummy.style.display = "block"), t.toolbar.style.top = e.option.stickyToolbar + "px", t.toolbar.style.width = c._isInline ? c._inlineToolbarAttr.width : t.toolbar.offsetWidth + "px", r.addClass(t.toolbar, "ke-toolbar-sticky"), c._sticky = !0
                                    },
                                    _offStickyToolbar: function() {
                                        const t = e.element;
                                        t._stickyDummy.style.display = "none", t.toolbar.style.top = c._isInline ? c._inlineToolbarAttr.top : "", t.toolbar.style.width = c._isInline ? c._inlineToolbarAttr.width : "", t.editorArea.style.marginTop = "", r.removeClass(t.toolbar, "ke-toolbar-sticky"), c._sticky = !1
                                    },
                                    _codeViewAutoHeight: function() {
                                        e.element.code.style.height = e.element.code.scrollHeight + "px"
                                    },
                                    onPaste_wysiwyg: function(e) {
                                        const t = e.clipboardData;
                                        if (!t) return !0;
                                        const i = c._charCount(t.getData("text/plain").length, !0),
                                            n = r.cleanHTML(t.getData("text/html"));
                                        return ("function" != typeof m.onPaste || m.onPaste(e, n, i)) && i ? void(n && (e.stopPropagation(), e.preventDefault(), c.execCommand("insertHTML", !1, n))) : (e.preventDefault(), e.stopPropagation(), !1)
                                    },
                                    onCut_wysiwyg: function() {
                                        a.setTimeout((function() {
                                            c._iframeAutoHeight(), c._charCount(0, !1), c.history.push()
                                        }))
                                    },
                                    onDragOver_wysiwyg: function(e) {
                                        e.preventDefault()
                                    },
                                    onDrop_wysiwyg: function(t) {
                                        const i = t.dataTransfer;
                                        if (!i) return !0;
                                        const n = i.files;
                                        if (n.length > 0 && c.plugins.image) d._setDropLocationSelection(t), c.callPlugin("image", (function() {
                                            e.image.imgInputFile.files = n, c.plugins.image.onRender_imgInput.call(c), e.image.imgInputFile.files = null
                                        }));
                                        else {
                                            if (!c._charCount(i.getData("text/plain").length, !0)) return t.preventDefault(), t.stopPropagation(), !1; {
                                                const e = r.cleanHTML(i.getData("text/html"));
                                                e && (d._setDropLocationSelection(t), c.execCommand("insertHTML", !1, e))
                                            }
                                        }
                                        m.onDrop && m.onDrop(t)
                                    },
                                    _setDropLocationSelection: function(e) {
                                        e.stopPropagation(), e.preventDefault();
                                        const t = c.getRange();
                                        c.setRange(t.startContainer, t.startOffset, t.endContainer, t.endOffset)
                                    },
                                    _onChange_historyStack: function() {
                                        e.tool.save && e.tool.save.removeAttribute("disabled"), m.onChange && m.onChange(c.getContents())
                                    },
                                    _addEvent: function() {
                                        const t = o.iframe ? c._ww : e.element.wysiwyg;
                                        e.element.toolbar.addEventListener("mousedown", d.onMouseDown_toolbar, !1), e.element.toolbar.addEventListener("click", d.onClick_toolbar, !1), t.addEventListener("click", d.onClick_wysiwyg, !1), t.addEventListener("keydown", d.onKeyDown_wysiwyg, !1), t.addEventListener("keyup", d.onKeyUp_wysiwyg, !1), t.addEventListener("paste", d.onPaste_wysiwyg, !1), t.addEventListener("cut", d.onCut_wysiwyg, !1), t.addEventListener("dragover", d.onDragOver_wysiwyg, !1), t.addEventListener("drop", d.onDrop_wysiwyg, !1), t.addEventListener("scroll", d.onScroll_wysiwyg, !1), (c._isBalloon || c.plugins.table) && t.addEventListener("mousedown", d.onMouseDown_wysiwyg, !1), c.plugins.table && t.addEventListener("touchstart", d.onMouseDown_wysiwyg, {
                                            passive: !0,
                                            useCapture: !1
                                        }), "auto" !== e.option.height || e.option.codeMirrorEditor || (e.element.code.addEventListener("keydown", d._codeViewAutoHeight, !1), e.element.code.addEventListener("keyup", d._codeViewAutoHeight, !1), e.element.code.addEventListener("paste", d._codeViewAutoHeight, !1)), e.element.resizingBar && (/\d+/.test(e.option.height) ? e.element.resizingBar.addEventListener("mousedown", d.onMouseDown_resizingBar, !1) : r.addClass(e.element.resizingBar, "ke-resizing-none")), c._isInline && t.addEventListener("focus", d._showToolbarInline, !1), (c._isInline || c._isBalloon) && t.addEventListener("blur", d._hideToolbar, !1), a.removeEventListener("resize", d.onResize_window), a.removeEventListener("scroll", d.onScroll_window), a.addEventListener("resize", d.onResize_window, !1), e.option.stickyToolbar > -1 && a.addEventListener("scroll", d.onScroll_window, !1)
                                    },
                                    _removeEvent: function() {
                                        const t = o.iframe ? c._ww : e.element.wysiwyg;
                                        e.element.toolbar.removeEventListener("mousedown", d.onMouseDown_toolbar), e.element.toolbar.removeEventListener("click", d.onClick_toolbar), t.removeEventListener("click", d.onClick_wysiwyg), t.removeEventListener("keydown", d.onKeyDown_wysiwyg), t.removeEventListener("keyup", d.onKeyUp_wysiwyg), t.removeEventListener("paste", d.onPaste_wysiwyg), t.removeEventListener("cut", d.onCut_wysiwyg), t.removeEventListener("dragover", d.onDragOver_wysiwyg), t.removeEventListener("drop", d.onDrop_wysiwyg), t.removeEventListener("scroll", d.onScroll_wysiwyg), t.removeEventListener("mousedown", d.onMouseDown_wysiwyg), t.removeEventListener("touchstart", d.onMouseDown_wysiwyg, {
                                            passive: !0,
                                            useCapture: !1
                                        }), t.removeEventListener("focus", d._showToolbarInline), t.removeEventListener("blur", d._hideToolbar), e.element.code.removeEventListener("keydown", d._codeViewAutoHeight), e.element.code.removeEventListener("keyup", d._codeViewAutoHeight), e.element.code.removeEventListener("paste", d._codeViewAutoHeight), e.element.resizingBar && e.element.resizingBar.removeEventListener("mousedown", d.onMouseDown_resizingBar), a.removeEventListener("resize", d.onResize_window), a.removeEventListener("scroll", d.onScroll_window)
                                    }
                                },
                                m = {
                                    onScroll: null,
                                    onClick: null,
                                    onKeyDown: null,
                                    onKeyUp: null,
                                    onDrop: null,
                                    onChange: null,
                                    onPaste: null,
                                    showInline: null,
                                    onImageUpload: null,
                                    onImageUploadError: null,
                                    setOptions: function(l) {
                                        d._removeEvent(), c.plugins = l.plugins || c.plugins;
                                        const s = [e.option, l].reduce((function(e, t) {
                                                return Object.keys(t).forEach((function(i) {
                                                    e[i] = t[i]
                                                })), e
                                            }), {}),
                                            a = h._setOptions(s, e, c.plugins, e.option);
                                        a.callButtons && (t = a.callButtons, c.initPlugins = {}), a.plugins && (c.plugins = i = a.plugins);
                                        const r = e.element,
                                            u = {
                                                _top: r.topArea,
                                                _relative: r.relative,
                                                _toolBar: r.toolbar,
                                                _editorArea: r.editorArea,
                                                _wysiwygArea: r.wysiwygFrame,
                                                _codeArea: r.code,
                                                _resizingBar: r.resizingBar,
                                                _navigation: r.navigation,
                                                _charCounter: r.charCounter,
                                                _loading: r.loading,
                                                _resizeBack: r.resizeBackground,
                                                _stickyDummy: r._stickyDummy,
                                                _arrow: r._arrow
                                            };
                                        o = s, c.lang = n = o.lang, c.context = e = g(e.element.originElement, u, o), c._init(), d._addEvent(), d._offStickyToolbar(), d.onResize_window(), c._checkComponents(), c.history = p(c, d._onChange_historyStack), c._charCount(0, !1), c.focus()
                                    },
                                    noticeOpen: function(e) {
                                        c.addModule([s]), s.open.call(c, e)
                                    },
                                    noticeClose: function() {
                                        c.addModule([s]), s.close.call(c)
                                    },
                                    save: function() {
                                        e.element.originElement.value = c.getContents()
                                    },
                                    getContext: function() {
                                        return e
                                    },
                                    getContents: function() {
                                        return c.getContents()
                                    },
                                    getImagesInfo: function() {
                                        return c._variable._imagesInfo
                                    },
                                    insertImage: function(e) {
                                        c.plugins.image && e && (c.initPlugins.image ? c.plugins.image.submitAction.call(c, e) : c.callPlugin("image", c.plugins.image.submitAction.bind(c, e)), c.focus())
                                    },
                                    insertHTML: function(e) {
                                        if (!e.nodeType || 1 !== e.nodeType) {
                                            const t = r.createElement("DIV");
                                            t.innerHTML = e, e = t.firstChild || t.content.firstChild
                                        }
                                        let t = null;
                                        (r.isFormatElement(e) || /^(IMG|IFRAME)$/i.test(e.nodeName)) && (t = r.getFormatElement(c.getSelectionNode())), c.insertNode(e, t), c.focus()
                                    },
                                    setContents: function(e) {
                                        c.setContents(e)
                                    },
                                    appendContents: function(t) {
                                        c._variable.wysiwygActive ? e.element.wysiwyg.innerHTML += r.convertContentsForEditor(t) : c._setCodeView(c._getCodeView() + r.convertHTMLForCodeView(t)), c.history.push()
                                    },
                                    disabled: function() {
                                        e.tool.cover.style.display = "block", e.element.wysiwyg.setAttribute("contenteditable", !1), e.option.codeMirrorEditor ? e.option.codeMirrorEditor.setOption("readOnly", !0) : e.element.code.setAttribute("disabled", "disabled")
                                    },
                                    enabled: function() {
                                        e.tool.cover.style.display = "none", e.element.wysiwyg.setAttribute("contenteditable", !0), e.option.codeMirrorEditor ? e.option.codeMirrorEditor.setOption("readOnly", !1) : e.element.code.removeAttribute("disabled")
                                    },
                                    show: function() {
                                        const t = e.element.topArea.style;
                                        "none" === t.display && (t.display = e.option.display)
                                    },
                                    hide: function() {
                                        e.element.topArea.style.display = "none"
                                    },
                                    destroy: function() {
                                        d._removeEvent(), r.removeItem(e.element.topArea), a.Object.keys(c).forEach((function(e) {
                                            delete c[e]
                                        })), a.Object.keys(d).forEach((function(e) {
                                            delete d[e]
                                        })), a.Object.keys(e).forEach((function(t) {
                                            delete e[t]
                                        })), a.Object.keys(t).forEach((function(e) {
                                            delete t[e]
                                        })), a.Object.keys(this).forEach(function(e) {
                                            delete this[e]
                                        }.bind(this))
                                    }
                                };
                            return c._init(), d._addEvent(), c._charCount(0, !1), m
                        }(g(n, o.constructed, o.options), o.pluginCallButtons, o.plugins, o.options.lang, t)
                }
            };
        window.KEDITOR = m.init({
            plugins: a
        })
    }
});