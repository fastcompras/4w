<?php

namespace App\Models;

use Doctrine\DBAL\Query\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use DB;

class ServicoBaseAgravante extends Model
{
    protected $table = "servico_base_agravante";
    public $timestamps = false;
    protected $dates = ['data_cadastro'];

    private $mapper = [
        'id'                   => 'id',
        'id_base_servico'      => 'base_id',
        'regra'                => 'rule',
        'valor'                => 'value',
        'incluso_servico_base' => 'is_included',
        'ordem_formulario'     => 'order',
        'rotulo_formulario'    => 'label',
        'tipo_formulario'      => 'field_type',
        'data_cadastro'        => 'created_at',
    ];

    const VALOR_SIMPLES = 1;
    const VALOR_ACUMULADO = 2;

    public function servicoBase()
    {
        return $this->belongsTo('App\Models\ServicoBase', 'id_base_servico');
    }

    public function opcoes()
    {
        return $this->hasMany('App\Models\AgravanteOpcoes', 'id_servico_base_agravante', 'id');
    }

    public function tipoCampo()
    {
        switch ($this->tipo_formulario) {
            case 1:
                return "text";
                break;
            case 2:
                return "checkbox";
                break;
            case 3:
                return "radio";
                break;
        }
    }

    public function getObject()
    {

        $data = $this->toArray();

        if (!empty($servicoBase = ServicoBase::find($data['id_base_servico']))) {
            $data['id_servico'] = $servicoBase->id_servico;
        }

        $opcoes = [];

        foreach ($this->opcoes as $opcao) {

            $item['id'] = $opcao->id;
            $item['descricao'] = $opcao->descricao;
            $item['valor'] = $opcao->valor;

            array_push($opcoes, $item);

        }

        $data['opcoes'] = $opcoes;

        return $data;
    }

    public function conflitaOrdem($idServico, $ordem)
    {
        $agravante = ServicoBaseAgravante::where('id', '<>', $this->id)
                                        ->where('id_base_servico', '=', $idServico)
                                        ->where('ordem_formulario', '=', $ordem)->get();
        return count($agravante) > 0;
    }

    public function limpaOpcoesAntigas()
    {
        foreach ($this->opcoes as $opcoes) {
            if (!empty($opcao = AgravanteOpcoes::find($opcoes->id))) {
                $opcao->delete();
            }
        }
    }


    /**
     * Retorna uma lista de serviços agravantes paginado
     *
     * @return QueryBuilder $list
     */
    public static function search($params)
    {
        $list = DB::table('servico_base_agravante')
            ->join('servico_base', 'servico_base_agravante.id_base_servico', '=', 'servico_base.id')
            ->join('servico', 'servico_base.id_servico', '=', 'servico.id')
            ->selectRaw(
                "servico_base_agravante.*, 
                 servico_base.nome as servico_base,
                 servico.descricao as servico,
                 servico.id as id_servico
                "
            );

        if (empty($params)) {
            return $list->paginate(10);
        }

        if (! empty($params['servico'])) {
            $list->where('id_servico', '=', $params['servico']);
        }

        if (! empty($params['servicoBase'])) {
            $list->where('id_base_servico', '=', $params['servicoBase']);
        }

        if (! empty($params['descricao'])) {
            $list->where('rotulo_formulario', 'like', '%' . $params['descricao'] . '%');
        }

        return $list->paginate(10);
    }

    public function getData()
    {
        $data    = $this->toArray();
        $options = [];

        foreach ($data as $key => $value) {

            if (isset($this->mapper[$key]) && $this->mapper[$key] === $key) {
                continue;
            }

            if (! array_key_exists($key, $this->mapper)) {
                unset($data[$key]);
                continue;
            }

            $data[$this->mapper[$key]] = $data[$key];

            unset($data[$key]);
        }

        foreach ($this->opcoes as $option) {

            $item['id']        = $option->id;
            $item['value']     = $option->valor;
            $item['description'] = $option->descricao;

            array_push($options, $item);
        }

        $data['options']        = $options;
        $data['selected']       = $this->incluso_servico_base ?: 0;
        $data['field_type']     = $this->tipoCampo();

        return $data;
    }
}
