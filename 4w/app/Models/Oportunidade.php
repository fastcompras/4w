<?php

namespace App\Models;

use App\Helpers\MPMail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use DB;
use Mail;
use League\Flysystem\Exception;

class Oportunidade extends Model
{
    public $timestamps      = false;
    protected $table        = "oportunidade";
    protected $dates        = ['data_cadastro', 'data_execucao', 'data_encerramento'];
    protected $statusValues = ['pendentes' => 0, 'confirmados' => 1, 'historico' => 2];

    const OPORTUNIDADE_PENDENTE_PRESTADOR = 0;
    const OPORTUNIDADE_APROVADA_PRESTADOR = 1;
    const OPORTUNIDADE_REJEITADA_PRESTADOR = 2;

    const STATUS_AGUARDANDO_PAGAMENTO = 0;
    const STATUS_PAGAMENTO_COM_FALHA = 1;
    const STATUS_ENCONTRANDO_PROFISSIONAL = 2;
    const STATUS_REJEITADO = 3;
    const STATUS_AGENDADO = 4;
    const STATUS_CANCELADO = 5;
    const STATUS_ENCERRADO = 6;

    public static function mapStatus($status) {
        return (new Oportunidade())->statusValues[$status];
    }

    public function getResumo()
    {
        $descricao = '';

        if ($this->servicoBase) {
            $descricao = "<strong> Serviço Base: </strong>" . $this->servicoBase->nome . "<br/>";

            foreach ($this->itens as $item) {
                $descricao .= "<strong>" . $item->agravante->rotulo_formulario . ": </strong>";

                if ($item->id_agravante_opcao) {

                    if (! empty($opcao = AgravanteOpcoes::find($item->id_agravante_opcao))) {
                        $descricao .= $opcao->descricao . "<br/>";
                    }

                } else {
                    if ($item->agravante->tipoCampo() == "checkbox") {
                        $descricao .= ($item->quantidade) ? "Sim" : "Não";
                        $descricao .= "<br/>";
                    } else {
                        $descricao .= $item->quantidade . "<br/>";
                    }
                }
            }
        }

        return $descricao;
    }

    public function getStatus()
    {

        $user = Auth::user();

        switch ($this->status) {

            case Oportunidade::STATUS_AGUARDANDO_PAGAMENTO:
                return ($user->getPerfil() == 'Usuário' || $user->getPerfil() == 'Admin') ? 'Aguardando Confirmação de Pagamento' : '';

            case Oportunidade::STATUS_PAGAMENTO_COM_FALHA:
                return ($user->getPerfil() == 'Usuário' || $user->getPerfil() == 'Admin') ? 'Falha no Pagamento do Serviço' : '';

            case Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL:
                return ($user->getPerfil() == 'Usuário' || $user->getPerfil() == 'Admin') ? 'Selecionando Profissional' : 'Aguardando Confirmação';

            case Oportunidade::STATUS_REJEITADO:
                return ($user->getPerfil() == 'Usuário') ? 'Selecionando Profissional' : 'Serviço Rejeitado';

            case Oportunidade::STATUS_AGENDADO:
                return ($user->getPerfil() == 'Usuário') ? 'Serviço Agendado' : 'Serviço Agendado';

            case Oportunidade::STATUS_CANCELADO:
                return ($user->getPerfil() == 'Usuário') ? 'Serviço Cancelado' : 'Serviço Cancelado';

            case Oportunidade::STATUS_ENCERRADO:

                if ($user->getPerfil() == 'Usuário') {
                    $avaliou = $this->usuario->avaliouServico($this->id);
                } else {
                    $avaliou = $this->prestador->avaliouServico($this->id);
                }

                return ($avaliou) ? 'Serviço Encerrado' : 'Aguardando Avaliação';
        }
    }

    public function getStatusPagamento()
    {
        switch ($this->status_pagamento_mp) {
            case 'approved':
                return 'Aprovado';
                break;
            case 'opened':
            case 'pending':
                return 'Pendente';
                break;
            case 'rejected':
                return 'Rejeitado';
                break;
            case 'refunded':
                return 'Estornado';
                break;
            case 'cancelled':
                return 'Cancelado';
                break;
            case 'in_mediation':
                return 'Em mediação';
                break;
            case 'charged_back':
                return 'Estorno no Cartão';
                break;
            default:
                return 'Pendente';
        }
    }

    public function getTipoPagamento()
    {
        switch ($this->tipo_pagamento_mp) {
            case 'ticket':
                return "Boleto Bancário";
                 break;
            case 'credit_card':
                return "Cartão de Crédito";
                break;
            default:
                return 'Não informado';
        }
    }

    public function getClassStatus()
    {
        switch ($this->status) {
            case 0:
                $class = "btn btn-sm btn-warning";
                break;
            case 1:
                $class = "btn btn-sm btn-primary";
                break;
            case 2:
            case 3:
                $class = "btn btn-sm btn-danger";
                break;
        }

        return $class;
    }

    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'id_usuario_solicitante');
    }

	public function cupom()
	{
		return $this->belongsTo('App\Models\Cupom', 'id_cupom');
	}

    public function prestador()
    {
        return $this->belongsTo('App\Models\User', 'id_prestador_selecionado');
    }

    public function cidade()
    {
        return $this->belongsTo('App\Models\Cidade', 'id_cidade');
    }

    public function avaliacao()
    {
        return $this->hasMany('App\Models\AvaliacaoServico', 'id_oportunidade', 'id');
    }

    public function avaliacaoTomador()
    {
        return $this->avaliacao()->where('destinatario','=', AvaliacaoServico::DESTINATARIO_TOMADOR);
    }

    public function avaliacaoPrestador()
    {
        return $this->avaliacao()->where('destinatario','=', AvaliacaoServico::DESTINATARIO_PRESTADOR);
    }

    public function itens()
    {
        return $this->hasMany('App\Models\OportunidadeItem', 'id_oportunidade', 'id');
    }

    public function candidatos()
    {
        return $this->belongsToMany('App\Models\User', 'oportunidade_tem_candidato', 'id_oportunidade', 'id_usuario');
    }

    public function servico()
    {
        return $this->belongsTo('App\Models\Servico', 'id_servico');
    }

    public function servicoBase()
    {
        return $this->belongsTo('App\Models\ServicoBase', 'id_servico_base');
    }

    public function servicosCancelados()
    {
        return $this->belongsToMany('App\Models\User', 'usuario_cancelou_servico', 'id_oportunidade', 'id_usuario');
    }

    public function possuiPrestador()
    {
        $obj = DB::table('oportunidade as o')
            ->join('oportunidade_tem_candidato as oc', 'oc.id_oportunidade', '=', 'o.id')
            ->join('usuario as u', 'u.id', '=', 'oc.id_usuario')
            ->selectRaw("o.*, oc.status")
            ->where('oc.id_oportunidade', '=', $this->id)->get();

        foreach ($obj as $item) {
            if ($item->status == OportunidadeTemCandidato::STATUS_ACEITO) return true;
        }

        return false;
    }

    public function ehCandidato($id)
    {
        foreach ($this->candidatos as $candidato) {
            if ($id == $candidato->id) return true;
        }

        return false;
    }

    public function ehPrestadorSelecionado($id)
    {
        return ($this->id_prestador_selecionado == $id);
    }

    public function geraLogCancelamentoServico($idPrestador)
    {
        $log = new UsuarioCancelouServico();
        $log->id_usuario = $idPrestador;
        $log->id_oportunidade = $this->id;
        $log->data_cancelamento = date("Y-m-d H:i:s");

        if ($log->save()) {
            return true;
        }
        return false;
    }

    public function prestadorJaCancelou($idPrestador)
    {
        foreach ($this->servicosCancelados as $servico) {
            if ($servico->id == $idPrestador) return true;
        }

        return false;
    }

    public function prestadorRejeitou($idPrestador)
    {
        $obj = DB::table('oportunidade as o')
            ->join('oportunidade_tem_candidato as oc', 'oc.id_oportunidade', '=', 'o.id')
            ->join('usuario as u', 'u.id', '=', 'oc.id_usuario')
            ->selectRaw("oc.status")
            ->where('oc.id_oportunidade', '=', $this->id)
            ->where('oc.id_usuario', '=', $idPrestador)
            ->where('oc.status', '=', OportunidadeTemCandidato::STATUS_REJEITADO)->get();

        return (count($obj) > 0);
    }

    public function foiAvaliada($destinatario)
    {
        if (count($this->avaliacao) > 0) {
            if ($destinatario == AvaliacaoServico::DESTINATARIO_TOMADOR) {
                return count($this->avaliacaoTomador) > 0 ? true : false;
            } else {
                return count($this->avaliacaoPrestador) > 0 ? true : false;
            }
        }

        return false;
    }

    public function calculaMediaAvaliacoesPrestador($idPrestador)
    {
        $usuario = User::find($idPrestador);

        if (!is_null($usuario)) {
            $servicos = Oportunidade::where('id_prestador_selecionado', '=' ,$usuario->id)->where('status','=',Oportunidade::STATUS_FINALIZADA)->get();

            $media = 0;
            $count = 0;
            foreach ($servicos as $s) {
                if ($s->avaliacaoPrestador) {
                    $count++;
                    $media += $s->avaliacaoPrestador[0]->nota;
                }
            }

            if ($count > 0) {
                $novaMedia = round($media/$count);
            } else {
                $novaMedia = 0;
            }

            $usuario->media_avaliacoes = $novaMedia;

            return $usuario->save();
        }
    }

    public function getEndereco()
    {
        $endereco = "";

        $endereco .= $this->logradouro . ", ";
        $endereco .= $this->numero;

        if (strlen($this->complemento) > 0) {
            $endereco .= "(" . $this->complemento . ")";
        }

        $endereco .= " - " . $this->bairro;

        return $endereco;
    }

    public function getEnderecoParaMapa()
    {
        $endereco = "";

        $endereco .= $this->logradouro . ", " . $this->numero . " - ";

        if ($this->cidade) {
            $endereco .= $this->cidade->nome . " , " . $this->cidade->estado->uf;
        }

        return $endereco;
    }

    public function montaObjeto()
    {
        $result = [];

        $result['id_servico']        = $this->servico->id;
        $result['descricao_servico'] = $this->servico->descricao;
        $result['id_servico_base']   = $this->servicoBase->id;
        $result['nome_servico_base'] = $this->servicoBase->nome;
        $result['id_estado']         = $this->cidade->estado->id;
        $result['nome_estado']       = $this->cidade->estado->nome;
        $result['id_cidade']         = $this->id_cidade;
        $result['nome_cidade']       = $this->cidade->nome;
        $result['logradouro']        = $this->logradouro;
        $result['numero']            = $this->numero;
        $result['complemento']       = $this->complemento;
        $result['bairro']            = $this->bairro;
        $result['cep']               = $this->cep;
        $result['data_execucao']     = date("d/m/Y", strtotime($this->data_execucao));
        $result['hora_execucao']     = date("H:i", strtotime($this->data_execucao));
        $result['informacoes_adicionais'] = $this->informacoes_adicionais;

        return $result;
    }

    public function updatePaymentMP($data)
    {
        $deal = $this;

        $deal->id_order_mp         = $data['merchant_order_id'];
        $deal->id_pedido_mp        = $data['id'];
        $deal->tipo_pagamento_mp   = $data["payment_type"];
        $deal->status_pagamento_mp = $data['status'];

        $deal->status              = ($data['status'] == "approved")
            ? self::STATUS_ENCONTRANDO_PROFISSIONAL
            : self::STATUS_PAGAMENTO_COM_FALHA;

        $deal->data_aprovacao_mp = ($data['status'] == "approved")
            ? $data['date_approved']
            : null;

        try {

            if (! $deal->save()) {

                // Send email with failure status
                MPMail::merchantPaymentFail($deal);

                return false;
            }

            $deal['usuario']          = $deal->usuario;
            $deal['data_hora']        = $deal->data_execucao->format('d/m/Y H:i:s');
            $deal['tipo_pagamento']   = $deal->getTipoPagamento();
            $deal['usuario_telefone'] = $deal->usuario->getTelefone();
            $deal['status_traduzido'] = $deal->getStatusPagamento($this->status_pagamento_mp);

        } catch (Exception $e) {
            throw new $e;
        }

        // Send email with success status
        MPMail::merchantPaymentSuccess($deal);

        return true;
    }
    
    public function updateOrderMP($data)
    {
        $deal = $this;

        $deal->id_order_mp         = $data['id'];
        $deal->id_pedido_mp        = $data['preference_id'];
        $deal->status_pagamento_mp = $data['status'];

        $deal->status              = ($data['status'] == "approved")
            ? self::STATUS_ENCONTRANDO_PROFISSIONAL
            : self::STATUS_PAGAMENTO_COM_FALHA;


        $deal->data_aprovacao_mp = ($data['status'] == "approved")
            ? $data['date_approved']
            : null;

        try {

            if (! $deal->save()) {

                // Send email with failure status
                MPMail::merchantOrderFail($deal);

                return false;
            }

            $deal['usuario']          = $deal->usuario;
            $deal['data_hora']        = $deal->data_execucao->format('d/m/Y H:i:s');
            $deal['tipo_pagamento']   = $deal->getTipoPagamento();
            $deal['usuario_telefone'] = $deal->usuario->getTelefone();
            $deal['status_traduzido'] = $deal->getStatusPagamento($this->status_pagamento_mp);

        } catch (Exception $e) {
            throw new $e;
        }

        // Send email with success status
        MPMail::merchantOrderSuccess($deal);

        return true;
    }

    public function hasDiscountCoupon()
    {
    	return $this->id_cupom !== null;
    }

    public function hasAvailableCupom()
    {
    	return $this->hasDiscountCoupon() && $this->cupom->canBeUsed();
    }

    public function getTotalWithDiscountCoupon()
    {

		if ($this->cupom->discountIsAbsolute()) {

			if ($this->valor_total >= $this->cupom->getDiscountAmount()) {
				return round($this->valor_total - $this->cupom->getDiscountAmount(), 2);
			}

		} else {

			if ($this->cupom->getDiscountAmount() >= 100) {
				return round(0, 2);
			}

			return round($this->valor_total - (($this->cupom->getDiscountAmount() / 100) * $this->valor_total), 2);
		}


		return round(0, 2);
    }

    public function useCoupon()
    {
    	$coupon = $this->cupom;

    	try {

    		if ($coupon && ! $this->wasCouponAlreadyUsed()) {

    			if ($coupon->ruleIsByAvailableQuantity() && $coupon->isAvailable()) {
    				$coupon->decreaseQuantity();
			    }

			    // Insert into history
			    CupomHistorico::create([
				    'id_cupom' => $coupon->id,
				    'id_oportunidade' => $this->id
			    ]);

		    }

	    } catch (\Exception $e) {
    		throw $e;
	    }
    }

    public function wasCouponAlreadyUsed()
    {
	    $history = CupomHistorico::where('id_oportunidade', $this->id)
		                ->get();

	    return count($history);
	 }

	 public function updateAmountAndCouponHistory()
	 {

	 	$coupon = $this->cupom;

		 try {

			 if ($coupon && $coupon->canBeUsed()) {

				 CupomHistorico::where('id_oportunidade', $this->id)
					 ->where('id_cupom', $coupon->id)
					 ->update(['data_cadastro' => date('Y-m-d H:i:s')]);


				 if ($this->valor_total)
				 $this->valor_total = $this->getTotalWithDiscountCoupon();
				 $this->save();
			 }

		 } catch (\Exception $e) {
			 throw $e;
		 }

	 }
}