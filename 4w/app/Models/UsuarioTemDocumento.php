<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioTemDocumento extends Model
{
    protected $table = 'usuario_tem_documento';
    public $timestamps = false;

}
