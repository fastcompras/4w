<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PerfilConta extends Model
{
    const PERFIL_PRESTADOR = 1;
    const PERFIL_TOMADOR = 2;
    const PERFIL_ADMIN = 3;

    protected $table = 'perfil_conta';
    public $timestamps = false;

    public function usuario()
    {
        return $this->hasMany('App\Models\User', 'id_perfil_conta');
    }
}
