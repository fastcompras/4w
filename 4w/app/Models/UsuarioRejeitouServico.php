<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioRejeitouServico extends Model
{
    protected $table = "usuario_rejeitou_servico";
    public $timestamps = false;
}
