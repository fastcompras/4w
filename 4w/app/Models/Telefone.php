<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model
{
    protected $table = 'telefone';
    public $timestamps = false;

    public function usuarios()
    {
        return $this->belongsToMany('App\Models\Usuario', 'usuario_tem_telefone', 'id_telefone', 'id_usuario');
    }

    public function getNumero(){
        return trim(substr($this->numero, 3));
    }

    public function getDDD()
    {
        return substr($this->numero, 1,2 );
    }
}
