<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = 'cidade';
    public $timestamps = false;

    public function estado()
    {
        return $this->belongsTo('App\Models\Estado', 'id_estado');
    }
}
