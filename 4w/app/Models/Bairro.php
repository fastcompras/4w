<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bairro extends Model
{
    protected $table = 'bairro';
    public $timestamps = false;

    public function cidade()
    {
        return $this->belongsTo('App\Models\Cidade', 'id_cidade');
    }

    public function regiao()
    {
        return $this->belongsTo('App\Models\Regiao', 'id_regiao');
    }
}
