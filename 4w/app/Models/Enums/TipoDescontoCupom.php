<?php

namespace App\Models\Enums;

class TipoDescontoCupom
{
	const ABSOLUTE   = 1;
	const PERCENTAGE = 2;
}
