<?php

namespace App\Models\Enums;

class TipoRegraCupom
{
	const VALIDATE_BY_AVAILABLE_QUANTITY = 1;
	const VALIDATE_BY_EXPIRATION_DATE    = 2;
	const VALIDATE_BY_DATE_AND_QUANTITY  = 3;
}
