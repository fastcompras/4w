<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MotivoRejeicao extends Model
{
    protected $table = "motivo_rejeicao";
    public $timestamps = false;

    public function oportunidade()
    {
        return $this->belongsTo('App\Models\Oportunidade', 'id_motivo_rejeicao');
    }
}
