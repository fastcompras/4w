<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regiao extends Model
{
    protected $table = 'regiao';
    public $timestamps = false;

    public function cidade()
    {
        return $this->belongsTo('App\Models\Cidade', 'id_cidade');
    }

    public function bairros()
    {
        return $this->hasMany('App\Models\Bairro', 'id_regiao', 'id');
    }
}
