<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OportunidadeTemCandidato extends Model
{
    const STATUS_CONVIDADO = 0;
    const STATUS_ACEITO = 1;
    const STATUS_REJEITADO = 2;

    protected $table   = 'oportunidade_tem_candidato';
    public $timestamps = false;

    public static function find($primaryOne, $PrimaryTwo) {
        return OportunidadeTemCandidato::where('id_oportunidade', '=', $primaryOne)
            ->where('id_usuario', '=', $PrimaryTwo)
            ->first();
    }

    public function rejeicao()
    {
        return $this->belongsTo('App\Models\MotivoRejeicao', 'id_motivo_rejeicao');
    }
}
