<?php

namespace App\Models;

use Illuminate\Contracts\Encryption\EncryptException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\PerfilConta;
use DB;
use League\Flysystem\Plugin\EmptyDir;

class User extends Authenticatable {
    use Notifiable;

    const USUARIO_PENDENTE = 0;
    const USUARIO_APROVADO = 1;
    const USUARIO_REPROVADO = 2;
    const USUARIO_BLOQUEADO = 3;

    const ID_ADMIN = 2;

    protected $table = 'usuario';
    public $timestamps = false;

    protected $fillable = [
        'email', 'password', 'cpfcnpj'
    ];

    protected $hidden = [
        'password',
    ];

    protected $dates = ['data_cadastro'];

    public function salvaDocumento($url, $tipo) {
        $documento = new Documento();
        $documento->url = $url;
        $documento->tipo = $tipo;
        $documento->data_cadastro = date("Y-m-d H:i:s");

        if ($documento->save()) {
            $relacao = new UsuarioTemDocumento();
            $relacao->id_usuario = $this->id;
            $relacao->id_documento = $documento->id;

            if ($relacao->save()) {
                return true;
            }
        }

        return false;
    }

    public function isPrestador() {
        return $this->id_perfil_conta == PerfilConta::PERFIL_PRESTADOR;
    }

    public function isTomador() {
        return $this->id_perfil_conta == PerfilConta::PERFIL_TOMADOR;
    }

    public function getPerfil() {

        if ($this->id_perfil_conta == PerfilConta::PERFIL_ADMIN) {
            return 'Admin';
        }

        return $this->id_perfil_conta == PerfilConta::PERFIL_PRESTADOR ? 'Prestador' : 'Usuário';
    }

    public function temDocumentosPendentes() {
        $total = 0;

        if ($this->isPrestador()) {
            foreach ($this->documentos as $doc) {
                if ($doc->tipo != Documento::TIPO_CERTIFICADO) $total++;
            }
        }

        return ($total < 6);
    }

    public function perfil() {
        return $this->belongsTo('App\Models\PerfilConta', 'id_perfil_conta');
    }

    public function cidade() {
        return $this->belongsTo('App\Models\Cidade', 'id_cidade');
    }

    public function documentos() {
        return $this->belongsToMany('App\Models\Documento', 'usuario_tem_documento', 'id_usuario', 'id_documento');
    }

    public function ofertas() {
        return $this->belongsToMany('App\Models\Oportunidade', 'oportunidade_tem_candidato', 'id_usuario', 'id_oportunidade');
    }

    public function ofertasAceitas() {
        return $this->belongsToMany('App\Models\Oportunidade', 'oportunidade_tem_candidato', 'id_usuario', 'id_oportunidade')->where('oportunidade_tem_candidato.status', '=', OportunidadeTemCandidato::STATUS_ACEITO);
    }

    public function ofertasRecusadas() {
        return $this->belongsToMany('App\Models\Oportunidade', 'oportunidade_tem_candidato', 'id_usuario', 'id_oportunidade')->where('oportunidade_tem_candidato.status', '=', OportunidadeTemCandidato::STATUS_REJEITADO);
    }

    public function ofertasPendentes() {
        return $this->belongsToMany('App\Models\Oportunidade', 'oportunidade_tem_candidato', 'id_usuario', 'id_oportunidade')->where('oportunidade_tem_candidato.status', '=', OportunidadeTemCandidato::STATUS_CONVIDADO);
    }

    public function servicosCancelados() {
        return $this->belongsToMany('App\Models\Oportunidade', 'usuario_cancelou_servico', 'id_usuario', 'id_oportunidade');
    }

    public function buscaAgenda() {
        $obj = DB::table('oportunidade as o')
            ->join('oportunidade_tem_candidato as oc', 'oc.id_oportunidade', '=', 'o.id')
            ->join('usuario as u', 'u.id', '=', 'oc.id_usuario')
            ->join('usuario as sol', 'sol.id', '=', 'o.id_usuario_solicitante')
            ->join('servico as s', 's.id', '=', 'o.id_servico')
            ->selectRaw("o.*, s.id as id_servico, sol.nome as nome_solicitante, s.descricao as servico")
            ->where('o.status', '=', Oportunidade::STATUS_AGENDADO)
            ->where('o.data_execucao', '>', date('Y-m-d H:i:s'))
            ->where('oc.id_usuario', '=', $this->id)->get();

        $data = [];
        $result = [];
        if(count($obj) > 0) {
            foreach ($obj as $servico) {
                $oportunidade = Oportunidade::find($servico->id);

                if ($oportunidade) {
                    $result['id_servico'] = $servico->id_servico;
                    $result['title'] = $servico->servico;
                    $result['start'] = $servico->data_execucao;
                    $result['url'] = '/oportunidades/visualizar/' . $oportunidade->id;
                    $result['allDay'] = false;
                    $result['timeFormat'] = 'H(:mm)';
                    $result['description'] = "<strong>Solicitante: </strong>" . $servico->nome_solicitante . "<br/>" . "<span><strong>Serviço: </strong>" . $oportunidade->servicoBase->descricao . "</span><br/><span><strong>Hora: </strong>" . date('H:i', strtotime($oportunidade->data_execucao)) . "</span>";

                    array_push($data, $result);
                }

            }
        }

        return $data;
    }

    public function getEnderecoTratado() {
        $endereco = "";

        if (count($this->enderecos)) {
            $obj = $this->enderecos[0];

            $endereco .= $obj->logradouro . ", ";
            $endereco .= $obj->numero;

            if (strlen($obj->complemento) > 0) {
                $endereco .= " (" . $obj->complemento . ")";
            }

            $endereco .= " - " . $obj->bairro;

            return $endereco;
        }

        return "Endereço não informado";
    }

    public function getEndereco() {
        return count($this->enderecos) > 0 ? $this->enderecos[0] : null;
    }

    public function getTelefone() {
        return count($this->telefones) > 0 ? $this->telefones[0] : null;
    }

    public function getTelefoneTratado() {
        $telefone = "";

        if (count($this->telefones) > 0) {
            $fone = $this->telefones[0];

            $telefone = $fone->numero;

            return $telefone;
        }

        return 'Sem telefone';
    }

    public function getCidade() {

        if (count($this->cidade) > 0) {
            return $this->cidade->nome . ' / ' . $this->cidade->estado->uf;
        }

        return 'Cidade não informada';
    }

    public function enderecos() {
        return $this->belongsToMany('App\Models\Endereco', 'usuario_tem_endereco', 'id_usuario', 'id_endereco');
    }

    public function telefones() {
        return $this->belongsToMany('App\Models\Telefone', 'usuario_tem_telefone', 'id_usuario', 'id_telefone');
    }

    public function servicos() {
        return $this->belongsToMany('App\Models\Servico', 'usuario_presta_servico', 'id_usuario', 'id_servico');
    }

    public function notificacoes() {
        return $this->hasMany('App\Models\Notificacao', 'id_destinatario')->whereNull('data_leitura');
    }

    public function simulacoes() {
        return $this->hasMany('App\Models\Checkout\Simulacao', 'usuario_id');
    }

    public function aprovacaoPendente() {
        return $this->aprovado == self::USUARIO_PENDENTE;
    }

    public function enviouDocumento($idTipo) {
        foreach ($this->documentos as $doc) {
            if ($doc->tipo == $idTipo) return true;
        }

        return false;
    }

    public function isAdmin() {
        return $this->id_perfil_conta == 3;
    }

    public function recebeuOferta($idOportunidade) {
        foreach ($this->ofertas as $oferta) {
            if ($oferta->id == $idOportunidade) return true;
        }

        return false;
    }

    public function solicitouServico($idOportunidade) {
        $servicos = Oportunidade::where('id_usuario_solicitante', '=', $this->id)->get();

        foreach ($servicos as $s) {
            if ($s->id == $idOportunidade) return true;
        }

        return false;
    }

    public function buscaResumo($idOportunidade) {
        $resumo = [];
        $servico = [];
        $servicos = [];
        $servicosAgenda = [];

        $totalOfertasRecebidas = count($this->ofertas);
        $totalOfertasPendentes = count($this->ofertasPendentes);
        $totalOfertasRecusadas = count($this->ofertasRecusadas);
        $totalOfertasAceitas = count($this->ofertasAceitas);
        $totaldeServicosCancelados = count($this->servicosCancelados);

        if ($totalOfertasRecebidas > 0) {
            $pctPendentes = ($totalOfertasPendentes/$totalOfertasRecebidas)*100;
            $pctAceitas = ($totalOfertasAceitas/$totalOfertasRecebidas)*100;
            $pctRecusadas = ($totalOfertasRecusadas/$totalOfertasRecebidas)*100;
        } else {
            $pctPendentes = 0;
            $pctAceitas = 0;
            $pctRecusadas = 0;
        }

        $resumo['nome_usuario'] = $this->nome;
        $resumo['avatar_usuario'] = $this->getAvatar();
        $resumo['id_usuario'] = $this->id;
        $resumo['eh_candidato'] = $this->recebeuOferta($idOportunidade);
        $resumo['total_ofertas_recebidas'] = $totalOfertasRecebidas;
        $resumo['total_ofertas_pendentes'] = $totalOfertasPendentes;
        $resumo['total_ofertas_recusadas'] = $totalOfertasRecusadas;
        $resumo['total_ofertas_aceitas'] = $totalOfertasAceitas;
        $resumo['pct_ofertas_pendentes'] = $pctPendentes;
        $resumo['pct_ofertas_aceitas'] = $pctAceitas;
        $resumo['pct_ofertas_recusadas'] = $pctRecusadas;
        $resumo['total_servicos_cancelados'] = $totaldeServicosCancelados;
        $resumo['total_servicos_aceitos'] = $totalOfertasAceitas;


        $servicosRealizados = Oportunidade::where('id_prestador_selecionado', '=', $this->id)->where('status', '=', Oportunidade::STATUS_ENCERRADO)->orderBy('id', 'desc')->take(5)->get();
        $proximosServicos = Oportunidade::where('id_prestador_selecionado', '=', $this->id)->where('status', '=', Oportunidade::STATUS_AGENDADO)->orderBy('id', 'desc')->take(5)->get();

        foreach ($servicosRealizados as $s) {
            $servico['id'] = $s->id;
            $servico['servico'] = $s->servico->descricao;
            $servico['bairro'] = $s->bairro;
            $servico['data_execucao'] = date("d/m/Y", strtotime($s->data_execucao));
            $servico['status'] = "Finalizado";

            array_push($servicos, $servico);
        }

        foreach ($proximosServicos as $s) {
            $servico['id'] = $s->id;
            $servico['servico'] = $s->servico->descricao;
            $servico['bairro'] = $s->bairro;
            $servico['data_execucao'] = date("d/m/Y", strtotime($s->data_execucao));


            if (strtotime($s->data_execucao) < date("Y-m-d H:i:s")) {
                $servico['status'] = "Iniciada";
            } else {
                $servico['status'] = "Não iniciada";
            }

            array_push($servicosAgenda, $servico);
        }

        $resumo['servicos'] = $servicos;
        $resumo['proximos_servicos'] = $servicosAgenda;

        return $resumo;
    }

    public function getAvatar() {
        if (!is_null($this->avatar)) {
            return $this->avatar;
        }
        return "/img/ninja.png";
    }

    public function buscaNotificacoes() {
        $notificacao = [];
        $data = [];

        foreach ($this->notificacoes as $n) {
            $notificacao['id'] = $n->id;
            $notificacao['mensagem'] = $n->mensagem;
            $notificacao['data_cadastro'] = date("d/m/Y H:i:s", strtotime($n->data_cadastro));

            array_push($data, $notificacao);
        }

        return $data;
    }

    public function buscaNotificacoesDash() {
        $notificacoes = Notificacao::where('id_destinatario', '=', $this->id)->orderBy('id', 'desc')->take(5)->get();
        $data = [];
        $notificacao = [];

        foreach ($notificacoes as $n) {
            $notificacao['id'] = $n->id;
            $notificacao['msg'] = $n->mensagem;
            $notificacao['data'] = date("d/m/Y H:i:s", strtotime($n->data_cadastro));

            if (!is_null($n->data_leitura)) {
                $notificacao['lida'] = true;
            } else {
                $notificacao['lida'] = false;
            }

            array_push($data, $notificacao);
        }

        return $data;
    }

    public function buscaServicosDash() {
        $data = [];
        $servico = [];

        $proximosServicos = Oportunidade::where('id_prestador_selecionado', '=', $this->id)->where('status', '=', Oportunidade::STATUS_AGENDADO)->orderBy('id', 'desc')->take(5)->get();
        foreach ($proximosServicos as $s) {
            $servico['id'] = $s->id;
            $servico['servico'] = $s->servico->descricao;
            $servico['bairro'] = $s->bairro;
            $servico['data_execucao'] = date("d/m/Y", strtotime($s->data_execucao));

            $dataExecucao = date("Y-m-d", strtotime($s->data_execucao));
            if ($dataExecucao < date("Y-m-d")) {
                $servico['status'] = "Iniciada";
            } else {
                $servico['status'] = "Não iniciada";
            }

            array_push($data, $servico);
        }

        return $data;
    }

    public function buscaAvaliacoes() {
        $servicos = Oportunidade::where('id_prestador_selecionado','=', $this->id)->where('status','=',Oportunidade::STATUS_ENCERRADO)->get();
        $data = [];
        $registro = [];

        foreach ($servicos as $s) {
            if (!is_null($s->avaliacao)) {

                $registro['id'] = $s->id;
                $registro['servico'] = $s->servico->descricao;
                $registro['endereco'] = $s->getEndereco();
                $registro['nota'] = $s->avaliacao->nota;
                $registro['comentarios'] = $s->avaliacao->comentarios;
                $registro['tomador'] = $s->usuario->nome;
                $registro['avatar_tomador'] = $s->usuario->getAvatar();

                array_push($data, $registro);
            }
        }

        return $data;
    }

    public function getMediaAvaliacoes() {
        if ($this->perfil->id == 1) {
            return $this->media_avaliacoes ? $this->media_avaliacoes : 0;
        } else {
            return $this->media_avaliacoes_tomador ? $this->media_avaliacoes_tomador : 0;
        }
    }

    public function mediaAvaliacoes() {
        $servicos = Oportunidade::where('id_prestador_selecionado', '=', $this->id)->where('status','=',Oportunidade::STATUS_ENCERRADO)->get();
        $media = 0;
        $total = count($servicos);

        foreach ($servicos as $s) {
            if ($s->avaliacaoPrestador) {
                $media += $s->avaliacaoPrestador[0]->nota;
            }
        }

        if ($total == 0) return 0;

        return round($media/$total);
    }

    public function mediaAvaliacoesTomador() {
        $servicos = Oportunidade::where('id_usuario_solicitante', '=', $this->id)->where('status','=',Oportunidade::STATUS_ENCERRADO)->get();
        $media = 0;
        $total = count($servicos);

        foreach ($servicos as $s) {
            if ($s->avaliacaoTomador) {
                $media += $s->avaliacaoTomador[0]->nota;
            }
        }

        if ($total == 0) return 0;

        return round($media/$total);
    }

    public function buscaResumoConta() {
        $data = [];
        $usuario = [];
        $cidade = Cidade::find($this->id_cidade);
        $estado = Estado::find($cidade->id_estado);

        $usuario['nome'] = $this->nome;
        $usuario['email'] = $this->email;
        $usuario['avatar'] = $this->getAvatar();

        if (count($this->enderecos) > 0) {
            $usuario['cep'] = $this->enderecos[0]->cep;

            if (!is_null($this->enderecos[0]->complemento)) {
                $usuario['endereco'] = $this->enderecos[0]->logradouro . " , " . $this->enderecos[0]->numero . " - " . $this->enderecos[0]->complemento;
            } else {
                $usuario['endereco'] = $this->enderecos[0]->logradouro . " , " . $this->enderecos[0]->numero;
            }
            $usuario['bairro'] = $this->enderecos[0]->bairro;
            $usuario['cidade'] = $cidade->nome . "/" . $estado->uf . " - " . $usuario['cep'];
        }

        if (count($this->telefones) > 0) {
            $usuario['telefone'] = $this->telefones[0]->numero;
        }

        array_push($data, $usuario);

        return $data;
    }

    public static function filtraPrestadores($request) {
        $obj = DB::table('usuario as u')
            ->leftjoin('usuario_tem_endereco as ue', 'ue.id_usuario', '=', 'u.id')
            ->leftjoin('usuario_presta_servico as ups', 'ups.id_usuario', '=', 'u.id')
            ->leftjoin('usuario_tem_telefone as ut', 'ut.id_usuario', '=', 'u.id')
            ->leftjoin('endereco as e', 'e.id', '=', 'ue.id_endereco')
            ->leftjoin('telefone as t', 't.id', '=', 'ut.id_telefone')
            ->leftjoin('cidade as c', 'c.id', '=', 'u.id_cidade')
            ->leftjoin('servico as s', 's.id', '=', 'ups.id_servico')
            ->selectRaw("u.id as id_usuario, u.nome, u.media_avaliacoes, c.id,  IF(u.avatar IS NULL, '/img/ninja.png', u.avatar) AS avatar, u.email, e.*, t.numero as numero_telefone, GROUP_CONCAT(IF(s.descricao IS NOT NULL, s.descricao, 'Não Informado') SEPARATOR ', ') as servico_prestado")
            ->where('u.id_perfil_conta', '=', PerfilConta::PERFIL_PRESTADOR)
            ->where('u.aprovado', '=', User::USUARIO_APROVADO)
            ->groupBy('u.id')
            ->groupBy('u.nome')
            ->groupBy('u.email')
            ->groupBy('u.avatar')
            ->groupBy('u.media_avaliacoes')
            ->groupBy('c.id')
            ->groupBy('e.id')
            ->groupBy('e.logradouro')
            ->groupBy('e.cep')
            ->groupBy('e.numero')
            ->groupBy('e.bairro')
            ->groupBy('e.complemento')
            ->groupBy('t.id')
            ->groupBy('t.numero');
        //->where('u.aprovado', '=', User::USUARIO_APROVADO);
        //->toSql();

        if (!empty($request->input('nome'))) {
            $obj = $obj->where("u.nome", "like", '%'.$request->input('nome').'%');
        }

        if (!empty($request->input('cidade'))) {
            $obj->where("c.id", "=", $request->input('cidade'));
        }

        if (!empty($request->input('servicos'))) {
            $obj->where("ups.id_servico", "=", $request->input('servicos'));
        }

        $obj = $obj->paginate(20);

        if(count($obj) > 0)
            return $obj;
        return null;
    }

    /**
     * Verifica se o usuário logado já avaliou determinado serviço encerrado
     *
     * @param $idServico
     *
     * @return bool
     */
    public function avaliouServico($idServico) {
        $statusToFind = ($this->getPerfil() == 'Usuário') ? AvaliacaoServico::DESTINATARIO_PRESTADOR : AvaliacaoServico::DESTINATARIO_TOMADOR;

        $query = DB::table('avaliacao_servico')
                ->where('id_oportunidade', '=', $idServico)
                ->where('destinatario', '=', $statusToFind)
                ->get();

        return (count($query) > 0);
    }

    /**
     * Retorna o status de um determinado usuário em relação ao cadastro
     *
     * @return string
     */
    public function getStatus($adminWatching = false) {

        switch ($this->aprovado) {

            case User::USUARIO_PENDENTE:
                return $adminWatching ? 'Aguardando Aprovação' : 'Cadastro Pendente';

            case User::USUARIO_APROVADO:
                return 'Cadastro Aprovado';

            case User::USUARIO_REPROVADO:
                return 'Cadastro Rejeitado';

            case User::USUARIO_BLOQUEADO:
                return 'Usuário Bloqueado';

        }
    }

    /**
     * Retorna a quantidade de serviços que o usuário está envolvido com um status específico
     *
     * @return int
     */
    public function getQuantidadeServicos($status) {

        $query = DB::table('oportunidade')->where('status', '=', $status);

        switch ($this->getPerfil()) {
            case 'Admin':
                $query = $query->get();
                break;

            case 'Usuário':
                $query = $query->where('id_usuario_solicitante', '=', $this->id)->get();
                break;

            case 'Prestador':
                $query = $query->where('id_prestador_selecionado', '=', $this->id)->get();
                break;
        }

        return count($query);
    }

    /**
     * Retorna uma lista de serviços solicitados que o pagamento ainda não foi identificado
     *
     * @return array
     */
    public function servicosAguardandoPagamento() {
        $servicos = DB::table('oportunidade')
            ->join('servico', 'servico.id', '=', 'oportunidade.id_servico')
            ->where('status', '=', Oportunidade::STATUS_AGUARDANDO_PAGAMENTO)
            ->where('id_usuario_solicitante', '=', $this->id)
            ->selectRaw('servico.descricao, oportunidade.id, oportunidade.valor_total, oportunidade.valor_4w, oportunidade.link_mercado_pago, oportunidade.bairro')->get();

        $itens = [];

        foreach ($servicos as $item) {
            $data['id'] = $item->id;
            $data['valor'] = "R$ " . number_format($item->valor_total, 2, ',', '.');
            $data['valor_prestador'] = "R$ " . number_format(($item->valor_total - $item->valor_4w), 2, ',', '.');
            $data['bairro'] = $item->bairro;
            $data['descricao'] = $item->descricao;
            $data['link'] = $item->link_mercado_pago;

            array_push($itens, $data);
        }

        return $itens;
    }

    /**
     * Retorna uma lista de serviços pendentes de avaliação
     *
     * @return array
     */
    public function servicosPendentesAvaliacao() {
        $query = DB::table('oportunidade')
            ->join('servico', 'servico.id', '=', 'oportunidade.id_servico')
            ->where('status', '=', Oportunidade::STATUS_ENCERRADO)
            ->selectRaw('servico.descricao, oportunidade.id, oportunidade.valor_total, oportunidade.valor_4w, oportunidade.bairro');

        $itens = [];

        switch ($this->getPerfil()) {

            case 'Usuário':
                $query->where('id_usuario_solicitante', '=', $this->id);
                $query->whereNotIn('oportunidade.id', function ($query) {
                    $query->select('id_oportunidade')
                        ->from('avaliacao_servico')
                        ->whereRaw('avaliacao_servico.id_oportunidade = oportunidade.id and destinatario = ' . AvaliacaoServico::DESTINATARIO_PRESTADOR);
                });
                break;

            case 'Prestador':
                $query->where('id_prestador_selecionado', '=', $this->id);
                $query->whereNotIn('oportunidade.id', function ($query) {
                    $query->select('id_oportunidade')
                        ->from('avaliacao_servico')
                        ->whereRaw('avaliacao_servico.id_oportunidade = oportunidade.id and destinatario = ' . AvaliacaoServico::DESTINATARIO_TOMADOR);
                });
                break;
        }

        $servicos = $query->get();

        foreach ($servicos as $item) {
            $data['id'] = $item->id;
            $data['valor'] = "R$ " . number_format($item->valor_total, 2, ',', '.');
            $data['valor_prestador'] = "R$ " . number_format(($item->valor_total - $item->valor_4w), 2, ',', '.');
            $data['bairro'] = $item->bairro;
            $data['descricao'] = $item->descricao;

            array_push($itens, $data);
        }

        return $itens;
    }

    /**
     * Retorna uma lista de serviços agendados
     *
     * @return array
     */
    public function servicosAgendados() {
        $query = DB::table('oportunidade')
            ->join('servico', 'servico.id', '=', 'oportunidade.id_servico')
            ->where('status', '=', Oportunidade::STATUS_AGENDADO)
            ->selectRaw('servico.descricao, oportunidade.id, oportunidade.valor_total, oportunidade.valor_4w, oportunidade.bairro');

        $itens = [];

        switch ($this->getPerfil()) {

            case 'Usuário':
                $query->where('id_usuario_solicitante', '=', $this->id);
                break;

            case 'Prestador':
                $query->where('id_prestador_selecionado', '=', $this->id);
                break;
        }

        $servicos = $query->get();

        foreach ($servicos as $item) {
            $data['id'] = $item->id;
            $data['valor'] = "R$ " . number_format($item->valor_total, 2, ',', '.');
            $data['valor_prestador'] = "R$ " . number_format(($item->valor_total - $item->valor_4w), 2, ',', '.');
            $data['bairro'] = $item->bairro;
            $data['descricao'] = $item->descricao;

            array_push($itens, $data);
        }

        return $itens;
    }

    /**
     * Retorna uma lista de serviços que a administração está procurando um profissional para executá-lo
     *
     * @return array
     */
    public function servicosSelecionandoProfissional() {
        $query = DB::table('oportunidade')
            ->join('servico', 'servico.id', '=', 'oportunidade.id_servico')
            ->where('status', '=', Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL)
            ->selectRaw('servico.descricao, oportunidade.id, oportunidade.valor_total, oportunidade.valor_4w, oportunidade.bairro');

        if ($this->getPerfil() == 'Usuário') {
            $query->where('id_usuario_solicitante', '=', $this->id);
        }

        $itens = [];

        $servicos = $query->get();

        foreach ($servicos as $item) {
            $data['id'] = $item->id;
            $data['valor'] = "R$ " . number_format($item->valor_total, 2, ',', '.');
            $data['valor_prestador'] = "R$ " . number_format(($item->valor_total - $item->valor_4w), 2, ',', '.');
            $data['bairro'] = $item->bairro;
            $data['descricao'] = $item->descricao;

            array_push($itens, $data);
        }

        return $itens;
    }

    /**
     * Retorna uma lista de serviços pendentes de aprovação por parte do prestador
     *
     * @return array
     */
    public function servicosAguardandoConfirmacao() {
        $query = DB::table('oportunidade')
            ->leftJoin('servico', 'servico.id', '=', 'oportunidade.id_servico')
            ->leftJoin('oportunidade_tem_candidato', 'oportunidade_tem_candidato.id_oportunidade', '=', 'oportunidade.id')
            ->where('oportunidade.status', '=', Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL)
            ->where('oportunidade_tem_candidato.status', '=', OportunidadeTemCandidato::STATUS_CONVIDADO)
            ->selectRaw('servico.descricao, oportunidade.id, oportunidade.valor_total, oportunidade.valor_4w, oportunidade.bairro')
            ->groupBy('oportunidade.id')
            ->groupBy('servico.descricao')
            ->groupBy('oportunidade.valor_total')
            ->groupBy('oportunidade.valor_4w')
            ->groupBy('oportunidade.bairro');

        $itens = [];

        if ($this->getPerfil() == 'Prestador') {
            $query->where('oportunidade_tem_candidato.id_usuario', '=', $this->id);
        }

        $servicos = $query->get();

        foreach ($servicos as $item) {
            $data['id'] = $item->id;
            $data['valor'] = "R$ " . number_format($item->valor_total, 2, ',', '.');
            $data['valor_prestador'] = "R$ " . number_format(($item->valor_total - $item->valor_4w), 2, ',', '.');
            $data['bairro'] = $item->bairro;
            $data['descricao'] = $item->descricao;

            array_push($itens, $data);
        }

        return $itens;
    }

    /**
     * Retorna uma lista de serviços pendentes de avaliação
     *
     * @return array
     */
    public function servicosAguardandoEncerramento() {
        $query = DB::table('oportunidade')
            ->join('servico', 'servico.id', '=', 'oportunidade.id_servico')
            ->where('status', '=', Oportunidade::STATUS_AGENDADO)
            ->where('oportunidade.id_prestador_selecionado', '=', $this->id)
            ->where('oportunidade.data_execucao', '<=', 'now()')
            ->whereNull('oportunidade.data_encerramento')
            ->selectRaw('servico.descricao, oportunidade.id, oportunidade.valor_total, oportunidade.valor_4w, oportunidade.bairro');

        $itens = [];

        $servicos = $query->get();

        foreach ($servicos as $item) {
            $data['id'] = $item->id;
            $data['valor'] = "R$ " . number_format($item->valor_total, 2, ',', '.');
            $data['valor_prestador'] = "R$ " . number_format(($item->valor_total - $item->valor_4w), 2, ',', '.');
            $data['bairro'] = $item->bairro;
            $data['descricao'] = $item->descricao;

            array_push($itens, $data);
        }

        return $itens;
    }

    /**
     * Retorna uma lista de usuários prestadores pendentes de aprovação
     *
     * @return array
     */
    public function usuariosPendentesAprovacao() {
        $usuarios = DB::table('usuario')
            ->where('aprovado', '=', User::USUARIO_PENDENTE)
            ->where('id_perfil_conta', '=', PerfilConta::PERFIL_PRESTADOR)->get();

        $itens = [];

        foreach ($usuarios as $item) {
            $data['id'] = $item->id;
            $data['nome'] = $item->nome;
            $data['data_cadastro'] = date("d/m/Y", strtotime($item->data_cadastro));

            array_push($itens, $data);
        }

        return $itens;
    }

    public function qtdOportunidadesRecebidas() {
        $query = DB::table('oportunidade_tem_candidato')->where('id_usuario', '=', $this->id)->get();

        return count($query);
    }

    public function qtdOportunidadesAceitas() {
        $query = DB::table('oportunidade_tem_candidato')
                    ->where('id_usuario', '=', $this->id)
                    ->where('status', '=', OportunidadeTemCandidato::STATUS_ACEITO)
                    ->get();

        return count($query);
    }

    public function qtdOportunidadesPerdidas() {
        $recebidas = $this->qtdOportunidadesRecebidas();
        $aceitas = $this->getQuantidadeServicos(Oportunidade::STATUS_ENCERRADO);

        return (int) $recebidas - (int) $aceitas;
    }

    public function getTotalRecebido() {
        $totalServicos = DB::table('oportunidade')
            ->where('id_prestador_selecionado', '=', $this->id)
            ->where('status', '=', Oportunidade::STATUS_ENCERRADO)
            ->selectRaw('sum(oportunidade.valor_total - oportunidade.valor_4w) as total')
            ->first();

        if ($totalServicos) {
            $total = "R$ " . number_format(($totalServicos->total), 2, ',', '.');
        } else {
            $total = "R$ 0,00";
        }

        return $total;
    }

    public function unreadNotifications() {
        $notifications = DB::table('notificacao')
            ->where('id_destinatario', '=', $this->id)
            ->whereNull('data_leitura')
            ->selectRaw('*')
            ->take(5)->get();

        return $notifications;
    }

    /**
     * Atualiza ou cria um telefone e vincula em um usuário
     *
     * @params string $number
     * @return boolean
     */
    public function updateOrCreatePhone($number) {

        if (empty($relation = UsuarioTemTelefone::findByUser($this->id))) {

            $phone = new Telefone();
            $phone->numero = $number;

            if (!$phone->save()) {
                return false;
            }

            $relation = new UsuarioTemTelefone();
            $relation->id_usuario = $this->id;
            $relation->id_telefone = $phone->id;
        }

        // Get the existent phone number
        $phone = Telefone::find($relation->id_telefone);

        if ($phone) {
            $phone->numero = $number;

            if (!$phone->save()) {
                return false;
            }

        }

        if (!$relation->save()) {
            return false;
        }

        return true;
    }

    /**
     * Atualiza ou cria um endereço e vincula em um usuário
     *
     * @params array $params
     * @return boolean
     */
    public function updateOrCreateLocation($params) {

        if (empty($relation = UsuarioTemEndereco::where('id_usuario', '=', $this->id)->first())) {

            $address              = new Endereco();
            $address->cep         = $params['cep'];
            $address->logradouro  = $params['logradouro'];
            $address->numero      = $params['numero'];
            $address->complemento = $params['complemento'];
            $address->bairro      = $params['bairro'];

            if (!$address->save()) {
                return false;
            }

            $relation              = new UsuarioTemEndereco();
            $relation->id_usuario  = $this->id;
            $relation->id_endereco = $address->id;
        }

        // Get the existent address
        $address = Endereco::find($relation->id_endereco);

        if ($address) {

            $address->cep         = $params['cep'];
            $address->logradouro  = $params['logradouro'];
            $address->numero      = $params['numero'];
            $address->complemento = $params['complemento'];
            $address->bairro      = $params['bairro'];

            if (!$address->save()) {
                return false;
            }

        }

        if (!$relation->save()) {
            return false;
        }

        return true;
    }

    /**
     * Verifica se um usuário já cancelou um
     * serviço específico.
     *
     * @params int $id
     * @return boolean
     */
    public function cancelouServico($id) {

        if (empty($id)) {
            return false;
        }

        try {

            $alreadyBeenCanceled = UsuarioCancelouServico
                ::where('id_usuario',      '=', $this->id)
                ->where('id_oportunidade', '=', $id)
                ->count();

        } catch (\Exception $e) {
            dd($e->getMessage());
        }


        return $alreadyBeenCanceled
            ? true
            : false;
    }
}
