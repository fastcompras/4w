<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Servico extends Model
{

    protected $table   = "servico";
    protected $dates   = ['data_cadastro'];
    public $timestamps = false;

    public function usuarios()
    {
        return $this->belongsToMany('App\Models\User', 'usuario_presta_servico', 'id_servico', 'id_usuario');
    }

    public static function getAvailableServices($params = [])
    {

        if (empty($params)) {
            return [];
        }

        $query = Servico::where('servico.descricao', 'like',  '%' . $params['term']. '%')
            ->join('servico_base', 'servico.id', '=', 'servico_base.id_servico')
            ->join('servico_base_agravante', 'servico_base.id', '=', 'servico_base_agravante.id_base_servico')
            ->orderBy('servico.descricao')
            ->groupBy('servico.id', 'servico.descricao')
            ->skip($params['offset'])
            ->take($params['limit'])
            ->get(['servico.id', DB::raw('servico.descricao as text')]
        );

        return $query;
    }
}
