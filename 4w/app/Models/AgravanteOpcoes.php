<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgravanteOpcoes extends Model
{
    protected $table = "agravante_tem_opcoes";
    public $timestamps = false;

    public function agravante()
    {
        return $this->belongsTo('App\Models\ServicoBaseAgravante', 'id_servico_base_agravante');
    }

}
