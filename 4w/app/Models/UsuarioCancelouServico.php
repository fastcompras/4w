<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioCancelouServico extends Model
{
    protected $table = "usuario_cancelou_servico";
    public $timestamps = false;
    protected $dates = ['data_cancelamento'];
}
