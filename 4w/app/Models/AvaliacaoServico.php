<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AvaliacaoServico extends Model
{
    protected $table = "avaliacao_servico";
    public $timestamps = false;

    const DESTINATARIO_TOMADOR = 1;
    const DESTINATARIO_PRESTADOR = 2;

    public function oportunidade()
    {
        return $this->belongsTo('App\Models\Oportunidade', 'id_oportunidade');
    }
}
