<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    protected $table = 'estado';
    public $timestamps = false;

    public function cidades()
    {
        $this->hasMany('App\Models\Cidade', 'id_estado', 'id');
    }
}
