<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SugestaoServico extends Model
{
    const SUGESTAO_PENDENTE = 0;
    const SUGESTAO_APROVADA = 1;
    const SUGESTAO_REJEITADA = 2;

    protected $table = "sugestao_servico";
    public $timestamps = false;
    protected $dates = ['data_cadastro'];

    public function usuario()
    {
        return $this->belongsTo('App\Models\User', 'id_usuario');
    }

    public function getStatus()
    {
        switch ($this->aprovado) {
            case 0:
                return "Pendente";
            break;
            case 1:
                return "Aprovado";
                break;
            case 2:
                return "Rejeitado";
                break;
        }
    }
}
