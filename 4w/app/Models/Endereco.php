<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Endereco extends Model
{
    protected $table = 'endereco';
    public $timestamps = false;

    public function usuarios()
    {
        return $this->belongsToMany('App\Models\Usuario', 'usuario_tem_endereco', 'id_endereco', 'id_usuario');
    }
}
