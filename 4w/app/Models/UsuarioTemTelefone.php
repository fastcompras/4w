<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioTemTelefone extends Model
{
    protected $table = 'usuario_tem_telefone';
    public $timestamps = false;

    public function find($primaryOne, $PrimaryTwo)
    {
        return UsuarioTemTelefone::where('id_usuario', '=', $primaryOne)
            ->where('id_telefone', '=', $PrimaryTwo)
            ->first();
    }

    public static function findByUser($userId)
    {
        return UsuarioTemTelefone::where('id_usuario', '=', $userId)->first();
    }
}
