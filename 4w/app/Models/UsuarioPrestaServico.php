<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioPrestaServico extends Model
{
    protected $table = "usuario_presta_servico";
    public $timestamps = false;
    protected $dates = ['data_cadastro'];
}
