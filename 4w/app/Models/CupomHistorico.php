<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CupomHistorico extends Model
{
	protected $table = 'cupom_historico';
	protected $fillable = ['id_cupom', 'id_oportunidade'];

	public $timestamps = false;

	public function save(array $options = [])
	{
		$this->data_cadastro = date('Y-m-d H:i:s');

		parent::save($options);
	}

	public function cupom()
	{
		return $this->belongsTo('App\Models\Cupom', 'id_cupom');
	}

	public function oportunidade()
	{
		return $this->belongsTo('App\Models\Oportunidade', 'id_oportunidade');
	}
}
