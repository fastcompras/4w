<?php

namespace App\Models\Checkout;

use Illuminate\Database\Eloquent\Model;

class SimulacaoOpcao extends Model
{
    protected $table = "opcoes_simulacao";

    /**
     * Disable timestamps
     *
     * @var bool
    */
    public $timestamps = false;

    protected $fillable = [
        'simulacao_id',
        'agravante_id',
        'agravante_value'
    ];

    public function simulacao()
    {
        return $this->belongsTo('App\Models\Checkout\Simulacao', 'simulacao_id');
    }

}
