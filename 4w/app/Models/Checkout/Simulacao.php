<?php

namespace App\Models\Checkout;

use Illuminate\Database\Eloquent\Model;

class Simulacao extends Model
{
    protected $table = "simulacoes";

    protected $fillable = [
        'servico_id',
        'servico_base_id',
        'nome',
        'email',
        'cep',
        'telefone',
        'empresa',
        'usuario_id'
    ];

    public function usuario()
    {
        return $this->belongsTo('App\Models\Usuario', 'usuario_id');
    }

    public function opcoes()
    {
        return $this->hasMany('App\Models\Checkout\SimulacaoOpcao', 'simulacao_id', 'id');
    }

}
