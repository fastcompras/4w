<?php

namespace App\Models;

use App\Models\Enums\TipoDescontoCupom;
use App\Models\Enums\TipoRegraCupom;
use Illuminate\Database\Eloquent\Model;

class Cupom extends Model
{
	protected $table   = 'cupom';
	protected $fillable = [
		'codigo',
		'qtd_disponivel',
		'desconto',
		'tipo_desconto',
		'tipo_regra',
		'data_expiracao',
		'data_cadastro'
	];
	public $timestamps = false;

	public function save(array $options = [])
	{
		$this->data_cadastro = date('Y-m-d H:i:s');

		parent::save($options);
	}

	public function canBeUsed()
	{
		if ($this->ruleIsByAvailableQuantity()) {
			return $this->isAvailable();
		}

		if ($this->ruleIsByExpirationDate()) {
			return ! $this->isExpired();
		}

		return $this->isAvailable() && ! $this->isExpired();
	}

	public function decreaseQuantity()
	{
		$this->qtd_disponivel = $this->qtd_disponivel - 1;
		$this->save();
	}

	public function discountIsAbsolute()
	{
		return $this->tipo_desconto == TipoDescontoCupom::ABSOLUTE;
	}

	public function discountIsPercentage()
	{
		return $this->tipo_desconto == TipoDescontoCupom::PERCENTAGE;
	}

	public function historico()
	{
		return $this->hasMany('App\Models\CupomHistorico', 'id_cupom', 'id');
	}

	public function isAvailable()
	{
		return $this->qtd_disponivel > 0;
	}

	public function isExpired()
	{
		return date('Y-m-d H:i:s') > $this->data_expiracao;
	}

	public function getDiscountAmount()
	{
		return round($this->desconto, 2);
	}

	public function ruleIsByExpirationDate()
	{
		return $this->tipo_regra == TipoRegraCupom::VALIDATE_BY_EXPIRATION_DATE;
	}

	public function ruleIsByAvailableQuantity()
	{
		return $this->tipo_regra == TipoRegraCupom::VALIDATE_BY_AVAILABLE_QUANTITY;
	}

	public function ruleIsByExpirationDateAndAvailableQuantity()
	{
		return $this->tipo_regra == TipoRegraCupom::VALIDATE_BY_DATE_AND_QUANTITY;
	}
}
