<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsuarioTemEndereco extends Model
{
    protected $table = 'usuario_tem_endereco';
    public $timestamps = false;
}
