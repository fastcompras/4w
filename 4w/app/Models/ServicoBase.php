<?php

namespace App\Models;

use App\Helpers\FormHelper;
use http\Env\Response;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Exception\HttpResponseException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class ServicoBase extends Model
{
    protected $table       = "servico_base";
    protected $dates       = ['data_cadastro'];
    public $timestamps     = false;
    public $rulesToPersist = [
        'nome' => 'required|min:2',
        'id_servico' => 'required',
        'valor' => 'required'
    ];

    public function servico()
    {
        return $this->belongsTo('App\Models\Servico', 'id_servico');
    }

    public function agravantes()
    {
        return $this->hasMany('App\Models\ServicoBaseAgravante', 'id_base_servico', 'id')->orderBy('ordem_formulario');
    }

    public function getAggravating()
    {
        $result = [];

        foreach ($this->agravantes as $aggr) {
            array_push($result, $aggr->getData());
        }

        array_multisort(array_column($result, 'order'), SORT_ASC, $result);

        return $result;
    }

    public function geraAgravantes($id = null)
    {
        $code = (count($this->agravantes) > 0) ? FormHelper::createHrLine() : "";

        if ($id && (count($this->agravantes) > 0)) {
            $itens = OportunidadeItem::where('id_oportunidade','=',$id)->get();

            foreach ($itens as $item) {
                if ($item->agravante->tipoCampo() == "text") {
                    $code .= FormHelper::createInputText($item->agravante->id, $item->agravante, $item->quantidade);
                } else if ($item->agravante->tipoCampo() == "checkbox") {
                    $code .= FormHelper::createCheckbox($item->agravante->id, $item->agravante, $item->quantidade);
                } else if ($item->agravante->tipoCampo() == "radio") {
                    $code .= FormHelper::createRadioBoxSelected($item->agravante->id, $item->agravante, $item->id_agravante_opcao);
                }
            }
        } else {
            foreach ($this->agravantes as $registro) {
                if ($registro->tipoCampo() == "text") {
                    $code .= FormHelper::createInputText($registro->id, $registro);
                } else if ($registro->tipoCampo() == "checkbox") {
                    $code .= FormHelper::createCheckbox($registro->id, $registro);
                } else if ($registro->tipoCampo() == "radio") {
                    $code .= FormHelper::createRadioBox($registro->id, $registro);
                }
            }
        }

        return $code;
    }

    public function estimateCost($aggravating = [])
    {

        $total = (float) $this->valor;

        if (empty($aggravating)) {
            return round($total, 2);
        }

        foreach ($aggravating as $index => $obj) {


	        if (empty($item = ServicoBaseAgravante::find($obj['id']))) {
		        continue;
	        }

	        if ($item->tipoCampo() == "radio") {

                if (empty($option = AgravanteOpcoes::find($obj['value']))) {
	                throw new NotFoundResourceException("Option not found with this id: " . $obj['value']);
                }

                if ($option->agravante->regra == ServicoBaseAgravante::VALOR_SIMPLES) {
                    $total += $this->valor * ($option->valor / 100);
                } else {
                    $total += $total * ($option->valor / 100);
                }

            } else {

                $value = ($item->incluso_servico_base) ? ($obj['value'] - 1) : $obj['value'];

                if ($value > 0) {

                    if ($item->regra == ServicoBaseAgravante::VALOR_SIMPLES) {
                        $total += ($value) * ($this->valor * ($item->valor / 100));
                    } else {
                        $total += ($value) * ($total * ($item->valor / 100));
                    }

                }

            }

        }

        return round($total, 2);
    }

    public static function getAvailableBaseServices($params = [])
    {
        $data  = [];
        $index = 0;

        if (empty($params)) {
            return $data;
        }

        if (! empty($params['id'])) {

            $query = ServicoBase::where('servico_base.id_servico', '=',  $params['id'])
                ->select('servico_base.*')
                ->orderBy('servico_base.descricao')
                ->get();

        } else {
            $query = ServicoBase::pluck('nome', 'id');
        }

        foreach ($query as $base) {

            $data[$index]['id']          = $base->id;
            $data[$index]['text']        = $base->nome;
            $data[$index]['value']       = $base->valor;
            $data[$index]['description'] = $base->descricao;

            $index++;
        }

        return $data;
    }
}
