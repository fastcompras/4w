<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Documento extends Model
{
    const TIPO_RG = 1;
    const TIPO_CPF = 2;
    const TIPO_RESIDENCIA = 3;
    const TIPO_ANTECEDENTES = 4;
    const TIPO_AVATAR = 5;
    const TIPO_CERTIFICADO = 6;
    const TIPO_MEI = 7;

    protected $table = 'documento';
    public $timestamps = false;
    protected $dates = ['data_cadastro'];

    public function usuarios()
    {
        return $this->belongsToMany('App\Models\Usuario', 'usuario_tem_documento', 'id_documento', 'id_usuario');
    }

    public function getTipo()
    {
        switch ($this->tipo) {
            case self::TIPO_RG:
                return "Carteira de Identidade";
            break;

            case self::TIPO_CPF:
                return "Cadastro de Pessoa Física";
                break;

            case self::TIPO_RESIDENCIA:
                return "Comprovante de Residência";
            break;

            case self::TIPO_ANTECEDENTES:
                return "Antecedentes Criminais";
            break;

            case self::TIPO_CERTIFICADO:
                return "Certificados/Diplomas";
                break;

            case self::TIPO_MEI:
                return "Documento de Microempreendedor Individual";
                break;

            case self::TIPO_AVATAR:
                return "Avatar";
                break;
        }
    }
}
