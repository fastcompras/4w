<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notificacao extends Model
{
    protected $table = "notificacao";
    public $timestamps = false;
    protected $dates = ['data_cadastro', 'data_leitura'];


    public static function novaOportunidade($idOportunidade)
    {
        $usuario = User::find(User::ID_ADMIN);
        $oportunidade = Oportunidade::find($idOportunidade);

        if (!is_null($usuario) && !is_null($oportunidade)) {
            $link = "/oportunidades/visualizar/{$oportunidade->id}";

            $notificacao = new Notificacao();
            $notificacao->id_destinatario = $usuario->id;
            $notificacao->mensagem = "O usuário {$oportunidade->usuario->nome} criou uma nova oportunidade.";
            $notificacao->link = $link;
            $notificacao->data_cadastro = date("Y-m-d H:i:s");

            $notificacao->save();
        }
    }

    public static function prestadorAceitou($idOportunidade)
    {
        $usuario = User::find(User::ID_ADMIN);
        $oportunidade = Oportunidade::find($idOportunidade);

        if (!is_null($usuario) && !is_null($oportunidade)) {
            $link = "/oportunidades/visualizar/{$oportunidade->id}";

            $notificacao = new Notificacao();
            $notificacao->id_destinatario = $usuario->id;
            $notificacao->mensagem = "O prestador {$oportunidade->prestador->nome} aceitou o serviço que lhe foi oferecido.";
            $notificacao->link = $link;
            $notificacao->data_cadastro = date("Y-m-d H:i:s");

            $notificacao->save();
        }
    }

    public static function prestadorRejeitou($idOportunidade, $prestador)
    {
        $usuario = User::find(User::ID_ADMIN);
        $oportunidade = Oportunidade::find($idOportunidade);

        if ($usuario && $oportunidade) {
            $link = "/oportunidades/visualizar/{$oportunidade->id}";

            $notificacao = new Notificacao();
            $notificacao->id_destinatario = $usuario->id;
            $notificacao->mensagem = "O prestador {$prestador->nome} rejeitou o serviço que lhe foi oferecido.";
            $notificacao->link = $link;
            $notificacao->data_cadastro = date("Y-m-d H:i:s");

            $notificacao->save();
        }
    }

    public static function prestadorEncontrado($idOportunidade, $idUsuario)
    {
        $usuario = User::find($idUsuario);
        $oportunidade = Oportunidade::find($idOportunidade);

        if (!is_null($usuario) && !is_null($oportunidade)) {
            $link = "/oportunidades/visualizar/{$oportunidade->id}";

            $notificacao = new Notificacao();
            $notificacao->id_destinatario = $usuario->id;
            $notificacao->mensagem = "O profissional {$oportunidade->prestador->nome} foi selecionado para prestar o serviço que você solicitou.\n";
            $notificacao->mensagem .= "Enviaremos um e-mail com mais informações.";
            $notificacao->data_cadastro = date("Y-m-d H:i:s");

            $notificacao->save();
        }
    }

    public static function prestadorSelecionado($idOportunidade, $idUsuario)
    {
        $usuario = User::find($idUsuario);
        $oportunidade = Oportunidade::find($idOportunidade);

        if (!is_null($usuario) && !is_null($oportunidade)) {
            $link = "/oportunidades/visualizar/{$oportunidade->id}";

            $notificacao = new Notificacao();
            $notificacao->id_destinatario = $usuario->id;
            $notificacao->mensagem = "Você foi convidado para uma oportunidade de serviço.\n";
            $notificacao->mensagem .= "Aguardamos o seu retorno para darmos continuidade ao processo.";
            $notificacao->link = $link;
            $notificacao->data_cadastro = date("Y-m-d H:i:s");

            $notificacao->save();
        }
    }

    public static function prestadorDesvinculado($idOportunidade, $idUsuario)
    {
        $usuario = User::find($idUsuario);
        $oportunidade = Oportunidade::find($idOportunidade);

        if (!is_null($usuario) && !is_null($oportunidade)) {
            $link = "/oportunidades/visualizar/{$oportunidade->id}";

            $notificacao = new Notificacao();
            $notificacao->id_destinatario = $usuario->id;
            $notificacao->mensagem = "Desculpe! Por algum motivo precisamos desvincular você de uma oportunidade.\n";
            $notificacao->link = $link;
            $notificacao->data_cadastro = date("Y-m-d H:i:s");

            $notificacao->save();
        }
    }
}
