<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OportunidadeItem extends Model
{
    protected $table = 'oportunidade_item';
    public $timestamps = false;
    protected $dates = ['data_cadastro'];

    public function agravante()
    {
        return $this->belongsTo('App\Models\ServicoBaseAgravante', 'id_agravante');
    }

    public function opcao()
    {
        return $this->belongsTo('App\Models\AgravanteOpcoes', 'id_agravante_opcao');
    }
}
