<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreCupomRequest extends FormRequest
{
	public function authorize()
    {
	    if (Auth::user() && Auth::user()->isAdmin()) {
		    return true;
	    }

	    return true;
    }

    public function rules()
    {
        return [
        	'codigo' => 'required|string',
            'qtd_disponivel' => 'numeric|between:1,999',
            'desconto' => 'required|numeric',
	        'tipo_desconto' => 'required|numeric|between:1,2',
	        'tipo_regra' => 'required|numeric|between:1,3',
	        'data_expiracao' => 'required|date_format:Y-m-d H:i:s'
        ];
    }

    public function validated()
    {
    	return $this->validate() === null;
    }
}
