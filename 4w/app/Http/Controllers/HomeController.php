<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Redireciona o usuário para sua dashboard
     *
     * Request:
     * --------
     *
     * GET /home/index
     *
     */
    public function index()
    {
        $usuario = Auth::user();

        if ($usuario->getPerfil() == 'Admin') {
            $view = "home/admin/index";
        } elseif ($usuario->getPerfil() == 'Usuário') {
            $view = "home/tomador/index";
        } else {

            if ($usuario->temDocumentosPendentes()) {
                $view = "home/prestador/index_pendente";
            } else {
                $view = "home/prestador/index";
            }

        }

        return view($view);
    }

    /**
     * Retorna se determinado usuário possui documentação pendente
     *
     * Request:
     * --------
     *
     * POST /home/verificaDocumentacaoPendente
     *
     * Response:
     * ---------
     * 200 - Ok
     *
     */
    public function verificaDocumentacaoPendente()
    {
        $user                = Auth::user();
        $documentosPendentes = $user->temDocumentosPendentes();

        $response = [
            'usuario_pendente'      => $user->aprovacaoPendente(),
            'documentacao_pendente' => (Auth::user()->isPrestador()) ? $documentosPendentes : false,
        ];

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
