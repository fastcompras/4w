<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Checkout\Simulacao;
use App\Models\Servico;
use App\Models\ServicoBase;
use App\Models\User;
use App\Models\Cidade;
use App\Models\Financeiro;
use App\Models\Oportunidade;
use App\Models\OportunidadeItem;
use App\Models\Cupom;
use App\Models\Notificacao;

use Log;

class ApiExecuteCheckout extends Controller
{

    public function prevalidate(Request $request)
    {
        try {
            $this->validateInput($request);
        } catch (\Exception $err) {
            return response()->json([
                "error" => true,
                "message" => $err->getMessage()
            ]);
        }
        return response()->json([
            "validated" => true
        ]);
    }
    // API links:
    // https://www.mercadopago.com.br/developers/pt/guides/payments/api/receiving-payment-by-card/

    // Testing cards:
    // https://www.mercadopago.com.br/developers/pt/guides/payments/api/testing/

    // MercadoPago Account (dilvane@sou4w.com.br)
    // https://www.mercadopago.com.br/activities

    private function executePaymentRequest(Oportunidade $oportunidade, User $paymentUser, $installments, $paymentMethodId, $issuerId) {
        \MercadoPago\SDK::setAccessToken(env('MERCADO_PAGO_ACCESS_TOKEN'));

        $transactionAmount = $oportunidade->valor_total;
        if (!($transactionAmount >= 0)) {
            throw new \Exception("A transação não pode ter um valor negativo");
        }
        if( strtotime("2020-08-01") < time() ){
            // No log after the date
        } else {
            // Your date is in the future, so log normally:
            Log::info("Execução de pagamento foi chamado com o preço '".$transactionAmount."', utilizando o access token: '".env('MERCADO_PAGO_ACCESS_TOKEN')."'");
        }

        $idToken = $oportunidade->id_pagamento_mp;
        if (strlen($idToken) !== 32) {
            Log::info("Atenção: A oportunidade id ".$oportunidade->id." tem ".strlen($idToken)." caracteres de token");
        }

        $payment = new \MercadoPago\Payment();
        $payment->transaction_amount = $oportunidade->valor_total;
        $payment->token = $idToken;
        $payment->description = 'Solicitação de Prestação de Serviço de ' . $oportunidade->servico->descricao;
        $payment->installments = $installments;
        $payment->payment_method_id = $paymentMethodId ? $paymentMethodId : "master";
        $payment->issuer_id = $issuerId;
        $payment->payer = [ "email" => $paymentUser->email ];
        $payment->save();

        if ($payment->error) {
            $errMessage = (string) $payment->error;
            if (strpos($errMessage, "The customer can't be equal to the collector") !== false) {
                throw new \Exception("O CPF/CNPJ informado é igual ao CPF/CNPJ que recebe o pagamento e isto não é permitido pelo MercadoPago");
            }
            if ($errMessage === "bad_request: Card Token not found") {
                throw new \Exception("O serviço de pagamento informou que ocorreu um erro:\n2006 - 'Card Token' não encontrado.\nPor favor, reinicie a página e tente novamente.\nSe o problema persistir, utilize outro cartão.");
            }
            if ($errMessage === "bad_request: security_code_id can't be null") {
                throw new \Exception("O serviço de pagamento informou que devido a um erro anterior, o campo 'security_code_id' não pode ser encontrado no pedido e é necessário que você reinicie a página e tente enviar seu pedido novamente.");
            }
            Log::info("Payment 1 Export: " . var_export($payment, true));
            Log::info("Payment 1 message: " . var_export($errMessage, true));
            Log::info("Payment 1 Status: " . var_export($payment->status, true));
            Log::info("Payment 1 Error: " . var_export($payment->error, true));
            if (strlen($errMessage) <= 1) {
                $errMessage = "Erro sem dados no serviço de pagamento";
            }
            throw new \Exception($errMessage);
        }

        $status = $payment->status;
        $statusDetail = $payment->status_detail;

        if ($status !== "approved" && $status !== "accredited" && $status !== "in_process") {
            Log::info("Payment 2 Export: " . var_export($payment, true));
            Log::info("Payment 2 Status: " . var_export($payment->status, true));
            Log::info("Payment 2 Error: " . var_export($payment->error, true));
            $errMessage = trim($statusDetail);
            // API responses:
            // https://www.mercadopago.com.br/developers/pt/guides/payments/api/handling-responses
            $errMap = [
                "cc_rejected_other_reason" => "O seu serviço de pagamento (".$payment->payment_method_id.") não processou seu pagamento",
                "cc_rejected_other_reasons" => "O seu serviço de pagamento (".$payment->payment_method_id.") não processou seu pagamento",
                "cc_rejected_max_attempts" => "Você atingiu o limite de tentativas permitido.\nEscolha outra forma de pagamento",
                "cc_rejected_insufficient_amount" => "O serviço de pagamento verificou que o cartão tem saldo insuficiente",
                "cc_rejected_high_risk" => "O seu pagamento foi recusado, entre em contato com seu banco",
                "cc_rejected_duplicated_payment" => "O pagamento já foi feito anteriormente",
                "cc_rejected_card_disabled" => "Seu cartão se encontra desativo para compras na internet.\nLigue para o número no verso do seu cartão ou vá até uma agência para ativa-lo",
                "cc_rejected_call_for_authorize" => "Seu cartão se encontra desativo para compras na internet.\nLigue para o número no verso do seu cartão ou vá até uma agência para ativa-lo",
                "cc_rejected_bad_filled_card_number" => "Sistema de pagamentos informa:\nConfira o número do cartão",
                "cc_rejected_bad_filled_date" => "Sistema de pagamentos informa:\nConfira a data de validade",
                "cc_rejected_bad_filled_other" => "Sistema de pagamentos informa:\nDados de cartão inválidos. Confira os dados de pagamento",
                "cc_rejected_bad_filled_security_code" => "Confira o código de segurança",
            ];
            if (array_key_exists($errMessage, $errMap)) {
                $errMessage = $errMap[$errMessage];
            }
            if (strlen($errMessage) <= 3) {
                $errMessage = $statusDetail;
            }
            if (strlen($errMessage) <= 3) {
                $errMessage = "Erro de pedido em status ".$status;
            }
            throw new \Exception($errMessage);
        }

        if ($status === "in_process" && $statusDetail === "pending_contingency") {
            $statusDetail = "Estamos processando o pagamento. Em até 2 dias úteis informaremos por e-mail o resultado";
        }

        if ($status === "in_process" && $statusDetail === "pending_review_manual") {
            $statusDetail = "Estamos processando o pagamento. Em até 2 dias úteis informaremos por e-mail se foi aprovado ou se precisamos de mais informações";
        }

        if( strtotime("2020-08-01") < time() ){
            // No log after the date
        } else {
            // Your date is in the future, so log normally:
            Log::info("Execução de pagamento completou com status ".var_export($status, true)." e detalhe ".var_export($statusDetail, true).".");
        }
        return [
            "status" => $status,
            "status_detail" => $statusDetail
        ];
    }

    public function requestGiftCard(Request $request) {
        $user = $request->input("email");
        $password = $request->input("password");
        $giftCardCode = $request->input("code");

        return response()->json([
            "success" => true,
            "code" => 1
        ], 200, [], JSON_PRETTY_PRINT);
    }

    public function execute(Request $request)
    {
        $this->validateInput($request);

        Log::info('Requisição de finalização de compra recebida: '.json_encode($request->input()));

        // get inputs
        $scheduling = $request->input("scheduling");
        $email = $request->input("email");
        $password = $request->input("password");
        $ccNumber = $request->input("ccNumber");
        $ccName = $request->input("ccName");
        $ccInstallment = $request->input("ccInstallment");
        $ccIssuerId = $request->input("ccIssuerId");
        $paymentMethodId = $this->getPaymentMethodIdFromRequest($request);
        $cardType = $paymentMethodId;
        $total = $request->input("total");
        $paymentLibraryResult = $request->input("paymentLibraryResult");
        $schedulingInfo = $request->input("schedulingInfo");
        $zipCode = $request->input("zipCode");
        $streetName = $request->input("street");
        $streetNumber = $request->input("number");
        $complement = $request->input("complement");
        $neighborhood = $request->input("neighborhood");
        $scheduling = $request->input("scheduling");
        $format = 'd/m/Y H:i';
        $dataExecucao = \DateTime::createFromFormat($format, $scheduling);
        $cityId = self::getCityIdByName($request->input("city"));

        // Payment data and id must exist
        $paymentInfo = $request->input("paymentInfo");
        if (!isset($paymentInfo) || !$paymentInfo || strlen($paymentInfo) <= 3 || $paymentInfo[0] !== "{") {
            throw new \Exception("Informações do pagamento estão inválidas ou ausentes");
        }
        $paymentInfo = json_decode($paymentInfo, true);
        if (!$paymentInfo) {
            throw new \Exception("Não foi possível interpretar as informações do pagamento");
        }
        if (!is_array($paymentInfo) || !array_key_exists("id", $paymentInfo)) {
            throw new \Exception("Não foi possível identificar o id nas informações do pagamento");
        }
        if (!$paymentMethodId || !is_string($paymentMethodId) || empty($paymentMethodId)) {
            throw new \Exception("O identificador do método de pagamento é inválido");
        }

        // get user
        $user = User::where('email', '=', $email)->first();
        $hasher = app('hash');
        if (!$user || !$hasher->check($password, $user->password)) {
            throw new \Exception("Erro de usuário");
        }

        // get service identifiers
        $service = $request->input("service");
        $service = json_decode($service, true);
        $simul = Simulacao::find($service["details"]["simulation-id"]);
        if (!$simul) {
            throw new \Exception("Seu pedido de solicitação de serviço identificado pelo numero ".$service["details"]["simulation-id"]." não foi encontrada ou foi removida e seu pedido não pode continuar");
        }

        $configuracao = Financeiro::all()->first();
        $percentual = ($configuracao) ? ($configuracao->percentual_sobre_servico / 100) : 0.12;

        // Salva a oportunidade
        $oportunidade = new Oportunidade();
        $oportunidade->id_usuario_solicitante = $user->id;
        $oportunidade->id_servico = $simul->servico_id;
        $oportunidade->id_cidade = $cityId;
        $oportunidade->id_servico_base = $simul->servico_base_id;
        $oportunidade->informacoes_adicionais = $schedulingInfo;
        $oportunidade->cep = $zipCode;
        $oportunidade->logradouro = $streetName;
        $oportunidade->numero = $streetNumber;
        $oportunidade->complemento = $complement;
        $oportunidade->bairro = $neighborhood;
        $oportunidade->sugestao_servico = "";
        $oportunidade->status = Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL;
        $oportunidade->status_pagamento_mp = "pending";
        $oportunidade->data_execucao = $dataExecucao;
        $oportunidade->data_cadastro = new \DateTime(null, new \DateTimeZone("America/Sao_Paulo"));
        $oportunidade->id_pagamento_mp = $paymentInfo["id"];

        $oportunidade->valor_total = floatval($total);
        $oportunidade->valor_4w = $oportunidade->valor_total * $percentual;
        if (!$oportunidade->save()) {
            throw new \Exception("Não foi possível salvar seu pedido no banco de dados");
        }

        $orderId = 1000000+$oportunidade->id;

        // Aplica o cupom se ele existe
        $couponId = $request->input("coupon");
        if (! empty($coupon = Cupom::where('codigo', $couponId)->first())) {
            $oportunidade->id_cupom = $cupom->id;
            $oportunidade->valor_total = $oportunidade->hasAvailableCupom() ? $oportunidade->getTotalWithDiscountCoupon() : $oportunidade->valor_total;
            $oportunidade->useCoupon();
            $oportunidade->valor_4w = $oportunidade->valor_total * $percentual;
            if (!$oportunidade->save()) {
                $oportunidade->delete();
                throw new \Exception("Não foi possível adicionar o cupom no seu pedido devido a um erro no banco de dados");
            }
        }

        // Salva os itens de oportunidade
        if (array_key_exists("options", $service) && count($service["options"]) > 0) {
            foreach ($service["options"] as $option) {
                if (!$option || !is_array($option) || !array_key_exists("option-type", $option)) {
                    continue;
                }
                $type = $option["option-type"];
                $id = $option["option-id"];
                $value = array_key_exists("option-value", $option) ? $option["option-value"] : "-1";
                $item = new OportunidadeItem();
                $item->id_oportunidade = $oportunidade->id;
                $item->id_servico = $oportunidade->id_servico;
                $item->id_base = $oportunidade->id_servico_base;
                $item->id_agravante = $id;
                $item->id_agravante_opcao = $value;
                $item->quantidade = 1;
                $item->data_cadastro = new \DateTime(null, new \DateTimeZone("America/Sao_Paulo"));
                if (!$item->save()) {
                    $oportunidade->delete();
                    throw new \Exception("Ocorreu uma falha ao salvar um item de sua lista de oportunidades no banco de dados.");
                }
            }
        }

        // Notificacao::novaOportunidade($oportunidade->id);

        /*
        $item = array(
            'id' => $oportunidade->id,
            'title' => 'Solicitação de Prestação de Serviço de ' . $oportunidade->servico->descricao,
            'quantity' => 1,
            'currency_id' => "BRL",
            'unit_price' => (float) number_format($oportunidade->valor_total, 2),
            "picture_url" => "https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
            "description" => $oportunidade->getResumo(),
        );
        */

        $paymentResult = null;
        try {
            $paymentResult = $this->executePaymentRequest($oportunidade, $user, intval($ccInstallment), $paymentMethodId, $ccIssuerId);
        } catch (\Exception $err) {
            $oportunidade->delete();
            Log::info("Erro no pagamento: ".$err->getMessage()."\nStack:\n".$err->getTraceAsString());

            // Trata o erro para melhor mostrar ao usuário
            $errMessage = (string) $err->getMessage();
            if (substr($errMessage, 0, 13) === "bad_request: ") {
                $errMessage = substr($errMessage, 13);
            }

            if (strtolower($errMessage) === "invalid card_token_id") {
                return response()->json([
                    "error" => true,
                    "message" => "O identificador do seu cartão é inválido (erro de token). Reinicie a página."
                ]);
            }
            if (strtolower($errMessage) === "cannot infer payment method") {
                return response()->json([
                    "error" => true,
                    "message" => "O sistema de pagamento não conseguiu identificar o tipo do seu cartão. Revise os dados de pagamento e tente novamente"
                ]);
            }

            return response()->json([
                "error" => true,
                "message" => strlen($errMessage) <= 1 ? "Erro desconhecido no sistema de pagamento" : $errMessage
            ]);
        }

        $cpfCnpj = $request->input("cpfcnpj");

        if (!$paymentResult || !is_array($paymentResult) || !array_key_exists("status", $paymentResult)) {
            $oportunidade->delete();
            Log::info("Pagamento sem informações normais de ".$email.", CPF/CNPJ ".$cpfCnpj.", tipo do cartão: ". $paymentMethodId." - ".var_export($paymentResult, true));
            return response()->json([
                "error" => true,
                "message" => $errMessage
            ]);
        }
        if ($paymentResult["status"] !== "approved" && $paymentResult["status"] !== "in_process") {
            $oportunidade->delete();

            Log::info("Pagamento rejeitado de ".$email.", CPF/CNPJ ".$cpfCnpj.", tipo do cartão: ". $paymentMethodId);

            return response()->json([
                "error" => true,
                "message" => "O serviço de pagamento rejeitou o seu pedido. Revise os dados de pagamento e tente novamente"
            ]);
        }

        if ($paymentResult["status"] === "approved") {
            if ($oportunidade->status !== Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL) {
                $oportunidade->status = Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL;
                $oportunidade->save();
            }
        } else if ($paymentResult["status"] === "in_process" || $paymentResult["status"] === "in_progress") {
            if ($oportunidade->status !== Oportunidade::STATUS_AGUARDANDO_PAGAMENTO) {
                $oportunidade->status = Oportunidade::STATUS_AGUARDANDO_PAGAMENTO;
                $oportunidade->save();
            }
        } else {
            if ($oportunidade->status !== Oportunidade::STATUS_REJEITADO) {
                $oportunidade->status = Oportunidade::STATUS_REJEITADO;
                $oportunidade->save();
            }
        }

        $successInfo = [
            "id" => $orderId,
            "scheduling" => $scheduling,
            "email" => $email,
            "payment" => [
                "method" => "credit-card",
                "card-type" => $cardType,
                "card-name" => $ccName,
                "installments" => $ccInstallment,
                "total" => $total,
                "result" => $paymentResult
            ],
            "price" => [
                [
                    "label" => "Desconto",
                    "value" => 0
                ],
                [
                    "label" => "Valor Total",
                    "value" => $total
                ]
            ]
        ];

        try {
            $stages = $request->input("stages");
            if ($stages[0] === "[") {
                $stages = json_decode($stages, true);
                $this->updateDataFile($request->input("injectedDataFile"), $stages, $successInfo);
            } else {
                Log::info("Invalid stages input: ".$stages);
            }
        } catch (\Exception $err) {
            Log::info("Error while updating injected data file: ".$err->getMessage()." - ".$err->getTraceAsString());
        }

        return response()->json([
            "success" => $successInfo
        ], 200, [], JSON_PRETTY_PRINT);
    }

    private function getPaymentMethodIdFromRequest(Request $request) {
        $paymentMethodId = $request->input("ccPaymentMethodId");
        if ($paymentMethodId) {
            return (string) $paymentMethodId;
        }
        $paymentMethodId = $request->input("paymentMethodId");
        if ($paymentMethodId) {
            return (string) $paymentMethodId;
        }
        $paymentMethodId = $request->input("paymentLibraryResult");
        if ($paymentMethodId && $paymentMethodId[0] === "{") {
            $paymentMethodId = json_decode($paymentMethodId, true);
            if($paymentMethodId && is_array($paymentMethodId) && array_key_exists("id", $paymentMethodId)) {
                $paymentMethodId = $paymentMethodId["id"];
                if ($paymentMethodId) {
                    return (string) $paymentMethodId;
                }
            }
        }
        return null;
    }

    private function updateDataFile($dataFile, $stagesInfo, $successInfo) {
        if (!$dataFile || strlen($dataFile) <= 0) {
            throw new \Exception("Invalid data file parameter");
        }
        $fileDir = "/home/wcom4/public_html/checkout/simulations/";
        $filePath = $fileDir.$dataFile;
        if (!file_exists($filePath)) {
            throw new \Exception("Missing data file from simulations folder");
        }
        $fileData = file_get_contents($filePath);
        if (!$fileData || strlen($fileData) <= 3 || $fileData[0] !== "{") {
            throw new \Exception("Data file has missing or invalid data");
        }
        $fileObject = json_decode($fileData, true);
        if (!$fileObject) {
            throw new \Exception("Could not interpret data file");
        }
        if (!array_key_exists("simulation-details", $fileObject)) {
            throw new \Exception("Data file object is missing simulation details");
        }
        $fileObject["success"] = $successInfo;
        $stageArray = is_string($stagesInfo) ? json_decode($stagesInfo, true) : $stagesInfo;
        if (is_array($stagesInfo) && array_key_exists("stages", $fileObject) && is_array($fileObject["stages"]) && count($fileObject["stages"]) < $stagesInfo) {
            $fileObject["stages"] = $stagesInfo;
        }
        $fileData = json_encode($fileObject);
        if (!file_put_contents($filePath, $fileData)) {
            throw new \Exception("Could not update file data");
        }
        return true;
    }

    private function validateInput(Request $request)
    {
        // Email
        $email = $request->input("email");
        if (!$email || !is_string($email) || strlen($email) < 6) {
            throw new \Exception("email inválido");
        }

        // Password
        $password = $request->input("password");
        if (!$password || !is_string($password) || strlen($password) < 6) {
            throw new \Exception("password inválido");
        }

        $user = User::where('email', '=', $email)->first();

        if (!$user) {
            throw new \Exception("usuário não encontrado");
        }

        $hasher = app('hash');
        if (!$hasher->check($password, $user->password)) {
            throw new \Exception("senha incorreta");
        }

        // cpfCnpj
        $cpfcnpj = $request->input("cpfcnpj");
        if (!$cpfcnpj || !is_string($cpfcnpj) || (strlen($cpfcnpj) != 11 && strlen($cpfcnpj) != 14)) {
            throw new \Exception("CPF/CNPJ inválido");
        }
        // Schedule
        $scheduling = $request->input("scheduling");
        if (!$scheduling || !is_string($scheduling)) {
            throw new \Exception("horario de agendamento é obrigatório");
        }
        if (strlen($scheduling) < 6) {
            throw new \Exception("horario de agendamento muito curto (precisa ter mais do que os ".strlen($scheduling)." caracteres)");
        }

        $format = 'd/m/Y H:i';
        $date = \DateTime::createFromFormat($format, $scheduling);
        if (!$date) {
            throw new \Exception("horario de agendamento não pôde ser interpretado no formato esperado (dd/mm/YYYY HH:mm)");
        }
        $ts = $date->getTimestamp();
        if ($ts <= 0) {
            throw new \Exception("horario de agendamento fora do limite possível");
        }
        $t1 = $date;
        if ($t1->format("d/m/Y H:i") !== $scheduling) {
            throw new \Exception("formato de agendamento inválido");
        }
        $t2 = new \DateTime();

        $seconds = $t1->getTimestamp() - $t2->getTimestamp();
        $hours = $seconds / ( 60 * 60 );
        if ($hours <= 47 || $hours > 17000) {
            throw new \Exception("data fora do limite para efetuar pedido (".number_format($hours, 1, ',', '.').")");
        }

        // Address
        $zipCode = $request->input("zipCode");
        if (!$zipCode || !is_string($zipCode) || strlen($zipCode) < 6) {
            throw new \Exception("zip code inválido");
        }
        $street = $request->input("street");
        if (!$street || !is_string($street) || strlen($street) < 6) {
            throw new \Exception("nome da rua inválido");
        }
        $number = $request->input("number");
        if (!$number || !is_string($number) || strlen($number) < 1) {
            throw new \Exception("número da rua inválido");
        }
        $neighborhood = $request->input("neighborhood");
        if (!$neighborhood || !is_string($neighborhood) || strlen($neighborhood) < 6) {
            throw new \Exception("bairro inválido");
        }
        $state = $request->input("state");
        if (!$state || !is_string($state) || strlen($state) < 2) {
            throw new \Exception("estado inválido");
        }
        // Credit Card
        $ccNumber = $request->input("ccNumber");
        if (!$ccNumber || !is_string($ccNumber) || strlen($ccNumber) < 10) {
            throw new \Exception("numero do cartão inválido");
        }
        $ccName = $request->input("ccName");
        if (!$ccName || !is_string($ccName) || strlen($ccName) < 2) {
            throw new \Exception("nome no cartão inválido");
        }
        $ccInstallment = $request->input("ccInstallment");
        if (!$ccInstallment || !is_string($ccInstallment) || empty($ccInstallment)) {
            throw new \Exception("parcelamento inválido");
        }
        if (!ctype_digit($ccInstallment)) {
            throw new \Exception("parcelamento com caracteres inválidos");
        }
        $installments = intval($ccInstallment);
        if ($installments <= 0 || $installments >= 13) {
            throw new \Exception("parcelamento fora do limite aceitável");
        }
        $ccIssuerId = $request->input("ccIssuerId");
        if (!$ccIssuerId || !is_string($ccIssuerId) || empty($ccIssuerId)) {
            throw new \Exception("identificador do cartão inválido");
        }
        // City
        $city = $request->input("city");
        if (!$city || !is_string($city) || strlen($city) < 6) {
            throw new \Exception("cidade inválida");
        }
        $cityId = self::getCityIdByName($city);
        if (!$cityId) {
            throw new \Exception("Uma cidade com o nome de \"".$city."\" não foi encontrada em nosso banco de dados");
        }
        // Service
        $service = $request->input("service");
        $total = $request->input("total");
        if (!isset($service) || !$service || strlen($service) <= 3 || $service[0] !== "{") {
            throw new \Exception("Informações do serviço solicitado estão inválidas ou ausentes");
        }
        $service = json_decode($service, true);
        if (!$service) {
            throw new \Exception("Informações do serviço solicitado não estão em um formato esperado");
        }
        if (!array_key_exists("details", $service)) {
            throw new \Exception("Informações do serviço solicitado não contem detalhes específicos");
        }
        if (!array_key_exists("simulation-id", $service["details"])) {
            throw new \Exception("Informações do serviço solicitado não contem o identificados de sua simulação");
        }
        $simul = Simulacao::find($service["details"]["simulation-id"]);
        if (!$simul) {
            throw new \Exception("Seu pedido de solicitação de serviço identificado pelo numero ".$service["details"]["simulation-id"]." não foi encontrada ou foi removida e seu pedido não pode continuar");
        }

        // Price seen matches expected
        $realPrice = self::estimateCostFromSimulacao($simul);
        if (!$total) {
            throw new \Exception("Preço esperado total faltando nos dados");
        }
        if (is_string($total)) {
            $total = floatval($total);
        }
        if ($total < 6 || $realPrice < 6) {
            throw new \Exception("Há um problema com o preço do seu pedido (".number_format($total, 2, ',', '.').") e ele não pôde ser aprovado. Por favor reinicie o seu pedido.");
        }
        if (abs($total - $realPrice) >= 0.02) {
            throw new \Exception("Pedimos desculpas mas verificamos que o preço do pedido estava incorreto, o correto é R$ ".number_format($realPrice, 2, ',', '.').". Por favor reinicie o seu pedido para resolver este problema.");
        }
    }

    private static function getCityIdByName($name)
    {
        static $lastName = null;
        static $lastId = null;
        if ($lastName === $name) {
            return $lastId;
        }
        $lastName = $name;

        $cityName = (string) $name;
        $upperCity = strtoupper($cityName);

        // Perfect match
        $perfectMatchList = Cidade::whereRaw("UPPER(nome) = '". $upperCity ."'")->get();

        if (count($perfectMatchList) >= 1) {
            $lastId = $perfectMatchList[0]->id;
            return $lastId;
        }

        // Like match
        $likeMatchList = Cidade::whereRaw("UPPER(nome) LIKE '". $upperCity ."%'")->get();

        if (count($likeMatchList) === 1) {
            $lastId = $likeMatchList[0]->id;
            return $lastId;
        }

        // Find by levenshtein distance (heavy computional-wise but good at finding cities by close approximations)
        $resultList = [];

        $everyCity = Cidade::all();

        foreach($everyCity as $city) {
            if (!$city || !$city->nome) {
                continue;
            }
            $value = levenshtein(strtoupper($city->nome), $upperCity);
            if ($value < 6) {
                $resultList[$value] = $city->id; //["id" => $city->id, "nome" => $city->nome];
            }
        }
        for($i = 0; $i < 6; $i++) {
            if (isset($resultList[$i]) && $resultList[$i]) {
                $lastId = $resultList[$i];
                return $lastId;
            }
        }
        $lastId = null;
        return $lastId;
    }

    // Recalculate cost function to check if the user had the correct price for his service.
    private static function estimateCostFromSimulacao(Simulacao $simul) {
        static $result = null;
        if (!$result) {
            $aggravating = [];
            $simulationOptions = $simul->opcoes()->get();
            foreach ($simulationOptions as $simulationOption) {
                array_push($aggravating, [
                    "id" => $simulationOption->agravante_id,
                    "value" => $simulationOption->agravante_value
                ]);
            }

            $baseId = $simul->servico_base_id;
            $base = ServicoBase::find($baseId);
            $result = $base->estimateCost($aggravating);
        }
        return $result;
    }
}