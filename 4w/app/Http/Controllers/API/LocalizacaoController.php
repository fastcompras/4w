<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class LocalizacaoController extends Controller
{

    public function findAddressByCep(Request $request)
    {
        $params['cep'] = $request->input('cep', "000000000");

        try {
            $response = @file_get_contents('http://cep.republicavirtual.com.br/web_cep.php?formato=json&cep=' . $params['cep']);

            $response = json_decode($response);

        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
        }

        // This service returns a string "0" in this field
        if (! (int) $response->resultado) {
            return response()->json([], 422, [], JSON_PRETTY_PRINT);
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

}