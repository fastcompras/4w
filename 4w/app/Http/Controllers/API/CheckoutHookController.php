<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Log;

class CheckoutHookController extends Controller
{

    private function json($responseObject, $code = 200)
    {
        return response()->json($responseObject, $code, [], JSON_PRETTY_PRINT);
    }

    public function index(Request $request)
    {
        $data = $request->input();
        Log::info("Checkout Hook Received: ".var_export($data, true));
        return $this->json(["success" => true]);
    }
}
