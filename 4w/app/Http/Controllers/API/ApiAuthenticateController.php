<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Cidade;
use App\Models\Servico;
use App\Models\SugestaoServico;
use App\Models\User;
use App\Models\Endereco;
use App\Models\PerfilConta;
use App\Models\Telefone;
use App\Models\Documento;
use App\Models\UsuarioPrestaServico;
use App\Models\UsuarioTemEndereco;
use App\Models\UsuarioTemDocumento;
use App\Models\UsuarioTemTelefone;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Session;
use Auth;
use Validator;
use Mail;
use Log;

class ApiAuthenticateController extends Controller
{
    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('guest');
    }

    private function reply($responseObject, $responseHttpCode = 200) {
        return response()->json($responseObject, $responseHttpCode, [], JSON_PRETTY_PRINT);
    }

    private function createServiceTakerUser($name, $email, $password, $cpfcnpj, $telephone)
    {
        $user = new User();
        $user->nome = $name;
        $user->email = $email;
        $user->password = bcrypt($password);
        $user->id_perfil_conta = PerfilConta::PERFIL_TOMADOR;
        $user->id_cidade = null;
        $user->aprovado = User::USUARIO_PENDENTE;
        $user->cpfcnpj = $cpfcnpj;
        $user->data_cadastro = new \DateTime(null, new \DateTimeZone('America/Sao_Paulo'));

        if ($telephone && strlen($telephone) > 6 && strlen($telephone) < 20) {
            $tel = new Telefone();
            $tel->id_usuario = $user->id;
            $tel->numero = $telephone;

            try {
                if (!$tel->save()) {
                    return null;
                }
            } catch (\Exception $err) {
                Log::error('Não foi possível salvar o telefone devido a um erro interno: '.$err->getMessage());
                return $user;
            }

            $telRelation = new UsuarioTemTelefone();
            $telRelation->id_usuario = $user->id;
            $telRelation->id_telefone = $tel->id;

            try {
                if (!$telRelation->save()) {
                    $tel->delete();
                    Log::error('Não foi possível salvar o telefone devido a um erro interno desconhecido');
                    return $user;
                }
            } catch (\Exception $err) {
                try {
                    $tel->delete();
                } catch (\Exception $err2) {
                    Log::error('Não foi possível excluir a entrada de telefone depois que o salvamento da relação do telefone falhou: '.json_encode($tel->toArray(), JSON_PRETTY_PRINT));
                }
                Log::error('Não foi possível salvar o telefone devido a um erro interno: '.$err->getMessage());
                return $user;
            }
        }

        return $user;
    }

    public function loginServiceTaker(Request $request)
    {
        $rules = [
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ];

        $validator = $this->validator(Input::all(), $rules);

        if ($validator->fails()) {
            return $this->reply([
                'error' => true,
                'type' => 'validation',
                'message' => 'A validação do login falhou, por favor preencha todos os campos.',
                'details' => $validator->messages()
            ], 400);
        }

        $user = User::where('email', '=', Input::get('email'))->first();

        if (!$user) {
            return $this->reply([
                'error' => true,
                'type' => 'doesnt-exists',
                'message' => 'Um usuário com este email não existe'
            ], 400);
        }

        $hasher = app('hash');
        if (!$hasher->check(Input::get('password'), $user->password)) {
            return $this->reply([
                'error' => true,
                'type' => 'password',
                'message' => "A senha informada não confere com a que foi cadastrada."
            ], 400);
        }

        $result = [
            'success' => true,
            'user_id' => $user->id,
            'cpfcnpj' => $user->cpfcnpj,
            'email' => $user->email
        ];

        try {
            $endereco = $user->getEndereco();
            if ($endereco) {
                $result["address"] = [
                    "street" => $endereco->logradouro,
                    "zipCode" => $endereco->cep,
                    "number" => $endereco->numero,
                    "complement" => $endereco->complemento,
                    "neighborhood" => $endereco->bairro
                ];
            } else {
                $result["address"] = false;
            }
        } catch (\Exception $err) {
            $result["address-error"] = $err->getMessage();
        }

        return $this->reply($result);
    }

    public function registerServiceTaker(Request $request)
    {
        $rules = [
            'firstName' => 'required|min:3|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
            'cpfcnpj' => 'required',
        ];

        $validator = $this->validator(Input::all(), $rules);

        if ($validator->fails()) {
            return $this->reply([
                'error' => true,
                'type' => 'validation',
                'message' => 'A validação do registro falhou, por favor preencha todos os campos.',
                'details' => $validator->messages()
            ], 400);
        }

        if (User::where('email', '=', Input::get('email'))->count() > 0) {
            return $this->reply([
                'error' => true,
                'type' => 'already-exists',
                'message' => 'Este email já foi cadastrado previamente. Efetue o Login'
            ], 400);
        }

        $cpfcnpj = Input::get('cpfcnpj');
        if (strlen($cpfcnpj) !== 11 && strlen($cpfcnpj) !== 14) {
            return $this->reply([
                'error' => true,
                'type' => 'validation',
                'message' => 'Seu CPF ou CNPJ tem um tamanho incorreto',
                'details' => $validator->messages()
            ], 400);
        }

        try {
            $user = $this->createServiceTakerUser(
                Input::get('firstName').' '.Input::get('lastName'),
                Input::get('email'),
                Input::get('password'),
                $cpfcnpj,
                Input::get('telephone')
            );
            if (!$user) {
                return $this->reply([
                    'error' => true,
                    'type' => 'save-failed',
                    'message' => 'Tentativa de criação de usuario retornou nulo.'
                ], 400);
            }
        } catch (\Exception $err) {
            return $this->reply([
                'error' => true,
                'type' => 'save-failed',
                'message' => 'Não foi possível criar seu usuário devido a um erro interno: '.$err->getMessage()
            ], 400);
        }

        try {
            if (!$user->save()) {
                return $this->reply([
                    'error' => true,
                    'type' => 'save-failed',
                    'message' => 'Não foi possível salvar seu usuário, tente novamente mais tarde'
                ], 400);
            }
        } catch (\Exception $err) {
            return $this->reply([
                'error' => true,
                'type' => 'save-failed',
                'message' => 'Não foi possível criar seu usuário devido a um erro interno: '.$err->getMessage()
            ], 400);
        }

        Session::put('user_id', $user->id);

        try {
            Mail::send('emails.tomadores.cadastro-recebido', ['tomador' => $user->toArray(), 'title' => 'Cadastro Realizado'], function($message) use ($user) {
                $message->to($user->email);
                $message->subject('Cadastro Realizado');
            });
        } catch (\Exception $err) {
            Log::error('Falha ao enviar mensagem de cadastro realizado para o usuário: '.json_encode($user->toArray(), JSON_PRETTY_PRINT)."\n\nMensagem de erro: " . $err->getMessage() . "\n\nStack: " . $err->getTraceAsString());
        }

        return $this->reply([
            'success' => true,
            'user_id' => $user->id,
            'cpfcnpj' => $user->cpfcnpj,
            'email' => $user->email
        ]);
    }

    private function addTelephoneToUser(User $user, $telephone)
    {
        if (!$user || $user->id_perfil_conta !== PerfilConta::PERFIL_TOMADOR) {
            return [
                'error' => true,
                'type' => 'missing-user',
                'message' => 'Não há um usuário válido'
            ];
        }

        $telephone = new Telefone();
        $telephone->numero = $telephone;

        try {
            if (!$telephone->save()) {
                return [
                    'error' => true,
                    'type' => 'save-failed',
                    'message' => 'Não foi possível salvar o telefone, tente novamente mais tarde'
                ];
            }
        } catch (\Exception $err) {
            return [
                'error' => true,
                'type' => 'save-failed',
                'message' => 'Não foi possível salvar o telefone devido a um erro interno: '.$err->getMessage()
            ];
        }

        $telephoneRelation = new UsuarioTemTelefone();
        $telephoneRelation->id_usuario = $user->id;
        $telephoneRelation->id_telefone = $telephone->id;

        try {
            if (!$telephoneRelation->save()) {
                $telephone->delete();

                return [
                    'error' => true,
                    'type' => 'save-failed',
                    'message' => 'Não foi possível salvar o telefone, tente novamente mais tarde'
                ];
            }
        } catch (\Exception $err) {
            try {
                $telephone->delete();
            } catch (\Exception $err2) {
                Log::error('Não foi possível excluir a entrada de telefone depois que o salvamento da relação do telefone falhou então sobrou uma entrada no banco de dados:'.json_encode($telephone->toArray(), JSON_PRETTY_PRINT));
            }
            return [
                'error' => true,
                'type' => 'save-failed',
                'message' => 'Não foi possível salvar o telefone devido a um erro interno: '.$err->getMessage()
            ];
        }

        return [
            'success' => true,
            'user_id' => $user->id,
            'telephone_id' => $telephone->id,
            'telephone_relation_id' => $telephoneRelation->id
        ];
    }

    public function addAddressToUser(User $user, $cep, $logradouro, $numero, $bairro, $complemento, $id_cidade)
    {
        if (!$user || $user->id_perfil_conta !== PerfilConta::PERFIL_TOMADOR) {
            return ([
                'error' => true,
                'type' => 'missing-user',
                'message' => 'Não há um usuário válido'
            ]);
        }

        $address = new Endereco();
        $address->logradouro = $logradouro;
        $address->cep = $cep;
        $address->numero = $numero;
        $address->bairro = $bairro;
        if ($complemento && strlen($complemento) > 0) {
            $address->complemento = $complemento;
        }

        try {
            if (!$address->save()) {
                return ([
                    'error' => true,
                    'type' => 'save-failed',
                    'message' => 'Não foi possível salvar o endereço, tente novamente mais tarde'
                ]);
            }
        } catch (\Exception $err) {
            return ([
                'error' => true,
                'type' => 'save-failed',
                'message' => 'Não foi possível salvar o endereço devido a um erro interno: '.$err->getMessage()
            ]);
        }

        $addressRelation = new UsuarioTemEndereco();
        $addressRelation->id_usuario = $usuario->id;
        $addressRelation->id_endereco = $endereco->id;

        try {
            if (!$addressRelation->save()) {
                $address->delete();

                return ([
                    'error' => true,
                    'type' => 'save-failed',
                    'message' => 'Não foi possível salvar o endereço, tente novamente mais tarde'
                ]);
            }
        } catch (\Exception $err) {
            try {
                $address->delete();
            } catch (\Exception $err2) {
                Log::error('Não foi possível excluir a entrada de endereço depois que o salvamento da relação do endereço falhou então sobrou uma entrada no banco de dados:'.json_encode($telephone->toArray(), JSON_PRETTY_PRINT));
            }
            return ([
                'error' => true,
                'type' => 'save-failed',
                'message' => 'Não foi possível salvar o endereço devido a um erro interno: '.$err->getMessage()
            ]);
        }

        Session::put('address_id', $address->id);

        return ([
            'success' => true,
            'user_id' => $user->id,
            'address_id' => $address->id,
            'address_relation_id' => $addressRelation->id
        ]);
    }

    protected function validator(array $data, array $rules)
    {
        return Validator::make($data, $rules);
    }
}
