<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ServicoBase;

use Illuminate\Http\Request;

use DB;

class AgravanteController extends Controller
{

    public function find(Request $request)
    {
        if (empty($base = ServicoBase::find($request->input('base_id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        try {
            $data = $base->getAggravating();
        } catch (\Exception $e) {
            return response()->json(['msg' => $e->getMessage()], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json(['results' => $data], 200, [], JSON_PRETTY_PRINT);
    }

}