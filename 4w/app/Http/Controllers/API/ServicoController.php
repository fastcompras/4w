<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Servico;
use App\Models\ServicoBase;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use DB;

class ServicoController extends Controller
{

    public function find(Request $request)
    {
        $params['page'] = $request->input('page', 1);
        $params['term'] = Input::get('term');
        $params['limit'] = 25;
        $params['offset'] = ($params['page'] - 1) * $params['limit'];

        try {
            $data = Servico::getAvailableServices($params);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
        }

        $count = count($data);

        $endCount = $params['offset'] + $params['limit'];
        $morePages = $count > $endCount;

        $results = [
            "results" => $data,
            "pagination" => [
                "more" => $morePages,
                "next" => ($params['page'] + 1)
            ]
        ];

        return response()->json($results, 200, [], JSON_PRETTY_PRINT);
    }

    public function estimateCost(Request $request)
    {

        if (empty($base = ServicoBase::find($request->input('base_id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $aggravating = $request->input('aggravating', []);

        try {

            $ammount = $base->estimateCost($aggravating);

        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json(['ammount' => $ammount], 200, [], JSON_PRETTY_PRINT);
    }
}