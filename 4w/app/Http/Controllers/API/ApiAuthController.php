<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Estado;
use App\Models\Servico;
use App\Models\SugestaoServico;
use App\Models\User;
use App\Models\Endereco;
use App\Models\PerfilConta;
use App\Models\Telefone;
use App\Models\UsuarioPrestaServico;
use App\Models\UsuarioTemEndereco;
use App\Models\UsuarioTemDocumento;
use App\Models\UsuarioTemTelefone;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Session;
use Auth;

class ApiAuthController extends Controller
{

    private function reply($responseObject)
    {
        return response()->json($responseObject, 200, [], JSON_PRETTY_PRINT);
    }

    public function checkEmailExists(Request $request)
    {
        $email = Input::get('email');

        if (!$email) {
            return $this->reply([
                "error" => true,
                "message" => "Missing or invalid email input"
            ]);
        }

        try {
            $exists = (User::where('email', '=', $email)->count() > 0);
            return $this->reply([
                "success" => true,
                "email" => $email,
                "exists" => $exists
            ]);
        } catch (\Exception $err) {
            return $this->reply([
                "error" => true,
                "email" => $email,
                "message" => $err->getMessage()
            ]);
        }
    }

    public function getCurrentUserData(Request $request)
    {
        $userId = Session::get("user_id");

        if (!$userId) {
            $userId = Auth::id();
        }

        if (!$userId) {
            return $this->reply([
                "error" => true,
                "type" => "no-user",
                "message" => "Não há um id de usuário associado a sua conta"
            ]);
        }

        $user = User::find($userId);

        if (!$user) {
            return $this->reply([
                "success" => true,
                "type" => "no-user",
                "message" => "O usuário associado não é valido. Tente se cadastrar novamente."
            ]);
        }

        if (!Auth::check()) {
            return $this->reply([
                "success" => true,
                "type" => 'user-not-logged',
                "email" => $user->email
            ]);
        }

        return $this->reply([
            "success" => true,
            "type" => "user-is-logged",
            "name" => $user->name,
            "email" => $user->email
        ]);
    }
}
