<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ServicoBase;

use Illuminate\Http\Request;

use DB;

class ServicoBaseController extends Controller
{

    public function find(Request $request)
    {
        $params['id'] = $request->input('id');

        try {
            $data = ServicoBase::getAvailableBaseServices($params);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
        }

        $results = ["results" => $data];

        return response()->json($results, 200, [], JSON_PRETTY_PRINT);
    }
}