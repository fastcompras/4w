<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

use App\Models\User;

use Auth;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/login/index';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function index()
    {
        return view('login/index', ['title' => 'Login']);
    }

    public function authenticate(Request $request)
    {
        $credentials = [
          'email'     => Input::get('email'),
          'password'  => Input::get('password')
        ];

        $usuario = User::where('email', '=', Input::get('email'))->first();

        if (! empty($usuario)) {

            if (! $usuario->suspenso && Auth::attempt($credentials)) {
                return redirect()->intended('/');
            }

        }

        return Redirect::to('/login')->withErrors(['auth' => 'Dados não conferem!']);
    }

    public function getLogout()
    {
       return redirect()->intended('/login/index');
    }
}
