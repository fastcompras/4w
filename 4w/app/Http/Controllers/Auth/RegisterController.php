<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Estado;
use App\Models\Servico;
use App\Models\SugestaoServico;
use App\Models\User;
use App\Models\Endereco;
use App\Models\PerfilConta;
use App\Models\Telefone;
use App\Models\Documento;
use App\Models\UsuarioPrestaServico;
use App\Models\UsuarioTemEndereco;
use App\Models\UsuarioTemDocumento;
use App\Models\UsuarioTemTelefone;

use Illuminate\Validation\Rules\In;
use League\Flysystem\Exception;

use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Session;
use File;
use Auth;
use Validator;
use Mail;
use Redirect;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/login';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(Request $request)
    {
        $perfis = PerfilConta::pluck('descricao', 'id');
        $servicos = Servico::pluck('descricao', 'id');

        // Administrador
        if (isset($perfis[3])) {
            $perfis->forget(3);
        }

        if (empty($perfil = $request->get('perfil'))) {
            return redirect()->intended('/login');
        }

        return view('registrar/passo-1',
            [
                'perfis' => $perfis,
                'perfil' => $perfil,
                'servicos' => $servicos,
                'title' => 'Cadastro - Credenciais'
            ]
        );
    }

    public function create()
    {
        return view('registrar/vue', []);
    }

    public function passo1(Request $request)
    {
        $response = [
            'success' => false,
        ];

        $rules = [
            'nome' => 'required|min:3|max:255',
            'email' => 'required|email|max:255',
            'password' => 'required|min:6',
        ];

        $validator = $this->validator(Input::all(), $rules);

        if (!$validator->fails()) {
            Session::put('nome', Input::get('nome'));
            Session::put('email', Input::get('email'));
            Session::put('password', Input::get('password'));
            Session::put('perfil_conta', Input::get('id_perfil_conta'));
            Session::put('id_servico', Input::get('id_servico'));
            Session::put('sugestao', Input::get('sugestao'));

            $existeUsuario = User::where('email', '=', Input::get('email'))->get();

            if(count($existeUsuario) > 0) {
                if ($existeUsuario[0]->id_perfil_conta == PerfilConta::PERFIL_TOMADOR && Input::get('id_perfil_conta') == PerfilConta::PERFIL_PRESTADOR) {
                    $response = [
                        'success' => true,
                    ];
                } else {
                    $response = [
                        'success' => false,
                        'mensagem' => 'Já existe um usuário com este e-mail!'
                    ];
                }
            } else {
                $response = [
                    'success' => true,
                ];
            }
        } else {
            $response = [
                'success' => false,
                'mensagem' => $validator->messages(),
            ];
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function passo2(Request $request)
    {
        $response = [
            'success' => true,
            'cadastrou' => false,
            'url' => '',
            'mensagem' => 'Não foi possível realizar seu cadastro. Por favor, contate nosso suporte!'
        ];

        if ($request->isMethod('post')) {
            $rules = [
                'cep' => 'required',
                'logradouro' => 'required|max:255',
                'numero' => 'required|min:1',
                'bairro' => 'required|min:3',
                'telefone' => 'required|min:8',
                'id_cidade' => 'required',
            ];

            $validator = $this->validator(Input::all(), $rules);

            if (!$validator->fails()) {
                Session::put('cep', Input::get('cep'));
                Session::put('logradouro', Input::get('logradouro'));
                Session::put('numero', Input::get('numero'));
                Session::put('complemento', Input::get('complemento'));
                Session::put('bairro', Input::get('bairro'));
                Session::put('telefone', Input::get('telefone'));
                Session::put('id_cidade', Input::get('id_cidade'));
                Session::put('id_estado', Input::get('id_estado'));

                $usuario = new User();
                $usuario->nome = Session::get('nome');
                $usuario->email = Session::get('email');
                $usuario->password = bcrypt(Session::get('password'));
                $usuario->id_perfil_conta = Session::get('perfil_conta');
                $usuario->id_cidade = Session::get('id_cidade');
                $usuario->aprovado = ($usuario->id_perfil_conta == PerfilConta::PERFIL_PRESTADOR) ? User::USUARIO_PENDENTE : User::USUARIO_APROVADO;
                $usuario->data_cadastro = date("Y-m-d H:i:s");

                if ($usuario->save()) {
                    Session::put('user_id', $usuario->id);

                    //Usuários prestadores devem vincular seus serviços prestados
                    if ($usuario->id_perfil_conta == PerfilConta::PERFIL_PRESTADOR) {
                        $idsServicos = Session::get('id_servico');

                        if (count($idsServicos) > 0) {
                            foreach ($idsServicos as $id) {
                                $relacaoServico = new UsuarioPrestaServico();
                                $relacaoServico->id_usuario = $usuario->id;
                                $relacaoServico->id_servico = $id;
                                $relacaoServico->save();
                            }
                        }
                    }

                    //Salva o endereço e vincula ao usuário
                    $endereco = new Endereco();
                    $endereco->logradouro = Session::get('logradouro');
                    $endereco->cep = Session::get('cep');
                    $endereco->numero = Session::get('numero');

                    if (strlen(Session::get('complemento')) > 0) {
                        $endereco->complemento = Session::get('complemento');
                    }

                    $endereco->bairro = Session::get('bairro');

                    //Salva o telefone e vincula ao usuário
                    $telefone = new Telefone();
                    $telefone->numero = Session::get('telefone');


                    if ($endereco->save() && $telefone->save()) {
                        $relacao = new UsuarioTemEndereco();
                        $relacao->id_usuario = $usuario->id;
                        $relacao->id_endereco = $endereco->id;

                        $relacaoTelefone = new UsuarioTemTelefone();
                        $relacaoTelefone->id_usuario = $usuario->id;
                        $relacaoTelefone->id_telefone = $telefone->id;

                        if ($relacao->save() && $relacaoTelefone->save()) {
                            if (Session::get('perfil_conta') == PerfilConta::PERFIL_TOMADOR) {

                                $credentials = [
                                    'email'     => $usuario->email,
                                    'password'  => Session::get('password')
                                ];

                                Auth::attempt($credentials);

                                Mail::send('emails.tomadores.cadastro-recebido', ['tomador' => $usuario->toArray(), 'title' => 'Cadastro Realizado'], function($message) use ($usuario) {
                                    $message->to($usuario->email);
                                    $message->subject('Cadastro Realizado');
                                });


                                $response = [
                                    'success' => true,
                                    'cadastrou' => true,
                                    'url' => '/login',
                                ];
                            } else {
                                $response = [
                                    'success' => true,
                                    'cadastrou' => false,
                                    'url' => '/registrar/passo3',
                                ];
                            }
                        }
                    }
                }

                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            } else {
                $response = [
                    'success' => false,
                    'mensagem' => $validator->messages(),
                ];

                return response()->json($response, 200, [], JSON_PRETTY_PRINT);
            }
        }

        return view('registrar/passo-2', ['title' => 'Cadastro - Localização']);
    }

    public function passo3(Request $request)
    {
        if ($request->isMethod('post')) {
            $usuario = User::find(Session::get('user_id'));

            $avatar = Input::file('avatar');
            $rg = Input::file('rg');
            $cpf = Input::file('cpf');
            $mei = Input::file('mei');
            $residencia = Input::file('residencia');
            $antecedentes = Input::file('antecedentes');
            $certificado = Input::file('certificados');

            $path = public_path().DIRECTORY_SEPARATOR.'documentos';
            $destinationPath = $path . '/' . $usuario->email;
            $extensoesPermitidas = array("pdf", "png", "jpeg", "jpg");
            $extensoesPermitidasAvatar = array("jpeg", "png", "jpg", "bmp");

            //Cria diretório caso não exista
            if(!File::exists($destinationPath)) {
                $createDirectory = File::makeDirectory($destinationPath, 0777);
            }

            //Manipula avatar do prestador
            if (Input::hasFile('avatar')) {
                $avatarName = "avatar." . $avatar->getClientOriginalExtension();
                $avatarUrl = "documentos/" . $usuario->email . '/' . $avatarName;

                if(!in_array($avatar->getClientOriginalExtension(), $extensoesPermitidasAvatar)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize JPEG ou PNG.';

                    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                } else {

                    if ($avatar->move($destinationPath, $avatarName)) {
                        $usuario->salvaDocumento($avatarUrl, Documento::TIPO_AVATAR);
                        $usuario->avatar = '/'.$avatarUrl;
                        $usuario->save();
                    }
                }
            }

            //Manipula documento RG
            if (Input::hasFile('rg')) {
                $rgName = "documento_rg." . $rg->getClientOriginalExtension();
                $urlRg = "documentos/" . $usuario->email . '/' . $rgName;

                if(!in_array($rg->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                } else {

                    if ($rg->move($destinationPath, $rgName)) {
                        $usuario->salvaDocumento($urlRg, Documento::TIPO_RG);
                    }
                }
            }

            //Manipula documento RG
            if (Input::hasFile('mei')) {
                $meiName = "documento_mei." . $mei->getClientOriginalExtension();
                $urlMei = "documentos/" . $usuario->email . '/' . $meiName;

                if(!in_array($mei->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                } else {

                    if ($mei->move($destinationPath, $meiName)) {
                        $usuario->salvaDocumento($urlMei, Documento::TIPO_MEI);
                    } else {
                        dd("aqui");
                    }
                }
            }

            //Manipula documento CPF
            if (Input::hasFile('cpf')) {
                $cpfName = "documento_cpf." . $cpf->getClientOriginalExtension();
                $urlCpf = "documentos/" . $usuario->email . '/' . $cpfName;

                if(!in_array($cpf->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                } else {

                    if ($cpf->move($destinationPath, $cpfName)) {
                        $usuario->salvaDocumento($urlCpf, Documento::TIPO_CPF);
                    }
                }
            }

            //Manipula documento de comprovante de residência
            if (Input::hasFile('residencia')) {
                $residenciaName = "documento_residencia." . $residencia->getClientOriginalExtension();
                $urlResidencia = "documentos/" . Session::get('email') . '/' . $residenciaName;

                if(!in_array($residencia->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                } else {

                    if ($residencia->move($destinationPath, $residenciaName)) {
                        $usuario->salvaDocumento($urlResidencia, Documento::TIPO_RESIDENCIA);
                    }
                }
            }

            //Manipula documento de comprovante de bons antecedentes
            if (Input::hasFile('antecedentes')) {
                $antecedentesName = "documento_antecedentes." . $antecedentes->getClientOriginalExtension();
                $urlAntecedentes = "documentos/" . Session::get('email') . '/' . $antecedentesName;

                if(!in_array($antecedentes->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                } else {

                    if ($antecedentes->move($destinationPath, $antecedentesName)) {
                        $usuario->salvaDocumento($urlAntecedentes, Documento::TIPO_ANTECEDENTES);
                    }
                }
            }

            //Manipula documento CPF
            if (Input::hasFile('certificados')) {

                $i = 1;
                foreach ($certificado as $cert) {
                    $certificadoName = "certificado_" . $i . "." . $cert->getClientOriginalExtension();
                    $urlCertificado = "documentos/" . $usuario->email . '/' . $certificadoName;

                    if(!in_array($cert->getClientOriginalExtension(), $extensoesPermitidas)) {
                        $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                    } else {

                        if ($cert->move($destinationPath, $certificadoName)) {
                            $usuario->salvaDocumento($urlCertificado, Documento::TIPO_CERTIFICADO);
                        }
                    }
                    $i++;
                }
            }

            Mail::send('emails.prestadores.cadastro-pendente', ['prestador' => $usuario->toArray(), 'title' => 'Legal! Falta pouco para você fazer parte da nossa rede!'], function($message) use ($usuario) {
                $message->to($usuario->email);
                $message->subject('Cadastro Recebido');
            });

            //FIXME Colocar em um cronjob
            /*Mail::send('emails.prestadores.cadastro-incompleto', ['prestador' => $usuario->toArray(), 'title' => 'Complete seu cadastro! Você está perdendo oportunidades de negócio!'], function($message) use ($usuario) {
                $message->to($usuario->email);
                $message->subject('Conclua seu cadastro');
            });*/


            $credentials = [
                'email'     => $usuario->email,
                'password'  => Session::get('password')
            ];

            Auth::attempt($credentials);

            $response = [
                'success' => true,
                'url' => '/login',
                'mensagem' => 'Cadastro realizado com sucesso! Você receberá um e-mail assim que sua conta for avaliada!'
            ];

            return response()->json($response, 200, [], JSON_PRETTY_PRINT);
        }

        return view('registrar/passo-3', ['title' => 'Cadastro - Documentação']);

    }

    public function recoveryPassword()
    {
        return view('registrar/recuperar-senha', [
            'title' => 'Recuperar Senha'
        ]);
    }

    public function sendLinkToNewPassword(Request $request)
    {
        if (empty($email = $request->input('email'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($user = User::where('email', '=', $email)->first())) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        try {

            $token = bin2hex(random_bytes(35));

            // Creates a temporarily token
            $user->recovery_token = $token;

            if (! $user->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

            // Sends a e-mail to user
            Mail::send('emails.sys.recuperar-senha', [
                'user'    => $user->toArray(),
                'token'   => $token,
            ], function($message) use ($user) {
                $message->to($user->email);
                $message->subject('Solicitação de Redefinição de Senha');
            });

        } catch (\Exception $e) {
            return response()->json(['msg' => $e->getMessage()], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function showFormResetPassword(Request $request)
    {
        if (empty($token = $request->input('token'))) {
            return Redirect::action('Auth\LoginController@index');
        }

        if (empty($user = User::where('recovery_token', '=', $token)->first())) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        return view('registrar/redefinir-senha', [
            'token' => $user->recovery_token,
            'title' => 'Redefinir Senha'
        ]);

    }

    public function resetPassword(Request $request)
    {

        if (empty($password = $request->input('password'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($token = $request->input('token'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }


        if (empty($user = User::where('recovery_token', '=', $token)->first())) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        // Saves the new password
        $user->password = bcrypt($password);
        $user->recovery_token = null;

        try {

            if (! $user->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

        } catch (\Exception $e) {
            return response()->json(['msg' => $e->getMessage()], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    protected function validator(array $data, array $rules)
    {
        return Validator::make($data, $rules);
    }
}
