<?php

namespace App\Http\Controllers;

use App\Models\SugestaoServico;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

use DB;
use App\Models\Servico as Servico;
use App\Helpers\Helper as Helper;
use Illuminate\Support\Facades\Auth;

class ServicoController extends Controller
{
    public function index()
    {
        if (!Auth::user()->isAdmin()) {
            // abort(403);
        }

        $servicos = Servico::where('descricao', '<>', 'Outro')->paginate(10);

        return view('servicos/index', [ 'servicos' => $servicos ]);
    }

    public function sugeridos()
    {
        $servicos = SugestaoServico::where('aprovado', '<>', '1')->paginate(10);

        return view('servicos/sugeridos', ['servicos' => $servicos]);
    }

    public function novo($id = null)
    {
        if ($id == null) {
            $servico = new Servico();
        } else {
            $servico = Servico::find($id);
        }

        return view('servicos/novo', [
            'servico' => is_null($servico) ? new Servico() : $servico,
            'uploadedImages' => Helper::getWordpressUploadedImages(),
            'uploadUrl' => Helper::getWordpressUploadUrl()
        ]);
    }

    public function salvar(Request $request)
    {
        $response = [
            'msg' => 'Erro ao criar o serviço',
            'success' => false
        ];

        if ($request->isMethod('post')) {
            $data = [
                'id' => intval($request->input('id')),
                'descricao' => $request->input('descricao'),
                'top_image_url' => $request->input('top_image_url'),
                'descricao_extensa' => $request->input('descricao_extensa'),
                'descricao_completa' => $request->input('descricao_completa'),
                'url' => $request->input('url')
            ];

            if ($data['id']) {
                $servico = Servico::find($data['id']);

                if (is_null($servico)) {
                    return response()->json([
                        'msg' => 'O serviço ('. $data['id'] .') não foi encontrado, talvez ele tenha sido removido',
                        'success' => false,
                        'error' => true
                    ], 200, [], JSON_PRETTY_PRINT);
                }
            } else {
                $servico = new Servico();
                $servico->data_cadastro = date('Y-m-d H:i:s');
            }

            $servico->descricao = $data['descricao'];
            $servico->descricao_extensa = $data['descricao_extensa'];
            $servico->descricao_completa = $data["descricao_completa"];
            $servico->top_image_url = $data["top_image_url"];
            $servico->url = $data['url'];

            if (!$servico->descricao || strlen($servico->descricao) <= 5) {
                return response()->json([
                    'msg' => 'A descrição do serviço é muito curta',
                    'success' => false,
                    'error' => true
                ], 200, [], JSON_PRETTY_PRINT);
            }

            if (!$servico->url) {
                return response()->json([
                    'msg' => 'O serviço obrigatoriamente precisa ter uma url',
                    'success' => false,
                    'error' => true
                ], 200, [], JSON_PRETTY_PRINT);
            }

            if (strpos($servico->url, ' ') !== false || strpos($servico->url, '/') !== false || strpos($servico->url, '\\') !== false) {
                return response()->json([
                    'msg' => 'URL não pode conter espaços nem traços (/ ou \\)',
                    'success' => false,
                    'error' => true
                ], 200, [], JSON_PRETTY_PRINT);
            }

            $rules = array(
                'descricao' => 'required|min:5'
            );

            $validator = Validator::make(Input::all(), $rules);
            try {
                if (!$validator->fails()) {
                    if ($servico->save()) {
                        $response = [
                            'msg' => $data['id'] ? 'Serviço salvo com sucesso!' : 'Serviço criado com sucesso!',
                            'success' => true
                        ];
                    }
                }
            } catch (\Exception $err) {
                $response = [
                    'msg' => 'Ocorreu um erro ao salvar: '.$err->getMessage(),
                    'success' => false,
                    'error' => true
                ];
            }
        }
        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function aprovar(Request $request)
    {
        $response = [
            'msg' => 'Erro ao aprovar o serviço',
            'success' => false
        ];

        if ($request->isMethod('post')) {
            $sugestao = SugestaoServico::find($request->input('id'));

            if (!is_null($sugestao)) {
                $sugestao->aprovado = SugestaoServico::SUGESTAO_APROVADA;

                if ($sugestao->save()) {
                    $servico = new Servico();
                    $servico->descricao = $sugestao->descricao;
                    $servico->data_cadastro = date('Y-m-d H:i:s');

                    if ($servico->save()) {
                        $response = [
                            'msg' => 'Serviço aprovado com sucesso!',
                            'success' => true
                        ];
                    }
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function rejeitar(Request $request)
    {
        $response = [
            'msg' => 'Erro ao rejeitar o serviço',
            'success' => false
        ];

        if ($request->isMethod('post')) {
            $sugestao = SugestaoServico::find($request->input('id'));

            if (!is_null($sugestao)) {
                $sugestao->aprovado = SugestaoServico::SUGESTAO_REJEITADA;

                if ($sugestao->save()) {

                    $response = [
                        'msg' => 'Sugestão rejeitada com sucesso!',
                        'success' => true,
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function remover(Request $request)
    {
        $response = [
            'msg' => 'Erro ao remover o serviço!',
            'success' => false
        ];

        if ($request->isMethod('delete')) {
            $servico = Servico::find($request->input('id'));

            if ($servico->delete()) {
                $response = [
                    'msg' => 'Serviço removido com sucesso!!',
                    'success' => true,
                ];
            }

        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
