<?php

namespace App\Http\Controllers\Checkout;

use App\Http\Controllers\Controller;
use App\Models\Checkout\Simulacao;
use App\Models\Checkout\SimulacaoOpcao;
use App\Models\Servico;
use App\Models\ServicoBase;
use App\Models\ServicoBaseAgravante;
use App\Services\InternalRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Files;
use Redirect;
use App\Models\User;
use Log;

class StartCheckoutController extends Controller
{

    public function __construct(InternalRequest $internalRequest)
    {
        $this->internalRequest = $internalRequest;
    }

    private function applyToSimulation(Simulacao $sim, $simParameter, $configArray, $configParameter, $sandboxDefault = null, $isSandbox = false)
    {
        if (array_key_exists($configParameter, $configArray) && $configArray[$configParameter]) {
            return $sim->{$simParameter} = $configArray[$configParameter];
        }
        if ($isSandbox) {
            return $sim->{$simParameter} = $sandboxDefault;
        }

        throw new \Exception("Missing value for required \"".$configParameter."\" parameter.");
    }

    private function generateSimulation($config, $isSandbox = false)
    {
        $sim = new Simulacao();

        $this->applyToSimulation($sim, "servico_id", $config, "servico-id", "8", $isSandbox);
        $this->applyToSimulation($sim, "servico_base_id", $config, "servico-base-id", "24", $isSandbox);
        $this->applyToSimulation($sim, "nome", $config, "name", "Teste Sandbox", $isSandbox);
        $this->applyToSimulation($sim, "email", $config, "email", "sandbox@sou4w.com.br", $isSandbox);
        $sim->cep = (array_key_exists("cep", $config)) ? $config["cep"] : "";
        $sim->telefone = (array_key_exists("telefone", $config)) ? $config["telefone"] : "";
        if ($isSandbox) {
            $sim->usuario_id = 2;
        }

        if (Servico::find($sim->servico_id) === null) {
            throw new \Exception("Service id ".$sim->servico_id." was not found");
        }

        if (ServicoBase::find($sim->servico_base_id) === null) {
            throw new \Exception("Service base id ".$sim->servico_base_id." was not found");
        }

        return $sim;
    }

    private function getSimulationOptionsAssociativeArray($config)
    {
        $optionValues = [];

        foreach ($config as $key => $value) {
            if (substr($key, 0, 9) === "agrav-id-" && ctype_digit(substr($key, 9))) {
                $optionId = intval(substr($key, 9));
                $optionValues[$optionId] = (string) (ctype_digit($value)?$value:-1);
            }
        }

        return $optionValues;
    }

    private function generateSimulationOptions($config, $isSandbox = false)
    {
        $options = $this->getSimulationOptionsAssociativeArray($config);

        if (empty($options) && $isSandbox) {
            $options = [
                "215" => "1",
                "216" => "391",
                "217" => "1"
            ];
        }

        $objects = [];

        foreach ($options as $key=>$value) {
            $option = new SimulacaoOpcao();
            $option->agravante_id = intval($key);
            $option->agravante_value = intval($value);
            array_push($objects, $option);

            if (ServicoBaseAgravante::find($option->agravante_id) === null) {
                throw new \Exception("Service agravant id ".$option->agravante_id." was not found.");
            }
        }

        return $objects;
    }

    private function generatePriceEstimate(Simulacao $simulation)
    {
        $optionsList = [];
        $simulationOptions = $simulation->opcoes()->get();
        foreach ($simulationOptions as $simulationOption) {
            array_push($optionsList, [
                "id" => $simulationOption->agravante_id,
                "value" => $simulationOption->agravante_value
            ]);
        }

        $postData = [
            "base_id" => $simulation->servico_base_id,
            "aggravating" => $optionsList
        ];

        $response = $this->internalRequest->request(
            "POST",
            "/api/servicos/orcamento",
            $postData,
            ["Accept" => "application/json"]
        );

        $content = $response->content();

        if ($response->getStatusCode() >= 400) {
            Log::info("Price estimate returned error: ". $content);
            throw new \Exception("Não foi possível gerar um orçamento válido devido a um erro.");
        }

        try {
            $json = json_decode($content, true);
        } catch (\Exception $err) {
            Log::info("Price estimate json parsing: ". $content);
            throw new \Exception("Não foi possível gerar um orçamento válido devido a um erro.");
        }

        if (array_key_exists("error", $json) && $json["error"]) {
            if (array_key_exists("message", $json)) {
                throw new \Exception("Não foi possível gerar um orçamento válido devido a um erro: ".$json["message"]);
            } else {
                Log::info("Price estimate json indicated error: ". $content);
                throw new \Exception("Não foi possível gerar um orçamento válido devido a um erro.");
            }
        }

        foreach(["amount", "ammount", "value", "price"] as $possibleKey) {
            if (array_key_exists($possibleKey, $json)) {
                return $json[$possibleKey];
            }
        }

        Log::info("Price estimate json was missing key: ". $content);
        throw new \Exception("Não foi possível gerar um orçamento válido devido a ausência do valor no resultado.");
    }

    public function getServiceDetails(Simulacao $simulation)
    {
		$sql = implode("\n", [
			"SELECT",
			"	servico.id AS 'service-id',",
			"	servico.descricao AS 'service-name',",
            "   servico.url AS 'service-url',",
			"	servico_base.id AS 'service-base-id',",
			"	servico_base.nome AS 'service-base-name',",
            "   servico_base.url AS 'service-base-url',",
			"	servico_base.descricao AS 'service-base-description'",
			"FROM servico",
			"INNER JOIN servico_base ON servico_base.id_servico = servico.id",
			"INNER JOIN simulacoes ON simulacoes.servico_base_id = servico_base.id",
			"WHERE simulacoes.id = :id",
	        "LIMIT 1",
		]);

        $results = DB::select(DB::raw($sql),["id" => $simulation->id]);
        if (!$results) {
            throw new \Exception("Invalid database result");
        }

        foreach ($results as $row) {
            return [
                "simulation-id" => $simulation->id,
                "service-id" => $row->{"service-id"},
                "service-name" => $row->{"service-name"},
                "service-url" => $row->{"service-url"},
                "service-base-id" => $row->{"service-base-id"},
                "service-base-name" => $row->{"service-base-name"},
                "service-base-url" => $row->{"service-base-url"},
                "service-base-description" => $row->{"service-base-description"},
                "service-price" => $this->generatePriceEstimate($simulation)
            ];
        }

        throw new Exception("Could not find simulation with id ".$simulation->id);
    }

    /**
     * Retorna os dados de cada opção de uma simulação, especialmente o titulo do agravante, o valor da opção e até o titulo da opção do agravante
     * @param  Simulacao $simulation  A simulação relevante para buscar os dados
     * @return array                  Uma array de arrays associativas com os dados de cada agravante
     */
    public function getOptionDetails(Simulacao $simulation)
    {
        $array = $simulation->opcoes()->get();

		$sql = implode("\n", [
			"SELECT",
			"	servico_base_agravante.rotulo_formulario AS 'option-label',",
            "	CASE servico_base_agravante.tipo_formulario",
            "       WHEN 1 THEN 'numeric'",
            "       WHEN 2 THEN 'checkbox'",
            "       WHEN 3 THEN 'option'",
            "       ELSE servico_base_agravante.tipo_formulario",
            "   END AS 'option-type',",
			"	opcoes_simulacao.agravante_id AS 'option-id',",
            "	opcoes_simulacao.agravante_value AS 'option-value',",
            "   agravante_tem_opcoes.descricao AS 'option-description'",
			"FROM servico_base_agravante",
            "INNER JOIN opcoes_simulacao ON opcoes_simulacao.agravante_id = servico_base_agravante.id",
            "LEFT OUTER JOIN agravante_tem_opcoes ON agravante_tem_opcoes.id_servico_base_agravante = servico_base_agravante.id AND agravante_tem_opcoes.id = opcoes_simulacao.agravante_value",
			"WHERE opcoes_simulacao.simulacao_id = :id",
		]);

        $results = DB::select(DB::raw($sql),["id" => $simulation->id]);
        if (!$results) {
            throw new \Exception("Invalid database result");
        }

        $array = [];

        foreach ($results as $row) {
            array_push($array, [
                "option-label" => $row->{"option-label"},
                "option-type" => $row->{"option-type"},
                "option-id" => $row->{"option-id"},
                "option-value" => $row->{"option-value"},
                "option-description" => $row->{"option-description"}
            ]);
        }

        return $array;
    }

    private function sendNotificationEmail($simulation, $serviceDetails, $optionDetails)
    {
        try {
            $serviceName = $serviceDetails["service-name"];
            $serviceBaseName = $serviceDetails["service-base-name"];
            $estimate = $serviceDetails["service-price"];
            $name = $simulation->name;
            $telephone = $simulation->telefone;
            $email = $simulation->email;

            $subject = "Simulação de Orçamento Efetuada - Sou4W";

            $to = "atendimento@fastcompras.com.br";
            if (strpos($simulation->email, "fastcompras") !== false) {
                $to = $simulation->email;
            }
            $from = "atendimento@fastcompras.com.br";

            $headers = implode("\r\n", [
                "MIME-Version: 1.0",
                "Content-type: text/html; charset=utf-8",
                "From: ".$from,
                "X-Mailer: PHP/" . phpversion()
            ])."\r\n";

            // Compose a simple HTML email message
            $message = "<html><body>";
            $message .= "<h1 style='display:inline-block;width:70%;text-align:center;color:#222;'>Simulação de Orçamento Efetuada - Sou4W</h1>";
            $message .= "<div style='font-family:arial,sans-serif;color:#333;font-size:18px'>";

            $relevantInfo = [
                [
                    "title" => "Nome do Consumidor",
                    "value" => $name
                ],
                [
                    "title" => "Email do Consumidor",
                    "value" => $email
                ],
                [
                    "title" => "Serviço solicitado",
                    "value" => $serviceName
                ],
                [
                    "title" => "Serviço base solicitado",
                    "value" => $serviceBaseName
                ],
                [
                    "title" => "Valor oferecido",
                    "value" => "R$ ".number_format($estimate, 2, ",", ".")
                ]
            ];
            foreach ($relevantInfo as $info) {
                if (!$info["value"]) {
                    continue;
                }
                $message .= "<div style='margin-top:5px;font-weight:bold;width: 30%;text-align:right;margin-right:20px;display:inline-block'>".$info["title"].":</div>";
                $message .= "<div style='margin-top:5px;width: 60%;display:inline-block'>".$info["value"]."</div><br/>";
            }
            $message .= "<hr /><hr />";
            foreach ($optionDetails as $detail) {
                if (!is_array($detail)) {
                    dd($detail);
                }
                $value = array_key_exists("option-description", $detail) && $detail["option-description"] ? $detail["option-description"] : $detail["option-value"];
                $message .= "<div style='margin-top:5px;font-weight:bold;width: 30%;font-size: 12px;text-align:right;margin-right:20px;display:inline-block'>".$detail["option-label"].":</div>";
                $message .= "<div style='margin-top:5px;width: 60%;display:inline-block;font-size: 12px;'>".$value."</div><br/>";
            }
            $message .= "</div>";
            $message .= "<div style='font-family: monospace;font-size: 12px; display: inline-block;width:70%;text-align:center;margin-top:50px'>";
            $message .= "(Isto é uma mensagem gerada automáticamente)";
            $message .= "</div>";
            $message .= "</body></html>";
            /*
                Mail::send('emails.blade.url', $data, function($message) use ($deal) {
                    $message->cc('someone@');
                    $message->subject('Erro ao atualizar o pedido #' . $deal->id);
                });
             */
            if(!mail($to, $subject, $message, $headers)) {
                throw new \Exception("Could not sent simulation notification email due to a failure");
            }
        } catch (Exception $err) {
            Log::critical("Email error: ".((string) $err));
        }
    }

    public function betterScandir($dir, $sorting_order = SCANDIR_SORT_ASCENDING) {
        // Roll through the scandir values.
        $files = array();
        foreach (scandir($dir, $sorting_order) as $file) {
            if ($file[0] === '.') {
                continue;
            }
            $files[$file] = filemtime($dir . '/' . $file);
        } // foreach

        // Sort the files array.
        if ($sorting_order == SCANDIR_SORT_ASCENDING) {
            asort($files, SORT_NUMERIC);
        }
        else {
            arsort($files, SORT_NUMERIC);
        }

        // Set the final return value.
        $ret = array_keys($files);

        // Return the final value.
        return ($ret) ? $ret : false;
    }

    // Put simulation file for other service to consume
    public function writeSimulationFile($simulation, $serviceDetails, $optionDetails)
    {
        $userAgent = array_key_exists("HTTP_USER_AGENT", $_SERVER) ? $_SERVER["HTTP_USER_AGENT"] : "";
        $ip = array_key_exists("REMOTE_ADDR", $_SERVER) ? $_SERVER["REMOTE_ADDR"] : "";
        $data = [
            "version" => "1.0",
            "stages" => [
                ["datetime" => (new \DateTime(null, new \DateTimeZone("America/Sao_Paulo")))->format("Y-m-d H:i:s"), "name" => "starting-checkout"]
            ],
            "agent" => $userAgent,
            "ip" => $ip,
            "is-registered-user" => User::where('email', '=', $simulation->email)->first() ? true : false,
            "service-details" => $serviceDetails,
            "option-details" => $optionDetails,
            "simulation-details" => [
                "name" => $simulation->nome,
                "email" => $simulation->email,
                "telephone" => $simulation->telefone
            ]
        ];

        // I know, this is a hard-to-reach constant, whatever
        $filePath = "/home/wcom4/public_html/checkout/simulations";
        if (!file_exists($filePath)) {
            throw new \Exception("Missing simulation folder");
        }

        // Clean up if there's too many files
        try {
            $files = $this->betterScandir($filePath);
            if (count($files) > 300) {
                $leftOver = count($files);
                file_put_contents("/home/wcom4/public_html/checkout/simulations/debug.txt", "left over start: ".$leftOver."\n", FILE_APPEND);
                foreach ($files as $file) {
                    if ($file === "." || $file === "..") {
                        continue;
                    }
                    if (substr($file, -5) !== ".json") {
                        file_put_contents("/home/wcom4/public_html/checkout/simulations/debug.txt", "start skipped non-json: ".$file."\n", FILE_APPEND);
                        continue;
                    }
                    if (!is_file($filePath."/".$file)) {
                        file_put_contents("/home/wcom4/public_html/checkout/simulations/debug.txt", "start skipped non-valid-file: ".$file."\n", FILE_APPEND);
                        continue;
                    }
                    $leftOver -= 1;
                    if ($leftOver <= 150) {
                        file_put_contents("/home/wcom4/public_html/checkout/simulations/debug.txt", "broke at ".$file." because its below our limit: ".$leftOver."\n", FILE_APPEND);
                        break;
                    }
                    file_put_contents("/home/wcom4/public_html/checkout/simulations/debug.txt", "unlinking: ".$file." left over now: ".$leftOver."\n", FILE_APPEND);
                    unlink($filePath."/".$file);
                }
            }
        } catch (\Exception $err) {
            file_put_contents("/home/wcom4/public_html/checkout/simulations/debug.txt", "erro: ".$err->getStackAsString()."\n", FILE_APPEND);
        }
        $fileHash = "s".substr(md5($ip.$userAgent), 0, 7)."_".rand(100000,999999);
        $fileName = $fileHash . ".json";

        $data["datafile"] = $fileName;

        $count = file_put_contents($filePath . "/" . $fileName, json_encode($data));
        if (!($count > 1)) {
            throw new \Exception("Could not write simulation file");
        }
        return $fileHash;
    }

    public function replyOptions(Request $request)
    {
        header("Access-Control-Allow-Origin=*");
        header("Access-Control-Allow-Methods=GET, POST, PUT, DELETE, OPTIONS");
    }

    public function startCheckout(Request $request)
    {
        $isSandbox = $request->get('isSandbox');
        $parameters = $request->all();

        if( strtotime("2020-08-01") < time() ){
            // No log after the date
        } else {
            // Your date is in the future, so log normally:
            Log::info("Inicio de checkout com sandbox em: ".var_export($isSandbox, true)." e parametros:\n".var_export($parameters)."\n");
        }

        $simulation = $this->generateSimulation($parameters, $isSandbox);
        $simulationOptions = $this->generateSimulationOptions($parameters, $isSandbox);

        if (!$simulation->save()) {
            throw new Exception("Could not save simulation");
        }
        $simulation->opcoes()->saveMany($simulationOptions);

        $optionDetails = $this->getOptionDetails($simulation);
        $serviceDetails = $this->getServiceDetails($simulation);

        $this->sendNotificationEmail($simulation, $serviceDetails, $optionDetails);

        $fileHash = $this->writeSimulationFile($simulation, $serviceDetails, $optionDetails);

        return Redirect::away('https://www.sou4w.com.br/checkout');
    }
}