<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;

use App\Models\Regiao;
use App\Models\Bairro;
use App\Models\Estado;

class LocalizacaoController extends Controller
{
    public function regioes()
    {
        $regioes = Regiao::where('id', '>', 0)->paginate(10);

        return view('regioes/index', ['regioes' => $regioes]);
    }

    public function novaRegiao($id = null)
    {
        $estados = Estado::orderBy('nome', 'asc')->pluck('nome', 'uf');

        if ($id == null) {
            $regiao = new Regiao();
        } else {
            $regiao = Regiao::find($id);
        }

        return view('regioes/novo', ['regioes' => !is_null($regiao) ? $regiao : new Regiao(), 'estados' => $estados]);
    }

    public function salvarRegiao(Request $request)
    {
        $response = [
            'msg' => 'Erro ao criar a região',
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            $regiao = new Regiao();
            $regiao->id_cidade = $request->input('id_cidade');
            $regiao->nome = $request->input('regiao');


            $rules = array(
                'regiao' => 'required|min:5'
            );

            $validator = Validator::make(Input::all(), $rules);

            if (!$validator->fails()) {
                if ($regiao->save()) {
                    $response = [
                        'msg' => 'Região criada com sucesso!',
                        'success' => true,
                    ];
                }
            }

        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function bairros()
    {
        $bairros = Bairro::where('id', '>', 0)->paginate(10);

        return view('bairros/index', ['bairros' => $bairros]);
    }

    public function novoBairro()
    {
        $estados = Estado::orderBy('nome', 'asc')->pluck('nome', 'uf');

        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        $bairro = new Bairro();

        return view('bairros/novo', ['bairro' => $bairro, 'estados' => $estados]);
    }

    public function editarBairro($id)
    {
        $estados = Estado::orderBy('nome', 'asc')->pluck('nome', 'uf');

        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        if (empty($bairro = Bairro::find($id))) {
            abort(404);
        }

        return view('bairros/editar', ['bairro' => $bairro, 'estados' => $estados]);
    }

    public function salvarBairro(Request $request)
    {
       if (!Auth::user()->isAdmin()) {
           abort(403);
       }

       if (empty($cidade = $request->input('id_cidade'))) {
           return response()->json([], 404, [], JSON_PRETTY_PRINT);
       }

        if (empty($regiao = $request->input('id_regiao'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($nome = $request->input('bairro'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $bairro = new Bairro();
        $bairro->id_cidade = $cidade;
        $bairro->id_regiao = $regiao;
        $bairro->nome = $nome;

        try {
            if (!$bairro->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function atualizarBairro(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        if (empty($bairro = Bairro::find($request->input('id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($cidade = $request->input('id_cidade'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($regiao = $request->input('id_regiao'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($nome = $request->input('bairro'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $bairro->id_cidade = $cidade;
        $bairro->id_regiao = $regiao;
        $bairro->nome = $nome;

        try {
            if (!$bairro->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function removerBairro(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        if (empty($bairro = Bairro::find($request->input('id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        try {
            if (!$bairro->delete()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }
}
