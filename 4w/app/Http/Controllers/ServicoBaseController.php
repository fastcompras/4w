<?php

namespace App\Http\Controllers;

use App\Models\ServicoBase;
use App\Models\ServicoBaseAgravante;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Servico;

class ServicoBaseController extends Controller
{
    public function index()
    {
        $servicos = ServicoBase::paginate(10);

        return view('servicos/base/index', [
            'servicos' => $servicos
        ]);
    }

    public function update($id)
    {
        if (empty($base = ServicoBase::find($id))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $servicos = Servico::pluck('descricao', 'id');
        $metrica  = [
            '1' => 'R$ - Reais',
            '2' => '% - Porcentagem'
        ];

        return view('servicos/base/update', [
            'base'     => $base,
            'metrica'  => $metrica,
            'servicos' => $servicos
        ]);

    }

    public function create()
    {
        $servicos = Servico::pluck('descricao', 'id');
        $metrica  = [
            '1' => 'R$ - Reais',
            '2' => '% - Porcentagem'
        ];

        return view('servicos/base/create', [
            'metrica'  => $metrica,
            'servicos' => $servicos
        ]);
    }

    /**
     * Converts the name of a base service into a valid URL-like string, with only letters, numbers and dashes.
     *
     * @param string $string  The unsanitized title string of the base service
     * @return string         The sanitized string, separated by dashes
     *
     * @example nameToSlug("Olá, voçê está aqui?") === "ola-voce-esta-aqui"
     */
    private static function nameToSlug($string)
    {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r', '/' => '-', ' ' => '-'
        );

        // Remove duplicated spaces
        $stripped = trim(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string));

        // Fix invalid characters
        $replaced = strtolower(strtr($string, $table));

        // Allow only letters, numbers and dashes
        $filtered = preg_replace('/[^a-z0-9-]+/', '-', $replaced);

        // Remove duplicated dashes
        $distinct = implode("-", array_filter(explode('-', $filtered)));

        // Limit the size of the string to 100 characters
        return (strlen($distinct) > 100) ? substr($distinct, 0, 100) : $distinct;
    }

    public function save(Request $request)
    {
        if (empty($base = ServicoBase::find($request->input('id')))) {
            $base = new ServicoBase();
        }

        $validator = Validator::make(Input::all(), $base->rulesToPersist);

        if ($validator->fails()) {
            return response()->json([], 422, [], JSON_PRETTY_PRINT);
        }

        if (empty($servico = Servico::find($request->input('id_servico')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $base->id_servico     = $servico->id;
        $base->valor          = $request->input('valor');
        $base->nome           = $request->input('nome');
        $base->descricao      = $request->input('descricao');
        $base->url            = $request->input('url') ? $request->input('url') : $this->nameToSlug($base->nome);
        $base->flag_escondido = $request->input('invisivel_no_frontend') === '1' || $request->input('invisivel_no_frontend') === 1 ? 1 : 0;
        if ($base->url == null || $base->url === '' || !$base->url) {
            $base->url = 'base_service_'.$servico->id.'_'.mt_rand(999,100000);
        }
        $base->data_cadastro = date("Y-m-d H:i:s");

        try {

            if (! $base->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function remove(Request $request)
    {

        if (empty($servico = ServicoBase::find($request->input('id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        try {

            if (! $servico->delete()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function buscaDescricao(Request $request)
    {
        $response = [
            'success' => false,
        ];

        if ($request->isMethod('post')) {

            if (!empty($request->input('id_base'))) {
                $base = ServicoBase::find($request->input('id_base'));

                if (!is_null($base)) {

                    $response = [
                        'success' => true,
                        'descricao' => $base->descricao,
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function buscaAgravantes(Request $request)
    {
        $response = [
            'success' => false,
        ];

        if ($request->isMethod('post')) {

            if (!empty($request->input('id_base'))) {
                $base = ServicoBase::find($request->input('id_base'));

                if (!is_null($base)) {
                    $adicionais = $base->geraAgravantes($request->input('id'));

                    $response = [
                        'success' => true,
                        'fields' => $adicionais,
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
