<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use App\Models\Cidade;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

use DB;

class WebServiceController extends Controller
{
    public function cep($id)
    {
        $response = @file_get_contents('http://cep.republicavirtual.com.br/web_cep.php?formato=json&cep=' . $id);

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function estados(Request $request)
    {
        if ($request->ajax()) {
            $page = Input::get('page');
            $resultCount = 25;

            $offset = ($page - 1) * $resultCount;

            $services = Estado::where('nome', 'LIKE', '%' . Input::get("term") . '%')->orderBy('nome')->skip($offset)->take($resultCount)->get(['id', DB::raw('nome as text')]);
            $count = Count(Estado::where('nome', 'LIKE', '%' . Input::get("term") . '%')->orderBy('nome')->get(['id',DB::raw('nome as text')]));
            $endCount = $offset + $resultCount;
            $morePages = $count > $endCount;

            $results = array(
                "results" => $services,
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }
    }

    public function cidades(Request $request)
    {
        if ($request->ajax()) {
            $page = Input::get('page');
            $resultCount = 25;

            $offset = ($page - 1) * $resultCount;

            $services = Cidade::where('nome', 'LIKE', '%' . Input::get("term") . '%')->orderBy('nome')->skip($offset)->take($resultCount)->get(['id', DB::raw('nome as text')]);
            $count = Count(Cidade::where('nome', 'LIKE', '%' . Input::get("term") . '%')->orderBy('nome')->get(['id',DB::raw('nome as text')]));
            $endCount = $offset + $resultCount;
            $morePages = $count > $endCount;

            $results = array(
                "results" => $services,
                "pagination" => array(
                    "more" => $morePages
                )
            );

            return response()->json($results);
        }
    }
}
