<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\PerfilConta as PerfilConta;
use DB;

class PerfilContaController extends Controller
{
    public function novo()
    {
        return view('admin/novo-perfil-conta');
    }

    public function salvar(Request $request)
    {

        $response = [
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            $perfilConta = new PerfilConta();
            $perfilConta->descricao = $request->input('descricao');
            $perfilConta->data_cadastro = date("Y-m-d H:i:s");


            $rules = array(
                'descricao' => 'required|min:5'
            );

            $validator = Validator::make(Input::all(), $rules);

            if (!$validator->fails()) {
                if ($perfilConta->save()) {
                    $response = [
                        'success' => true,
                    ];
                }
            }

        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }


    public function findAll()
    {
        $profiles = PerfilConta::where('id', '<>', PerfilConta::PERFIL_ADMIN)
            ->get([
                'id',
                DB::raw('descricao as text')
            ]);

        return response()->json([
            'results' => $profiles
        ]);
    }
}
