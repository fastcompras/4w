<?php

namespace App\Http\Controllers;

use App\Models\Cidade;
use App\Models\Estado;
use App\Models\Regiao;
use App\Models\Bairro;
use App\Models\ServicoBase;
use Illuminate\Http\Request;

class HelperController extends Controller
{
    public function getCidades(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            if (!empty($request->input('uf'))) {
                $estado = Estado::where('uf', '=', $request->input('uf'))->first();

                $cidades = [];
                if ($estado) {
                    $cidades = Cidade::orderBy('nome', 'asc')->where('id_estado', '=', $estado->id)->get();
                }

                $response = [
                    'success' => true,
                    'id_estado' => $estado->id,
                    'nome_estado' => $estado->nome,
                    'cidades' => $cidades
                ];
            } else {
                if (!empty($request->input('id'))) {
                    $estado = Estado::where('id', '=', $request->input('id'))->first();

                    $cidades = [];
                    if ($estado) {
                        $cidades = Cidade::orderBy('nome', 'asc')->where('id_estado', '=', $estado->id)->get();
                    }

                    $response = [
                        'success' => true,
                        'id_estado' => $estado->id,
                        'nome_estado' => $estado->nome,
                        'cidades' => $cidades
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function getRegioesPorCidade(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            if (!empty($request->input('id_cidade'))) {
                $regioes = Regiao::where('id_cidade', '=', $request->input('id_cidade'))->get();

                $response = [
                    'success' => true,
                    'regioes' => $regioes
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function getBairrosPorRegiao(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            if (!empty($request->input('id_regiao'))) {
                $bairros = Bairro::where('id_regiao', '=', $request->input('id_regiao'))->get();

                $response = [
                    'success' => true,
                    'bairros' => $bairros
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function getServicosBase(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            if (!empty($request->input('id_servico'))) {
                $servicos = ServicoBase::where('id_servico', '=', $request->input('id_servico'))->get();

                $response = [
                    'success' => true,
                    'servicos' => $servicos
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
