<?php

namespace App\Http\Controllers;

use App\Models\Cidade;
use App\Models\Endereco;
use App\Models\Estado;
use App\Models\Notificacao;
use App\Models\Telefone;
use App\Models\UsuarioPrestaServico;
use App\Models\UsuarioTemDocumento;
use App\Models\UsuarioTemEndereco;
use App\Models\UsuarioTemTelefone;
use Illuminate\Http\Request;
use App\Models\PerfilConta;
use App\Models\User;
use App\Models\Documento;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use File;
use League\Flysystem\Exception;
use Mail;
use DB;

class UsuarioController extends Controller
{
    public function editar() {
        $usuario = Auth::user();
        $cidade = Cidade::find($usuario->id_cidade);
        $estado = $cidade ? Estado::find($cidade->id_estado) : null;
        $estadoId = $estado ? $estado->id : null;

        if ($usuario->isPrestador()) {
            abort(403);
        }

        return view('usuarios/editar', ['usuario' => $usuario, 'id_estado' => $estadoId]);
    }

    public function perfil($id)
    {
        $user       = User::findOrFail($id);
        $loggedUser = Auth::user();
        $perfis     = PerfilConta::pluck('descricao', 'id');

        // Administrador
        if (isset($perfis[3])) {
            $perfis->forget(3);
        }

        if (!$loggedUser->isAdmin() && $loggedUser->id != $user->id) {
            abort(403);
        }

        $servicosPrestados = $user->isPrestador() ? $user->servicos : [];

        return view('usuarios/perfil', [
            'usuario' => $user,
            'perfis' => $perfis,
            'servicosPrestados' => $servicosPrestados
        ]);
    }

    public function atualizarImagem(Request $request)
    {
        $usuario = Auth::user();

        if (Input::hasFile('croppedImage')) {
            $path = public_path().DIRECTORY_SEPARATOR.'documentos';
            $destinationPath = $path . '/' . $usuario->email;

            $avatar = Input::file('croppedImage');
            $avatarName = "avatar.png";
            $avatarUrl = "documentos/" . $usuario->email . '/' . $avatarName;

            if(substr($avatar->getMimeType(), 0, 5) != 'image')  {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            } else {
                if ($avatar->move($destinationPath, $avatarName)) {
                    $usuario->salvaDocumento($avatarUrl, Documento::TIPO_AVATAR);
                    $usuario->avatar = '/'.$avatarUrl;
                    $usuario->save();
                }
            }
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function atualizar(Request $request)
    {
        $response = [
            'msg' => 'Erro ao atualizar sua conta! Por favor, tente novamente!',
            'success' => false,
        ];

        $usuario = Auth::user();

        if ($usuario) {
            $usuario->nome = $request->input('nome');
            $usuario->email = $request->input('email');

            if (!empty($request->input('senha'))) {
                $usuario->password = bcrypt($request->input('senha'));
            }

            $usuarioTelefone = UsuarioTemTelefone::findByUser($usuario->id);

            if ($usuarioTelefone) {
                $telefone = Telefone::find($usuarioTelefone->id_telefone);

                if ($telefone) {
                    $telefone->numero = $request->input('telefone');
                    $telefone->save();
                }
            }
        }

        $rules = array(
            'email' => 'required|min:5',
            'telefone' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);

        if (!$validator->fails()) {
            if ($usuario->save()) {
                $response = [
                    'msg' => 'Conta atualizada com sucesso!',
                    'success' => true,
                    'usuario' => $usuario,
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function atualizarLocalizacao(Request $request)
    {
        $response = [
            'msg' => 'Erro ao salvar a localização! Por favor, tente novamente!',
            'success' => false,
        ];

        if ($request->isMethod('put')) {

            $usuario = Auth::user();

            if (!is_null($usuario)) {
                $usuario->id_cidade = $request->input('id_cidade');

                if ($usuario->save()) {
                    $usuarioEndereco = UsuarioTemEndereco::where('id_usuario', '=', $usuario->id)->first();

                    if (!is_null($usuarioEndereco)) {
                        $endereco = Endereco::find($usuarioEndereco->id_endereco);

                        if (!is_null($endereco)) {
                            $endereco->cep = $request->input('cep');
                            $endereco->logradouro = $request->input('logradouro');
                            $endereco->numero = $request->input('numero');

                            if (!empty($request->input('complemento'))) {
                                $endereco->complemento = $request->input('complemento');
                            }

                            $endereco->bairro = $request->input('bairro');

                            $rules = array(
                                'logradouro' => 'required|min:5',
                                'numero' => 'required',
                                'cep' => 'required',
                                'bairro' => 'required',
                            );

                            $validator = Validator::make(Input::all(), $rules);

                            if (!$validator->fails()) {
                                if ($endereco->save()) {
                                    $response = [
                                        'msg' => 'Localização atualizada com sucesso!',
                                        'success' => true,
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function ativarDesativarPrestador(Request $request)
    {
        $response = [
            'msg' => 'Erro ao executar esta operação! Por favor, tente novamente!',
            'success' => false,
        ];

        if (!empty($usuario = User::find($request->input('id')))) {

            if ($usuario) {
                //Caso esteja bloqueado reativamos, senão suspendemos
                if ($usuario->aprovado == User::USUARIO_BLOQUEADO) {
                    $usuario->aprovado = User::USUARIO_APROVADO;
                } else {
                    $usuario->aprovado = User::USUARIO_BLOQUEADO;
                }

                if ($usuario->save()) {
                    $response = [
                        'success' => true,
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function tomadores()
    {
        $tomadores = User::where('id_perfil_conta', '=', PerfilConta::PERFIL_TOMADOR)->paginate(10);

        return view('usuarios/tomadores/index', ['tomadores' => $tomadores]);
    }

    public function prestadores()
    {
        $ids = [User::USUARIO_APROVADO, User::USUARIO_BLOQUEADO];

        $prestadores = User::where('id_perfil_conta', '=', PerfilConta::PERFIL_PRESTADOR)->whereIn('aprovado', $ids)->paginate(10);

        return view('usuarios/prestadores/index', ['prestadores' => $prestadores]);
    }

    public function prestadoresPendentes()
    {
        $prestadores = User::where('id_perfil_conta', '=', PerfilConta::PERFIL_PRESTADOR)->where('aprovado','=','0')->paginate(10);

        return view('usuarios/prestadores/pendentes', ['prestadores' => $prestadores]);
    }

    public function prestadoresReprovados()
    {
        $prestadores = User::where('id_perfil_conta', '=', PerfilConta::PERFIL_PRESTADOR)->where('aprovado','=','2')->paginate(10);

        return view('usuarios/prestadores/reprovados', ['prestadores' => $prestadores]);
    }

    public function aprovarPrestador(Request $request)
    {
        $response = [
            'msg' => 'Erro ao aprovar o prestador de serviço',
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            $prestador = User::find($request->input('id'));

            if (!is_null($prestador)) {
                $existeUsuario = User::where('email', '=', $prestador->email)->where('id_perfil_conta', '=', PerfilConta::PERFIL_TOMADOR)->get();

                //Usuário já possui uma conta
                if (count($existeUsuario) > 0) {
                    $existeUsuario[0]->id = $prestador->id;
                    $existeUsuario[0]->id_perfil_conta = PerfilConta::PERFIL_PRESTADOR;
                    $existeUsuario[0]->aprovado = User::USUARIO_APROVADO;

                    $documentos = array();
                    foreach ($prestador->documentos as $documento) {
                        array_push($documentos, $documento->id);
                    }

                    if ($prestador->delete()) {

                        if ($existeUsuario[0]->save()) {
                            foreach ($documentos as $docs) {
                                $usuarioTemDocumentacao = new UsuarioTemDocumento();
                                $usuarioTemDocumentacao->id_usuario = $existeUsuario[0]->id;
                                $usuarioTemDocumentacao->id_documento = $docs;
                                $usuarioTemDocumentacao->save();
                            }
                        }

                        $response = [
                            'msg' => 'Prestador de serviço aprovado com sucesso!',
                            'success' => 'true',
                        ];
                    }
                } else  {
                    $prestador->aprovado = User::USUARIO_APROVADO;

                    if ($prestador->save()) {
                        $response = [
                            'msg' => 'Prestador de serviço aprovado com sucesso!',
                            'success' => 'true',
                        ];
                    }

                    Mail::send('emails.prestadores.prestador-aprovado', ['prestador' => $prestador->toArray(), 'title' => 'Agora você é 4W!'], function($message) use ($prestador) {
                        $message->to($prestador->email);
                        $message->subject('Seu cadastro foi aprovado');
                    });
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function rejeitarPrestador(Request $request)
    {
        $response = [
            'msg' => 'Erro ao rejeitar o prestador de serviço',
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            $prestador = User::find($request->input('id'));

            if (!is_null($prestador)) {
                $prestador->aprovado = User::USUARIO_REPROVADO;

                if ($prestador->save()) {

                    Mail::send('emails.prestadores.prestador-rejeitado', ['prestador' => $prestador->toArray(), 'title' => 'Obrigado pelo seu interesse na plataforma 4W!'], function($message) use ($prestador) {
                        $message->to($prestador->email);
                        $message->subject('Cadastro não aprovado');
                    });

                    $response = [
                        'msg' => 'Prestador de serviço rejeitado com sucesso!',
                        'success' => true,
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function documentosPendentes()
    {
        $usuario = Auth::user();

        $documentos = $usuario->documentos;

        return view('usuarios/prestadores/documentos-pendentes', ['usuario' => $usuario, 'documentos' => $documentos]);
    }

    public function agenda()
    {
        $usuario = Auth::user();

        return view('usuarios/prestadores/agenda', ['usuario' => $usuario]);
    }

    public function enviaDocumentacao(Request $request)
    {

        $qtdEnviada = 0;
        $usuario = Auth::user();

        $avatar = Input::file('avatar');
        $rg = Input::file('rg');
        $cpf = Input::file('cpf');
        $mei = Input::file('mei');
        $residencia = Input::file('residencia');
        $antecedentes = Input::file('antecedentes');

        $path = 'documentos';
        $destinationPath = $path . '/' . $usuario->email;
        $extensoesPermitidas = array("pdf", "png", "jpeg", "jpg");
        $extensoesPermitidasAvatar = array("jpeg", "jpg", "bmp", "png");

        //Cria diretório caso não exista
        if(!File::exists($destinationPath)) {
            $createDirectory = File::makeDirectory($destinationPath, 0777);
        }

        try {
            //Manipula avatar do prestador
            if (Input::hasFile('avatar')) {
                $qtdEnviada++;
                $avatarName = "avatar." . $avatar->getClientOriginalExtension();
                $avatarUrl = "documentos/" . $usuario->email . '/' . $avatarName;

                if(!in_array($avatar->getClientOriginalExtension(), $extensoesPermitidasAvatar)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize JPEG ou PNG.';

                    return response()->json($response, 422, [], JSON_PRETTY_PRINT);
                } else {

                    if ($avatar->move($destinationPath, $avatarName)) {
                        $usuario->salvaDocumento($avatarUrl, Documento::TIPO_AVATAR);
                        $usuario->avatar = '/'.$avatarUrl;
                        $usuario->save();
                    }
                }
            }

            //Manipula documento RG
            if (Input::hasFile('rg')) {
                $qtdEnviada++;
                $rgName = "documento_rg." . $rg->getClientOriginalExtension();
                $urlRg = "documentos/" . $usuario->email . '/' . $rgName;

                if(!in_array($rg->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 422, [], JSON_PRETTY_PRINT);
                } else {

                    if ($rg->move($destinationPath, $rgName)) {
                        $usuario->salvaDocumento($urlRg, Documento::TIPO_RG);
                    }
                }
            }

            //Manipula documento CPF
            if (Input::hasFile('cpf')) {
                $qtdEnviada++;
                $cpfName = "documento_cpf." . $cpf->getClientOriginalExtension();
                $urlCpf = "documentos/" . $usuario->email . '/' . $cpfName;

                if(!in_array($cpf->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 422, [], JSON_PRETTY_PRINT);
                } else {

                    if ($cpf->move($destinationPath, $cpfName)) {
                        $usuario->salvaDocumento($urlCpf, Documento::TIPO_CPF);
                    }
                }
            }

            //Manipula documento MEI
            if (Input::hasFile('mei')) {
                $qtdEnviada++;
                $meiName = "documento_mei." . $mei->getClientOriginalExtension();
                $urlMei = "documentos/" . $usuario->email . '/' . $meiName;

                if(!in_array($mei->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 422, [], JSON_PRETTY_PRINT);
                } else {

                    if ($mei->move($destinationPath, $meiName)) {
                        $usuario->salvaDocumento($urlMei, Documento::TIPO_MEI);
                    }
                }
            }

            //Manipula documento de comprovante de residência
            if (Input::hasFile('residencia')) {
                $qtdEnviada++;
                $residenciaName = "documento_residencia." . $residencia->getClientOriginalExtension();
                $urlResidencia = "documentos/" . $usuario->email . '/' . $residenciaName;

                if(!in_array($residencia->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 422, [], JSON_PRETTY_PRINT);
                } else {

                    if ($residencia->move($destinationPath, $residenciaName)) {
                        $usuario->salvaDocumento($urlResidencia, Documento::TIPO_RESIDENCIA);
                    }
                }
            }

            //Manipula documento de comprovante de bons antecedentes
            if (Input::hasFile('antecedentes')) {
                $qtdEnviada++;
                $antecedentesName = "documento_antecedentes." . $antecedentes->getClientOriginalExtension();
                $urlAntecedentes = "documentos/" . $usuario->email . '/' . $antecedentesName;

                if(!in_array($antecedentes->getClientOriginalExtension(), $extensoesPermitidas)) {
                    $response['mensagem'] = 'Ops! Extensão não permitida! Por favor, utilize PDF, JPG ou PNG.';

                    return response()->json($response, 422, [], JSON_PRETTY_PRINT);
                } else {

                    if ($antecedentes->move($destinationPath, $antecedentesName)) {
                        $usuario->salvaDocumento($urlAntecedentes, Documento::TIPO_ANTECEDENTES);
                    }
                }
            }

            if ($qtdEnviada > 0) {
                $response = ['mensagem' => 'Documentação enviada com sucesso!'];
            } else {
                $response['mensagem'] = 'Você deve selecionar no mínimo 1 documento';
                return response()->json($response, 422, [], JSON_PRETTY_PRINT);
            }

        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
        }


        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function visualizarDocumentacao($id) {
        $usuario = User::find($id);

        if (!is_null($usuario)) {
            return view('usuarios/prestadores/visualizar-documentacao', ['usuario' => $usuario, 'documentos' => $usuario->documentos]);
        }
    }

    public function download(Request $request, $idUsuario, $idTipo)
    {
        $usuario = User::find($idUsuario);

        foreach ($usuario->documentos as $doc) {
            if ($doc->tipo == $idTipo) {
                $nome = $doc->url;
            }
        }

        return response()->download($nome);
    }

    public function buscaResumo(Request $request)
    {
        $response = [
            'msg' => 'Erro ao buscar as informações do usuário',
            'success' => false,
        ];

        if ($request->isMethod('post')) {

            if (!empty($request->input('id'))) {
                $usuario = User::find($request->input('id'));

                if (!is_null($usuario)) {
                    $response = [
                        'data' => $usuario->buscaResumo($request->input('id_oportunidade')),
                        'success' => true,
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function minhasAvaliacoes(Request $request)
    {
        $avaliacoes = [];

        if ($request->isMethod('get')) {

            $usuario = Auth::user();

            if (!is_null($usuario)) {
                $avaliacoes = $usuario->buscaAvaliacoes();

            }
        }

        return response()->json($avaliacoes, 200, [], JSON_PRETTY_PRINT);
    }

    public function marcarNotificacoesLidas(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        if ($request->isMethod('post')) {

            if (!empty($request->input('notificacoes'))) {
                foreach ($request->input('notificacoes') as $key => $value) {
                    $notificacao = Notificacao::find($value);

                    if (!is_null($notificacao)) {
                        $notificacao->data_leitura = date("Y-m-d H:i:s");
                        $notificacao->save();
                    }
                }
            }

            $response = [
                'success' => true,
            ];
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function buscaResumoPrestador(Request $request)
    {
        $response = [
            'msg' => 'Erro ao buscar as informações deste usuário!',
            'success' => false,
        ];

        if ($request->isMethod('post')) {

            if (!empty($request->input('id'))) {

                $usuario = User::find($request->input('id'));

                if (!is_null($usuario)) {
                    $response = [
                        'usuario' => $usuario->buscaResumoConta(),
                        'success' => true,
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function minhaAgenda()
    {
        $usuario = Auth::user();
        $oportunidades = $usuario->buscaAgenda();

        return view('oportunidades/agenda', ['oportunidades' => $oportunidades]);
    }

    public function unreadNotifications()
    {
        $user = Auth::user();

        try {
            $notifications = $user->unreadNotifications();
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json(['notifications' => $notifications], 200, [], JSON_PRETTY_PRINT);
    }

    public function readNotifications(Request $request)
    {

        if (empty($notifications = $request->input('notifications'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        foreach ($notifications as $key => $value) {
            $notification = Notificacao::find($value);


            if ($notification) {
                $notification->data_leitura = date('Y-m-d H:i:s');

                try {

                    if (!$notification->save()) {
                        return response()->json([], 422, [], JSON_PRETTY_PRINT);
                    }

                } catch (\Exception $e) {
                    return response()->json([], 500, [], JSON_PRETTY_PRINT);
                }
            }
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function editarPerfil(Request $request)
    {
        if (empty($user = User::find($request->input('id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($accountProfile = PerfilConta::find($request->input('perfil')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (!empty(User::where('id', '<>', $user->id)->where('email', '=', $request->input('email'))->first())) {
            return response()->json([], 409, [], JSON_PRETTY_PRINT);
        }

        DB::beginTransaction();

        try {

            if ($user->isPrestador()) {

                // Remove all records
                UsuarioPrestaServico::where('id_usuario', '=' , $user->id)->delete();

                if (!empty($services = $request->input('servicos'))) {

                    foreach ($services as $key => $value) {

                        $serviceUser = new UsuarioPrestaServico();
                        $serviceUser->id_usuario = $user->id;
                        $serviceUser->id_servico = $value;

                        if (!$serviceUser->save()) {
                            DB::rollback();
                            return response()->json([], 422, [], JSON_PRETTY_PRINT);
                        }

                    }
                }
            }

            $user->id_perfil_conta = $accountProfile->id;
            $user->nome            = $request->input('nome');
            $user->email           = $request->input('email');
            $user->id_cidade       = $request->input('id_cidade');

            if (!$user->save()) {
                DB::rollback();
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

            // Update the phone of user
            if (! $user->updateOrCreatePhone($request->input('telefone'))) {
                DB::rollback();
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

            // Update the phone of user
            if (! $user->updateOrCreateLocation($request->all())) {
                DB::rollback();
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

        } catch (Exception $e) {
            DB::rollback();
            dd($e->getMessage());
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        DB::commit();

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

	public function loginAsUser($id)
	{
		if (! Auth::user()->isAdmin()) {
			Auth::logout();
			return Redirect::to('/login');
		}

		Auth::logout();
		Auth::loginUsingId($id);

		return redirect()->to('/');
	}
}
