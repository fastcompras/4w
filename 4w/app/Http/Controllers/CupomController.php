<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCupomRequest;
use App\Http\Requests\UpdateCupomRequest;
use App\Models\Cupom;

class CupomController extends Controller
{

	public function index()
	{
		$coupons = Cupom::all()->toArray();

		return response()->json($coupons, 200, [], JSON_PRETTY_PRINT);
	}

	public function find(Request $request)
	{
		if (empty($id = $request->id) || empty($model = Cupom::find($id))) {
			return response()->json([], 404, [], JSON_PRETTY_PRINT);
		}

		return response()->json($model->toArray(), 200, [], JSON_PRETTY_PRINT);
	}

	public function store(StoreCupomRequest $request)
	{
		$validated = $request->validated();

		if (! $validated) {
			return response()->json($request->messages(), 422, [], JSON_PRETTY_PRINT);
		}

		try {
			$model = Cupom::create($request->all());
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
		}

		return response()->json($model->attributesToArray(), 201, [], JSON_PRETTY_PRINT);
	}

	public function update(UpdateCupomRequest $request)
	{
		if (empty($id = $request->id) || empty($model = Cupom::find($id))) {
			return response()->json($request->messages(), 404, [], JSON_PRETTY_PRINT);
		}

		$validated = $request->validated();

		if (! $validated) {
			return response()->json($request->messages(), 422, [], JSON_PRETTY_PRINT);
		}

		try {
			$model->fill($request->all());
			$model->save();
		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
		}

		return response()->json($model->attributesToArray(), 200, [], JSON_PRETTY_PRINT);
	}

	public function delete(Request $request)
	{
		if (empty($id = $request->id) || empty($model = Cupom::find($id))) {
			return response()->json([], 404, [], JSON_PRETTY_PRINT);
		}

		try {

			if (! $model->delete()) {
				return response()->json([], 422, [], JSON_PRETTY_PRINT);
			}

		} catch (\Exception $e) {
			return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
		}

		return response()->json([], 204, [], JSON_PRETTY_PRINT);
	}

}
