<?php

namespace App\Http\Controllers;

use App\Models\Oportunidade as Oportunidade;
use App\Helpers\MercadoPago;
use DB;
use Log;

class MPNotificationsController extends Controller
{
    public function orderNotification($id)
    {
        if (empty($id)) {
            return response()->json(['msg' => 'ID não pode ser vazio.'], 404, [], JSON_PRETTY_PRINT);
        }

        Log::useDailyFiles(storage_path().'/logs/mp/merchant/' . date('Y_m_d_H_i_s') . '_' . $id . '_order.log');
        Log::info('Requisição recebida às: -' . date('d/m/Y H:i:s'));

        try {

            $result = (new MercadoPago())->getMerchantOrder($id);
            $data   = isset($result['response']) ? $result['response'] : null;

            if (empty($data)) {
                return response()->json(['msg' => 'Corpo do retorno vazio.'], 404, [], JSON_PRETTY_PRINT);
            }

            if ($result['status'] != 200) {
                return response()->json(['msg' => 'Erro desconhecido'], $result['status'], [], JSON_PRETTY_PRINT);
            }

            if (empty($deal = Oportunidade::find($data["external_reference"]))) {
                return response()->json(['msg' => 'Oportunidade com o id ' . $data['external_reference'] . ' não encontrada na base.'], 404, [], JSON_PRETTY_PRINT);
            }

            // Update the deal with the order info
            if (! $deal->updateOrderMP($data)) {
                return response()->json(['msg' => 'Erro ao atualizar a oportunidade.'], 422, [], JSON_PRETTY_PRINT);
            }

        } catch (\Exception $e) {
            Log::emergency('Erro 500: ' . $e->getMessage());
            return response()->json(['msg' => 'Erro interno. Veja o log da integração :)'], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json(['msg' => 'Pedido atualizado com sucesso :)'], 200, [], JSON_PRETTY_PRINT);
    }

    public function paymentNotification($id)
    {
        if (empty($id)) {
            return response()->json(['code' => 404, 'msg' => 'ID não pode ser vazio.'], 200, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);

        Log::useDailyFiles(storage_path().'/logs/mp/payment/' . date('Y_m_d_H_i_s') . '_' . $id . '_payment.log');
        Log::info('Requisição recebida às: ' . date('d/m/Y H:i:s'));

        try {
            Log::info('Buscando informações do pagamento na API do Mercado Pago.');

            $result = (new MercadoPago())->getPaymentInfo($id);
            $data   = isset($result['response']) && isset($result['response']['collection']) ? $result['response']['collection'] : null;

            if (empty($data)) {
                Log::emergency('Erro. Corpo do retorno vazio.');
                return response()->json(['code' => 404, 'msg' => 'Corpo do retorno vazio.'], 200, [], JSON_PRETTY_PRINT);
            }

            Log::info('Informações recebidas da API do Mercado Pago.');

            if ($result['status'] != 200) {
                Log::emergency('Erro. Status diferente de 200. Status recebido: ' . $result['status']);
                return response()->json(['code' => $data['status'], 'msg' => 'Erro desconhecido'], 200, [], JSON_PRETTY_PRINT);
            }

            Log::info('Buscando oportunidade com id: #' . $data['external_reference'] . ' na base de dados.');

            if (empty($deal = Oportunidade::find($data["external_reference"]))) {
                Log::emergency('Erro. Oportunidade com id: #' . $data['external_reference'] . ' não encontrada na base.');
                return response()->json(['code' => 404, 'msg' => 'Oportunidade com o id ' . $data['external_reference'] . ' não encontrada na base.'], 200, [], JSON_PRETTY_PRINT);
            }

            Log::info('Oportunidade encontrada na base de dados.');
            Log::info('Iniciando atualização da oportunidade na base de dados.');

            // Update the deal with the order info
            if (! $deal->updatePaymentMP($data)) {
                Log::emergency('Erro. Não foi possível atualizar a oportunidade com id: #' . $data['external_reference']);
                return response()->json(['code' => 422, 'msg' => 'Erro ao atualizar a oportunidade.'], 200, [], JSON_PRETTY_PRINT);
            }

        } catch (\Exception $e) {
            Log::emergency('Erro. Comportamento inesperado: ' . $e->getMessage());
            return response()->json(['code' => 500, 'msg' => 'Erro interno. Veja o log da integração :)'], 200, [], JSON_PRETTY_PRINT);
        }

        Log::info('Oportunidade atualizada com sucesso com novo status de pagamento. Hora: ' . date('H:i:s'));
        return response()->json(['code' => 200, 'msg' => 'Pagamento atualizado com sucesso :)'], 200, [], JSON_PRETTY_PRINT);
    }
}
