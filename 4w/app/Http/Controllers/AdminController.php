<?php

namespace App\Http\Controllers;

use App\Models\Documento;
use App\Models\MotivoRejeicao;
use Illuminate\Http\Request;
use App\Models\Financeiro;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        $usuario = Auth::user();

        if (!$usuario->isAdmin()) {
            abort(403);
        }

        $configuracao = Financeiro::all()->first();

        return view('admin/financeiro/configuracao', ['configuracao' => $configuracao]);
    }

    public function salvar(Request $request)
    {
        if (empty($percentual = $request->input('percentual'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $configuracao = Financeiro::all()->first();

        if (!$configuracao) {
            $configuracao = new Financeiro();
        }

        $configuracao->percentual_sobre_servico = (float) $percentual;

        try {
            if (!$configuracao->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function removerDocumento(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        if (empty($documento = Documento::find($request->input('id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        try {
            if (!$documento->delete()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function motivosRejeicao()
    {
        $usuario = Auth::user();

        if (!$usuario->isAdmin()) {
            abort(403);
        }

        $motivos = MotivoRejeicao::where('id', '>', 0)->paginate(10);

        return view('admin/motivos/index', ['motivos' => $motivos]);
    }

    public function novoMotivoRejeicao()
    {
        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        $motivo = new MotivoRejeicao();

        return view('admin/motivos/novo', ['motivo' => $motivo]);
    }

    public function editarMotivoRejeicao($id)
    {
        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        if (empty($motivo = MotivoRejeicao::find($id))) {
            abort(404);
        }

        return view('admin/motivos/editar', ['motivo' => $motivo]);
    }

    public function salvarMotivoRejeicao(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        if (empty($descricao = $request->input('descricao'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $motivo = new MotivoRejeicao();
        $motivo->descricao = $descricao;

        try {
            if (!$motivo->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function atualizarMotivoRejeicao(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        if (empty($motivo = MotivoRejeicao::find($request->input('id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($descricao = $request->input('descricao'))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $motivo->descricao = $descricao;

        try {
            if (!$motivo->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function removerMotivoRejeicao(Request $request)
    {
        if (!Auth::user()->isAdmin()) {
            abort(403);
        }

        if (empty($motivo = MotivoRejeicao::find($request->input('id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        try {
            if (!$motivo->delete()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }
        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }
}
