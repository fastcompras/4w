<?php

namespace App\Http\Controllers;

use App\Models\ServicoBaseAgravante;
use App\Models\AgravanteOpcoes;
use App\Models\ServicoBase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\Servico;
use App\Helpers\Helper as Helper;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;

class ServicoBaseAgravanteController extends Controller
{
    public function index(Request $request)
    {
        $servicos = Servico::pluck('descricao', 'id');
        $servicosBase = ServicoBase::pluck('nome', 'id');

        try {

            $agravantes = ServicoBaseAgravante::search($request->all());

        } catch (\Exception $e) {
            abort(500);
        }

        return view('servicos/agravante/index', [
            'agravantes'    => $agravantes,
            'servicos'      => $servicos,
            'servicosBase'  => $servicosBase,
        ]);
    }

    public function novo()
    {

        $servicos = Servico::pluck('descricao', 'id');
        $servicosBase = ServicoBase::pluck('nome', 'id');
        $metrica = array('1' => 'R$', '2' => '%');
        $regras = array('1' => 'Simples', '2' => 'Acumulada');
        $tipos = array('1' => 'Texto', '2' => 'Checkbox', '3' => 'Radio');

        return view('servicos/agravante/novo', [
                'metrica' => $metrica,
                'servicos' => $servicos,
                'servicosBase' => $servicosBase,
                'regras' => $regras,
                'tipos' => $tipos,
            ]
        );
    }

    public function atualizar($id)
    {

        if (empty($agravante = ServicoBaseAgravante::find($id))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        $servicos = Servico::pluck('descricao', 'id');
        $servicosBase = ServicoBase::pluck('nome', 'id');
        $metrica = ['1' => 'R$', '2' => '%'];
        $regras = ['1' => 'Simples', '2' => 'Acumulada'];
        $tipos = ['1' => 'Texto', '2' => 'Checkbox', '3' => 'Radio'];

        return view('/servicos/agravante/atualizar', [
                'agravante' => $agravante->getObject(),
                'metrica' => $metrica,
                'servicos' => $servicos,
                'servicosBase' => $servicosBase,
                'regras' => $regras,
                'tipos' => $tipos,
            ]
        );
    }

    public function update(Request $request)
    {

        if (empty($agravante = ServicoBaseAgravante::find($request->input('id')))) {
            return response()->json(['msg' => 'Serviço não encontrado!'], 404, [], JSON_PRETTY_PRINT);
        }

        if ($agravante->conflitaOrdem($request->input('id_servico_base'), $request->input('ordem'))) {
            return response()->json(['msg' => 'Já existe um agravante com esta ordem!'], 409, [], JSON_PRETTY_PRINT);
        }

        $agravante->id_base_servico = $request->input('id_servico_base');
        $agravante->regra = $request->input('regra');
        $agravante->valor = Helper::moneyToMysql($request->input('valor'));
        $agravante->rotulo_formulario = $request->input('rotulo');
        $agravante->ordem_formulario = $request->input('ordem');
        $agravante->incluso_servico_base = $request->input('incluso');
        $agravante->tipo_formulario = $request->input('tipo_campo');
        $agravante->data_cadastro = date("Y-m-d H:i:s");

        try {

            if (!$agravante->save()) {
                return response()->json(['msg' => 'Erro ao atualizar o agravante!'], 404, [], JSON_PRETTY_PRINT);
            }

            if ($agravante->tipoCampo() == "radio") {

                if (!empty($request->input('opcoes'))) {
                    //Remove as opções anteriores e reinsere
                    $agravante->limpaOpcoesAntigas();

                    foreach ($request->input('opcoes') as $index => $obj) {
                        $opcao = new AgravanteOpcoes();
                        $opcao->id_servico_base_agravante = $agravante->id;
                        $opcao->descricao = $obj['descricao'];
                        $opcao->valor = $obj['valor'];
                        $opcao->save();
                    }

                }

            } else {
                //Remove as opções devido a alteração de tipo de campo
                $agravante->limpaOpcoesAntigas();
            }

        } catch (\Exception $e) {
            return response()->json(['msg' => $e->getMessage()], 404, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function save(Request $request)
    {

        $agravante = new ServicoBaseAgravante();
        $agravante->id_base_servico = $request->input('id_servico_base');
        $agravante->regra = $request->input('regra');
        $agravante->valor = Helper::moneyToMysql($request->input('valor'));
        $agravante->rotulo_formulario = $request->input('rotulo');
        $agravante->ordem_formulario = $request->input('ordem');
        $agravante->incluso_servico_base = $request->input('incluso');
        $agravante->tipo_formulario = $request->input('tipo_campo');
        $agravante->data_cadastro = date("Y-m-d H:i:s");

        DB::beginTransaction();

        try {

            if (!$agravante->save()) {
                return response()->json(['msg' => 'Erro ao salvar o agravante!'], 404, [], JSON_PRETTY_PRINT);
            }

            if ($agravante->conflitaOrdem($request->input('id_servico_base'), $request->input('ordem'))) {
                DB::rollBack();
                return response()->json(['msg' => 'Já existe um agravante com esta ordem!'], 409, [], JSON_PRETTY_PRINT);
            }

            if ($agravante->tipoCampo() == "radio") {

                if (!empty($request->input('opcoes'))) {

                    foreach ($request->input('opcoes') as $index => $obj) {
                        $opcao = new AgravanteOpcoes();
                        $opcao->id_servico_base_agravante = $agravante->id;
                        $opcao->descricao = $obj['descricao'];
                        $opcao->valor = $obj['valor'];
                        $opcao->save();
                    }

                }

            }

        } catch (\Exception $e) {
            dd($e->getMessage());
            return response()->json(['msg' => $e->getMessage()], 404, [], JSON_PRETTY_PRINT);
        }

        DB::commit();

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function remover(Request $request)
    {
        $response = [
            'msg' => 'Erro ao remover o agravante!',
            'success' => false,
        ];

        if ($request->isMethod('delete')) {
            $servico = ServicoBaseAgravante::find($request->input('id'));

            if ($servico->delete()) {
                $response = [
                    'msg' => 'Agravante removido com sucesso!!',
                    'success' => true,
                ];
            }

        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }
}
