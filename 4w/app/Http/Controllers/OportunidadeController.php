<?php

namespace App\Http\Controllers;

use App\Exceptions\InvalidCouponException;
use App\Helpers\MercadoPago;
use App\Models\AgravanteOpcoes;
use App\Models\AvaliacaoServico;
use App\Models\Cupom;
use App\Models\Financeiro;
use App\Models\MotivoRejeicao;
use App\Models\Notificacao;
use App\Models\ServicoBaseAgravante;
use App\Models\OportunidadeItem;
use App\Models\UsuarioCancelouServico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Servico;
use App\Models\Estado;
use App\Models\Oportunidade as Oportunidade;
use App\Models\OportunidadeTemCandidato as OportunidadeTemCandidato;
use App\Models\User;
use App\Models\ServicoBase;
use Illuminate\Support\Facades\Session;
use League\Flysystem\Exception;
use Validator;
use DateTime;
use Mail;
use MP;
use DB;
use Log;

class OportunidadeController extends Controller
{

    public function index()
    {
        $oportunidades = Oportunidade::where('id', '>', '0')->paginate(10);

        return view('oportunidades/index', ['oportunidades' => $oportunidades]);
    }

    public function agenda($id)
    {
        $usuarioLogado = Auth::user();

        if (!$usuarioLogado->isAdmin()) {
            abort(403);
        }

        if (empty($usuario = User::find($id))) {
            abort(404);
        }

        $oportunidades = $usuario->buscaAgenda();

        return view('usuarios/prestadores/agenda', ['usuario' => $usuario, 'oportunidades' => $oportunidades ? $oportunidades : []]);
    }

    /**
     * Visualização de todas os serviços pendentes de pagamento
     *
     * Request
     * -------
     *
     * GET /oportunidades/pendentes
     *
     */
    public function servicosPendentes()
    {
        $usuario = Auth::user();

        $ids = [Oportunidade::STATUS_AGUARDANDO_PAGAMENTO, Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL];

        $oportunidades = Oportunidade::where('id_usuario_solicitante', '=', $usuario->id)->whereIn('status', $ids)->paginate(10);

        return view('oportunidades/tomador/pendentes', ['oportunidades' => $oportunidades]);
    }

    /**
     * Visualização de todas os serviços pendentes de aprovação do prestador
     *
     * Request
     * -------
     *
     * GET /oportunidades/servicosAguardandoConfirmacao
     *
     */
    public function servicosAguardandoConfirmacao()
    {
        $usuario = Auth::user();

        $oportunidades = DB::table('oportunidade')
            ->leftJoin('servico', 'servico.id', '=', 'oportunidade.id_servico')
            ->leftJoin('oportunidade_tem_candidato', 'oportunidade_tem_candidato.id_oportunidade', '=', 'oportunidade.id')
            ->leftJoin('usuario', 'usuario.id', '=', 'oportunidade_tem_candidato.id_usuario')
            ->where('oportunidade.status', '=', Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL)
            ->where('oportunidade_tem_candidato.status', '=', OportunidadeTemCandidato::STATUS_CONVIDADO)
            ->where('oportunidade_tem_candidato.id_usuario', '=', $usuario->id)
            ->selectRaw('servico.descricao as servico, usuario.nome as usuario, oportunidade.*')->paginate(10);

        return view('oportunidades/prestador/pendentes', ['oportunidades' => $oportunidades]);
    }

    /**
     * Visualização de todas os serviços agendados para execução
     *
     * Request
     * -------
     *
     * GET /oportunidades/agendados
     */
    public function servicosAgendados()
    {
        $usuario = Auth::user();

        $oportunidades = Oportunidade::where(
                'id_usuario_solicitante', '=', $usuario->id
        )->where('status', '=', Oportunidade::STATUS_AGENDADO)->paginate(10);

        return view('oportunidades/tomador/ativas', ['oportunidades' => $oportunidades]);
    }

    /**
     * Visualização de todas os serviços encerrados ou cancelados
     *
     * Request
     * -------
     *
     * GET /oportunidades/historico
     */
    public function historicoServicos()
    {
        $usuario = Auth::user();

        $ids = [Oportunidade::STATUS_PAGAMENTO_COM_FALHA, Oportunidade::STATUS_CANCELADO, Oportunidade::STATUS_ENCERRADO];

        if ($usuario->getPerfil() == 'Usuário') {
            $oportunidades = Oportunidade::where('id_usuario_solicitante', '=', $usuario->id)->whereIn('status', $ids)->paginate(10);
        } elseif ($usuario->getPerfil() == 'Prestador') {
            $oportunidades = Oportunidade::where('id_prestador_selecionado', '=', $usuario->id)->whereIn('status', $ids)->paginate(10);
        }

        return view('oportunidades/historico', ['oportunidades' => $oportunidades]);
    }

    public function minhasAvaliacoes()
    {
        $usuario = Auth::user();

        if ($usuario->isPrestador()) {
            return view('/oportunidades/prestador/avaliacoes');
        }
    }

    public function visualizar($id = null, Request $request)
    {
        if (empty($oportunidade = Oportunidade::find($id))) {
            abort(404);
        }

        $usuario = Auth::user();

        if ($usuario->isPrestador() && !$oportunidade->ehCandidato($usuario->id) && $oportunidade->usuario->id != $usuario->id) {
            abort(403);
        }

        if ($usuario->isTomador()) {

            if ($oportunidade->id_usuario_solicitante != $usuario->id) {
                abort(403);
            } else {
                return view('oportunidades/tomador/visualizar', ['oportunidade' => $oportunidade, 'usuario' => $usuario]);
            }

        }

        if ($usuario->isAdmin()) {

            $servicos = Servico::pluck('descricao', 'id');
            $estados  = Estado::orderBy('nome', 'asc')->pluck('nome', 'uf');

            if ($request->isMethod('get')) {
                $prestadores = User::filtraPrestadores($request);
            }

            return view('oportunidades/visualizar', [
                'oportunidade' => $oportunidade,
                'estados'      => $estados,
                'servicos'     => $servicos,
                'prestadores'  => $prestadores
            ]);

        } else if ($usuario->isPrestador()) {
            $motivosRejeicao = MotivoRejeicao::all();

            //Caso o prestador seja o solicitante desta oportunidade
            if ($oportunidade->id_usuario_solicitante == $usuario->id) {

                return view('oportunidades/tomador/visualizar', [
                    'oportunidade' => $oportunidade,
                    'usuario'      => $usuario
                ]);

            } else {
                return view('oportunidades/prestador/visualizar', [
                    'oportunidade'    => $oportunidade,
                    'motivosRejeicao' => $motivosRejeicao,
                    'usuario'         => $usuario
                ]);
            }
        }
    }

    public function novo()
    {
        return view('oportunidades/nova', [
            'oportunidade' => new Oportunidade()
        ]);
    }

    public function editar($id = null)
    {
        $usuario = Auth::user();
        $oportunidade = null;

        if (empty($oportunidade = Oportunidade::find($id))) {
            abort(404);
        }

        if ($usuario->id != $oportunidade->id_usuario_solicitante || ($oportunidade->status != Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL && $oportunidade->status != Oportunidade::STATUS_AGUARDANDO_PAGAMENTO)) {
            abort(403);
        }

        return view('oportunidades/nova', [
            'oportunidade' => $oportunidade
        ]);
    }

    /**
     * Realiza o reagendamento de uma oportunidade específica
     *
     *  Request
     *  -------
     *
     *  POST /oportunidades/reagendar
     *  {
     *      "id": "int",
     *      "data_execucao": "string",
     *  }
     *
     *  Response
     *  --------
     *
     *  200 OK
     *  404 Not Found
     *  422 Unprocessable Entity
     *  500 Internal Server Error
     */
    public function reagendar(Request $request)
    {
        $usuario = Auth::user();

        if (empty($oportunidade = Oportunidade::find($request->input('id')))) {
            abort(404);
        }

        if ($usuario->getPerfil() != 'Admin' && $usuario->id != $oportunidade->id_usuario_solicitante) {
            abort(403);
        }

        $candidatos = OportunidadeTemCandidato::where('id_oportunidade', '=', $oportunidade->id)->get();

        foreach ($candidatos as $candidato) {
            $linha = OportunidadeTemCandidato::find($candidato->id_oportunidade, $candidato->id_usuario);

            if ($linha) {

                if ($candidato->id_usuario != $oportunidade->id_prestador_selecionado) {
                    $linha->delete();
                } else {
                    $linha->status = 0;
                    $linha->save();
                }

            }
        }

        $oportunidade->data_execucao = $request->input('data_execucao');
        $oportunidade->id_prestador_selecionado = null;
        $oportunidade->status = Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL;

        if (!$oportunidade->save()) {
            return response()->json([], 422, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Remove uma oportunidade e cancela o pagamento no MP
     *
     *  Request
     *  -------
     *
     *  DELETE /oportunidades/remover
     *  {
     *      "id": "int"
     *  }
     *
     *  Response
     *  --------
     *
     *  200 OK
     *  404 Not Found
     *  422 Unprocessable Entity
     *  500 Internal Server Error
    */
    public function remover(Request $request)
    {
        if (empty($oportunidade = Oportunidade::find($request->input('id')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        try {
            if ($oportunidade->id_pagamento_mp) {
                $result = (new MercadoPago())->cancel_payment($oportunidade->id_pagamento_mp);

                if ($result["status"] == 200) {
                    if (!$oportunidade->delete()) {
                        return response()->json([], 422, [], JSON_PRETTY_PRINT);
                    }
                }
            } else {
                if (!$oportunidade->delete()) {
                    return response()->json([], 422, [], JSON_PRETTY_PRINT);
                }
            }
        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function salvar(Request $request)
    {
        $response = [
            'success' => false,
        ];

        $configuracao = Financeiro::all()->first();

        //Percentual sobre cada serviço para a 4w.
        $percentual = ($configuracao) ? ($configuracao->percentual_sobre_servico / 100) : 0.12;

        $data_execucao = $request->input('data_execucao') . " " . $request->input('hora_execucao');

        $oportunidade = new Oportunidade();
        $oportunidade->id_usuario_solicitante = Auth::user()->id;
        $oportunidade->id_servico = $request->input('id_servico');
        $oportunidade->id_cidade = $request->input('id_cidade');
        $oportunidade->id_servico_base = Session::get('servico_base');
        $oportunidade->informacoes_adicionais = $request->input('informacoes_adicionais');
        $oportunidade->cep = $request->input('cep');
        $oportunidade->logradouro = $request->input('logradouro');
        $oportunidade->numero = $request->input('numero');
        $oportunidade->complemento = $request->input('complemento');
        $oportunidade->sugestao_servico = $request->input('sugestao_servico');
        $oportunidade->status = Oportunidade::STATUS_AGUARDANDO_PAGAMENTO;
        $oportunidade->status_pagamento_mp = "pending";
        $oportunidade->bairro = $request->input('bairro');
        $oportunidade->data_execucao = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data_execucao)));
        $oportunidade->data_cadastro = date("Y-m-d H:i:s");

	    $oportunidade->valor_total = $request->input('valor');
	    $oportunidade->valor_4w = $oportunidade->valor_total * $percentual;


        $dataServico = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data_execucao)));
        $dataExecucao = (new DateTime($dataServico));
        $dataMinima = (new DateTime(date('Y-m-d H:i:s')))->modify('+45 hours');

        $diff = $dataMinima->diff($dataExecucao);

        if ($diff->invert == 1) {
            $response['erro'] = "Prezado usuário este prazo para execução do serviço não permite localizar um prestador qualificado em tempo habil para lhe atender!";
            return response()->json($response, 500, [], JSON_PRETTY_PRINT);
        } else {

            try {

                if (! $oportunidade->save()) {
                    return response()->json($response, 422, [], JSON_PRETTY_PRINT);
                }

	            // Coupon
	            if (! empty($cupom = Cupom::where('codigo', $request->input('cupom'))->first())) {
		            $oportunidade->id_cupom = $cupom->id;
		            $oportunidade->valor_total = $oportunidade->hasAvailableCupom() ? $oportunidade->getTotalWithDiscountCoupon() : $request->input('valor');
		            $oportunidade->useCoupon();
	            }

	            $oportunidade->valor_4w = $oportunidade->valor_total * $percentual;

	            $oportunidade->save();

                $itens = Session::get('servicos_adicionais');

                foreach ($itens as $adicional) {

                    if ($adicional['type'] == "radio") {
                        $servicoAdicional = AgravanteOpcoes::find($adicional['id']);
                        $id = $servicoAdicional->id_servico_base_agravante;
                        $idOpcao = $servicoAdicional->id;
                    } else {
                        $servicoAdicional = ServicoBaseAgravante::find($adicional['id']);
                        $id = $servicoAdicional->id;
                        $idOpcao = null;
                    }

                    if ($servicoAdicional) {
                        if ($adicional['type'] == "radio" && $adicional['value'] != 1) {
                            continue;
                        } else {
                            $item = new OportunidadeItem();
                            $item->id_oportunidade = $oportunidade->id;
                            $item->id_servico = $oportunidade->servico->id;
                            $item->id_base = Session::get('servico_base');
                            $item->id_agravante = $id;
                            $item->id_agravante_opcao = $idOpcao ?: 0;
                            $item->quantidade = $adicional['value'];
                            $item->data_cadastro = date('Y-m-d H:i:s');
                            $item->save();
                        }
                    }
                }

                Notificacao::novaOportunidade($oportunidade->id);

                $usuario = User::find($oportunidade->id_usuario_solicitante);

                $item = array(
                    'id' => $oportunidade->id,
                    'title' => 'Solicitação de Prestação de Serviço de ' . $oportunidade->servico->descricao,
                    'quantity' => 1,
                    'currency_id' => "BRL",
                    'unit_price' => (float) number_format($oportunidade->valor_total, 2),
                    "picture_url" => "https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
                    "description" => $oportunidade->getResumo(),
                );

                try {
                    $pagamento = new MercadoPago();
                    $preference = $pagamento->sendPayment($item, $usuario);

                    $oportunidade->id_pedido_mp = $preference['response']['id'];
                    $oportunidade->link_mercado_pago = $preference['response']['init_point'];
                    $oportunidade->save();

                    $response = [
                        'success' => true,
                        'id' => $oportunidade->id,
	                    'valor_total' => $oportunidade->valor_total,
                        'tipo_servico' => $oportunidade->servico->descricao,
                        'resumo' => $oportunidade->getResumo(),
                        'mercado_pago' => $preference['response']['init_point'],
                    ];

                } catch (\Exception $e){
                    return response()->json(['msg' => $e->getMessage()], 500, [], JSON_PRETTY_PRINT);
                }
            } catch (\Exception $e) {
                return response()->json(['msg' => $e->getMessage()], 500, [], JSON_PRETTY_PRINT);
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function atualizar(Request $request)
    {
        $response = [];

        if (!empty($oportunidade = Oportunidade::find($request->input('id')))) {
            $configuracao = Financeiro::all()->first();

            //Percentual sobre cada serviço para a 4w.
            $percentual = ($configuracao) ? ($configuracao->percentual_sobre_servico / 100) : 0.12;

            // Concatena data e hora que estão separadas no front-end
            $data_execucao = $request->input('data_execucao') . " " . $request->input('hora_execucao');

            $oportunidade->id_servico = $request->input('id_servico');
            $oportunidade->id_cidade = $request->input('id_cidade');
            $oportunidade->id_servico_base = Session::get('servico_base');
            $oportunidade->informacoes_adicionais = $request->input('informacoes_adicionais');
            $oportunidade->cep = $request->input('cep');
            $oportunidade->logradouro = $request->input('logradouro');
            $oportunidade->numero = $request->input('numero');
            $oportunidade->complemento = $request->input('complemento');
            $oportunidade->sugestao_servico = $request->input('sugestao_servico');
            $oportunidade->status_pagamento_mp = "pending";
            $oportunidade->bairro = $request->input('bairro');
            $oportunidade->data_execucao = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data_execucao)));
            $oportunidade->data_cadastro = date("Y-m-d H:i:s");

            $oldValue = $oportunidade->valor_total;
	        $oportunidade->valor_total = $request->input('valor');

	        // Coupon

	        if (empty($cupom = Cupom::where('codigo', $request->input('cupom'))->first())) {

	        	if ($oportunidade->id_cupom) {
	        		$oportunidade->updateAmountAndCouponHistory();
		        }

	        } else {

		        if ($oportunidade->id_cupom === null) {
			        $oportunidade->id_cupom = $cupom->id;
			        $oportunidade->valor_total = $oportunidade->hasAvailableCupom() ? $oportunidade->getTotalWithDiscountCoupon() : $request->input('valor');
			        $oportunidade->useCoupon();

		        } else {

		        	if ($oldValue != $oportunidade->valor_total) {
				        $oportunidade->updateAmountAndCouponHistory();
			        }
		        }

	        }

	        $oportunidade->valor_4w = $oportunidade->valor_total * $percentual;

	        $dataServico = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data_execucao)));
	        $dataExecucao = (new DateTime($dataServico));
	        $dataMinima = (new DateTime(date('Y-m-d H:i:s')))->modify('+45 hours');

	        $diff = $dataMinima->diff($dataExecucao);

	        if ($diff->invert == 1) {
                $response['erro'] = "Prezado usuário este prazo para execução do serviço não permite localizar um prestador qualificado em tempo habil para lhe atender!";
                return response()->json($response, 422, [], JSON_PRETTY_PRINT);
            } else {
                try {
                    if (!$oportunidade->save()) {
                        return response()->json($response, 422, [], JSON_PRETTY_PRINT);
                    }

                    $itens = Session::get('servicos_adicionais');
                    OportunidadeItem::where('id_oportunidade', '=', $oportunidade->id)->delete();

                    foreach ($itens as $adicional) {
                        if ($adicional['type'] == "radio") {
                            $servicoAdicional = AgravanteOpcoes::find($adicional['id']);
                            $id = $servicoAdicional->id_servico_base_agravante;
                            $idOpcao = $servicoAdicional->id;
                        } else {
                            $servicoAdicional = ServicoBaseAgravante::find($adicional['id']);
                            $id = $servicoAdicional->id;
                            $idOpcao = null;
                        }

                        if ($servicoAdicional) {
                            if ($adicional['type'] == "radio" && $adicional['value'] != 1) {
                                continue;
                            } else {
                                $item = new OportunidadeItem();
                                $item->id_oportunidade = $oportunidade->id;
                                $item->id_servico = $oportunidade->servico->id;
                                $item->id_base = Session::get('servico_base');
                                $item->id_agravante = $id;
                                $item->id_agravante_opcao = $idOpcao;
                                $item->quantidade = $adicional['value'];
                                $item->data_cadastro = date('Y-m-d H:i:s');
                                $item->save();
                            }
                        }
                    }

                    $usuario = User::find($oportunidade->id_usuario_solicitante);

                    $item = array(
                        'id' => $oportunidade->id,
                        'title' => 'Solicitação de Prestação de Serviço de ' . $oportunidade->servico->descricao,
                        'quantity' => 1,
                        'currency_id' => "BRL",
                        'unit_price' => (float) number_format($oportunidade->valor_total, 2),
                        "picture_url" => "https://www.mercadopago.com/org-img/MP3/home/logomp3.gif",
                        "description" => $oportunidade->getResumo(),
                    );

                    try {
                        $pagamento = new MercadoPago();
                        $preference = $pagamento->updateOrder($oportunidade->id_pedido_mp, $item, $usuario);

                        $oportunidade->link_mercado_pago = $preference['response']['init_point'];
                        $oportunidade->save();

                        $response = [
	                        'success' => true,
                            'id' => $oportunidade->id,
                            'valor_total' => $oportunidade->valor_total,
                            'tipo_servico' => $oportunidade->servico->descricao,
                            'resumo' => $oportunidade->getResumo(),
                            'mercado_pago' => $preference['response']['init_point'],
                        ];

                    } catch (\Exception $e){
                        return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
                    }
                } catch (\Exception $e) {
                    return response()->json($e->getMessage(), 500, [], JSON_PRETTY_PRINT);
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function selecionarPrestador(Request $request)
    {
        if (empty($oportunidade = Oportunidade::find($request->input('id_oportunidade')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($prestador = User::find($request->input('id_prestador_selecionado')))) {
            return response()->json([], 404, [], JSON_PRETTY_PRINT);
        }

        if ($prestador->id == $oportunidade->usuario->id) {
            return response()->json([], 409, [], JSON_PRETTY_PRINT);
        }

        // Caso o prestador já tenha cancelado este serviço
        // realiza a limpeza da tabela de cancelamento para
        // postular ele novamente.
        if ($prestador->cancelouServico($oportunidade->id)) {
            try {
                UsuarioCancelouServico
                    ::where('id_usuario',      '=', $prestador->id)
                    ->where('id_oportunidade', '=', $oportunidade->id)
                    ->delete();
            } catch (\Exception $e) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

        }

        $oferta = new OportunidadeTemCandidato();
        $oferta->id_oportunidade = $oportunidade->id;
        $oferta->id_usuario = $prestador->id;
        $oferta->status = 0;
        $oferta->data_cadastro = date("Y-m-d H:i:s");

        $oportunidade['cidade'] = $oportunidade->cidade->estado;
        $oportunidade['data_hora'] = $oportunidade->data_execucao->format('d/m/Y H:i:s');

        try {
            if (!$oferta->save()) {
                return response()->json([], 422, [], JSON_PRETTY_PRINT);
            }

            Notificacao::prestadorSelecionado($oportunidade->id, $prestador->id);

            Mail::send('emails.prestadores.prestador-selecionado', [
                'oportunidade' => $oportunidade->toArray(),
                'prestador' => $prestador->toArray(),
                'title' => 'Temos uma nova oferta de ' . $oportunidade->servico->descricao . ' para você!'
            ], function($message) use ($prestador) {
                $message->to($prestador->email);
                $message->subject('[URGENTE] Tem alguém precisando do seu serviço');
            });

        } catch (\Exception $e) {
            return response()->json([], 500, [], JSON_PRETTY_PRINT);
        }

        return response()->json([], 200, [], JSON_PRETTY_PRINT);
    }

    public function desvincularPrestador(Request $request)
    {
        $response = [
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            $oportunidade = Oportunidade::find($request->input('id_oportunidade'));
            $prestador = User::find($request->input('id_prestador_selecionado'));

            if ($prestador && $oportunidade) {
                $oferta = OportunidadeTemCandidato::where('id_oportunidade', '=', $oportunidade->id)->where('id_usuario', '=', $prestador->id);

                if ($oferta) {
                    if ($oferta->delete()) {

                        Notificacao::prestadorDesvinculado($oportunidade->id, $prestador->id);

                        $response = [
                            'success' => true,
                        ];
                    }
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Prestador aceita um serviço específico para executar
     *
     *  Request
     *  -------
     *
     *  POST /oportunidades/aceitarOferta
     *  {
     *      "id_oportunidade": "int",
     *      "id_prestador_selecionado": "int",
     *  }
     *
     *  Response
     *  --------
     *
     *  200 OK
     */
    public function aceitarOferta(Request $request)
    {
        $response = [
            'msg' => "Não foi possível realizar esta ação! Tente novamente!",
            'success' => false,
        ];

        $oportunidade = Oportunidade::find($request->input('id_oportunidade'));
        $prestador = User::find($request->input('id_prestador_selecionado'));
        $tomador = User::find($oportunidade->id_usuario_solicitante);


        if ($prestador && $oportunidade) {
            $oferta = OportunidadeTemCandidato::find($oportunidade->id, $prestador->id);

            if (!$oportunidade->possuiPrestador()) {

                if ($oferta) {
                    $oferta->id_usuario = $prestador->id;
                    $oferta->id_oportunidade = $oportunidade->id;
                    $oferta->status = OportunidadeTemCandidato::STATUS_ACEITO;
                    $msg = "Oferta aceita com sucesso!";

                    $oportunidade->id_prestador_selecionado = $prestador->id;
                    $oportunidade->qtd_prestador_selecionado = $oportunidade->qtd_prestador_selecionado + 1;
                    $oportunidade->status = Oportunidade::STATUS_AGENDADO;

                    if ($oferta->save() && $oportunidade->save()) {

                        $oportunidade['cidade'] = $oportunidade->cidade->estado;
                        $oportunidade['data_hora'] = $oportunidade->data_execucao->format('d/m/Y H:i:s');
                        $oportunidade['usuario'] = $oportunidade->usuario;
                        $oportunidade['prestador'] = $oportunidade->prestador;
                        $oportunidade['usuario_telefone'] = $oportunidade->usuario->getTelefone();
                        $oportunidade['prestador_telefone'] = $oportunidade->prestador->getTelefone();

                        Notificacao::prestadorAceitou($oportunidade->id);
                        Notificacao::prestadorEncontrado($oportunidade->id, $tomador->id);

                        Mail::send('emails.prestadores.confirmacao-servico', ['oportunidade' => $oportunidade->toArray(), 'prestador' => $prestador->toArray(), 'title' => 'Parabéns! Agora você tem a oportunidade de mostrar seu talento!'], function($message) use ($prestador) {
                            $message->to($prestador->email);
                            $message->subject('Confirmação de Serviço');
                        });

                        if ($oportunidade->qtd_prestador_selecionado > 1) {
                            Mail::send('emails.tomadores.troca-prestador-selecionado', ['oportunidade' => $oportunidade->toArray(), 'tomador' => $tomador->toArray(), 'title' => 'Ocorreu um imprevisto! Temos um novo profissional para lhe atender!'], function($message) use ($tomador) {
                                $message->to($tomador->email);
                                $message->subject('Troca de perfil de prestador');
                            });
                        } else {
                            Mail::send('emails.tomadores.prestador-selecionado', ['oportunidade' => $oportunidade->toArray(), 'tomador' => $tomador->toArray(), 'title' => 'Selecionamos o melhor profissional para você!'], function($message) use ($tomador) {
                                $message->to($tomador->email);
                                $message->subject('Perfil de prestador selecionado');
                            });
                        }

                        $response = [
                            'msg' => $msg,
                            'success' => true,
                        ];
                    }
                }

            } else {
                $response['msg'] = "Pedimos desculpa, mas um profissional já aceitou esta oferta!";
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Prestador rejeita um determinado serviço
     *
     *  Request
     *  -------
     *
     *  POST /oportunidades/aceitarOferta
     *  {
     *      "id_oportunidade": "int",
     *      "id_prestador_selecionado": "int",
     *  }
     *
     *  Response
     *  --------
     *
     *  200 OK
     */
    public function rejeitarOferta(Request $request)
    {
        $response = [
            'msg' => "Não foi possível realizar esta ação! Tente novamente!",
            'success' => false,
        ];

        $oportunidade = Oportunidade::find($request->input('id_oportunidade'));
        $prestador = User::find($request->input('id_prestador_selecionado'));


        if ($prestador && $oportunidade) {

            $oferta = OportunidadeTemCandidato::find($oportunidade->id, $prestador->id);

            if ($oferta) {

                if ($oferta->status == OportunidadeTemCandidato::STATUS_REJEITADO) {
                    $response['msg'] = "Você já rejeitou esta oferta de serviço!";

                    return response()->json($response, 200, [], JSON_PRETTY_PRINT);
                }

                $oferta->status = OportunidadeTemCandidato::STATUS_REJEITADO;
                $oferta->id_motivo_rejeicao = $request->input('id_motivo_rejeicao');

                $msg = "Oferta rejeitada com sucesso!";

                if ($oferta->save()) {

                    Notificacao::prestadorRejeitou($oportunidade->id, $prestador);

                    //Enviar para o administrador
                    Mail::send('emails.admin.prestador-rejeitou', ['prestador' => $prestador->toArray(), 'title' => 'Oferta rejeitada :('], function($message) use ($prestador) {
                        $message->to("lorenzo.kniss@gmail.com");
                        $message->subject('Oferta rejeitada pelo profissional');
                    });

                    $response = [
                        'msg' => $msg,
                        'success' => true,
                    ];
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Usuário (Prestador ou tomador) cancelam um serviço previamente acordado
     *
     *  Request
     *  -------
     *
     *  POST /oportunidades/aceitarOferta
     *  {
     *      "id_oportunidade": "int",
     *      "id_prestador_selecionado": "int",
     *  }
     *
     *  Response
     *  --------
     *
     *  200 OK
     *  500 Internal Server Error
     */
    public function cancelarServico(Request $request)
    {
        $response = [
            'msg' => "Não foi possível realizar esta ação! Tente novamente!",
            'success' => false,
        ];

        $oportunidade = Oportunidade::find($request->input('id_oportunidade'));
        $prestador = User::find($request->input('id_prestador_selecionado'));

        if ($prestador && $oportunidade) {
            $tomador = $oportunidade->usuario;
            $oportunidade->id_prestador_selecionado = null;
            $oportunidade->status = Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL;

            $msg = "Serviço cancelado com sucesso!";

            $oferta = OportunidadeTemCandidato::find($oportunidade->id, $prestador->id);

            if ($oferta) {
                $oferta->status = Oportunidade::STATUS_CANCELADO;

                if ($oportunidade->id_pagamento_mp) {

                    try {
                        if ($oportunidade->save() && $oferta->save()) {
                            if ($oportunidade->geraLogCancelamentoServico($prestador->id)) {

                                Mail::send('emails.prestadores.servico-cancelado', ['oportunidade' => $oportunidade->toArray(), 'prestador' => $prestador->toArray(), 'title' => 'Ops! Infelizmente este serviço foi cancelado pelo cliente!'], function($message) use ($prestador) {
                                    $message->to($prestador->email);
                                    $message->subject('Serviço Cancelado');
                                });

                                Mail::send('emails.tomadores.servico-cancelado', ['oportunidade' => $oportunidade->toArray(), 'tomador' => $tomador->toArray(), 'title' => 'Confirmamos a sua solicitação de cancelamento!'], function($message) use ($tomador) {
                                    $message->to($tomador->email);
                                    $message->subject('Serviço Cancelado');
                                });

                                $response = [
                                    'msg' => $msg,
                                    'success' => true,
                                ];
                            }
                        }
                    } catch (\Exception $e) {
                        return response()->json($response, 500, [], JSON_PRETTY_PRINT);
                    }

                } else {
                    if ($oportunidade->save() && $oferta->save()) {
                        if ($oportunidade->geraLogCancelamentoServico($prestador->id)) {

                            Mail::send('emails.prestadores.servico-cancelado', ['oportunidade' => $oportunidade->toArray(), 'prestador' => $prestador->toArray(), 'title' => 'Ops! Infelizmente este serviço foi cancelado pelo cliente!'], function($message) use ($prestador) {
                                $message->to($prestador->email);
                                $message->subject('Serviço Cancelado');
                            });

                            Mail::send('emails.tomadores.servico-cancelado', ['oportunidade' => $oportunidade->toArray(), 'tomador' => $tomador->toArray(), 'title' => 'Confirmamos a sua solicitação de cancelamento!'], function($message) use ($tomador) {
                                $message->to($tomador->email);
                                $message->subject('Serviço Cancelado');
                            });

                            $response = [
                                'msg' => $msg,
                                'success' => true,
                            ];
                        }
                    }
                }
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    /**
     * Prestador encerra a execução de um determinado serviço
     *
     *  Request
     *  -------
     *
     *  POST /oportunidades/finalizarServico
     *  {
     *      "id_oportunidade": "int",
     *  }
     *
     *  Response
     *  --------
     *
     *  200 OK
     */
    public function finalizarServico(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        $oportunidade = Oportunidade::find($request->input('id_oportunidade'));
        $prestador = Auth::user();

        if ($prestador && $oportunidade) {
            $tomador = $oportunidade->usuario;

            $oportunidade->data_encerramento = date("Y-m-d H:i:s");
            $oportunidade->comentarios_encerramento = $request->input('comentarios');
            $oportunidade->status = Oportunidade::STATUS_ENCERRADO;

            if ($oportunidade->save()) {

                Mail::send('emails.prestadores.conclusao-servico', ['oportunidade' => $oportunidade->toArray(), 'prestador' => $prestador->toArray(), 'title' => 'Parabéns! Agradecemos por mais um serviço prestado na 4W!'], function($message) use ($prestador) {
                    $message->to($prestador->email);
                    $message->subject('Serviço Concluído');
                });

                Mail::send('emails.tomadores.conclusao-servico', ['oportunidade' => $oportunidade->toArray(), 'tomador' => $tomador->toArray(), 'title' => 'Obrigado por escolher a 4W! Finalize e avalie o serviço prestado. É rápido e fácil!'], function($message) use ($tomador) {
                    $message->to($tomador->email);
                    $message->subject('Serviço Concluído');
                });

                //FIXME colocar em um cronjob
                /*Mail::send('emails.tomadores.lembrete-avaliacao', ['oportunidade' => $oportunidade->toArray(), 'tomador' => $tomador->toArray(), 'title' => 'Aguardamos a sua avaliação! É rápido e fácil!'], function($message) use ($tomador) {
                    $message->to($tomador->email);
                    $message->subject('Avalie nosso serviço!');
                });*/

                $response = [
                    'msg' => 'Serviço finalizado com sucesso!',
                    'success' => true,
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function cancelarSolicitacaoServico(Request $request)
    {
        if (empty($oportunidade = Oportunidade::find($request->input('id_oportunidade')))) {

            return response()->json([
                'msg' => 'Não foi possível cancelar o serviço.',
                'success' => false
            ], 422, [], JSON_PRETTY_PRINT);

        }

        $tomador   = $oportunidade->usuario;
        $prestador = $oportunidade->prestador;

        Mail::send('emails.tomadores.servico-cancelado', [
            'oportunidade' => $oportunidade->toArray(),
            'tomador' => $tomador->toArray(),
            'title' => 'Confirmamos a sua solicitação de cancelamento!'
        ], function($message) use ($tomador) {
            $message->to($tomador->email);
            $message->subject('Serviço Cancelado');
        });

        if ($prestador) {
            Mail::send('emails.prestadores.servico-cancelado', [
                'oportunidade' => $oportunidade->toArray(),
                'prestador' => $prestador->toArray(),
                'title' => 'Ops! Infelizmente este serviço foi cancelado pelo cliente!'
            ], function($message) use ($prestador) {
                $message->to($prestador->email);
                $message->subject('Serviço Cancelado');
            });
        }

        $oportunidade->status = Oportunidade::STATUS_CANCELADO;
        $oportunidade->save();

        return response()->json([
            'msg' => "Serviço cancelado com sucesso!",
            'success' => true,
        ], 200, [], JSON_PRETTY_PRINT);
    }

    public function avaliarServico(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        $oportunidade = Oportunidade::find($request->input('id_oportunidade'));

        if ($oportunidade) {
            $avaliacao = new AvaliacaoServico();
            $avaliacao->id_oportunidade = $oportunidade->id;
            $avaliacao->destinatario = AvaliacaoServico::DESTINATARIO_PRESTADOR;
            $avaliacao->nota = $request->input('nota');
            $avaliacao->comentarios = $request->input('comentarios');

            if ($avaliacao->save()) {
                $prestador = User::find($oportunidade->prestador->id);

                if ($prestador) {
                    $prestador->media_avaliacoes = $prestador->mediaAvaliacoes();
                    $prestador->save();
                }

                $response = [
                    'msg' => 'Serviço avaliado com sucesso! O prestador será notificado!',
                    'success' => true,
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function avaliarTomador(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        $oportunidade = Oportunidade::find($request->input('id_oportunidade'));

        if ($oportunidade) {
            $avaliacao = new AvaliacaoServico();
            $avaliacao->id_oportunidade = $oportunidade->id;
            $avaliacao->destinatario = AvaliacaoServico::DESTINATARIO_TOMADOR;
            $avaliacao->nota = $request->input('nota');
            $avaliacao->comentarios = $request->input('comentarios');

            if ($avaliacao->save()) {
                $tomador = User::find($oportunidade->usuario->id);

                if ($tomador) {
                    $tomador->media_avaliacoes_tomador = $tomador->mediaAvaliacoesTomador();
                    $tomador->save();
                }

                $response = [
                    'msg' => 'Cliente avaliado com sucesso! O usuário será notificado!',
                    'success' => true,
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function getComentariosAvaliacao(Request $request)
    {
        $response = [
            'msg' => 'Erro',
            'success' => false,
        ];

        if ($request->isMethod('post')) {
            $oportunidade = Oportunidade::find($request->input('id'));

            if (!is_null($oportunidade)) {
                $comentario = $oportunidade->avaliacao->comentarios;

                $response = [
                    'avatar' => $oportunidade->usuario->getAvatar(),
                    'comentario' => $comentario,
                    'success' => true
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function calculaValor(Request $request)
    {
        $response = [
            'success' => false,
        ];

        $adicionais = $request->input('adicionais');

        if (! empty($adicionais) && ! empty($request['id_base'])) {
            $base = ServicoBase::find($request['id_base']);

            Session::put('servico_base', $base->id);
            Session::put('servicos_adicionais', $adicionais);

            if ($base) {

                    $valorBase = $base->valor;
                    $valorTotal = $valorBase;

                    foreach ($adicionais as $index => $obj) {

                        if ($obj['type'] == "radio") {
                            $adicional = AgravanteOpcoes::find($obj['id']);
                        } else {
                            $adicional = ServicoBaseAgravante::find($obj['id']);
                        }

                        if ($adicional) {
                            
                            if ($obj['type'] == "radio" && $obj['value'] != 1) {
                                continue;
                            }

                            //Caso esteja incluso no serviço base deve-se descontar uma unidade
                            $value = ($adicional->incluso_servico_base) ? ($obj['value'] - 1) : $obj['value'];

                            //Se for checkbox calcular apenas se estiver habilitado
                            if ($value > 0) {

                                if ($obj['type'] == "radio") {

                                    if ($adicional->agravante->regra == ServicoBaseAgravante::VALOR_SIMPLES) {
                                        $valorTotal += ($value) * ($valorBase * ($adicional->valor / 100));
                                    } else {
                                        $valorTotal += ($value) * ($valorTotal * ($adicional->valor/100));
                                    }

                                } else {

                                    if ($adicional->regra == ServicoBaseAgravante::VALOR_SIMPLES) {
                                        $valorTotal += ($value) * ($valorBase * ($adicional->valor / 100));
                                    } else {
                                        $valorTotal += ($value) * ($valorTotal * ($adicional->valor/100));
                                    }

                                }
                            }
                    }
                }

                $response = [
                    'success' => true,
                    'valor' => round($valorTotal, 2)
                ];
            }
        }

        return response()->json($response, 200, [], JSON_PRETTY_PRINT);
    }

    public function pagamentoFalha(Request $request)
    {
        if (empty($oportunidade = Oportunidade::find($request['external_reference']))) {
            abort(404);
        }

        $oportunidade->status_pagamento_mp = MercadoPago::STATUS_FALHA;
        $oportunidade->id_pagamento_mp = $request['collection_id'];
        $oportunidade->id_pedido_mp = $request['preference_id'];
        $oportunidade->status = Oportunidade::STATUS_PAGAMENTO_COM_FALHA;
        $oportunidade->save();

        return view('oportunidades/pagamento-falha', ['oportunidade' => $oportunidade]);
    }

    public function pagamentoSucesso(Request $request)
    {
        if (empty($oportunidade = Oportunidade::find($request['external_reference']))) {
            abort(404);
        }

        $oportunidade->status_pagamento_mp = MercadoPago::STATUS_SUCESSO;
        $oportunidade->id_pagamento_mp = $request['collection_id'];
        $oportunidade->id_pedido_mp = $request['preference_id'];
        $oportunidade->status = Oportunidade::STATUS_ENCONTRANDO_PROFISSIONAL;
        $oportunidade->save();

        return view('oportunidades/pagamento-sucesso', ['oportunidade' => $oportunidade]);
    }

    public function pagamentoPendente(Request $request)
    {
        if (empty($oportunidade = Oportunidade::find($request['external_reference']))) {
            abort(404);
        }

        $oportunidade->status_pagamento_mp = MercadoPago::STATUS_PENDENTE;
        $oportunidade->id_pagamento_mp = $request['collection_id'];
        $oportunidade->id_pedido_mp = $request['preference_id'];
        $oportunidade->status = Oportunidade::STATUS_AGUARDANDO_PAGAMENTO;
        $oportunidade->save();

        return view('oportunidades/pagamento-pendente', ['oportunidade' => $oportunidade]);
    }

    public function ultimaSolicitacao($id)
    {
        if (empty($oportunidade = Oportunidade::find($id))) {
            return response()->json('Serviço não encontrado', 404, [], JSON_PRETTY_PRINT);
        }

        return response()->json($oportunidade->montaObjeto(), 200, [], JSON_PRETTY_PRINT);
    }

    public function motivosRejeicao(Request $request)
    {
        if (empty($oportunidade = Oportunidade::find($request->input('id_oportunidade')))) {
            return response()->json('Serviço não encontrado', 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($prestador = User::find($request->input('id_prestador')))) {
            return response()->json('Prestador não encontrado', 404, [], JSON_PRETTY_PRINT);
        }

        if (empty($oferta = OportunidadeTemCandidato::find($oportunidade->id, $prestador->id))) {
            return response()->json('Oferta não encontrada para este candidato', 404, [], JSON_PRETTY_PRINT);
        }

        $motivo = ($oferta->rejeicao) ? $oferta->rejeicao->descricao : 'Motivo não informado';

        return response()->json(['motivo' => $motivo], 200, [], JSON_PRETTY_PRINT);
    }

    public function paymentNotification()
    {
        $id     = isset($_GET["id"]) ? $_GET["id"] : null;
        $topic  = isset($_GET["topic"]) ? $_GET["topic"] : null;

        Log::useDailyFiles(storage_path().'/logs/mp/payment/' . date('Y_m_d_H_i_s') . '_' . $id . '_payment.log');
        Log::info('[INÍCIO] - Requisição recebida às: -' . date('d/m/Y H:i:s'));

        $responseStatus = [
            'code' => '404',
            'msg' => 'Serviço não encontrado. SEM ID'
        ];

        if (!isset($id) || !ctype_digit($id) || !isset($topic)) {

            Mail::send('emails.pagamento.erro-pagamento', [
                'status' => $responseStatus['code'],
                'id'     => $id,
                'title'  => 'Atualização no Pagamento (MercadoPago)'
            ], function($message) {
                $message->to('leandro@sou4w.com.br');
                $message->cc('lorenzo.kniss@gmail.com');
                $message->subject('Atualização no Pagamento (Mercado Pago)');
            });

            Log::emergency("[ERRO] - O pagamento não possui um ID especificado.");

            return response()->json($responseStatus, 200, [], JSON_PRETTY_PRINT);
        }

        try {

            Log::info("[BUSCANDO] - Buscando o serviço na base de dados");
            Log::info("[BUSCANDO ID] Encontrado :" . $_GET["id"]);
            Log::info("[BUSCANDO TOPIC] Encontrado :" . $_GET["topic"]);


            switch ($topic) {
                case "merchant_order":
                    $result = (new MercadoPago())->getMerchantOrder($id);
                    $data   = $result['response'];
                    break;
                case "payment":
                    $result = (new MercadoPago())->getPaymentInfo($id);
                    $data   = $result['response']['collection'];
                    break;
            }

            if ($result["status"] == 200) {

                echo "ahaha";
                print_r($result);
                die();


                //$oportunidade = Oportunidade::find($data["external_reference"]);

            } else {
                $responseStatus['code'] = $result['status'];

                Mail::send('emails.pagamento.erro-pagamento', [
                    'status' => $responseStatus['code'],
                    'id'     => $id,
                    'title' => 'Atualização no Pagamento (MercadoPago)'
                ], function($message) {
                    $message->to('leandro@sou4w.com.br');
                    $message->cc('lorenzo.kniss@gmail.com');
                    $message->subject('Atualização no Pagamento (Mercado Pago)');
                });

                Log::emergency("[ERRO] - Houve um erro desconhecido cujo código é: " . $responseStatus['code']);
                Log::emergency("[ERRO] - Retorno " . $result);
            }

        } catch (\Exception $e) {
            $responseStatus['code'] = '500';
            $responseStatus['msg'] = 'Erro ao atualizar o servico!';

            Mail::send('emails.pagamento.erro-pagamento', [
                'status' => $responseStatus['code'],
                'id'     => $id,
                'title'  => 'Atualização no Pagamento (MercadoPago)'
            ], function($message) {
                $message->to('leandro@sou4w.com.br');
                $message->cc('lorenzo.kniss@gmail.com');
                $message->subject('Atualização no Pagamento (Mercado Pago)');
            });

            Log::emergency("[ERRO] - Houve um erro interno: \n" . $e->getMessage());

            return response()->json($responseStatus, 200, [], JSON_PRETTY_PRINT);
        }

        Log::info("[FIM] - O processamento foi finalizado às: " . date('Y-m-d H:i:s'));

        return response()->json($responseStatus, 200, [], JSON_PRETTY_PRINT);
    }
}
