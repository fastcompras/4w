<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashController extends Controller
{
    public function buscaNotificacoes()
    {
        $notificacoes = Auth::user()->buscaNotificacoesDash();

        return response()->json($notificacoes, 200, [], JSON_PRETTY_PRINT);
    }
}
