<?php

namespace App\Services;

use Illuminate\Http\Request;
use \App\Interfaces\MakesInternalRequests;
use Illuminate\Foundation\Application;
use \App\Exceptions\FailedInternalRequestException;

/**
 * Internal request service
 */
class InternalRequest implements MakesInternalRequests
{
    /**
     * The app instance
     *
     * @var $app
     */
    protected $app;

    /**
     * Constructor
     *
     * @param Application $app        The app instance.
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Make an internal request
     *
     * @param string $action  The HTTP verb to use.
     * @param string $url     The resource to look up.
     * @param array  $data    The request body.
     *
     * @return \Illuminate\Http\Response
     */
    public function request($action, $url, $data = [], $headers = [])
    {
        // Create request
        $request = Request::create($url, $action, $data, [], [], $headers);

        // Get response
        $response = $this->app->handle($request);

        // Dispatch the request
        return $response;
    }
}