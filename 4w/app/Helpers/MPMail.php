<?php

namespace App\Helpers;

use Mail;

class MPMail
{

    public static function merchantOrderFail($deal)
    {
        $data = [
            'oportunidade' => $deal->toArray(),
            'status'       => 422,
            'title'        => 'Erro ao atualizar o pedido'
        ];

        Mail::send('emails.mp.pedidos.erro-atualizar', $data, function($message) use ($deal) {
            //$message->cc('lorenzo.kniss@gmail.com');
            $message->subject('Erro ao atualizar o pedido #' . $deal->id);
        });
    }

    public static function merchantPaymentFail($deal)
    {
        $data = [
            'oportunidade' => $deal->toArray(),
            'status'       => 422,
            'title'        => 'Erro ao atualizar o pagamento'
        ];

        Mail::send('emails.mp.pagamento.erro-atualizar', $data, function($message) use ($deal) {
            //$message->cc('lorenzo.kniss@gmail.com');
            $message->subject('Erro ao atualizar o pagamento #' . $deal->id);
        });
    }

    public static function merchantOrderSuccess($deal)
    {
        $data = [
            'title'        => 'Pedido Atualizado',
            'oportunidade' => $deal->toArray(),
        ];

        Mail::send('emails.mp.pedidos.atualizado', $data, function($message) use ($deal) {
            //$message->cc('lorenzo.kniss@gmail.com');
            $message->subject('Pedido atualizado #' . $deal->id);
        });
    }

    public static function merchantPaymentSuccess($deal)
    {
        $data = [
            'title'        => 'Pagamento Atualizado',
            'oportunidade' => $deal->toArray(),
        ];

        Mail::send('emails.mp.pagamento.atualizado', $data, function($message) use ($deal) {
            //$message->cc('lorenzo.kniss@gmail.com');
            $message->subject('Pagamento atualizado #' . $deal->id);
        });
    }
}