<?php

namespace App\Helpers;

use MP;

class MercadoPago
{
    const STATUS_PENDENTE       = "pending";
    const STATUS_SUCESSO        = "approved";
    const STATUS_FALHA          = "rejected";
    const STATUS_ESTORNADO      = "refunded";
    const STATUS_CANCELADO      = "cancelled";
    const STATUS_EM_MEDIACAO    = "in_mediation";
    const STATUS_ESTORNO_CARTAO = "charged_back";

    const TIPO_BOLETO = 0;
    const TIPO_CARTAO = 1;

    private $url_success;
    private $url_failure;
    private $url_pending;

    function __construct()
    {
        $this->url_success = config('app.url'). "/mp/success";
        $this->url_failure = config('app.url'). "/mp/failure";
        $this->url_pending = config('app.url'). "/mp/pending";
    }

    public function sendPayment($item, $client)
    {
        $preference_data = [
            "items" => [
                $item
            ],
            "payer" => [
                "name"         => $client->nome,
                "surname"      => "",
                "email"        => $client->email,
                "date_created" => date("Y-m-d H:i:s"),
                "phone" => [
                    "area_code" => $client->getTelefone()->getDDD(),
                    "number"    => $client->getTelefone()->getNumero()
                ],
                "identification" => [
                    "type"   => "CLIENTE",
                    "number" => $client->id
                ],
                "address" => [
                    "street_name"   => $client->getEndereco()->logradouro,
                    "street_number" => $client->getEndereco()->numero,
                    "zip_code"      => $client->getEndereco()->cep
                ]
            ],
            "back_urls" => [
                "success" => $this->url_success,
                "failure" => $this->url_failure,
                "pending" => $this->url_pending
            ],
            "auto_return"          => "all",
            "notification_url"     => config('app.url') . "/api/mp/notification",
            "external_reference"   => $item['id'],
            "expires"              => false,
            "expiration_date_from" => null,
            "expiration_date_to"   => null
        ];

        $preference = MP::create_preference($preference_data);

        return $preference;
    }

    public function updateOrder($id, $item, $client)
    {
        $preference_data = [
            "items" => [
                $item
            ],
            "payer" => [
                "name"         => $client->nome,
                "surname"      => "",
                "email"        => $client->email,
                "date_created" => date("Y-m-d H:i:s"),
                "phone" => [
                    "area_code" => $client->getTelefone()->getDDD(),
                    "number"    => $client->getTelefone()->getNumero()
                ],
                "identification" => [
                    "type" => "CLIENTE",
                    "number" => $client->id
                ],
                "address" => [
                    "street_name"   => $client->getEndereco()->logradouro,
                    "street_number" => $client->getEndereco()->numero,
                    "zip_code"      => $client->getEndereco()->cep
                ]
            ],
            "back_urls" => [
                "success" => $this->url_success,
                "failure" => $this->url_failure,
                "pending" => $this->url_pending
            ],
            "auto_return"          => "all",
            "notification_url"     => config('app.url') . "/api/mp/notification",
            "external_reference"   => $item['id'],
            "expires"              => false,
            "expiration_date_from" => null,
            "expiration_date_to"   => null
        ];

        $preference = MP::update_preference($id, $preference_data);

        return $preference;
    }

    public function getPayment($collectionId)
    {
        return MP::get_payment($collectionId);
    }

    public function getMerchantOrder($merchantOrder)
    {
        return MP::get_merchant_info($merchantOrder);
    }

    public function getPaymentInfo($collectionId)
    {
        return MP::get_payment_info($collectionId);
    }

    public function cancel_payment($collectionId)
    {
        return MP::cancel_payment($collectionId);
    }

    public function refund_payment($collectionId)
    {
        return MP::refund_payment($collectionId);
    }

    public function getAcessToken()
    {
        return MP::get_access_token();
    }
}
