<?php

namespace App\Helpers;

class FormHelper
{
    public static function createHrLine()
    {
        $input = "<div class=\"hr-line-dashed\"></div>";

        return $input;
    }

    public static function createInputText($id, $obj, $oldValue = 0)
    {
        if ($oldValue) {
            $value = $oldValue;
            $min = 1;
        } else {
            $value = $obj->incluso_servico_base ? 1 : "0";
            $min = $value;
        }

        $idForm = "adicional-".$id;

        $input = "<div class='form-group'>";
        $input .= "<label class=\"col-sm-3 control-label\">" . $obj->rotulo_formulario . "</label>";
        $input .= "<div class=\"col-sm-6\">";
        $input .= "<input type=\"text\" id=\"$idForm\" data-id=\"$id\" min=\"$min\" value=\"$value\" readonly class=\"spinner adicional form-control\" required=\"true\">";
        $input .= "</div>";
        $input .= "</div>";

        return $input;
    }

    public static function createCheckbox($id, $obj, $selected = false)
    {
        $idForm = "adicional-".$id;
        $checked = ($selected) ? "checked" : "";

        $input = "<div class='form-group'>";
        $input .= "<label class=\"col-sm-3 control-label\">".$obj->rotulo_formulario . "</label>";
        $input .= "<div class=\"col-sm-6\" style='font-size:14px; font-weight: bold'>";
        $input .= "<input type=\"checkbox\" {$checked} class=\"adicional\" data-id=\"$id\" style=\"margin-top: 10px;\" id=\"$idForm\">" . " " . " Sim";
        $input .= "</div>";
        $input .= "</div>";

        return $input;
    }

    public static function createRadioBox($id, $obj)
    {
        $input = "<div class='form-group'>";
        $input .= "<label class=\"col-sm-3 control-label\">".$obj->rotulo_formulario . "</label>";
        $input .= "<div class=\"col-sm-6\" style='font-size:14px; font-weight: bold'>";

        $i = 0;
        foreach ($obj->opcoes as $opcao) {
            $checked = ($i == 0) ? "checked" : "";
            $idForm = "adicional-".$obj->id;
            $input .= "<label class=\"radio-inline\"><input type=\"radio\" {$checked} class=\"adicional\" id=\"$idForm\" data-id=\"$opcao->id\" name=\"$idForm\">$opcao->descricao</label>";
            $i++;
        }

        $input .= "</div>";
        $input .= "</div>";

        return $input;
    }

    public static function createRadioBoxSelected($id, $obj, $idSelected)
    {
        $input = "<div class='form-group'>";
        $input .= "<label class=\"col-sm-3 control-label\">".$obj->rotulo_formulario . "</label>";
        $input .= "<div class=\"col-sm-6\" style='font-size:14px; font-weight: bold'>";

        foreach ($obj->opcoes as $opcao) {
            $checked = ($opcao->id == $idSelected) ? "checked" : "";
            $idForm = "adicional-".$obj->id;
            $input .= "<label class=\"radio-inline\"><input type=\"radio\" {$checked} class=\"adicional\" id=\"$idForm\" data-id=\"$opcao->id\" name=\"$idForm\">$opcao->descricao</label>";
        }

        $input .= "</div>";
        $input .= "</div>";

        return $input;
    }
}