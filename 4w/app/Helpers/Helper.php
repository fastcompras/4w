<?php

namespace App\Helpers;

class Helper
{
    public static function moneyToMysql($value)
    {
        $newValue = str_replace(',', '.', str_replace('.', '', $value));
        return $newValue;
    }

    public static function getWordpressUploadUrl() {
        return 'https://www.sou4w.com.br/wp-admin/upload.php';
    }

    public static function getWordpressUploadedImages() {
        $wordpressUploadDir = __DIR__.'/../../../wp-content/uploads';
        if (!file_exists($wordpressUploadDir)) {
            return [];
        }

        $result = [];
        foreach (scandir($wordpressUploadDir) as $year) {
            if ($year[0] != '2' || strlen($year) !== 4 || !ctype_digit($year)) {
                continue;
            }
            $yearDir = $wordpressUploadDir."/".$year;
            foreach (scandir($yearDir) as $month) {
                if (strlen($month) !== 2 || !ctype_digit($month)) {
                    continue;
                }
                $monthDir = $yearDir."/".$month;
                foreach (scandir($monthDir) as $file) {
                    if ($file === "." || $file === ".." || strlen($file) <= 5) {
                        continue;
                    }
                    if (!in_array(substr($file, -4), [".png", ".jpg", "jpeg", ".gif", ".bmp"])) {
                        continue;
                    }
                    array_push($result, [
                        "period" => intval($year.$month),
                        "url" => "https://www.sou4w.com.br/wp-content/uploads/".$year."/".$month."/".$file
                    ]);
                }
            }
        }

        usort($result, function($a, $b) {
            if ($a["period"] == $b["period"]) {
                return 0;
            }
            return ($a["period"] > $b["period"]) ? -1 : 1;
        });

        return $result;
    }
}
