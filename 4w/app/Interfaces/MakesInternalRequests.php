<?php

namespace App\Interfaces;

interface MakesInternalRequests
{
    /**
     * Make an internal request
     *
     * @param string $action   The HTTP verb to use.
     * @param string $url      The resource to look up.
     * @param array  $data     The request body.
     *
     * @return \Illuminate\Http\Response
     */
    public function request($action, $url, $data = [], $headers = []);
}
