<?php


$factory->define(App\Models\Cupom::class, function (Faker\Generator $faker) {
	return [
		'codigo' => $faker->swiftBicNumber,
		'qtd_disponivel' => $faker->randomDigitNotNull,
		'desconto' => $faker->randomFloat(2, 0, 1000),
		'tipo_desconto' => $faker->numberBetween(1, 2),
		'tipo_regra' => $faker->numberBetween(1, 3),
		'data_expiracao' => $faker->date('Y-m-d H:i:s'),
	];
});

$factory->state(App\Models\Cupom::class, 'validate_by_available_quantity', function (Faker\Generator $faker) {
	return [
		'tipo_regra' => \App\Models\Enums\TipoRegraCupom::VALIDATE_BY_AVAILABLE_QUANTITY,
	];
});

$factory->state(App\Models\Cupom::class, 'validate_by_expiration_date', function (Faker\Generator $faker) {
	return [
		'tipo_regra' => \App\Models\Enums\TipoRegraCupom::VALIDATE_BY_EXPIRATION_DATE,
	];
});

$factory->state(App\Models\Cupom::class, 'absolute_discount', function (Faker\Generator $faker) {
	return [
		'tipo_desconto' => \App\Models\Enums\TipoDescontoCupom::ABSOLUTE,
	];
});

$factory->state(App\Models\Cupom::class, 'percentage_discount', function (Faker\Generator $faker) {
	return [
		'tipo_desconto' => \App\Models\Enums\TipoDescontoCupom::PERCENTAGE,
	];
});