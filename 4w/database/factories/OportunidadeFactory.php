<?php


$factory->define(App\Models\Oportunidade::class, function (Faker\Generator $faker) {
	return [
		'id_usuario_solicitante' => $faker->numberBetween(1, 999),
		'id_prestador_selecionado' => $faker->numberBetween(1, 999),
		'id_servico' => $faker->numberBetween(1, 999),
		'id_servico_base' => $faker->numberBetween(1, 999),
		'id_cidade' => $faker->numberBetween(1, 999),
		'numero' => $faker->buildingNumber,
		'logradouro' => $faker->streetAddress,
		'complemento' => $faker->secondaryAddress,
		'cep' => $faker->postcode,
		'bairro' => 'Centro',
		'data_execucao' => $faker->date('Y-m-d H:i:s'),
		'status' => \App\Models\Oportunidade::STATUS_AGENDADO,
		'valor_total' => $faker->randomFloat(2, 0, 10000),
		'valor_4w' => $faker->randomFloat(2, 0, 100)
	];
});