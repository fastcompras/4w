<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFkMotivoRejeicao extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('oportunidade_tem_candidato', function($table) {
            $table->index('id_motivo_rejeicao');
            $table->foreign('id_motivo_rejeicao')->references('id')->on('motivo_rejeicao')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('oportunidade_tem_candidato', function($table) {
            $table->dropForeign('oportunidade_tem_candidato_id_motivo_rejeicao_foreign');
            $table->dropIndex('oportunidade_tem_candidato_id_motivo_rejeicao_index');
        });
    }
}
