<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimulationOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opcoes_simulacao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('simulacao_id')->unsigned();
            $table->integer('agravante_id')->unsigned();
            $table->integer('agravante_value')->unsigned();

            $table->foreign('simulacao_id')
                ->references('id')->on('simulacoes')
                ->onDelete('cascade');

            $table->foreign('agravante_id')
                ->references('id')->on('servico_base_agravante');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('opcoes_simulacao');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
