<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCupomHistoricoTable extends Migration
{
    public function up()
    {
	    Schema::create('cupom_historico', function (Blueprint $table) {
		    $table->increments('id');
		    $table->integer('id_oportunidade')->unsigned();
		    $table->integer('id_cupom')->unsigned();
		    $table->dateTime('data_cadastro');
	    });

	    Schema::table('cupom_historico', function (Blueprint $table) {
		    $table->index('id_oportunidade');
		    $table->foreign('id_oportunidade')->references('id')->on('oportunidade')->onDelete('cascade');
		    $table->index('id_cupom');
		    $table->foreign('id_cupom')->references('id')->on('cupom')->onDelete('cascade');
	    });
    }

    public function down()
    {

	    Schema::drop('cupom_historico');
    }
}
