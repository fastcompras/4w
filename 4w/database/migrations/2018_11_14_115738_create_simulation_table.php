<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimulationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulacoes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('servico_id');
            $table->integer('servico_base_id');
            $table->string('nome');
            $table->string('email');
            $table->string('cep');
            $table->string('telefone');
            $table->string('empresa')->nullable();
            $table->integer('usuario_id')->nullable();
            $table->timestamps();
        });

        Schema::table('simulacoes', function($table) {
            $table->foreign('servico_id')
                ->references('id')->on('servico');
        
            $table->foreign('servico_base_id')
                ->references('id')->on('servico_base');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulacoes');
    }
}
