<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCupomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('cupom', function (Blueprint $table) {
		    $table->increments('id');
		    $table->string('codigo')->unique();
		    $table->integer('qtd_disponivel')->nullable();
		    $table->decimal('desconto', 10, 2);
		    $table->tinyInteger('tipo_desconto');
		    $table->dateTime('data_expiracao')->nullable();
		    $table->dateTime('data_cadastro');
		    $table->tinyInteger('tipo_regra')->default(1);
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::drop('cupom');
    }
}
