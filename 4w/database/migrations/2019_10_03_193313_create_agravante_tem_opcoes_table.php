<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgravanteTemOpcoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agravante_tem_opcoes', function (Blueprint $table) {
            $table->increments('id');
		    $table->integer('id_servico_base_agravante')->unsigned();
		    $table->string('descricao');
            $table->decimal('valor', 10, 2);
        });

	    Schema::table('agravante_tem_opcoes', function (Blueprint $table) {
		    $table->index('id_servico_base_agravante');
		    $table->foreign('id_servico_base_agravante')->references('id')->on('servico_base_agravante')->onDelete('cascade');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agravante_tem_opcoes');
    }
}
