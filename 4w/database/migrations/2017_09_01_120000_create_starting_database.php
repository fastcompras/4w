<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartingDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTables();
        $this->createForeignKeys();
    }

    private function createForeignKeys() {

        Schema::table('servico_base', function (Blueprint $table) {
            $table->foreign('id_servico')->references('id')->on('servico');
        });

        Schema::table('cidade', function (Blueprint $table) {
            $table->foreign('id_estado')->references('id')->on('estado');
        });

        Schema::table('log_acesso_autorizado', function (Blueprint $table) {
            $table->foreign('id_usuario')->references('id')->on('usuario');
        });

        Schema::table('notificacao', function (Blueprint $table) {
            $table->foreign('id_usuario')->references('id')->on('usuario');
        });

        Schema::table('oportunidade', function (Blueprint $table) {
            $table->foreign('id_prestador_selecionado')->references('id')->on('usuario');
            $table->foreign('id_servico')->references('id')->on('servico');
            $table->foreign('id_usuario_solicitante')->references('id')->on('usuario');
        });

        Schema::table('oportunidade_tem_candidato', function (Blueprint $table) {
            $table->foreign('id_oportunidade')->references('id')->on('oportunidade');
            $table->foreign('id_usuario')->references('id')->on('usuario');
        });
    }

    private function createTables() {

        Schema::create('servico', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent();
            $table->string('descricao');
            $table->string('top_image_url')->nullable();
            $table->string('url')->nullable();
        });

        Schema::create('servico_base', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent();
            $table->decimal('valor', 10, 2);
            $table->integer('id_servico')->unsigned();
            $table->longText('descricao');
            $table->string('image_url')->nullable();
            $table->string('nome');
            $table->string('url')->nullable();
        });

        Schema::create('servico_base_agravante', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('valor', 10, 2)->default(0);
            $table->integer('id_base_servico')->unsigned();
            $table->string('rotulo_formulario');
            $table->timestamp('data_cadastro')->default(null);
            $table->tinyInteger('incluso_servico_base')->default(0)->comment('0 => Não\n1 => Sim');
            $table->tinyInteger('ordem_formulario')->default(10)->comment('Ordem em que aparecerão os campos na tela');
            $table->tinyInteger('regra')->default(null)->comment('1 => Simples\n2 => Acumulado');
            $table->tinyInteger('tipo_formulario')->default(1)->comment('1 => Texto\n2 => Checkbox');
        });

        Schema::create('cidade', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_estado')->unsigned();
            $table->string('nome');
        });

        Schema::create('estado', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('uf');
        });

        Schema::create('documento', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent();
            $table->integer('tipo');
            $table->string('url');
        });

        Schema::create('endereco', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent();
            $table->integer('associado_id')->unsigned()->nullable();
            $table->integer('numero');
            $table->string('bairro');
            $table->string('cep');
            $table->string('complemento');
            $table->string('logradouro');
            $table->tinyInteger('id_associado')->unsigned()->nullable();
        });

        Schema::create('log_acesso_autorizado', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data');
            $table->dateTime('data_logout')->nullable();
            $table->integer('id_usuario')->unsigned();
            $table->string('ip')->nullable();
            $table->string('navegador')->nullable();
        });

        Schema::create('log_acesso_negado', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data');
            $table->string('ip')->nullable();
            $table->string('navegador')->nullable();
        });

        Schema::create('notificacao', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data');
            $table->integer('id_usuario')->unsigned();
            $table->string('ip');
            $table->string('navegador');
        });

        Schema::create('oportunidade', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent()->nullable();
            $table->dateTime('data_execucao')->nullable();
            $table->integer('id_prestador_selecionado')->unsigned()->nullable();
            $table->integer('id_servico')->unsigned()->nullable();
            $table->integer('id_usuario_solicitante')->unsigned();
            $table->integer('numero')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cep')->nullable();
            $table->string('complemento')->nullable();
            $table->string('escopo');
            $table->string('ip');
            $table->string('link_mercado_pago')->nullable();
            $table->string('logradouro')->nullable();
            $table->string('navegador');
            $table->string('sugestao_servico')->nullable();
        });

        Schema::create('oportunidade_tem_candidato', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent()->nullable();
            $table->integer('id_oportunidade')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->tinyInteger('status')->default(0)->comment("Candidatos a prestação de determinado serviço\n\n0 => Convidado\n1 => Aceitou\n2 => Recusou");
        });

        Schema::create('pagamento', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent();
            $table->dateTime('data_pagamento');
            $table->dateTime('data_vencimento');
            $table->decimal('valor', 19, 2);
            $table->decimal('valor_pago', 19, 2);
            $table->integer('id_plano_comercial')->unsigned();
            $table->tinyInteger('status_pagamento')->default(0)->comment("0 = em aberto\n1 = pago\n");
        });

        Schema::create('perfil_conta', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent()->nullable();
            $table->string('descricao');
        });

        Schema::create('plano_comercial', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent()->nullable();
            $table->decimal('valor', 10, 2);
            $table->decimal('valor_promocional', 10, 2);
            $table->string('descricao');
            $table->string('nome');
            $table->tinyInteger('tipo_faturamento')->default('0')->comment("0 = mensal\n1 = trimestral\n2 = semestral\n3 = anual\n");
        });

        Schema::create('recuperar_senha', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data')->nullable();
            $table->integer('id_usuario')->unsigned();
            $table->string('hash');
            $table->tinyInteger('status');
        });

        Schema::create('regiao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cidade')->unsigned();
            $table->string('nome')->nullable();
        });

        Schema::create('sugestao_servico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_usuario')->unsigned();
            $table->string('aprovado')->default(0);
            $table->string('data_cadastro')->useCurrent()->nullable();
            $table->string('descricao');
        });

        Schema::create('telefone', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero')->nullable();
        });

        Schema::create('usuario', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cadastro')->useCurrent()->nullable();
            $table->dateTime('ultimo_login')->nullable();
            $table->integer('id_cidade')->unsigned()->nullable();
            $table->integer('id_perfil_conta')->unsigned()->nullable();
            $table->string('email');
            $table->string('nome');
            $table->string('password')->nullable();
            $table->string('remember_token')->nullable();
            $table->string('avatar');
            $table->tinyInteger('aprovado')->default(0);
            $table->tinyInteger('ativo')->default(0)->comment("0 = Aguardando aprovação \n1 = Aprovado\n");
        });

        Schema::create('usuario_cancelou_servico', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('data_cancelamento')->useCurrent()->nullable();
            $table->integer('id_oportunidade')->unsigned();
            $table->integer('id_usuario')->unsigned();
            $table->string('motivos');
        });

        Schema::create('usuario_presta_servico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_servico')->unsigned();
            $table->integer('id_usuario')->unsigned();
        });

        Schema::create('usuario_tem_documento', function (Blueprint $table) {
            $table->integer('id_documento')->unsigned();
            $table->integer('id_usuario')->unsigned();
        });

        Schema::create('usuario_tem_endereco', function (Blueprint $table) {
            $table->integer('id_endereco')->unsigned();
            $table->integer('id_usuario')->unsigned();
        });

        Schema::create('usuario_tem_telefone', function (Blueprint $table) {
            $table->integer('id_telefone')->unsigned();
            $table->integer('id_usuario')->unsigned();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('servico');
        Schema::dropIfExists('servico_base');
        Schema::dropIfExists('servico_base_agravante');
        Schema::dropIfExists('cidade');
        Schema::dropIfExists('estado');
        Schema::dropIfExists('documento');
        Schema::dropIfExists('endereco');
        Schema::dropIfExists('log_acesso_autorizado');
        Schema::dropIfExists('log_acesso_negado');
        Schema::dropIfExists('notificacao');
        Schema::dropIfExists('oportunidade');
        Schema::dropIfExists('oportunidade_tem_candidato');
        Schema::dropIfExists('pagamento');
        Schema::dropIfExists('perfil_conta');
        Schema::dropIfExists('plano_comercial');
        Schema::dropIfExists('recuperar_senha');
        Schema::dropIfExists('regiao');
        Schema::dropIfExists('sugestao_servico');
        Schema::dropIfExists('telefone');
        Schema::dropIfExists('usuario');
        Schema::dropIfExists('usuario_cancelou_servico');
        Schema::dropIfExists('usuario_presta_servico');
        Schema::dropIfExists('usuario_tem_documento');
        Schema::dropIfExists('usuario_tem_endereco');
        Schema::dropIfExists('usuario_tem_telefone');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
