<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateOportunidadesAddColumnCupomId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::table('oportunidade', function($table) {
		    $table->integer('id_cupom')->unsigned()->nullable();
		    $table->foreign('id_cupom')->references('id')->on('cupom');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('oportunidade', function($table) {
		    $table->dropForeign('oportunidade_id_cupom_foreign');
		    $table->dropColumn('id_cupom');
	    });
    }
}
