<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsuarioTemDocumentacaoCascade extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('usuario_tem_documento', function($table) {
            $table->index('id_documento');
            $table->foreign('id_documento')->references('id')->on('documento')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('usuario_tem_documento', function($table) {
            $table->dropForeign('usuario_tem_documento_id_documento_foreign');
            $table->dropIndex('usuario_tem_documento_id_documento_index');
        });
    }
}
