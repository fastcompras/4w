<?php

use Illuminate\Database\Seeder;

class ServicoBaseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servico_base')->insert([
            [
                "id" => "2",
                "id_servico" => "1",
                "nome" => "Limpeza Geral",
                "descricao" => "Serviço de limpeza e higienização em geral. O valor deste serviço inclui somente mão de obra, sendo responsabilidade do contratante a aquisição e disponibilização dos materiais e utensílios a serem aplicados no serviço.",
                "valor" => "119.00",
                "data_cadastro" => "2019-01-15 14:44:22",
                "url" => "limpeza-geral",
                "image_url" => "limpeza-geral.png"
            ], [
                "id" => "3",
                "id_servico" => "1",
                "nome" => "Limpeza Pesada",
                "descricao" => "Serviço de limpeza pesada, como retirada de mofo de paredes ou tetos, desencardido de pisos, mover moveis pesados, limpeza de rejunte, etc.\nO valor deste serviço inclui somente mão de obra.",
                "valor" => "155.00",
                "data_cadastro" => "2019-01-15 14:44:26",
                "url" => "limpeza-pesada",
                "image_url" => "limpeza-pesada.png"
            ], [
                "id" => "4",
                "id_servico" => "10",
                "nome" => "Cuidar de Crianças e Pré-adolescentes",
                "descricao" => "Cuidar e acompanhar crianças e pré-adolescentes, promovendo atividades lúdicas e de entretenimento. Administrar alimentação e remédios, quando for o caso.",
                "valor" => "26.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "cuidar-de-criancas-e-pre-adolescentes",
                "image_url" => ""
            ], [
                "id" => "5",
                "id_servico" => "18",
                "nome" => "Ajustes e reformas",
                "descricao" => "Realizar pequenos reparos ou ajustes em vestuários. O valor deste serviço inclui somente mão de obra, sendo responsabilidade do contratante a aquisição materiais como tecidos, ziperes, velcros, botões, etc. a serem aplicados no serviço.",
                "valor" => "5.20",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "ajustes-e-reformas",
                "image_url" => ""
            ], [
                "id" => "6",
                "id_servico" => "17",
                "nome" => "Instalações Elétricas",
                "descricao" => "Serviço de instalação em novos empreendimentos ou projetos. O valor deste serviço inclui somente mão de obra, sendo responsabilidade do contratante a aquisição e disponibilização dos materiais a serem aplicados no serviço.",
                "valor" => "645.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "instalacoes-eletricas",
                "image_url" => ""
            ], [
                "id" => "7",
                "id_servico" => "17",
                "nome" => "Manutenção elétrica",
                "descricao" => "Serviço de manutenção em instalações elétricas. O valor deste serviço inclui somente mão de obra, sendo responsabilidade do contratante a aquisição e disponibilização dos materiais a serem aplicados no serviço.",
                "valor" => "31.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "manutencao-eletrica",
                "image_url" => ""
            ], [
                "id" => "8",
                "id_servico" => "13",
                "nome" => "Aulas de Violão",
                "descricao" => "",
                "valor" => "58.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "aulas-de-violao",
                "image_url" => ""
            ], [
                "id" => "9",
                "id_servico" => "22",
                "nome" => "Atendimento as necessidades do paciente ",
                "descricao" => "Execução de procedimentos de assistência de enfermagem. O valor deste serviço inclui somente mão de obra, sendo responsabilidade do contratante a aquisição e disponibilização dos materiais e utensílios a serem aplicados.",
                "valor" => "10.00",
                "data_cadastro" => "2018-10-02 15:06:01",
                "url" => "atendimento-as-necessidades-do-paciente",
                "image_url" => ""
            ], [
                "id" => "10",
                "id_servico" => "22",
                "nome" => "Consultoria em Enfermagem",
                "descricao" => "Avaliação geral do paciente e do ambiente para apresentação de parecer técnico, orientação e treinamento para familiares ou cuidadores, visando a excelência no cuidado e pronto restabelecimento. Os profissionais que executam este serviços possuem formação superior e registro no Coren.",
                "valor" => "166.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "consultoria-em-enfermagem",
                "image_url" => ""
            ], [
                "id" => "11",
                "id_servico" => "9",
                "nome" => "Disciplinas do Ensino Fundamental",
                "descricao" => "Aulas de reforço para alunos do ensino fundamental. Descreva no campo informações adicionais os objetivos, colégio, livro em uso e demais informações uteis na preparação das aulas.",
                "valor" => "62.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "disciplinas-do-ensino-fundamental",
                "image_url" => ""
            ], [
                "id" => "12",
                "id_servico" => "9",
                "nome" => "Disciplina do Ensino Médio",
                "descricao" => "Aulas de reforço para alunos do ensino médio. Descreva no campo informações adicionais os objetivos, colégio, livro em uso e demais informações uteis na preparação das aulas.",
                "valor" => "74.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "disciplina-do-ensino-medio",
                "image_url" => ""
            ], [
                "id" => "13",
                "id_servico" => "15",
                "nome" => "Passeios diurnos",
                "descricao" => "Realizar passeios e brincadeiras com cães. O intervalo de horários de passeios para agendamento vão das 06:00 às 19:00 horas. Não inclui serviço de adestramento.",
                "valor" => "16.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "passeios-diurnos",
                "image_url" => ""
            ], [
                "id" => "14",
                "id_servico" => "18",
                "nome" => "Confecção sob medidas",
                "descricao" => "Realizar confecção sob medidas em vestuários. O valor deste serviço inclui somente mão de obra, sendo responsabilidade do contratante a aquisição materiais como tecidos, ziperes, velcros, botões, etc. a serem aplicados no serviço.",
                "valor" => "21.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "confeccao-sob-medidas",
                "image_url" => ""
            ], [
                "id" => "15",
                "id_servico" => "23",
                "nome" => "Capacitação no uso da Internet e Redes Sociais",
                "descricao" => "Habilitar idosos na utilização na navegação, comunicação e leitura. Este serviço inclui somente o valor das aulas, os equipamentos para aprendizado e treinamento devem ser disponibilizados pelo contratante do serviço.",
                "valor" => "42.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "capacitacao-no-uso-da-internet-e-redes-sociais",
                "image_url" => ""
            ], [
                "id" => "16",
                "id_servico" => "24",
                "nome" => "Instalação de Ar Split",
                "descricao" => "Este serviço refere-se a mão de obra de instalação de ar condicionado. Tubulações e canaletas somente serão fornecidas  quando selecionada a metragem para cada equipamento a ser instalado. Esta contratação não inclui os equipamentos de ar, sendo responsabilidade do contratante a aquisição e disponibilização do(s) equipamento(s).",
                "valor" => "52.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "instalacao-de-ar-split",
                "image_url" => ""
            ], [
                "id" => "17",
                "id_servico" => "24",
                "nome" => "Manutenção ou remoção de Ar Split",
                "descricao" => "Este serviço refere-se a mão de obra de manutenção de ar condicionado. Eventuais peças ou materiais utilizados para a manutenção, não estão cobertos neste orçamento, devendo serem adquiridos em orçamento específico, após visita de diagnóstico.",
                "valor" => "20.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "manutencao-ou-remocao-de-ar-split",
                "image_url" => ""
            ], [
                "id" => "18",
                "id_servico" => "21",
                "nome" => "Micropigmentadora facial",
                "descricao" => "Micropigmentação com utilização de caneta elétrica para introdução do pigmento.",
                "valor" => "31.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "micropigmentadora-facial",
                "image_url" => ""
            ], [
                "id" => "19",
                "id_servico" => "25",
                "nome" => "Psicoterapia Individual ",
                "descricao" => "Terapia cujo a finalidade é o tratamento de problemas psicológicos, tais como: Depressão, ansiedade, dificuldades de relacionamento ou outras difuldades pontuais que aflinja o dia a dia. Sessões de 50 minutos.",
                "valor" => "188.00",
                "data_cadastro" => "2018-10-02 15:06:01",
                "url" => "psicoterapia-individual",
                "image_url" => ""
            ], [
                "id" => "20",
                "id_servico" => "25",
                "nome" => "Orientação Profissional",
                "descricao" => "Auxiliar o individuo na descoberta de seus conhecimentos e de suas habilidades para a escolha de sua carreira profissional. Inclusive para os casos de redirecionamento de carreira. Este serviço será executado somente na modalidade On Line (Skype, Hangout, Facetime ou Whatsapp).",
                "valor" => "176.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "orientacao-profissional",
                "image_url" => ""
            ], [
                "id" => "21",
                "id_servico" => "25",
                "nome" => "Orientação aos Pais ",
                "descricao" => "Trata da interassão dos pais com os filhos, seu processo de comunicação. Assim como a orientação psicológica nas principais fases de desenvolvimento: infancia, puberdade e adolescência. Este serviço será executado somente na moralidade On Line (Skype, Hangout, Facetime ou Whatsapp).",
                "valor" => "176.00",
                "data_cadastro" => "2018-10-02 15:06:01",
                "url" => "orientacao-aos-pais",
                "image_url" => ""
            ], [
                "id" => "22",
                "id_servico" => "20",
                "nome" => "Pintura para mãos e pés",
                "descricao" => "Serviço de manicure e pedicure para embelezmento de pés e māos. Consiste em procedimentos de higienização, remoção do esmalte, modelagem, cutilagem, hidratação, esmaltação e decoração de unhas.",
                "valor" => "5.00",
                "data_cadastro" => "2019-06-24 10:50:47",
                "url" => "pintura-para-maos-e-pes",
                "image_url" => ""
            ], [
                "id" => "23",
                "id_servico" => "20",
                "nome" => "Alongamento de Unhas",
                "descricao" => "Técnicas para alongamento para obtenção de unhas longas e bonitas.",
                "valor" => "14.00",
                "data_cadastro" => "2019-07-17 11:05:26",
                "url" => "alongamento-de-unhas",
                "image_url" => ""
            ], [
                "id" => "24",
                "id_servico" => "8",
                "nome" => "Orientação nutricional",
                "descricao" => "Atendimento focado em pacientes que buscam:\nReeducação alimentar individual ou familiar; \nManejo nutricional para diabéticos, hipertensos ou com dislipidemia (colesterol, triglicerídeos, ferritina, esteatose); \nGestantes e Nutriz;\nEmagrecimento ou ganho de peso;\nAtletas",
                "valor" => "60.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "orientacao-nutricional",
                "image_url" => ""
            ], [
                "id" => "25",
                "id_servico" => "12",
                "nome" => "Acompanhamento e Vigilância",
                "descricao" => "Serviço de acompanhamento e vigilância de pessoa enfermo ou idoso, com atendimento residencial ou em hospital.\nImportante relatar particularidades do paciente no campo de \"Informações Adicionais\"",
                "valor" => "12.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "acompanhamento-e-vigilancia",
                "image_url" => ""
            ], [
                "id" => "26",
                "id_servico" => "26",
                "nome" => "Instalação ou manutenção hidráulica",
                "descricao" => "Serviço de instalação ou manutenção hidráulica residencial. O valor deste serviço inclui somente mão de obra, sendo responsabilidade do contratante a aquisição e disponibilização dos materiais a serem aplicados no serviço.",
                "valor" => "20.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "instalacao-ou-manutencao-hidraulica",
                "image_url" => ""
            ], [
                "id" => "27",
                "id_servico" => "27",
                "nome" => "Serviço de pintura de paredes",
                "descricao" => "Serviço de pintura interna ou externa de imóveis. O valor deste serviço inclui somente mão de obra, sendo responsabilidade do contratante a aquisição e disponibilização dos materiais a serem aplicados no serviço como, tinta, verniz, solventes, massa corrida, lixas, etc. Os equipamentos de pintura serão de responsabilidade do profissional (pinceis, rolos, mascara, escada, etc.). ",
                "valor" => "10.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "servico-de-pintura-de-paredes",
                "image_url" => ""
            ], [
                "id" => "28",
                "id_servico" => "28",
                "nome" => "Preparatório para Curso ou Concurso",
                "descricao" => "Aulas preparatórias para apoio a capacitação para prova. Descreva os objetivos e o curso ou concurso do aluno nas informações adicionais.",
                "valor" => "82.00",
                "data_cadastro" => "2018-10-02 15:04:39",
                "url" => "preparatorio-para-curso-ou-concurso",
                "image_url" => ""
            ], [
                "id" => "32",
                "id_servico" => "30",
                "nome" => "Geral",
                "descricao" => "Geral",
                "valor" => "100.00",
                "data_cadastro" => "2019-04-18 13:22:42",
                "url" => null,
                "image_url" => null
            ], [
                "id" => "33",
                "id_servico" => "30",
                "nome" => "Serviços de Maquiagem",
                "descricao" => "",
                "valor" => "130.00",
                "data_cadastro" => "2019-04-20 16:46:58",
                "url" => null,
                "image_url" => null
            ], [
                "id" => "34",
                "id_servico" => "20",
                "nome" => "Pacote de manutenção de alongamento de unhas em fibra de vidro",
                "descricao" => "Este procedimento visa garantir a durabilidade e beleza do alongamento de unhas em fibra de vidro. O prazo recomendado para a realização deste procedimento é 15 dias após a aplicação do alongamento e depois desta primeira manutenção o prazo de manutenção poderá ser executado após 30 dias. Este serviço NÃO inclui procedimento de remoção ou recolocação, que devem serem solicitados em pedido específico.",
                "valor" => "115.00",
                "data_cadastro" => "2019-07-17 11:15:33",
                "url" => null,
                "image_url" => null
            ], [
                "id" => "35",
                "id_servico" => "20",
                "nome" => "Remoção ou reposição de alongamento de unhas",
                "descricao" => "Serviço de remoção ou reposição de alongamentos de unhas em fibra de vidro, acrílico ou gel. Quando for necessário realizar a reposição de uma quebrada e há existência de resíduos para a remoção, deverá ser solicitado ambos os serviços, proporcional ao número de unhas.",
                "valor" => "10.00",
                "data_cadastro" => "2019-07-11 22:41:47",
                "url" => null,
                "image_url" => null,
            ]
        ]);
    }
}
