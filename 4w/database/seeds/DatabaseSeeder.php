<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(EstadoTableSeeder::class);
        $this->call(ServicoTableSeeder::class);
        $this->call(ServicoBaseTableSeeder::class);
        $this->call(ServicoBaseAgravanteTableSeeder::class);
        $this->call(CidadeTableSeeder::class);
        $this->call(AgravanteOpcoesTableSeeder::class);
    }
}
