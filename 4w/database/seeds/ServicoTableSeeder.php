<?php

use Illuminate\Database\Seeder;

class ServicoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servico')->insert([
            [
                "id" => "1",
                "descricao" => "Diarista",
                "data_cadastro" => "2017-12-03 20:43:55",
                "url" => "diarista",
                "top_image_url" => "https://www.sou4w.com.br/wp-content/uploads/2017/05/01-Quer-sair-e-não-tem-com-quem-deixar-as-crianças_menor.png",
            ], [
                "id" => "8",
                "descricao" => "Nutricionista",
                "data_cadastro" => "2017-12-05 15:57:36",
                "url" => "nutricionista",
                "top_image_url" => "",
            ], [
                "id" => "9",
                "descricao" => "Professor Particular - Reforço Escolar",
                "data_cadastro" => "2018-03-16 17:20:36",
                "url" => "professor-particular",
                "top_image_url" => "",
            ], [
                "id" => "10",
                "descricao" => "Babá (Baby Sitter)",
                "data_cadastro" => "2017-12-13 11:42:59",
                "url" => "baba-baby-sitter",
                "top_image_url" => "",
            ], [
                "id" => "12",
                "descricao" => "Cuidadora",
                "data_cadastro" => "2018-03-28 14:45:32",
                "url" => "cuidadora",
                "top_image_url" => "",
            ], [
                "id" => "13",
                "descricao" => "Instrutor de Violão",
                "data_cadastro" => "2018-03-16 17:21:17",
                "url" => "instrutor-de-violao",
                "top_image_url" => "",
            ], [
                "id" => "15",
                "descricao" => "Passeador de cães",
                "data_cadastro" => "2018-03-23 15:44:19",
                "url" => "passeador-de-caes",
                "top_image_url" => "",
            ], [
                "id" => "17",
                "descricao" => "Eletricista",
                "data_cadastro" => "2017-12-13 11:52:39",
                "url" => "eletricista",
                "top_image_url" => "",
            ], [
                "id" => "18",
                "descricao" => "Costureira",
                "data_cadastro" => "2017-12-13 11:53:22",
                "url" => "costureira",
                "top_image_url" => "",
            ], [
                "id" => "19",
                "descricao" => "Depiladora",
                "data_cadastro" => "2017-12-13 11:57:42",
                "url" => "depiladora",
                "top_image_url" => "",
            ], [
                "id" => "20",
                "descricao" => "Manicure",
                "data_cadastro" => "2017-12-13 11:58:38",
                "url" => "manicure",
                "top_image_url" => "",
            ], [
                "id" => "21",
                "descricao" => "Esteticísta",
                "data_cadastro" => "2018-02-08 16:02:40",
                "url" => "esteticista",
                "top_image_url" => "",
            ], [
                "id" => "22",
                "descricao" => "Enfermeiro",
                "data_cadastro" => "2018-03-07 19:38:28",
                "url" => "enfermeiro",
                "top_image_url" => "",
            ], [
                "id" => "23",
                "descricao" => "Instrutor de informática para terceira idade",
                "data_cadastro" => "2018-03-23 16:57:21",
                "url" => "instrutor-de-informatica-para-terceira-idade",
                "top_image_url" => "",
            ], [
                "id" => "24",
                "descricao" => "Assistência Técnica em Ar Condicionado",
                "data_cadastro" => "2018-03-28 14:29:32",
                "url" => "assistencia-tecnica-em-ar-condicionado",
                "top_image_url" => "",
            ], [
                "id" => "25",
                "descricao" => "Psicólogo",
                "data_cadastro" => "2018-04-26 12:50:55",
                "url" => "psicologo",
                "top_image_url" => "",
            ], [
                "id" => "26",
                "descricao" => "Encanador",
                "data_cadastro" => "2018-07-27 10:54:44",
                "url" => "encanador",
                "top_image_url" => "",
            ], [
                "id" => "27",
                "descricao" => "Pintor",
                "data_cadastro" => "2018-07-27 12:08:25",
                "url" => "pintor",
                "top_image_url" => "",
            ], [
                "id" => "28",
                "descricao" => "Professor Particular - Curso ou Concurso",
                "data_cadastro" => "2018-08-27 15:56:27",
                "url" => "professor-particular-curso-ou-concurso",
                "top_image_url" => "",
            ], [
                "id" => "30",
                "descricao" => "Maquiadora",
                "data_cadastro" => "2019-04-15 13:11:55",
                "url" => "maquiadora",
                "top_image_url" => "",
            ]
        ]);
    }
}
