<?php

use Illuminate\Database\Seeder;

class ServicoBaseAgravanteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servico_base_agravante')->insert([
            [
                "id" => "2",
                "id_base_servico" => "2",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe a área do Imóvel",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-18 10:24:28"
            ], [
                "id" => "3",
                "id_base_servico" => "2",
                "valor" => "10.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe a quantidade de banheiros do Imóvel",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "1",
                "data_cadastro" => "2018-03-01 17:56:25"
            ], [
                "id" => "5",
                "id_base_servico" => "2",
                "valor" => "10.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Limpar área externa (pátio ou calçada)",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-01 19:52:57"
            ], [
                "id" => "6",
                "id_base_servico" => "2",
                "valor" => "5.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Limpar Vidros (janelas, portas ou divisórias)",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-01-17 19:29:30"
            ], [
                "id" => "7",
                "id_base_servico" => "2",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Informe a quantidade de peças de roupas para passar",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 17:05:24"
            ], [
                "id" => "8",
                "id_base_servico" => "2",
                "valor" => "4.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Lavar Roupa (com máquina de lavar)",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-01-17 19:39:01"
            ], [
                "id" => "9",
                "id_base_servico" => "2",
                "valor" => "3.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Lavar Louça",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-01-17 19:40:52"
            ], [
                "id" => "10",
                "id_base_servico" => "3",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informa a área do Imóvel",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-01 20:58:25"
            ], [
                "id" => "11",
                "id_base_servico" => "4",
                "valor" => "50.00",
                "regra" => "2",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe a quantidade de crianças ou pré-adolescentes",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "1",
                "data_cadastro" => "2018-01-17 20:19:59"
            ], [
                "id" => "12",
                "id_base_servico" => "4",
                "valor" => "80.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe a quantidade de horas a disposição",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "1",
                "data_cadastro" => "2018-01-17 20:21:38"
            ], [
                "id" => "13",
                "id_base_servico" => "4",
                "valor" => "10.00",
                "regra" => "2",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Necessita administrar alimentação",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-01-17 20:23:39"
            ], [
                "id" => "15",
                "id_base_servico" => "4",
                "valor" => "10.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Necessita administrar medicação",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-01-17 20:25:40"
            ], [
                "id" => "18",
                "id_base_servico" => "5",
                "valor" => "180.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Reforçar ou refazer costura",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-06 07:59:30"
            ], [
                "id" => "19",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Bainha de calça (italiana)",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-06 07:59:52"
            ], [
                "id" => "21",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Ajustar ou soltar blusa simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:19:08"
            ], [
                "id" => "22",
                "id_base_servico" => "5",
                "valor" => "400.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Ajustar ou soltar blusa com manga",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:26:29"
            ], [
                "id" => "23",
                "id_base_servico" => "5",
                "valor" => "600.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Ajustar ou soltar blazer forrado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:27:46"
            ], [
                "id" => "24",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Ajustar ou soltar cintura de calça ou saia",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:29:22"
            ], [
                "id" => "25",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Ajustar ou soltar pernas de calça",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:29:57"
            ], [
                "id" => "26",
                "id_base_servico" => "5",
                "valor" => "400.00",
                "regra" => "1",
                "ordem_formulario" => "10",
                "rotulo_formulario" => "Ajustar ou soltar vestido simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:31:28"
            ], [
                "id" => "27",
                "id_base_servico" => "5",
                "valor" => "500.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Ajustar ou soltar vestido forrado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:45:13"
            ], [
                "id" => "28",
                "id_base_servico" => "5",
                "valor" => "900.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Ajustar ou soltar vestido de festa",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:46:07"
            ], [
                "id" => "29",
                "id_base_servico" => "5",
                "valor" => "500.00",
                "regra" => "1",
                "ordem_formulario" => "13",
                "rotulo_formulario" => "Ajustar ombro de blazer forrado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 09:47:47"
            ], [
                "id" => "30",
                "id_base_servico" => "5",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "14",
                "rotulo_formulario" => "Barra de lençol",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:03:28"
            ], [
                "id" => "31",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "15",
                "rotulo_formulario" => "Colocar remendo ou emenda",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:04:08"
            ], [
                "id" => "32",
                "id_base_servico" => "5",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "16",
                "rotulo_formulario" => "Diminuir alça de blusa",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:05:09"
            ], [
                "id" => "33",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "17",
                "rotulo_formulario" => "Diminuir comprimento de blusa ou vestido",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:06:07"
            ], [
                "id" => "34",
                "id_base_servico" => "5",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "18",
                "rotulo_formulario" => "Diminuir comprimento de manga simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:06:50"
            ], [
                "id" => "35",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "19",
                "rotulo_formulario" => "Diminuir comprimento de manga de camisa com punho",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:07:36"
            ], [
                "id" => "36",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "20",
                "rotulo_formulario" => "Diminuir comprimento de manga de blazer forrado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:08:33"
            ], [
                "id" => "37",
                "id_base_servico" => "5",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "21",
                "rotulo_formulario" => "Soltar ou dar pences em blusas ou vestidos",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:09:08"
            ], [
                "id" => "38",
                "id_base_servico" => "5",
                "valor" => "400.00",
                "regra" => "1",
                "ordem_formulario" => "22",
                "rotulo_formulario" => "Trocar colarinho",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:09:57"
            ], [
                "id" => "39",
                "id_base_servico" => "5",
                "valor" => "40.00",
                "regra" => "1",
                "ordem_formulario" => "23",
                "rotulo_formulario" => "Trocar botão ou fazer casa (unidade)",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:10:47"
            ], [
                "id" => "40",
                "id_base_servico" => "5",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "24",
                "rotulo_formulario" => "Trocar ziper de calça ou saia",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:11:32"
            ], [
                "id" => "43",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "25",
                "rotulo_formulario" => "Trocar ziper de jaqueta",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:12:06"
            ], [
                "id" => "44",
                "id_base_servico" => "5",
                "valor" => "280.00",
                "regra" => "1",
                "ordem_formulario" => "26",
                "rotulo_formulario" => "Trocar ziper de vestido simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:13:19"
            ], [
                "id" => "47",
                "id_base_servico" => "5",
                "valor" => "400.00",
                "regra" => "1",
                "ordem_formulario" => "27",
                "rotulo_formulario" => "Trocar ziper de vestido forrado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:13:58"
            ], [
                "id" => "49",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "29",
                "rotulo_formulario" => "Trocar ziper de macacão",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:14:42"
            ], [
                "id" => "50",
                "id_base_servico" => "6",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Instalação e montagem de QDG",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:00:19"
            ], [
                "id" => "53",
                "id_base_servico" => "6",
                "valor" => "35.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Instalação de tomadas padrão simples 3P + T",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:38:40"
            ], [
                "id" => "54",
                "id_base_servico" => "6",
                "valor" => "35.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Instalação de interruptores padrão simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:39:35"
            ], [
                "id" => "56",
                "id_base_servico" => "6",
                "valor" => "120.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Instalação de chuveiro",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 11:39:31"
            ], [
                "id" => "58",
                "id_base_servico" => "6",
                "valor" => "50.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Instalação globo/estopa ou receptáculos para lampadas",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:42:17"
            ], [
                "id" => "59",
                "id_base_servico" => "6",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "15",
                "rotulo_formulario" => "Instalação da campainha",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:45:12"
            ], [
                "id" => "62",
                "id_base_servico" => "7",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Instalação ou troca de tomada simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:48:10"
            ], [
                "id" => "63",
                "id_base_servico" => "7",
                "valor" => "120.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Instalação ou troca de tomada dupla",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:49:58"
            ], [
                "id" => "64",
                "id_base_servico" => "7",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Instalação ou troca de interruptor simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:50:39"
            ], [
                "id" => "65",
                "id_base_servico" => "7",
                "valor" => "110.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Instalação ou troca de interruptor duplo",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:51:09"
            ], [
                "id" => "67",
                "id_base_servico" => "7",
                "valor" => "180.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Instalação de luminária ou lustre simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:56:01"
            ], [
                "id" => "68",
                "id_base_servico" => "7",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Instalação de luminária ou lustre conjugado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:58:58"
            ], [
                "id" => "69",
                "id_base_servico" => "7",
                "valor" => "230.00",
                "regra" => "1",
                "ordem_formulario" => "20",
                "rotulo_formulario" => "Instalação de arandela ou spot de jardim",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:01:16"
            ], [
                "id" => "70",
                "id_base_servico" => "7",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "32",
                "rotulo_formulario" => "Instalação de ventilador de teto sem tomada",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:08:09"
            ], [
                "id" => "72",
                "id_base_servico" => "7",
                "valor" => "400.00",
                "regra" => "1",
                "ordem_formulario" => "31",
                "rotulo_formulario" => "Instalação de ventilador de teto com tomada",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:06:20"
            ], [
                "id" => "73",
                "id_base_servico" => "7",
                "valor" => "260.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Instalação de chuveiro simples até 5500 W",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:37:23"
            ], [
                "id" => "74",
                "id_base_servico" => "7",
                "valor" => "360.00",
                "regra" => "1",
                "ordem_formulario" => "13",
                "rotulo_formulario" => "Instalação de chuveiro com pressurizador",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:41:47"
            ], [
                "id" => "75",
                "id_base_servico" => "7",
                "valor" => "250.00",
                "regra" => "1",
                "ordem_formulario" => "14",
                "rotulo_formulario" => "Instalação de torneira elétrica",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:47:31"
            ], [
                "id" => "76",
                "id_base_servico" => "7",
                "valor" => "230.00",
                "regra" => "1",
                "ordem_formulario" => "15",
                "rotulo_formulario" => "instalação de campainha residêncial",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:50:36"
            ], [
                "id" => "78",
                "id_base_servico" => "7",
                "valor" => "850.00",
                "regra" => "1",
                "ordem_formulario" => "16",
                "rotulo_formulario" => "Instalação ou troca de QDG até 06 Disjuntores",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:54:19"
            ], [
                "id" => "79",
                "id_base_servico" => "7",
                "valor" => "80.00",
                "regra" => "1",
                "ordem_formulario" => "19",
                "rotulo_formulario" => "Instalação de cabos por metro - Quantos metros?",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:07:17"
            ], [
                "id" => "80",
                "id_base_servico" => "7",
                "valor" => "500.00",
                "regra" => "1",
                "ordem_formulario" => "21",
                "rotulo_formulario" => "Instalação de exaustor de cozinha ou banheiros",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:06:39"
            ], [
                "id" => "81",
                "id_base_servico" => "7",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "18",
                "rotulo_formulario" => "Instalação ou troca de disjuntor trifázico simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:09:11"
            ], [
                "id" => "82",
                "id_base_servico" => "7",
                "valor" => "130.00",
                "regra" => "2",
                "ordem_formulario" => "22",
                "rotulo_formulario" => "Instalação de sensor de presença",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:11:20"
            ], [
                "id" => "83",
                "id_base_servico" => "7",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "23",
                "rotulo_formulario" => "Instalação de ponto ou ramal telefônico",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:12:20"
            ], [
                "id" => "84",
                "id_base_servico" => "7",
                "valor" => "700.00",
                "regra" => "1",
                "ordem_formulario" => "24",
                "rotulo_formulario" => "Instalação de interfone sem fechadura até 30m",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:14:56"
            ], [
                "id" => "85",
                "id_base_servico" => "7",
                "valor" => "720.00",
                "regra" => "1",
                "ordem_formulario" => "25",
                "rotulo_formulario" => "Instalação de interfone com fechadura",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:16:04"
            ], [
                "id" => "86",
                "id_base_servico" => "7",
                "valor" => "500.00",
                "regra" => "1",
                "ordem_formulario" => "26",
                "rotulo_formulario" => "Instalação de fechadura eletrônica",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:16:52"
            ], [
                "id" => "87",
                "id_base_servico" => "7",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "27",
                "rotulo_formulario" => "Instalação de luminária de emergência",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:17:51"
            ], [
                "id" => "88",
                "id_base_servico" => "7",
                "valor" => "150.00",
                "regra" => "1",
                "ordem_formulario" => "28",
                "rotulo_formulario" => "Instalação de câmera de segurança",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 12:18:57"
            ], [
                "id" => "89",
                "id_base_servico" => "2",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o tipo de imóvel",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-01 18:18:49"
            ], [
                "id" => "90",
                "id_base_servico" => "3",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o tipo de imóvel",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-01 21:01:30"
            ], [
                "id" => "91",
                "id_base_servico" => "3",
                "valor" => "10.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe a quantidade de banheiros do imóvel",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-01 21:04:00"
            ], [
                "id" => "92",
                "id_base_servico" => "3",
                "valor" => "15.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Limpar área externa ou fachada",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-01 21:05:44"
            ], [
                "id" => "93",
                "id_base_servico" => "3",
                "valor" => "10.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Limpar vidros (Janelas, portas, divisórias ou fachadas)",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-01 21:08:12"
            ], [
                "id" => "94",
                "id_base_servico" => "9",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o grau de autonomia motora",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "1",
                "data_cadastro" => "2018-06-09 18:47:38"
            ], [
                "id" => "95",
                "id_base_servico" => "9",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "14",
                "rotulo_formulario" => "Paciente com obesidade mórbida?",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 17:53:33"
            ], [
                "id" => "96",
                "id_base_servico" => "9",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe o tipo de lesão para execução de curativo",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:15:19"
            ], [
                "id" => "97",
                "id_base_servico" => "9",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Informe o tamanho da lesão para execução de curativo",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:18:01"
            ], [
                "id" => "98",
                "id_base_servico" => "9",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Informe a localização da lesão para curativo",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:20:22"
            ], [
                "id" => "99",
                "id_base_servico" => "9",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Cuidados integrais para higiene e conforto. Informe o local para o banho:",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:21:24"
            ], [
                "id" => "100",
                "id_base_servico" => "9",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Necessita administrar medicação? (Somente com receita médica válida)",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:24:19"
            ], [
                "id" => "101",
                "id_base_servico" => "9",
                "valor" => "30.00",
                "regra" => "1",
                "ordem_formulario" => "13",
                "rotulo_formulario" => "Paciente é diabético?",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:27:18"
            ], [
                "id" => "102",
                "id_base_servico" => "9",
                "valor" => "50.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Paciente com doença infecto-contagiosa?",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:27:52"
            ], [
                "id" => "103",
                "id_base_servico" => "9",
                "valor" => "30.00",
                "regra" => "1",
                "ordem_formulario" => "15",
                "rotulo_formulario" => "Paciente com doença auto-imune?",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 17:52:27"
            ], [
                "id" => "104",
                "id_base_servico" => "9",
                "valor" => "60.00",
                "regra" => "1",
                "ordem_formulario" => "16",
                "rotulo_formulario" => "Paciente com problemas neurológicos?",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 17:51:37"
            ], [
                "id" => "112",
                "id_base_servico" => "9",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "17",
                "rotulo_formulario" => "Adquirir pacotes de atendimento",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "1",
                "data_cadastro" => "2018-06-09 17:49:30"
            ], [
                "id" => "114",
                "id_base_servico" => "5",
                "valor" => "280.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Bainha de calça jeans (Original)",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:16:15"
            ], [
                "id" => "115",
                "id_base_servico" => "10",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o pacote de visitas que deseja ",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 09:31:35"
            ], [
                "id" => "116",
                "id_base_servico" => "11",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o tempo de duração da aula",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 10:43:32"
            ], [
                "id" => "118",
                "id_base_servico" => "11",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe o número de aulas que deseja contratar",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-08-27 16:24:42"
            ], [
                "id" => "119",
                "id_base_servico" => "11",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe o número de Alunos",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 12:03:18"
            ], [
                "id" => "120",
                "id_base_servico" => "12",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o tempo de duração da aula",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 11:40:42"
            ], [
                "id" => "121",
                "id_base_servico" => "12",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe o número de aulas que deseja",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-08-26 12:48:07"
            ], [
                "id" => "122",
                "id_base_servico" => "12",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe o número de Alunos",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-08-26 12:55:08"
            ], [
                "id" => "123",
                "id_base_servico" => "11",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Informe a disciplina ",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-03 10:13:43"
            ], [
                "id" => "124",
                "id_base_servico" => "12",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Informe a disciplina",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-10-01 12:20:46"
            ], [
                "id" => "127",
                "id_base_servico" => "8",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o tempo de duração da aula",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 12:33:46"
            ], [
                "id" => "128",
                "id_base_servico" => "8",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Adquira pacotes de aulas",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 16:02:05"
            ], [
                "id" => "129",
                "id_base_servico" => "8",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe o estágio de conhecimento para aplicação das aulas",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 15:40:08"
            ], [
                "id" => "130",
                "id_base_servico" => "13",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe a quantidade de cães",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 17:37:39"
            ], [
                "id" => "132",
                "id_base_servico" => "13",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe o tempo de duração do passeio",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 17:44:00"
            ], [
                "id" => "133",
                "id_base_servico" => "13",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe a quantidade de passeios por dia",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 17:47:46"
            ], [
                "id" => "135",
                "id_base_servico" => "13",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Informe a frequência de dias da semana",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 18:07:19"
            ], [
                "id" => "136",
                "id_base_servico" => "13",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Informe o perfil de interação com outros cães",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-16 18:12:23"
            ], [
                "id" => "137",
                "id_base_servico" => "5",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Bainha de calça a mão",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 10:17:41"
            ], [
                "id" => "138",
                "id_base_servico" => "14",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Blusa simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 11:58:04"
            ], [
                "id" => "139",
                "id_base_servico" => "14",
                "valor" => "140.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Blusa com detalhes",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 11:59:30"
            ], [
                "id" => "140",
                "id_base_servico" => "14",
                "valor" => "125.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Bermudas",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 09:57:46"
            ], [
                "id" => "141",
                "id_base_servico" => "14",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Blazer sem forro",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:02:09"
            ], [
                "id" => "143",
                "id_base_servico" => "14",
                "valor" => "400.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Blazer forrado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:03:10"
            ], [
                "id" => "144",
                "id_base_servico" => "14",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Calça social",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 10:05:33"
            ], [
                "id" => "145",
                "id_base_servico" => "14",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Camisa social feminina",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:06:28"
            ], [
                "id" => "146",
                "id_base_servico" => "14",
                "valor" => "250.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Camisa social masculina",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:07:12"
            ], [
                "id" => "148",
                "id_base_servico" => "14",
                "valor" => "150.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Saia",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:08:48"
            ], [
                "id" => "149",
                "id_base_servico" => "14",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "10",
                "rotulo_formulario" => "Short",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:10:08"
            ], [
                "id" => "150",
                "id_base_servico" => "14",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Vestido simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:13:46"
            ], [
                "id" => "151",
                "id_base_servico" => "14",
                "valor" => "650.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Vestido de festa",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:16:54"
            ], [
                "id" => "152",
                "id_base_servico" => "14",
                "valor" => "150.00",
                "regra" => "1",
                "ordem_formulario" => "14",
                "rotulo_formulario" => "Calça Infantil",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 09:54:50"
            ], [
                "id" => "153",
                "id_base_servico" => "14",
                "valor" => "2000.00",
                "regra" => "1",
                "ordem_formulario" => "13",
                "rotulo_formulario" => "Vestido de festas longo e c/detalhes manuais",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-21 12:21:52"
            ], [
                "id" => "154",
                "id_base_servico" => "14",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "15",
                "rotulo_formulario" => "Roupa de Bebê",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 10:15:03"
            ], [
                "id" => "155",
                "id_base_servico" => "14",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "16",
                "rotulo_formulario" => "Short infantil",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 10:20:09"
            ], [
                "id" => "156",
                "id_base_servico" => "14",
                "valor" => "150.00",
                "regra" => "1",
                "ordem_formulario" => "17",
                "rotulo_formulario" => "Vestido Infantil",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 10:21:48"
            ], [
                "id" => "157",
                "id_base_servico" => "14",
                "valor" => "650.00",
                "regra" => "1",
                "ordem_formulario" => "18",
                "rotulo_formulario" => "Vestido Infantil de festa",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 10:23:44"
            ], [
                "id" => "158",
                "id_base_servico" => "6",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Passagem de cabeamento em geral",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:23:19"
            ], [
                "id" => "159",
                "id_base_servico" => "15",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o estagio de conhecimento e utilização do mouse e touchscreen",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 16:51:27"
            ], [
                "id" => "160",
                "id_base_servico" => "15",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe a quantidade de aulas (Tempo duração cada aula de 2h.)",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-23 16:44:47"
            ], [
                "id" => "161",
                "id_base_servico" => "6",
                "valor" => "115.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Instalação de lustre",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:49:02"
            ], [
                "id" => "162",
                "id_base_servico" => "6",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Instalação de ventilador de teto com tomada",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:52:59"
            ], [
                "id" => "163",
                "id_base_servico" => "6",
                "valor" => "130.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Instalação de torneira elétrica",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:55:00"
            ], [
                "id" => "164",
                "id_base_servico" => "6",
                "valor" => "250.00",
                "regra" => "1",
                "ordem_formulario" => "10",
                "rotulo_formulario" => "Instalação de exaustor de cozinha ou banheiro",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 09:57:59"
            ], [
                "id" => "165",
                "id_base_servico" => "6",
                "valor" => "70.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Instalação de sensor de presença",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:01:46"
            ], [
                "id" => "166",
                "id_base_servico" => "6",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Instalação de ponto ou ramal telefônico",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:04:07"
            ], [
                "id" => "167",
                "id_base_servico" => "6",
                "valor" => "80.00",
                "regra" => "1",
                "ordem_formulario" => "13",
                "rotulo_formulario" => "Instalação de câmera sem cabeamento",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:08:55"
            ], [
                "id" => "168",
                "id_base_servico" => "6",
                "valor" => "40.00",
                "regra" => "1",
                "ordem_formulario" => "14",
                "rotulo_formulario" => "Instalação de cabo trifásico acima por metro",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 10:15:31"
            ], [
                "id" => "169",
                "id_base_servico" => "7",
                "valor" => "130.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Instalação de spot ou receptáculo para 01 lampada",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:29:12"
            ], [
                "id" => "170",
                "id_base_servico" => "7",
                "valor" => "150.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Instalação de spot ou receptáculo para 02 lampadas",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:31:09"
            ], [
                "id" => "171",
                "id_base_servico" => "7",
                "valor" => "160.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Instalação de spot ou receptáculo para 3 lampadas",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:32:14"
            ], [
                "id" => "172",
                "id_base_servico" => "7",
                "valor" => "280.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Instalação de chuveiro acima de 5500 W",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:44:51"
            ], [
                "id" => "173",
                "id_base_servico" => "7",
                "valor" => "1100.00",
                "regra" => "1",
                "ordem_formulario" => "17",
                "rotulo_formulario" => "Instalação ou troca de QDG com mais de 06 Disjuntores",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 11:58:11"
            ], [
                "id" => "174",
                "id_base_servico" => "16",
                "valor" => "540.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe a quantidade de equipamentos de 7.000 a 9,000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:48:00"
            ], [
                "id" => "175",
                "id_base_servico" => "16",
                "valor" => "600.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe a quantidade de equipamentos de 12.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:48:25"
            ], [
                "id" => "176",
                "id_base_servico" => "16",
                "valor" => "700.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe a quantidade de equipamentos de 18.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:48:52"
            ], [
                "id" => "177",
                "id_base_servico" => "16",
                "valor" => "840.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Informe a quantidade de equipamentos de 22.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:49:20"
            ], [
                "id" => "178",
                "id_base_servico" => "16",
                "valor" => "1100.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Informe a quantidade de equipamentos acima de 24.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:50:24"
            ], [
                "id" => "179",
                "id_base_servico" => "16",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Informe a metragem total de tubulação em cobre para instalação",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:54:58"
            ], [
                "id" => "180",
                "id_base_servico" => "16",
                "valor" => "50.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Informe a metragem total de canaleta de PVC para instalação",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:08:32"
            ], [
                "id" => "181",
                "id_base_servico" => "16",
                "valor" => "180.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Instalação de disjuntor trifásico simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:16:08"
            ], [
                "id" => "182",
                "id_base_servico" => "16",
                "valor" => "50.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Instalação de cabos elétricos por metro",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:17:57"
            ], [
                "id" => "183",
                "id_base_servico" => "16",
                "valor" => "80.00",
                "regra" => "1",
                "ordem_formulario" => "10",
                "rotulo_formulario" => "Instalação de tomada simples",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:22:11"
            ], [
                "id" => "184",
                "id_base_servico" => "16",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Descreva no campo \"informações Adicionais\" a(s) marca(s) e modelo(s) ",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 15:26:24"
            ], [
                "id" => "185",
                "id_base_servico" => "16",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Projeto de análise e adequação para o(s) ambiente(s)",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 16:07:31"
            ], [
                "id" => "186",
                "id_base_servico" => "18",
                "valor" => "1250.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Microblanding",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 17:30:23"
            ], [
                "id" => "187",
                "id_base_servico" => "18",
                "valor" => "1450.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Micropigmentação de olhos e boca",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 17:24:43"
            ], [
                "id" => "188",
                "id_base_servico" => "18",
                "valor" => "1250.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Micropigmentação fio a fio",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 17:29:46"
            ], [
                "id" => "189",
                "id_base_servico" => "18",
                "valor" => "130.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Designer de sobrancelhas",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 17:37:34"
            ], [
                "id" => "190",
                "id_base_servico" => "18",
                "valor" => "380.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Alongamento de cilios",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-03-28 17:41:17"
            ], [
                "id" => "191",
                "id_base_servico" => "19",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Defina a forma de atendimento",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-25 16:20:54"
            ], [
                "id" => "192",
                "id_base_servico" => "19",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o número de sessões",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-25 16:20:08"
            ], [
                "id" => "193",
                "id_base_servico" => "19",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe o genero",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-25 15:56:11"
            ], [
                "id" => "194",
                "id_base_servico" => "19",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Informe a faixa etária do paciente",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-25 16:02:06"
            ], [
                "id" => "195",
                "id_base_servico" => "20",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Indique seu objetivo",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-25 17:10:58"
            ], [
                "id" => "196",
                "id_base_servico" => "20",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Indique sua situação",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-25 17:22:50"
            ], [
                "id" => "197",
                "id_base_servico" => "21",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Escolha o pacote de sessōes que deseja contratar",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-04-25 17:53:52"
            ], [
                "id" => "198",
                "id_base_servico" => "22",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Como você quer suas cutículas?",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2019-07-17 11:48:39"
            ], [
                "id" => "199",
                "id_base_servico" => "22",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Pintar unhas de",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2019-06-24 10:57:56"
            ], [
                "id" => "200",
                "id_base_servico" => "22",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Decoração com micro pintura",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-05-25 15:19:58"
            ], [
                "id" => "201",
                "id_base_servico" => "22",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Decoração com pedras",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-05-25 15:27:33"
            ], [
                "id" => "202",
                "id_base_servico" => "23",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Defina o material para aplicação no alongamento?",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2019-07-17 17:15:35"
            ], [
                "id" => "204",
                "id_base_servico" => "23",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Deseja contratar serviço de manutenção?",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2019-07-17 17:13:53"
            ], [
                "id" => "215",
                "id_base_servico" => "24",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Primeira Consulta",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-05-25 17:49:12"
            ], [
                "id" => "216",
                "id_base_servico" => "24",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe as consultas de revisão que desejas contratar",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-05-25 17:59:37"
            ], [
                "id" => "217",
                "id_base_servico" => "24",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Contratar exame de Bioimpedância",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-05-25 18:01:26"
            ], [
                "id" => "218",
                "id_base_servico" => "24",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Contratar exame Hortomolecular",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-05-25 18:02:50"
            ], [
                "id" => "219",
                "id_base_servico" => "9",
                "valor" => "330.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Inserção ou remoção de sonda para alimentação",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 17:59:07"
            ], [
                "id" => "220",
                "id_base_servico" => "9",
                "valor" => "330.00",
                "regra" => "1",
                "ordem_formulario" => "10",
                "rotulo_formulario" => "Instalação e Montagem de respirador artificial",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:08:33"
            ], [
                "id" => "221",
                "id_base_servico" => "9",
                "valor" => "50.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Nebulização",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:09:01"
            ], [
                "id" => "222",
                "id_base_servico" => "9",
                "valor" => "310.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Verificar respiração, pulso e pressão arterial",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:09:37"
            ], [
                "id" => "224",
                "id_base_servico" => "9",
                "valor" => "350.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Retirar pontos",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-09 18:10:19"
            ], [
                "id" => "225",
                "id_base_servico" => "17",
                "valor" => "380.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Manutenção e limpeza preventiva de split de 7.000 a 10.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-18 15:32:55"
            ], [
                "id" => "226",
                "id_base_servico" => "17",
                "valor" => "420.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Manutenção e limpeza preventiva de split de 12.000 a 18.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-18 15:34:26"
            ], [
                "id" => "227",
                "id_base_servico" => "17",
                "valor" => "455.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Manutenção e limpeza preventiva split acima de 18.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-18 15:43:06"
            ], [
                "id" => "228",
                "id_base_servico" => "17",
                "valor" => "695.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Manutenção corretiva de split de 7.000 a 12.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-18 15:44:00"
            ], [
                "id" => "229",
                "id_base_servico" => "17",
                "valor" => "800.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Manutenção corretiva de split de 18.000 BTUs ou acima",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-18 15:44:54"
            ], [
                "id" => "230",
                "id_base_servico" => "17",
                "valor" => "395.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Limpeza da evaporadora do ar condicionado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 10:26:52"
            ], [
                "id" => "232",
                "id_base_servico" => "17",
                "valor" => "395.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Retirada de vazamento de água",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 10:36:22"
            ], [
                "id" => "233",
                "id_base_servico" => "17",
                "valor" => "1000.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Carga de gás em split de 7.000 a 12.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-18 15:49:31"
            ], [
                "id" => "234",
                "id_base_servico" => "17",
                "valor" => "1150.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Carga de gás em split de 18.000 a 24.000 BTUs",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-18 15:50:12"
            ], [
                "id" => "235",
                "id_base_servico" => "17",
                "valor" => "1055.00",
                "regra" => "1",
                "ordem_formulario" => "10",
                "rotulo_formulario" => "Remover ar condicionado split",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-18 15:55:36"
            ], [
                "id" => "236",
                "id_base_servico" => "17",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Informe a marca do equipamento",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 10:29:27"
            ], [
                "id" => "237",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o grau de autonomia motora do paciente",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 12:54:18"
            ], [
                "id" => "238",
                "id_base_servico" => "25",
                "valor" => "2.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Paciente possui  com obesidade mórbida",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 12:59:45"
            ], [
                "id" => "239",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe o sexo do paciente",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 14:57:26"
            ], [
                "id" => "240",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Informe o local do acompanhamento?",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 14:59:34"
            ], [
                "id" => "241",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Informe o período diário do acompanhamento",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:07:19"
            ], [
                "id" => "242",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Informe os dias da semana do acompanhamento",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:32:03"
            ], [
                "id" => "243",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "15",
                "rotulo_formulario" => "Informe o número de horas diárias de acompanhamento",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:27:02"
            ], [
                "id" => "244",
                "id_base_servico" => "25",
                "valor" => "90.00",
                "regra" => "2",
                "ordem_formulario" => "16",
                "rotulo_formulario" => "Informe a quantidade de dias de acompanhamento deseja contratar. (Não contar folgas)",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "1",
                "data_cadastro" => "2018-06-20 16:39:06"
            ], [
                "id" => "245",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Segunda-feira",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:54:31"
            ], [
                "id" => "246",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Terça-feira",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:55:03"
            ], [
                "id" => "247",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Quarta-feira",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:55:35"
            ], [
                "id" => "248",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "10",
                "rotulo_formulario" => "Quinta-feira",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:56:02"
            ], [
                "id" => "249",
                "id_base_servico" => "25",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Sexta-feira",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:56:31"
            ], [
                "id" => "250",
                "id_base_servico" => "25",
                "valor" => "5.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Sábado",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:57:39"
            ], [
                "id" => "251",
                "id_base_servico" => "25",
                "valor" => "15.00",
                "regra" => "1",
                "ordem_formulario" => "13",
                "rotulo_formulario" => "Domingo",
                "tipo_formulario" => "2",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-06-20 15:58:09"
            ], [
                "id" => "252",
                "id_base_servico" => "17",
                "valor" => "1400.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Limpeza do condensadora com remoção do ar condicionado",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 10:41:15"
            ], [
                "id" => "253",
                "id_base_servico" => "26",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Instalação de torneira ou registro",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:04:52"
            ], [
                "id" => "254",
                "id_base_servico" => "26",
                "valor" => "500.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Instalação de chuveiro ou torneira elétricos",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:11:23"
            ], [
                "id" => "255",
                "id_base_servico" => "26",
                "valor" => "1200.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Instalação de vazo sanitário",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:16:05"
            ], [
                "id" => "256",
                "id_base_servico" => "26",
                "valor" => "450.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Instalação de caixa de descarga",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:17:44"
            ], [
                "id" => "257",
                "id_base_servico" => "26",
                "valor" => "150.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Instalação de acessórios para banheiros - informe o número de peças.",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:22:05"
            ], [
                "id" => "258",
                "id_base_servico" => "26",
                "valor" => "110.00",
                "regra" => "1",
                "ordem_formulario" => "6",
                "rotulo_formulario" => "Instalação de tubulação - informe a metragem linear ",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:28:23"
            ], [
                "id" => "259",
                "id_base_servico" => "26",
                "valor" => "150.00",
                "regra" => "1",
                "ordem_formulario" => "7",
                "rotulo_formulario" => "Escavação de parede ou solo para instalação ou reparo de tubulação - em metros",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:29:57"
            ], [
                "id" => "261",
                "id_base_servico" => "26",
                "valor" => "1150.00",
                "regra" => "1",
                "ordem_formulario" => "8",
                "rotulo_formulario" => "Instalação de caixa de água até 500 litros",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:34:12"
            ], [
                "id" => "262",
                "id_base_servico" => "26",
                "valor" => "1450.00",
                "regra" => "1",
                "ordem_formulario" => "9",
                "rotulo_formulario" => "Instalação de caixa de água acima de 500 litros",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:37:39"
            ], [
                "id" => "263",
                "id_base_servico" => "26",
                "valor" => "350.00",
                "regra" => "1",
                "ordem_formulario" => "10",
                "rotulo_formulario" => "Reparo de vazamentos em torneiras, registros ou caixas de descarga",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 11:49:16"
            ], [
                "id" => "264",
                "id_base_servico" => "27",
                "valor" => "550.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Pintura de fachada -  informe a área em m²",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 12:35:29"
            ], [
                "id" => "265",
                "id_base_servico" => "27",
                "valor" => "80.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Pintura interna com tinta lavável - informe a área em m²",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 12:31:57"
            ], [
                "id" => "266",
                "id_base_servico" => "27",
                "valor" => "140.00",
                "regra" => "1",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Pintura externa com tinta lavável - informe a área em m²",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 12:37:44"
            ], [
                "id" => "267",
                "id_base_servico" => "27",
                "valor" => "200.00",
                "regra" => "1",
                "ordem_formulario" => "4",
                "rotulo_formulario" => "Pintura de forro com tinta lavável - informe a área em m²",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 12:40:37"
            ], [
                "id" => "268",
                "id_base_servico" => "27",
                "valor" => "300.00",
                "regra" => "1",
                "ordem_formulario" => "5",
                "rotulo_formulario" => "Pintura com tinta acrílica - informe a área em m²",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 12:45:12"
            ], [
                "id" => "269",
                "id_base_servico" => "27",
                "valor" => "100.00",
                "regra" => "1",
                "ordem_formulario" => "11",
                "rotulo_formulario" => "Pintura com textura - informe a área em m²",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 13:42:01"
            ], [
                "id" => "270",
                "id_base_servico" => "27",
                "valor" => "140.00",
                "regra" => "1",
                "ordem_formulario" => "12",
                "rotulo_formulario" => "Pintura tinta óleo - informe a área em m²",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 13:44:18"
            ], [
                "id" => "271",
                "id_base_servico" => "27",
                "valor" => "1700.00",
                "regra" => "1",
                "ordem_formulario" => "13",
                "rotulo_formulario" => "Pintura em verniz - informe a área em m²",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-07-27 13:48:30"
            ], [
                "id" => "272",
                "id_base_servico" => "28",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Informe o tempo de duração da aula",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-08-27 16:21:37"
            ], [
                "id" => "274",
                "id_base_servico" => "28",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe o número de aulas que deseja contratar",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-08-27 16:24:36"
            ], [
                "id" => "275",
                "id_base_servico" => "28",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe o número de alunos ",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2018-08-27 16:27:28"
            ], [
                "id" => "277",
                "id_base_servico" => "32",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Geral",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "1",
                "data_cadastro" => "2019-04-18 13:24:47"
            ], [
                "id" => "278",
                "id_base_servico" => "34",
                "valor" => "0.00",
                "regra" => "2",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Defina a quantidade de manutenções você deseja contratar?",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "1",
                "data_cadastro" => "2019-07-17 11:20:26"
            ], [
                "id" => "279",
                "id_base_servico" => "35",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "1",
                "rotulo_formulario" => "Defina o tipo de material aplicado no alongamento",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2019-07-11 22:57:46"
            ], [
                "id" => "280",
                "id_base_servico" => "35",
                "valor" => "50.00",
                "regra" => "2",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe a quantidade de unhas para a remoção",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2019-07-11 22:44:41"
            ], [
                "id" => "281",
                "id_base_servico" => "35",
                "valor" => "100.00",
                "regra" => "2",
                "ordem_formulario" => "3",
                "rotulo_formulario" => "Informe a quantidade de unhas para reposição",
                "tipo_formulario" => "1",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2019-07-11 22:50:22"
            ], [
                "id" => "282",
                "id_base_servico" => "23",
                "valor" => "0.00",
                "regra" => "1",
                "ordem_formulario" => "2",
                "rotulo_formulario" => "Informe o tipo de decoração de seu alongamento:",
                "tipo_formulario" => "3",
                "incluso_servico_base" => "0",
                "data_cadastro" => "2019-07-17 11:06:57",
            ]
        ]);
    }
}
