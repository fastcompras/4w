<?php

use Illuminate\Database\Seeder;

class CidadeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cidade')->insert(
            [
                "id" => 6934,
                "id_estado" => 23,
                "nome" => "Acegua"
            ], [
                "id" => 6935,
                "id_estado" => 23,
                "nome" => "Afonso Rodrigues"
            ], [
                "id" => 6936,
                "id_estado" => 23,
                "nome" => "Agua Santa"
            ], [
                "id" => 6937,
                "id_estado" => 23,
                "nome" => "Aguas Claras"
            ], [
                "id" => 6938,
                "id_estado" => 23,
                "nome" => "Agudo"
            ], [
                "id" => 6939,
                "id_estado" => 23,
                "nome" => "Ajuricaba"
            ], [
                "id" => 6940,
                "id_estado" => 23,
                "nome" => "Albardao"
            ], [
                "id" => 6941,
                "id_estado" => 23,
                "nome" => "Alecrim"
            ], [
                "id" => 6942,
                "id_estado" => 23,
                "nome" => "Alegrete"
            ], [
                "id" => 6943,
                "id_estado" => 23,
                "nome" => "Alegria"
            ], [
                "id" => 6944,
                "id_estado" => 23,
                "nome" => "Alfredo Brenner"
            ], [
                "id" => 6945,
                "id_estado" => 23,
                "nome" => "Almirante Tamandare"
            ], [
                "id" => 6946,
                "id_estado" => 23,
                "nome" => "Alpestre"
            ], [
                "id" => 6947,
                "id_estado" => 23,
                "nome" => "Alto Alegre"
            ], [
                "id" => 6948,
                "id_estado" => 23,
                "nome" => "Alto da Uniao"
            ], [
                "id" => 6949,
                "id_estado" => 23,
                "nome" => "Alto Feliz"
            ], [
                "id" => 6950,
                "id_estado" => 23,
                "nome" => "Alto Paredao"
            ], [
                "id" => 6951,
                "id_estado" => 23,
                "nome" => "Alto Recreio"
            ], [
                "id" => 6952,
                "id_estado" => 23,
                "nome" => "Alto Uruguai"
            ], [
                "id" => 6953,
                "id_estado" => 23,
                "nome" => "Alvorada"
            ], [
                "id" => 6954,
                "id_estado" => 23,
                "nome" => "Amaral Ferrador"
            ], [
                "id" => 6955,
                "id_estado" => 23,
                "nome" => "Ametista do Sul"
            ], [
                "id" => 6956,
                "id_estado" => 23,
                "nome" => "Andre da Rocha"
            ], [
                "id" => 6957,
                "id_estado" => 23,
                "nome" => "Anta Gorda"
            ], [
                "id" => 6958,
                "id_estado" => 23,
                "nome" => "Antonio Kerpel"
            ], [
                "id" => 6959,
                "id_estado" => 23,
                "nome" => "Antonio Prado"
            ], [
                "id" => 6960,
                "id_estado" => 23,
                "nome" => "Arambare"
            ], [
                "id" => 6961,
                "id_estado" => 23,
                "nome" => "Ararica"
            ], [
                "id" => 6962,
                "id_estado" => 23,
                "nome" => "Aratiba"
            ], [
                "id" => 6963,
                "id_estado" => 23,
                "nome" => "Arco Verde"
            ], [
                "id" => 6964,
                "id_estado" => 23,
                "nome" => "Arco-Iris"
            ], [
                "id" => 6965,
                "id_estado" => 23,
                "nome" => "Arroio Canoas"
            ], [
                "id" => 6966,
                "id_estado" => 23,
                "nome" => "Arroio do Meio"
            ], [
                "id" => 6967,
                "id_estado" => 23,
                "nome" => "Arroio do Padre"
            ], [
                "id" => 6968,
                "id_estado" => 23,
                "nome" => "Arroio do Sal"
            ], [
                "id" => 6969,
                "id_estado" => 23,
                "nome" => "Arroio do So"
            ], [
                "id" => 6970,
                "id_estado" => 23,
                "nome" => "Arroio do Tigre"
            ], [
                "id" => 6971,
                "id_estado" => 23,
                "nome" => "Arroio dos Ratos"
            ], [
                "id" => 6972,
                "id_estado" => 23,
                "nome" => "Arroio Grande"
            ], [
                "id" => 6973,
                "id_estado" => 23,
                "nome" => "Arvore So"
            ], [
                "id" => 6974,
                "id_estado" => 23,
                "nome" => "Arvorezinha"
            ], [
                "id" => 6975,
                "id_estado" => 23,
                "nome" => "Atafona"
            ], [
                "id" => 6976,
                "id_estado" => 23,
                "nome" => "Atiacu"
            ], [
                "id" => 6977,
                "id_estado" => 23,
                "nome" => "Augusto Pestana"
            ], [
                "id" => 6978,
                "id_estado" => 23,
                "nome" => "Aurea"
            ], [
                "id" => 6979,
                "id_estado" => 23,
                "nome" => "Avelino Paranhos"
            ], [
                "id" => 6980,
                "id_estado" => 23,
                "nome" => "Azevedo Sodre"
            ], [
                "id" => 6981,
                "id_estado" => 23,
                "nome" => "Bacupari"
            ], [
                "id" => 6982,
                "id_estado" => 23,
                "nome" => "Bage"
            ], [
                "id" => 6983,
                "id_estado" => 23,
                "nome" => "Baliza"
            ], [
                "id" => 6984,
                "id_estado" => 23,
                "nome" => "Banhado do Colegio"
            ], [
                "id" => 6985,
                "id_estado" => 23,
                "nome" => "Barao"
            ], [
                "id" => 6986,
                "id_estado" => 23,
                "nome" => "Barao de Cotegipe"
            ], [
                "id" => 6987,
                "id_estado" => 23,
                "nome" => "Barao do Triunfo"
            ], [
                "id" => 6988,
                "id_estado" => 23,
                "nome" => "Barra do Guarita"
            ], [
                "id" => 6989,
                "id_estado" => 23,
                "nome" => "Barra do Ouro"
            ], [
                "id" => 6990,
                "id_estado" => 23,
                "nome" => "Barra do Quarai"
            ], [
                "id" => 6991,
                "id_estado" => 23,
                "nome" => "Barra do Ribeiro"
            ], [
                "id" => 6992,
                "id_estado" => 23,
                "nome" => "Barra do Rio Azul"
            ], [
                "id" => 6993,
                "id_estado" => 23,
                "nome" => "Barra Funda"
            ], [
                "id" => 6994,
                "id_estado" => 23,
                "nome" => "Barracao"
            ], [
                "id" => 6995,
                "id_estado" => 23,
                "nome" => "Barreirinho"
            ], [
                "id" => 6996,
                "id_estado" => 23,
                "nome" => "Barreiro"
            ], [
                "id" => 6997,
                "id_estado" => 23,
                "nome" => "Barro Preto"
            ], [
                "id" => 6998,
                "id_estado" => 23,
                "nome" => "Barro Vermelho"
            ], [
                "id" => 6999,
                "id_estado" => 23,
                "nome" => "Barros Cassal"
            ], [
                "id" => 7000,
                "id_estado" => 23,
                "nome" => "Basilio"
            ], [
                "id" => 7001,
                "id_estado" => 23,
                "nome" => "Bela Vista"
            ], [
                "id" => 7002,
                "id_estado" => 23,
                "nome" => "Beluno"
            ], [
                "id" => 7003,
                "id_estado" => 23,
                "nome" => "Benjamin Constant"
            ], [
                "id" => 7004,
                "id_estado" => 23,
                "nome" => "Bento Goncalves"
            ], [
                "id" => 7005,
                "id_estado" => 23,
                "nome" => "Bexiga"
            ], [
                "id" => 7006,
                "id_estado" => 23,
                "nome" => "Boa Esperanca"
            ], [
                "id" => 7007,
                "id_estado" => 23,
                "nome" => "Boa Vista"
            ], [
                "id" => 7008,
                "id_estado" => 23,
                "nome" => "Boa Vista das Missoes"
            ], [
                "id" => 7009,
                "id_estado" => 23,
                "nome" => "Boa Vista do Burica"
            ], [
                "id" => 7010,
                "id_estado" => 23,
                "nome" => "Boa Vista do Incra"
            ], [
                "id" => 7011,
                "id_estado" => 23,
                "nome" => "Boca do Monte"
            ], [
                "id" => 7012,
                "id_estado" => 23,
                "nome" => "Boi Preto"
            ], [
                "id" => 7013,
                "id_estado" => 23,
                "nome" => "Bojuru"
            ], [
                "id" => 7014,
                "id_estado" => 23,
                "nome" => "Bom Jardim"
            ], [
                "id" => 7015,
                "id_estado" => 23,
                "nome" => "Bom Jesus"
            ], [
                "id" => 7016,
                "id_estado" => 23,
                "nome" => "Bom Principio"
            ], [
                "id" => 7017,
                "id_estado" => 23,
                "nome" => "Bom Progresso"
            ], [
                "id" => 7018,
                "id_estado" => 23,
                "nome" => "Bom Retiro"
            ], [
                "id" => 7019,
                "id_estado" => 23,
                "nome" => "Bom Retiro do Guaiba"
            ], [
                "id" => 7020,
                "id_estado" => 23,
                "nome" => "Bom Retiro do Sul"
            ], [
                "id" => 7021,
                "id_estado" => 23,
                "nome" => "Bonito"
            ], [
                "id" => 7022,
                "id_estado" => 23,
                "nome" => "Boqueirao"
            ], [
                "id" => 7023,
                "id_estado" => 23,
                "nome" => "Boqueirao do Leao"
            ], [
                "id" => 7024,
                "id_estado" => 23,
                "nome" => "Borore"
            ], [
                "id" => 7025,
                "id_estado" => 23,
                "nome" => "Bossoroca"
            ], [
                "id" => 7026,
                "id_estado" => 23,
                "nome" => "Botucarai"
            ], [
                "id" => 7027,
                "id_estado" => 23,
                "nome" => "Braga"
            ], [
                "id" => 7028,
                "id_estado" => 23,
                "nome" => "Brochier"
            ], [
                "id" => 7029,
                "id_estado" => 23,
                "nome" => "Buriti"
            ], [
                "id" => 7030,
                "id_estado" => 23,
                "nome" => "Butia"
            ], [
                "id" => 7031,
                "id_estado" => 23,
                "nome" => "Butias"
            ], [
                "id" => 7032,
                "id_estado" => 23,
                "nome" => "Cacapava do Sul"
            ], [
                "id" => 7033,
                "id_estado" => 23,
                "nome" => "Cacequi"
            ], [
                "id" => 7034,
                "id_estado" => 23,
                "nome" => "Cachoeira do Sul"
            ], [
                "id" => 7035,
                "id_estado" => 23,
                "nome" => "Cachoeirinha"
            ], [
                "id" => 7036,
                "id_estado" => 23,
                "nome" => "Cacique Doble"
            ], [
                "id" => 7037,
                "id_estado" => 23,
                "nome" => "Cadeado"
            ], [
                "id" => 7038,
                "id_estado" => 23,
                "nome" => "Cadorna"
            ], [
                "id" => 7039,
                "id_estado" => 23,
                "nome" => "Caibate"
            ], [
                "id" => 7040,
                "id_estado" => 23,
                "nome" => "Caicara"
            ], [
                "id" => 7041,
                "id_estado" => 23,
                "nome" => "Camaqua"
            ], [
                "id" => 7042,
                "id_estado" => 23,
                "nome" => "Camargo"
            ], [
                "id" => 7043,
                "id_estado" => 23,
                "nome" => "Cambara do Sul"
            ], [
                "id" => 7044,
                "id_estado" => 23,
                "nome" => "Camobi"
            ], [
                "id" => 7045,
                "id_estado" => 23,
                "nome" => "Campestre Baixo"
            ], [
                "id" => 7046,
                "id_estado" => 23,
                "nome" => "Campestre da Serra"
            ], [
                "id" => 7047,
                "id_estado" => 23,
                "nome" => "Campina das Missoes"
            ], [
                "id" => 7048,
                "id_estado" => 23,
                "nome" => "Campina Redonda"
            ], [
                "id" => 7049,
                "id_estado" => 23,
                "nome" => "Campinas"
            ], [
                "id" => 7050,
                "id_estado" => 23,
                "nome" => "Campinas do Sul"
            ], [
                "id" => 7051,
                "id_estado" => 23,
                "nome" => "Campo Bom"
            ], [
                "id" => 7052,
                "id_estado" => 23,
                "nome" => "Campo Branco"
            ], [
                "id" => 7053,
                "id_estado" => 23,
                "nome" => "Campo do Meio"
            ], [
                "id" => 7054,
                "id_estado" => 23,
                "nome" => "Campo Novo"
            ], [
                "id" => 7055,
                "id_estado" => 23,
                "nome" => "Campo Santo"
            ], [
                "id" => 7056,
                "id_estado" => 23,
                "nome" => "Campo Seco"
            ], [
                "id" => 7057,
                "id_estado" => 23,
                "nome" => "Campo Vicente"
            ], [
                "id" => 7058,
                "id_estado" => 23,
                "nome" => "Campos Borges"
            ], [
                "id" => 7059,
                "id_estado" => 23,
                "nome" => "Candelaria"
            ], [
                "id" => 7060,
                "id_estado" => 23,
                "nome" => "Candido Freire"
            ], [
                "id" => 7061,
                "id_estado" => 23,
                "nome" => "Candido Godoi"
            ], [
                "id" => 7062,
                "id_estado" => 23,
                "nome" => "Candiota"
            ], [
                "id" => 7063,
                "id_estado" => 23,
                "nome" => "Canela"
            ], [
                "id" => 7064,
                "id_estado" => 23,
                "nome" => "Cangucu"
            ], [
                "id" => 7065,
                "id_estado" => 23,
                "nome" => "Canhembora"
            ], [
                "id" => 7066,
                "id_estado" => 23,
                "nome" => "Canoas"
            ], [
                "id" => 7067,
                "id_estado" => 23,
                "nome" => "Canudos"
            ], [
                "id" => 7068,
                "id_estado" => 23,
                "nome" => "Capane"
            ], [
                "id" => 7069,
                "id_estado" => 23,
                "nome" => "Capao Bonito"
            ], [
                "id" => 7070,
                "id_estado" => 23,
                "nome" => "Capao Comprido"
            ], [
                "id" => 7071,
                "id_estado" => 23,
                "nome" => "Capao da Canoa"
            ], [
                "id" => 7072,
                "id_estado" => 23,
                "nome" => "Capao da Porteira"
            ], [
                "id" => 7073,
                "id_estado" => 23,
                "nome" => "Capao do Cedro"
            ], [
                "id" => 7074,
                "id_estado" => 23,
                "nome" => "Capao do Cipo"
            ], [
                "id" => 7075,
                "id_estado" => 23,
                "nome" => "Capao do Leao"
            ], [
                "id" => 7076,
                "id_estado" => 23,
                "nome" => "Capela de Santana"
            ], [
                "id" => 7077,
                "id_estado" => 23,
                "nome" => "Capela Velha"
            ], [
                "id" => 7078,
                "id_estado" => 23,
                "nome" => "Capinzal"
            ], [
                "id" => 7079,
                "id_estado" => 23,
                "nome" => "Capitao"
            ], [
                "id" => 7080,
                "id_estado" => 23,
                "nome" => "Capivari"
            ], [
                "id" => 7081,
                "id_estado" => 23,
                "nome" => "Capivarita"
            ], [
                "id" => 7082,
                "id_estado" => 23,
                "nome" => "Capo-Ere"
            ], [
                "id" => 7083,
                "id_estado" => 23,
                "nome" => "Capoeira Grande"
            ], [
                "id" => 7084,
                "id_estado" => 23,
                "nome" => "Caraa"
            ], [
                "id" => 7085,
                "id_estado" => 23,
                "nome" => "Caraja Seival"
            ], [
                "id" => 7086,
                "id_estado" => 23,
                "nome" => "Carazinho"
            ], [
                "id" => 7087,
                "id_estado" => 23,
                "nome" => "Carlos Barbosa"
            ], [
                "id" => 7088,
                "id_estado" => 23,
                "nome" => "Carlos Gomes"
            ], [
                "id" => 7089,
                "id_estado" => 23,
                "nome" => "Carovi"
            ], [
                "id" => 7090,
                "id_estado" => 23,
                "nome" => "Casca"
            ], [
                "id" => 7091,
                "id_estado" => 23,
                "nome" => "Cascata"
            ], [
                "id" => 7092,
                "id_estado" => 23,
                "nome" => "Caseiros"
            ], [
                "id" => 7093,
                "id_estado" => 23,
                "nome" => "Castelinho"
            ], [
                "id" => 7094,
                "id_estado" => 23,
                "nome" => "Catuipe"
            ], [
                "id" => 7095,
                "id_estado" => 23,
                "nome" => "Cavajureta"
            ], [
                "id" => 7096,
                "id_estado" => 23,
                "nome" => "Cavera"
            ], [
                "id" => 7097,
                "id_estado" => 23,
                "nome" => "Caxias do Sul"
            ], [
                "id" => 7098,
                "id_estado" => 23,
                "nome" => "Cazuza Ferreira"
            ], [
                "id" => 7099,
                "id_estado" => 23,
                "nome" => "Cedro Marcado"
            ], [
                "id" => 7100,
                "id_estado" => 23,
                "nome" => "Centenario"
            ], [
                "id" => 7101,
                "id_estado" => 23,
                "nome" => "Centro Linha Brasil"
            ], [
                "id" => 7102,
                "id_estado" => 23,
                "nome" => "Cerrito Alegre"
            ], [
                "id" => 7103,
                "id_estado" => 23,
                "nome" => "Cerrito do Ouro Ou Vila do Cer"
            ], [
                "id" => 7104,
                "id_estado" => 23,
                "nome" => "Cerro Alto"
            ], [
                "id" => 7105,
                "id_estado" => 23,
                "nome" => "Cerro Branco"
            ], [
                "id" => 7106,
                "id_estado" => 23,
                "nome" => "Cerro Claro"
            ], [
                "id" => 7107,
                "id_estado" => 23,
                "nome" => "Cerro do Martins"
            ], [
                "id" => 7108,
                "id_estado" => 23,
                "nome" => "Cerro do Roque"
            ], [
                "id" => 7109,
                "id_estado" => 23,
                "nome" => "Cerro Grande"
            ], [
                "id" => 7110,
                "id_estado" => 23,
                "nome" => "Cerro Grande do Sul"
            ], [
                "id" => 7111,
                "id_estado" => 23,
                "nome" => "Cerro Largo"
            ], [
                "id" => 7112,
                "id_estado" => 23,
                "nome" => "Chapada"
            ], [
                "id" => 7113,
                "id_estado" => 23,
                "nome" => "Charqueadas"
            ], [
                "id" => 7114,
                "id_estado" => 23,
                "nome" => "Charrua"
            ], [
                "id" => 7115,
                "id_estado" => 23,
                "nome" => "Chiapetta"
            ], [
                "id" => 7116,
                "id_estado" => 23,
                "nome" => "Chicoloma"
            ], [
                "id" => 7117,
                "id_estado" => 23,
                "nome" => "Chimarrao"
            ], [
                "id" => 7118,
                "id_estado" => 23,
                "nome" => "Chorao"
            ], [
                "id" => 7119,
                "id_estado" => 23,
                "nome" => "Chui"
            ], [
                "id" => 7120,
                "id_estado" => 23,
                "nome" => "Cidreira"
            ], [
                "id" => 7121,
                "id_estado" => 23,
                "nome" => "Cinquentenario"
            ], [
                "id" => 7122,
                "id_estado" => 23,
                "nome" => "Ciriaco"
            ], [
                "id" => 7123,
                "id_estado" => 23,
                "nome" => "Clara"
            ], [
                "id" => 7124,
                "id_estado" => 23,
                "nome" => "Clemente Argolo"
            ], [
                "id" => 7125,
                "id_estado" => 23,
                "nome" => "Coimbra"
            ], [
                "id" => 7126,
                "id_estado" => 23,
                "nome" => "Colinas"
            ], [
                "id" => 7127,
                "id_estado" => 23,
                "nome" => "Colonia das Almas"
            ], [
                "id" => 7128,
                "id_estado" => 23,
                "nome" => "Colonia Langaro"
            ], [
                "id" => 7129,
                "id_estado" => 23,
                "nome" => "Colonia Medeiros"
            ], [
                "id" => 7130,
                "id_estado" => 23,
                "nome" => "Colonia Municipal"
            ], [
                "id" => 7131,
                "id_estado" => 23,
                "nome" => "Colonia Nova"
            ], [
                "id" => 7132,
                "id_estado" => 23,
                "nome" => "Colonia Sao Joao"
            ], [
                "id" => 7133,
                "id_estado" => 23,
                "nome" => "Colonia Z-3"
            ], [
                "id" => 7134,
                "id_estado" => 23,
                "nome" => "Coloninha"
            ], [
                "id" => 7135,
                "id_estado" => 23,
                "nome" => "Colorado"
            ], [
                "id" => 7136,
                "id_estado" => 23,
                "nome" => "Comandai"
            ], [
                "id" => 7137,
                "id_estado" => 23,
                "nome" => "Condor"
            ], [
                "id" => 7138,
                "id_estado" => 23,
                "nome" => "Consolata"
            ], [
                "id" => 7139,
                "id_estado" => 23,
                "nome" => "Constantina"
            ], [
                "id" => 7140,
                "id_estado" => 23,
                "nome" => "Coqueiro Baixo"
            ], [
                "id" => 7141,
                "id_estado" => 23,
                "nome" => "Coqueiros do Sul"
            ], [
                "id" => 7142,
                "id_estado" => 23,
                "nome" => "Cordilheira"
            ], [
                "id" => 7143,
                "id_estado" => 23,
                "nome" => "Coroados"
            ], [
                "id" => 7144,
                "id_estado" => 23,
                "nome" => "Coronel Barros"
            ], [
                "id" => 7145,
                "id_estado" => 23,
                "nome" => "Coronel Bicaco"
            ], [
                "id" => 7146,
                "id_estado" => 23,
                "nome" => "Coronel Finzito"
            ], [
                "id" => 7147,
                "id_estado" => 23,
                "nome" => "Coronel Pilar"
            ], [
                "id" => 7148,
                "id_estado" => 23,
                "nome" => "Coronel Teixeira"
            ], [
                "id" => 7149,
                "id_estado" => 23,
                "nome" => "Cortado"
            ], [
                "id" => 7150,
                "id_estado" => 23,
                "nome" => "Costa da Cadeia"
            ], [
                "id" => 7151,
                "id_estado" => 23,
                "nome" => "Costao"
            ], [
                "id" => 7152,
                "id_estado" => 23,
                "nome" => "Cotipora"
            ], [
                "id" => 7153,
                "id_estado" => 23,
                "nome" => "Coxilha"
            ], [
                "id" => 7154,
                "id_estado" => 23,
                "nome" => "Coxilha Grande"
            ], [
                "id" => 7155,
                "id_estado" => 23,
                "nome" => "Cr-1"
            ], [
                "id" => 7156,
                "id_estado" => 23,
                "nome" => "Crissiumal"
            ], [
                "id" => 7157,
                "id_estado" => 23,
                "nome" => "Cristal"
            ], [
                "id" => 7158,
                "id_estado" => 23,
                "nome" => "Criuva"
            ], [
                "id" => 7159,
                "id_estado" => 23,
                "nome" => "Cruz Alta"
            ], [
                "id" => 7160,
                "id_estado" => 23,
                "nome" => "Cruz Altense"
            ], [
                "id" => 7161,
                "id_estado" => 23,
                "nome" => "Cruzeiro"
            ], [
                "id" => 7162,
                "id_estado" => 23,
                "nome" => "Cruzeiro do Sul"
            ], [
                "id" => 7163,
                "id_estado" => 23,
                "nome" => "Curral Alto"
            ], [
                "id" => 7164,
                "id_estado" => 23,
                "nome" => "Curumim"
            ], [
                "id" => 7165,
                "id_estado" => 23,
                "nome" => "Daltro Filho"
            ], [
                "id" => 7166,
                "id_estado" => 23,
                "nome" => "Dario Lassance"
            ], [
                "id" => 7167,
                "id_estado" => 23,
                "nome" => "David Canabarro"
            ], [
                "id" => 7168,
                "id_estado" => 23,
                "nome" => "Delfina"
            ], [
                "id" => 7169,
                "id_estado" => 23,
                "nome" => "Deodoro"
            ], [
                "id" => 7170,
                "id_estado" => 23,
                "nome" => "Deposito"
            ], [
                "id" => 7171,
                "id_estado" => 23,
                "nome" => "Derrubadas"
            ], [
                "id" => 7172,
                "id_estado" => 23,
                "nome" => "Dezesseis de Novembro"
            ], [
                "id" => 7173,
                "id_estado" => 23,
                "nome" => "Dilermando de Aguiar"
            ], [
                "id" => 7174,
                "id_estado" => 23,
                "nome" => "Divino"
            ], [
                "id" => 7175,
                "id_estado" => 23,
                "nome" => "Dois Irmaos"
            ], [
                "id" => 7176,
                "id_estado" => 23,
                "nome" => "Dois Irmaos das Missoes"
            ], [
                "id" => 7177,
                "id_estado" => 23,
                "nome" => "Dois Lajeados"
            ], [
                "id" => 7178,
                "id_estado" => 23,
                "nome" => "Dom Diogo"
            ], [
                "id" => 7179,
                "id_estado" => 23,
                "nome" => "Dom Feliciano"
            ], [
                "id" => 7180,
                "id_estado" => 23,
                "nome" => "Dom Pedrito"
            ], [
                "id" => 7181,
                "id_estado" => 23,
                "nome" => "Dona Francisca"
            ], [
                "id" => 7182,
                "id_estado" => 23,
                "nome" => "Dona Otilia"
            ], [
                "id" => 7183,
                "id_estado" => 23,
                "nome" => "Dourado"
            ], [
                "id" => 7184,
                "id_estado" => 23,
                "nome" => "Doutor Bozano"
            ], [
                "id" => 7185,
                "id_estado" => 23,
                "nome" => "Doutor Edgardo Pereira Velho"
            ], [
                "id" => 7186,
                "id_estado" => 23,
                "nome" => "Doutor Mauricio Cardoso"
            ], [
                "id" => 7187,
                "id_estado" => 23,
                "nome" => "Eldorado do Sul"
            ], [
                "id" => 7188,
                "id_estado" => 23,
                "nome" => "Eletra"
            ], [
                "id" => 7189,
                "id_estado" => 23,
                "nome" => "Encantado"
            ], [
                "id" => 7190,
                "id_estado" => 23,
                "nome" => "Encruzilhada"
            ], [
                "id" => 7191,
                "id_estado" => 23,
                "nome" => "Encruzilhada do Sul"
            ], [
                "id" => 7192,
                "id_estado" => 23,
                "nome" => "Engenho Velho"
            ], [
                "id" => 7193,
                "id_estado" => 23,
                "nome" => "Entre Ijuis"
            ], [
                "id" => 7194,
                "id_estado" => 23,
                "nome" => "Entre Rios do Sul"
            ], [
                "id" => 7195,
                "id_estado" => 23,
                "nome" => "Entrepelado"
            ], [
                "id" => 7196,
                "id_estado" => 23,
                "nome" => "Erebango"
            ], [
                "id" => 7197,
                "id_estado" => 23,
                "nome" => "Erechim"
            ], [
                "id" => 7198,
                "id_estado" => 23,
                "nome" => "Ernestina"
            ], [
                "id" => 7199,
                "id_estado" => 23,
                "nome" => "Ernesto Alves"
            ], [
                "id" => 7200,
                "id_estado" => 23,
                "nome" => "Erval Grande"
            ], [
                "id" => 7201,
                "id_estado" => 23,
                "nome" => "Erval Seco"
            ], [
                "id" => 7202,
                "id_estado" => 23,
                "nome" => "Erveiras"
            ], [
                "id" => 7203,
                "id_estado" => 23,
                "nome" => "Esmeralda"
            ], [
                "id" => 7204,
                "id_estado" => 23,
                "nome" => "Esperanca"
            ], [
                "id" => 7205,
                "id_estado" => 23,
                "nome" => "Espigao"
            ], [
                "id" => 7206,
                "id_estado" => 23,
                "nome" => "Espigao Alto"
            ], [
                "id" => 7207,
                "id_estado" => 23,
                "nome" => "Espinilho Grande"
            ], [
                "id" => 7208,
                "id_estado" => 23,
                "nome" => "Espirito Santo"
            ], [
                "id" => 7209,
                "id_estado" => 23,
                "nome" => "Espumoso"
            ], [
                "id" => 7210,
                "id_estado" => 23,
                "nome" => "Esquina Araujo"
            ], [
                "id" => 7211,
                "id_estado" => 23,
                "nome" => "Esquina Bom Sucesso"
            ], [
                "id" => 7212,
                "id_estado" => 23,
                "nome" => "Esquina Gaucha"
            ], [
                "id" => 7213,
                "id_estado" => 23,
                "nome" => "Esquina Ipiranga"
            ], [
                "id" => 7214,
                "id_estado" => 23,
                "nome" => "Esquina Piratini"
            ], [
                "id" => 7215,
                "id_estado" => 23,
                "nome" => "Estacao"
            ], [
                "id" => 7216,
                "id_estado" => 23,
                "nome" => "Estancia Grande"
            ], [
                "id" => 7217,
                "id_estado" => 23,
                "nome" => "Estancia Velha"
            ], [
                "id" => 7218,
                "id_estado" => 23,
                "nome" => "Esteio"
            ], [
                "id" => 7219,
                "id_estado" => 23,
                "nome" => "Esteira"
            ], [
                "id" => 7220,
                "id_estado" => 23,
                "nome" => "Estreito"
            ], [
                "id" => 7221,
                "id_estado" => 23,
                "nome" => "Estrela"
            ], [
                "id" => 7222,
                "id_estado" => 23,
                "nome" => "Estrela Velha"
            ], [
                "id" => 7223,
                "id_estado" => 23,
                "nome" => "Eugenio de Castro"
            ], [
                "id" => 7224,
                "id_estado" => 23,
                "nome" => "Evangelista"
            ], [
                "id" => 7225,
                "id_estado" => 23,
                "nome" => "Fagundes Varela"
            ], [
                "id" => 7226,
                "id_estado" => 23,
                "nome" => "Fao"
            ], [
                "id" => 7227,
                "id_estado" => 23,
                "nome" => "Faria Lemos"
            ], [
                "id" => 7228,
                "id_estado" => 23,
                "nome" => "Farinhas"
            ], [
                "id" => 7229,
                "id_estado" => 23,
                "nome" => "Farrapos"
            ], [
                "id" => 7230,
                "id_estado" => 23,
                "nome" => "Farroupilha"
            ], [
                "id" => 7231,
                "id_estado" => 23,
                "nome" => "Faxinal"
            ], [
                "id" => 7232,
                "id_estado" => 23,
                "nome" => "Faxinal do Soturno"
            ], [
                "id" => 7233,
                "id_estado" => 23,
                "nome" => "Faxinalzinho"
            ], [
                "id" => 7234,
                "id_estado" => 23,
                "nome" => "Fazenda Fialho"
            ], [
                "id" => 7235,
                "id_estado" => 23,
                "nome" => "Fazenda Souza"
            ], [
                "id" => 7236,
                "id_estado" => 23,
                "nome" => "Fazenda Vilanova"
            ], [
                "id" => 7237,
                "id_estado" => 23,
                "nome" => "Feliz"
            ], [
                "id" => 7238,
                "id_estado" => 23,
                "nome" => "Ferreira"
            ], [
                "id" => 7239,
                "id_estado" => 23,
                "nome" => "Flores da Cunha"
            ], [
                "id" => 7240,
                "id_estado" => 23,
                "nome" => "Floresta"
            ], [
                "id" => 7241,
                "id_estado" => 23,
                "nome" => "Floriano Peixoto"
            ], [
                "id" => 7242,
                "id_estado" => 23,
                "nome" => "Florida"
            ], [
                "id" => 7243,
                "id_estado" => 23,
                "nome" => "Fontoura Xavier"
            ], [
                "id" => 7244,
                "id_estado" => 23,
                "nome" => "Formigueiro"
            ], [
                "id" => 7245,
                "id_estado" => 23,
                "nome" => "Formosa"
            ], [
                "id" => 7246,
                "id_estado" => 23,
                "nome" => "Forninho"
            ], [
                "id" => 7247,
                "id_estado" => 23,
                "nome" => "Forquetinha"
            ], [
                "id" => 7248,
                "id_estado" => 23,
                "nome" => "Fortaleza dos Valos"
            ], [
                "id" => 7249,
                "id_estado" => 23,
                "nome" => "Frederico Westphalen"
            ], [
                "id" => 7250,
                "id_estado" => 23,
                "nome" => "Frei Sebastiao"
            ], [
                "id" => 7251,
                "id_estado" => 23,
                "nome" => "Freire"
            ], [
                "id" => 7252,
                "id_estado" => 23,
                "nome" => "Garibaldi"
            ], [
                "id" => 7253,
                "id_estado" => 23,
                "nome" => "Garibaldina"
            ], [
                "id" => 7254,
                "id_estado" => 23,
                "nome" => "Garruchos"
            ], [
                "id" => 7255,
                "id_estado" => 23,
                "nome" => "Gaurama"
            ], [
                "id" => 7256,
                "id_estado" => 23,
                "nome" => "General Camara"
            ], [
                "id" => 7257,
                "id_estado" => 23,
                "nome" => "Gentil"
            ], [
                "id" => 7258,
                "id_estado" => 23,
                "nome" => "Getulio Vargas"
            ], [
                "id" => 7259,
                "id_estado" => 23,
                "nome" => "Girua"
            ], [
                "id" => 7260,
                "id_estado" => 23,
                "nome" => "Gloria"
            ], [
                "id" => 7261,
                "id_estado" => 23,
                "nome" => "Glorinha"
            ], [
                "id" => 7262,
                "id_estado" => 23,
                "nome" => "Goio-En"
            ], [
                "id" => 7263,
                "id_estado" => 23,
                "nome" => "Gramado"
            ], [
                "id" => 7264,
                "id_estado" => 23,
                "nome" => "Gramado dos Loureiros"
            ], [
                "id" => 7265,
                "id_estado" => 23,
                "nome" => "Gramado Sao Pedro"
            ], [
                "id" => 7266,
                "id_estado" => 23,
                "nome" => "Gramado Xavier"
            ], [
                "id" => 7267,
                "id_estado" => 23,
                "nome" => "Gravatai"
            ], [
                "id" => 7268,
                "id_estado" => 23,
                "nome" => "Guabiju"
            ], [
                "id" => 7269,
                "id_estado" => 23,
                "nome" => "Guaiba"
            ], [
                "id" => 7270,
                "id_estado" => 23,
                "nome" => "Guajuviras"
            ], [
                "id" => 7271,
                "id_estado" => 23,
                "nome" => "Guapore"
            ], [
                "id" => 7272,
                "id_estado" => 23,
                "nome" => "Guarani das Missoes"
            ], [
                "id" => 7273,
                "id_estado" => 23,
                "nome" => "Guassupi"
            ], [
                "id" => 7274,
                "id_estado" => 23,
                "nome" => "Harmonia"
            ], [
                "id" => 7275,
                "id_estado" => 23,
                "nome" => "Herval"
            ], [
                "id" => 7276,
                "id_estado" => 23,
                "nome" => "Hidraulica"
            ], [
                "id" => 7277,
                "id_estado" => 23,
                "nome" => "Horizontina"
            ], [
                "id" => 7278,
                "id_estado" => 23,
                "nome" => "Hulha Negra"
            ], [
                "id" => 7279,
                "id_estado" => 23,
                "nome" => "Humaita"
            ], [
                "id" => 7280,
                "id_estado" => 23,
                "nome" => "Ibarama"
            ], [
                "id" => 7281,
                "id_estado" => 23,
                "nome" => "Ibare"
            ], [
                "id" => 7282,
                "id_estado" => 23,
                "nome" => "Ibiaca"
            ], [
                "id" => 7283,
                "id_estado" => 23,
                "nome" => "Ibiraiaras"
            ], [
                "id" => 7284,
                "id_estado" => 23,
                "nome" => "Ibirapuita"
            ], [
                "id" => 7285,
                "id_estado" => 23,
                "nome" => "Ibiruba"
            ], [
                "id" => 7286,
                "id_estado" => 23,
                "nome" => "Igrejinha"
            ], [
                "id" => 7287,
                "id_estado" => 23,
                "nome" => "Ijucapirama"
            ], [
                "id" => 7288,
                "id_estado" => 23,
                "nome" => "Ijui"
            ], [
                "id" => 7289,
                "id_estado" => 23,
                "nome" => "Ilha dos Marinheiros"
            ], [
                "id" => 7290,
                "id_estado" => 23,
                "nome" => "Ilopolis"
            ], [
                "id" => 7291,
                "id_estado" => 23,
                "nome" => "Imbe"
            ], [
                "id" => 7292,
                "id_estado" => 23,
                "nome" => "Imigrante"
            ], [
                "id" => 7293,
                "id_estado" => 23,
                "nome" => "Independencia"
            ], [
                "id" => 7294,
                "id_estado" => 23,
                "nome" => "Inhacora"
            ], [
                "id" => 7295,
                "id_estado" => 23,
                "nome" => "Ipe"
            ], [
                "id" => 7296,
                "id_estado" => 23,
                "nome" => "Ipiranga"
            ], [
                "id" => 7297,
                "id_estado" => 23,
                "nome" => "Ipiranga do Sul"
            ], [
                "id" => 7298,
                "id_estado" => 23,
                "nome" => "Ipuacu"
            ], [
                "id" => 7299,
                "id_estado" => 23,
                "nome" => "Irai"
            ], [
                "id" => 7300,
                "id_estado" => 23,
                "nome" => "Irui"
            ], [
                "id" => 7301,
                "id_estado" => 23,
                "nome" => "Itaara"
            ], [
                "id" => 7302,
                "id_estado" => 23,
                "nome" => "Itacolomi"
            ], [
                "id" => 7303,
                "id_estado" => 23,
                "nome" => "Itacurubi"
            ], [
                "id" => 7304,
                "id_estado" => 23,
                "nome" => "Itai"
            ], [
                "id" => 7305,
                "id_estado" => 23,
                "nome" => "Itaimbezinho"
            ], [
                "id" => 7306,
                "id_estado" => 23,
                "nome" => "Itao"
            ], [
                "id" => 7307,
                "id_estado" => 23,
                "nome" => "Itapua"
            ], [
                "id" => 7308,
                "id_estado" => 23,
                "nome" => "Itapuca"
            ], [
                "id" => 7309,
                "id_estado" => 23,
                "nome" => "Itaqui"
            ], [
                "id" => 7310,
                "id_estado" => 23,
                "nome" => "Itati"
            ], [
                "id" => 7311,
                "id_estado" => 23,
                "nome" => "Itatiba do Sul"
            ], [
                "id" => 7312,
                "id_estado" => 23,
                "nome" => "Itauba"
            ], [
                "id" => 7313,
                "id_estado" => 23,
                "nome" => "Ituim"
            ], [
                "id" => 7314,
                "id_estado" => 23,
                "nome" => "Ivai"
            ], [
                "id" => 7315,
                "id_estado" => 23,
                "nome" => "Ivora"
            ], [
                "id" => 7316,
                "id_estado" => 23,
                "nome" => "Ivoti"
            ], [
                "id" => 7317,
                "id_estado" => 23,
                "nome" => "Jaboticaba"
            ], [
                "id" => 7318,
                "id_estado" => 23,
                "nome" => "Jacuizinho"
            ], [
                "id" => 7319,
                "id_estado" => 23,
                "nome" => "Jacutinga"
            ], [
                "id" => 7320,
                "id_estado" => 23,
                "nome" => "Jaguarao"
            ], [
                "id" => 7321,
                "id_estado" => 23,
                "nome" => "Jaguarete"
            ], [
                "id" => 7322,
                "id_estado" => 23,
                "nome" => "Jaguari"
            ], [
                "id" => 7323,
                "id_estado" => 23,
                "nome" => "Jansen"
            ], [
                "id" => 7324,
                "id_estado" => 23,
                "nome" => "Jaquirana"
            ], [
                "id" => 7325,
                "id_estado" => 23,
                "nome" => "Jari"
            ], [
                "id" => 7326,
                "id_estado" => 23,
                "nome" => "Jazidas Ou Capela Sao Vicente"
            ], [
                "id" => 7327,
                "id_estado" => 23,
                "nome" => "Joao Arregui"
            ], [
                "id" => 7328,
                "id_estado" => 23,
                "nome" => "Joao Rodrigues"
            ], [
                "id" => 7329,
                "id_estado" => 23,
                "nome" => "Joca Tavares"
            ], [
                "id" => 7330,
                "id_estado" => 23,
                "nome" => "Joia"
            ], [
                "id" => 7331,
                "id_estado" => 23,
                "nome" => "Jose Otavio"
            ], [
                "id" => 7332,
                "id_estado" => 23,
                "nome" => "Jua"
            ], [
                "id" => 7333,
                "id_estado" => 23,
                "nome" => "Julio de Castilhos"
            ], [
                "id" => 7334,
                "id_estado" => 23,
                "nome" => "Lagoa Bonita"
            ], [
                "id" => 7335,
                "id_estado" => 23,
                "nome" => "Lagoa dos Tres Cantos"
            ], [
                "id" => 7336,
                "id_estado" => 23,
                "nome" => "Lagoa Vermelha"
            ], [
                "id" => 7337,
                "id_estado" => 23,
                "nome" => "Lagoao"
            ], [
                "id" => 7338,
                "id_estado" => 23,
                "nome" => "Lajeado"
            ], [
                "id" => 7339,
                "id_estado" => 23,
                "nome" => "Lajeado Bonito"
            ], [
                "id" => 7340,
                "id_estado" => 23,
                "nome" => "Lajeado Cerne"
            ], [
                "id" => 7341,
                "id_estado" => 23,
                "nome" => "Lajeado do Bugre"
            ], [
                "id" => 7342,
                "id_estado" => 23,
                "nome" => "Lajeado Grande"
            ], [
                "id" => 7343,
                "id_estado" => 23,
                "nome" => "Lara"
            ], [
                "id" => 7344,
                "id_estado" => 23,
                "nome" => "Laranjeira"
            ], [
                "id" => 7345,
                "id_estado" => 23,
                "nome" => "Lava-Pes"
            ], [
                "id" => 7346,
                "id_estado" => 23,
                "nome" => "Lavras do Sul"
            ], [
                "id" => 7347,
                "id_estado" => 23,
                "nome" => "Leonel Rocha"
            ], [
                "id" => 7348,
                "id_estado" => 23,
                "nome" => "Liberato Salzano"
            ], [
                "id" => 7349,
                "id_estado" => 23,
                "nome" => "Lindolfo Collor"
            ], [
                "id" => 7350,
                "id_estado" => 23,
                "nome" => "Linha Comprida"
            ], [
                "id" => 7351,
                "id_estado" => 23,
                "nome" => "Linha Nova"
            ], [
                "id" => 7352,
                "id_estado" => 23,
                "nome" => "Linha Vitoria"
            ], [
                "id" => 7353,
                "id_estado" => 23,
                "nome" => "Loreto"
            ], [
                "id" => 7354,
                "id_estado" => 23,
                "nome" => "Machadinho"
            ], [
                "id" => 7355,
                "id_estado" => 23,
                "nome" => "Magisterio"
            ], [
                "id" => 7356,
                "id_estado" => 23,
                "nome" => "Manchinha"
            ], [
                "id" => 7357,
                "id_estado" => 23,
                "nome" => "Mangueiras"
            ], [
                "id" => 7358,
                "id_estado" => 23,
                "nome" => "Manoel Viana"
            ], [
                "id" => 7359,
                "id_estado" => 23,
                "nome" => "Maquine"
            ], [
                "id" => 7360,
                "id_estado" => 23,
                "nome" => "Marata"
            ], [
                "id" => 7361,
                "id_estado" => 23,
                "nome" => "Marau"
            ], [
                "id" => 7362,
                "id_estado" => 23,
                "nome" => "Marcelino Ramos"
            ], [
                "id" => 7363,
                "id_estado" => 23,
                "nome" => "Marcorama"
            ], [
                "id" => 7364,
                "id_estado" => 23,
                "nome" => "Mariana Pimentel"
            ], [
                "id" => 7365,
                "id_estado" => 23,
                "nome" => "Mariano Moro"
            ], [
                "id" => 7366,
                "id_estado" => 23,
                "nome" => "Mariante"
            ], [
                "id" => 7367,
                "id_estado" => 23,
                "nome" => "Mariapolis"
            ], [
                "id" => 7368,
                "id_estado" => 23,
                "nome" => "Marques de Souza"
            ], [
                "id" => 7369,
                "id_estado" => 23,
                "nome" => "Massambara"
            ], [
                "id" => 7370,
                "id_estado" => 23,
                "nome" => "Mata"
            ], [
                "id" => 7371,
                "id_estado" => 23,
                "nome" => "Matarazzo"
            ], [
                "id" => 7372,
                "id_estado" => 23,
                "nome" => "Mato Castelhano"
            ], [
                "id" => 7373,
                "id_estado" => 23,
                "nome" => "Mato Grande"
            ], [
                "id" => 7374,
                "id_estado" => 23,
                "nome" => "Mato Leitao"
            ], [
                "id" => 7375,
                "id_estado" => 23,
                "nome" => "Mato Queimado"
            ], [
                "id" => 7376,
                "id_estado" => 23,
                "nome" => "Maua"
            ], [
                "id" => 7377,
                "id_estado" => 23,
                "nome" => "Maximiliano de Almeida"
            ], [
                "id" => 7378,
                "id_estado" => 23,
                "nome" => "Medianeira"
            ], [
                "id" => 7379,
                "id_estado" => 23,
                "nome" => "Melos"
            ], [
                "id" => 7380,
                "id_estado" => 23,
                "nome" => "Minas do Leao"
            ], [
                "id" => 7381,
                "id_estado" => 23,
                "nome" => "Miraguai"
            ], [
                "id" => 7382,
                "id_estado" => 23,
                "nome" => "Miraguaia"
            ], [
                "id" => 7383,
                "id_estado" => 23,
                "nome" => "Mirim"
            ], [
                "id" => 7384,
                "id_estado" => 23,
                "nome" => "Montauri"
            ], [
                "id" => 7385,
                "id_estado" => 23,
                "nome" => "Monte Alegre"
            ], [
                "id" => 7386,
                "id_estado" => 23,
                "nome" => "Monte Alverne"
            ], [
                "id" => 7387,
                "id_estado" => 23,
                "nome" => "Monte Belo do Sul"
            ], [
                "id" => 7388,
                "id_estado" => 23,
                "nome" => "Monte Bonito"
            ], [
                "id" => 7389,
                "id_estado" => 23,
                "nome" => "Montenegro"
            ], [
                "id" => 7390,
                "id_estado" => 23,
                "nome" => "Mormaco"
            ], [
                "id" => 7391,
                "id_estado" => 23,
                "nome" => "Morrinhos"
            ], [
                "id" => 7392,
                "id_estado" => 23,
                "nome" => "Morrinhos do Sul"
            ], [
                "id" => 7393,
                "id_estado" => 23,
                "nome" => "Morro Alto"
            ], [
                "id" => 7394,
                "id_estado" => 23,
                "nome" => "Morro Azul"
            ], [
                "id" => 7395,
                "id_estado" => 23,
                "nome" => "Morro Redondo"
            ], [
                "id" => 7396,
                "id_estado" => 23,
                "nome" => "Morro Reuter"
            ], [
                "id" => 7397,
                "id_estado" => 23,
                "nome" => "Morungava"
            ], [
                "id" => 7398,
                "id_estado" => 23,
                "nome" => "Mostardas"
            ], [
                "id" => 7399,
                "id_estado" => 23,
                "nome" => "Mucum"
            ], [
                "id" => 7400,
                "id_estado" => 23,
                "nome" => "Muitos Capoes"
            ], [
                "id" => 7401,
                "id_estado" => 23,
                "nome" => "Muliterno"
            ], [
                "id" => 7402,
                "id_estado" => 23,
                "nome" => "Nao-Me-Toque"
            ], [
                "id" => 7403,
                "id_estado" => 23,
                "nome" => "Nazare"
            ], [
                "id" => 7404,
                "id_estado" => 23,
                "nome" => "Nicolau Vergueiro"
            ], [
                "id" => 7405,
                "id_estado" => 23,
                "nome" => "Nonoai"
            ], [
                "id" => 7406,
                "id_estado" => 23,
                "nome" => "Nossa Senhora Aparecida"
            ], [
                "id" => 7407,
                "id_estado" => 23,
                "nome" => "Nossa Senhora da Conceicao"
            ], [
                "id" => 7408,
                "id_estado" => 23,
                "nome" => "Nova Alvorada"
            ], [
                "id" => 7409,
                "id_estado" => 23,
                "nome" => "Nova Araca"
            ], [
                "id" => 7410,
                "id_estado" => 23,
                "nome" => "Nova Bassano"
            ], [
                "id" => 7411,
                "id_estado" => 23,
                "nome" => "Nova Boa Vista"
            ], [
                "id" => 7412,
                "id_estado" => 23,
                "nome" => "Nova Brescia"
            ], [
                "id" => 7413,
                "id_estado" => 23,
                "nome" => "Nova Esperanca do Sul"
            ], [
                "id" => 7414,
                "id_estado" => 23,
                "nome" => "Nova Hartz"
            ], [
                "id" => 7415,
                "id_estado" => 23,
                "nome" => "Nova Milano"
            ], [
                "id" => 7416,
                "id_estado" => 23,
                "nome" => "Nova Padua"
            ], [
                "id" => 7417,
                "id_estado" => 23,
                "nome" => "Nova Palma"
            ], [
                "id" => 7418,
                "id_estado" => 23,
                "nome" => "Nova Petropolis"
            ], [
                "id" => 7419,
                "id_estado" => 23,
                "nome" => "Nova Prata"
            ], [
                "id" => 7420,
                "id_estado" => 23,
                "nome" => "Nova Roma do Sul"
            ], [
                "id" => 7421,
                "id_estado" => 23,
                "nome" => "Nova Santa Rita"
            ], [
                "id" => 7422,
                "id_estado" => 23,
                "nome" => "Nova Sardenha"
            ], [
                "id" => 7423,
                "id_estado" => 23,
                "nome" => "Novo Barreiro"
            ], [
                "id" => 7424,
                "id_estado" => 23,
                "nome" => "Novo Hamburgo"
            ], [
                "id" => 7425,
                "id_estado" => 23,
                "nome" => "Novo Horizonte"
            ], [
                "id" => 7426,
                "id_estado" => 23,
                "nome" => "Novo Machado"
            ], [
                "id" => 7427,
                "id_estado" => 23,
                "nome" => "Novo Planalto"
            ], [
                "id" => 7428,
                "id_estado" => 23,
                "nome" => "Novo Tiradentes"
            ], [
                "id" => 7429,
                "id_estado" => 23,
                "nome" => "Oliva"
            ], [
                "id" => 7430,
                "id_estado" => 23,
                "nome" => "Oralina"
            ], [
                "id" => 7431,
                "id_estado" => 23,
                "nome" => "Osorio"
            ], [
                "id" => 7432,
                "id_estado" => 23,
                "nome" => "Osvaldo Cruz"
            ], [
                "id" => 7433,
                "id_estado" => 23,
                "nome" => "Osvaldo Kroeff"
            ], [
                "id" => 7434,
                "id_estado" => 23,
                "nome" => "Otavio Rocha"
            ], [
                "id" => 7435,
                "id_estado" => 23,
                "nome" => "Pacheca"
            ], [
                "id" => 7436,
                "id_estado" => 23,
                "nome" => "Padilha"
            ], [
                "id" => 7437,
                "id_estado" => 23,
                "nome" => "Padre Gonzales"
            ], [
                "id" => 7438,
                "id_estado" => 23,
                "nome" => "Paim Filho"
            ], [
                "id" => 7439,
                "id_estado" => 23,
                "nome" => "Palmares do Sul"
            ], [
                "id" => 7440,
                "id_estado" => 23,
                "nome" => "Palmas"
            ], [
                "id" => 7441,
                "id_estado" => 23,
                "nome" => "Palmeira das Missoes"
            ], [
                "id" => 7442,
                "id_estado" => 23,
                "nome" => "Palmitinho"
            ], [
                "id" => 7443,
                "id_estado" => 23,
                "nome" => "Pampeiro"
            ], [
                "id" => 7444,
                "id_estado" => 23,
                "nome" => "Panambi"
            ], [
                "id" => 7445,
                "id_estado" => 23,
                "nome" => "Pantano Grande"
            ], [
                "id" => 7446,
                "id_estado" => 23,
                "nome" => "Parai"
            ], [
                "id" => 7447,
                "id_estado" => 23,
                "nome" => "Paraiso do Sul"
            ], [
                "id" => 7448,
                "id_estado" => 23,
                "nome" => "Pareci Novo"
            ], [
                "id" => 7449,
                "id_estado" => 23,
                "nome" => "Parobe"
            ], [
                "id" => 7450,
                "id_estado" => 23,
                "nome" => "Passa Sete"
            ], [
                "id" => 7451,
                "id_estado" => 23,
                "nome" => "Passinhos"
            ], [
                "id" => 7452,
                "id_estado" => 23,
                "nome" => "Passo Burmann"
            ], [
                "id" => 7453,
                "id_estado" => 23,
                "nome" => "Passo da Areia"
            ], [
                "id" => 7454,
                "id_estado" => 23,
                "nome" => "Passo da Caveira"
            ], [
                "id" => 7455,
                "id_estado" => 23,
                "nome" => "Passo das Pedras"
            ], [
                "id" => 7456,
                "id_estado" => 23,
                "nome" => "Passo do Adao"
            ], [
                "id" => 7457,
                "id_estado" => 23,
                "nome" => "Passo do Sabao"
            ], [
                "id" => 7458,
                "id_estado" => 23,
                "nome" => "Passo do Sobrado"
            ], [
                "id" => 7459,
                "id_estado" => 23,
                "nome" => "Passo Fundo"
            ], [
                "id" => 7460,
                "id_estado" => 23,
                "nome" => "Passo Novo"
            ], [
                "id" => 7461,
                "id_estado" => 23,
                "nome" => "Passo Raso"
            ], [
                "id" => 7462,
                "id_estado" => 23,
                "nome" => "Paulo Bento"
            ], [
                "id" => 7463,
                "id_estado" => 23,
                "nome" => "Pavao"
            ], [
                "id" => 7464,
                "id_estado" => 23,
                "nome" => "Paverama"
            ], [
                "id" => 7465,
                "id_estado" => 23,
                "nome" => "Pedras Altas"
            ], [
                "id" => 7466,
                "id_estado" => 23,
                "nome" => "Pedreiras"
            ], [
                "id" => 7467,
                "id_estado" => 23,
                "nome" => "Pedro Garcia"
            ], [
                "id" => 7468,
                "id_estado" => 23,
                "nome" => "Pedro Osorio"
            ], [
                "id" => 7469,
                "id_estado" => 23,
                "nome" => "Pedro Paiva"
            ], [
                "id" => 7470,
                "id_estado" => 23,
                "nome" => "Pejucara"
            ], [
                "id" => 7471,
                "id_estado" => 23,
                "nome" => "Pelotas"
            ], [
                "id" => 7472,
                "id_estado" => 23,
                "nome" => "Picada Cafe"
            ], [
                "id" => 7473,
                "id_estado" => 23,
                "nome" => "Pinhal"
            ], [
                "id" => 7474,
                "id_estado" => 23,
                "nome" => "Pinhal Alto"
            ], [
                "id" => 7475,
                "id_estado" => 23,
                "nome" => "Pinhal da Serra"
            ], [
                "id" => 7476,
                "id_estado" => 23,
                "nome" => "Pinhal Grande"
            ], [
                "id" => 7477,
                "id_estado" => 23,
                "nome" => "Pinhalzinho"
            ], [
                "id" => 7478,
                "id_estado" => 23,
                "nome" => "Pinheirinho do Vale"
            ], [
                "id" => 7479,
                "id_estado" => 23,
                "nome" => "Pinheiro Machado"
            ], [
                "id" => 7480,
                "id_estado" => 23,
                "nome" => "Pinheiro Marcado"
            ], [
                "id" => 7481,
                "id_estado" => 23,
                "nome" => "Pinto Bandeira"
            ], [
                "id" => 7482,
                "id_estado" => 23,
                "nome" => "Pirai"
            ], [
                "id" => 7483,
                "id_estado" => 23,
                "nome" => "Pirapo"
            ], [
                "id" => 7484,
                "id_estado" => 23,
                "nome" => "Piratini"
            ], [
                "id" => 7485,
                "id_estado" => 23,
                "nome" => "Pitanga"
            ], [
                "id" => 7486,
                "id_estado" => 23,
                "nome" => "Planalto"
            ], [
                "id" => 7487,
                "id_estado" => 23,
                "nome" => "Plano Alto"
            ], [
                "id" => 7488,
                "id_estado" => 23,
                "nome" => "Poco das Antas"
            ], [
                "id" => 7489,
                "id_estado" => 23,
                "nome" => "Poligono do Erval"
            ], [
                "id" => 7490,
                "id_estado" => 23,
                "nome" => "Polo Petroquimico de Triunfo"
            ], [
                "id" => 7491,
                "id_estado" => 23,
                "nome" => "Pontao"
            ], [
                "id" => 7492,
                "id_estado" => 23,
                "nome" => "Ponte Preta"
            ], [
                "id" => 7493,
                "id_estado" => 23,
                "nome" => "Portao"
            ], [
                "id" => 7494,
                "id_estado" => 23,
                "nome" => "Porto Alegre"
            ], [
                "id" => 7495,
                "id_estado" => 23,
                "nome" => "Porto Batista"
            ], [
                "id" => 7496,
                "id_estado" => 23,
                "nome" => "Porto Lucena"
            ], [
                "id" => 7497,
                "id_estado" => 23,
                "nome" => "Porto Maua"
            ], [
                "id" => 7498,
                "id_estado" => 23,
                "nome" => "Porto Vera Cruz"
            ], [
                "id" => 7499,
                "id_estado" => 23,
                "nome" => "Porto Xavier"
            ], [
                "id" => 7500,
                "id_estado" => 23,
                "nome" => "Pouso Novo"
            ], [
                "id" => 7501,
                "id_estado" => 23,
                "nome" => "Povo Novo"
            ], [
                "id" => 7502,
                "id_estado" => 23,
                "nome" => "Povoado Tozzo"
            ], [
                "id" => 7503,
                "id_estado" => 23,
                "nome" => "Pranchada"
            ], [
                "id" => 7504,
                "id_estado" => 23,
                "nome" => "Pratos"
            ], [
                "id" => 7505,
                "id_estado" => 23,
                "nome" => "Presidente Lucena"
            ], [
                "id" => 7506,
                "id_estado" => 23,
                "nome" => "Progresso"
            ], [
                "id" => 7507,
                "id_estado" => 23,
                "nome" => "Protasio Alves"
            ], [
                "id" => 7508,
                "id_estado" => 23,
                "nome" => "Pulador"
            ], [
                "id" => 7509,
                "id_estado" => 23,
                "nome" => "Putinga"
            ], [
                "id" => 7510,
                "id_estado" => 23,
                "nome" => "Quarai"
            ], [
                "id" => 7511,
                "id_estado" => 23,
                "nome" => "Quaraim"
            ], [
                "id" => 7512,
                "id_estado" => 23,
                "nome" => "Quatro Irmaos"
            ], [
                "id" => 7513,
                "id_estado" => 23,
                "nome" => "Quevedos"
            ], [
                "id" => 7514,
                "id_estado" => 23,
                "nome" => "Quilombo"
            ], [
                "id" => 7515,
                "id_estado" => 23,
                "nome" => "Quinta"
            ], [
                "id" => 7516,
                "id_estado" => 23,
                "nome" => "Quintao"
            ], [
                "id" => 7517,
                "id_estado" => 23,
                "nome" => "Quinze de Novembro"
            ], [
                "id" => 7518,
                "id_estado" => 23,
                "nome" => "Quiteria"
            ], [
                "id" => 7519,
                "id_estado" => 23,
                "nome" => "Ramada"
            ], [
                "id" => 7520,
                "id_estado" => 23,
                "nome" => "Rancho Velho"
            ], [
                "id" => 7521,
                "id_estado" => 23,
                "nome" => "Redentora"
            ], [
                "id" => 7522,
                "id_estado" => 23,
                "nome" => "Refugiado"
            ], [
                "id" => 7523,
                "id_estado" => 23,
                "nome" => "Relvado"
            ], [
                "id" => 7524,
                "id_estado" => 23,
                "nome" => "Restinga Seca"
            ], [
                "id" => 7525,
                "id_estado" => 23,
                "nome" => "Ricardo"
            ], [
                "id" => 7526,
                "id_estado" => 23,
                "nome" => "Rincao de Sao Pedro"
            ], [
                "id" => 7527,
                "id_estado" => 23,
                "nome" => "Rincao del Rei"
            ], [
                "id" => 7528,
                "id_estado" => 23,
                "nome" => "Rincao do Cristovao Pereira"
            ], [
                "id" => 7529,
                "id_estado" => 23,
                "nome" => "Rincao do Meio"
            ], [
                "id" => 7530,
                "id_estado" => 23,
                "nome" => "Rincao do Segredo"
            ], [
                "id" => 7531,
                "id_estado" => 23,
                "nome" => "Rincao Doce"
            ], [
                "id" => 7532,
                "id_estado" => 23,
                "nome" => "Rincao dos Kroeff"
            ], [
                "id" => 7533,
                "id_estado" => 23,
                "nome" => "Rincao dos Mendes"
            ], [
                "id" => 7534,
                "id_estado" => 23,
                "nome" => "Rincao Vermelho"
            ], [
                "id" => 7535,
                "id_estado" => 23,
                "nome" => "Rio Azul"
            ], [
                "id" => 7536,
                "id_estado" => 23,
                "nome" => "Rio Branco"
            ], [
                "id" => 7537,
                "id_estado" => 23,
                "nome" => "Rio da Ilha"
            ], [
                "id" => 7538,
                "id_estado" => 23,
                "nome" => "Rio dos Indios"
            ], [
                "id" => 7539,
                "id_estado" => 23,
                "nome" => "Rio Grande"
            ], [
                "id" => 7540,
                "id_estado" => 23,
                "nome" => "Rio Pardinho"
            ], [
                "id" => 7541,
                "id_estado" => 23,
                "nome" => "Rio Pardo"
            ], [
                "id" => 7542,
                "id_estado" => 23,
                "nome" => "Rio Telha"
            ], [
                "id" => 7543,
                "id_estado" => 23,
                "nome" => "Rio Tigre"
            ], [
                "id" => 7544,
                "id_estado" => 23,
                "nome" => "Rio Toldo"
            ], [
                "id" => 7545,
                "id_estado" => 23,
                "nome" => "Riozinho"
            ], [
                "id" => 7546,
                "id_estado" => 23,
                "nome" => "Roca Sales"
            ], [
                "id" => 7547,
                "id_estado" => 23,
                "nome" => "Rodeio Bonito"
            ], [
                "id" => 7548,
                "id_estado" => 23,
                "nome" => "Rolador"
            ], [
                "id" => 7549,
                "id_estado" => 23,
                "nome" => "Rolante"
            ], [
                "id" => 7550,
                "id_estado" => 23,
                "nome" => "Rolantinho da Figueira"
            ], [
                "id" => 7551,
                "id_estado" => 23,
                "nome" => "Ronda Alta"
            ], [
                "id" => 7552,
                "id_estado" => 23,
                "nome" => "Rondinha"
            ], [
                "id" => 7553,
                "id_estado" => 23,
                "nome" => "Roque Gonzales"
            ], [
                "id" => 7554,
                "id_estado" => 23,
                "nome" => "Rosario"
            ], [
                "id" => 7555,
                "id_estado" => 23,
                "nome" => "Rosario do Sul"
            ], [
                "id" => 7556,
                "id_estado" => 23,
                "nome" => "Rua Nova"
            ], [
                "id" => 7557,
                "id_estado" => 23,
                "nome" => "Sagrada Familia"
            ], [
                "id" => 7558,
                "id_estado" => 23,
                "nome" => "Saica"
            ], [
                "id" => 7559,
                "id_estado" => 23,
                "nome" => "Saldanha Marinho"
            ], [
                "id" => 7560,
                "id_estado" => 23,
                "nome" => "Salgado Filho"
            ], [
                "id" => 7561,
                "id_estado" => 23,
                "nome" => "Saltinho"
            ], [
                "id" => 7562,
                "id_estado" => 23,
                "nome" => "Salto"
            ], [
                "id" => 7563,
                "id_estado" => 23,
                "nome" => "Salto do Jacui"
            ], [
                "id" => 7564,
                "id_estado" => 23,
                "nome" => "Salvador das Missoes"
            ], [
                "id" => 7565,
                "id_estado" => 23,
                "nome" => "Salvador do Sul"
            ], [
                "id" => 7566,
                "id_estado" => 23,
                "nome" => "Sananduva"
            ], [
                "id" => 7567,
                "id_estado" => 23,
                "nome" => "Sant Auta"
            ], [
                "id" => 7568,
                "id_estado" => 23,
                "nome" => "Santa Barbara"
            ], [
                "id" => 7569,
                "id_estado" => 23,
                "nome" => "Santa Barbara do Sul"
            ], [
                "id" => 7570,
                "id_estado" => 23,
                "nome" => "Santa Catarina"
            ], [
                "id" => 7571,
                "id_estado" => 23,
                "nome" => "Santa Cecilia"
            ], [
                "id" => 7572,
                "id_estado" => 23,
                "nome" => "Santa Clara do Ingai"
            ], [
                "id" => 7573,
                "id_estado" => 23,
                "nome" => "Santa Clara do Sul"
            ], [
                "id" => 7574,
                "id_estado" => 23,
                "nome" => "Santa Cristina"
            ], [
                "id" => 7575,
                "id_estado" => 23,
                "nome" => "Santa Cruz"
            ], [
                "id" => 7576,
                "id_estado" => 23,
                "nome" => "Santa Cruz da Concordia"
            ], [
                "id" => 7577,
                "id_estado" => 23,
                "nome" => "Santa Cruz do Sul"
            ], [
                "id" => 7578,
                "id_estado" => 23,
                "nome" => "Santa Flora"
            ], [
                "id" => 7579,
                "id_estado" => 23,
                "nome" => "Santa Ines"
            ], [
                "id" => 7580,
                "id_estado" => 23,
                "nome" => "Santa Izabel do Sul"
            ], [
                "id" => 7581,
                "id_estado" => 23,
                "nome" => "Santa Lucia"
            ], [
                "id" => 7582,
                "id_estado" => 23,
                "nome" => "Santa Lucia do Piai"
            ], [
                "id" => 7583,
                "id_estado" => 23,
                "nome" => "Santa Luiza"
            ], [
                "id" => 7584,
                "id_estado" => 23,
                "nome" => "Santa Luzia"
            ], [
                "id" => 7585,
                "id_estado" => 23,
                "nome" => "Santa Maria"
            ], [
                "id" => 7586,
                "id_estado" => 23,
                "nome" => "Santa Maria do Herval"
            ], [
                "id" => 7587,
                "id_estado" => 23,
                "nome" => "Santa Rita do Sul"
            ], [
                "id" => 7588,
                "id_estado" => 23,
                "nome" => "Santa Rosa"
            ], [
                "id" => 7589,
                "id_estado" => 23,
                "nome" => "Santa Silvana"
            ], [
                "id" => 7590,
                "id_estado" => 23,
                "nome" => "Santa Teresinha"
            ], [
                "id" => 7591,
                "id_estado" => 23,
                "nome" => "Santa Tereza"
            ], [
                "id" => 7592,
                "id_estado" => 23,
                "nome" => "Santa Vitoria do Palmar"
            ], [
                "id" => 7593,
                "id_estado" => 23,
                "nome" => "Santana"
            ], [
                "id" => 7594,
                "id_estado" => 23,
                "nome" => "Santana da Boa Vista"
            ], [
                "id" => 7595,
                "id_estado" => 23,
                "nome" => "Santana do Livramento"
            ], [
                "id" => 7596,
                "id_estado" => 23,
                "nome" => "Santiago"
            ], [
                "id" => 7597,
                "id_estado" => 23,
                "nome" => "Santo Amaro do Sul"
            ], [
                "id" => 7598,
                "id_estado" => 23,
                "nome" => "Santo Angelo"
            ], [
                "id" => 7599,
                "id_estado" => 23,
                "nome" => "Santo Antonio"
            ], [
                "id" => 7600,
                "id_estado" => 23,
                "nome" => "Santo Antonio da Patrulha"
            ], [
                "id" => 7601,
                "id_estado" => 23,
                "nome" => "Santo Antonio das Missoes"
            ], [
                "id" => 7602,
                "id_estado" => 23,
                "nome" => "Santo Antonio de Castro"
            ], [
                "id" => 7603,
                "id_estado" => 23,
                "nome" => "Santo Antonio do Bom Retiro"
            ], [
                "id" => 7604,
                "id_estado" => 23,
                "nome" => "Santo Antonio do Palma"
            ], [
                "id" => 7605,
                "id_estado" => 23,
                "nome" => "Santo Antonio do Planalto"
            ], [
                "id" => 7606,
                "id_estado" => 23,
                "nome" => "Santo Augusto"
            ], [
                "id" => 7607,
                "id_estado" => 23,
                "nome" => "Santo Cristo"
            ], [
                "id" => 7608,
                "id_estado" => 23,
                "nome" => "Santo Expedito do Sul"
            ], [
                "id" => 7609,
                "id_estado" => 23,
                "nome" => "Santo Inacio"
            ], [
                "id" => 7610,
                "id_estado" => 23,
                "nome" => "Sao Bento"
            ], [
                "id" => 7611,
                "id_estado" => 23,
                "nome" => "Sao Bom Jesus"
            ], [
                "id" => 7612,
                "id_estado" => 23,
                "nome" => "Sao Borja"
            ], [
                "id" => 7613,
                "id_estado" => 23,
                "nome" => "Sao Braz"
            ], [
                "id" => 7614,
                "id_estado" => 23,
                "nome" => "Sao Carlos"
            ], [
                "id" => 7615,
                "id_estado" => 23,
                "nome" => "Sao Domingos do Sul"
            ], [
                "id" => 7616,
                "id_estado" => 23,
                "nome" => "Sao Francisco"
            ], [
                "id" => 7617,
                "id_estado" => 23,
                "nome" => "Sao Francisco de Assis"
            ], [
                "id" => 7618,
                "id_estado" => 23,
                "nome" => "Sao Francisco de Paula"
            ], [
                "id" => 7619,
                "id_estado" => 23,
                "nome" => "Sao Gabriel"
            ], [
                "id" => 7620,
                "id_estado" => 23,
                "nome" => "Sao Jeronimo"
            ], [
                "id" => 7621,
                "id_estado" => 23,
                "nome" => "Sao Joao"
            ], [
                "id" => 7622,
                "id_estado" => 23,
                "nome" => "Sao Joao Batista"
            ], [
                "id" => 7623,
                "id_estado" => 23,
                "nome" => "Sao Joao Bosco"
            ], [
                "id" => 7624,
                "id_estado" => 23,
                "nome" => "Sao Joao da Urtiga"
            ], [
                "id" => 7625,
                "id_estado" => 23,
                "nome" => "Sao Joao do Polesine"
            ], [
                "id" => 7626,
                "id_estado" => 23,
                "nome" => "Sao Jorge"
            ], [
                "id" => 7627,
                "id_estado" => 23,
                "nome" => "Sao Jose"
            ], [
                "id" => 7628,
                "id_estado" => 23,
                "nome" => "Sao Jose da Gloria"
            ], [
                "id" => 7629,
                "id_estado" => 23,
                "nome" => "Sao Jose das Missoes"
            ], [
                "id" => 7630,
                "id_estado" => 23,
                "nome" => "Sao Jose de Castro"
            ], [
                "id" => 7631,
                "id_estado" => 23,
                "nome" => "Sao Jose do Centro"
            ], [
                "id" => 7632,
                "id_estado" => 23,
                "nome" => "Sao Jose do Herval"
            ], [
                "id" => 7633,
                "id_estado" => 23,
                "nome" => "Sao Jose do Hortencio"
            ], [
                "id" => 7634,
                "id_estado" => 23,
                "nome" => "Sao Jose do Inhacora"
            ], [
                "id" => 7635,
                "id_estado" => 23,
                "nome" => "Sao Jose do Norte"
            ], [
                "id" => 7636,
                "id_estado" => 23,
                "nome" => "Sao Jose do Ouro"
            ], [
                "id" => 7637,
                "id_estado" => 23,
                "nome" => "Sao Jose dos Ausentes"
            ], [
                "id" => 7638,
                "id_estado" => 23,
                "nome" => "Sao Leopoldo"
            ], [
                "id" => 7639,
                "id_estado" => 23,
                "nome" => "Sao Lourenco das Missoes"
            ], [
                "id" => 7640,
                "id_estado" => 23,
                "nome" => "Sao Lourenco do Sul"
            ], [
                "id" => 7641,
                "id_estado" => 23,
                "nome" => "Sao Luis Rei"
            ], [
                "id" => 7642,
                "id_estado" => 23,
                "nome" => "Sao Luiz"
            ], [
                "id" => 7643,
                "id_estado" => 23,
                "nome" => "Sao Luiz Gonzaga"
            ], [
                "id" => 7644,
                "id_estado" => 23,
                "nome" => "Sao Manuel"
            ], [
                "id" => 7645,
                "id_estado" => 23,
                "nome" => "Sao Marcos"
            ], [
                "id" => 7646,
                "id_estado" => 23,
                "nome" => "Sao Martinho"
            ], [
                "id" => 7647,
                "id_estado" => 23,
                "nome" => "Sao Martinho da Serra"
            ], [
                "id" => 7648,
                "id_estado" => 23,
                "nome" => "Sao Miguel"
            ], [
                "id" => 7649,
                "id_estado" => 23,
                "nome" => "Sao Miguel das Missoes"
            ], [
                "id" => 7650,
                "id_estado" => 23,
                "nome" => "Sao Nicolau"
            ], [
                "id" => 7651,
                "id_estado" => 23,
                "nome" => "Sao Paulo"
            ], [
                "id" => 7652,
                "id_estado" => 23,
                "nome" => "Sao Paulo das Missoes"
            ], [
                "id" => 7653,
                "id_estado" => 23,
                "nome" => "Sao Pedro"
            ], [
                "id" => 7654,
                "id_estado" => 23,
                "nome" => "Sao Pedro da Serra"
            ], [
                "id" => 7655,
                "id_estado" => 23,
                "nome" => "Sao Pedro de Alcantara"
            ], [
                "id" => 7656,
                "id_estado" => 23,
                "nome" => "Sao Pedro do Butia"
            ], [
                "id" => 7657,
                "id_estado" => 23,
                "nome" => "Sao Pedro do Iraxim"
            ], [
                "id" => 7658,
                "id_estado" => 23,
                "nome" => "Sao Pedro do Sul"
            ], [
                "id" => 7659,
                "id_estado" => 23,
                "nome" => "Sao Roque"
            ], [
                "id" => 7660,
                "id_estado" => 23,
                "nome" => "Sao Sebastiao"
            ], [
                "id" => 7661,
                "id_estado" => 23,
                "nome" => "Sao Sebastiao do Cai"
            ], [
                "id" => 7662,
                "id_estado" => 23,
                "nome" => "Sao Sepe"
            ], [
                "id" => 7663,
                "id_estado" => 23,
                "nome" => "Sao Simao"
            ], [
                "id" => 7664,
                "id_estado" => 23,
                "nome" => "Sao Valentim"
            ], [
                "id" => 7665,
                "id_estado" => 23,
                "nome" => "Sao Valentim do Sul"
            ], [
                "id" => 7666,
                "id_estado" => 23,
                "nome" => "Sao Valerio do Sul"
            ], [
                "id" => 7667,
                "id_estado" => 23,
                "nome" => "Sao Vendelino"
            ], [
                "id" => 7668,
                "id_estado" => 23,
                "nome" => "Sao Vicente do Sul"
            ], [
                "id" => 7669,
                "id_estado" => 23,
                "nome" => "Sapiranga"
            ], [
                "id" => 7670,
                "id_estado" => 23,
                "nome" => "Sapucaia do Sul"
            ], [
                "id" => 7671,
                "id_estado" => 23,
                "nome" => "Sarandi"
            ], [
                "id" => 7672,
                "id_estado" => 23,
                "nome" => "Scharlau"
            ], [
                "id" => 7673,
                "id_estado" => 23,
                "nome" => "Seberi"
            ], [
                "id" => 7674,
                "id_estado" => 23,
                "nome" => "Seca"
            ], [
                "id" => 7675,
                "id_estado" => 23,
                "nome" => "Sede Aurora"
            ], [
                "id" => 7676,
                "id_estado" => 23,
                "nome" => "Sede Nova"
            ], [
                "id" => 7677,
                "id_estado" => 23,
                "nome" => "Segredo"
            ], [
                "id" => 7678,
                "id_estado" => 23,
                "nome" => "Seival"
            ], [
                "id" => 7679,
                "id_estado" => 23,
                "nome" => "Selbach"
            ], [
                "id" => 7680,
                "id_estado" => 23,
                "nome" => "Sentinela do Sul"
            ], [
                "id" => 7681,
                "id_estado" => 23,
                "nome" => "Serafim Schmidt"
            ], [
                "id" => 7682,
                "id_estado" => 23,
                "nome" => "Serafina Correa"
            ], [
                "id" => 7683,
                "id_estado" => 23,
                "nome" => "Serio"
            ], [
                "id" => 7684,
                "id_estado" => 23,
                "nome" => "Serra dos Gregorios"
            ], [
                "id" => 7685,
                "id_estado" => 23,
                "nome" => "Serrinha"
            ], [
                "id" => 7686,
                "id_estado" => 23,
                "nome" => "Sertao"
            ], [
                "id" => 7687,
                "id_estado" => 23,
                "nome" => "Sertao Santana"
            ], [
                "id" => 7688,
                "id_estado" => 23,
                "nome" => "Sertaozinho"
            ], [
                "id" => 7689,
                "id_estado" => 23,
                "nome" => "Sete de Setembro"
            ], [
                "id" => 7690,
                "id_estado" => 23,
                "nome" => "Sete Lagoas"
            ], [
                "id" => 7691,
                "id_estado" => 23,
                "nome" => "Severiano de Almeida"
            ], [
                "id" => 7692,
                "id_estado" => 23,
                "nome" => "Silva Jardim"
            ], [
                "id" => 7693,
                "id_estado" => 23,
                "nome" => "Silveira"
            ], [
                "id" => 7694,
                "id_estado" => 23,
                "nome" => "Silveira Martins"
            ], [
                "id" => 7695,
                "id_estado" => 23,
                "nome" => "Sinimbu"
            ], [
                "id" => 7696,
                "id_estado" => 23,
                "nome" => "Sirio"
            ], [
                "id" => 7697,
                "id_estado" => 23,
                "nome" => "Sitio Gabriel"
            ], [
                "id" => 7698,
                "id_estado" => 23,
                "nome" => "Sobradinho"
            ], [
                "id" => 7699,
                "id_estado" => 23,
                "nome" => "Soledade"
            ], [
                "id" => 7700,
                "id_estado" => 23,
                "nome" => "Souza Ramos"
            ], [
                "id" => 7701,
                "id_estado" => 23,
                "nome" => "Suspiro"
            ], [
                "id" => 7702,
                "id_estado" => 23,
                "nome" => "Tabai"
            ], [
                "id" => 7703,
                "id_estado" => 23,
                "nome" => "Tabajara"
            ], [
                "id" => 7704,
                "id_estado" => 23,
                "nome" => "Taim"
            ], [
                "id" => 7705,
                "id_estado" => 23,
                "nome" => "Tainhas"
            ], [
                "id" => 7706,
                "id_estado" => 23,
                "nome" => "Tamandua"
            ], [
                "id" => 7707,
                "id_estado" => 23,
                "nome" => "Tanque"
            ], [
                "id" => 7708,
                "id_estado" => 23,
                "nome" => "Tapejara"
            ], [
                "id" => 7709,
                "id_estado" => 23,
                "nome" => "Tapera"
            ], [
                "id" => 7710,
                "id_estado" => 23,
                "nome" => "Tapes"
            ], [
                "id" => 7711,
                "id_estado" => 23,
                "nome" => "Taquara"
            ], [
                "id" => 7712,
                "id_estado" => 23,
                "nome" => "Taquari"
            ], [
                "id" => 7713,
                "id_estado" => 23,
                "nome" => "Taquarichim"
            ], [
                "id" => 7714,
                "id_estado" => 23,
                "nome" => "Taquarucu do Sul"
            ], [
                "id" => 7715,
                "id_estado" => 23,
                "nome" => "Tavares"
            ], [
                "id" => 7716,
                "id_estado" => 23,
                "nome" => "Tenente Portela"
            ], [
                "id" => 7717,
                "id_estado" => 23,
                "nome" => "Terra de Areia"
            ], [
                "id" => 7718,
                "id_estado" => 23,
                "nome" => "Tesouras"
            ], [
                "id" => 7719,
                "id_estado" => 23,
                "nome" => "Teutonia"
            ], [
                "id" => 7720,
                "id_estado" => 23,
                "nome" => "Tiaraju"
            ], [
                "id" => 7721,
                "id_estado" => 23,
                "nome" => "Timbauva"
            ], [
                "id" => 7722,
                "id_estado" => 23,
                "nome" => "Tiradentes do Sul"
            ], [
                "id" => 7723,
                "id_estado" => 23,
                "nome" => "Toropi"
            ], [
                "id" => 7724,
                "id_estado" => 23,
                "nome" => "Toroqua"
            ], [
                "id" => 7725,
                "id_estado" => 23,
                "nome" => "Torquato Severo"
            ], [
                "id" => 7726,
                "id_estado" => 23,
                "nome" => "Torres"
            ], [
                "id" => 7727,
                "id_estado" => 23,
                "nome" => "Torrinhas"
            ], [
                "id" => 7728,
                "id_estado" => 23,
                "nome" => "Touro Passo"
            ], [
                "id" => 7729,
                "id_estado" => 23,
                "nome" => "Tramandai"
            ], [
                "id" => 7730,
                "id_estado" => 23,
                "nome" => "Travesseiro"
            ], [
                "id" => 7731,
                "id_estado" => 23,
                "nome" => "Trentin"
            ], [
                "id" => 7732,
                "id_estado" => 23,
                "nome" => "Tres Arroios"
            ], [
                "id" => 7733,
                "id_estado" => 23,
                "nome" => "Tres Barras"
            ], [
                "id" => 7734,
                "id_estado" => 23,
                "nome" => "Tres Cachoeiras"
            ], [
                "id" => 7735,
                "id_estado" => 23,
                "nome" => "Tres Coroas"
            ], [
                "id" => 7736,
                "id_estado" => 23,
                "nome" => "Tres de Maio"
            ], [
                "id" => 7737,
                "id_estado" => 23,
                "nome" => "Tres Forquilhas"
            ], [
                "id" => 7738,
                "id_estado" => 23,
                "nome" => "Tres Palmeiras"
            ], [
                "id" => 7739,
                "id_estado" => 23,
                "nome" => "Tres Passos"
            ], [
                "id" => 7740,
                "id_estado" => 23,
                "nome" => "Tres Vendas"
            ], [
                "id" => 7741,
                "id_estado" => 23,
                "nome" => "Trindade do Sul"
            ], [
                "id" => 7742,
                "id_estado" => 23,
                "nome" => "Triunfo"
            ], [
                "id" => 7743,
                "id_estado" => 23,
                "nome" => "Tronqueiras"
            ], [
                "id" => 7744,
                "id_estado" => 23,
                "nome" => "Tucunduva"
            ], [
                "id" => 7745,
                "id_estado" => 23,
                "nome" => "Tuiuti"
            ], [
                "id" => 7746,
                "id_estado" => 23,
                "nome" => "Tunas"
            ], [
                "id" => 7747,
                "id_estado" => 23,
                "nome" => "Tupanci do Sul"
            ], [
                "id" => 7748,
                "id_estado" => 23,
                "nome" => "Tupancireta"
            ], [
                "id" => 7749,
                "id_estado" => 23,
                "nome" => "Tupancy Ou Vila Block"
            ], [
                "id" => 7750,
                "id_estado" => 23,
                "nome" => "Tupandi"
            ], [
                "id" => 7751,
                "id_estado" => 23,
                "nome" => "Tupantuba"
            ], [
                "id" => 7752,
                "id_estado" => 23,
                "nome" => "Tuparendi"
            ], [
                "id" => 7753,
                "id_estado" => 23,
                "nome" => "Tupi Silveira"
            ], [
                "id" => 7754,
                "id_estado" => 23,
                "nome" => "Tupinamba"
            ], [
                "id" => 7755,
                "id_estado" => 23,
                "nome" => "Turvinho"
            ], [
                "id" => 7756,
                "id_estado" => 23,
                "nome" => "Ubiretama"
            ], [
                "id" => 7757,
                "id_estado" => 23,
                "nome" => "Umbu"
            ], [
                "id" => 7758,
                "id_estado" => 23,
                "nome" => "Uniao da Serra"
            ], [
                "id" => 7759,
                "id_estado" => 23,
                "nome" => "Unistalda"
            ], [
                "id" => 7760,
                "id_estado" => 23,
                "nome" => "Uruguaiana"
            ], [
                "id" => 7761,
                "id_estado" => 23,
                "nome" => "Vacacai"
            ], [
                "id" => 7762,
                "id_estado" => 23,
                "nome" => "Vacaria"
            ], [
                "id" => 7763,
                "id_estado" => 23,
                "nome" => "Vale do Rio Cai"
            ], [
                "id" => 7764,
                "id_estado" => 23,
                "nome" => "Vale do Sol"
            ], [
                "id" => 7765,
                "id_estado" => 23,
                "nome" => "Vale Real"
            ], [
                "id" => 7766,
                "id_estado" => 23,
                "nome" => "Vale Veneto"
            ], [
                "id" => 7767,
                "id_estado" => 23,
                "nome" => "Vanini"
            ], [
                "id" => 7768,
                "id_estado" => 23,
                "nome" => "Venancio Aires"
            ], [
                "id" => 7769,
                "id_estado" => 23,
                "nome" => "Vera Cruz"
            ], [
                "id" => 7770,
                "id_estado" => 23,
                "nome" => "Veranopolis"
            ], [
                "id" => 7771,
                "id_estado" => 23,
                "nome" => "Vertentes"
            ], [
                "id" => 7772,
                "id_estado" => 23,
                "nome" => "Vespasiano Correa"
            ], [
                "id" => 7773,
                "id_estado" => 23,
                "nome" => "Viadutos"
            ], [
                "id" => 7774,
                "id_estado" => 23,
                "nome" => "Viamao"
            ], [
                "id" => 7775,
                "id_estado" => 23,
                "nome" => "Vicente Dutra"
            ], [
                "id" => 7776,
                "id_estado" => 23,
                "nome" => "Victor Graeff"
            ], [
                "id" => 7777,
                "id_estado" => 23,
                "nome" => "Vila Bender"
            ], [
                "id" => 7778,
                "id_estado" => 23,
                "nome" => "Vila Cristal"
            ], [
                "id" => 7779,
                "id_estado" => 23,
                "nome" => "Vila Cruz"
            ], [
                "id" => 7780,
                "id_estado" => 23,
                "nome" => "Vila Flores"
            ], [
                "id" => 7781,
                "id_estado" => 23,
                "nome" => "Vila Lange"
            ], [
                "id" => 7782,
                "id_estado" => 23,
                "nome" => "Vila Laranjeira"
            ], [
                "id" => 7783,
                "id_estado" => 23,
                "nome" => "Vila Maria"
            ], [
                "id" => 7784,
                "id_estado" => 23,
                "nome" => "Vila Nova do Sul"
            ], [
                "id" => 7785,
                "id_estado" => 23,
                "nome" => "Vila Operaria"
            ], [
                "id" => 7786,
                "id_estado" => 23,
                "nome" => "Vila Rica"
            ], [
                "id" => 7787,
                "id_estado" => 23,
                "nome" => "Vila Seca"
            ], [
                "id" => 7788,
                "id_estado" => 23,
                "nome" => "Vila Turvo"
            ], [
                "id" => 7789,
                "id_estado" => 23,
                "nome" => "Vinte e Sete da Boa Vista"
            ], [
                "id" => 7790,
                "id_estado" => 23,
                "nome" => "Vista Alegre"
            ], [
                "id" => 7791,
                "id_estado" => 23,
                "nome" => "Vista Alegre do Prata"
            ], [
                "id" => 7792,
                "id_estado" => 23,
                "nome" => "Vista Gaucha"
            ], [
                "id" => 7793,
                "id_estado" => 23,
                "nome" => "Vitoria"
            ], [
                "id" => 7794,
                "id_estado" => 23,
                "nome" => "Vitoria das Missoes"
            ], [
                "id" => 7795,
                "id_estado" => 23,
                "nome" => "Volta Alegre"
            ], [
                "id" => 7796,
                "id_estado" => 23,
                "nome" => "Volta Fechada"
            ], [
                "id" => 7797,
                "id_estado" => 23,
                "nome" => "Volta Grande"
            ], [
                "id" => 7798,
                "id_estado" => 23,
                "nome" => "Xadrez"
            ], [
                "id" => 7799,
                "id_estado" => 23,
                "nome" => "Xangri-La"
            ], [
                "id" => 7800,
                "id_estado" => 23,
                "nome" => "Xingu"
            ]
        );
    }
}
