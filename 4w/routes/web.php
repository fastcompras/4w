<?php

use Illuminate\Http\Request;
use App\Models\Servico;

Route::group(['middleware' => ['auth', 'forceSSL']], function()
{
    //Dash interna
    Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/impersonate/{id}', 'UsuarioController@loginAsUser');

    //Funcionalidade que verifica documentação pendente quando o usuário está na página principal
    Route::post('/usuario/verificaDocumentacaoPendente', 'HomeController@verificaDocumentacaoPendente');

    Route::get('/unreadNotifications', 'UsuarioController@unreadNotifications');
    Route::post('/notifications/read/', 'UsuarioController@readNotifications');

    //Conta do usuário

    Route::post('/conta/perfil/editar', 'UsuarioController@editarPerfil');

    Route::post('/conta/atualizarImagem', 'UsuarioController@atualizarImagem');
    Route::get('/conta/agenda', 'UsuarioController@minhaAgenda');
    Route::get('/conta/editar', 'UsuarioController@editar');
    Route::get('/conta/perfil/{id}', 'UsuarioController@perfil');
    Route::put('/conta/atualizar', 'UsuarioController@atualizar');
    Route::put('/conta/atualizarLocalizacao', 'UsuarioController@atualizarLocalizacao');
    Route::get('/usuario/agenda/{id}', 'OportunidadeController@agenda');

    //Perfil de Conta
    Route::get('/cadastros/perfilConta', 'PerfilContaController@novo')->name('perfil.create');
    Route::post('/cadastros/perfilConta', 'PerfilContaController@salvar');

    //Mercado Pago
    Route::get('/mp/success/', 'OportunidadeController@pagamentoSucesso');
    Route::get('/mp/failure/', 'OportunidadeController@pagamentoFalha');
    Route::get('/mp/pending/', 'OportunidadeController@pagamentoPendente');

    //Oportunidades
    Route::get('/oportunidades/', 'OportunidadeController@index');
    Route::get('/oportunidades/pendentes/', 'OportunidadeController@servicosPendentes');
    Route::get('/oportunidades/pendentesConfirmacao/', 'OportunidadeController@servicosAguardandoConfirmacao');
    Route::get('/oportunidades/agendados/', 'OportunidadeController@servicosAgendados');
    Route::get('/oportunidades/historico/', 'OportunidadeController@historicoServicos');

    Route::get('/oportunidades/ultimaSolicitacao/{id}', 'OportunidadeController@ultimaSolicitacao');
    Route::get('/oportunidades/solicitar', 'OportunidadeController@novo');
    Route::get('/oportunidades/visualizar/{id}', 'OportunidadeController@visualizar');
    Route::get('/oportunidades/editar/{id}', 'OportunidadeController@editar');
    Route::post('/oportunidades/salvar', 'OportunidadeController@salvar');
    Route::post('/oportunidades/reagendar', 'OportunidadeController@reagendar');
    Route::delete('/oportunidades/remover', 'OportunidadeController@remover');
    Route::post('/oportunidades/atualizar', 'OportunidadeController@atualizar');
    Route::post('/oportunidades/selecionarPrestador', 'OportunidadeController@selecionarPrestador');
    Route::post('/oportunidades/desvincularPrestador', 'OportunidadeController@desvincularPrestador');
    Route::post('/oportunidades/aceitarOferta', 'OportunidadeController@aceitarOferta');
    Route::post('/oportunidades/rejeitarOferta', 'OportunidadeController@rejeitarOferta');
    Route::post('/oportunidades/cancelarServico', 'OportunidadeController@cancelarServico');
    Route::post('/oportunidades/cancelarSolicitacaoServico', 'OportunidadeController@cancelarSolicitacaoServico');
    Route::post('/oportunidades/finalizarServico', 'OportunidadeController@finalizarServico');
    Route::post('/oportunidades/avaliarServico', 'OportunidadeController@avaliarServico');
    Route::post('/oportunidades/avaliarTomador', 'OportunidadeController@avaliarTomador');
    Route::post('/oportunidades/motivosRejeicao', 'OportunidadeController@motivosRejeicao');
    Route::get('/oportunidades/avaliacoes', 'OportunidadeController@minhasAvaliacoes');
    Route::post('/oportunidades/buscaComentarioAvaliacao', 'OportunidadeController@getComentariosAvaliacao');
    Route::post('/oportunidades/calculaValor', 'OportunidadeController@calculaValor');

    //Documentação do Prestador
    Route::get('/conta/documentosPendentes', 'UsuarioController@documentosPendentes');
    Route::post('/conta/enviarDocumentacao', 'UsuarioController@enviaDocumentacao');
    Route::delete('/documentos/remover', 'AdminController@removerDocumento');

    //Usuários
    Route::get('/usuarios/tomadores', 'UsuarioController@tomadores')->name('usuarios.tomadores.show');
    Route::get('/usuarios/prestadores/', 'UsuarioController@prestadores')->name('usuarios.prestadores.show');
    Route::post('/usuarios/prestadores/suspender', 'UsuarioController@ativarDesativarPrestador');
    Route::post('/usuarios/prestadores/reativar', 'UsuarioController@ativarDesativarPrestador');
    Route::post('/usuarios/prestadores/buscaResumo', 'UsuarioController@buscaResumoPrestador');
    Route::get('/usuarios/prestadores/pendentes', 'UsuarioController@prestadoresPendentes')->name('usuarios.prestadores.pendentes.show');
    Route::get('/usuarios/prestadores/reprovados', 'UsuarioController@prestadoresReprovados')->name('usuarios.prestadores.reprovados.show');
    Route::post('/usuarios/prestadores/aprovar', 'UsuarioController@aprovarPrestador');
    Route::post('/usuarios/prestadores/rejeitar', 'UsuarioController@rejeitarPrestador');
    Route::get('/usuarios/prestadores/visualizarDocumentacao/{id}', 'UsuarioController@visualizarDocumentacao');
    Route::get('/usuarios/prestadores/download/{idUsuario}/{idTipo}', 'UsuarioController@download');
    Route::post('/usuario/buscaResumo/', 'UsuarioController@buscaResumo');
    Route::get('/usuario/avaliacoes/', 'UsuarioController@minhasAvaliacoes');

    //Serviços
    Route::get('/servicos/preco/base', 'ServicoBaseController@index')->name('base.show');
    Route::get('/servicos/preco/base/atualizar/{id}', 'ServicoBaseController@update')->name('base.update');
    Route::get('/servicos/preco/base/novo', 'ServicoBaseController@create')->name('base.create');

    Route::post('/servicos/preco/base/salvar', 'ServicoBaseController@save');
    Route::put('/servicos/preco/base/salvar', 'ServicoBaseController@save');

    Route::delete('/servicos/preco/base/remover/{id}', 'ServicoBaseController@remove');

    Route::post('/servicos/base/buscaAgravantes', 'ServicoBaseController@buscaAgravantes');
    Route::post('/servicos/base/buscaDescricao', 'ServicoBaseController@buscaDescricao');

    Route::get('/servicos/preco/agravante', 'ServicoBaseAgravanteController@index')->name('agravantes.show');
    Route::post('/servicos/preco/agravante', 'ServicoBaseAgravanteController@index');

    Route::get('/servicos/preco/agravante/novo', 'ServicoBaseAgravanteController@novo')->name('agravantes.create');
    Route::post('/servicos/preco/agravante/salvar', 'ServicoBaseAgravanteController@save');
    Route::put('/servicos/preco/agravante/update', 'ServicoBaseAgravanteController@update');
    Route::get('/servicos/preco/agravante/atualizar/{id}', 'ServicoBaseAgravanteController@atualizar')->name('agravantes.update');
    Route::delete('/servicos/preco/agravante/remover/{id}', 'ServicoBaseAgravanteController@remover');

    Route::get('/servicos/', 'ServicoController@index')->name('servicos.show');
    Route::get('/servicos/sugeridos', 'ServicoController@sugeridos')->name('servicos.sugestao.show');
    Route::get('/servicos/atualizar/{id}', 'ServicoController@novo')->name('servicos.update');
    Route::get('/servicos/novo', 'ServicoController@novo')->name('servicos.create');
    Route::post('/servicos/salvar', 'ServicoController@salvar');
    Route::post('/servicos/aprovar/{id}', 'ServicoController@aprovar');
    Route::post('/servicos/rejeitar/{id}', 'ServicoController@rejeitar');
    Route::put('/servicos/salvar', 'ServicoController@atualizar');
    Route::delete('/servicos/remover/{id}', 'ServicoController@remover');

    //Regiões e Bairros
    Route::get('/regioes/novo', 'LocalizacaoController@novaRegiao')->name('regiao.create');
    Route::get('/regioes/', 'LocalizacaoController@regioes')->name('regiao.show');
    Route::post('/regioes/salvar', 'LocalizacaoController@salvarRegiao');
    Route::get('/regioes/atualizar/{id}', 'LocalizacaoController@novaRegiao')->name('regiao.update');

    Route::get('/bairros/', 'LocalizacaoController@bairros')->name('bairro.show');
    Route::get('/bairros/novo', 'LocalizacaoController@novoBairro')->name('bairro.create');
    Route::post('/bairros/salvar', 'LocalizacaoController@salvarBairro');
    Route::get('/bairros/editar/{id}', 'LocalizacaoController@editarBairro')->name('bairro.update');
    Route::put('/bairros/atualizar', 'LocalizacaoController@atualizarBairro');
    Route::delete('/bairros/remover', 'LocalizacaoController@removerBairro');

    Route::get('/dash/notificacoes', 'DashController@buscaNotificacoes');

    Route::get('/mp/paymentTest/', 'OportunidadeController@paymentNotification');

    //Financeiro
    Route::get('/financeiro/configuracao/', 'AdminController@index')->name('financeiro.create');
    Route::post('/financeiro/configuracao/salvar', 'AdminController@salvar');

    //Motivos de Rejeição
    Route::get('/motivos-rejeicao/', 'AdminController@motivosRejeicao')->name('motivosrejeicao.show');
    Route::get('/motivos-rejeicao/novo', 'AdminController@novoMotivoRejeicao')->name('motivosrejeicao.create');
    Route::post('/motivos-rejeicao/salvar', 'AdminController@salvarMotivoRejeicao');
    Route::get('/motivos-rejeicao/editar/{id}', 'AdminController@editarMotivoRejeicao')->name('motivosrejeicao.update');
    Route::put('/motivos-rejeicao/atualizar', 'AdminController@atualizarMotivoRejeicao');
    Route::delete('/motivos-rejeicao/remover', 'AdminController@removerMotivoRejeicao');


    // Cupom de Desconto
    Route::get('/cupom', 'CupomController@index');
    Route::get('/cupom/{id}', 'CupomController@find')->name('cupom.find');
    Route::put('/cupom/{id}', 'CupomController@update')->name('cupom.update');
	Route::post('/cupom', 'CupomController@store')->name('cupom.store');
	Route::delete('/cupom/{id}', 'CupomController@delete')->name('cupom.delete');

    //Logout
    Route::get('/logout',  'Auth\LoginController@logout');
});


Route::group(['middleware' => 'forceSSL'], function() {

    //Dash (Mudar depois)
    Route::get('/', 'Auth\LoginController@index');

    //WebService que busca endereços pelo CEP
    Route::get('/webservice/cep/{id}', 'WebServiceController@cep');

    // Autenticação de usuários
    Route::get('/login', 'Auth\LoginController@index');
    Route::post('/login', 'Auth\LoginController@authenticate');

    //Registro de novos usuários
    Route::get('/registrar/passo1', 'Auth\RegisterController@index');
    Route::post('/registrar/passo1', 'Auth\RegisterController@passo1');
    Route::get('/registrar/passo2', 'Auth\RegisterController@passo2');
    Route::post('/registrar/passo2', 'Auth\RegisterController@passo2');
    Route::get('/registrar/passo3', 'Auth\RegisterController@passo3');
    Route::post('/registrar/finalizar', 'Auth\RegisterController@passo3');

    // Tests with VueJs
    Route::get('/solicitacao/', 'Auth\RegisterController@create');

    Route::get('/registrar/recuperar-senha', 'Auth\RegisterController@recoveryPassword');
    Route::post('/register/sendLinkToNewPassword', 'Auth\RegisterController@sendLinkToNewPassword');

    Route::get('/registrar/redefinir-senha', 'Auth\RegisterController@showFormResetPassword');
    Route::post('/register/resetPassword', 'Auth\RegisterController@resetPassword');

    //Helper
    Route::post('/helper/getCidades/', 'HelperController@getCidades');
    Route::post('/helper/getRegioesPorCidade/', 'HelperController@getRegioesPorCidade');
    Route::post('/helper/getBairrosPorRegiao/', 'HelperController@getBairrosPorRegiao');
    Route::post('/helper/getServicosBase/', 'HelperController@getServicosBase');
});

Route::get('/testando', function () {
    $result = (new \App\Helpers\MercadoPago())->getMerchantOrder("703297687");

    echo "<pre>";
    var_dump($result);
    die();
});

// Rotas do checkout
Route::group([ "middleware" => ["cors"] ], function () {
    Route::options("/checkout", 'Checkout\StartCheckoutController@replyOptions');
    Route::options("/checkout/", 'Checkout\StartCheckoutController@replyOptions');
    Route::post("/checkout", 'Checkout\StartCheckoutController@startCheckout');
    Route::post("/checkout/", 'Checkout\StartCheckoutController@startCheckout');
});

