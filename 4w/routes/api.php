<?php

use Illuminate\Http\Request;


Route::group(['namespace' => '\API'], function()
{
    Route::get('/servicos', [
        'uses' => 'ServicoController@find'
    ]);

    Route::get('/servicos/base', [
        'uses' => 'ServicoBaseController@find'
    ]);

    Route::get('/servicos/base/agravantes', [
        'uses' => 'AgravanteController@find'
    ]);

    Route::get('/cep', [
        'uses' => 'LocalizacaoController@findAddressByCep'
    ]);

    Route::post('/servicos/orcamento', ['uses' => 'ServicoController@estimateCost']);

});

Route::get('/estados', 'WebServiceController@estados');
Route::get('/cidades', 'WebServiceController@cidades');
Route::get('/perfil', 'PerfilContaController@findAll');

Route::post('/mp/paymentNotification', 'OportunidadeController@paymentNotification');
Route::get('/mp/paymentNotification/{id}', ['as' => 'paymentNotification', 'uses' => 'MPNotificationsController@paymentNotification']);
Route::get('/mp/orderNotification/{id}', ['as' => 'orderNotification', 'uses' => 'MPNotificationsController@orderNotification']);

Route::post('/mp/notification', function (Request $request) {

    if (empty($id = $request->input('id')) || empty($topic = $request->input('topic'))) {
        abort(200);
    }

    switch ($topic) {
        case "payment":
            return redirect()->route('paymentNotification', ['id' => $id]);
        case "merchant_orders":
            return redirect()->route('orderNotification', ['id' => $id]);
        default:
            abort(200);
    }

});

Route::group(['middleware' => ['forceSSL', 'throttle:11,8'], 'namespace' => '\API'], function() {
    Route::post('/efetuar-pedido', 'ApiExecuteCheckout@execute');
    Route::post('/validar-pedido', 'ApiExecuteCheckout@prevalidate');
    Route::post('/pedir-cupom', 'ApiExecuteCheckout@requestGiftCard');
    Route::post('/tomador/registrar', 'ApiAuthenticateController@registerServiceTaker');
    Route::post('/tomador/login', 'ApiAuthenticateController@loginServiceTaker');
    Route::get('/tomador/verifica-email-existe', 'ApiAuthenticateController@checkEmailExists');

    Route::any("/checkout-hook", "CheckoutHookController@index");
});
